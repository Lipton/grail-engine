# Grail Engine

Welcome to the development repository of Grail, a graphics/game engine that I develop in my free time for the purpose of learning things like multi-platform development, C++, Vulkan, general graphics programming, project maintainability, etc.

![alt text](banner.jpg "Grail Engine")

# Features

- Multi-platform support (Windows, Linux, Android)
- Integrated editor
- Scripting support (AngelsScript)
- Custom GPU memory allocator
- Render graph implementation
- PBR deferred pipeline
- Cascaded Shadow Mapping
- Temporal AA, AO
- Per-object motion blur
- Resource system
- Frustrum culling
- Renderer supports different materials
- ImGUI integration
- Ability to save and load scenes to and from files


# To-Do List

- Unified file system
- Skeletal animations
- Compute support
- RTX support (only a test case is working currently)
- Code cleanup
- Documentation
- Better scene hierarchy management
- Multiple light support