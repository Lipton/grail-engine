#ifndef GRAIL_THREAD_POOL_H
#define GRAIL_THREAD_POOL_H

#include <thread>
#include <queue>
#include <mutex>
#include <condition_variable>

namespace grail {
	class Thread {
	public:
		Thread() {
			worker = std::thread(&Thread::processJobs, this);
		}

		~Thread() {
			if (worker.joinable()) {
				wait();
				queueMutex.lock();
				active = false;
				condition.notify_one();
				queueMutex.unlock();
				worker.join();
			}
		}

		void addJob(std::function<void()> function) {
			std::lock_guard<std::mutex> lock(queueMutex);
			jobQueue.push(std::move(function));
			condition.notify_one();
		}

		void wait() {
			std::unique_lock<std::mutex> lock(queueMutex);
			condition.wait(lock, [this]() {return jobQueue.empty(); });
		}
	private:
		bool active = true;
		std::thread worker;
		std::queue<std::function<void()>> jobQueue;
		std::mutex queueMutex;
		std::condition_variable condition;

		void processJobs() {
			while (true) {
				std::function<void()> job;
				{
					std::unique_lock<std::mutex> lock(queueMutex);
					condition.wait(lock, [this] {return !jobQueue.empty() || !active; });

					if (!active) {
						break;
					}

					job = jobQueue.front();
				}

				job();

				{
					std::lock_guard<std::mutex> lock(queueMutex);
					jobQueue.pop();
					condition.notify_one();
				}
			}
		}
	};

	template<typename T, typename... Args>
	std::unique_ptr<T> make_unique(Args&&... args) {
		return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
	}

	class ThreadPool {
	public:
		std::vector<std::unique_ptr<Thread>> threads;

		void allocateThreads(uint32_t count) {
			threads.clear();
			for (uint32_t i = 0; i < count; i++) {
				threads.push_back(make_unique<Thread>());
			}
		}

		void wait() {
			for (auto& thread : threads) {
				thread->wait();
			}
		}
	};
}

#endif
