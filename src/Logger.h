#ifndef GRAIL_LOGGER_H
#define GRAIL_LOGGER_H

#include <iostream>
#include <sstream>
#include <chrono>

#include "Platform.h"

#if defined(OS_ANDROID)
#include <android/log.h>
#endif

#undef ERROR

#define GRAIL_LOG(logType, tag) grail::Log(grail::LogType::logType, tag)

namespace grail {
	//Log type, used with Grail::Log
	enum class LogType {
		INFO = 0,
		DEBUG = 1,
		WARNING = 2,
		ERROR = 3
	};

	/*
	This namespace contains less frequently used logging functions, such as setting the log level and changing the current
	logger implementation
	*/
	namespace logger {
		void info(const std::string& tag, const std::string& message);
		void debug(const std::string& tag, const std::string& message);
		void warning(const std::string& tag, const std::string& message);
		void error(const std::string& tag, const std::string& message);

		/**
		Logger implementation interface, can be extended and passed to the logger to redirect
		the output of the logger

		Virtual functions:
		virtual const char* print(std::stringstream& ss, clock_t timestamp, LogType type, const char* log, const char* tag)

		Argument "ss":
		an std::stringstream object that is used to make the final string to print
		Argument "timestamp":
		time that has passed since the start of the application
		Argument "type":
		the type of log message (see Grail::LogType)
		Argument "log":
		the message that is logged
		Argument "tag":
		the user-set tag for the log message
		*/
		class Implementation {
		public:
			virtual void print(std::stringstream& ss, time_t timestamp, LogType type, const char* log, const char* tag) = 0;
			virtual ~Implementation() {};

			inline void outputMessage(const char* message) {
#if defined (OS_ANDROID)
				__android_log_print(ANDROID_LOG_DEBUG, "GRAIL", message, nullptr);
#else
				printf("%s", message);
#endif
			}
		};

		/*
		Returns:
		the string representation of the LogType
		*/
		const char* logTypeToString(LogType type);

		namespace detail {
			class DefaultImplemenentation : public Implementation {
				void print(std::stringstream& ss, time_t timestamp, LogType type, const char* log, const char* tag) {
					long minutes = timestamp / (1000 * 60);
					long seconds = timestamp / (1000) % 60;

					ss << "[" << (minutes <= 9 ? "0" : "") << minutes << ":" << (seconds <= 9 ? "0" : "") << seconds << "] [" << logTypeToString(type) << "] [" << tag << "]: " << log << "\n";

					outputMessage(ss.str().c_str());
				}
			};

			extern Implementation* logImplementation;
			extern std::chrono::high_resolution_clock::time_point startTime;
			extern int LOGGER_LOG_LEVEL;

			extern const char* logTypes[5];

			void dispose();
		}
		/*
		Sets the Logger's output implementation (see: Grail::Logger:Implementation)

		Argument "implementation":
		The pointer to the implementation

		Notes:
		setting a new implementation deletes the old one, so it's not advisable to keep pointers to implementations
		*/
		void setLoggerImplementation(Implementation* implementation);

		/*
		Sets the Logger's log level

		Argument "type":
		The log type to set (see Grail::LogTypes)

		Notes:
		The log types are

		INFO
		DEBUG
		WARNING
		ERROR

		setting a log level, filters out any logs whose log type is below the current type, for example
		a log level of TRACE shows all logs, while a log level of DEBUG shows only DEBUG, WARNING and ERROR logs
		*/
		void setLogLevel(LogType type);
	}

	class Log {
	public:
		/*
		The Log class logs the specified message, using the currently set logger implementation (see: Grail::Logger::Implementation)
		can be toggled off by defining GRAIL_NO_LOG

		Notes:
		Toggling logging off, makes the calls to Log() do nothing, removing the overhead

		Example Use:
		Grail::Log(Grail::LogType::INFO, "TEST") << "this is a test " << 123;
		the default implementation would print:
		[0:0] [INFO] [TEST]: this is a test 123
		*/
		Log(LogType type, const char* tag)
#ifndef GRAIL_NO_LOG
			:type(type), tag(tag)
		{
			print = (int)type >= logger::detail::LOGGER_LOG_LEVEL;
		}
#else
		{}
#endif
		~Log()
#ifndef GRAIL_NO_LOG
		{
			if (print) {
				const std::string composedLog = ss.str();

				ss.str(std::string(""));
				logger::detail::logImplementation->print(ss,
					std::chrono::duration_cast<std::chrono::milliseconds>((std::chrono::high_resolution_clock::now() - logger::detail::startTime)).count(),
					type, composedLog.c_str(), tag);

				/*#if defined(OS_ANDROID)
				__android_log_print(ANDROID_LOG_INFO, " ", ss.str().c_str(), nullptr);
				#else
				std::cout << ss.str() << std::endl;
				#endif*/
			}
		}
#else
		{}
#endif

		template<class T>
		Log& operator << (const T& t) {
#ifndef GRAIL_NO_LOG
			if (print)
				ss << t;

			return *this;
#endif
			return *this;
		}
	private:
#ifndef GRAIL_NO_LOG
		bool print;
		LogType type;
		std::stringstream ss;
		const char* tag;
#endif
	};
}

#endif
