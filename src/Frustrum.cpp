#include "Frustum.h"

grail::Frustum::Frustum() {
}

void grail::Frustum::update(glm::mat4 matrix) {
	planes[LEFT].x = matrix[0].w + matrix[0].x;
	planes[LEFT].y = matrix[1].w + matrix[1].x;
	planes[LEFT].z = matrix[2].w + matrix[2].x;
	planes[LEFT].w = matrix[3].w + matrix[3].x;

	planes[RIGHT].x = matrix[0].w - matrix[0].x;
	planes[RIGHT].y = matrix[1].w - matrix[1].x;
	planes[RIGHT].z = matrix[2].w - matrix[2].x;
	planes[RIGHT].w = matrix[3].w - matrix[3].x;

	planes[TOP].x = matrix[0].w - matrix[0].y;
	planes[TOP].y = matrix[1].w - matrix[1].y;
	planes[TOP].z = matrix[2].w - matrix[2].y;
	planes[TOP].w = matrix[3].w - matrix[3].y;

	planes[BOTTOM].x = matrix[0].w + matrix[0].y;
	planes[BOTTOM].y = matrix[1].w + matrix[1].y;
	planes[BOTTOM].z = matrix[2].w + matrix[2].y;
	planes[BOTTOM].w = matrix[3].w + matrix[3].y;

	planes[BACK].x = matrix[0].w + matrix[0].z;
	planes[BACK].y = matrix[1].w + matrix[1].z;
	planes[BACK].z = matrix[2].w + matrix[2].z;
	planes[BACK].w = matrix[3].w + matrix[3].z;

	planes[FRONT].x = matrix[0].w - matrix[0].z;
	planes[FRONT].y = matrix[1].w - matrix[1].z;
	planes[FRONT].z = matrix[2].w - matrix[2].z;
	planes[FRONT].w = matrix[3].w - matrix[3].z;

	for (auto i = 0; i < 6; i++) {
		float length = sqrtf(planes[i].x * planes[i].x + planes[i].y * planes[i].y + planes[i].z * planes[i].z);
		planes[i] /= length;
	}
}

bool grail::Frustum::pointInFrustum(float x, float y, float z) {
	for (glm::vec4 plane : planes) {
		float dist = plane.x * x + plane.y * y + plane.z * z + plane.w;
		if (dist < 0) return false;
	}
	return true;
}

bool grail::Frustum::pointInFrustum(glm::vec3  point) {
	return pointInFrustum(point.x, point.y, point.z);
}

bool grail::Frustum::sphereInFrustum(float x, float y, float z, float radius) {
	for (glm::vec4 plane : planes) {
		if ((plane.x * x) + (plane.y * y) + (plane.z * z) + plane.w <= -radius) return false;
	}
	return true;
}

bool grail::Frustum::sphereInFrustum(glm::vec3 position, float radius) {
	return sphereInFrustum(position.x, position.y, position.z, radius);
}

bool grail::Frustum::AABBInFrustum(glm::vec3 min, glm::vec3 max) {
	// check box outside/inside of frustum
	for (int i = 0; i < 6; i++) {
		int out = 0;
		out += ((glm::dot(planes[i], glm::vec4(min.x, min.y, min.z, 1.0f)) < 0.0) ? 1 : 0);
		out += ((glm::dot(planes[i], glm::vec4(max.x, min.y, min.z, 1.0f)) < 0.0) ? 1 : 0);
		out += ((glm::dot(planes[i], glm::vec4(min.x, max.y, min.z, 1.0f)) < 0.0) ? 1 : 0);
		out += ((glm::dot(planes[i], glm::vec4(max.x, max.y, min.z, 1.0f)) < 0.0) ? 1 : 0);
		out += ((glm::dot(planes[i], glm::vec4(min.x, min.y, max.z, 1.0f)) < 0.0) ? 1 : 0);
		out += ((glm::dot(planes[i], glm::vec4(max.x, min.y, max.z, 1.0f)) < 0.0) ? 1 : 0);
		out += ((glm::dot(planes[i], glm::vec4(min.x, max.y, max.z, 1.0f)) < 0.0) ? 1 : 0);
		out += ((glm::dot(planes[i], glm::vec4(max.x, max.y, max.z, 1.0f)) < 0.0) ? 1 : 0);

		if (out == 8) return false;
	}

	return true;
}

glm::vec4 grail::Frustum::normalizePlane(const glm::vec4 plane) {
	glm::vec3 normal(plane.x, plane.y, plane.z);
	float length = glm::length(normal);
	return plane / length;
}
