#ifndef GRAIL_SHADER_H
#define GRAIL_SHADER_H

#include "VulkanContext.h"

namespace grail {
#undef VOID

	enum class ShaderVarBaseTypeT {
		UNKNOWN,
		VOID,
		BOOLEAN,
		CHAR,
		SBYTE,
		UBYTE,
		SHORT,
		USHORT,
		INT,
		UINT,
		INT64,
		ATOMIC_COUNTER,
		HALF,
		FLOAT,
		DOUBLE,
		STRUCT,
		IMAGE,
		SAMPLED_IMAGE,
		SAMPLER
	};

#define VOID void

	enum class ShaderVarTypeT {
		UNKNOWN,
		SCALAR,
		VECTOR,
		MATRIX
	};

	enum class ShaderImageDim {
		_1D,
		_2D,
		_3D,
		CUBE,
		RECT,
		BUFFER,
		SUBBPASS_DATA
	};

	class ShaderVarT {
		friend class ShaderLoader;
	public:

	protected:
		ShaderVarT() {}
	public:
		std::string name = "";

		ShaderVarBaseTypeT baseType = ShaderVarBaseTypeT::UNKNOWN;
		ShaderVarTypeT type = ShaderVarTypeT::UNKNOWN;

		uint32_t rows = 0;
		uint32_t columns = 0;
		std::vector<uint32_t> dimensions;
	};

	class ImageShaderVar : public ShaderVarT {
		friend class ShaderLoader;
	public:
		ImageShaderVar() {}

		uint32_t set = 0;
		uint32_t binding = 0;

		ShaderImageDim dim = ShaderImageDim::_1D;
		bool arrayed = false;

		//Interal Use
		uint32_t resourceID;
	};

	class IOShaderVar : public ShaderVarT {
		friend class ShaderLoader;
	public:
		IOShaderVar() {}

		uint32_t location = 0;
	};

	class BufferBlockShaderVar : public ShaderVarT {
		friend class ShaderLoader;
	public:
		BufferBlockShaderVar() {}

		std::string bufferName = "";

		uint32_t size = 0;
		uint32_t offset = 0;

		uint32_t stride = 0;
	};

	class BufferBlock {
		friend class ShaderLoader;
		friend class Shader;
		friend class ShaderPermutation;
	public:
		BufferBlock() {}

		std::string name = "";
		uint32_t memberCount = 0;
		uint32_t size = 0;

		uint32_t binding = 0;
		uint32_t set = 0;

		std::vector<BufferBlockShaderVar> variables;

		BufferBlockShaderVar* getVarByName(const std::string& name);
	};

	class ShaderPermutation {
		friend class ShaderLoader;
		friend class Shader;
	public:
		ShaderPermutation() {}

		VkShaderModule* getModule();
	public:
		std::vector<std::string> defines;

		std::vector<IOShaderVar> inputs;
		std::vector<IOShaderVar> outputs;

		std::vector<BufferBlock> uniformBuffers;
		std::vector<BufferBlock> storageBuffers;

		std::vector<ImageShaderVar> sampledImages;
		std::vector<ImageShaderVar> storageImages;

		BufferBlock pushConstants;

		ShaderStageBits stage = ShaderStageBits::ALL;

		bool hasUniformBuffer(const std::string& name);
		BufferBlock& getUniformBuffer(const std::string& name);
	private:
		VkShaderModule handle = VK_NULL_HANDLE;
	};

	class Shader {
		friend class ShaderLoader;
	public:
		Shader() {}

		ShaderPermutation* getPermutation(uint32_t index);
		ShaderPermutation* getBasePermutation();

		ShaderStageBits getStage();

		ShaderPermutation* compilePermutation(std::vector<std::string>& defines, bool remapDescriptors = false);
	private:
		std::string identifier;

		std::string source = "";

		ShaderStageBits stage = ShaderStageBits::ALL;

		std::vector<ShaderPermutation*> permutationMap;
	};
}

#endif