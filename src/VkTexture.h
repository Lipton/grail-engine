#ifndef GRAIL_TEXTURE_H
#define GRAIL_TEXTURE_H

#include "Common.h"
#include "MemoryRegion.h"
#include "PipelineFactory.h"
#include "Resource.h"
#include "MemoryAllocator.h"

namespace grail {
	struct SamplerInfo {
		SamplerFilter magFilter = SamplerFilter::LINEAR;
		SamplerFilter minFilter = SamplerFilter::LINEAR;

		SamplerMipmapMode mipmapMode = SamplerMipmapMode::LINEAR;

		SamplerAddressMode addressModeU = SamplerAddressMode::REPEAT;
		SamplerAddressMode addressModeV = SamplerAddressMode::REPEAT;
		SamplerAddressMode addressModeW = SamplerAddressMode::REPEAT;

		float mipLodBias = 0.0f;

		VkBool32 anisotropyEnable = false;
		float maxAnisotropy = 0.0f;

		VkBool32 compareEnable = false;
		CompareOp compareOp = CompareOp::NEVER;

		VkBool32 autoMaxLod = true;

		float minLod = 0.0f;
		float maxLod = 0.0f;

		SamplerBorderColor borderColor = SamplerBorderColor::FLOAT_OPAQUE_WHITE;

		VkBool32 unnormalizedCoordinates = false;
	};

	struct TextureCreateInfo {
		SamplerInfo samplerInfo = {};

		ImageType type = ImageType::TYPE_2D;
		ImageTiling tiling = ImageTiling::OPTIMAL;
		ImageUsageBits usageBits = ImageUsageBits::TRANSFER_SRC_BIT | ImageUsageBits::TRANSFER_DST_BIT | ImageUsageBits::SAMPLED_BIT;
		SharingMode sharingMode = SharingMode::EXCLUSIVE;

		// Usually when creating a texture, it's espected that it will be used to read from
		ImageLayout initialLayout = ImageLayout::SHADER_READ_ONLY_OPTIMAL;

		VkImageCreateFlags createFlags = 0;

		VkFormat format = VK_FORMAT_R8G8B8A8_UNORM;

		SampleCountBits sampleCount = SampleCountBits::COUNT_1_BIT;

		uint32_t arrayLevels = 1;
		uint32_t bpp = 4;

		ComponentSwizzle swizzleMasks[4] = { ComponentSwizzle::RED, ComponentSwizzle::GREEN, ComponentSwizzle::BLUE, ComponentSwizzle::ALPHA };

		//VkImageUsageFlags usageFlags = VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;

		uint32_t mipLevels = 0;
		VkBool32 generateMipmaps = true;

		GPU gpu = VulkanContext::mainGPU;
	};

	class VkTexture {
		friend class TextureLoader;
		friend class FrameGraph;
		friend class RenderPass;

		friend class Editor;
	public:
		const std::string& getIdentifier();
		ResourceType getResourceType();

		const GPU& getGPU();
		const MemoryAllocation& getMemoryRegion();

		VkSampler getSampler();
		VkImage getImage();

		VkImageLayout getLayout();

		VkImageAspectFlags getAspect();

		// Retrieve the default view
		VkImageView getImageView();
		VkImageViewType getViewType();

		// Retrieve the specified view, if it not found, it's created and hashed
		VkImageView getImageView(VkImageAspectFlags aspect, VkImageSubresourceRange subresourceRange, VkImageViewType type);
		VkImageView getImageView(VkImageSubresourceRange subresourceRange, VkImageViewType type);
		VkImageView getImageView(VkImageSubresourceRange subresourceRange);

		uint32_t getWidth();
		uint32_t getHeight();
		uint32_t getDepth();
		uint32_t getLayerCount();
		uint32_t getMipCount();

		VkFormat getFormat();

		VkDescriptorImageInfo& getDescriptor(VkImageLayout layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		VkDescriptorImageInfo& getDescriptor(VkImageView view, VkImageLayout layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);


		// Transition specified image subresource to layout with specified barriers
		void transitionImage(VkCommandBuffer cmd, VkImageLayout newLayout, VkImageSubresourceRange subresource, VkPipelineStageFlags srcStageMask, VkPipelineStageFlags dstStageMask);
		
		// Transition specified image subresources to specified layout
		void transitionImage(VkCommandBuffer cmd, VkImageLayout newLayout, VkImageSubresourceRange subresource);
		
		// Transition the whole image to specified layout
		void transitionImage(VkCommandBuffer cmd, VkImageLayout newLayout);



		// TEMPORARY
		std::string identifier;
		void setImageLayout(VkCommandBuffer cmdbuffer, VkImageAspectFlags aspectMask, VkImageLayout oldImageLayout, VkImageLayout newImageLayout, VkImageSubresourceRange subresourceRange, VkPipelineStageFlags srcStageMask, VkPipelineStageFlags dstStageMask);
		void setImageLayout(VkCommandBuffer cmdbuffer, VkImageAspectFlags aspectMask, VkImageLayout oldImageLayout, VkImageLayout newImageLayout, VkImageSubresourceRange subresourceRange);
	
		void dispose();
	protected:
		void createSampler(SamplerInfo& samplerInfo);
	private:
		// Perform a transition with a specified old layout
		void transitionImageInternal(VkCommandBuffer cmd, VkImageLayout oldLayout, VkImageLayout newLayout, VkImageSubresourceRange subresource, VkPipelineStageFlags srcStageMask, VkPipelineStageFlags dstStageMask);
		void transitionImageInternal(VkCommandBuffer cmd, VkImageLayout oldLayout, VkImageLayout newLayout, VkImageSubresourceRange subresource);
		void transitionImageInternal(VkCommandBuffer cmd, VkImageLayout oldLayout, VkImageLayout newLayout);

		VkDescriptorImageInfo descriptorImageInfo;

		ResourceType resrouceType = ResourceType::UNKNOWN;

		GPU gpu;
		MemoryAllocation memoryDescriptor;

		VkSampler sampler;

		VkImageAspectFlags aspectMask;

		VkImage image;
	public:
		VkImageLayout imageLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	private:

		VkImageViewType viewType;
		VkImageView imageView;

		uint32_t width;
		uint32_t height;
		uint32_t depth = 1;
		uint32_t mipLevels = 1;
		uint32_t layerLevels = 1;

		ComponentSwizzle swizzleMasks[4] = { ComponentSwizzle::RED, ComponentSwizzle::GREEN, ComponentSwizzle::BLUE, ComponentSwizzle::ALPHA };
		VkFormat format;

		std::unordered_map<uint32_t, VkImageView> customViews;
	};
}

#endif