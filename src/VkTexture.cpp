#include "VkTexture.h"

/*VkImageLayout grail::VkTexture::getImageLayout(uint32_t layer, uint32_t mip) {
	return imageLayouts[layer][mip];
}

bool grail::VkTexture::isLayoutUniform() {
	VkImageLayout layout = getImageLayout();

	for (uint32_t l = 0; l < layerLevels; l++) {
		for (uint32_t m = 0; m < mipLevels; m++) {
			if (layout != imageLayouts[l][m]) {
				return false;
			}
		}
	}
	
	return true;
}*/


VkDescriptorImageInfo & grail::VkTexture::getDescriptor(VkImageLayout layout) {
	return getDescriptor(imageView, layout);
}

VkDescriptorImageInfo & grail::VkTexture::getDescriptor(VkImageView view, VkImageLayout layout) {
	// NOTE, THIS MIGHT NOT BE ALRIGHT
	descriptorImageInfo.imageLayout = layout;

	descriptorImageInfo.imageView = view;
	descriptorImageInfo.sampler = sampler;

	return descriptorImageInfo;
}

void grail::VkTexture::createSampler(SamplerInfo & info) {
	VkSamplerCreateInfo samplerInfo = {};
	samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
	samplerInfo.flags = 0;
	samplerInfo.pNext = nullptr;
	samplerInfo.magFilter = static_cast<VkFilter>(info.magFilter);
	samplerInfo.minFilter = static_cast<VkFilter>(info.minFilter);
	samplerInfo.mipmapMode = static_cast<VkSamplerMipmapMode>(info.mipmapMode);
	samplerInfo.addressModeU = static_cast<VkSamplerAddressMode>(info.addressModeU);
	samplerInfo.addressModeV = static_cast<VkSamplerAddressMode>(info.addressModeV);
	samplerInfo.addressModeW = static_cast<VkSamplerAddressMode>(info.addressModeW);
	samplerInfo.mipLodBias = info.mipLodBias;
	samplerInfo.compareOp = static_cast<VkCompareOp>(info.compareOp);
	samplerInfo.minLod = info.minLod;
	samplerInfo.maxLod = (float)mipLevels;
	samplerInfo.maxAnisotropy = info.maxAnisotropy;
	samplerInfo.anisotropyEnable = info.anisotropyEnable;
	samplerInfo.borderColor = static_cast<VkBorderColor>(info.borderColor);

	VK_VALIDATE_RESULT(vkCreateSampler(gpu.device, &samplerInfo, nullptr, &sampler));
}

void grail::VkTexture::transitionImageInternal(VkCommandBuffer cmd, VkImageLayout oldLayout, VkImageLayout newLayout, VkImageSubresourceRange subresource, VkPipelineStageFlags srcStageMask, VkPipelineStageFlags dstStageMask) {
	// Create an image barrier object
	VkImageMemoryBarrier imageMemoryBarrier = {};
	imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
	imageMemoryBarrier.pNext = nullptr;
	imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	imageMemoryBarrier.oldLayout = oldLayout;
	imageMemoryBarrier.newLayout = newLayout;
	imageMemoryBarrier.image = image;
	imageMemoryBarrier.subresourceRange = subresource;

	// Source layouts (old)
	// Source access mask controls actions that have to be finished on the old layout
	// before it will be transitioned to the new layout
	switch (oldLayout)
	{
	case VK_IMAGE_LAYOUT_UNDEFINED:
		// Image layout is undefined (or does not matter)
		// Only valid as initial layout
		// No flags required, listed only for completeness
		imageMemoryBarrier.srcAccessMask = 0;
		break;

	case VK_IMAGE_LAYOUT_PREINITIALIZED:
		// Image is preinitialized
		// Only valid as initial layout for linear images, preserves memory contents
		// Make sure host writes have been finished
		imageMemoryBarrier.srcAccessMask = VK_ACCESS_HOST_WRITE_BIT;
		break;

	case VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL:
		// Image is a color attachment
		// Make sure any writes to the color buffer have been finished
		imageMemoryBarrier.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		break;

	case VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL:
		// Image is a depth/stencil attachment
		// Make sure any writes to the depth/stencil buffer have been finished
		imageMemoryBarrier.srcAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
		break;

	case VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL:
		// Image is a transfer source 
		// Make sure any reads from the image have been finished
		imageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
		break;

	case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:
		// Image is a transfer destination
		// Make sure any writes to the image have been finished
		imageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		break;

	case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL:
		// Image is read by a shader
		// Make sure any shader reads from the image have been finished
		imageMemoryBarrier.srcAccessMask = VK_ACCESS_SHADER_READ_BIT;
		break;
	default:
		// Other source layouts aren't handled (yet)
		break;
	}

	// Target layouts (new)
	// Destination access mask controls the dependency for the new image layout
	switch (newLayout)
	{
	case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:
		// Image will be used as a transfer destination
		// Make sure any writes to the image have been finished
		imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		break;

	case VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL:
		// Image will be used as a transfer source
		// Make sure any reads from the image have been finished
		imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
		break;

	case VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL:
		// Image will be used as a color attachment
		// Make sure any writes to the color buffer have been finished
		imageMemoryBarrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		break;

	case VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL:
		// Image layout will be used as a depth/stencil attachment
		// Make sure any writes to depth/stencil buffer have been finished
		imageMemoryBarrier.dstAccessMask = imageMemoryBarrier.dstAccessMask | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
		break;

	case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL:
		// Image will be read in a shader (sampler, input attachment)
		// Make sure any writes to the image have been finished
		if (imageMemoryBarrier.srcAccessMask == 0)
		{
			imageMemoryBarrier.srcAccessMask = VK_ACCESS_HOST_WRITE_BIT | VK_ACCESS_TRANSFER_WRITE_BIT;
		}
		imageMemoryBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
		break;
	default:
		// Other source layouts aren't handled (yet)
		break;
	}


	// Put barrier on top
	VkPipelineStageFlags srcStageFlags = srcStageMask;
	VkPipelineStageFlags destStageFlags = dstStageMask;

	// NOTE only change layout variable if the whole image is being transitioned
	if (subresource.baseArrayLayer == 0 &&
		subresource.baseMipLevel == 0 &&
		subresource.layerCount == layerLevels &&
		subresource.levelCount == mipLevels) {
		this->imageLayout = newLayout;
	}

	// Put barrier inside setup command buffer
	vkCmdPipelineBarrier(
		cmd,
		srcStageFlags,
		destStageFlags,
		0,
		0, nullptr,
		0, nullptr,
		1, &imageMemoryBarrier);

	/*for (uint32_t l = subresourceRange.baseArrayLayer - 1; l < subresourceRange.layerCount; l++) {
		for (uint32_t m = subresourceRange.baseMipLevel - 1; m < subresourceRange.levelCount; m++) {
			imageLayouts[l][m] = newLayout;
		}
	}*/
}

void grail::VkTexture::transitionImageInternal(VkCommandBuffer cmd, VkImageLayout oldLayout, VkImageLayout newLayout, VkImageSubresourceRange subresource) {
	transitionImageInternal(
		cmd,
		oldLayout,
		newLayout,
		subresource,
		VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
		VK_PIPELINE_STAGE_ALL_COMMANDS_BIT);
}

void grail::VkTexture::transitionImageInternal(VkCommandBuffer cmd, VkImageLayout oldLayout, VkImageLayout newLayout) {
	VkImageSubresourceRange subresource;
	subresource.aspectMask = aspectMask;
	subresource.baseArrayLayer = 0;
	subresource.baseMipLevel = 0;
	subresource.layerCount = layerLevels;
	subresource.levelCount = mipLevels;

	transitionImageInternal(
		cmd,
		oldLayout,
		newLayout,
		subresource,
		VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
		VK_PIPELINE_STAGE_ALL_COMMANDS_BIT);
}

void grail::VkTexture::setImageLayout(VkCommandBuffer cmdbuffer, VkImageAspectFlags aspectMask, VkImageLayout oldImageLayout, VkImageLayout newImageLayout, VkImageSubresourceRange subresourceRange) {
	// Create an image barrier object
	VkImageMemoryBarrier imageMemoryBarrier = {};
	imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
	imageMemoryBarrier.pNext = nullptr;
	imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	imageMemoryBarrier.oldLayout = oldImageLayout;
	imageMemoryBarrier.newLayout = newImageLayout;
	imageMemoryBarrier.image = image;
	imageMemoryBarrier.subresourceRange = subresourceRange;

	// Source layouts (old)
	// Source access mask controls actions that have to be finished on the old layout
	// before it will be transitioned to the new layout
	switch (oldImageLayout)
	{
	case VK_IMAGE_LAYOUT_UNDEFINED:
		// Image layout is undefined (or does not matter)
		// Only valid as initial layout
		// No flags required, listed only for completeness
		imageMemoryBarrier.srcAccessMask = 0;
		break;

	case VK_IMAGE_LAYOUT_PREINITIALIZED:
		// Image is preinitialized
		// Only valid as initial layout for linear images, preserves memory contents
		// Make sure host writes have been finished
		imageMemoryBarrier.srcAccessMask = VK_ACCESS_HOST_WRITE_BIT;
		break;

	case VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL:
		// Image is a color attachment
		// Make sure any writes to the color buffer have been finished
		imageMemoryBarrier.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		break;

	case VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL:
		// Image is a depth/stencil attachment
		// Make sure any writes to the depth/stencil buffer have been finished
		imageMemoryBarrier.srcAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
		break;

	case VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL:
		// Image is a transfer source 
		// Make sure any reads from the image have been finished
		imageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
		break;

	case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:
		// Image is a transfer destination
		// Make sure any writes to the image have been finished
		imageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		break;

	case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL:
		// Image is read by a shader
		// Make sure any shader reads from the image have been finished
		imageMemoryBarrier.srcAccessMask = VK_ACCESS_SHADER_READ_BIT;
		break;
	default:
		// Other source layouts aren't handled (yet)
		break;
	}

	// Target layouts (new)
	// Destination access mask controls the dependency for the new image layout
	switch (newImageLayout)
	{
	case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:
		// Image will be used as a transfer destination
		// Make sure any writes to the image have been finished
		imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		break;

	case VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL:
		// Image will be used as a transfer source
		// Make sure any reads from the image have been finished
		imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
		break;

	case VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL:
		// Image will be used as a color attachment
		// Make sure any writes to the color buffer have been finished
		imageMemoryBarrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		break;

	case VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL:
		// Image layout will be used as a depth/stencil attachment
		// Make sure any writes to depth/stencil buffer have been finished
		imageMemoryBarrier.dstAccessMask = imageMemoryBarrier.dstAccessMask | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
		break;

	case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL:
		// Image will be read in a shader (sampler, input attachment)
		// Make sure any writes to the image have been finished
		if (imageMemoryBarrier.srcAccessMask == 0)
		{
			imageMemoryBarrier.srcAccessMask = VK_ACCESS_HOST_WRITE_BIT | VK_ACCESS_TRANSFER_WRITE_BIT;
		}
		imageMemoryBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
		break;
	default:
		// Other source layouts aren't handled (yet)
		break;
	}


	// Put barrier on top
	VkPipelineStageFlags srcStageFlags = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;
	VkPipelineStageFlags destStageFlags = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;

	/*Log(LogType::INFO, "IMG OLD LAYOUT") << oldImageLayout;
	Log(LogType::INFO, "IMG NEW LAYOUT") << newImageLayout;
	Log(LogType::INFO, "IMG LAYOUT SRC") << imageMemoryBarrier.srcAccessMask;
	Log(LogType::INFO, "IMG LAYOUT DST") << imageMemoryBarrier.dstAccessMask;*/

	// Put barrier inside setup command buffer
	vkCmdPipelineBarrier(
		cmdbuffer,
		srcStageFlags,
		destStageFlags,
		0,
		0, nullptr,
		0, nullptr,
		1, &imageMemoryBarrier);
}

void grail::VkTexture::dispose() {
	memoryDescriptor.page->allocator->deallocate(memoryDescriptor);

	vkDestroySampler(gpu.device, sampler, nullptr);
	vkDestroyImage(gpu.device, image, nullptr);

	vkDestroyImageView(gpu.device, imageView, nullptr);

	for (auto& it : customViews) {
		vkDestroyImageView(gpu.device, it.second, nullptr);
	}

	customViews.clear();
}

void grail::VkTexture::setImageLayout(VkCommandBuffer cmdbuffer, VkImageAspectFlags aspectMask, VkImageLayout oldImageLayout, VkImageLayout newImageLayout, VkImageSubresourceRange subresourceRange, VkPipelineStageFlags srcStageMask, VkPipelineStageFlags dstStageMask) {
	// Create an image barrier object
	VkImageMemoryBarrier imageMemoryBarrier = {};
	imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
	imageMemoryBarrier.pNext = nullptr;
	imageMemoryBarrier.oldLayout = oldImageLayout;
	imageMemoryBarrier.newLayout = newImageLayout;
	imageMemoryBarrier.image = image;
	imageMemoryBarrier.subresourceRange = subresourceRange;

	// Source layouts (old)
	// Source access mask controls actions that have to be finished on the old layout
	// before it will be transitioned to the new layout
	switch (oldImageLayout)
	{
	case VK_IMAGE_LAYOUT_UNDEFINED:
		// Image layout is undefined (or does not matter)
		// Only valid as initial layout
		// No flags required, listed only for completeness
		imageMemoryBarrier.srcAccessMask = 0;
		break;

	case VK_IMAGE_LAYOUT_PREINITIALIZED:
		// Image is preinitialized
		// Only valid as initial layout for linear images, preserves memory contents
		// Make sure host writes have been finished
		imageMemoryBarrier.srcAccessMask = VK_ACCESS_HOST_WRITE_BIT;
		break;

	case VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL:
		// Image is a color attachment
		// Make sure any writes to the color buffer have been finished
		imageMemoryBarrier.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		break;

	case VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL:
		// Image is a depth/stencil attachment
		// Make sure any writes to the depth/stencil buffer have been finished
		imageMemoryBarrier.srcAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
		break;

	case VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL:
		// Image is a transfer source 
		// Make sure any reads from the image have been finished
		imageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
		break;

	case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:
		// Image is a transfer destination
		// Make sure any writes to the image have been finished
		imageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		break;

	case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL:
		// Image is read by a shader
		// Make sure any shader reads from the image have been finished
		imageMemoryBarrier.srcAccessMask = VK_ACCESS_SHADER_READ_BIT;
		break;
	}

	// Target layouts (new)
	// Destination access mask controls the dependency for the new image layout
	switch (newImageLayout)
	{
	case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:
		// Image will be used as a transfer destination
		// Make sure any writes to the image have been finished
		imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		break;

	case VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL:
		// Image will be used as a transfer source
		// Make sure any reads from and writes to the image have been finished
		//imageMemoryBarrier.srcAccessMask = imageMemoryBarrier.srcAccessMask | VK_ACCESS_TRANSFER_READ_BIT;
		imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
		break;

	case VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL:
		// Image will be used as a color attachment
		// Make sure any writes to the color buffer have been finished
		imageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
		imageMemoryBarrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		break;

	case VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL:
		// Image layout will be used as a depth/stencil attachment
		// Make sure any writes to depth/stencil buffer have been finished
		imageMemoryBarrier.dstAccessMask = imageMemoryBarrier.dstAccessMask | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
		break;

	case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL:
		// Image will be read in a shader (sampler, input attachment)
		// Make sure any writes to the image have been finished
		if (imageMemoryBarrier.srcAccessMask == 0)
		{
			imageMemoryBarrier.srcAccessMask = VK_ACCESS_HOST_WRITE_BIT | VK_ACCESS_TRANSFER_WRITE_BIT;
		}
		imageMemoryBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
		break;
	}

	// Put barrier inside setup command buffer
	vkCmdPipelineBarrier(
		cmdbuffer,
		srcStageMask,
		dstStageMask,
		0,
		0, nullptr,
		0, nullptr,
		1, &imageMemoryBarrier);
}

void grail::VkTexture::transitionImage(VkCommandBuffer cmd, VkImageLayout newLayout, VkImageSubresourceRange subresource, VkPipelineStageFlags srcStageMask, VkPipelineStageFlags dstStageMask) {
	transitionImageInternal(cmd, this->imageLayout, newLayout, subresource, srcStageMask, dstStageMask);
}

void grail::VkTexture::transitionImage(VkCommandBuffer cmd, VkImageLayout newLayout, VkImageSubresourceRange subresource) {
	transitionImage(
		cmd,
		newLayout,
		subresource,
		VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
		VK_PIPELINE_STAGE_ALL_COMMANDS_BIT);
}

void grail::VkTexture::transitionImage(VkCommandBuffer cmd, VkImageLayout newLayout) {
	VkImageSubresourceRange subresource;
	subresource.aspectMask = aspectMask;
	subresource.baseArrayLayer = 0;
	subresource.baseMipLevel = 0;
	subresource.layerCount = layerLevels;
	subresource.levelCount = mipLevels;
	
	transitionImage(
		cmd,
		newLayout,
		subresource,
		VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
		VK_PIPELINE_STAGE_ALL_COMMANDS_BIT);
}

const std::string& grail::VkTexture::getIdentifier() {
	return identifier;
}

grail::ResourceType grail::VkTexture::getResourceType() {
	return resrouceType;
}

const grail::GPU & grail::VkTexture::getGPU() {
	return gpu;
}

const grail::MemoryAllocation & grail::VkTexture::getMemoryRegion() {
	return memoryDescriptor;
}

VkSampler grail::VkTexture::getSampler() {
	return sampler;
}

VkImage grail::VkTexture::getImage() {
	return image;
}

VkImageLayout grail::VkTexture::getLayout() {
	return imageLayout;
}

VkImageAspectFlags grail::VkTexture::getAspect() {
	return aspectMask;
}

VkImageView grail::VkTexture::getImageView() {
	return imageView;
}

VkImageViewType grail::VkTexture::getViewType() {
	return viewType;
}

VkImageView grail::VkTexture::getImageView(VkImageAspectFlags aspect, VkImageSubresourceRange subresourceRange, VkImageViewType type) {
	// Hash the relevant information of the image view

	size_t hash = 0;

	if (aspect == VK_IMAGE_ASPECT_FLAG_BITS_MAX_ENUM) {
		aspect = getAspect();
	}

	subresourceRange.aspectMask = aspect;

	utils::hash::combineHash(hash, type);
	utils::hash::combineHash(hash, subresourceRange.baseArrayLayer);
	utils::hash::combineHash(hash, subresourceRange.baseMipLevel);
	utils::hash::combineHash(hash, subresourceRange.layerCount);
	utils::hash::combineHash(hash, subresourceRange.levelCount);
	utils::hash::combineHash(hash, subresourceRange.aspectMask);

	// Check if a view with this configuration already exists, if so, return it, else create one
	if (customViews.find(hash) != customViews.end()) {
		return customViews[hash];
	}

	VkImageView view;

	VkImageViewCreateInfo viewInfo = {};
	viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	viewInfo.pNext = nullptr;
	viewInfo.viewType = type;
	viewInfo.format = format;
	viewInfo.subresourceRange = subresourceRange;
	viewInfo.image = image;
	VK_VALIDATE_RESULT(vkCreateImageView(gpu.device, &viewInfo, nullptr, &view));

	customViews[hash] = view;

	return view;
}

VkImageView grail::VkTexture::getImageView(VkImageSubresourceRange subresourceRange, VkImageViewType type) {
	return getImageView(VK_IMAGE_ASPECT_FLAG_BITS_MAX_ENUM, subresourceRange, type);
}

VkImageView grail::VkTexture::getImageView(VkImageSubresourceRange subresourceRange) {
	return getImageView(VK_IMAGE_ASPECT_FLAG_BITS_MAX_ENUM, subresourceRange, viewType);
}

uint32_t grail::VkTexture::getWidth() {
	return width;
}

uint32_t grail::VkTexture::getHeight() {
	return height;
}

uint32_t grail::VkTexture::getDepth() {
	return depth;
}

uint32_t grail::VkTexture::getLayerCount() {
	return layerLevels;
}

uint32_t grail::VkTexture::getMipCount() {
	return mipLevels;
}

VkFormat grail::VkTexture::getFormat() {
	return format;
}
