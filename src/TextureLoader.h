#ifndef GRAIL_TEXTURE_LOADER_H
#define GRAIL_TEXTURE_LOADER_H

#include "VkTexture.h"
#include "Resources.h"

#include <string>

namespace grail {
	class MemoryRegion;
	class NativeTextureContainer;
	class MemoryAllocator;
	
	class TextureLoader {
	public:
		static VkTexture* loadTexture(std::string path, TextureCreateInfo& createInfo, MemoryAllocator* stagingMemory = Resources::getStagingMemoryAllocator(), MemoryAllocator* dedicatedMemory = Resources::getTextureMemoryAllocator());
		static VkTexture* createTexture(std::string name, uint32_t width, uint32_t height, TextureCreateInfo& createInfo, MemoryAllocator* dedicatedMemory);
		static VkTexture* createTexture(std::string name, uint32_t width, uint32_t height, void* initialData, uint32_t size, TextureCreateInfo& createInfo, MemoryAllocator* dedicatedMemory, MemoryAllocator* stagingMemory);
	private:
		static void create(
			std::string& identifier,
			VkTexture* texture, 
			uint32_t width,
			uint32_t height,
			uint32_t depth,
			TextureCreateInfo& info,
			MemoryAllocator* stagingMemory,
			MemoryAllocator* dedicatedMemory,
			void* data = nullptr,
			uint32_t dataSize = 0, 
			NativeTextureContainer* nativeTex = nullptr
		);

		TextureLoader& operator = (const TextureLoader&) = delete;
		TextureLoader(const TextureLoader&) = delete;
		TextureLoader() = default;
	};
}

#endif