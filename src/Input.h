#ifndef GRAIL_INPUT_H
#define GRAIL_INPUT_H

#define GRAIL_KEY_RELEASE 0
#define GRAIL_KEY_PRESS 1 
#define GRAIL_KEY_REPEAT 2

#define GRAIL_MOUSE_PRESS GRAIL_KEY_PRESS
#define GRAIL_MOUSE_RELEASE GRAIL_KEY_RELEASE

namespace grail {
	class Input {
		friend class ApplicationWindow;
	public:
		static float getMouseX();
		static float getMouseY();

		static float getMouseTouchX();
		static float getMouseTouchY();

		static float getMouseScrollX();
		static float getMouseScrollY();

		static bool isKeyPressed(unsigned short int key);
		static bool isButtonPressed(unsigned short int button);

		static bool isKeyJustPressed(unsigned short int key);
		static bool isButtonJustPressed(unsigned short int button);

		static void captureMouse(bool capture);
		static bool isMouseCaptured();
	private:
		static class ApplicationWindow* window;

		static bool mouseCaptured;

		static float mouseX;
		static float mouseY;

		static float mouseTouchX;
		static float mouseTouchY;

		static float mouseScrollX;
		static float mouseScrollY;

		static bool keys[];
		static bool buttons[];

		static bool justPressedKeys[];
		static bool justPressedButtons[];

		Input& operator = (const Input&) = delete;
		Input(const Input&) = delete;
		Input() = default;
	};

#undef DELETE
	namespace Keys {
		namespace Mods {
			const unsigned short int SHIFT = 0x0001;
			const unsigned short int CONTROL = 0x0002;
			const unsigned short int ALT = 0x0004;
			const unsigned short int SUPER = 0x0008;
		}

		const unsigned short int UNKNOWN = -1;
		const unsigned short int SPACE = 32;
		const unsigned short int APOSTROPHE = 39;
		const unsigned short int COMMA = 44;
		const unsigned short int MINUS = 45;
		const unsigned short int PERIOD = 46;
		const unsigned short int SLASH = 47;
		const unsigned short int NUM_0 = 48;
		const unsigned short int NUM_1 = 49;
		const unsigned short int NUM_2 = 50;
		const unsigned short int NUM_3 = 51;
		const unsigned short int NUM_4 = 52;
		const unsigned short int NUM_5 = 53;
		const unsigned short int NUM_6 = 54;
		const unsigned short int NUM_7 = 55;
		const unsigned short int NUM_8 = 56;
		const unsigned short int NUM_9 = 57;
		const unsigned short int SEMICOLON = 59;
		const unsigned short int EQUAL = 61;
		const unsigned short int A = 65;
		const unsigned short int B = 66;
		const unsigned short int C = 67;
		const unsigned short int D = 68;
		const unsigned short int E = 69;
		const unsigned short int F = 70;
		const unsigned short int G = 71;
		const unsigned short int H = 72;
		const unsigned short int I = 73;
		const unsigned short int J = 74;
		const unsigned short int K = 75;
		const unsigned short int L = 76;
		const unsigned short int M = 77;
		const unsigned short int N = 78;
		const unsigned short int O = 79;
		const unsigned short int P = 80;
		const unsigned short int Q = 81;
		const unsigned short int R = 82;
		const unsigned short int S = 83;
		const unsigned short int T = 84;
		const unsigned short int U = 85;
		const unsigned short int V = 86;
		const unsigned short int W = 87;
		const unsigned short int X = 88;
		const unsigned short int Y = 89;
		const unsigned short int Z = 90;
		const unsigned short int LEFT_BRACKET = 91;
		const unsigned short int BACKSLASH = 92;
		const unsigned short int RIGHT_BRACKET = 93;
		const unsigned short int GRAVE_ACCENT = 96;
		const unsigned short int WORLD_1 = 161;
		const unsigned short int WORLD_2 = 162;
		const unsigned short int ESCAPE = 256;
		const unsigned short int ENTER = 257;
		const unsigned short int TAB = 258;
		const unsigned short int BACKSPACE = 259;
		const unsigned short int INSERT = 260;
		const unsigned short int DELETE = 261;
		const unsigned short int RIGHT = 262;
		const unsigned short int LEFT = 263;
		const unsigned short int DOWN = 264;
		const unsigned short int UP = 265;
		const unsigned short int PAGE_UP = 266;
		const unsigned short int PAGE_DOWN = 267;
		const unsigned short int HOME = 268;
		const unsigned short int END = 269;
		const unsigned short int CAPS_LOCK = 280;
		const unsigned short int SCROLL_LOCK = 281;
		const unsigned short int NUM_LOCK = 282;
		const unsigned short int PRINT_SCREEN = 283;
		const unsigned short int PAUSE = 284;
		const unsigned short int F1 = 290;
		const unsigned short int F2 = 291;
		const unsigned short int F3 = 292;
		const unsigned short int F4 = 293;
		const unsigned short int F5 = 294;
		const unsigned short int F6 = 295;
		const unsigned short int F7 = 296;
		const unsigned short int F8 = 297;
		const unsigned short int F9 = 298;
		const unsigned short int F10 = 299;
		const unsigned short int F11 = 300;
		const unsigned short int F12 = 301;
		const unsigned short int F13 = 302;
		const unsigned short int F14 = 303;
		const unsigned short int F15 = 304;
		const unsigned short int F16 = 305;
		const unsigned short int F17 = 306;
		const unsigned short int F18 = 307;
		const unsigned short int F19 = 308;
		const unsigned short int F20 = 309;
		const unsigned short int F21 = 310;
		const unsigned short int F22 = 311;
		const unsigned short int F23 = 312;
		const unsigned short int F24 = 313;
		const unsigned short int F25 = 314;
		const unsigned short int KEYPAD_0 = 320;
		const unsigned short int KEYPAD_1 = 321;
		const unsigned short int KEYPAD_2 = 322;
		const unsigned short int KEYPAD_3 = 323;
		const unsigned short int KEYPAD_4 = 324;
		const unsigned short int KEYPAD_5 = 325;
		const unsigned short int KEYPAD_6 = 326;
		const unsigned short int KEYPAD_7 = 327;
		const unsigned short int KEYPAD_8 = 328;
		const unsigned short int KEYPAD_9 = 329;
		const unsigned short int KEYPAD_DECIMAL = 330;
		const unsigned short int KEYPAD_DIVIDE = 331;
		const unsigned short int KEYPAD_MULTIPLY = 322;
		const unsigned short int KEYPAD_SUBTRACT = 333;
		const unsigned short int KEYPAD_ADD = 334;
		const unsigned short int KEYPAD_ENTER = 335;
		const unsigned short int KEYPAD_EQUAL = 336;
		const unsigned short int LEFT_SHIFT = 340;
		const unsigned short int LEFT_CONTROL = 341;
		const unsigned short int LEFT_ALT = 342;
		const unsigned short int LEFT_SUPER = 343;
		const unsigned short int RIGHT_SHIFT = 344;
		const unsigned short int RIGHT_CONTROL = 345;
		const unsigned short int RIGHT_ALT = 346;
		const unsigned short int RIGHT_SUPER = 347;
		const unsigned short int MENU = 348;

		const unsigned short int LAST = MENU;
#define DELETE (0x00010000L)
	}

	namespace Buttons {
		const unsigned short int BUTTON_1 = 0;
		const unsigned short int BUTTON_2 = 1;
		const unsigned short int BUTTON_3 = 2;
		const unsigned short int BUTTON_4 = 3;
		const unsigned short int BUTTON_5 = 4;
		const unsigned short int BUTTON_6 = 5;
		const unsigned short int BUTTON_7 = 6;
		const unsigned short int BUTTON_8 = 7;
		const unsigned short int LEFT = BUTTON_1;
		const unsigned short int RIGHT = BUTTON_2;
		const unsigned short int MIDDLE = BUTTON_3;

		const unsigned short int LAST = BUTTON_8;
	}
}

#endif