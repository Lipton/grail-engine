#ifndef GRAIL_H
#define GRAIL_H

#include "Common.h"
#include "Screen.h"

namespace grail {
	struct EngineConfiguration {
		// desktop platforms only
		const char* windowTitle = "Untitled";
		bool fullscreen = false;
		bool resizableWindow = false;
		int windowWidth = 1024;
		int windowHeight = 764;

		int framesInFlight = 3;

		// should vulkan validation layers be enabled
		bool enableValidationLayers = false;

		// validation layer flags
		VkDebugReportFlagsEXT validationLayerFlags;
		
		// requested physical device features
		VkPhysicalDeviceFeatures requestedFeatures = {};

		bool vsync = true;

		std::vector<std::string> requestedInstanceExtensions;
		std::vector<std::string> requestedDeviceExtensions;


		EngineConfiguration() {
			validationLayerFlags =
				//VK_DEBUG_REPORT_DEBUG_BIT_EXT |
				VK_DEBUG_REPORT_ERROR_BIT_EXT |
				//VK_DEBUG_REPORT_INFORMATION_BIT_EXT |
				VK_DEBUG_REPORT_WARNING_BIT_EXT |
				VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT;
		}
	};

	struct EngineInitializationInfo {
		EngineConfiguration* engineConfiguration = nullptr;

		// entry screen register name
		std::string screenName = "main";
		// entry screen pointer
		grail::Screen* screen = nullptr;

#if defined(OS_ANDROID)
		struct android_app* android_app;
#endif
	};


	class Grail {
	public:
		Grail(EngineInitializationInfo& initInfo);
	private:
		void startEngine();
		void exit();

		static bool running;

		EngineInitializationInfo initInfo;

		class ApplicationWindow* window;
		class VulkanContext* vulkanContext;
		class ScreenManager* screenManager;
	};
}

#if defined(OS_ANDROID)
#define GRAIL_MAIN_FUNCTION void android_main(struct android_app* android_app)

#define GRAIL_INITIALIZE_ENGINE(initializationStruct)							\
	app_dummy();																\
																				\
	initializationStruct.android_app = android_app;								\
	grail::Grail engine = grail::Grail(initializationStruct)					\

#else
#define GRAIL_MAIN_FUNCTION int main()

#define GRAIL_INITIALIZE_ENGINE(initializationStruct)							\
	grail::Grail engine = grail::Grail(initializationStruct);					\
	(void)(engine);																\
	return 0																										
#endif

#endif