#ifndef GRAIL_SCENE_NODE_H
#define GRAIL_SCENE_NODE_H

#include <vector>

//#include "Scene.h"

#include "GLMIncludes.h"
#include "SceneNodeComponents.h"
#include <entt.hpp>

namespace grail {
	class Transform {
		friend class SceneNode;
		friend class SceneLoader;
		friend class Scene;
		friend class Physics;
	public:
		glm::mat4 getLocalMatrix() { return localMatrix; }
		glm::mat4 getWorldMatrix() { return worldMatrix; }

		glm::mat4 getLastFrameLocalMatrix() { return lastFrameLocalMatrix; }
		glm::mat4 getLastFrameWorldMatrix() { return lastFrameWorldMatrix; }

		glm::vec3 getWorldBoundsMin();
		glm::vec3 getWorldBoundsMax();

		glm::vec3 getLocalBoundsMin();
		glm::vec3 getLocalBoundsMax();

		void setLocalPosition(glm::vec3 position);
		void setLocalPosition(float x, float y, float z);
		void setLocalPositionX(float x);
		void setLocalPositionY(float y);
		void setLocalPositionZ(float z);

		void translateLocal(glm::vec3 translation);
		void translateLocal(float x, float y, float z);
		void translateLocalX(float x);
		void translateLocalY(float y);
		void translateLocalZ(float z);

		glm::vec3 getLocalPosition();

		void setWorldPosition(glm::vec3 position);
		void setWorldPosition(float x, float y, float z);
		void setWorldPositionX(float x);
		void setWorldPositionY(float y);
		void setWorldPositionZ(float z);

		void translateWorld(glm::vec3 translation);
		void translateWorld(float x, float y, float z);
		void translateWorldX(float x);
		void translateWorldY(float y);
		void translateWorldZ(float z);
		glm::vec3 getWorldPosition();

		void setLocalEuler(glm::vec3 angles);
		void setLocalEuler(float x, float y, float z);
		void setLocalEulerX(float x);
		void setLocalEulerY(float y);
		void setLocalEulerZ(float z);

		void rotateLocalEuler(glm::vec3 rotation);
		void rotateLocalEuler(float x, float y, float z);
		void rotateLocalEulerX(float x);
		void rotateLocalEulerY(float y);
		void rotateLocalEulerZ(float z);
		glm::vec3 getLocalEuler();

		void setLocalRotation(glm::quat rotation);
		void rotateLocal(glm::quat rotation);
		glm::quat getLocalRotation();

		void setWorldEuler(glm::vec3 angles);
		void setWorldEuler(float x, float y, float z);
		void setWorldEulerX(float x);
		void setWorldEulerY(float y);
		void setWorldEulerZ(float z);

		void rotateWorldEuler(glm::vec3 rotation);
		void rotateWorldEuler(float x, float y, float z);
		void rotateWorldEulerX(float x);
		void rotateWorldEulerY(float y);
		void rotateWorldEulerZ(float z);
		glm::vec3 getWorldEuler();

		void setWorldRotation(glm::quat rotation);
		void rotateWorld(glm::quat& rotation);
		glm::quat getWorldRotation();

		void setLocalScale(glm::vec3 scale);
		void setLocalScale(float x, float y, float z);
		void setLocalScaleX(float x);
		void setLocalScaleY(float y);
		void setLocalScaleZ(float z);

		void scaleLocal(glm::vec3 scale);
		void scaleLocal(float x, float y, float z);
		void scaleLocalX(float x);
		void scaleLocalY(float y);
		void scaleLocalZ(float z);
		glm::vec3 getLocalScale();

		void setWorldScale(glm::vec3 scale);
		void setWorldScale(float x, float y, float z);
		void setWorldScaleX(float x);
		void setWorldScaleY(float y);
		void setWorldScaleZ(float z);

		void scaleWorld(glm::vec3 scale);
		void scaleWorld(float x, float y, float z);
		void scaleWorldX(float x);
		void scaleWorldY(float y);
		void scaleWorldZ(float z);
		glm::vec3 getWorldScale();

		// Local space into world space
		glm::mat4 getLocalToWorldMatrix();

		// World space into local space
		glm::mat4 getWorldToLocalMatrix();

		glm::vec3 getRight();
		glm::vec3 getUp();
		glm::vec3 getForward();

		void copyTo(Transform& dst);
	private:
		bool dirty = true;

		glm::mat4 localMatrix;
		glm::mat4 lastFrameLocalMatrix;

		glm::mat4 worldMatrix;
		glm::mat4 inverseWorldMatrix;
		glm::mat4 inverseParentWorld = glm::mat4(1.0);
		glm::mat4 lastFrameWorldMatrix;

		glm::vec3 right = glm::vec3(1, 0, 0);
		glm::vec3 up = glm::vec3(0, 1, 0);
		glm::vec3 forward = glm::vec3(0, 0, 1);

		glm::vec3 localPosition = glm::vec3();
		glm::vec3 worldPosition = glm::vec3();

		glm::quat localRotation = glm::quat(1.0f, 0.0f, 0.0f, 0.0f);
		glm::vec3 localEuler = glm::vec3();

		glm::quat worldRotation = glm::quat(1.0f, 0.0f, 0.0f, 0.0f);
		glm::vec3 worldEuler = glm::vec3();

		glm::quat inverseParentRotation = glm::quat(1.0f, 0.0f, 0.0f, 0.0f);

		glm::vec3 localScale = glm::vec3(1.0f, 1.0f, 1.0f);
		glm::vec3 worldScale = glm::vec3(1.0f, 1.0f, 1.0f);

		glm::vec3 boundsMin = glm::vec3(-0.5f, -0.5f, -0.5f);
		glm::vec3 boundsMax = glm::vec3(0.5f, 0.5f, 0.5f);

		glm::vec3 boundsMinLocal = glm::vec3(-0.5f, -0.5f, -0.5f);
		glm::vec3 boundsMaxLocal = glm::vec3(0.5f, 0.5f, 0.5f);

		Transform* parent = nullptr;
	};

	class Mesh;

	class SceneNode {
		friend class Renderer;
		friend class SceneLoader;
		friend class MeshLoader;
		friend class Scene;
		friend class Physics;
	public:
		std::string name;

		SceneNode();
		SceneNode(const std::string& name);
		SceneNode(SceneNode* copy);
		~SceneNode();

		uint32_t getID();

		void addChild(SceneNode* node, bool preservePosition = false);
		void removeChild(uint32_t id);
		void removeChild(SceneNode* child);

		std::vector<SceneNode*> getChildren();

		uint32_t getChildCount();
		SceneNode* getChild(uint32_t id);
		SceneNode* getChildByIndex(uint32_t index);
		SceneNode* getChildByName(const std::string& name);
		SceneNode* getParent();

		void clearChildren();

		void gatherChildren(std::vector<SceneNode*>& list);
		void gatherChildren(SceneNode* base, std::vector<SceneNode*> list);

		void updateTransform(bool forceUpdate = false);

		bool descendantOf(SceneNode* node);

		/*template<typename Component, typename ...Args>
		inline void addComponent(Args&& ...args) {
			Scene::assignComponent<Component>(this, args);
		}

		template<typename Component>
		inline auto* getComponent() {
			return Scene::getComponent<Component>(this);
		}*/

		void createComponentFromMap(const std::string& componentName);
		nodeComponents::NodeComponent* getComponentFromMap(const std::string& componentName);
	
		Transform transform;

		void setAsPrefab(bool prefab);
		bool isPrefab();
		SceneNode* getDerivedPrefab();

		std::vector<std::string> getPrefabClones();

		void syncPrefabClones();

		SceneNode* clone();
	private:
		std::vector<SceneNode*> children;
		std::vector<std::string> prefabClones;
		SceneNode* parent = nullptr;

		SceneNode* derivedPrefab = nullptr;

		uint32_t ID;

		static uint32_t IDCounter;

		entt::entity entity;

		bool prefab = false;
	public:
	};
}

#endif
