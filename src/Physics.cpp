#include "Physics.h"
#include "Mesh.h"
#include "SceneNode.h"
#include "VkBuffer.h"
#include "Material.h"
#include "VertexLayout.h"
#include "MaterialInstance.h"
#include "Resources.h"
#include "Camera.h"

/*btDefaultCollisionConfiguration* grail::Physics::collisionConfiguration = nullptr;
btCollisionDispatcher* grail::Physics::collisionDispatcher = nullptr;
btBroadphaseInterface* grail::Physics::overlappingPairCache = nullptr;
btSequentialImpulseConstraintSolver* grail::Physics::impulseSolver = nullptr;
btDiscreteDynamicsWorld* grail::Physics::dynamicsWorld = nullptr;
btAlignedObjectArray<btCollisionShape*> grail::Physics::collisionShapes = btAlignedObjectArray<btCollisionShape*>();*/
grail::PhysicsDebugRenderer* grail::Physics::debugRenderer = nullptr;

void grail::Physics::initialize() {
	/*collisionConfiguration = new btDefaultCollisionConfiguration();
	collisionDispatcher = new btCollisionDispatcher(collisionConfiguration);
	overlappingPairCache = new btDbvtBroadphase();
	impulseSolver = new btSequentialImpulseConstraintSolver();

	dynamicsWorld = new btDiscreteDynamicsWorld(collisionDispatcher, overlappingPairCache, impulseSolver, collisionConfiguration);
	dynamicsWorld->setGravity(btVector3(0, -9.8f, 0));*/
}

void grail::Physics::initializeDebug(VkRenderPass renderPass) {
	debugRenderer = new PhysicsDebugRenderer(renderPass);
	//dynamicsWorld->setDebugDrawer(debugRenderer);
}

/*void grail::Physics::createShapeForMesh(SceneNode* node, Mesh* mesh, btCollisionShape** shape, btRigidBody** body) {
	glm::vec3 size = glm::abs(mesh->boundsMin - mesh->boundsMax) * 0.5f;

	glm::vec3 position = node->transform.getWorldPosition();
	glm::quat rotation = node->transform.getWorldRotation();
	glm::vec3 scale = node->transform.getWorldScale();

	*shape = new btBoxShape(btVector3(size.x, size.y, size.z));
	(*shape)->setLocalScaling(btVector3(scale.x, scale.y, scale.z));
	collisionShapes.push_back(*shape);

	btTransform transform;
	transform.setOrigin(btVector3(position.x, position.y, position.z));
	transform.setRotation(btQuaternion(
		(rotation.x),
		(rotation.y),
		(rotation.z),
		(rotation.w)));
	//transform.setFromOpenGLMatrix(glm::value_ptr(node->transform.getWorldMatrix()));

	btScalar mass = 0.0;
	btVector3 localInertia = btVector3(0.0, 0.0, 0.0);
	if (mass != 0.0) {
		(*shape)->calculateLocalInertia(mass, localInertia);
	}

	btDefaultMotionState* motionState = new btDefaultMotionState(transform);
	btRigidBody::btRigidBodyConstructionInfo constructionInfo(mass, motionState, *shape, localInertia);

	*body = new btRigidBody(constructionInfo);
	(*body)->setUserPointer(node);

	dynamicsWorld->addRigidBody(*body);
}*/

void grail::Physics::simulate() {
	/*dynamicsWorld->stepSimulation(1.0f / 60.0f, 5);

	for (int i = dynamicsWorld->getNumCollisionObjects() - 1; i >= 0; i--) {
		btCollisionObject* obj = dynamicsWorld->getCollisionObjectArray()[i];
		btRigidBody* body = btRigidBody::upcast(obj);
		btTransform transform;

		if (body && body->getMotionState()) {
			body->getMotionState()->getWorldTransform(transform);
		}
		else {
			transform = obj->getWorldTransform();
		}

		if (body) {
			SceneNode* node = reinterpret_cast<SceneNode*>(body->getUserPointer());

			glm::vec3 pos = glm::vec3(
				transform.getOrigin().getX(),
				transform.getOrigin().getY(),
				transform.getOrigin().getZ()
			);

			node->transform.setWorldRotation(glm::quat(
				(transform.getRotation().getW()),
				(transform.getRotation().getX()),
				(transform.getRotation().getY()),
				(transform.getRotation().getZ())
			));
			node->transform.setWorldPosition(pos);
		}

		
		//GRAIL_LOG(INFO, "POSITION") << transform.getOrigin().getX() << " " << transform.getOrigin().getY() << " " << transform.getOrigin().getZ();
		
	}*/
}

void grail::Physics::drawDebug(Camera* camera, VkCommandBuffer cmdBuffer) {	
	/*if (debugRenderer) {
		debugRenderer->begin(camera, cmdBuffer);
		dynamicsWorld->debugDrawWorld();
		debugRenderer->end(camera, cmdBuffer);
	}*/
}
/*
void grail::Physics::removeBody(btRigidBody* body) {
	dynamicsWorld->removeRigidBody(body);
}
*/
grail::PhysicsDebugRenderer::PhysicsDebugRenderer(VkRenderPass renderPass) {
	DescriptorLayout descriptorLayout;
	descriptorLayout.addPushConstant({ ShaderStageBits::VERTEX, 0, sizeof(glm::mat4) });
	descriptorLayout.finalize();

	bufferVertexCount = 10000;

	BufferCreateInfo bufferInfo = {};
	bufferInfo.memoryType = MemoryType::DYNAMIC;
	bufferInfo.memoryProperties |= MemoryPropertyFlags::HOST_COHERENT;
	bufferInfo.usageBits = BufferUsageBits::VERTEX_BUFFER_BIT;
	bufferInfo.size = sizeof(float) * 6 * bufferVertexCount;
	vertexBuffer = new Buffer(bufferInfo, Resources::getBufferMemoryAllocator());

	vertexLayout = new VertexLayout({
		VertexAttribute(VertexAttributeType::POSITION, VK_FORMAT_R32G32B32_SFLOAT, 3, sizeof(float)),
		VertexAttribute(VertexAttributeType::COLOR, VK_FORMAT_R32G32B32_SFLOAT, 3, sizeof(float))
	});

	PushConstantRange pushConstantRange = {};
	pushConstantRange.stageFlags = ShaderStageBits::VERTEX;
	pushConstantRange.size = sizeof(glm::mat4);
	pushConstantRange.offset = 0;

	GraphicsPipelineCreateInfo pipelineInfo = {};
	pipelineInfo.colorBlendState.attachmentCount = 1;
	pipelineInfo.renderPass = renderPass;

	Shader* vS = Resources::loadSPIRVShader("shaders/glsl/physicsDebug.vert");
	Shader* fS = Resources::loadSPIRVShader("shaders/glsl/physicsDebug.frag");

	pipelineInfo.rasterizationState.cullMode = CullMode::NONE;
	pipelineInfo.rasterizationState.polygonMode = PolygonMode::LINE;
	pipelineInfo.assemblyState.topology = PrimitiveTopology::LINE_LIST;
	pipelineInfo.rasterizationState.lineWidth = 2.0f;

	pipelineInfo.colorBlendState.blendEnable = VK_FALSE;
	pipelineInfo.colorBlendState.srcColorBlendFactor = BlendFactor::SRC_ALPHA;
	pipelineInfo.colorBlendState.dstColorBlendFactor = BlendFactor::ONE_MINUS_SRC_ALPHA;
	pipelineInfo.colorBlendState.srcAlphaBlendFactor = BlendFactor::ONE_MINUS_SRC_ALPHA;
	pipelineInfo.colorBlendState.dstAlphaBlendFactor = BlendFactor::ZERO;

	material = new MaterialDescription("Physics Debug Material");
	Material* base = material->createMaterial(*vertexLayout, descriptorLayout, { vS->getBasePermutation(), fS->getBasePermutation() }, pipelineInfo);
	materialInstace = base->createInstance();
	materialInstace->updateAllDescriptors();
}

/*void grail::PhysicsDebugRenderer::drawLine(const btVector3& from, const btVector3& to, const btVector3& color) {
	if ((vtxCount + 12) > bufferVertexCount) return;

	*vtxDst = from.getX(); vtxDst++;
	*vtxDst = from.getY(); vtxDst++;
	*vtxDst = from.getZ(); vtxDst++;
	*vtxDst = color.getX(); vtxDst++;
	*vtxDst = color.getY(); vtxDst++;
	*vtxDst = color.getZ(); vtxDst++;

	*vtxDst = to.getX(); vtxDst++;
	*vtxDst = to.getY(); vtxDst++;
	*vtxDst = to.getZ(); vtxDst++;
	*vtxDst = color.getX(); vtxDst++;
	*vtxDst = color.getY(); vtxDst++;
	*vtxDst = color.getZ(); vtxDst++;

	vtxCount += 12;
}*/

void grail::PhysicsDebugRenderer::drawLine(glm::mat4 model, glm::vec3 from, glm::vec3 to, glm::vec3 color) {
	if ((vtxCount + 12) > bufferVertexCount) return;

	glm::vec3 f = glm::vec3(model * glm::vec4(from, 1.0f));
	glm::vec3 t = glm::vec3(model * glm::vec4(to, 1.0f));

	*vtxDst = f.x; vtxDst++;
	*vtxDst = f.y; vtxDst++;
	*vtxDst = f.z; vtxDst++;
	*vtxDst = color.x; vtxDst++;
	*vtxDst = color.y; vtxDst++;
	*vtxDst = color.z; vtxDst++;

	*vtxDst = t.x; vtxDst++;
	*vtxDst = t.y; vtxDst++;
	*vtxDst = t.z; vtxDst++;
	*vtxDst = color.x; vtxDst++;
	*vtxDst = color.y; vtxDst++;
	*vtxDst = color.z; vtxDst++;

	vtxCount += 2;
}
void grail::PhysicsDebugRenderer::drawBox(glm::mat4 model, glm::vec3 min, glm::vec3 max, glm::vec3 color) {

	glm::vec3 size = glm::abs(min - max);

	drawLine(model, min, min + glm::vec3(size.x, 0, 0), color);
	drawLine(model, min + glm::vec3(size.x, 0, 0), min + glm::vec3(size.x, 0, size.z), color);
	drawLine(model, min + glm::vec3(size.x, 0, size.z), min + glm::vec3(0, 0, size.z), color);
	drawLine(model, min + glm::vec3(0, 0, size.z), min + glm::vec3(0, 0, 0), color);
	drawLine(model, min + glm::vec3(0, 0, 0), min + glm::vec3(0, size.y, 0), color);
	drawLine(model, min + glm::vec3(0, size.y, 0), min + glm::vec3(size.x, size.y, 0), color);
	drawLine(model, min + glm::vec3(size.x, size.y, 0), min + glm::vec3(size.x, size.y, size.z), color);
	drawLine(model, min + glm::vec3(size.x, size.y, size.z), min + glm::vec3(0, size.y, size.z), color);
	drawLine(model, min + glm::vec3(0, size.y, size.z), min + glm::vec3(0, size.y, 0), color);
	drawLine(model, min + glm::vec3(size.x, 0, 0), min + glm::vec3(size.x, size.y, 0), color);
	drawLine(model, min + glm::vec3(size.x, 0, size.z), min + glm::vec3(size.x, size.y, size.z), color);
	drawLine(model, min + glm::vec3(0, 0, size.z), min + glm::vec3(0, size.y, size.z), color);
}
/*
void grail::PhysicsDebugRenderer::drawContactPoint(const btVector3& PointOnB, const btVector3& normalOnB, btScalar distance, int lifeTime, const btVector3& color) {

}

void grail::PhysicsDebugRenderer::reportErrorWarning(const char* warningString) {

}

void grail::PhysicsDebugRenderer::draw3dText(const btVector3& location, const char* textString) {

}

void grail::PhysicsDebugRenderer::setDebugMode(int debugMode) {

}

int grail::PhysicsDebugRenderer::getDebugMode() const {
	return 1;
}*/

void grail::PhysicsDebugRenderer::begin(Camera* camera, VkCommandBuffer& cmd) {
	vertexBuffer->map();
	vtxDst = (float*)(static_cast<char*>(vertexBuffer->getMappedPtr()) + vertexBuffer->getOffset());

	cam = camera;
	cmdBuff = cmd;
}

void grail::PhysicsDebugRenderer::end(Camera* camera, VkCommandBuffer& cmd) {
	vertexBuffer->unmap();

	if (vtxCount == 0) return;

	glm::mat4 push = camera->combined;
	vkCmdPushConstants(cmd,
		materialInstace->getCurrentFrameDescriptor().getPipelineLayout(),
		static_cast<VkShaderStageFlagBits>(materialInstace->getCurrentFrameDescriptor().getPushConstant(0).stageFlags),
		materialInstace->getCurrentFrameDescriptor().getPushConstant(0).offset,
		materialInstace->getCurrentFrameDescriptor().getPushConstant(0).size, &push);

	vkCmdBindPipeline(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, materialInstace->getPipeline());

	VkDeviceSize offsets[1] = { vertexBuffer->getOffset() };
	vkCmdBindVertexBuffers(cmd, 0, 1, &vertexBuffer->handle, offsets);

	/*DescriptorGroup* group = &materialInstace->getCurrentFrameDescriptor();
	vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, group->getPipelineLayout(), 0, group->getDescriptorCount(), group->getHandlePtr(), 0, nullptr);
	*/

	VkRect2D scissorRect;
	scissorRect.offset.x = 0;
	scissorRect.offset.y = 0;
	scissorRect.extent.width = Graphics::getWidth();
	scissorRect.extent.height = Graphics::getHeight();

	vkCmdDraw(cmd, vtxCount, 1, 0, 1);

	vtxCount = 0;
}
