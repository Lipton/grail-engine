#ifndef GRAIL_VK_STRUCTS_H
#define GRAIL_VK_STRUCTS_H

#include <vulkan.h>
#include <string>
#include <vector>

namespace grail {
	enum class SamplerFilter {
		NEAREST = VK_FILTER_NEAREST,
		LINEAR = VK_FILTER_LINEAR
	};

	enum class SamplerMipmapMode {
		NEAREST = VK_SAMPLER_MIPMAP_MODE_NEAREST,
		LINEAR = VK_SAMPLER_MIPMAP_MODE_LINEAR
	};

	enum class SamplerAddressMode {
		REPEAT = VK_SAMPLER_ADDRESS_MODE_REPEAT,
		MIRRORED_REPEAT = VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT,
		CLAMP_TO_EDGE = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
		CLAMP_TO_BORDER = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER,
		MIRROR_CLAMP_TO_EDGE = VK_SAMPLER_ADDRESS_MODE_MIRROR_CLAMP_TO_EDGE
	};

	enum class SamplerBorderColor {
		FLOAT_TRANSPARENT_BLACK = VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK,
		INT_TRANSPARENT_BLACK = VK_BORDER_COLOR_INT_TRANSPARENT_BLACK,
		FLOAT_OPAQUE_BLACK = VK_BORDER_COLOR_FLOAT_OPAQUE_BLACK,
		INT_OPAQUE_BLACK = VK_BORDER_COLOR_INT_OPAQUE_BLACK,
		FLOAT_OPAQUE_WHITE = VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE,
		INT_OPAQUE_WHITE = VK_BORDER_COLOR_INT_OPAQUE_WHITE
	};

	enum class ImageType {
		TYPE_1D = VK_IMAGE_TYPE_1D,
		TYPE_2D = VK_IMAGE_TYPE_2D,
		TYPE_3D = VK_IMAGE_TYPE_3D
	};

	enum class ImageTiling {
		OPTIMAL = VK_IMAGE_TILING_OPTIMAL,
		LINEAR = VK_IMAGE_TILING_LINEAR
	};

	enum class ImageUsageBits : uint32_t {
		TRANSFER_SRC_BIT = VK_IMAGE_USAGE_TRANSFER_SRC_BIT,
		TRANSFER_DST_BIT = VK_IMAGE_USAGE_TRANSFER_DST_BIT,
		SAMPLED_BIT = VK_IMAGE_USAGE_SAMPLED_BIT,
		STORAGE_BIT = VK_IMAGE_USAGE_STORAGE_BIT,
		COLOR_ATTACHMENT_BIT = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
		DEPTH_STENCIL_ATTACHMENT_BIT = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
		TRANSIENT_ATTACHMENT_BIT = VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT,
		INPUT_ATTACHMENT_BIT = VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT
	};

	inline ImageUsageBits operator~ (ImageUsageBits a) { return (ImageUsageBits)~(int)a; }
	inline ImageUsageBits operator| (ImageUsageBits a, ImageUsageBits b) { return (ImageUsageBits)((int)a | (int)b); }
	inline ImageUsageBits operator& (ImageUsageBits a, ImageUsageBits b) { return (ImageUsageBits)((int)a & (int)b); }
	inline ImageUsageBits operator^ (ImageUsageBits a, ImageUsageBits b) { return (ImageUsageBits)((int)a ^ (int)b); }
	inline ImageUsageBits& operator|= (ImageUsageBits& a, ImageUsageBits b) { return (ImageUsageBits&)((int&)a |= (int)b); }
	inline ImageUsageBits& operator&= (ImageUsageBits& a, ImageUsageBits b) { return (ImageUsageBits&)((int&)a &= (int)b); }
	inline ImageUsageBits& operator^= (ImageUsageBits& a, ImageUsageBits b) { return (ImageUsageBits&)((int&)a ^= (int)b); }

	enum class SharingMode {
		EXCLUSIVE = VK_SHARING_MODE_EXCLUSIVE,
		CONCURRENT = VK_SHARING_MODE_CONCURRENT
	};

	enum class ImageLayout {
		UNDEFINED = VK_IMAGE_LAYOUT_UNDEFINED,
		GENERAL = VK_IMAGE_LAYOUT_GENERAL,
		COLOR_ATTACHMENT_OPTIMAL = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
		DEPTH_STENCIL_ATTACHMENT_OPTIMAL = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
		DEPTH_STENCIL_READ_ONLY_OPTIMAL = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL,
		SHADER_READ_ONLY_OPTIMAL = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
		TRANSFER_SRC_OPTIMAL = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
		TRANSFER_DST_OPTIMAL = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
		PREINITIALIZED = VK_IMAGE_LAYOUT_PREINITIALIZED,
		PRESENT_SRC = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
	};

	enum class AttachmentLoadOp {
		LOAD = VK_ATTACHMENT_LOAD_OP_LOAD,
		CLEAR = VK_ATTACHMENT_LOAD_OP_CLEAR,
		DONT_CARE = VK_ATTACHMENT_LOAD_OP_DONT_CARE
	};

	enum class AttachmentStoreOp {
		STORE = VK_ATTACHMENT_STORE_OP_STORE,
		DONT_CARE = VK_ATTACHMENT_STORE_OP_DONT_CARE
	};

	enum class PrimitiveTopology {
		POINT_LIST = VK_PRIMITIVE_TOPOLOGY_POINT_LIST,
		LINE_LIST = VK_PRIMITIVE_TOPOLOGY_LINE_LIST,
		LINE_STRIP = VK_PRIMITIVE_TOPOLOGY_LINE_STRIP,
		TRIANGLE_LIST = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
		TRIANGLE_STRIP = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP,
		TRIANGLE_FAN = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN,
		LINE_LIST_WITH_ADJACENCY = VK_PRIMITIVE_TOPOLOGY_LINE_LIST_WITH_ADJACENCY,
		LINE_STRIP_WITH_ADJACENCY = VK_PRIMITIVE_TOPOLOGY_LINE_STRIP_WITH_ADJACENCY,
		TRIANGLE_LIST_WITH_ADJACENCY = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST_WITH_ADJACENCY,
		TRIANGLE_STRIP_WITH_ADJACENCY = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP_WITH_ADJACENCY,
		PATCH_LIST = VK_PRIMITIVE_TOPOLOGY_PATCH_LIST
	};

	enum class PolygonMode {
		FILL = VK_POLYGON_MODE_FILL,
		LINE = VK_POLYGON_MODE_LINE,
		POINT = VK_POLYGON_MODE_POINT
	};

	enum class CullMode {
		NONE = VK_CULL_MODE_NONE,
		FRONT = VK_CULL_MODE_FRONT_BIT,
		BACK = VK_CULL_MODE_BACK_BIT,
		FRONT_AND_BACK = VK_CULL_MODE_FRONT_AND_BACK
	};

	enum class FrontFace {
		COUNTER_CLOCKWISE = VK_FRONT_FACE_COUNTER_CLOCKWISE,
		CLOCKWISE = VK_FRONT_FACE_CLOCKWISE
	};

	enum class BlendFactor {
		ZERO = VK_BLEND_FACTOR_ZERO,
		ONE = VK_BLEND_FACTOR_ONE,
		SRC_COLOR = VK_BLEND_FACTOR_SRC_COLOR,
		ONE_MINUS_SRC_COLOR = VK_BLEND_FACTOR_ONE_MINUS_SRC_COLOR,
		DST_COLOR = VK_BLEND_FACTOR_DST_COLOR,
		ONE_MINUS_DST_COLOR = VK_BLEND_FACTOR_ONE_MINUS_DST_COLOR,
		SRC_ALPHA = VK_BLEND_FACTOR_SRC_ALPHA,
		ONE_MINUS_SRC_ALPHA = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,
		DST_ALPHA = VK_BLEND_FACTOR_DST_ALPHA,
		CONSTANT_COLOR = VK_BLEND_FACTOR_CONSTANT_COLOR,
		ONE_MINUS_CONSTANT_COLOR = VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_COLOR,
		CONSTANT_ALPHA = VK_BLEND_FACTOR_CONSTANT_ALPHA,
		ONE_MINUS_CONSTANT_ALPHA = VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_ALPHA,
		SRC_ALPHA_SATURATE = VK_BLEND_FACTOR_SRC_ALPHA_SATURATE,
		SRC1_COLOR = VK_BLEND_FACTOR_SRC1_COLOR,
		ONE_MINUS_SRC1_COLOR = VK_BLEND_FACTOR_ONE_MINUS_SRC1_COLOR,
		SRC1_ALPHA = VK_BLEND_FACTOR_SRC1_ALPHA,
		ONE_MINUS_SRC1_ALPHA = VK_BLEND_FACTOR_ONE_MINUS_SRC1_ALPHA
	};

	enum class BlendOp {
		ADD = VK_BLEND_OP_ADD,
		SUBTRACT = VK_BLEND_OP_SUBTRACT,
		REVERSE_SUBTRACT = VK_BLEND_OP_REVERSE_SUBTRACT,
		MIN = VK_BLEND_OP_MIN,
		MAX = VK_BLEND_OP_MAX
	};

	enum class ColorComponentFlags {
		R = VK_COLOR_COMPONENT_R_BIT,
		G = VK_COLOR_COMPONENT_G_BIT,
		B = VK_COLOR_COMPONENT_B_BIT,
		A = VK_COLOR_COMPONENT_A_BIT,

		RGB = R | G | B,
		RGBA = R | G | B | A
	};

	inline ColorComponentFlags operator~ (ColorComponentFlags a) { return (ColorComponentFlags)~(int)a; }
	inline ColorComponentFlags operator| (ColorComponentFlags a, ColorComponentFlags b) { return (ColorComponentFlags)((int)a | (int)b); }
	inline ColorComponentFlags operator& (ColorComponentFlags a, ColorComponentFlags b) { return (ColorComponentFlags)((int)a & (int)b); }
	inline ColorComponentFlags operator^ (ColorComponentFlags a, ColorComponentFlags b) { return (ColorComponentFlags)((int)a ^ (int)b); }
	inline ColorComponentFlags& operator|= (ColorComponentFlags& a, ColorComponentFlags b) { return (ColorComponentFlags&)((int&)a |= (int)b); }
	inline ColorComponentFlags& operator&= (ColorComponentFlags& a, ColorComponentFlags b) { return (ColorComponentFlags&)((int&)a &= (int)b); }
	inline ColorComponentFlags& operator^= (ColorComponentFlags& a, ColorComponentFlags b) { return (ColorComponentFlags&)((int&)a ^= (int)b); }

	enum class CompareOp {
		NEVER = VK_COMPARE_OP_NEVER,
		LESS = VK_COMPARE_OP_LESS,
		EQUAL = VK_COMPARE_OP_EQUAL,
		LESS_OR_EQUAL = VK_COMPARE_OP_LESS_OR_EQUAL,
		GREATER = VK_COMPARE_OP_GREATER,
		NOT_EQUAL = VK_COMPARE_OP_NOT_EQUAL,
		GREATER_OR_EQUAL = VK_COMPARE_OP_GREATER_OR_EQUAL,
		ALWAYS = VK_COMPARE_OP_ALWAYS
	};

	enum class StencilOp {
		KEEP = VK_STENCIL_OP_KEEP,
		ZERO = VK_STENCIL_OP_ZERO,
		REPLACE = VK_STENCIL_OP_REPLACE,
		INCREMENT_AND_CLAMP = VK_STENCIL_OP_INCREMENT_AND_CLAMP,
		DECREMENT_AND_CLAMP = VK_STENCIL_OP_DECREMENT_AND_CLAMP,
		INVERT = VK_STENCIL_OP_INVERT,
		INCREMENT_AND_WRAP = VK_STENCIL_OP_INCREMENT_AND_WRAP,
		DECREMENT_AND_WRAP = VK_STENCIL_OP_DECREMENT_AND_WRAP
	};

	enum class SampleCountBits {
		COUNT_1_BIT = VK_SAMPLE_COUNT_1_BIT,
		COUNT_2_BIT = VK_SAMPLE_COUNT_2_BIT,
		COUNT_4_BIT = VK_SAMPLE_COUNT_4_BIT,
		COUNT_8_BIT = VK_SAMPLE_COUNT_8_BIT,
		COUNT_16_BIT = VK_SAMPLE_COUNT_16_BIT,
		COUNT_32_BIT = VK_SAMPLE_COUNT_32_BIT,
		COUNT_64_BIT = VK_SAMPLE_COUNT_64_BIT
	};

	inline SampleCountBits operator~ (SampleCountBits a) { return (SampleCountBits)~(int)a; }
	inline SampleCountBits operator| (SampleCountBits a, SampleCountBits b) { return (SampleCountBits)((int)a | (int)b); }
	inline SampleCountBits operator& (SampleCountBits a, SampleCountBits b) { return (SampleCountBits)((int)a & (int)b); }
	inline SampleCountBits operator^ (SampleCountBits a, SampleCountBits b) { return (SampleCountBits)((int)a ^ (int)b); }
	inline SampleCountBits& operator|= (SampleCountBits& a, SampleCountBits b) { return (SampleCountBits&)((int&)a |= (int)b); }
	inline SampleCountBits& operator&= (SampleCountBits& a, SampleCountBits b) { return (SampleCountBits&)((int&)a &= (int)b); }
	inline SampleCountBits& operator^= (SampleCountBits& a, SampleCountBits b) { return (SampleCountBits&)((int&)a ^= (int)b); }

	enum class ShaderStageBits {
		VERTEX = VK_SHADER_STAGE_VERTEX_BIT,
		TESSELLATION_CONTROL = VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT,
		TESSELLATION_EVALUATION = VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT,
		GEOMETRY = VK_SHADER_STAGE_GEOMETRY_BIT,
		FRAGMENT = VK_SHADER_STAGE_FRAGMENT_BIT,
		COMPUTE = VK_SHADER_STAGE_COMPUTE_BIT,
		ALL_GRAPHICS = VK_SHADER_STAGE_ALL_GRAPHICS,
		ALL = VK_SHADER_STAGE_ALL
	};

	inline ShaderStageBits operator~ (ShaderStageBits a) { return (ShaderStageBits)~(int)a; }
	inline ShaderStageBits operator| (ShaderStageBits a, ShaderStageBits b) { return (ShaderStageBits)((int)a | (int)b); }
	inline ShaderStageBits operator& (ShaderStageBits a, ShaderStageBits b) { return (ShaderStageBits)((int)a & (int)b); }
	inline ShaderStageBits operator^ (ShaderStageBits a, ShaderStageBits b) { return (ShaderStageBits)((int)a ^ (int)b); }
	inline ShaderStageBits& operator|= (ShaderStageBits& a, ShaderStageBits b) { return (ShaderStageBits&)((int&)a |= (int)b); }
	inline ShaderStageBits& operator&= (ShaderStageBits& a, ShaderStageBits b) { return (ShaderStageBits&)((int&)a &= (int)b); }
	inline ShaderStageBits& operator^= (ShaderStageBits& a, ShaderStageBits b) { return (ShaderStageBits&)((int&)a ^= (int)b); }

	enum class BufferUsageBits {
		TRANSFER_SRC_BIT = VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
		TRANSFER_DST_BIT = VK_BUFFER_USAGE_TRANSFER_DST_BIT,
		UNIFORM_TEXEL_BUFFER_BIT = VK_BUFFER_USAGE_UNIFORM_TEXEL_BUFFER_BIT,
		STORAGE_TEXEL_BUFFER_BIT = VK_BUFFER_USAGE_STORAGE_TEXEL_BUFFER_BIT,
		UNIFORM_BUFFER_BIT = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
		STORAGE_BUFFER_BIT = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
		INDEX_BUFFER_BIT = VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
		VERTEX_BUFFER_BIT = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
		INDIRECT_BUFFER_BIT = VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT,
		RAY_TRACING_BIT_NV = VK_BUFFER_USAGE_RAY_TRACING_BIT_NV
	};

	inline BufferUsageBits operator~ (BufferUsageBits a) { return (BufferUsageBits)~(int)a; }
	inline BufferUsageBits operator| (BufferUsageBits a, BufferUsageBits b) { return (BufferUsageBits)((int)a | (int)b); }
	inline BufferUsageBits operator& (BufferUsageBits a, BufferUsageBits b) { return (BufferUsageBits)((int)a & (int)b); }
	inline BufferUsageBits operator^ (BufferUsageBits a, BufferUsageBits b) { return (BufferUsageBits)((int)a ^ (int)b); }
	inline BufferUsageBits& operator|= (BufferUsageBits& a, BufferUsageBits b) { return (BufferUsageBits&)((int&)a |= (int)b); }
	inline BufferUsageBits& operator&= (BufferUsageBits& a, BufferUsageBits b) { return (BufferUsageBits&)((int&)a &= (int)b); }
	inline BufferUsageBits& operator^= (BufferUsageBits& a, BufferUsageBits b) { return (BufferUsageBits&)((int&)a ^= (int)b); }

	enum class BufferCreateFlags {
		NONE = 0,
		SPARSE_BINDING_BIT = VK_BUFFER_CREATE_SPARSE_BINDING_BIT,
		SPARSE_RESIDENCY_BIT = VK_BUFFER_CREATE_SPARSE_RESIDENCY_BIT,
		SPARSE_ALIASED_BIT = VK_BUFFER_CREATE_SPARSE_ALIASED_BIT
	};

	inline BufferCreateFlags operator~ (BufferCreateFlags a) { return (BufferCreateFlags)~(int)a; }
	inline BufferCreateFlags operator| (BufferCreateFlags a, BufferCreateFlags b) { return (BufferCreateFlags)((int)a | (int)b); }
	inline BufferCreateFlags operator& (BufferCreateFlags a, BufferCreateFlags b) { return (BufferCreateFlags)((int)a & (int)b); }
	inline BufferCreateFlags operator^ (BufferCreateFlags a, BufferCreateFlags b) { return (BufferCreateFlags)((int)a ^ (int)b); }
	inline BufferCreateFlags& operator|= (BufferCreateFlags& a, BufferCreateFlags b) { return (BufferCreateFlags&)((int&)a |= (int)b); }
	inline BufferCreateFlags& operator&= (BufferCreateFlags& a, BufferCreateFlags b) { return (BufferCreateFlags&)((int&)a &= (int)b); }
	inline BufferCreateFlags& operator^= (BufferCreateFlags& a, BufferCreateFlags b) { return (BufferCreateFlags&)((int&)a ^= (int)b); }

	enum class ComponentSwizzle {
		IDENTITY = VK_COMPONENT_SWIZZLE_IDENTITY,
		ZERO = VK_COMPONENT_SWIZZLE_ZERO,
		ONE = VK_COMPONENT_SWIZZLE_ONE,
		RED = VK_COMPONENT_SWIZZLE_R,
		GREEN = VK_COMPONENT_SWIZZLE_G,
		BLUE = VK_COMPONENT_SWIZZLE_B,
		ALPHA = VK_COMPONENT_SWIZZLE_A
	};

	inline ComponentSwizzle operator~ (ComponentSwizzle a) { return (ComponentSwizzle)~(int)a; }
	inline ComponentSwizzle operator| (ComponentSwizzle a, ComponentSwizzle b) { return (ComponentSwizzle)((int)a | (int)b); }
	inline ComponentSwizzle operator& (ComponentSwizzle a, ComponentSwizzle b) { return (ComponentSwizzle)((int)a & (int)b); }
	inline ComponentSwizzle operator^ (ComponentSwizzle a, ComponentSwizzle b) { return (ComponentSwizzle)((int)a ^ (int)b); }
	inline ComponentSwizzle& operator|= (ComponentSwizzle& a, ComponentSwizzle b) { return (ComponentSwizzle&)((int&)a |= (int)b); }
	inline ComponentSwizzle& operator&= (ComponentSwizzle& a, ComponentSwizzle b) { return (ComponentSwizzle&)((int&)a &= (int)b); }
	inline ComponentSwizzle& operator^= (ComponentSwizzle& a, ComponentSwizzle b) { return (ComponentSwizzle&)((int&)a ^= (int)b); }


	enum class MemoryPropertyFlags {
		DEVICE_LOCAL = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
		HOST_VISIBLE = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
		HOST_COHERENT = VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
		HOST_CACHED = VK_MEMORY_PROPERTY_HOST_CACHED_BIT,
		LAZILY_ALLOCATED = VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT,
		PROTECTED = VK_MEMORY_PROPERTY_PROTECTED_BIT
	};

	inline MemoryPropertyFlags operator~ (MemoryPropertyFlags a) { return (MemoryPropertyFlags)~(int)a; }
	inline MemoryPropertyFlags operator| (MemoryPropertyFlags a, MemoryPropertyFlags b) { return (MemoryPropertyFlags)((int)a | (int)b); }
	inline MemoryPropertyFlags operator& (MemoryPropertyFlags a, MemoryPropertyFlags b) { return (MemoryPropertyFlags)((int)a & (int)b); }
	inline MemoryPropertyFlags operator^ (MemoryPropertyFlags a, MemoryPropertyFlags b) { return (MemoryPropertyFlags)((int)a ^ (int)b); }
	inline MemoryPropertyFlags& operator|= (MemoryPropertyFlags& a, MemoryPropertyFlags b) { return (MemoryPropertyFlags&)((int&)a |= (int)b); }
	inline MemoryPropertyFlags& operator&= (MemoryPropertyFlags& a, MemoryPropertyFlags b) { return (MemoryPropertyFlags&)((int&)a &= (int)b); }
	inline MemoryPropertyFlags& operator^= (MemoryPropertyFlags& a, MemoryPropertyFlags b) { return (MemoryPropertyFlags&)((int&)a ^= (int)b); }

	namespace vkUtils {
		extern std::vector<std::string> samplerFilterNames;
		SamplerFilter samplerFilterFromString(const ::std::string& string);
		::std::string samplerFilterToString(const SamplerFilter& filter);

		extern std::vector<std::string> samplerMipmapModeNames;
		SamplerMipmapMode samplerMipmapModeFromString(const ::std::string& string);
		::std::string samplerMipmapModeToString(const SamplerMipmapMode& filter);

		extern std::vector<std::string> samplerAddressModeNames;
		SamplerAddressMode samplerAddressModeFromString(const ::std::string& string);
		::std::string samplerAddressModeToString(const SamplerAddressMode& mode);
	}
}

#endif // !GRAIL_VK_STRUCTS_H
