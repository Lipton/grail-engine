#include "FreeTypeFont.h"
#include "Logger.h"
#include "FileIO.h"
#include "Resources.h"

grail::FreeTypeFontGenerator::FreeTypeFontGenerator(const char * fontPath) {
	if (FT_Init_FreeType(&lib))
		GRAIL_LOG(ERROR, "FREE TYPE FONT GENERATOR") << "Could not init FreeType library";


	uint32_t size;
	unsigned char* data;

	FileIO::readBinaryFile(fontPath, &data, &size);

	if (FT_New_Memory_Face(lib, data, size, 0, &face))
		GRAIL_LOG(ERROR, "FREE TYPE FONT GENERATOR") << "Failed to load font";

	this->fontName = fontPath;
}

grail::FreeTypeFontGenerator::~FreeTypeFontGenerator() {
	FT_Done_Face(face);
	FT_Done_FreeType(lib);
}

grail::FreeTypeFont * grail::FreeTypeFontGenerator::generateFont(uint32_t fontSize) {
	FT_Set_Pixel_Sizes(face, 0, fontSize);

	float surfaceArea = (((fontSize + padding) * (fontSize + padding))) * 128;

	std::vector<Glyph> glyphs;
	for (uint32_t i = 0; i < 256; i++) {
		if (FT_Load_Char(face, i, FT_LOAD_RENDER)) {
			GRAIL_LOG(ERROR, "FREE TYPE FONT GENERATOR") << "Failed to load char:" << i;
			continue;
		}

		glyphs.push_back({
			i,
			0, 0,
			face->glyph->bitmap.width,
			face->glyph->bitmap.rows,
			face->glyph->bitmap_left,
			face->glyph->bitmap_top,
			face->glyph->advance.x
		});
	}
	std::sort(glyphs.begin(), glyphs.end());

	surfaceArea *= surfacePadding;
	surfaceArea = glm::sqrt(surfaceArea);
	surfaceArea *= 1.1f;
	//surfaceArea += glyphs[0].height;

	uint32_t HLimit = static_cast<uint32_t>(surfaceArea);

	uint32_t nextGlyph = 0;
	uint32_t HMark = padding;
	uint32_t VMark = padding;

	uint32_t lastRowMax = 0;

	while (nextGlyph != (glyphs.size())) {
		for (;;) {
			Glyph& g = glyphs[nextGlyph];

			if (HMark + g.width + padding > HLimit) {
				VMark += lastRowMax + padding;
				lastRowMax = 0;
				HMark = 0;
				break;
			}

			lastRowMax = glm::max(lastRowMax, g.height);

			g.x = HMark;
			g.y = VMark;

			HMark += g.width + padding;

			nextGlyph++;

			if (nextGlyph >= glyphs.size())
				break;
		}
	}

	struct Pixel {
		unsigned char r;
		unsigned char g;
		unsigned char b;
		unsigned char a;
	};

	std::vector<Pixel> textureData(HLimit * HLimit);

	for (Glyph& g : glyphs) {
		if (FT_Load_Char(face, g.c, FT_LOAD_RENDER)) {
			GRAIL_LOG(ERROR, "FREE TYPE FONT GENERATOR") << "Failed to load char:" << g.c;
			continue;
		}


		//GRAIL_LOG(INFO, "AWEFAWEFWAE") << face->glyph->bitmap.width << " | " << g.width;


		for (uint32_t x = 0; x < g.width; x++) {
			for (uint32_t y = 0; y < g.height; y++) {

				uint32_t xI = x + g.x;
				uint32_t yI = y + g.y;

				textureData[xI + yI * HLimit].r = face->glyph->bitmap.buffer[x + y * g.width];
				textureData[xI + yI * HLimit].g = face->glyph->bitmap.buffer[x + y * g.width];
				textureData[xI + yI * HLimit].b = face->glyph->bitmap.buffer[x + y * g.width];
				textureData[xI + yI * HLimit].a = face->glyph->bitmap.buffer[x + y * g.width];
			}
		}
	}

	std::map<char, FreeTypeFont::Character> charMap;

	for (Glyph& g : glyphs) {
		charMap.insert({ g.c,
			FreeTypeFont::Character{
			g.width,
			g.height,
			g.bearingX,
			g.bearingY,
			g.advance,
			((float)g.x / HLimit),
			((float)(g.x + g.width) / HLimit),
			((float)g.y / HLimit),
			((float)(g.y + g.height) / HLimit)
		} });
	}

	VkDeviceSize uploadSize = HLimit * HLimit * 4 * sizeof(unsigned char);

	TextureCreateInfo attachmentInfo = {};
	attachmentInfo.generateMipmaps = true;
	attachmentInfo.samplerInfo.addressModeU = SamplerAddressMode::CLAMP_TO_EDGE;
	attachmentInfo.samplerInfo.addressModeV = SamplerAddressMode::CLAMP_TO_EDGE;
	attachmentInfo.samplerInfo.magFilter = SamplerFilter::LINEAR;
	attachmentInfo.samplerInfo.minFilter = SamplerFilter::LINEAR;
	attachmentInfo.samplerInfo.mipmapMode = SamplerMipmapMode::LINEAR;
	attachmentInfo.format = VK_FORMAT_R8G8B8A8_UNORM;


	return new FreeTypeFont(charMap, Resources::createTexture(fontName, HLimit, HLimit, &textureData[0], uploadSize, attachmentInfo));
}

grail::FreeTypeFont::Character & grail::FreeTypeFont::getCharacter(char c) {
	return charMap.at(c);
}

grail::FreeTypeFont::FreeTypeFont(std::map<char, Character> charMap, VkTexture* texture) {
	this->charMap = charMap;
	this->texture = texture;
}

grail::FreeTypeFont::~FreeTypeFont() {
	if (texture) {
		delete texture;
	}
}
