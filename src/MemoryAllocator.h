#ifndef GRAIL_MEMORY_ALLOCATOR_H
#define GRAIL_MEMORY_ALLOCATOR_H

#include "Common.h"

namespace grail {
	class MemoryPage;
	class MemoryAllocator;

	struct PageRegion {
		// Size of the region
		VkDeviceSize size;

		// Offset of the region in the page
		VkDeviceSize baseAddress;

		// Page that the region belongs to
		MemoryPage* page = nullptr;

		// Next element in the linked list
		PageRegion* next = nullptr;
	};

	struct MemoryAllocation {
		// Requested size of the allocation
		VkDeviceSize size;

		// Full size with alignment
		VkDeviceSize alignedSize;

		// Offset in the page without alignment
		VkDeviceSize baseAddress;

		// Offset in the page with alignment
		VkDeviceSize baseAddressAligned;

		// Requested alignment of the memory during allocation
		VkDeviceSize alignment;

		// Region that this allocation belongs to
		MemoryPage* page = nullptr;

		void* mapMemory();
		void unmapMemory();
	};

	struct MemoryPage {
		friend class MemoryAllocator;
		friend class Editor;

		VkDeviceMemory memoryHandle;

		VkDeviceSize size;
		VkFlags memoryFlags;
		uint32_t memoryTypeIndex;

		MemoryAllocator* allocator = nullptr;

		void* map();
		void* getMappedPointer();
		void unmap();

		// Completely empty the page, all allocations are invalidated, user needs to track this on his own
		void clearMemory();

		VkDeviceSize getUsedMemory();
		VkDeviceSize getFreeMemory();
	private:
		void* mappedPointer = nullptr;

		PageRegion* headRegion = nullptr;
		std::unordered_map<uint64_t, PageRegion> freeRegions;
	};

	/*
		Memory allocator for GPU memory, allocates fixed-size blocks when
		memory is needed then uses a free list to suballocate the available
		blocks to allow deallocations, currently defragmentation is only a
		pipe dream as this would need to notify all of the resources that
		reference the moved memory to migrate to a new location, this is
		also extended to resource removal
	*/
	class MemoryAllocator {
		// TEMPORARY
		friend class Editor;
	public:
		MemoryAllocator(GPU& gpu, const std::string& identifier, VkDeviceSize pageSize = 256 * 1024 * 1024);
	
		MemoryAllocation allocate(VkDeviceSize allocationSize, VkDeviceSize allocationAlignment, VkMemoryPropertyFlags memoryFlags, uint32_t memoryTypeIndex);
		MemoryAllocation allocate(VkMemoryRequirements& memoryRequirements, VkMemoryPropertyFlags memoryFlags);
		void deallocate(MemoryAllocation& allocation);

		void removeEmptyPages();
		void clearAllPages();

		GPU& getGPU();

		VkDeviceSize getPageSize();

		std::string getIdentifier();

		VkDeviceSize getTotalAllocatedMemory();
		VkDeviceSize getTotalUsedMemory();
		VkDeviceSize getTotalFreeMemory();
	private:
		MemoryPage* allocatePage(VkMemoryPropertyFlags memoryFlags, uint32_t memoryTypeBits, uint32_t blockSize);

		std::string identifier = "";
		
		// Size of memory per page in bytes
		VkDeviceSize pageSize = 0;
		
		// GPU which owns this allocator
		GPU gpu;
		
		std::vector<MemoryPage*> pages;
	};
}

#endif