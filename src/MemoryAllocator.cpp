#include "MemoryAllocator.h"

#include "Utils.h"
#include <algorithm>

grail::MemoryAllocator::MemoryAllocator(GPU& gpu, const std::string& identifier, VkDeviceSize pageSize) {
	this->gpu = gpu;
	this->identifier = identifier;
	this->pageSize = pageSize;
}

grail::MemoryAllocation grail::MemoryAllocator::allocate(VkDeviceSize allocationSize, VkDeviceSize allocationAlignment, VkMemoryPropertyFlags memoryFlags, uint32_t memoryTypeIndex) {
	VkDeviceSize maxSize = allocationSize + allocationAlignment;
	uint32_t requiredType = gpu.getMemoryType(memoryTypeIndex, memoryFlags);

	// Find a fitting region to allocate the memory from, if one is not found,
	// then create a new memory page
	VkDeviceSize bestSize = std::numeric_limits<uint64_t>::max();
	PageRegion* bestRegionPrev = nullptr;
	PageRegion* bestRegion = nullptr;

	for (MemoryPage* page : pages) {
		if (page->memoryTypeIndex != requiredType) continue;

		PageRegion* region = page->headRegion;
		PageRegion* prevRegion = nullptr;

		while (region != nullptr) {
			// Can the region theoretically fit the allocation
			if (region->size >= allocationAlignment) {
				VkDeviceSize multiplier = region->baseAddress == 0 ? 0 : (region->baseAddress / allocationAlignment) + 1;
				VkDeviceSize alignedAddress = multiplier * allocationAlignment;
				VkDeviceSize padding = alignedAddress - region->baseAddress;

				VkDeviceSize requiredSize = allocationSize + padding;

				// Can the region actually fit the allocation
				if (region->size >= requiredSize) {
					VkDeviceSize remainingSize = region->size - requiredSize;

					if (remainingSize < bestSize) {
						bestSize = remainingSize;
						bestRegion = region;
						bestRegionPrev = prevRegion;
					}
				}
			}

			prevRegion = region;
			region = region->next;
		}
	}

	// If a free region was not found, then create a new page
	if (bestRegion == nullptr) {
		VkDeviceSize newPageSize = std::max(pageSize, allocationSize + allocationAlignment);

		MemoryPage* newPage = allocatePage(memoryFlags, memoryTypeIndex, newPageSize);
		bestRegion = newPage->headRegion;
	}

	// Once a region is selected, allocate the memory
	VkDeviceSize multiplier = bestRegion->baseAddress == 0 ? 0 : (bestRegion->baseAddress / allocationAlignment) + 1;
	VkDeviceSize alignedAddress = multiplier * allocationAlignment;
	VkDeviceSize padding = alignedAddress - bestRegion->baseAddress;

	VkDeviceSize requiredSize = allocationSize + padding;
	VkDeviceSize remainingSize = bestRegion->size - requiredSize;

	MemoryAllocation allocation = {};
	allocation.alignment = allocationAlignment;
	allocation.baseAddress = bestRegion->baseAddress;
	allocation.baseAddressAligned = bestRegion->baseAddress + padding;
	allocation.page = bestRegion->page;
	allocation.alignedSize = requiredSize;
	allocation.size = allocationSize;

	// If there is still remaining memory, create a new free region
	if (remainingSize > 0) {
		VkDeviceSize newRegionOffset = bestRegion->baseAddress + requiredSize;
		bestRegion->page->freeRegions.insert({ static_cast<uint64_t>(newRegionOffset), PageRegion{} });
		PageRegion* newRegion = &bestRegion->page->freeRegions.at((uint64_t)newRegionOffset);	
		newRegion->baseAddress = newRegionOffset;
		newRegion->page = bestRegion->page;
		newRegion->size = remainingSize;

		if (bestRegionPrev == nullptr) {
			/* 
				If the region has no previous free space,
				this should be the new head region, hence we remove the
				old head
			*/
			bestRegion->page->headRegion = newRegion;
			newRegion->next = bestRegion->next;
		}
		else {
			if (bestRegionPrev->next != nullptr) {
				newRegion->next = bestRegion->next;
			}

			bestRegionPrev->next = newRegion;
		}
		bestRegion->page->freeRegions.erase(bestRegion->baseAddress);

	}
	else {
		// If there is no memory left, we need no new free regions
		if (bestRegionPrev == nullptr) {
			// If head
			bestRegion->page->headRegion = nullptr;
			bestRegion->page->freeRegions.erase(bestRegion->baseAddress);
		}
		else {
			if (bestRegion->next) {
				bestRegionPrev->next = bestRegion->next;
			}
		}
	}

	return allocation;
}

grail::MemoryAllocation grail::MemoryAllocator::allocate(VkMemoryRequirements& memoryRequirements, VkMemoryPropertyFlags memoryFlags) {
	return allocate(memoryRequirements.size, memoryRequirements.alignment, memoryFlags, memoryRequirements.memoryTypeBits);
}

void grail::MemoryAllocator::deallocate(MemoryAllocation& allocation) {
	for (MemoryPage* page : pages) {
		if (allocation.page == page && page->allocator == allocation.page->allocator) {
			if (page->freeRegions.size() == 0) {
				page->freeRegions.insert({ (uint64_t)allocation.baseAddress, PageRegion{} });

				PageRegion* region = &page->freeRegions[(uint64_t)allocation.baseAddress];
				region->next = nullptr;
				region->baseAddress = (uint64_t)allocation.baseAddress;
				region->page = page;
				region->size = allocation.alignedSize;

				page->headRegion = region;
			}
			else {
				PageRegion* currentRegion = page->headRegion;
				PageRegion* prevRegion = nullptr;

				// Start looking from the head region
				while (currentRegion) {
					VkDeviceSize allocationBase = allocation.baseAddress;
					VkDeviceSize currentBase = currentRegion->baseAddress;

					if (allocationBase <= currentBase) {
						break;
					}

					prevRegion = currentRegion;
					currentRegion = currentRegion->next;
				}

				if (prevRegion) {
					// The new region is not the head
					// Is it possible to merge with previous?
					if (prevRegion->baseAddress + prevRegion->size == allocation.baseAddress) {
						// Possible to merge
						prevRegion->size += allocation.alignedSize;

						// Is it possible to merge with the next one?
						if (prevRegion->next) {
							if (prevRegion->baseAddress + prevRegion->size == prevRegion->next->baseAddress) {
								PageRegion* oldRegion = prevRegion->next;

								prevRegion->size += prevRegion->next->size;
								prevRegion->next = prevRegion->next->next;

								page->freeRegions.erase(oldRegion->baseAddress);
							}
						}
					}
					else {
						// Not possible to merge with the previous one

						PageRegion* next = prevRegion->next;

						bool nextMerge = false;
						if (next) { nextMerge = true; }
						if (nextMerge) { if (allocation.baseAddress + allocation.alignedSize != next->baseAddress) { nextMerge = false; } }

						// Is it possible to merge with the next one?
						if (nextMerge) {
							page->freeRegions.insert({ static_cast<uint64_t>(allocation.baseAddress), PageRegion{} });
							PageRegion* newRegion = &page->freeRegions.at((uint64_t)allocation.baseAddress);
							newRegion->baseAddress = allocation.baseAddress;
							newRegion->page = page;
							newRegion->size = allocation.alignedSize + next->size;

							prevRegion->next = newRegion;
							newRegion->next = next->next;

							page->freeRegions.erase(next->baseAddress);
						}
						else {
							// No merging possible, insert a new region
							page->freeRegions.insert({ static_cast<uint64_t>(allocation.baseAddress), PageRegion{} });
							PageRegion* newRegion = &page->freeRegions.at((uint64_t)allocation.baseAddress);
							newRegion->baseAddress = allocation.baseAddress;
							newRegion->page = page;
							newRegion->size = allocation.alignedSize;

							newRegion->next = prevRegion->next;
							prevRegion->next = newRegion;
						}
					}
				}
				else {
					// The new region would be a new head
					page->freeRegions.insert({ static_cast<uint64_t>(allocation.baseAddress), PageRegion{} });
					PageRegion* newRegion = &page->freeRegions.at((uint64_t)allocation.baseAddress);
					newRegion->baseAddress = allocation.baseAddress;
					newRegion->page = page;
					newRegion->size = allocation.alignedSize;

					newRegion->next = page->headRegion;
					page->headRegion = newRegion;

					if (newRegion->next) {
						if (newRegion->baseAddress + newRegion->size == newRegion->next->baseAddress) {
							VkDeviceSize originalSize = newRegion->baseAddress + newRegion->size;

							newRegion->size += newRegion->next->size;
							newRegion->next = newRegion->next->next;

							page->freeRegions.erase(originalSize);
						}
					}
				}
			}
		}
	}
}

void grail::MemoryAllocator::removeEmptyPages() {
	for (MemoryPage* page : pages) {
		if (page->freeRegions.size() == 1) {
			if (page->headRegion->baseAddress == 0 && page->headRegion->size == page->size) {
				vkFreeMemory(gpu.device, page->memoryHandle, nullptr);
				page->memoryHandle = VK_NULL_HANDLE;
			}

		}
	}

	auto end = std::remove_if(pages.begin(), pages.end(), [](const MemoryPage* i) {return i->memoryHandle == VK_NULL_HANDLE; });
	pages.erase(end, pages.end());
}

void grail::MemoryAllocator::clearAllPages() {
	for (MemoryPage* page : pages) {
		page->clearMemory();
	}
}

grail::GPU& grail::MemoryAllocator::getGPU() {
	return gpu;
}

VkDeviceSize grail::MemoryAllocator::getPageSize() {
	return pageSize;
}

std::string grail::MemoryAllocator::getIdentifier() {
	return identifier;
}

VkDeviceSize grail::MemoryAllocator::getTotalAllocatedMemory() {
	VkDeviceSize size = 0;

	for (MemoryPage* page : pages) {
		size += page->size;
	}

	return size;
}

VkDeviceSize grail::MemoryAllocator::getTotalUsedMemory() {
	VkDeviceSize used = 0;

	for (MemoryPage* page : pages) {
		used += page->getUsedMemory();
	}

	return used;
}

VkDeviceSize grail::MemoryAllocator::getTotalFreeMemory() {
	VkDeviceSize free = 0;

	for (MemoryPage* page : pages) {
		free += page->getFreeMemory();
	}

	return free;
}

grail::MemoryPage* grail::MemoryAllocator::allocatePage(VkMemoryPropertyFlags memoryFlags, uint32_t memoryTypeBits, uint32_t blockSize) {  
	GRAIL_LOG(INFO, "MEMORY REGION") 
		<< "Allocating new page on " << identifier 
		<< " [Flags: " << memoryFlags
		<<", Type Bits: " << memoryTypeBits
		<< ", Size: " << utils::string::bytesToString(blockSize) << " Bytes]...";

	MemoryPage* page = new MemoryPage();
	page->allocator = this;
	page->memoryFlags = memoryFlags;
	page->memoryTypeIndex = gpu.getMemoryType(memoryTypeBits, memoryFlags);
	page->size = blockSize;

	page->freeRegions.insert({ (uint64_t)0, PageRegion{} });

	PageRegion* region = &page->freeRegions[0];
	region->next = nullptr;
	region->baseAddress = 0;
	region->page = page;
	region->size = blockSize;

	page->headRegion = region;
	
	VkMemoryAllocateInfo allocationInfo = {};
	allocationInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	allocationInfo.pNext = nullptr;
	allocationInfo.allocationSize = page->size;
	allocationInfo.memoryTypeIndex = page->memoryTypeIndex;
	VK_VALIDATE_RESULT(vkAllocateMemory(gpu.device, &allocationInfo, nullptr, &page->memoryHandle));
	
	this->pages.push_back(page);

	return page;
}

void* grail::MemoryAllocation::mapMemory() {
	void* ptr = page->map();
	unsigned char* address = reinterpret_cast<unsigned char*>(ptr);
	address += this->baseAddressAligned;

	return reinterpret_cast<void*>(address);
}

void grail::MemoryAllocation::unmapMemory() {
	page->unmap();
}

void* grail::MemoryPage::map() {
	// Cant map non-visible memory
	if (!(memoryFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT))
		return nullptr;

	if (!mappedPointer) {
		// We map the whole page at the same time
		VK_VALIDATE_RESULT(vkMapMemory(allocator->getGPU().device, memoryHandle, 0, VK_WHOLE_SIZE, 0, &mappedPointer));
	}

	return mappedPointer;
}

void* grail::MemoryPage::getMappedPointer() {
	return mappedPointer;
}

void grail::MemoryPage::unmap() {
	if (!mappedPointer)
		return;

	// No need to unmap coherent memory
	if (!(memoryFlags & VK_MEMORY_PROPERTY_HOST_COHERENT_BIT))
		return;

	vkUnmapMemory(allocator->getGPU().device, memoryHandle);
	mappedPointer = nullptr;
}

void grail::MemoryPage::clearMemory() {
	this->freeRegions.clear();

	this->freeRegions.insert({ (uint64_t)0, PageRegion{} });
	PageRegion* region = &this->freeRegions[0];
	region->next = nullptr;
	region->baseAddress = 0;
	region->page = this;
	region->size = size;

	headRegion = region;
}

VkDeviceSize grail::MemoryPage::getUsedMemory() {
	return size - getFreeMemory();
}

VkDeviceSize grail::MemoryPage::getFreeMemory() {
	VkDeviceSize freeMemory = 0;
	
	PageRegion* region = headRegion;

	while (region) {
		freeMemory += region->size;

		region = region->next;
	}

	return freeMemory;
}
