#ifndef GRAIL_MATERIAL_PARSER_H
#define GRAIL_MATERIAL_PARSER_H

#include "Common.h"
#include "Material.h"
#include <json.hpp>
#include <spirv_cross.hpp>

namespace grail {
	class FrameGraph;

	enum ShaderVarType {
		INT,
		FLOAT,
		VEC2,
		VEC3,
		VEC4,
		MAT2,
		MAT3,
		MAT4,
		SAMPLER_2D,
		SAMPLER_3D,
		SAMPLER_CUBE,
		SAMPLER_ARRAY_2D,
		SAMPLER_ARRAY_3D
	};

	enum class VarType : uint32_t {
		NONE = 0,
		COLOR = 1,
		SCALAR = 2,
		SCALAR_SEPARATE = 4,
		BOOLEAN = 8,
		LIST = 16,
		TEXTURE = 32
	};

	inline VarType operator~ (VarType a) { return (VarType)~(int)a; }
	inline VarType operator| (VarType a, VarType b) { return (VarType)((int)a | (int)b); }
	inline VarType operator& (VarType a, VarType b) { return (VarType)((int)a & (int)b); }
	inline VarType operator^ (VarType a, VarType b) { return (VarType)((int)a ^ (int)b); }
	inline VarType& operator|= (VarType& a, VarType b) { return (VarType&)((int&)a |= (int)b); }
	inline VarType& operator&= (VarType& a, VarType b) { return (VarType&)((int&)a &= (int)b); }
	inline VarType& operator^= (VarType& a, VarType b) { return (VarType&)((int&)a ^= (int)b); }

	struct ModifiableShaderVar {
		std::string name;
		std::string shaderName;

		ShaderVarType varType;
		VarType editType = VarType::NONE;

		size_t offset;
		size_t size;

		float step = 0.1f;
		float min = 0.0f;
		float max = 0.0f;

		bool boolVal = true;

		std::map<std::string, std::string> properties;

		std::string getProperty(std::string name) {
			if (properties.find(name) != properties.end()) {
				return properties[name];
			}

			return "";
		}
	};

	struct ShaderVar {
		std::string name;
		ShaderVarType type;
		uint32_t offset;
		uint32_t size;
	};


	class VkTexture;
	struct ShaderTexture {
		std::string name;

		ShaderStageBits stages;
		uint32_t binding;

		VkTexture* texture;
		std::string currentTextureName;
	};

	struct ShaderBuffer {
		std::vector<ShaderVar> vars;

		ShaderStageBits stageFlags;

		uint32_t size;

	};

	struct MaterialDescriptor {
		//MaterialCreateInfo createInfo;

		std::vector<std::string> shaders;

		PushConstantRange pushConstantRange;
		std::vector<ShaderVar> pushBlockVars;
		std::vector<ModifiableShaderVar> modifiableVars;
		std::vector<ShaderTexture> textures;

		std::map<std::string, std::string> defaults;
	};

	enum class ShaderOpaqueType {
		SAMPLER_1D,
		SAMPLER_2D,
		SAMPLER_3D,
		SAMPLER_CUBE_MAP,
		SAMPLER_RECTANGLE,
		SAMPLER_1D_ARRAY,
		SAMPLER_2D_ARRAY,
		SAMPLER_BUFFER,
		SAMPLER_2D_MULTISAMPLE,
		SAMPLER_2D_MULTISAMPLE_ARRAY
	};

	struct ShaderOpaque {
		std::string identifier;

		ShaderStageBits usedStages;
		uint32_t set;
		uint32_t binding;


	};

	struct ShaderModule {
		std::string name;
	};

	/*struct MaterialDescription {
		std::string name;

		std::vector<ShaderModule> shaderEntries;
	};*/

	class MaterialParser {
	public:
		static MaterialDescriptor parseMaterial_(const std::string& descriptorFile);

		static MaterialDescription parseMaterialFromFile(const std::string& descriptorFile, const std::string& staticDefines = "");

		// Parse and crated a base material based on the descriptor
		// Need to solve the issue with having to provide a Frame Graph
		// The rules for a material file and permutations are:
		// Descriptor layout cannot differ per permutation
		// A FrameGraph with the required passes has to be provided
		// The vertex layout is parsed by looking at the shader
		static MaterialDescription* parseMaterial(const std::string& descriptorFile, FrameGraph* graph);

	private:
		static std::map<std::string, CullMode> cullModeMappings;
		static std::map<std::string, FrontFace> frontFaceMappings;
		static std::map<std::string, VkBool32> booleanMappings;

		// Search trough a give node and gather all of the defined pipeline information fields
		// and put them in the given info structure
		static void parsePipeline(GraphicsPipelineCreateInfo& info, nlohmann::json& baseNode, FrameGraph* graph);
		static void parsePipeline(GraphicsPipelineCreateInfo& info, nlohmann::json& baseNode, FrameGraph* graph, std::vector<std::string>& overloads);

		static std::string getDescriptorValueRequired(nlohmann::json* node, const std::string& id);

		static void gatherResources(std::vector<std::string>& input, MaterialDescription& description);

		template <class T>
		static void getMapping(const std::string& name, std::map<std::string, T>& map, T* val);

		template <class T>
		static void getMapping(nlohmann::json& node, const std::string& name, std::map<std::string, T>& map, T* val);
	};

	template<class T>
	inline void MaterialParser::getMapping(const std::string & name, std::map<std::string, T>& map, T * val) {
		std::string key = std::string(name);
		std::transform(key.begin(), key.end(), key.begin(), ::tolower);

		if (map.find(key) != map.end()) {
			*val = static_cast<T>(map[key]);
		}
	}

	template<class T>
	inline void MaterialParser::getMapping(nlohmann::json& node, const std::string & name, std::map<std::string, T>& map, T * val) {
		if (node.find(name) != node.end()) {
			if (map.find(node[name]) != map.end()) {
				*val = static_cast<T>(map[node[name]]);
			}
		}
	}

	static ModifiableShaderVar* findParam(std::vector<ModifiableShaderVar>& params, std::string name);
	static std::vector<ModifiableShaderVar> getModifiableValues(std::string& code);
}

#endif