#ifndef GRAIL_RENDERER_H
#define GRAIL_RENDERER_H

#include "Common.h"
#include "DescriptorSet.h"
#include "VertexLayout.h"
#include "MemoryRegion.h"
#include "ModelLoader.h"
#include "ThreadPool.h"
#include "PipelineFactory.h"
#include "Camera.h"
#include "Texture.h"
#include "Font.h"
#include "VkBuffer.h"

#include "ImGuizmo.h"

namespace grail {
	class SceneNode;
	class MaterialLibrary;
	class Material;
	class RenderChain;
	class MaterialCreateInfo;

	class Framebuffer;

	class Mesh;

	class Renderer {
	public:
		struct UniformBuffer {
			VkBuffer handle;
			VkDescriptorBufferInfo descriptor;
			void* mappedPtr = nullptr;

			void copyData(void* data, uint32_t size) {
				void* pData = mapBuffer();
				memcpy(pData, data, size);
			}

			void* mapBuffer() {
				/*if (!mappedPtr)
					mappedPtr = memoryRegion.block->mapMemory(memoryRegion);

				return mappedPtr;*/

				return nullptr;
			}

			void flush() {
				/*memoryRegion.block->unmapMemory();

				VkMappedMemoryRange mappedRange = {};
				mappedRange.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
				mappedRange.memory = memoryRegion.block->memoryHandle;
				mappedRange.offset = memoryRegion.offset;
				mappedRange.size = VK_WHOLE_SIZE;

				VK_VALIDATE_RESULT(vkFlushMappedMemoryRanges(memoryRegion.region->getGPU().device, 1, &mappedRange));*/
			}
		};

		Renderer();

		void render(SceneNode** rootNode, grail::Camera* camera, float delta);

		void printMemoryConsumption();

		void createDefaultDeferredMaterial(SceneNode* node);

		bool TAA = false;
		bool renderUI = true;

		SceneNode* currentRoot = nullptr;
		SceneNode** baseRoot = nullptr;

		void createMaterial(const std::string& materialDescriptor, SceneNode* node);

		UniformBuffer createBuffer(std::string identifier, VkDeviceSize size, VkBufferUsageFlags usageFlags);



		RenderChain* androidChain;

		void initializeAndroid();
		void renderAndroid(SceneNode** rootNode, grail::Camera* camera, float delta);
	private:
		// DEBUG VARS
		bool freezeFrustum = false;
		bool cull = false;
		bool multithreadedCull = false;
		bool hyperthreading = false;

		bool enableLUT = false;

		uint32_t concurency;
		ThreadPool* threadPool;

		// FUNCTIONS
		void beginFrame();
		void endFrame();

		void initialize();
		void allocateMemory();
		void createCommandBuffers();
		void createSyncObjects();
		void createFramebuffers();
		void createMeshes();
		void createMaterials();

		void generateBRDFLUT();
		void generateIrradianceCube();
		void generatePrefilteredCube();

		void renderMesh(VkCommandBuffer commandBuffer, Mesh* mesh, Material* material, void* pushConstants);

		void drawUI();
		std::string processCPUTimings();

		float passed_time = 0;
		float passed_time_old = 0;

		Camera* camera;
		RenderChain* renderChain;

		Mesh* fullscreenMesh;

		VertexLayout* defaultVertexLayout;
		VertexLayout* fullscreenVertexLayout;

		// GLOBAL INFO
		Swapchain* swapchain;
		uint32_t currentSwapchainBuffer = 0;
		uint32_t inflightFrameIndex = 0;

		VkInstance instance;

		GPU mainGPU;
		std::vector<GPU> gpus;

		VkQueue graphicsQueue;
		uint32_t graphicsQueueIndex;

		VkFormat depthFormat;
		VkFormat colorFormat;
		VkColorSpaceKHR colorSpace;

		// SYNC OBJECTS
		VkSemaphore presentCompleteSemaphores[2];
		VkSemaphore renderCompleteSemaphores[2];

		std::vector<VkFence> waitFences;
		//std::vector<DescriptorSetGroup*> TAADescriptors;
		//std::vector<DescriptorSetGroup*> presentDescriptors;

		ImGuizmo::OPERATION transformType = ImGuizmo::OPERATION::DISABLED;
		uint32_t imguiDescriptorIndex = 1;
		//std::array<DescriptorSetGroup*, 500> imguiDescriptors;
		std::string sceneInput;
		bool transforming = false;
		glm::mat4 localCopy;
		glm::mat4 worldCopy;
		glm::mat4 delta = glm::mat4(1.0);

		// COMMAND POOLS AND BUFFERS
		VkCommandPool renderCommandBufferPool;

		VkCommandBuffer setupCommandBuffer;
		VkCommandBuffer renderCommandBuffer;

		// FRAMEBUFFERS AND RENDER PASSES
		std::vector<VkFramebuffer> presentFramebuffers;
		VkRenderPass presentPass;

		// SUBMIT RELATED
		VkTexture* defaultAlbedo;
		VkTexture* defaultNormals;
		VkTexture* defaultRoughness;
		VkTexture* defaultMetallic;
		VkTexture* envMap;

		Material* depthPrepassMaterial;
		Material* cascadeMaterial;

		//DescriptorSetGroup* group = nullptr;
		VkPipeline pipeline = 0;

		struct RenderPassMaterialInfo {
			PushConstantRange pushConstantsRange = { };

			std::string vertexShader = "";
			std::string fragmentShader = "";
			std::string computeShader = "";
				
			CullMode cullMode = CullMode::BACK;
		};

		struct DescriptorSetInfo {
			std::string identifier;
			DescriptorBindingType type;
			VkShaderStageFlags stageFlags;
			void* ref;
		};

		void createPassMaterial(const std::string& passName, RenderPassMaterialInfo& info, std::initializer_list<DescriptorSetInfo> descriptors);
		void createPassMaterial(const std::string& passName, const std::string& matName, RenderPassMaterialInfo& info, std::initializer_list<DescriptorSetInfo> descriptors);
		void* attachmentDescriptor(const std::string& attachmentName);
		void* attachmentDescriptor(const std::string& attachmentName, uint32_t level);
		void* uniformDescriptor(const std::string& uniformName);

		public:
		std::map<std::string, Material*> materials;
		private:
		uint32_t lightpassFramebufferIndex = 0;
		VkFramebuffer lightpassFramebuffers[2];

		std::vector<VkFramebuffer> depthBuffers;
		//std::vector<DescriptorSetGroup*> depthDescriptors;

		VkSubmitInfo submitInfo;
		VkPipelineStageFlags submitPipelineStages = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

		MaterialLibrary* materialLibrary;
		PipelineFactory* pipelineFactory;

		uint64_t frameCount = 0;
		uint64_t m_SampleIndex = 0;

		glm::vec2 subsampleOffset = glm::vec2(0.0);
		glm::vec2 subsampleOffsetOld = glm::vec2(0.0);

		const int shadowMapCascadeSplits = 4;
		float cascadeSplitLambda = 0.755f; // 0.755
		uint32_t shadowCascadeMapSize = 4096;

		Buffer* particleBuffer;
		//0.624 0.949
		glm::vec3 lightPos = glm::vec3(-0.478, 0.624, 0.949);
		glm::vec3 lightColor = glm::vec3(1.0, 1.0, 1.0);
		float lightIntensity = 4.0f;
		int activeCascades = 3;
		bool visualizeCascades = false;
		int BRDFFunc = 0;
		float sharpenFactor = 0.0;
		float envScaleGlobal = 1.0f;
		float envScale = 1.0f;
		float envScaleIBL = 1.0f;
		int envColorFunc = 0;
		int envSource = 1;

		bool sharpen = false;
		bool taa = true;
		int taaMode = 1;
		bool enableSSR = false;
		glm::vec2 taaJitter;

		std::map<std::string, double> cpuTimings;
		std::vector<VkShaderModule> modules;

		struct Cascade {
			VkImageView view;
			VkFramebuffer framebuffer;

			float splitDepth;
			glm::mat4 combinedMat;
			Frustum frustum;
		};

		void updateCascades();
		void cullRenderables(uint32_t i, std::vector<grail::Renderer::Cascade>& cascades, std::vector<grail::SceneNode*>& renderables, std::vector<grail::SceneNode*>& culledNodes, std::vector<std::vector<grail::SceneNode*>>& shadowNodes);

		std::vector<Cascade> cascades;

		std::vector<VkFramebuffer> bloomBuffers;

		bool debugShadows = false;
		int shadowCascadeIndex = 0;
		struct {
			glm::mat4 combined;
			glm::mat4 lastCombined;
			glm::mat4 cascadeMatrices[4];

			glm::vec4 jitter;

			glm::vec4 cameraPos;

			glm::vec2 time;
		} uboVS;

		struct {
			glm::mat4 invProj;
			glm::mat4 view;
		} lightpassVS;

		struct {
			glm::mat4 invCombined;
			glm::vec4 cameraPos;
			float cascadeSplits[4];
			glm::mat4 inverseView;
			glm::vec4 lightDir;
			glm::mat4 cascadeViewProj[4];
			glm::mat4 inverseProjection;
			glm::mat4 combined;
			glm::vec4 lightColorIntensity;
			uint32_t cascadeCount;
			int visualize;
			int brdfFunc;
		} lightpassFS;

		struct {
			glm::mat4 projection;
			glm::mat4 inverseView;
			glm::mat4 inverseProjection;
			glm::mat4 inverseCombined;
			glm::mat4 view;
			glm::vec4 cameraPos;
			glm::vec2 jitter;
		} ssaoPassFSCam;

		struct {
			glm::mat4 inverseCombined;
			glm::vec4 cameraPos;
			glm::mat4 inverseView;
			glm::mat4 inverseProjection;
			glm::mat4 combined;
			glm::vec4 cameraScreen;
			int enableSharpen;
		} SSRPassFS;

		glm::mat4 jitterMatrix;

		std::map<std::string, UniformBuffer> uniformBuffers;

		void createUniformBuffer(std::string identifier, VkDeviceSize size);
		void updateUniformBuffers(grail::Camera* camera);

		const glm::vec2 SAMPLE_LOCS_16[16] = {
			glm::vec2(-8.0f, 0.0f) / 8.0f,
			glm::vec2(-6.0f, -4.0f) / 8.0f,
			glm::vec2(-3.0f, -2.0f) / 8.0f,
			glm::vec2(-2.0f, -6.0f) / 8.0f,
			glm::vec2(1.0f, -1.0f) / 8.0f,
			glm::vec2(2.0f, -5.0f) / 8.0f,
			glm::vec2(6.0f, -7.0f) / 8.0f,
			glm::vec2(5.0f, -3.0f) / 8.0f,
			glm::vec2(4.0f, 1.0f) / 8.0f,
			glm::vec2(7.0f, 4.0f) / 8.0f,
			glm::vec2(3.0f, 5.0f) / 8.0f,
			glm::vec2(0.0f, 7.0f) / 8.0f,
			glm::vec2(-1.0f, 3.0f) / 8.0f,
			glm::vec2(-4.0f, 6.0f) / 8.0f,
			glm::vec2(-7.0f, 8.0f) / 8.0f,
			glm::vec2(-5.0f, 2.0f) / 8.0f
		};

		#define SAMPLE_LOCS SAMPLE_LOCS_16
		#define SAMPLE_COUNT 16

		//PushConstantRange pushConstantRange;

		/*VkSemaphore depthPrepassCompleteSemaphore;
		VkSemaphore gPassCompleteSemaphore;
		VkSemaphore lightPassCompleteSemaphore;*/

		/*VkCommandBuffer depthPrepassCommandBuffer;
		VkCommandBuffer gPassCommandBuffer;
		VkCommandBuffer lightPassCommandBuffer;*/

		/*VkRenderPass gRenderPass;
		VkRenderPass lightRenderPass;*/

		/*Framebuffer* lightPassBuffer;
		Framebuffer* depthPrepassBuffer;
		Framebuffer* gBuffer;*/

		//glm::vec3 rotation = glm::vec3(0.0f);

		//std::vector<VkCommandBuffer> prePresentCommandBuffers;
		//std::vector<VkCommandBuffer> postPresentCommandBuffers;

		//VkPipeline pipeline;

		//VkBuffer globalHostLocalBuffer;

		//DescriptorSetGroup* testGroup;

		/*struct RenderThread {
		VkCommandPool cmdPool;
		VkCommandBuffer cmdBuffer;
		};
		std::vector<RenderThread> renderThreads;
		ThreadPool* renderThreadPool;*/

		//std::vector<MeshR> meshes;
		//std::unordered_map<std::string, VkTexture> textureMap;

		//MeshR loadMesh(MeshDataR& data);
		//VkTexture loadTexture(const std::string& path, TextureCreateInfo& createInfo);


		// UTILITY FUNCTIONS
		/*void setImageLayout(
		VkCommandBuffer cmdbuffer,
		VkImage image,
		VkImageAspectFlags aspectMask,
		VkImageLayout oldImageLayout,
		VkImageLayout newImageLayout,
		VkImageSubresourceRange subresourceRange);

		void setImageLayout(
		VkCommandBuffer cmdbuffer,
		VkImage image,
		VkImageAspectFlags aspectMask,
		VkImageLayout oldImageLayout,
		VkImageLayout newImageLayout);

		void setImageLayout(
		VkCommandBuffer cmdbuffer,
		VkImage image,
		VkImageAspectFlags aspectMask,
		VkImageLayout oldImageLayout,
		VkImageLayout newImageLayout,
		VkImageSubresourceRange subresourceRange,
		VkPipelineStageFlags srcStageMask,
		VkPipelineStageFlags dstStageMask);*/

		/*struct MeshR {
		uint32_t indicesCount;

		MemoryRegionDescriptor vertexRegionDescriptor;
		MemoryRegionDescriptor indexRegionDescriptor;

		VkBuffer buffer;

		VkTexture* diffuseTexture;
		DescriptorSetGroup* descriptorGroup;

		MeshPushConstants material;

		float radius;
		};*/

		// ATTACHMENT STRUCTS
		/*struct {
		VkImage image;
		VkDeviceMemory deviceMem;
		VkImageView imageView;
		} depthStencilStruct;*/

		/*struct MeshPushConstants {
		glm::vec3 diffuseColor;
		};*/
	};
}

#endif