#include "TextureAssetImporter.h"

#include "FileIO.h"
#include "VkTexture.h"
#include "Resources.h"
#include "ImGuiHelper.h"

void grail::TextureAssetImporter::importAsset(const std::string& path) {
	std::string basePath = FileIO::getBasePath(path);
	std::string assetName = FileIO::getFilename(path);
	std::string extension = FileIO::getExtension(path);

	std::string descriptorPath = basePath + assetName + ".meta";

	TextureAssetDescriptor* descriptor = new TextureAssetDescriptor();

	if (!FileIO::fileExists(descriptorPath)) {
		descriptor->assetPath = path;
		descriptor->assetExtension = extension;
		descriptor->assetDirectory = basePath;
		descriptor->uuid = uuid_utils::v4();

		nlohmann::json data;
		descriptor->write(data);

		std::ofstream file;
		file.open(descriptorPath);
		file << data.dump(4);
		file.close();
	}

	char* contents = nullptr;
	uint32_t size = 0;

	FileIO::readBinaryFileExternal(descriptorPath, &contents, &size);

	nlohmann::json data = nlohmann::json::parse(std::string(contents, size));
	descriptor->read(data);

	delete[] contents;

	Resources::loadTextureAsset(descriptor);
}

bool grail::TextureAssetImporter::showImportMenu(AssetRef<Asset> asset, ImGuiHelper* uiHelper) {
	TextureAsset* tex = dynamic_cast<TextureAsset*>(asset.get());
	TextureAssetDescriptor* descriptor = dynamic_cast<TextureAssetDescriptor*>(tex->getDescriptor());

	UI::Image(uiHelper->imageID(&tex->getHandle()), ImVec2(512 * UI::scale, 512 * UI::scale));
	UI::NewLine();
	UI::TextColored(ImVec4(.8, .8, .8, 1), "%s", "Asset Information");
	UI::Separator();
	UI::Text("Name: %s", asset->getAssetName().c_str());
	UI::Text("Width: %i", tex->getHandle().getWidth());
	UI::Text("Height: %i", tex->getHandle().getHeight());
	UI::Text("Depth: %i", tex->getHandle().getDepth());
	UI::Text("Mip Levels: %i", tex->getHandle().getMipCount());
	UI::Text("Layers: %i", tex->getHandle().getLayerCount());
	UI::Text("Format: %s", utils::vulkan::formatToString(tex->getHandle().getFormat()).c_str());
	UI::Text("Layout: %s", utils::vulkan::imageLayoutToString(tex->getHandle().getLayout()).c_str());
	UI::Text("Type: %s", utils::vulkan::imageViewTypeToString(tex->getHandle().getViewType()).c_str());
	UI::Text("UUID: %s", asset->getAssetUUIDString().c_str());
	UI::Text("Size In Bytes: %s", utils::string::bytesToString(tex->getHandle().getMemoryRegion().alignedSize));

	UI::NewLine();
	UI::TextColored(ImVec4(.8, .8, .8, 1), "%s", "Mipmapping");
	UI::Separator();
	UI::Checkbox("Generate Mipmaps##grail_texture_importer", &descriptor->generateMipmaps);

	UI::NewLine();
	UI::TextColored(ImVec4(.8, .8, .8, 1), "%s", "Filtering");
	UI::Separator();
	{
		std::string currentMinFilter = vkUtils::samplerFilterToString(descriptor->minFilter);
		if (UI::BeginCombo("Min Filter##grail_texture_importer", currentMinFilter.c_str())) {
			for (const std::string& name : vkUtils::samplerFilterNames) {
				bool is_selected = (currentMinFilter.compare(name) == 0);
				if (UI::Selectable(name.c_str(), is_selected))
					descriptor->minFilter = vkUtils::samplerFilterFromString(name);
				if (is_selected)
					UI::SetItemDefaultFocus();
			}
			ImGui::EndCombo();
		}
	}

	{
		std::string currentMagFilter = vkUtils::samplerFilterToString(descriptor->magFilter);
		if (UI::BeginCombo("Mag Filter##grail_texture_importer", currentMagFilter.c_str())) {
			for (const std::string& name : vkUtils::samplerFilterNames) {
				bool is_selected = (currentMagFilter.compare(name) == 0);
				if (UI::Selectable(name.c_str(), is_selected))
					descriptor->magFilter = vkUtils::samplerFilterFromString(name);
				if (is_selected)
					UI::SetItemDefaultFocus();
			}
			ImGui::EndCombo();
		}
	}

	UI::NewLine();
	UI::TextColored(ImVec4(.8, .8, .8, 1), "%s", "Wrap");
	UI::Separator();
	{
		std::string currentWrapU = vkUtils::samplerAddressModeToString(descriptor->addressModeU);
		if (UI::BeginCombo("U Wrap##grail_texture_importer", currentWrapU.c_str())) {
			for (const std::string& name : vkUtils::samplerAddressModeNames) {
				bool is_selected = (currentWrapU.compare(name) == 0);
				if (UI::Selectable(name.c_str(), is_selected))
					descriptor->addressModeU = vkUtils::samplerAddressModeFromString(name);
				if (is_selected)
					UI::SetItemDefaultFocus();
			}
			ImGui::EndCombo();
		}
	}

	{
		std::string currentWrapV = vkUtils::samplerAddressModeToString(descriptor->addressModeV);
		if (UI::BeginCombo("V Wrap##grail_texture_importer", currentWrapV.c_str())) {
			for (const std::string& name : vkUtils::samplerAddressModeNames) {
				bool is_selected = (currentWrapV.compare(name) == 0);
				if (UI::Selectable(name.c_str(), is_selected))
					descriptor->addressModeV = vkUtils::samplerAddressModeFromString(name);
				if (is_selected)
					UI::SetItemDefaultFocus();
			}
			ImGui::EndCombo();
		}
	}

	{
		std::string currentWrapW = vkUtils::samplerAddressModeToString(descriptor->addressModeW);
		if (UI::BeginCombo("W Wrap##grail_texture_importer", currentWrapW.c_str())) {
			for (const std::string& name : vkUtils::samplerAddressModeNames) {
				bool is_selected = (currentWrapW.compare(name) == 0);
				if (UI::Selectable(name.c_str(), is_selected))
					descriptor->addressModeW = vkUtils::samplerAddressModeFromString(name);
				if (is_selected)
					UI::SetItemDefaultFocus();
			}
			ImGui::EndCombo();
		}
	}

	UI::NewLine();
	UI::TextColored(ImVec4(.8, .8, .8, 1), "%s", "Anisotropy");
	UI::Separator();

	int anisotropy = (int)descriptor->maxAnisotropy;
	UI::SliderInt("##grail_texture_importer_anisotropy", &anisotropy, 0, 16);
	descriptor->maxAnisotropy = (float)anisotropy;

	UI::NewLine();
	if (UI::Button("Reimport Texture")) {
		nlohmann::json data;
		descriptor->write(data);

		std::ofstream file;
		file.open(descriptor->assetDirectory + FileIO::getFilename(asset->getAssetName()) + ".meta");
		file << data.dump(4);
		file.close();

		Resources::unloadAsset(asset);
		Resources::loadTextureAsset(descriptor);
	
		return true;
	}

	return false;
}

//void grail::TextureAssetImporter::getDescriptor(const std::string& descriptorPath, const std::string& assetPath) {
	/*if (!FileIO::fileExists(descriptorPath)) {
		TextureAssetDescriptor descriptor;
		descriptor.descriptorVersion = 1;
		descriptor.filePath = assetPath;
		descriptor.id = uuid_utils::v4();

		nlohmann::json data;
		descriptor.write(data);

		std::ofstream file;
		file.open(descriptorPath);
		file << data.dump(4);
		file.close();
	}

	char* contents = nullptr;
	uint32_t size = 0;

	FileIO::readBinaryFileExternal(descriptorPath, &contents, &size);

	nlohmann::json data = nlohmann::json::parse(std::string(contents, size));
	tempDescriptor.read(data);

	delete[] contents;*/
//}

//void grail::TextureAssetImporter::saveDescriptor(const std::string& descriptorPath) {
	//nlohmann::json data;
	//tempDescriptor.write(data);

	//std::ofstream file;
	//file.open(descriptorPath);
	//file << data.dump(4);
	//file.close();
//}

/*void grail::TextureAssetDescriptor::write(nlohmann::json& node) {
	node["version"] = descriptorVersion;
	node["path"] = filePath;
	node["uuid"] = id.str();

	node["minFilter"] = vkUtils::samplerFilterToString(minFilter);
	node["magFilter"] = vkUtils::samplerFilterToString(magFilter);

	node["mipmapMode"] = vkUtils::samplerMipmapModeToString(mipmapMode);

	node["addressModeU"] = vkUtils::samplerAddressModeToString(addressModeU);
	node["addressModeV"] = vkUtils::samplerAddressModeToString(addressModeV);
	node["addressModeW"] = vkUtils::samplerAddressModeToString(addressModeW);

	node["generateMipmaps"] = generateMipmaps;
	node["anisotropy"] = maxAnisotropy;
}

void grail::TextureAssetDescriptor::read(nlohmann::json& node) {
	descriptorVersion = node["version"].get<uint32_t>();
	filePath = node["path"];
	id = sole::rebuild(node["uuid"]);

	minFilter = vkUtils::samplerFilterFromString(node["minFilter"]);
	magFilter = vkUtils::samplerFilterFromString(node["magFilter"]);

	mipmapMode = vkUtils::samplerMipmapModeFromString(node["mipmapMode"]);

	addressModeU = vkUtils::samplerAddressModeFromString(node["addressModeU"]);
	addressModeV = vkUtils::samplerAddressModeFromString(node["addressModeV"]);
	addressModeW = vkUtils::samplerAddressModeFromString(node["addressModeW"]);

	generateMipmaps = (node["generateMipmaps"]);
	maxAnisotropy = (node["anisotropy"]);
}*/
