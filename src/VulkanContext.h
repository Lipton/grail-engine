#ifndef GRAIL_VULKAN_CONTEXT_H
#define GRAIL_VULKAN_CONTEXT_H

#include "Common.h"

#define VK_VALIDATE_RESULT(r) {																																				\
	VkResult result = r;																																					\
																																											\
	if (result != VK_SUCCESS) {																																				\
		GRAIL_LOG(ERROR, "VK_VALIDATE_RESULT") << "On line: " << __LINE__ << " In: " << __FILE__ << " (" << grail::vkTools::errorToString(result).c_str() << ")";			\
		throw std::runtime_error("VK_VALIDATION_FAILED");																													\
	}																																										\
}

namespace grail {
	namespace vkTools {
		std::string errorToString(VkResult result);
	}

	struct TemporaryCommandBuffer;

	struct VulkanQueue {
		VkQueue handle = VK_NULL_HANDLE;
		uint32_t index = UINT32_MAX;

		VkQueueFamilyProperties properties;
	};

	struct GPU {
		friend class VulkanContext;
	public:
		VkDevice device;
		VkPhysicalDevice physicalDevice;
		VkPhysicalDeviceProperties properties;
		VkPhysicalDeviceFeatures features;
		VkPhysicalDeviceLimits limits;
		VkPhysicalDeviceMemoryProperties memoryProperties;
		VkPhysicalDeviceSparseProperties sparseProperties;
		std::vector<VkQueueFamilyProperties> queueFamilyProperties;

		VulkanQueue graphicsQueue = {};
		VulkanQueue computeQueue = {};
		VulkanQueue transferQueue = {};
		VulkanQueue sparseBindingQueue = {};

		VkFormat depthFormat;
		VkFormat colorFormat;
		VkColorSpaceKHR colorSpace;

		uint32_t getMemoryType(uint32_t typeBits, VkFlags properties);
		VkBool32 getMemoryType(uint32_t typeBits, VkFlags properties, uint32_t* typeIndex);
		VkBool32 getSupportedDepthFormat(VkFormat* depthFormat);

		TemporaryCommandBuffer createTemporaryBuffer(VulkanQueue queue, VkCommandBufferLevel level);

		void createSemaphore(VkSemaphore* pSemaphore, uint32_t count = 1);
		void createFence(VkFence* pFence, uint32_t count = 1);
	private:
		std::unordered_map<uint32_t, VkCommandPool> poolMap;
	};

	struct TemporaryCommandBuffer {
		friend struct GPU;
	public:
		void begin();
		void end();
		void submit();
		void dispose();

		const VkCommandBuffer& getHandle();
	private:
		VkQueue queue;
		VkCommandPool pool;
		GPU gpu;
		VkCommandBuffer handle = VK_NULL_HANDLE;
	};

	class VulkanContext {
		friend class Grail;
	public:
		static class Swapchain* swapchain;

		static VkInstance instance;

		static GPU mainGPU;
		static std::vector<GPU> gpus;

		static bool hasDepth(VkFormat format);
		static bool hasStencil(VkFormat format);
		static bool isDepthStencil(VkFormat format);
	private:
		VulkanContext();
		~VulkanContext();

		bool initializeContext(struct EngineInitializationInfo& initInfo, class ApplicationWindow* window);
	};
}

#endif