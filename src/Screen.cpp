#include "Screen.h"

grail::ScreenManager & grail::Screen::getScreenManager() {
	return *screenManager;
}

grail::InputProcessor & grail::Screen::getInputProcessor() {
	return *inputProcessor;
}

void grail::Screen::innerInit() {
	if (inputProcessor == nullptr)
		inputProcessor = new InputProcessor();

	init();
}
