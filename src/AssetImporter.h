#ifndef GRAIL_ASSET_IMPORTER_H
#define GRAIL_ASSET_IMPORTER_H

#include <string>
#include <json.hpp>
#include "Asset.h"

namespace grail {
	class AssetImporter {
	public:
		virtual void importAsset(const std::string& path) = 0;
		virtual bool showImportMenu(AssetRef<Asset> asset, class ImGuiHelper* uiHelper) = 0;
		virtual ~AssetImporter() {};
	};
}

#endif

