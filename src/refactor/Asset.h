#pragma once

#include <string>
#include "../UUID.h"

namespace grailr {
	class Asset {
	public:
		const std::string& getName() const;
		const std::string& getUUIDString() const;
		const std::string& getFilePath() const;
	protected:
		std::string name;
		std::string path;

		std::string uuidString;
		grail::uuid uuid;

		virtual void dispose() = 0;
	};


}



