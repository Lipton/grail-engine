#include "Shader.h"
#include "Utils.h"
#include "ShaderLoader.h"


grail::ShaderPermutation* grail::Shader::getPermutation(uint32_t index) {
	return permutationMap[index];
}

grail::ShaderPermutation* grail::Shader::getBasePermutation() {
	return permutationMap[0];
}

grail::ShaderStageBits grail::Shader::getStage() {
	return stage;
}

grail::ShaderPermutation* grail::Shader::compilePermutation(std::vector<std::string>& defines, bool remapDescriptors) {
	ShaderPermutation* permutation = ShaderLoader::parsePermutation(this, defines, remapDescriptors);
	permutationMap.push_back(permutation);

	return permutation;
}

VkShaderModule* grail::ShaderPermutation::getModule() {
	return &handle;
}

bool grail::ShaderPermutation::hasUniformBuffer(const std::string& name) {
	for (BufferBlock& block : uniformBuffers) {
		if (block.name.compare(name) == 0) {
			return true;
		}
	}

	return false;
}

grail::BufferBlock& grail::ShaderPermutation::getUniformBuffer(const std::string& name) {
	for (BufferBlock& block : uniformBuffers) {
		if (block.name.compare(name) == 0) {
			return block;
		}
	}

	throw new exceptions::RuntimeException("Uniform buffer [" + name + "] was not found in shader");
}

grail::BufferBlockShaderVar* grail::BufferBlock::getVarByName(const std::string& name) {
	for (BufferBlockShaderVar& var : variables) {
		if (var.name.compare(name) == 0)
			return &var;
	}
	
	return nullptr;
}
