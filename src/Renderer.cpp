#include "Renderer.h"
#include "Swapchain.h"
#include "FileIO.h"
#include "PerspectiveCamera.h"
#include "ModelLoader.h"
#include "Texture.h"
#include "Resources.h"
#include "SceneNode.h"
#include "MaterialLibrary.h"
#include "Material.h"
#include "Framebuffer.h"
#include "Mesh.h"
#include "RenderChain.h"
#include "Utils.h"

#include <stack>
#include <random>

#include <json.hpp>
#include <shaderc/shaderc.hpp>

#include "imgui.h"
#include "ImGuizmo.h"

#include "MaterialParser.h"
#include "SceneLoader.h"

#include <glm/gtx/quaternion.hpp>

#include "MemoryAllocator.h"

#define START_TIMER(identifier) auto identifier = std::chrono::high_resolution_clock::now()
#define STOP_TIMER(identifier) cpuTimings[#identifier] = std::chrono::duration<double, std::milli>(std::chrono::high_resolution_clock::now() - identifier).count()

grail::Renderer::Renderer() {
	initialize();
	allocateMemory();

	{
		{
			TextureCreateInfo attachmentInfo = {};
			attachmentInfo.generateMipmaps = true;
			attachmentInfo.samplerInfo.addressModeU = SamplerAddressMode::REPEAT;
			attachmentInfo.samplerInfo.addressModeV = SamplerAddressMode::REPEAT;
			attachmentInfo.samplerInfo.magFilter = SamplerFilter::LINEAR;
			attachmentInfo.samplerInfo.minFilter = SamplerFilter::LINEAR;
			attachmentInfo.format = VK_FORMAT_R8G8B8A8_UNORM;

			{
				unsigned char data[] = { 255, 255, 255, 255 };
				Resources::createTexture("white", 1, 1, &data[0], 4, attachmentInfo);
			}

			{
				unsigned char data[] = { 0, 0, 0, 255 };
				Resources::createTexture("black", 1, 1, &data[0], 4, attachmentInfo);
			}

			{
				unsigned char data[] = { 127, 127, 127, 255 };
				Resources::createTexture("gray", 1, 1, &data[0], 4, attachmentInfo);
			}

			{
				unsigned char data[] = { 127, 127, 255, 255 };
				Resources::createTexture("bump", 1, 1, &data[0], 4, attachmentInfo);
			}

			{
				unsigned char data[] = { 255, 0, 0, 255 };
				Resources::createTexture("red", 1, 1, &data[0], 4, attachmentInfo);
			}
		}

		defaultAlbedo = Resources::getTexture("white");
		defaultNormals = Resources::getTexture("bump");
		defaultRoughness = Resources::getTexture("gray");
		defaultMetallic = Resources::getTexture("black");
	}

	createMeshes();
}

class RenderComparator {
	glm::vec3& position;

public:
	RenderComparator(glm::vec3& position) : position(position) {}

	bool operator()(grail::SceneNode* lhs, grail::SceneNode* rhs) {
		//return glm::distance(position, lhs->getWorldPosition()) < glm::distance(position, rhs->getWorldPosition());
		return 1;
	}
};

void grail::Renderer::cullRenderables(uint32_t i, std::vector<grail::Renderer::Cascade>& cascades, std::vector<grail::SceneNode*>& renderables, std::vector<grail::SceneNode*>& culledNodes, std::vector<std::vector<grail::SceneNode*>>& shadowNodes) {
	for (grail::SceneNode* node : renderables) {
		/*if (cascades[i].frustum.AABBInFrustum(node->boundsMinWorld, node->boundsMaxWorld)) {
			shadowNodes[i].push_back(node);
		}*/
	}
}

void grail::Renderer::createFramebuffers() {
	GRAIL_LOG(INFO, "RENDERER") << "Creating framebuffers...";

	uint32_t width = Graphics::getWidth();
	uint32_t height = Graphics::getHeight();

	{
		renderChain = new RenderChain();

		{
			TextureCreateInfo ai = {};
			ai.generateMipmaps = false;
			ai.samplerInfo.addressModeU = SamplerAddressMode::CLAMP_TO_EDGE;
			ai.samplerInfo.addressModeV = SamplerAddressMode::CLAMP_TO_EDGE;
			ai.samplerInfo.magFilter = SamplerFilter::LINEAR;
			ai.samplerInfo.minFilter = SamplerFilter::LINEAR;
			ai.samplerInfo.mipmapMode = SamplerMipmapMode::LINEAR;
			ai.samplerInfo.maxAnisotropy = 1.0f;
			ai.arrayLevels = shadowMapCascadeSplits;
			ai.format = VK_FORMAT_D32_SFLOAT;
			ai.samplerInfo.autoMaxLod = false;
			ai.samplerInfo.mipLodBias = 0.0f;
			ai.samplerInfo.minLod = 0.0f;
			ai.samplerInfo.maxLod = 1.0f;
			/*ai.samplerInfo.compareEnable = true;
			ai.samplerInfo.compareOp = CompareOp::LESS_OR_EQUAL;*/
			ai.usageBits = ImageUsageBits::DEPTH_STENCIL_ATTACHMENT_BIT | ImageUsageBits::SAMPLED_BIT;
            RenderStepAttachmentCreateInfo stepInfo = RenderStepAttachmentCreateInfo{ ai, shadowCascadeMapSize, shadowCascadeMapSize, ImageLayout::UNDEFINED, ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL, AttachmentLoadOp::CLEAR };
			renderChain->createAttachment("shadow_cascade_map", stepInfo);
		}

		TextureCreateInfo attachmentInfo = {};
		attachmentInfo.samplerInfo.addressModeU = SamplerAddressMode::CLAMP_TO_EDGE;
		attachmentInfo.samplerInfo.addressModeV = SamplerAddressMode::CLAMP_TO_EDGE;
		attachmentInfo.samplerInfo.magFilter = SamplerFilter::NEAREST;
		attachmentInfo.samplerInfo.minFilter = SamplerFilter::NEAREST;

		attachmentInfo.format = VK_FORMAT_D16_UNORM;
		attachmentInfo.usageBits = ImageUsageBits::DEPTH_STENCIL_ATTACHMENT_BIT | ImageUsageBits::SAMPLED_BIT;
        {
            RenderStepAttachmentCreateInfo stepInfo = RenderStepAttachmentCreateInfo{ attachmentInfo, width, height, ImageLayout::UNDEFINED, ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL };
            renderChain->createAttachment("depth_prepass_depth", stepInfo);
        }
		attachmentInfo.generateMipmaps = false;


	    RenderStepAttachmentCreateInfo stepInfo;

		attachmentInfo.format = VK_FORMAT_R32_SFLOAT;
		attachmentInfo.usageBits = ImageUsageBits::COLOR_ATTACHMENT_BIT | ImageUsageBits::SAMPLED_BIT;
		stepInfo = RenderStepAttachmentCreateInfo{ attachmentInfo, width / 2, height / 2 };
		renderChain->createAttachment("depth_halfRes", stepInfo);

		attachmentInfo.samplerInfo.magFilter = SamplerFilter::LINEAR;
		attachmentInfo.samplerInfo.minFilter = SamplerFilter::LINEAR;
		attachmentInfo.format = VK_FORMAT_R8G8B8A8_UNORM;
		attachmentInfo.usageBits = ImageUsageBits::COLOR_ATTACHMENT_BIT | ImageUsageBits::SAMPLED_BIT | ImageUsageBits::STORAGE_BIT;
		stepInfo = RenderStepAttachmentCreateInfo{ attachmentInfo, width, height };
		renderChain->createAttachment("gpass_albedo", stepInfo);

		attachmentInfo.format = VK_FORMAT_R16G16B16A16_SFLOAT;
		stepInfo = RenderStepAttachmentCreateInfo{ attachmentInfo, width, height };
		renderChain->createAttachment("gpass_normals", stepInfo);

		attachmentInfo.format = VK_FORMAT_R16G16_SFLOAT;
		stepInfo = RenderStepAttachmentCreateInfo{ attachmentInfo, width, height };
		renderChain->createAttachment("gpass_velocity", stepInfo);

		attachmentInfo.format = VK_FORMAT_R16G16B16A16_SFLOAT;
		stepInfo = RenderStepAttachmentCreateInfo{ attachmentInfo, width, height };
		renderChain->createAttachment("lightpass_accumulation", stepInfo);

		attachmentInfo.format = VK_FORMAT_R16G16B16A16_SFLOAT;
		stepInfo = RenderStepAttachmentCreateInfo{ attachmentInfo, width, height };
		renderChain->createAttachment("SSR_resolve", stepInfo);

		attachmentInfo.format = VK_FORMAT_R16G16B16A16_SFLOAT;
		stepInfo = RenderStepAttachmentCreateInfo{ attachmentInfo, width, height };
		renderChain->createAttachment("TAA_resolve0", stepInfo);

		attachmentInfo.format = VK_FORMAT_R16G16B16A16_SFLOAT;
		stepInfo = RenderStepAttachmentCreateInfo{ attachmentInfo, width, height };
		renderChain->createAttachment("TAA_resolve1", stepInfo);

		attachmentInfo.format = colorFormat;
		stepInfo = RenderStepAttachmentCreateInfo{ attachmentInfo, width, height };
		renderChain->createAttachment("ui", stepInfo);
		stepInfo = RenderStepAttachmentCreateInfo{ attachmentInfo, width, height };
		renderChain->createAttachment("present", stepInfo);

		attachmentInfo.format = colorFormat;
		stepInfo = RenderStepAttachmentCreateInfo{ attachmentInfo, 0, 0, ImageLayout::UNDEFINED, ImageLayout::PRESENT_SRC };
		renderChain->createAttachment("fxaa", stepInfo);

		attachmentInfo.format = VK_FORMAT_R8_UNORM;
		stepInfo = RenderStepAttachmentCreateInfo{ attachmentInfo, width / 2, height / 2 };
		renderChain->createAttachment("ssao", stepInfo);
		stepInfo = RenderStepAttachmentCreateInfo{ attachmentInfo, width, height };
		renderChain->createAttachment("ssao_blurred", stepInfo);

		attachmentInfo.format = VK_FORMAT_R16G16B16A16_SFLOAT;
		stepInfo =  RenderStepAttachmentCreateInfo{ attachmentInfo, 1, 1 };
		renderChain->createAttachment("SSR_uvs", stepInfo);
		stepInfo = RenderStepAttachmentCreateInfo{ attachmentInfo, width, height };
		renderChain->createAttachment("SSR_uvs_compute", stepInfo);

		attachmentInfo.format = VK_FORMAT_R32_SFLOAT;
		attachmentInfo.generateMipmaps = true;
		attachmentInfo.samplerInfo.addressModeU = SamplerAddressMode::CLAMP_TO_EDGE;
		attachmentInfo.samplerInfo.addressModeV = SamplerAddressMode::CLAMP_TO_EDGE;
		attachmentInfo.samplerInfo.magFilter = SamplerFilter::NEAREST;
		attachmentInfo.samplerInfo.minFilter = SamplerFilter::NEAREST;
		attachmentInfo.samplerInfo.mipmapMode = SamplerMipmapMode::NEAREST;
		attachmentInfo.usageBits = ImageUsageBits::COLOR_ATTACHMENT_BIT | ImageUsageBits::SAMPLED_BIT | ImageUsageBits::STORAGE_BIT;
		stepInfo = RenderStepAttachmentCreateInfo{ attachmentInfo, width, height, ImageLayout::SHADER_READ_ONLY_OPTIMAL };
		renderChain->createAttachment("depth_pyramid", stepInfo);

		RenderStepDescription description;

		renderChain->describeStep("shadow_cascade_pass", description = RenderStepDescription{ shadowCascadeMapSize, shadowCascadeMapSize, 2, 2,{ "shadow_cascade_map" }, "" });
		renderChain->describeStep("depth_prepass", description = RenderStepDescription{ width, height, 2, 2,{ "depth_prepass_depth" }, "shadow_cascade_pass" });
		renderChain->describeStep("depth_copy", description = RenderStepDescription{ width, height, 2, 2,{ "depth_pyramid" }, "depth_prepass" });
		renderChain->describeStep("depth_downsample", description = RenderStepDescription{ 1, 1, 2, 2,{ "depth_pyramid" }, "depth_copy" });
		renderChain->describeStep("gpass", description = RenderStepDescription{ width, height, 2, 2,{ "gpass_albedo",  "gpass_normals", "gpass_velocity", "depth_prepass_depth" }, "depth_downsample" });
		renderChain->describeStep("ssaopass", description = RenderStepDescription{ width / 2, height / 2, 2, 2,{ "ssao" }, "gpass" });
		renderChain->describeStep("ssaoblur", description = RenderStepDescription{ width, height, 2, 2,{ "ssao_blurred" }, "ssaopass" });
		renderChain->describeStep("lightpass", description = RenderStepDescription{ width, height, 2, 2,{ "lightpass_accumulation" }, "ssaoblur" });

		/*renderChain->describeStep("bloom_pass", RenderStepDescription{ width, height, 2, 2,{ "bloom_base" }, "lightpass" });
		renderChain->describeStep("bloom_pass_1", RenderStepDescription{ width / 2, height / 2, 2, 2,{ "bloom_1" }, "bloom_pass" });
		renderChain->describeStep("bloom_pass_2", RenderStepDescription{ width / 4, height / 4, 2, 2,{ "bloom_2" }, "bloom_pass_1" });
		renderChain->describeStep("bloom_pass_3", RenderStepDescription{ width / 8, height / 8, 2, 2,{ "bloom_3" }, "bloom_pass_2" });
		renderChain->describeStep("bloom_pass_4", RenderStepDescription{ width / 16, height / 16, 2, 2,{ "bloom_4" }, "bloom_pass_3" });*/
		//renderChain->describeStep("SSR_uv_pass", RenderStepDescription{ width, height, 1, 0,{ "SSR_uvs" }, "lightpass" });

		renderChain->describeStep("compute_test", description = RenderStepDescription{ 0, 0, 2, 2,{}, "lightpass", true });

		renderChain->describeStep("SSR_pass", description = RenderStepDescription{ width, height, 2, 2,{ "SSR_resolve" }, "compute_test" });
		renderChain->describeStep("TAA_pass", description = RenderStepDescription{ width, height, 2, 2,{ "TAA_resolve0" }, "SSR_pass" });
		renderChain->describeStep("present_pass", description = RenderStepDescription{ width, height, 2, 2,{ "present" }, "TAA_pass" });
		//renderChain->describeStep("ui_pass", RenderStepDescription{ width, height, 1, 0,{ "ui" }, "present_pass" });
		renderChain->describeStep("fxaa_pass", description = RenderStepDescription{ width, height, 2, 2,{ "fxaa" }, "present_pass" });

		renderChain->finalizeChain();

		int index = 0;
		std::stringstream ss;
		ss << "COMPUTED RENDER CHAIN ORDER: ---------------------- \n";
		ss << " -> ";
		for (std::string step : renderChain->getChainOrder()) {
			RenderStep& s = renderChain->getStep(step);
			ss << s.identifier << " ( ";

			for (auto& attachment : s.attachments) {
				ss << "[" << attachment.first << "] ";
			}

			ss << " ) -> \n";
		}

		ss << "--------------------------------------------";
		GRAIL_LOG(WARNING, "CHAIN ORDER") << ss.str();
		VkTexture* ref = renderChain->getAttachment("depth_pyramid").texture;
		TemporaryCommandBuffer tmp = mainGPU.createTemporaryBuffer(mainGPU.graphicsQueue, VK_COMMAND_BUFFER_LEVEL_PRIMARY);
		tmp.begin();
		for (int i = 1; i < ref->getMipCount(); i++) {
			VkImageSubresourceRange r = {};
			//r.aspectMask = ref->subresourceRange.aspectMask | VK_IMAGE_ASPECT_COLOR_BIT;
			r.baseMipLevel = i;
			r.levelCount = 1;
			r.layerCount = 1;
			r.baseArrayLayer = 0;

			VkImageMemoryBarrier imageMemoryBarrier = {};
			imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
			imageMemoryBarrier.pNext = nullptr;
			imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
			imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
			imageMemoryBarrier.image = ref->getImage();
			imageMemoryBarrier.subresourceRange = r;

			/*vkCmdPipelineBarrier(
				tmp.getHandle(),
				VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
				VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
				0, 
				0,
				nullptr,
				0,
				nullptr,
				1,
				&imageMemoryBarrier);

			ref->descriptorImageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;*/
		}
		tmp.end();
		tmp.submit();
		tmp.dispose();


		depthBuffers.resize(renderChain->getAttachment("depth_pyramid").texture->getMipCount());
		//depthDescriptors.resize(depthBuffers.size());
		for (int i = 1; i < depthBuffers.size(); i++) {
			VkFramebufferCreateInfo framebufferInfo = {};
			framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
			framebufferInfo.pNext = nullptr;
			framebufferInfo.flags = 0;
			framebufferInfo.renderPass = renderChain->getStep("depth_downsample").renderPass;
			framebufferInfo.attachmentCount = 1;
		//	framebufferInfo.pAttachments = &renderChain->getAttachment("depth_pyramid").texture->mipViews[i];
			framebufferInfo.width = int32_t(renderChain->getAttachment("depth_pyramid").texture->getWidth() >> (i));
			framebufferInfo.height = int32_t(renderChain->getAttachment("depth_pyramid").texture->getHeight() >> (i));
			framebufferInfo.layers = 1;
			VK_VALIDATE_RESULT(vkCreateFramebuffer(mainGPU.device, &framebufferInfo, nullptr, &depthBuffers[i]));

			/*depthDescriptors[i] = new DescriptorSetGroup(PushConstantRange{ ShaderStageBits::FRAGMENT, 0, sizeof(glm::vec2) }, std::initializer_list<DescriptorSet>{
				std::initializer_list<DescriptorBindingInfo>{ {"mip", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT } },
			});
			depthDescriptors[i]->initialize();
			depthDescriptors[i]->getBinding("mip").setDescriptor(renderChain->getAttachment("depth_pyramid").texture->getMipDescriptor(i - 1));
			depthDescriptors[i]->update();*/
		}

	/*	bloomBuffers.resize(4);
		for (uint32_t i = 0; i < 4; i++) {
			VkFramebufferCreateInfo framebufferInfo = {};
			framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
			framebufferInfo.renderPass = renderChain->getStep("bloom_pass_" + std::to_string(i + 1)).renderPass;
			framebufferInfo.pAttachments = &renderChain->getAttachment("bloom_temp").texture->mipViews[0];
			framebufferInfo.attachmentCount = 1;
			framebufferInfo.width = renderChain->getStep("bloom_pass_" + std::to_string(i + 1)).width;
			framebufferInfo.height = renderChain->getStep("bloom_pass_" + std::to_string(i + 1)).height;
			framebufferInfo.layers = 1;


			VK_VALIDATE_RESULT(vkCreateFramebuffer(VulkanContext::mainGPU.device, &framebufferInfo, nullptr, &bloomBuffers[i]));
		}*/

		// One image and framebuffer per cascade
		for (uint32_t i = 0; i < shadowMapCascadeSplits; i++) {
			// Image view for this cascade's layer (inside the depth map)
			// This view is used to render to that specific depth image layer
			VkImageViewCreateInfo viewInfo = {};
			viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
			viewInfo.pNext = nullptr;
			viewInfo.flags = 0;
			viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D_ARRAY;
			viewInfo.format = renderChain->getAttachment("shadow_cascade_map").description.format;
			viewInfo.subresourceRange = {};
			viewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
			viewInfo.subresourceRange.baseMipLevel = 0;
			viewInfo.subresourceRange.levelCount = 1;
			viewInfo.subresourceRange.baseArrayLayer = i;
			viewInfo.subresourceRange.layerCount = 1;
			viewInfo.image = renderChain->getAttachment("shadow_cascade_map").texture->getImage();
			VK_VALIDATE_RESULT(vkCreateImageView(mainGPU.device, &viewInfo, nullptr, &cascades[i].view));
			// Framebuffer
			VkFramebufferCreateInfo framebufferInfo = {};
			framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
			framebufferInfo.pNext = nullptr;
			framebufferInfo.flags = 0;
			framebufferInfo.renderPass = renderChain->getStep("shadow_cascade_pass").renderPass;
			framebufferInfo.attachmentCount = 1;
			framebufferInfo.pAttachments = &cascades[i].view;
			framebufferInfo.width = shadowCascadeMapSize;
			framebufferInfo.height = shadowCascadeMapSize;
			framebufferInfo.layers = 1;
			VK_VALIDATE_RESULT(vkCreateFramebuffer(mainGPU.device, &framebufferInfo, nullptr, &cascades[i].framebuffer));

			cascades[i].frustum = Frustum();
		}

		/*TAADescriptors.resize(2);
		presentDescriptors.resize(2);
		for (uint32_t i = 0; i < 2; i++) {
			VkFramebufferCreateInfo framebufferInfo = {};
			framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
			framebufferInfo.pNext = nullptr;
			framebufferInfo.flags = 0;
			framebufferInfo.renderPass = renderChain->getStep("TAA_pass").renderPass;
			framebufferInfo.attachmentCount = 1;
			framebufferInfo.pAttachments = &renderChain->getAttachment("TAA_resolve" + std::to_string(i)).texture->imageView;
			framebufferInfo.width = renderChain->getAttachment("TAA_resolve" + std::to_string(i)).texture->width;
			framebufferInfo.height = renderChain->getAttachment("TAA_resolve" + std::to_string(i)).texture->height;
			framebufferInfo.layers = 1;
			VK_VALIDATE_RESULT(vkCreateFramebuffer(mainGPU.device, &framebufferInfo, nullptr, &lightpassFramebuffers[i]));


			TAADescriptors[i] = new DescriptorSetGroup(PushConstantRange{ ShaderStageBits::FRAGMENT, 0, 96 }, std::initializer_list<DescriptorSet>{
				std::initializer_list<DescriptorBindingInfo>{
					{ "color", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT},
					{ "color_history", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT },
					{ "velocity", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT },
					{ "depth", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT },
				},
			});
			TAADescriptors[i]->initialize();
			TAADescriptors[i]->getBinding("color").setDescriptor(attachmentDescriptor("SSR_resolve"));
			TAADescriptors[i]->getBinding("color_history").setDescriptor(&renderChain->getAttachment("TAA_resolve" + std::to_string(1 - i)).texture->getDescriptor());
			TAADescriptors[i]->getBinding("velocity").setDescriptor(attachmentDescriptor("gpass_velocity"));
			TAADescriptors[i]->getBinding("depth").setDescriptor(attachmentDescriptor("depth_prepass_depth"));
			TAADescriptors[i]->update();

			presentDescriptors[i] = new DescriptorSetGroup(PushConstantRange{ ShaderStageBits::FRAGMENT | ShaderStageBits::VERTEX, 0, sizeof(glm::vec4) }, std::initializer_list<DescriptorSet>{
				std::initializer_list<DescriptorBindingInfo>{
					{ "color_texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT },
					{ "velocity_buffer", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT },
					{ "lut_texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT },
					/*{ "bloom1", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT },
					{ "bloom2", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT },
					{ "bloom3", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT },
					{ "bloom4", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT },*/
			/*	},
			});
			presentDescriptors[i]->initialize();
			presentDescriptors[i]->getBinding("color_texture").setDescriptor(&renderChain->getAttachment("TAA_resolve" + std::to_string(1 - i)).texture->getDescriptor());
			presentDescriptors[i]->getBinding("velocity_buffer").setDescriptor(attachmentDescriptor("gpass_velocity"));
			{
				TextureCreateInfo attachmentInfo = {};
				attachmentInfo.samplerInfo.addressModeU = SamplerAddressMode::CLAMP_TO_EDGE;
				attachmentInfo.samplerInfo.addressModeV = SamplerAddressMode::CLAMP_TO_EDGE;
				attachmentInfo.samplerInfo.magFilter = SamplerFilter::LINEAR;
				attachmentInfo.samplerInfo.minFilter = SamplerFilter::LINEAR;
				attachmentInfo.generateMipmaps = false;

				presentDescriptors[i]->getBinding("lut_texture").setDescriptor(&Resources::loadTexture("luts/warm.png", attachmentInfo)->getDescriptor());
			}
			/*presentDescriptors[i]->getBinding("bloom1").setDeawscriptor(attachmentDescriptor("bloom_1"));
			presentDescriptors[i]->getBinding("bloom2").setDescriptor(attachmentDescriptor("bloom_2"));
			presentDescriptors[i]->getBinding("bloom3").setDescriptor(attachmentDescriptor("bloom_3"));
			presentDescriptors[i]->getBinding("bloom4").setDescriptor(attachmentDescriptor("bloom_4"));*/
			/*presentDescriptors[i]->update();*/
		//}
	}

	GRAIL_LOG(INFO, "RENDERER") << "Initializing ImGui";

	VkImageView imgAttachments[1];

	VkFramebufferCreateInfo framebufferCreateInfo = {};
	framebufferCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
	framebufferCreateInfo.pNext = nullptr;
	framebufferCreateInfo.renderPass = renderChain->getStep("fxaa_pass").renderPass;
	framebufferCreateInfo.attachmentCount = 1;
	framebufferCreateInfo.pAttachments = &imgAttachments[0];
	framebufferCreateInfo.width = width;
	framebufferCreateInfo.height = height;
	framebufferCreateInfo.layers = 1;

	presentFramebuffers.resize(swapchain->imageCount);
	for (uint32_t i = 0; i < presentFramebuffers.size(); i++) {
		imgAttachments[0] = swapchain->buffers[i].view;
		VK_VALIDATE_RESULT(vkCreateFramebuffer(mainGPU.device, &framebufferCreateInfo, nullptr, &presentFramebuffers[i]));
	}

	{
		ImGui::CreateContext();
		// Color scheme
		ImGuiStyle & style = ImGui::GetStyle();

		float alpha_ = 0.75f;

		style.AntiAliasedFill = true;
		style.AntiAliasedLines = false;

		style.Alpha = 1.0f;
		style.FrameRounding = 3.0f;
		style.Colors[ImGuiCol_Text] = ImVec4(0.00f, 0.00f, 0.00f, 1.00f);
		style.Colors[ImGuiCol_TextDisabled] = ImVec4(0.60f, 0.60f, 0.60f, 1.00f);
		style.Colors[ImGuiCol_WindowBg] = ImVec4(0.94f, 0.94f, 0.94f, 0.94f);
		style.Colors[ImGuiCol_ChildWindowBg] = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
		style.Colors[ImGuiCol_PopupBg] = ImVec4(1.00f, 1.00f, 1.00f, 0.94f);
		style.Colors[ImGuiCol_Border] = ImVec4(0.00f, 0.00f, 0.00f, 0.39f);
		style.Colors[ImGuiCol_BorderShadow] = ImVec4(1.00f, 1.00f, 1.00f, 0.10f);
		style.Colors[ImGuiCol_FrameBg] = ImVec4(1.00f, 1.00f, 1.00f, 0.94f);
		style.Colors[ImGuiCol_FrameBgHovered] = ImVec4(0.26f, 0.59f, 0.98f, 0.40f);
		style.Colors[ImGuiCol_FrameBgActive] = ImVec4(0.26f, 0.59f, 0.98f, 0.67f);
		style.Colors[ImGuiCol_TitleBg] = ImVec4(0.96f, 0.96f, 0.96f, 1.00f);
		style.Colors[ImGuiCol_TitleBgCollapsed] = ImVec4(1.00f, 1.00f, 1.00f, 0.51f);
		style.Colors[ImGuiCol_TitleBgActive] = ImVec4(0.82f, 0.82f, 0.82f, 1.00f);
		style.Colors[ImGuiCol_MenuBarBg] = ImVec4(0.86f, 0.86f, 0.86f, 1.00f);
		style.Colors[ImGuiCol_ScrollbarBg] = ImVec4(0.98f, 0.98f, 0.98f, 0.53f);
		style.Colors[ImGuiCol_ScrollbarGrab] = ImVec4(0.69f, 0.69f, 0.69f, 1.00f);
		style.Colors[ImGuiCol_ScrollbarGrabHovered] = ImVec4(0.59f, 0.59f, 0.59f, 1.00f);
		style.Colors[ImGuiCol_ScrollbarGrabActive] = ImVec4(0.49f, 0.49f, 0.49f, 1.00f);
		style.Colors[ImGuiCol_CheckMark] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
		style.Colors[ImGuiCol_SliderGrab] = ImVec4(0.24f, 0.52f, 0.88f, 1.00f);
		style.Colors[ImGuiCol_SliderGrabActive] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
		style.Colors[ImGuiCol_Button] = ImVec4(0.26f, 0.59f, 0.98f, 0.40f);
		style.Colors[ImGuiCol_ButtonHovered] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
		style.Colors[ImGuiCol_ButtonActive] = ImVec4(0.06f, 0.53f, 0.98f, 1.00f);
		style.Colors[ImGuiCol_Header] = ImVec4(0.26f, 0.59f, 0.98f, 0.31f);
		style.Colors[ImGuiCol_HeaderHovered] = ImVec4(0.26f, 0.59f, 0.98f, 0.80f);
		style.Colors[ImGuiCol_HeaderActive] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
		//style.Colors[ImGuiCol] = ImVec4(0.39f, 0.39f, 0.39f, 1.00f);
		//style.Colors[ImGuiCol_ColumnHovered] = ImVec4(0.26f, 0.59f, 0.98f, 0.78f);
		//style.Colors[ImGuiCol_ColumnActive] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
		style.Colors[ImGuiCol_ResizeGrip] = ImVec4(1.00f, 1.00f, 1.00f, 0.50f);
		style.Colors[ImGuiCol_ResizeGripHovered] = ImVec4(0.26f, 0.59f, 0.98f, 0.67f);
		style.Colors[ImGuiCol_ResizeGripActive] = ImVec4(0.26f, 0.59f, 0.98f, 0.95f);
		style.Colors[ImGuiCol_PlotLines] = ImVec4(0.39f, 0.39f, 0.39f, 1.00f);
		style.Colors[ImGuiCol_PlotLinesHovered] = ImVec4(1.00f, 0.43f, 0.35f, 1.00f);
		style.Colors[ImGuiCol_PlotHistogram] = ImVec4(0.90f, 0.70f, 0.00f, 1.00f);
		style.Colors[ImGuiCol_PlotHistogramHovered] = ImVec4(1.00f, 0.60f, 0.00f, 1.00f);
		style.Colors[ImGuiCol_TextSelectedBg] = ImVec4(0.26f, 0.59f, 0.98f, 0.35f);
		style.Colors[ImGuiCol_ModalWindowDarkening] = ImVec4(0.20f, 0.20f, 0.20f, 0.35f);

		for (int i = 0; i <= ImGuiCol_COUNT; i++)
		{
			ImVec4& col = style.Colors[i];
			float H, S, V;
			ImGui::ColorConvertRGBtoHSV(col.x, col.y, col.z, H, S, V);

			if (S < 0.1f)
			{
				V = 1.0f - V;
			}
			ImGui::ColorConvertHSVtoRGB(H, S, V, col.x, col.y, col.z);
			if (col.w < 1.00f)
			{
				col.w *= alpha_;
			}
		}

		// Dimensions
		ImGuiIO& io = ImGui::GetIO();
		io.ConfigInputTextCursorBlink = true;
		io.DisplaySize = ImVec2(Graphics::getWidth(), Graphics::getHeight());
		io.DisplayFramebufferScale = ImVec2(1.0f, .0f);
		io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard | ImGuiWindowFlags_AlwaysAutoResize;
		float SCALE = 1.75f / (((1080 * 2) / Graphics::getHeight()));
		ImFontConfig cfg; cfg.SizePixels = 13 * SCALE; ImGui::GetIO().Fonts->AddFontDefault(&cfg)->DisplayOffset.y = SCALE;
		cfg.OversampleH = 10;

		io.KeyMap[ImGuiKey_Tab] = Keys::TAB;                     // Keyboard mapping. ImGui will use those indices to peek into the io.KeyDown[] array.
		io.KeyMap[ImGuiKey_LeftArrow] = Keys::LEFT;
		io.KeyMap[ImGuiKey_RightArrow] = Keys::RIGHT;
		io.KeyMap[ImGuiKey_UpArrow] = Keys::UP;
		io.KeyMap[ImGuiKey_DownArrow] = Keys::DOWN;
		io.KeyMap[ImGuiKey_PageUp] = Keys::PAGE_UP;
		io.KeyMap[ImGuiKey_PageDown] = Keys::PAGE_DOWN;
		io.KeyMap[ImGuiKey_Home] = Keys::HOME;
		io.KeyMap[ImGuiKey_End] = Keys::END;
		io.KeyMap[ImGuiKey_Delete] = 261;
		io.KeyMap[ImGuiKey_Backspace] = Keys::BACKSPACE;
		io.KeyMap[ImGuiKey_Enter] = Keys::ENTER;
		io.KeyMap[ImGuiKey_Space] = Keys::SPACE;
		io.KeyMap[ImGuiKey_Escape] = Keys::ESCAPE;
		io.KeyMap[ImGuiKey_A] = Keys::A;
		io.KeyMap[ImGuiKey_C] = Keys::C;
		io.KeyMap[ImGuiKey_V] = Keys::V;
		io.KeyMap[ImGuiKey_X] = Keys::X;
		io.KeyMap[ImGuiKey_Y] = Keys::Y;
		io.KeyMap[ImGuiKey_Z] = Keys::Z;

		// Create font texture
		unsigned char* fontData;
		int texWidth, texHeight;
		io.Fonts->GetTexDataAsRGBA32(&fontData, &texWidth, &texHeight);
		VkDeviceSize uploadSize = texWidth * texHeight * 4 * sizeof(char);

		TextureCreateInfo attachmentInfo = {};
		attachmentInfo.generateMipmaps = false;
		attachmentInfo.samplerInfo.addressModeU = SamplerAddressMode::CLAMP_TO_EDGE;
		attachmentInfo.samplerInfo.addressModeV = SamplerAddressMode::CLAMP_TO_EDGE;
		attachmentInfo.samplerInfo.magFilter = SamplerFilter::NEAREST;
		attachmentInfo.samplerInfo.minFilter = SamplerFilter::NEAREST;
		attachmentInfo.format = VK_FORMAT_R8G8B8A8_UNORM;
		VkTexture* imguiFontTexture = Resources::createTexture("imguiFont", texWidth, texHeight, fontData, uploadSize, attachmentInfo);
		imguiFontTexture->getDescriptor().imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		{
			TemporaryCommandBuffer temp = mainGPU.createTemporaryBuffer(mainGPU.graphicsQueue, VK_COMMAND_BUFFER_LEVEL_PRIMARY);
			temp.begin();

			VkImageSubresourceRange r = {};
			r.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			r.baseArrayLayer = 0;
			r.baseMipLevel = 0;
			r.layerCount = 1;
			r.levelCount = 1;

			imguiFontTexture->setImageLayout(temp.getHandle(), VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, r);

			temp.end();
			temp.submit();
			temp.dispose();
		}

		int quadCount = 100000;

		createBuffer("imguiVerts", sizeof(ImDrawVert) * quadCount, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT);
		createBuffer("imguiIndices", sizeof(ImDrawIdx) * quadCount * 6, VK_BUFFER_USAGE_INDEX_BUFFER_BIT);

		VertexLayout* guiLayout = new VertexLayout({
			VertexAttribute(VertexAttributeType::POSITION, VK_FORMAT_R32G32_SFLOAT, 2, sizeof(float)),
			VertexAttribute(VertexAttributeType::UV, VK_FORMAT_R32G32_SFLOAT, 2, sizeof(float)),
			VertexAttribute(VertexAttributeType::COLOR, VK_FORMAT_R8G8B8A8_UNORM, 3, sizeof(uint32_t), true)
			});

		PushConstantRange pushConstantRange = {};
		pushConstantRange.stageFlags = ShaderStageBits::VERTEX;
		pushConstantRange.size = sizeof(glm::vec4);
		pushConstantRange.offset = 0;

		GraphicsPipelineCreateInfo pipelineInfo = {};
		pipelineInfo.colorBlendState.attachmentCount = 1;
		pipelineInfo.renderPass = renderChain->getStep("fxaa_pass").renderPass;
		/*pipelineInfo.shaderStageState.set(ShaderStageBits::VERTEX, Resources::loadShader("shaders/ui.vert.spv"));
		pipelineInfo.shaderStageState.set(ShaderStageBits::FRAGMENT, Resources::loadShader("shaders/ui.frag.spv"));*/
		pipelineInfo.rasterizationState.cullMode = CullMode::BACK;

		pipelineInfo.colorBlendState.blendEnable = VK_TRUE;
		pipelineInfo.colorBlendState.srcColorBlendFactor = BlendFactor::SRC_ALPHA;
		pipelineInfo.colorBlendState.dstColorBlendFactor = BlendFactor::ONE_MINUS_SRC_ALPHA;
		pipelineInfo.colorBlendState.srcAlphaBlendFactor = BlendFactor::ONE_MINUS_SRC_ALPHA;
		pipelineInfo.colorBlendState.dstAlphaBlendFactor = BlendFactor::ZERO;

		/*GRAIL_LOG(INFO, "IMGUI DESCRIPTORS");
		for (int i = 0; i < imguiDescriptors.size(); i++) {
			imguiDescriptors[i] = new DescriptorSetGroup(PushConstantRange{ ShaderStageBits::VERTEX, 0, sizeof(glm::vec4) }, std::initializer_list<DescriptorSet>{
				std::initializer_list<DescriptorBindingInfo>{ {"texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT } },
			});
			imguiDescriptors[i]->initialize();
			imguiDescriptors[i]->getSet(0).getBinding(0).setDescriptor(&imguiFontTexture->getDescriptor());
			imguiDescriptors[i]->update();
		}

		DescriptorSetGroup g = DescriptorSetGroup();

		MaterialCreateInfo createInfo = {
			guiLayout,
			DescriptorSetGroup(pushConstantRange,{
				{
					{ "texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT }
				}
				}),
			pipelineInfo,
			pipelineFactory,
			MaterialType::MAT_DEFERRED
		};
		Material* material = new Material(createInfo);
		material->getDescriptors().getSet(0).getBinding(0).setDescriptor(&imguiFontTexture->getDescriptor());
		material->getDescriptors().update();

		materials["ui_pass"] = material;*/
	}
}

void grail::Renderer::render(SceneNode** rootNode, grail::Camera* camera, float delta) {
	START_TIMER(sort_and_cull);
	frameCount += 1;
	passed_time += delta * 1.0;

	if (!currentRoot)
		currentRoot = *rootNode;

	baseRoot = rootNode;

	this->camera = camera;
	this->camera->update(!freezeFrustum);
	
	if (debugShadows) {
		this->camera->combined = uboVS.cascadeMatrices[shadowCascadeIndex];
	}

	std::vector<SceneNode*> renderables;

	(*rootNode)->updateTransform(true);

	std::stack<SceneNode*> nodes;
	nodes.push(*rootNode);

	while (!nodes.empty()) {
		SceneNode* node = nodes.top();
		nodes.pop();

		//Mesh* mesh = node->mesh;

	//	if (mesh && (node->materials[0])) {
		//	renderables.push_back(node);
		//}

		for (uint32_t i = 0; i < node->getChildCount(); i++)
			nodes.push(node->getChildByIndex(i));
	}

	std::vector<SceneNode*> culledNodes;
	std::vector<std::vector<SceneNode*>> shadowNodes;
	shadowNodes.resize(activeCascades);

	for (uint32_t i = 0; i < activeCascades; i++) {
		if (!freezeFrustum) {
			cascades[i].frustum.update(cascades[i].combinedMat);
		}
	}

	if (cull) {
		if (multithreadedCull) {
			uint32_t threadIndex = 0;

			for (uint32_t i = 0; i < activeCascades; i++) {
				threadPool->threads[i]->addJob([&, i] {
					for (grail::SceneNode* node : renderables) {
						/*if (cascades[i].frustum.AABBInFrustum(node->boundsMinWorld, node->boundsMaxWorld)) {
							shadowNodes[i].push_back(node);
						}*/
					}
				});
			}

			for (SceneNode* node : renderables) {
				/*if (camera->frustum.AABBInFrustum(node->boundsMinWorld, node->boundsMaxWorld)) {
					culledNodes.push_back(node);
				}*/
			}

			threadPool->wait();
		}
		else {
			for (SceneNode* node : renderables) {
				/*if (camera->frustum.AABBInFrustum(node->boundsMinWorld, node->boundsMaxWorld)) {
					culledNodes.push_back(node);
				}*/
			}

			for (uint32_t i = 0; i < activeCascades; i++) {
				for (SceneNode* node : renderables) {
					/*if (cascades[i].frustum.AABBInFrustum(node->boundsMinWorld, node->boundsMaxWorld)) {
						shadowNodes[i].push_back(node);
					}*/
				}
			}
		}
	}
	else {
		for (SceneNode* node : renderables) {
			culledNodes.push_back(node);
		}

		for (uint32_t i = 0; i < activeCascades; i++) {
			for (SceneNode* node : renderables) {
				shadowNodes[i].push_back(node);
			}
		}
	}
	STOP_TIMER(sort_and_cull);
	
	//SHADOW
	START_TIMER(cascade_pass);
	renderChain->frameIndex = inflightFrameIndex;
	RenderStep* step = renderChain->beginChain();
	{
		step->defaultSubmitInfo.waitSemaphoreCount = 1;
		step->defaultSubmitInfo.pWaitSemaphores = &renderCompleteSemaphores[inflightFrameIndex];



		VkCommandBuffer cmdBuff = step->cmdBuffers[inflightFrameIndex];

		VkCommandBufferBeginInfo cmdBufferBeginInfo = {};
		cmdBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		cmdBufferBeginInfo.pNext = nullptr;
		cmdBufferBeginInfo.flags = 0;
		cmdBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

		VkClearValue clearValues[1];
		clearValues[0].depthStencil = { 1.0f, 0 };

		VkRenderPassBeginInfo renderPassBeginInfo = {};
		renderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
		renderPassBeginInfo.pNext = nullptr;
		renderPassBeginInfo.renderPass = step->renderPass;
		renderPassBeginInfo.renderArea.offset.x = 0;
		renderPassBeginInfo.renderArea.offset.y = 0;
		renderPassBeginInfo.renderArea.extent.width = shadowCascadeMapSize;
		renderPassBeginInfo.renderArea.extent.height = shadowCascadeMapSize;
		renderPassBeginInfo.clearValueCount = 1;
		renderPassBeginInfo.pClearValues = clearValues;

		step->beginStep(cmdBuff);

		VkViewport viewport = {};
		viewport.width = (float)shadowCascadeMapSize;
		viewport.height = (float)shadowCascadeMapSize;
		viewport.minDepth = (float) 0.0f;
		viewport.maxDepth = (float) 1.0f;

		vkCmdSetViewport(cmdBuff, 0, 1, &viewport);


		VkRect2D scissor = {};
		scissor.extent.width = (float)shadowCascadeMapSize;
		scissor.extent.height = (float)shadowCascadeMapSize;
		scissor.offset.x = 0;
		scissor.offset.y = 0;

		vkCmdSetScissor(cmdBuff, 0, 1, &scissor);

		// One pass per cascade
		// The layer that this pass renders too is defined by the cascade's image view (selected via the cascade's decsriptor set)

		for (uint32_t i = 0; i < activeCascades; i++) {
			renderPassBeginInfo.framebuffer = cascades[i].framebuffer;
			vkCmdBeginRenderPass(cmdBuff, &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);
		

			for (SceneNode* node : shadowNodes[i]) {
				//Material* material = node->materials[1];

				//*(((float*)node->materialMemory) + 32) = i;
				//*(((float*)node->materialMemory) + 33) = i;

				//vkCmdBindPipeline(cmdBuff, VK_PIPELINE_BIND_POINT_GRAPHICS, material->getPipeline());

				//vkCmdBindDescriptorSets(cmdBuff, VK_PIPELINE_BIND_POINT_GRAPHICS, material->getDescriptors().getPipelineLayout(), 0, material->getDescriptors().getSetSize(), material->getDescriptors().getSetPtr(), 0, nullptr);
				
				//vkCmdBindVertexBuffers(cmdBuff, 0, 1, &node->mesh->buffer, &node->mesh->vertexOffset);
				//vkCmdBindIndexBuffer(cmdBuff, node->mesh->buffer, node->mesh->indexOffset, VK_INDEX_TYPE_UINT32);

				//vkCmdPushConstants(cmdBuff, material->getDescriptors().getPipelineLayout(), static_cast<VkShaderStageFlagBits>(material->getPushConstants().stageFlags), material->getPushConstants().offset, material->getPushConstants().size, node->materialMemory);

				//vkCmdDrawIndexed(cmdBuff, node->mesh->indicesCount, 1, 0, 0, 1);
			}

			vkCmdEndRenderPass(cmdBuff);
		}


		step->endStep(cmdBuff);
		//step->submitStep(mainGPU.graphicsQueue.handle);
	}
	STOP_TIMER(cascade_pass);

	// DEPTH
	START_TIMER(depth_prepass);
	step = renderChain->getNextStep();
	{
		VkCommandBuffer cmdBuff = step->cmdBuffers[inflightFrameIndex];
		step->setupStep(cmdBuff);
		{

			//Material* material = depthPrepassMaterial;

			//vkCmdBindPipeline(cmdBuff, VK_PIPELINE_BIND_POINT_GRAPHICS, material->getPipeline());
		

			for (SceneNode* node : culledNodes) {
			//	Material* material = node->materials[2];

				//vkCmdBindPipeline(cmdBuff, VK_PIPELINE_BIND_POINT_GRAPHICS, material->getPipeline());
				//vkCmdBindDescriptorSets(cmdBuff, VK_PIPELINE_BIND_POINT_GRAPHICS, material->getDescriptors().getPipelineLayout(), 0, material->getDescriptors().getSetSize(), material->getDescriptors().getSetPtr(), 0, nullptr);
				
				//vkCmdBindVertexBuffers(cmdBuff, 0, 1, &node->mesh->buffer, &node->mesh->vertexOffset);
				//vkCmdBindIndexBuffer(cmdBuff, node->mesh->buffer, node->mesh->indexOffset, VK_INDEX_TYPE_UINT32);

				//if (material->getPushConstants().size > 0)
					//vkCmdPushConstants(cmdBuff, material->getDescriptors().getPipelineLayout(), static_cast<VkShaderStageFlagBits>(material->getPushConstants().stageFlags), material->getPushConstants().offset, material->getPushConstants().size, node->materialMemory);

			//	vkCmdDrawIndexed(cmdBuff, node->mesh->indicesCount, 1, 0, 0, 1);
			}
		}
		step->endStep(cmdBuff);
	}
	STOP_TIMER(depth_prepass);

	//GRAIL_LOG(INFO, "RENDERER") << "DEPTH COPY";
	START_TIMER(depth_copy);
	step = renderChain->getNextStep();
	{
		VkCommandBuffer cmdBuff = step->cmdBuffers[inflightFrameIndex];
		step->setupStep(cmdBuff);
		{
			renderMesh(cmdBuff, fullscreenMesh, materials[step->identifier], nullptr);
		}
		step->endStep(cmdBuff);
	}
	STOP_TIMER(depth_copy);

	//GRAIL_LOG(INFO, "RENDERER") << "DEPTH DOWNSAMPLE";
	START_TIMER(depth_downsample);
	step = renderChain->getNextStep();
	{
		VkCommandBuffer cmdBuff = step->cmdBuffers[inflightFrameIndex];

		VkTexture* ref = renderChain->getAttachment("depth_pyramid").texture;

		step->beginStep(cmdBuff);

		for (int i = 1; i < ref->getMipCount(); i++) {
			VkCommandBufferBeginInfo cmdBufferBeginInfo = {};
			cmdBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
			cmdBufferBeginInfo.pNext = nullptr;
			cmdBufferBeginInfo.flags = 0;
			cmdBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

			//VK_VALIDATE_RESULT(vkBeginCommandBuffer(cmdBuff, &cmdBufferBeginInfo));

			VkFramebuffer buffer = depthBuffers[i];

			step->defaultRenderPassBeginInfo.renderArea.extent.width = int32_t(ref->getWidth() >> i);
			step->defaultRenderPassBeginInfo.renderArea.extent.height = int32_t(ref->getHeight() >> i);
			step->defaultRenderPassBeginInfo.framebuffer = depthBuffers[i];

			step->defaultViewport.width = int32_t(ref->getWidth() >> i);
			step->defaultViewport.height = int32_t(ref->getHeight() >> i);

			step->defaultScissor.extent.width = int32_t(ref->getWidth() >> i);
			step->defaultScissor.extent.height = int32_t(ref->getHeight() >> i);

			vkCmdSetViewport(cmdBuff, 0, 1, &step->defaultViewport);
			vkCmdSetScissor(cmdBuff, 0, 1, &step->defaultScissor);

			vkCmdBeginRenderPass(cmdBuff, &step->defaultRenderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

			glm::vec2 resolution = glm::vec2(int32_t(ref->getWidth() >> i), int32_t(ref->getHeight() >> i));
			//renderMesh(cmdBuff, fullscreenMesh, materials[step->identifier], &resolution);


			vkCmdBindPipeline(cmdBuff, VK_PIPELINE_BIND_POINT_GRAPHICS, materials[step->identifier]->getPipeline());

			vkCmdBindVertexBuffers(cmdBuff, 0, 1, &fullscreenMesh->buffer, &fullscreenMesh->vertexOffset);
			vkCmdBindIndexBuffer(cmdBuff, fullscreenMesh->buffer, fullscreenMesh->indexOffset, VK_INDEX_TYPE_UINT32);

			//vkCmdBindDescriptorSets(cmdBuff, VK_PIPELINE_BIND_POINT_GRAPHICS, depthDescriptors[i]->getPipelineLayout(), 0, depthDescriptors[i]->getSetSize(), depthDescriptors[i]->getSetPtr(), 0, nullptr);

			/*if (materials[step->identifier]->getPushConstants().size > 0)
				vkCmdPushConstants(cmdBuff, depthDescriptors[i]->getPipelineLayout(), static_cast<VkShaderStageFlagBits>(materials[step->identifier]->getPushConstants().stageFlags), materials[step->identifier]->getPushConstants().offset, materials[step->identifier]->getPushConstants().size, &resolution);*/

			vkCmdDrawIndexed(cmdBuff, fullscreenMesh->indicesCount, 1, 0, 0, 1);

			vkCmdEndRenderPass(cmdBuff);

			VkSubmitInfo submitInfo = {};
			submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
			submitInfo.commandBufferCount = 1;
			submitInfo.pCommandBuffers = &cmdBuff;

			VkImageSubresourceRange r = {};
			//r.aspectMask = ref->subresourceRange.aspectMask | VK_IMAGE_ASPECT_COLOR_BIT;
			r.baseMipLevel = 0;
			r.levelCount = VK_REMAINING_MIP_LEVELS;
			r.layerCount = 1;
			r.baseArrayLayer = 0;

			VkImageMemoryBarrier imageMemoryBarrier = {};
			imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
			imageMemoryBarrier.pNext = nullptr;
			imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
			imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
			imageMemoryBarrier.image = ref->getImage();
			imageMemoryBarrier.subresourceRange = r;

			vkCmdPipelineBarrier(
				cmdBuff,
				VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
				VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
				0,
				0,
				nullptr,
				0,
				nullptr,
				1,
				&imageMemoryBarrier);


			//VK_VALIDATE_RESULT(vkEndCommandBuffer(cmdBuff));

			//vkQueueSubmit(mainGPU.graphicsQueue.handle, 1, &submitInfo, nullptr);
			//VK_VALIDATE_RESULT(vkQueueWaitIdle(mainGPU.graphicsQueue.handle));
		}
		step->endStep(cmdBuff);
	}
	STOP_TIMER(depth_downsample);

	// GPASS
	START_TIMER(gpass);
	step = renderChain->getNextStep(); 
	{
		VkCommandBuffer cmdBuff = step->cmdBuffers[inflightFrameIndex];
		step->setupStep(cmdBuff);
		{
			for (SceneNode* node : culledNodes) {
				//Material* material = node->materials[0];

				//vkCmdBindPipeline(cmdBuff, VK_PIPELINE_BIND_POINT_GRAPHICS, material->getPipeline());

				//vkCmdBindVertexBuffers(cmdBuff, 0, 1, &node->mesh->buffer, &node->mesh->vertexOffset);
				//vkCmdBindIndexBuffer(cmdBuff, node->mesh->buffer, node->mesh->indexOffset, VK_INDEX_TYPE_UINT32);

				//vkCmdBindDescriptorSets(cmdBuff, VK_PIPELINE_BIND_POINT_GRAPHICS, material->getDescriptors().getPipelineLayout(), 0, material->getDescriptors().getSetSize(), material->getDescriptors().getSetPtr(), 0, nullptr);

				//if (material->getPushConstants().size > 0)
					//vkCmdPushConstants(cmdBuff, material->getDescriptors().getPipelineLayout(), static_cast<VkShaderStageFlagBits>(material->getPushConstants().stageFlags), material->getPushConstants().offset, material->getPushConstants().size, node->materialMemory);

				//vkCmdDrawIndexed(cmdBuff, node->mesh->indicesCount, 1, 0, 0, 1);
			}
		}
		step->endStep(cmdBuff);
	}
	STOP_TIMER(gpass);

	//GRAIL_LOG(INFO, "RENDERER") << "SSAO";
	// SSAO PASS
	START_TIMER(ssao_pass);
	step = renderChain->getNextStep();
	{
		VkCommandBuffer cmdBuff = step->cmdBuffers[inflightFrameIndex];
		step->setupStep(cmdBuff);
		{
			glm::vec2 uvScale = glm::vec2(renderChain->getAttachment("ssao").width / Resources::getTexture("ssaoNoise")->getWidth(), renderChain->getAttachment("ssao").height / Resources::getTexture("ssaoNoise")->getHeight());

			renderMesh(cmdBuff, fullscreenMesh, materials[step->identifier], &uvScale);
		}
		step->endStep(cmdBuff);
	}
	STOP_TIMER(ssao_pass);

	//GRAIL_LOG(INFO, "RENDERER") << "SSAO BLUR";
	// SSAO BLUR PASS
	START_TIMER(ssao_blur_pass);
	step = renderChain->getNextStep();
	{
		VkCommandBuffer cmdBuff = step->cmdBuffers[inflightFrameIndex];
		step->setupStep(cmdBuff);
		{
			glm::vec2 texelSize = glm::vec2(Graphics::getWidth(), Graphics::getHeight());

			renderMesh(cmdBuff, fullscreenMesh, materials[step->identifier], &texelSize);
		}

		/*{
			glm::vec2 texelSize = glm::vec2(0.0, renderChain->getAttachment("ssao").height);

			renderMesh(cmdBuff, fullscreenMesh, materials[step->identifier], &texelSize);
		}*/
		step->endStep(cmdBuff);
	}
	STOP_TIMER(ssao_blur_pass);
	
	//GRAIL_LOG(INFO, "RENDERER") << "LIGHT";
	// LIGHT PASS
	START_TIMER(light_pass);
	step = renderChain->getNextStep();
	{
		VkCommandBuffer cmdBuff = step->cmdBuffers[inflightFrameIndex];

		step->setupStep(cmdBuff);
		{
			glm::vec4 texelSize = glm::vec4(envScaleIBL * envScaleGlobal, envScale * envScaleGlobal, envColorFunc, envSource);

			renderMesh(cmdBuff, fullscreenMesh, materials[step->identifier], &texelSize);
		}
		step->endStep(cmdBuff);
	}
	STOP_TIMER(light_pass);


	START_TIMER(ssr_pass);
	//GRAIL_LOG(INFO, "RENDERER") << "COMPUTE";
	step = renderChain->getNextStep();
	{
		VkCommandBuffer cmdBuff = step->cmdBuffers[inflightFrameIndex];
		Material* mat = materials[step->identifier];
		step->beginStep(cmdBuff);
		{
			vkCmdBindPipeline(cmdBuff, VK_PIPELINE_BIND_POINT_COMPUTE, mat->getPipeline());
			//vkCmdBindDescriptorSets(cmdBuff, VK_PIPELINE_BIND_POINT_COMPUTE, mat->getDescriptors().getPipelineLayout(), 0, mat->getDescriptors().getSetSize(), mat->getDescriptors().getSetPtr(), 0, nullptr);

			if(enableSSR)
				vkCmdDispatch(cmdBuff, (Graphics::getWidth() / 32) + 1, (Graphics::getHeight() / 32) + 1, 1);
		}
		step->endStep(cmdBuff);
	}
	STOP_TIMER(ssr_pass);

	//GRAIL_LOG(INFO, "RENDERER") << "SSR";
	// SSR PASS
	START_TIMER(ssr_resolve_pass);
	step = renderChain->getNextStep();
	{
		VkCommandBuffer cmdBuff = step->cmdBuffers[inflightFrameIndex];

		step->setupStep(cmdBuff);
		{
			float ssr = enableSSR ? 1.0f : 0.0f;

			renderMesh(cmdBuff, fullscreenMesh, materials[step->identifier], &ssr);
		}
		step->endStep(cmdBuff);
	}
	STOP_TIMER(ssr_resolve_pass);

	//GRAIL_LOG(INFO, "RENDERER") << "TAA";
	// TAA PASS
	START_TIMER(taa_pass);
	step = renderChain->getNextStep();
	{
		/*materials[step->identifier]->getDescriptors().getBinding("color_history").setDescriptor(&renderChain->getAttachment("TAA_resolve" + std::to_string(lightpassFramebufferIndex)).texture->descriptorImageInfo);
		materials[step->identifier]->update();*/

		VkCommandBuffer cmdBuff = step->cmdBuffers[inflightFrameIndex];

		step->defaultRenderPassBeginInfo.framebuffer = lightpassFramebuffers[lightpassFramebufferIndex = (lightpassFramebufferIndex + 1) % 2];

		Material* material = materials[step->identifier];

		step->setupStep(cmdBuff);
		{
			struct {
				glm::vec4 texelSize;
				glm::vec2 jitter;
				glm::mat4 projection;
				glm::vec2 sinTime;
			} pass = {};

			pass.texelSize = glm::vec4(1.0 / Graphics::getWidth(), 1.0 / Graphics::getHeight(), taa ? 1.0 : 0.0, (float)taaMode);
			pass.jitter = taaJitter;
			pass.projection = camera->projection;
			pass.sinTime.x = glm::sin(passed_time);

			vkCmdBindPipeline(cmdBuff, VK_PIPELINE_BIND_POINT_GRAPHICS, material->getPipeline());
			vkCmdBindVertexBuffers(cmdBuff, 0, 1, &fullscreenMesh->buffer, &fullscreenMesh->vertexOffset);

			vkCmdBindIndexBuffer(cmdBuff, fullscreenMesh->buffer, fullscreenMesh->indexOffset, VK_INDEX_TYPE_UINT32);
			//vkCmdBindDescriptorSets(cmdBuff, VK_PIPELINE_BIND_POINT_GRAPHICS, TAADescriptors[lightpassFramebufferIndex]->getPipelineLayout(), 0, TAADescriptors[lightpassFramebufferIndex]->getSetSize(), TAADescriptors[lightpassFramebufferIndex]->getSetPtr(), 0, nullptr);
			/*if (material->getPushConstants().size > 0)
				vkCmdPushConstants(cmdBuff, TAADescriptors[lightpassFramebufferIndex]->getPipelineLayout(), VK_SHADER_STAGE_FRAGMENT_BIT, material->getPushConstants().offset, material->getPushConstants().size, &pass);*/
			
			vkCmdDrawIndexed(cmdBuff, fullscreenMesh->indicesCount, 1, 0, 0, 1);
		}
		step->endStep(cmdBuff);
	}
	STOP_TIMER(taa_pass);

	//GRAIL_LOG(INFO, "RENDERER") << "PRESENT";
	START_TIMER(tonemap_pass);
	step = renderChain->getNextStep();
	{
		/*materials[step->identifier]->getDescriptors().getBinding("color_texture").setDescriptor(&renderChain->getAttachment("TAA_resolve" + std::to_string(lightpassFramebufferIndex)).texture->descriptorImageInfo);
		materials[step->identifier]->update();*/

		Material* material = materials[step->identifier];

		VkCommandBuffer cmdBuff = step->cmdBuffers[inflightFrameIndex];
		step->setupStep(cmdBuff);
		{
			glm::vec4 texelSize = glm::vec4(1.0 / Graphics::getWidth(), 1.0 / Graphics::getHeight(), (float)Graphics::getSmoothedFPS() / 60.0, enableLUT ? 1.0 : 0.0);

			vkCmdBindPipeline(cmdBuff, VK_PIPELINE_BIND_POINT_GRAPHICS, material->getPipeline());
			vkCmdBindVertexBuffers(cmdBuff, 0, 1, &fullscreenMesh->buffer, &fullscreenMesh->vertexOffset);

			vkCmdBindIndexBuffer(cmdBuff, fullscreenMesh->buffer, fullscreenMesh->indexOffset, VK_INDEX_TYPE_UINT32);
			//vkCmdBindDescriptorSets(cmdBuff, VK_PIPELINE_BIND_POINT_GRAPHICS, presentDescriptors[lightpassFramebufferIndex]->getPipelineLayout(), 0, presentDescriptors[lightpassFramebufferIndex]->getSetSize(), presentDescriptors[lightpassFramebufferIndex]->getSetPtr(), 0, nullptr);
			/*if (material->getPushConstants().size > 0)
				vkCmdPushConstants(cmdBuff, presentDescriptors[lightpassFramebufferIndex]->getPipelineLayout(), static_cast<VkShaderStageFlagBits>(material->getPushConstants().stageFlags), material->getPushConstants().offset, material->getPushConstants().size, &texelSize);*/

			vkCmdDrawIndexed(cmdBuff, fullscreenMesh->indicesCount, 1, 0, 0, 1);
		}
		step->endStep(cmdBuff);
	}
	STOP_TIMER(tonemap_pass);

	//GRAIL_LOG(INFO, "RENDERER") << "FXAA";
	// PRESENT PASS
	START_TIMER(fxaa_pass);
	step = renderChain->getNextStep();
	//GRAIL_LOG(INFO, "HM?") << step->identifier;
	{
		step->defaultRenderPassBeginInfo.framebuffer = presentFramebuffers[currentSwapchainBuffer];
		step->defaultSubmitInfo.pSignalSemaphores = &renderCompleteSemaphores[inflightFrameIndex];

		VkCommandBuffer cmdBuff = step->cmdBuffers[inflightFrameIndex];
		step->setupStep(cmdBuff);
		{
			glm::vec4 texelSize = glm::vec4(Graphics::getWidth(), Graphics::getHeight(), sharpen ? 1.0 : 0.0, sharpenFactor);

			renderMesh(cmdBuff, fullscreenMesh, materials[step->identifier], &texelSize);

			if(renderUI) {
				ImGuiIO& io = ImGui::GetIO();
	
				ImGui::NewFrame();
				ImGuizmo::BeginFrame();

				ImGuizmo::SetRect(0, 0, io.DisplaySize.x, io.DisplaySize.y);

				drawUI();
				ImGui::EndFrame();
				ImGui::Render();

				for (int i = 0; i < 512; i++) {
					if(i != 341 || i != 340 || i != 342)
						io.KeysDown[i] = false;
				}

				io.DisplaySize = ImVec2((float)Graphics::getWidth(), (float)Graphics::getHeight());
				io.DeltaTime = Graphics::getDeltaTime();

				io.MousePos = ImVec2(Input::getMouseX(), Input::getMouseY());
				io.MouseDown[0] = Input::isButtonPressed(Buttons::LEFT);
				io.MouseDown[1] = Input::isButtonPressed(Buttons::RIGHT);
				io.MouseWheel = Input::getMouseScrollY();
				io.MouseWheelH = Input::getMouseScrollY();

				//GRAIL_LOG(INFO, "RENDERER") << "UI";
				//step = renderChain->getNextStep();
				{

					Material* material = materials["ui_pass"];

					//VkCommandBuffer cmdBuff = step->cmdBuffers[0];
					//step->setupStep(cmdBuff);
					{
						ImGuiIO& io = ImGui::GetIO();

						vkCmdBindPipeline(cmdBuff, VK_PIPELINE_BIND_POINT_GRAPHICS, material->getPipeline());

						//vkCmdBindVertexBuffers(commandBuffer, 0, 1, &mesh->buffer, &mesh->vertexOffset);
						//vkCmdBindIndexBuffer(commandBuffer, mesh->buffer, mesh->indexOffset, VK_INDEX_TYPE_UINT32);
						//vkCmdDrawIndexed(commandBuffer, mesh->indicesCount, 1, 0, 0, 1);


						VkViewport viewport = {};
						viewport.width = ImGui::GetIO().DisplaySize.x;
						viewport.height = ImGui::GetIO().DisplaySize.y;
						viewport.minDepth = 0.0f;
						viewport.maxDepth = 1.0f;
						vkCmdSetViewport(cmdBuff, 0, 1, &viewport);

						// UI scale and translate via push constants
						glm::vec4 push = glm::vec4(2.0f /io.DisplaySize.x, 2.0f / io.DisplaySize.y, -1.0f, -1.0f);
						//vkCmdPushConstants(cmdBuff, material->getDescriptors().getPipelineLayout(), static_cast<VkShaderStageFlagBits>(material->getPushConstants().stageFlags), material->getPushConstants().offset, material->getPushConstants().size, &push);

						// Render commands
						ImDrawData* imDrawData = ImGui::GetDrawData();
						int32_t vertexOffset = 0;
						int32_t indexOffset = 0;

						if (imDrawData->CmdListsCount > 0) {

							VkDeviceSize offsets[1] = { 0 };
							vkCmdBindVertexBuffers(cmdBuff, 0, 1, &uniformBuffers["imguiVerts"].handle, offsets);
							vkCmdBindIndexBuffer(cmdBuff, uniformBuffers["imguiIndices"].handle, 0, VK_INDEX_TYPE_UINT16);

							for (int32_t i = 0; i < imDrawData->CmdListsCount; i++)
							{
								const ImDrawList* cmd_list = imDrawData->CmdLists[i];
								for (int32_t j = 0; j < cmd_list->CmdBuffer.Size; j++)
								{
									const ImDrawCmd* pcmd = &cmd_list->CmdBuffer[j];

								/*if (pcmd->TextureId) {
										DescriptorSetGroup* group = imguiDescriptors[(size_t)pcmd->TextureId];

										vkCmdBindDescriptorSets(cmdBuff, VK_PIPELINE_BIND_POINT_GRAPHICS, group->getPipelineLayout(), 0, group->getSetSize(), group->getSetPtr(), 0, nullptr);
									}
									else {
										DescriptorSetGroup* group = imguiDescriptors[0];

										vkCmdBindDescriptorSets(cmdBuff, VK_PIPELINE_BIND_POINT_GRAPHICS, group->getPipelineLayout(), 0, group->getSetSize(), group->getSetPtr(), 0, nullptr);
									}*/

									VkRect2D scissorRect;
									scissorRect.offset.x = std::max((int32_t)(pcmd->ClipRect.x), 0);
									scissorRect.offset.y = std::max((int32_t)(pcmd->ClipRect.y), 0);
									scissorRect.extent.width = (uint32_t)(pcmd->ClipRect.z - pcmd->ClipRect.x);
									scissorRect.extent.height = (uint32_t)(pcmd->ClipRect.w - pcmd->ClipRect.y);
									vkCmdSetScissor(cmdBuff, 0, 1, &scissorRect);

									vkCmdDrawIndexed(cmdBuff, pcmd->ElemCount, 1, indexOffset, vertexOffset, 0);

									indexOffset += pcmd->ElemCount;
								}
								vertexOffset += cmd_list->VtxBuffer.Size;
							}
						}
					}
					//step->endStep(mainGPU.graphicsQueue.handle, cmdBuff);
				}

			}
		}

		step->endStep(cmdBuff);
	}
	STOP_TIMER(fxaa_pass);

	vkWaitForFences(mainGPU.device, 1, &waitFences[0], VK_TRUE, UINT64_MAX);
	vkResetFences(mainGPU.device, 1, &waitFences[0]);

	for (grail::SceneNode* node : culledNodes) {
		/*if (node->materials[0]) {
			if (node->materials[0]->dirty) {
				node->materials[0]->update();
				node->materials[1]->update();
				node->materials[2]->update();
				node->materials[0]->dirty = false;
			}
		}*/
	}

	updateUniformBuffers(camera);
	if (renderUI) {
		ImDrawData* imDrawData = ImGui::GetDrawData();

		// Upload data
		ImDrawVert* vtxDst = (ImDrawVert*)uniformBuffers["imguiVerts"].mapBuffer();
		ImDrawIdx* idxDst = (ImDrawIdx*)uniformBuffers["imguiIndices"].mapBuffer();

		for (int n = 0; n < imDrawData->CmdListsCount; n++) {
			const ImDrawList* cmd_list = imDrawData->CmdLists[n];
			memcpy(vtxDst, cmd_list->VtxBuffer.Data, cmd_list->VtxBuffer.Size * sizeof(ImDrawVert));
			memcpy(idxDst, cmd_list->IdxBuffer.Data, cmd_list->IdxBuffer.Size * sizeof(ImDrawIdx));
			vtxDst += cmd_list->VtxBuffer.Size;
			idxDst += cmd_list->IdxBuffer.Size;
		}

		/*uniformBuffers["imguiVerts"].memoryRegion.unmapMemory();
		uniformBuffers["imguiIndices"].memoryRegion.unmapMemory();*/
	}

	beginFrame();
	for (std::string name : renderChain->getChainOrder()) {
		RenderStep* step = &renderChain->getStep(name);

		step->defaultSubmitInfo.pCommandBuffers = &step->cmdBuffers[inflightFrameIndex];
		step->defaultSubmitInfo.pSignalSemaphores = &step->semaphores[inflightFrameIndex];
		if (step->prevStep) {
			step->defaultSubmitInfo.pWaitSemaphores = &step->prevStep->semaphores[inflightFrameIndex];
		}

		//GRAIL_LOG(INFO, "NAME") << name;

		if (name.compare("fxaa_pass") == 0) {
			step->defaultSubmitInfo.pSignalSemaphores = &renderCompleteSemaphores[inflightFrameIndex];
			VK_VALIDATE_RESULT(vkQueueSubmit(mainGPU.graphicsQueue.handle, 1, &step->defaultSubmitInfo, waitFences[0]));
		}
		else {
			step->defaultSubmitInfo.pCommandBuffers = &step->cmdBuffers[inflightFrameIndex];

			if (name.compare("compute_test") == 0) {
				step->submitStep(mainGPU.computeQueue.handle);
			}
			else {
				step->submitStep(mainGPU.graphicsQueue.handle);
			}
		}
	}
	endFrame();

	uboVS.lastCombined = camera->combined;
	uboVS.time.y = passed_time;

	inflightFrameIndex = (inflightFrameIndex + 1) % 2;
}

void grail::Renderer::printMemoryConsumption() {
	//GRAIL_LOG(DEBUG, "BUFFER MEMORY USED") << static_cast<float>((bufferMemory->getHeapSize() - bufferMemory->getRemainingMemory())) / 1024 / 1024 << "MB / " << static_cast<float>((bufferMemory->getHeapSize())) / 1024 / 1024 << "MB";
	//GRAIL_LOG(DEBUG, "TEXTURE MEMORY USED") << static_cast<float>((textureMemory->getHeapSize() - textureMemory->getRemainingMemory())) / 1024 / 1024 << "MB / " << static_cast<float>((textureMemory->getHeapSize())) / 1024 / 1024 << "MB";
	//GRAIL_LOG(DEBUG, "UNIFORM MEMORY USED") << static_cast<float>((uniformMemory->getHeapSize() - uniformMemory->getRemainingMemory())) / 1024 / 1024 << "MB / " << static_cast<float>((uniformMemory->getHeapSize())) / 1024 / 1024 << "MB";
}

void grail::Renderer::createDefaultDeferredMaterial(grail::SceneNode* node) {
	createMaterial("dev/default_material.mat", node);
}

VkShaderModule compileShader(std::string shader, std::string definition, shaderc_shader_kind* kind) {
	std::string extension = grail::FileIO::getExtension(shader);

	VkShaderModuleCreateInfo moduleCreateInfo = {};
	moduleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;

	VkShaderModule shaderModule;

	shaderc_shader_kind shaderKind;

	if (grail::utils::string::equalsCaseInsensitive(extension, "frag"))
		shaderKind = shaderc_shader_kind::shaderc_fragment_shader;
	else if (grail::utils::string::equalsCaseInsensitive(extension, "vert"))
		shaderKind = shaderc_shader_kind::shaderc_vertex_shader;
	else if (grail::utils::string::equalsCaseInsensitive(extension, "tcs"))
		shaderKind = shaderc_shader_kind::shaderc_tess_control_shader;
	else if (grail::utils::string::equalsCaseInsensitive(extension, "tes"))
		shaderKind = shaderc_shader_kind::shaderc_tess_evaluation_shader;
	else if (grail::utils::string::equalsCaseInsensitive(extension, "geom"))
		shaderKind = shaderc_shader_kind::shaderc_geometry_shader;
	else if (grail::utils::string::equalsCaseInsensitive(extension, "comp"))
		shaderKind = shaderc_shader_kind::shaderc_compute_shader;

	*kind = shaderKind;

	shaderc::Compiler compiler;
	shaderc::CompileOptions options;
	options.SetOptimizationLevel(shaderc_optimization_level_performance);
	options.AddMacroDefinition(definition);

	char* contents;
	uint32_t size;

	grail::FileIO::readBinaryFile(shader, &contents, &size);

	shaderc::SpvCompilationResult module = compiler.CompileGlslToSpv(contents, size, shaderKind, "main", options);
	delete[] contents;

	if (module.GetCompilationStatus() != shaderc_compilation_status_success) {
		GRAIL_LOG(ERROR, "SHADER LOADER") << "Error compiling: " << shader << ":\n" << module.GetErrorMessage();
		throw new grail::exceptions::RuntimeException("Shader Compilation Failed");
	}

	std::vector<uint32_t> result(module.cbegin(), module.cend());

	moduleCreateInfo.codeSize = result.size() * sizeof(uint32_t);
	moduleCreateInfo.pCode = (uint32_t*)result.data();

	VK_VALIDATE_RESULT(vkCreateShaderModule(grail::VulkanContext::mainGPU.device, &moduleCreateInfo, nullptr, &shaderModule));

	return shaderModule;
}

static std::map<uint32_t, VkPipeline> loadedPipelines;

void grail::Renderer::createMaterial(const std::string& materialDescriptor, SceneNode* node) {
	/*MaterialDescriptor desc = MaterialParser::parseMaterial_(materialDescriptor);

	CullMode originalMode = desc.createInfo.pipelineInformation.rasterizationState.cullMode;
	bool originalDepthClamp = desc.createInfo.pipelineInformation.rasterizationState.depthClampEnable ;

	std::array<std::string, 3> passNames = { "DEFERRED", "SHADOW", "DEPTH" };
	std::array<MaterialType, 3> passType = { MaterialType::MAT_DEFERRED, MaterialType::MAT_SHADOW, MaterialType::MAT_DEPTH_PREPASS };
	std::array<uint32_t, 3> passAttachments = { 3, 0, 0 };
	std::array<CompareOp, 3> compareOps = { CompareOp::LESS_OR_EQUAL,  CompareOp::LESS_OR_EQUAL,  CompareOp::LESS_OR_EQUAL };
	std::array<bool, 3> depthWrites = { false, true, true };
	std::array<bool, 3> colorWrites = { true, false, false };
	std::array<std::string, 3> renderPassNames = { "gpass", "shadow_cascade_pass", "depth_prepass" };

	std::map<shaderc_shader_kind, ShaderStageBits> kindMapping = {
		{ shaderc_shader_kind::shaderc_fragment_shader, ShaderStageBits::FRAGMENT },
		{ shaderc_shader_kind::shaderc_vertex_shader, ShaderStageBits::VERTEX },
		{ shaderc_shader_kind::shaderc_tess_control_shader, ShaderStageBits::TESSELLATION_CONTROL },
		{ shaderc_shader_kind::shaderc_tess_evaluation_shader, ShaderStageBits::TESSELLATION_EVALUATION },
		{ shaderc_shader_kind::shaderc_geometry_shader, ShaderStageBits::GEOMETRY },
		{ shaderc_shader_kind::shaderc_compute_shader, ShaderStageBits::COMPUTE },
	};*/

	/*for (int i = 0; i < passNames.size(); i++) {
		desc.createInfo.pipelineInformation.colorBlendState.attachmentCount = passAttachments[i];
		desc.createInfo.pipelineInformation.renderPass = renderChain->getStep(renderPassNames[i]).renderPass;
		desc.createInfo.pipelineInformation.depthStencilState.depthCompareOp = compareOps[i];
		desc.createInfo.pipelineInformation.depthStencilState.depthWriteEnable = depthWrites[i];

		if (i == 1) {
			desc.createInfo.pipelineInformation.rasterizationState.cullMode = CullMode::NONE;
			desc.createInfo.pipelineInformation.rasterizationState.depthClampEnable = true;
		}
		else {
			desc.createInfo.pipelineInformation.rasterizationState.cullMode = originalMode;
			desc.createInfo.pipelineInformation.rasterizationState.depthClampEnable = originalDepthClamp;
		}

		for (int j = 0; j < desc.shaders.size(); j++) {
			//shaderc_shader_kind kind;
			//VkShaderModule module = compileShader(desc.shaders[j], passNames[i], &kind);

			Shader* shader = Resources::loadSPIRVShader(desc.shaders[j]);
			ShaderPermuation* permutation = shader->getPermutation(passNames[i]);

			//GRAIL_LOG(INFO, "awefawe") << permutation->getModule();

			desc.createInfo.pipelineInformation.shaderStageState.set(shader->getStage(), permutation->getModule());
		}

		/*std::vector<DescriptorBindingInfo> bindings;

		bindings.push_back({ "uniform_buffer", DescriptorBindingType::BUFFER, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT });

		for (auto& texture : desc.textures) {
			bindings.push_back( { texture.name, DescriptorBindingType::TEXTURE, static_cast<VkShaderStageFlags>(texture.stages) } );
		}

		desc.createInfo.layout = defaultVertexLayout;
		desc.createInfo.descriptors = DescriptorSetGroup(desc.pushConstantRange, {
			DescriptorSet(bindings)
		});
		desc.createInfo.pipelineFactory = pipelineFactory;
		desc.createInfo.type = passType[i];

		uint32_t hash = desc.createInfo.pipelineInformation.toHash();

		VkPipeline hashedPipeline = VK_NULL_HANDLE;
		if (loadedPipelines.find(hash) != loadedPipelines.end()) {
			hashedPipeline = loadedPipelines[hash];
		}
		desc.createInfo._TEMPORARY_PIPELINE_VAR = hashedPipeline;

		Material* material = new Material(desc.createInfo);
		if (loadedPipelines.find(hash) == loadedPipelines.end()) {
			loadedPipelines[hash] = material->getPipeline();
		}
		//GRAIL_LOG(INFO, "MATERIAL HASH") << desc.createInfo.pipelineInformation.toHash();
		material->getDescriptors().getBinding("uniform_buffer").setDescriptor(&uniformBuffers["gpass"].descriptor);

		for (auto& texture : desc.textures) {
			if (desc.defaults.find(texture.name) != desc.defaults.end()) {
				if (Resources::getTexture(desc.defaults[texture.name])) {
					texture.texture = Resources::getTexture(desc.defaults[texture.name]);
					texture.currentTextureName = desc.defaults[texture.name];
				}
				else {
					texture.texture = Resources::loadTexture(desc.defaults[texture.name]);
					texture.currentTextureName = desc.defaults[texture.name];
				}
			}

			if (!texture.texture) {
				texture.texture = Resources::getTexture("red");
				texture.currentTextureName = "red";
			}

			material->getDescriptors().getBinding(texture.name).setDescriptor(&texture.texture->getDescriptor());
		}

		/*material->getDescriptors().getBinding("albedo_texture").setDescriptor(&defaultAlbedo->descriptorImageInfo);
		material->getDescriptors().getBinding("normal_texture").setDescriptor(&defaultNormals->descriptorImageInfo);
		material->getDescriptors().getBinding("roughness_texture").setDescriptor(&defaultRoughness->descriptorImageInfo);
		material->getDescriptors().getBinding("metallic_texture").setDescriptor(&defaultMetallic->descriptorImageInfo);*/
		//material->getDescriptors().update();

		//GRAIL_LOG(INFO, "UPDATING");

		//node->materials[i] = material;

		//materialLibrary->registerBaseMaterial("default_deferred", material);
	//}

/*	node->materialRef = materialDescriptor;
	node->modifiableVars = desc.modifiableVars;
	node->textures = desc.textures;

	if (node->materialMemory) delete node->materialMemory;
	node->materialMemory = std::malloc(desc.pushConstantRange.size);
	node->materialMemorySize = desc.pushConstantRange.size;

	int remainder = desc.pushConstantRange.size % sizeof(float);
	if (remainder != 0) GRAIL_LOG(WARNING, "MATERIAL MEMORY") << "Material memory not aligned to float, may be undefined behaviour";

	for (int i = 0; i < desc.pushConstantRange.size / sizeof(float); i++) {
		float* val = (((float*)node->materialMemory) + i);
		*val = 1;
	}

	for (auto& it : desc.defaults) {
		for (ModifiableShaderVar& v : node->modifiableVars) {
			if (it.first.compare(v.shaderName) == 0) {
				void* basePtr = static_cast<char*>(node->materialMemory) + v.offset;
				float* ptr = static_cast<float*>(basePtr);

				*ptr = std::stof(it.second);

				if ((uint32_t)(v.editType & VarType::BOOLEAN)) {

					if (std::stof(it.second) == 0) {
						v.boolVal = false;
					}
				}

				if ((uint32_t)(v.editType & VarType::LIST)) {
					v.properties["listItem"] = utils::string::splitByRegex(v.properties["list"], ";")[std::stoi(it.second)];
				}
			}
		}
	}*/
}

void grail::Renderer::renderMesh(VkCommandBuffer commandBuffer, Mesh * mesh, Material * material, void* pushConstants) {
	vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, material->getPipeline());

	vkCmdBindVertexBuffers(commandBuffer, 0, 1, &mesh->buffer, &mesh->vertexOffset);
	vkCmdBindIndexBuffer(commandBuffer, mesh->buffer, mesh->indexOffset, VK_INDEX_TYPE_UINT32);

	//vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, material->getDescriptors().getPipelineLayout(), 0, material->getDescriptors().getSetSize(), material->getDescriptors().getSetPtr(), 0, nullptr);

	/*if (material->getPushConstants().size > 0)
		vkCmdPushConstants(commandBuffer, material->getDescriptors().getPipelineLayout(), static_cast<VkShaderStageFlagBits>(material->getPushConstants().stageFlags), material->getPushConstants().offset, material->getPushConstants().size, pushConstants);*/

	vkCmdDrawIndexed(commandBuffer, mesh->indicesCount, 1, 0, 0, 1);
}

int parseNode(grail::SceneNode* node, int index, int* node_clicked, int selection_mask, grail::SceneNode** newRoot) {
	ImGuiTreeNodeFlags node_flags = ImGuiTreeNodeFlags_DefaultOpen | ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick | ((selection_mask & (1 << index)) ? ImGuiTreeNodeFlags_Selected : 0);

	if (node->getChildCount() == 0) {
		node_flags |= ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen; // ImGuiTreeNodeFlags_Bullet
	}

	std::string name = "";

	/*if (!node->mesh) {
		name.append("[] ");
	}*/

	static grail::SceneNode* from;
	static grail::SceneNode* to;

	name.append(node->name.c_str());

	// Node
	bool node_open = ImGui::TreeNodeEx((void*)(intptr_t)index, node_flags, name.c_str(), "");
	if (ImGui::BeginDragDropSource(ImGuiDragDropFlags_SourceNoHoldToOpenOthers | ImGuiDragDropFlags_SourceNoDisableHover)) {
		ImGui::Text("Moving \"%s\"", name.c_str());
		ImGui::SetDragDropPayload("SCENE_REORDER", &node, sizeof(grail::SceneNode**));
		ImGui::EndDragDropSource();
	}
	if (ImGui::BeginDragDropTarget()) {
		if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("SCENE_REORDER", 0))
		{
			from = *(grail::SceneNode**)payload->Data;
			to = node;
		}
		ImGui::EndDragDropTarget();
	}
	if (ImGui::IsItemClicked()) {
		*node_clicked = index;
		*newRoot = node;
	}
	if (node_open) {
		if (node->getChildren().size() > 0) {
			for (grail::SceneNode* n : node->getChildren()) {
				int res = parseNode(n, ++index, node_clicked, selection_mask, newRoot);
				index = res;
			}
			ImGui::TreePop();
		}
	}

	if (from && to)
	{
		//GRAIL_LOG(INFO, "MOVED") << "FROM:" << from->name << " TO:" << to->name;

		if (from->getParent()) {
			from->getParent()->removeChild(from);
		}

		to->addChild(from);

		ImGui::SetDragDropPayload("SCENE_REORDER", &to, sizeof(grail::SceneNode**));

		from = nullptr;
		to = nullptr;
	}

	return index;

	//ImGui::TreePop();
}

/*void displayMemoryRegion(grail::MemoryRegion* region) {
	std::stringstream ss;
	ss << region->getName() << " " << static_cast<float>((region->getTotalUsedSpace())) / 1024 / 1024 << "MB" << " / " << static_cast<float>((region->getTotalSpace())) / 1024 / 1024 << "MB";
	if (ImGui::CollapsingHeader(ss.str().c_str()), ImGuiTreeNodeFlags_DefaultOpen) {
		ImVec2 pos = ImGui::GetCursorScreenPos();
		ImVec2 currentPos = ImVec2(0, 0);

		float width = 100;
		float height = 20;
		float padding = 10;

		for (grail::MemoryBlock* block : region->getBlocks()) {
			ImVec2 finalPos = ImVec2(pos.x + currentPos.x, pos.y + currentPos.y);

			ImGui::GetWindowDrawList()->AddRectFilled(finalPos, ImVec2(finalPos.x + width, finalPos.y + height), IM_COL32(10, 10, 10, 255));
			for (grail::MemoryRegionDescriptor& allocation : block->allocations) {

				ImVec4 offsetSize = ImVec4();
				offsetSize.x = allocation.offset == 0 ? 0 : (float)allocation.offset / block->size;
				offsetSize.x *= width;

				offsetSize.y = 0;

				offsetSize.z = allocation.size == 0 ? 0 : (float)(allocation.offset + allocation.size) / block->size;
				offsetSize.z *= width;

				offsetSize.w = height;

				ImGui::GetWindowDrawList()->AddRectFilled(ImVec2(finalPos.x + offsetSize.x, finalPos.y + offsetSize.y), ImVec2(finalPos.x + offsetSize.z, finalPos.y + offsetSize.w), IM_COL32(50, 120, 10, 255));
				ImGui::GetWindowDrawList()->AddRectFilled(ImVec2(finalPos.x + offsetSize.z - 1, finalPos.y), ImVec2(finalPos.x + offsetSize.z, finalPos.y + height), IM_COL32(255, 10, 10, 255));
			}

			currentPos.x += width + padding;
		}
		ImGui::Dummy(ImVec2(currentPos.x, 20));
	}
}*/

#include "ImCurveEdit.h"

// Helper to display a little (?) mark which shows a tooltip when hovered.
static void ShowHelpMarker(const char* desc)
{
	ImGui::TextDisabled("(?)");
	if (ImGui::IsItemHovered())
	{
		ImGui::BeginTooltip();
		ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
		ImGui::TextUnformatted(desc);
		ImGui::PopTextWrapPos();
		ImGui::EndTooltip();
	}
}

void grail::Renderer::drawUI() {
	imguiDescriptorIndex = 1;

	//ImGui::ShowDemoWindow();

	// TIMINGS
	ImGui::SetNextWindowSize(ImVec2(0, 0), ImGuiCond_Always);
	ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiCond_FirstUseEver);
	ImGui::Begin("Frame Timings");
	if (ImGui::CollapsingHeader("CPU")) {
		ImGui::Text(processCPUTimings().c_str(), "");
	}

	if (ImGui::CollapsingHeader("GPU", ImGuiTreeNodeFlags_DefaultOpen)) {
		ImGui::Text(renderChain->getStepExecutionTimes().c_str(), "");
	}
	ImGui::End();

	// DEBUG
	ImGui::SetNextWindowSize(ImVec2(0, 0), ImGuiCond_Always);
	ImGui::SetNextWindowPos(ImVec2(0, 300), ImGuiCond_FirstUseEver);
	ImGui::Begin("Debug");

	ImGui::Text((std::string("Camera Position: ") + std::to_string(camera->position.x) + " " + std::to_string(camera->position.y) + " " + std::to_string(camera->position.z)).c_str(), "");
	ImGui::Text((std::string("Camera Direction: ") + std::to_string(camera->direction.x) + " " + std::to_string(camera->direction.y) + " " + std::to_string(camera->direction.z)).c_str(), "");

	if (ImGui::CollapsingHeader("Memory", ImGuiTreeNodeFlags_DefaultOpen)) {
		//displayMemoryRegion(textureMemory);
		//displayMemoryRegion(bufferMemory);
		//displayMemoryRegion(uniformMemory);
	}

	if (ImGui::CollapsingHeader("Culling", ImGuiTreeNodeFlags_DefaultOpen)) {
		ImGui::Checkbox("Enable Cull", &cull);
		ImGui::Checkbox("Multithreaded", &multithreadedCull);
		//ImGui::Checkbox("Hyperthreaded", &hyperthreading);
		ImGui::Checkbox("Freeze Frustum", &freezeFrustum);
		ImGui::NewLine();
	}

	if (ImGui::CollapsingHeader("Directional Light", ImGuiTreeNodeFlags_DefaultOpen)) {
		ImGui::SliderInt("CascadeIndex", &shadowCascadeIndex, 1, 3);
		ImGui::Checkbox("Debug Cascades", &debugShadows);

		ImGui::SliderFloat3("Light Position", &lightPos[0], -1.0f, 1.0f);
		ImGui::NewLine();

		ImGui::ColorEdit3("Light Color", &lightColor[0]);
		ImGui::SliderFloat("Light Intensity", &lightIntensity, 0.0f, 100.0f);
		ImGui::NewLine();

		ImGui::SliderInt("Active Cascades", &activeCascades, 1, 4); ImGui::SameLine(); ShowHelpMarker("Sets how many shadow map cascades to use, increasing shadow quality\nHigher number - better quality\nHigh performance impact");
		ImGui::SliderFloat("Cascade Lambda", &cascadeSplitLambda, 0.01f, 1.0f); ImGui::SameLine(); ShowHelpMarker("Sets how many shadow map cascades to use, increasing shadow quality\nHigher number - better quality\nHigh performance impact");
		ImGui::Checkbox("Visualize Cascades", &visualizeCascades);
		ImGui::NewLine();

//		ImCurveEdit::Edit()

		ImGui::SliderFloat("Enviroment Global Brightness", &envScaleGlobal, 0.0f, 10.0f);
		ImGui::SliderFloat("Enviroment IBL Brightness", &envScaleIBL, 0.0f, 10.0f);
		ImGui::SliderFloat("Enviroment Visual Brightness", &envScale, 0.0f, 10.0f);
		const char* envFunc[] = { "Default", "POW(2.2)", "POW(1.0/2.2)" };
		ImGui::Combo("Enviroment Color Func", &envColorFunc, envFunc, IM_ARRAYSIZE(envFunc));
		const char* envSrc[] = { "Irradiance", "Input" };
		ImGui::Combo("Enviroment Source", &envSource, envSrc, IM_ARRAYSIZE(envSrc));
		ImGui::NewLine();

		const char* items[] = { "Default", "Experimental" };
		ImGui::Combo("BRDF Function", &BRDFFunc, items, IM_ARRAYSIZE(items));
		ImGui::NewLine();
	}

	if (ImGui::CollapsingHeader("Post Processing")) {
		if (ImGui::TreeNode("Sharpen")) {
			ImGui::Checkbox("Sharpen Filter", &sharpen);
			ImGui::SliderFloat("Factor", &sharpenFactor, 0.0, 1.0);
			ImGui::TreePop();
			ImGui::Separator();

			ImGui::Checkbox("Color LUT", &enableLUT);
			ImGui::TreePop();
			ImGui::Separator();
		}

		if (ImGui::TreeNode("TXAA")) {
			ImGui::SliderInt("TAA Resolve Mode", &taaMode, 1, 4);

			ImGui::Checkbox("Enable", &taa);

			if (!taa) {
				sharpen = false;
			}
			else {
				sharpen = true;
			}
			ImGui::TreePop();
			ImGui::Separator();
		}

		if (ImGui::TreeNode("SSR")) {
			ImGui::Checkbox("Enable (HELP)", &enableSSR);
			ImGui::TreePop();
			ImGui::Separator();
		}
	}
	ImGui::End();

/*	glm::mat4 projection = camera->projection;
	glm::mat4 view = camera->view;
	projection = glm::scale(projection, glm::vec3(1, -1, 1));

	if (transformType == ImGuizmo::OPERATION::TRANSLATE) {
		float* current = nullptr;
		
		if (ImGuizmo::IsUsing()) {
			if (!transforming) {
				//worldCopy = currentRoot->worldTransform;
				transforming = true;
			}

			current = glm::value_ptr(worldCopy);
		}
		else {
			//current = glm::value_ptr(currentRoot->worldTransform);
			transforming = false;
		}

		glm::mat4 delta = glm::mat4(1.0);
		ImGuizmo::Manipulate(glm::value_ptr(view), glm::value_ptr(projection), transformType, ImGuizmo::MODE::WORLD,
			(current),
			glm::value_ptr(delta));

		float t[3];
		float r[3];
		float s[3];

		ImGuizmo::DecomposeMatrixToComponents(glm::value_ptr(delta), &t[0], &r[0], &s[0]);

		glm::vec3 tt = glm::vec3(t[0], t[1], t[2]) / (currentRoot->parent ? currentRoot->worldScale / currentRoot->localScale : glm::vec3(1.0));
		tt = tt * (currentRoot->parent ? currentRoot->worldRotation * glm::inverse(currentRoot->localRotation) : glm::mat3(1.0));

		currentRoot->localTranslation.x += tt[0];
		currentRoot->localTranslation.y += tt[1];
		currentRoot->localTranslation.z += tt[2];
	}

	static glm::vec3 scaleCopy;
	if (transformType == ImGuizmo::OPERATION::SCALE) {
		glm::mat4 copy = currentRoot->worldTransform;
		glm::mat4 delta = glm::mat4(1.0);
		ImGuizmo::Manipulate(glm::value_ptr(view), glm::value_ptr(projection), transformType, ImGuizmo::MODE::LOCAL,
			glm::value_ptr(copy),
			glm::value_ptr(delta));

		float t[3];
		float r[3];
		float s[3];

		ImGuizmo::DecomposeMatrixToComponents(glm::value_ptr(delta), &t[0], &r[0], &s[0]);

		if (ImGuizmo::IsUsing()) {
			currentRoot->localScale.x = scaleCopy.x + (s[0] - 1.0);
			currentRoot->localScale.y = scaleCopy.y + (s[1] - 1.0);
			currentRoot->localScale.z = scaleCopy.z + (s[2] - 1.0);
		}
		else {
			scaleCopy = currentRoot->localScale;
		}
	}

	if (transformType == ImGuizmo::OPERATION::ROTATE) {
		glm::mat4 copy = currentRoot->worldTransform;
		glm::mat4 delta = glm::mat4(1.0);
		ImGuizmo::Manipulate(glm::value_ptr(view), glm::value_ptr(projection), transformType, ImGuizmo::MODE::WORLD,
			glm::value_ptr(copy),
			glm::value_ptr(delta));

		float t[3];
		float r[3];
		float s[3];

		ImGuizmo::DecomposeMatrixToComponents(glm::value_ptr(delta), &t[0], &r[0], &s[0]);

		if (ImGuizmo::IsUsing()) {
			//glm::vec3 scale = glm::vec3(r[0], r[1], r[2]) * (currentRoot->parent ? currentRoot->parent->worldRotation : glm::mat3(1.0));

			currentRoot->localRotation = glm::quat(glm::vec3(glm::radians(r[0]), glm::radians(r[1]), glm::radians(r[2]))) * currentRoot->localRotation;
		}
	}

	//currentRoot->updateTransform(true);

	ImGui::SetNextWindowSize(ImVec2(0, 0), ImGuiCond_Always);
	ImGui::SetNextWindowPos(ImVec2(1000, 0), ImGuiCond_FirstUseEver);
	ImGui::Begin("Attributes");

	ImGui::InputText("Node Name", (char*)currentRoot->name.data(), currentRoot->name.capacity() + 1, ImGuiInputTextFlags_CallbackResize, [](ImGuiInputTextCallbackData* data) {
		std::string* str = (std::string*)data->UserData;

		str->resize(data->BufTextLen);
		data->Buf = (char*)str->c_str();

		return 0;
	}, &currentRoot->name);

	glm::vec3 trans = (currentRoot->localTranslation * (currentRoot->parent ? currentRoot->worldScale / currentRoot->localScale : glm::vec3(1.0))) * (currentRoot->parent ? glm::inverse(currentRoot->worldRotation * glm::inverse(currentRoot->localRotation)) : glm::mat3(1.0));
	if (ImGui::InputFloat3("Position", &trans[0], 2)) {
		//currentRoot->localTranslation = trans * (currentRoot->parent ? (glm::toMat3(currentRoot->worldRotation)) : glm::mat3(1.0));
	};

	if (ImGui::InputFloat3("Scale", &currentRoot->worldScale[0], 2)) {
		//currentRoot->localTranslation = trans * (currentRoot->parent ? (glm::toMat3(currentRoot->worldRotation)) : glm::mat3(1.0));
	};

	ImGui::Separator();
	*/
	//if (currentRoot->mesh) {
	/*	for (ModifiableShaderVar& var : currentRoot->modifiableVars) {
			ImGui::Separator();

			if (ImGui::CollapsingHeader(("Prop: " + var.shaderName).c_str())) {
				void* basePtr = static_cast<char*>(currentRoot->materialMemory) + var.offset;
				float* ptr = static_cast<float*>(basePtr);


				if ((uint32_t)(var.editType & VarType::COLOR)) {
					ImGui::Separator();
					ImGui::PushItemWidth(150);
					if (var.size == sizeof(float) * 3) {
						ImGui::ColorPicker3((var.name + "##color").c_str(), ptr, ImGuiColorEditFlags_HDR | ImGuiColorEditFlags_Float);
					}

					if (var.size == sizeof(float) * 4) {
						float ref = 10000;
						ImGui::ColorPicker4((var.name + "##color").c_str(), ptr, ImGuiColorEditFlags_HDR | ImGuiColorEditFlags_Float | ImGuiColorEditFlags_RGB);
					}
					ImGui::PopItemWidth();
				}

				if ((uint32_t)(var.editType & VarType::SCALAR_SEPARATE)) {
					ImGui::Separator();
					std::vector<std::string> map = { "X", "Y", "Z", "W" };

					for (int i = 0; i < var.size / sizeof(float); i++) {
						ImGui::DragFloat((var.name + " " + map[i] + "##component").c_str(), (ptr + i), var.step, var.min, var.max);
					}
				}

				if ((uint32_t)(var.editType & VarType::SCALAR)) {
					ImGui::Separator();
					if (ImGui::DragFloat((var.name + "##scalar").c_str(), ptr, var.step, var.min, var.max)) {
						float val = *ptr;

						for (int i = 0; i < var.size / sizeof(float); i++) {
							*ptr = val;

							ptr++;
						}
					}
				}

				if ((uint32_t)(var.editType & VarType::BOOLEAN)) {
					ImGui::Separator();
					if (ImGui::Checkbox((var.name + "##bool").c_str(), &var.boolVal)) {
						var.boolVal ? *ptr = 1.0f : *ptr = 0.0f;
					}
				}

				if ((uint32_t)(var.editType & VarType::LIST)) {
					ImGui::Separator();

					auto items = utils::string::splitByRegex(var.properties["list"], ";");

					int index = 0;
					for (auto& item : items) {
						if (utils::string::qualsCaseInsensitive(var.properties["listItem"], item)) {
							*ptr = index;
						}

						index++;
					}

					if (ImGui::BeginCombo((var.name + "##list").c_str(), (var.properties["listItem"].data()))) {

						int index = 0;
						for (auto& item : items) {
							bool is_selected = (utils::string::qualsCaseInsensitive(var.properties["listItem"], item));
							if (ImGui::Selectable((item + "##list_" + var.name).c_str(), is_selected)) {
								var.properties["listItem"] = item;
							}
							if (is_selected)
								ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support)
						}

						ImGui::EndCombo();
					}
				}
			}
		}*/

		if (ImGui::CollapsingHeader("Textures", ImGuiTreeNodeFlags_DefaultOpen)) {
			/*for (ShaderTexture& texture : currentRoot->textures) {
				ImGui::Text(texture.name.c_str(), "");
				ImGui::Image((void*)(uint32_t)(imguiDescriptorIndex), ImVec2(40, 40));
				imguiDescriptors[imguiDescriptorIndex]->getSet(0).getBinding(0).setDescriptor(&texture.texture->getDescriptor());
				imguiDescriptors[imguiDescriptorIndex]->update();
				if (ImGui::BeginDragDropTarget()) {
					ImGuiDragDropFlags target_flags = 0;
					if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("texture", target_flags)) {
						VkTexture& t = Resources::textures[GRAIL_SID(*(std::string*)payload->Data)];

						texture.texture = &t;
						texture.currentTextureName = (*(std::string*)payload->Data);

						for (int i = 0; i < 3; i++) {
							currentRoot->materials[i]->getDescriptors().getBinding(texture.name).setDescriptor(&t.getDescriptor());
						}

						currentRoot->materials[0]->dirty = true;
					}
					ImGui::EndDragDropTarget();
				}
				ImGui::SameLine();
				ImGui::Text(texture.currentTextureName.c_str(), "");


				imguiDescriptorIndex++;
			}*/
		}
	//}

	ImGui::End();

	ImGui::SetNextWindowSize(ImVec2(0, 0), ImGuiCond_Always);
	ImGui::SetNextWindowPos(ImVec2(600, 0), ImGuiCond_FirstUseEver);
	ImGui::Begin("Scene");

	if (ImGui::CollapsingHeader("Hierarchy", ImGuiTreeNodeFlags_DefaultOpen)) {
		ImGui::BeginChild("HierachyChild", ImVec2(ImGui::GetWindowContentRegionWidth(), 300), false, ImGuiWindowFlags_HorizontalScrollbar);
		ImGui::PushStyleVar(ImGuiStyleVar_IndentSpacing, ImGui::GetFontSize() * 1); // Increase spacing to differentiate leaves from expanded contents.
		ImGui::Text(("Current Node: " + currentRoot->name).c_str(), "");
		ImGui::Separator();

		int index = 0;
		int node_clicked = -1;
		static int selection_mask = (1 << 2);

		parseNode(*baseRoot, index, &node_clicked, selection_mask, &currentRoot);

		if (node_clicked != -1) {
			// Update selection state. Process outside of tree loop to avoid visual inconsistencies during the clicking-frame.
			if (ImGui::GetIO().KeyCtrl)
				selection_mask ^= (1 << node_clicked);          // CTRL+click to toggle
			else //if (!(selection_mask & (1 << node_clicked))) // Depending on selection behavior you want, this commented bit preserve selection when clicking on item that is part of the selection
				selection_mask = (1 << node_clicked);           // Click to single-select
		}

		ImGui::PopStyleVar();
		//ImGui::TreePop();
		ImGui::EndChild();
	}
	ImGui::Separator();
	if (ImGui::Button("Add New Node")) {
		currentRoot->addChild(new SceneNode());
	}

	ImGui::SameLine();
	if (ImGui::Button("Delete Current Node")) {
		if (currentRoot == *baseRoot) {
			(*baseRoot)->children.clear();
		}

		if (currentRoot->parent) {
			SceneNode* oldRoot = currentRoot;
			currentRoot = oldRoot->parent;

			oldRoot->parent->removeChild(oldRoot->ID);
		}
	}

	if (ImGui::Button("Duplicate Current Node")) {
		if (currentRoot) {
			/*SceneNode* copy = new SceneNode(currentRoot);
			copy->name = currentRoot->name + "_" + std::to_string(SceneNode::IDCounter);
			copy->baseMeshPath = currentRoot->baseMeshPath;
			//copy->textures = currentRoot->textures;
			//copy->modifiableVars = currentRoot->modifiableVars;
			//copy->localRotation = currentRoot->localRotation;
			//copy->localTranslation = currentRoot->localTranslation;
			//copy->localScale = currentRoot->localScale;

			createMaterial(currentRoot->materialRef, copy);*/

			//std::memcpy(copy->materialMemory, currentRoot->materialMemory, currentRoot->materialMemorySize);

		/*	for (int i = 0; i < 3; i++) {
				for (int j = 0; j < copy->materials[i]->getDescriptors().getSetSize(); j++) {
					for (int k = 0; k < copy->materials[i]->getDescriptors().getSet(j).bindings.size(); k++) {
						copy->materials[i]->getDescriptors().getSet(j).getBinding(k).object = currentRoot->materials[i]->getDescriptors().getSet(j).getBinding(k).object;
					}
				}
				copy->materials[i]->update();
			}*/

			//for (int i = 0; i < currentRoot->textures.size(); i++) {
				//copy->textures[i].currentTextureName = currentRoot->textures[i].currentTextureName;
				//copy->textures[i].texture = currentRoot->textures[i].texture;
			//}


			/*if (currentRoot->parent)
				currentRoot->parent->addChild(copy);
			else
				(*baseRoot)->addChild(copy);

			currentRoot = copy;*/
		}
	}

	ImGui::Separator();

	ImGui::InputText("Node Name", (char*)sceneInput.data(), sceneInput.capacity() + 1, ImGuiInputTextFlags_CallbackResize, [](ImGuiInputTextCallbackData* data) {
		std::string* str = (std::string*)data->UserData;

		str->resize(data->BufTextLen);
		data->Buf = (char*)str->c_str();

		return 0;
	}, &sceneInput);

	/*if (ImGui::Button("Save Scene")) {
		if(!sceneInput.empty())
			SceneLoader::saveScene("scenes/" + sceneInput + ".scn", *baseRoot);
	}
	ImGui::SameLine();
	if (ImGui::Button("Load Scene")) {
		if(!sceneInput.empty())
			if (FileIO::fileExists(FileIO::getAssetPath("scenes/" + sceneInput + ".scn"))) {
				*baseRoot = SceneLoader::loadScene(("scenes/" + sceneInput + ".scn"), this);
			}
	}*/
	ImGui::End();


	// LOBSOURCES ^_^
	ImGui::SetNextWindowSize(ImVec2(0, 0), ImGuiCond_Always);
	ImGui::SetNextWindowPos(ImVec2(0, Graphics::getHeight() - 165), ImGuiCond_FirstUseEver);
	ImGui::Begin("Resources", nullptr, ImGuiWindowFlags_NoMove);
	if (ImGui::CollapsingHeader("Textures", ImGuiTreeNodeFlags_DefaultOpen)) {
		ImGui::BeginChild("TextureContent", ImVec2(Graphics::getWidth(), 70), false, ImGuiWindowFlags_HorizontalScrollbar);
		/*for (auto& entry : Resources::namedTextures) {
			//if(ImGui::BeginDragDropSource())
			ImGui::SameLine();
			(ImGui::ImageButton((void*)(uint32_t)(imguiDescriptorIndex), ImVec2(60, 60)));
			if (ImGui::IsItemHovered()) {
				ImGui::BeginTooltip();
				ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
				ImGui::TextUnformatted(entry.name.c_str());
				ImGui::PopTextWrapPos();
				ImGui::EndTooltip();
			}

			/*imguiDescriptors[imguiDescriptorIndex]->getSet(0).getBinding(0).setDescriptor(&entry.texture.getDescriptor());
			imguiDescriptors[imguiDescriptorIndex]->update();*/
	
			/*ImGuiDragDropFlags src_flags = 0;
			src_flags |= ImGuiDragDropFlags_SourceNoDisableHover;     // Keep the source displayed as hovered

			if (ImGui::BeginDragDropSource(src_flags)) {
				if (!(src_flags & ImGuiDragDropFlags_SourceNoPreviewTooltip)) {

					ImGui::Image((void*)(uint32_t)(imguiDescriptorIndex), ImVec2(200, 200));
					/*imguiDescriptors[imguiDescriptorIndex]->getSet(0).getBinding(0).setDescriptor(&entry.texture.getDescriptor());
					imguiDescriptors[imguiDescriptorIndex]->update();*/

				/*	imguiDescriptorIndex++;
				}

				ImGui::SetDragDropPayload("texture", &entry.name, sizeof(std::string));
				ImGui::EndDragDropSource();
			}


			imguiDescriptorIndex++;
		}*/
		ImGui::EndChild();
	}
	ImGui::End();
}

std::string grail::Renderer::processCPUTimings() {
	std::stringstream ss;
	double totalTime = 0;
	for (const auto &pair : cpuTimings) {
		std::string executionTime = (std::to_string(pair.second).substr(0, 5) + "ms");
		std::string name = pair.first;

		ss << utils::string::align(name, 20) << ": " << utils::string::align(executionTime, 13) << "\n";

		totalTime += pair.second;
	}

	ss << "\n\nTotal: " << utils::string::align(std::to_string(totalTime).substr(0, 5) + "ms", 15);

	return totalTime == 0.0 ? "" : ss.str();
}

void grail::Renderer::beginFrame() {
	VkResult result = (swapchain->aquireNextImage(renderCompleteSemaphores[(inflightFrameIndex + 1) % 2], &currentSwapchainBuffer, 0));
	if (result == VK_ERROR_OUT_OF_DATE_KHR) {
		swapchain->recreate();
	}
	else {
		//VK_VALIDATE_RESULT(result);
	}
}

void grail::Renderer::endFrame() {
	VkResult result = (swapchain->queuePresent(graphicsQueue, currentSwapchainBuffer, renderCompleteSemaphores[inflightFrameIndex]));
	if (result == VK_ERROR_OUT_OF_DATE_KHR) {
		swapchain->recreate();
	}
	else {
		//VK_VALIDATE_RESULT(result);
	}
	//VK_VALIDATE_RESULT(vkQueueWaitIdle(graphicsQueue));
}

float angl = 0.0f;

void grail::Renderer::createPassMaterial(const std::string & passName, RenderPassMaterialInfo & info, std::initializer_list<DescriptorSetInfo> descriptors) {
	createPassMaterial(passName, passName, info, descriptors);
}

void grail::Renderer::createPassMaterial(const std::string & passName, const std::string & matName, RenderPassMaterialInfo & info, std::initializer_list<DescriptorSetInfo> descriptors) {
	GRAIL_LOG(INFO, "MATERIAL") << "Creating material for: " << passName;

	RenderStep& pass = renderChain->getStep(passName);

	GraphicsPipelineCreateInfo pipeline = {};
	pipeline.colorBlendState.attachmentCount = pass.attachments.size();
	pipeline.renderPass = pass.renderPass;
	/*if (strcmp(info.vertexShader.c_str(), "") != 0) pipeline.shaderStageState.set(ShaderStageBits::VERTEX, Resources::loadShader("shaders/" + info.vertexShader + ".vert.spv"));
	if (strcmp(info.fragmentShader.c_str(), "") != 0) pipeline.shaderStageState.set(ShaderStageBits::FRAGMENT, Resources::loadShader("shaders/" + info.fragmentShader + ".frag.spv"));
	if (strcmp(info.computeShader.c_str(), "") != 0) pipeline.shaderStageState.set(ShaderStageBits::COMPUTE, Resources::loadShader("shaders/" + info.computeShader + ".comp.spv"));*/
	pipeline.rasterizationState.cullMode = info.cullMode;

	/*std::vector<DescriptorBindingInfo> bindings;
	for (DescriptorSetInfo descriptor : descriptors) {
		bindings.push_back({ descriptor.identifier, descriptor.type, descriptor.stageFlags });
	}

	MaterialCreateInfo materialInfo = {
		fullscreenVertexLayout,
		DescriptorSetGroup(info.pushConstantsRange,{ DescriptorSet(bindings) }),
		pipeline,
		pipelineFactory,
		MaterialType::MAT_POST_PROCESS
	};

	Material* material = new Material(materialInfo);
	for (DescriptorSetInfo descriptor : descriptors) {
		material->getDescriptors().getBinding(descriptor.identifier).setDescriptor(descriptor.ref);
	}
	material->update();*/

	//materials[matName] = material;
}

void * grail::Renderer::attachmentDescriptor(const std::string & attachmentName) {
	return &renderChain->getAttachment(attachmentName).texture->getDescriptor();
}

void * grail::Renderer::attachmentDescriptor(const std::string & attachmentName, uint32_t level) {
	return nullptr;
}

void * grail::Renderer::uniformDescriptor(const std::string & uniformName) {
	return &uniformBuffers[uniformName].descriptor;
}

void grail::Renderer::updateCascades() {
	uint32_t shadowMapCascadeSplits = activeCascades;

	std::vector<float> cascadeSplits = std::vector<float>(shadowMapCascadeSplits);

	float nearClip = camera->nearPlane;
	float farClip = camera->farPlane;
	float clipRange = farClip - nearClip;

	float minZ = nearClip;
	float maxZ = nearClip + clipRange;

	float range = maxZ - minZ;
	float ratio = maxZ / minZ;

	// Calculate split depths based on view camera furstum
	// Based on method presentd in https://developer.nvidia.com/gpugems/GPUGems3/gpugems3_ch10.html
	for (uint32_t i = 0; i < shadowMapCascadeSplits; i++) {
		float p = (i + 1) / static_cast<float>(shadowMapCascadeSplits);
		float log = minZ * std::pow(std::abs(ratio), p);
		float uniform = minZ + range * p;
		float d = cascadeSplitLambda * (log - uniform) + uniform;
		cascadeSplits[i] = (d - nearClip) / clipRange;
	}

	// Calculate orthographic projection matrix for each cascade
	float lastSplitDist = 0.0;
	for (uint32_t i = 0; i < shadowMapCascadeSplits; i++) {
		float splitDist = cascadeSplits[i];

		glm::vec3 frustumCorners[8] = {
			glm::vec3(-1.0f,  1.0f, -1.0f),
			glm::vec3(1.0f,  1.0f, -1.0f),
			glm::vec3(1.0f, -1.0f, -1.0f),
			glm::vec3(-1.0f, -1.0f, -1.0f),
			glm::vec3(-1.0f,  1.0f,  1.0f),
			glm::vec3(1.0f,  1.0f,  1.0f),
			glm::vec3(1.0f, -1.0f,  1.0f),
			glm::vec3(-1.0f, -1.0f,  1.0f),
		};

		// Project frustum corners into world space
		glm::mat4 invCam = glm::inverse(camera->projection * camera->view);
		for (uint32_t i = 0; i < 8; i++) {
			glm::vec4 invCorner = invCam * glm::vec4(frustumCorners[i], 1.0f);
			frustumCorners[i] = invCorner / invCorner.w;
		}

		for (uint32_t i = 0; i < 4; i++) {
			glm::vec3 dist = frustumCorners[i + 4] - frustumCorners[i];
			frustumCorners[i + 4] = frustumCorners[i] + (dist * splitDist);
			frustumCorners[i] = frustumCorners[i] + (dist * lastSplitDist);
		}

		// Get frustum center
		glm::vec3 frustumCenter = glm::vec3(0.0f);
		for (uint32_t i = 0; i < 8; i++) {
			frustumCenter += frustumCorners[i];
		}
		frustumCenter /= 8.0f;

		float radius = 0.0f;
		for (uint32_t i = 0; i < 8; i++) {
			float distance = glm::length(frustumCorners[i] - frustumCenter);
			radius = glm::max(radius, distance);
		}
		radius = std::ceil(radius * 16.0f) / 16.0f;

		glm::vec3 maxExtents = glm::vec3(radius);
		glm::vec3 minExtents = -maxExtents;

		glm::vec3 lightDir = glm::normalize(-lightPos);
		glm::mat4 lightViewMatrix = glm::lookAt(frustumCenter - lightDir * -minExtents.z, frustumCenter, glm::vec3(0.0f, 1.0f, 0.0f));
		glm::mat4 lightOrthoMatrix = glm::ortho(minExtents.x, maxExtents.x, minExtents.y, maxExtents.y, 0.0f, maxExtents.z - minExtents.z);

		glm::mat4 shadowMatrix = lightOrthoMatrix * lightViewMatrix;
		glm::vec4 shadowOrigin = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
		shadowOrigin = shadowMatrix * shadowOrigin;
		shadowOrigin = shadowOrigin * static_cast<float>(shadowCascadeMapSize) / 2.0f;

		glm::vec4 roundedOrigin = glm::round(shadowOrigin);
		glm::vec4 roundOffset = roundedOrigin - shadowOrigin;
		roundOffset = roundOffset * 2.0f / static_cast<float>(shadowCascadeMapSize);
		roundOffset.z = 0.0f;
		roundOffset.w = 0.0f;

		glm::mat4 shadowProj = lightOrthoMatrix;
		shadowProj[3] += roundOffset;
		lightOrthoMatrix = shadowProj;

		// Store split distance and matrix in cascade
		cascades[i].splitDepth = (camera->nearPlane + splitDist * clipRange) * -1.0f;
		cascades[i].combinedMat = lightOrthoMatrix * lightViewMatrix;

		lastSplitDist = cascadeSplits[i];
	}
}

void grail::Renderer::createUniformBuffer(std::string identifier, VkDeviceSize size) {
	/*UniformBuffer buffer = {};

	VkMemoryRequirements memReqs;

	VkBufferCreateInfo bufferInfo = {};

	bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferInfo.size = size;
	bufferInfo.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;

	VK_VALIDATE_RESULT(vkCreateBuffer(mainGPU.device, &bufferInfo, nullptr, &buffer.handle));
	vkGetBufferMemoryRequirements(mainGPU.device, buffer.handle, &memReqs);

	buffer.memoryRegion = uniformMemory->allocateRegion(memReqs.size, memReqs.alignment, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, memReqs.memoryTypeBits);

	VK_VALIDATE_RESULT(vkBindBufferMemory(mainGPU.device, buffer.handle, buffer.memoryRegion.block->memoryHandle, buffer.memoryRegion.offset));

	buffer.descriptor.buffer = buffer.handle;
	buffer.descriptor.offset = 0;
	buffer.descriptor.range = size;

	uniformBuffers.insert({ identifier, buffer });*/
}

grail::Renderer::UniformBuffer grail::Renderer::createBuffer(std::string identifier, VkDeviceSize size, VkBufferUsageFlags usageFlags) {
	/*UniformBuffer buffer = {};

	VkMemoryRequirements memReqs;

	VkBufferCreateInfo bufferInfo = {};

	bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferInfo.size = size;
	bufferInfo.usage = usageFlags;

	VK_VALIDATE_RESULT(vkCreateBuffer(mainGPU.device, &bufferInfo, nullptr, &buffer.handle));
	vkGetBufferMemoryRequirements(mainGPU.device, buffer.handle, &memReqs);

	buffer.memoryRegion = uniformMemory->allocateRegion(memReqs.size, memReqs.alignment, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, memReqs.memoryTypeBits);

	VK_VALIDATE_RESULT(vkBindBufferMemory(mainGPU.device, buffer.handle, buffer.memoryRegion.block->memoryHandle, buffer.memoryRegion.offset));

	buffer.descriptor.buffer = buffer.handle;
	buffer.descriptor.offset = 0;
	buffer.descriptor.range = size;

	uniformBuffers.insert({ identifier, buffer });
	
	return buffer;*/

	return UniformBuffer{};
}

void grail::Renderer::initializeAndroid() {
	{
		androidChain = new RenderChain();

		TextureCreateInfo attachmentInfo = {};
		attachmentInfo.generateMipmaps = false;
		attachmentInfo.samplerInfo.addressModeU = SamplerAddressMode::CLAMP_TO_EDGE;
		attachmentInfo.samplerInfo.addressModeV = SamplerAddressMode::CLAMP_TO_EDGE;
		attachmentInfo.samplerInfo.magFilter = SamplerFilter::NEAREST;
		attachmentInfo.samplerInfo.minFilter = SamplerFilter::NEAREST;
		attachmentInfo.samplerInfo.anisotropyEnable = false;

		uint32_t width = Graphics::getWidth();
		uint32_t height = Graphics::getHeight();
		RenderStepAttachmentCreateInfo stepInfo = RenderStepAttachmentCreateInfo{ attachmentInfo, width, height };
		renderChain->createAttachment("present", stepInfo);

		RenderStepDescription description;

		renderChain->describeStep("present_pass", description = RenderStepDescription{ width, height, 2, 2,{ "present" }, "" });

		renderChain->finalizeChain();

		int index = 0;
		std::stringstream ss;
		ss << "*********************************\nCOMPUTED RENDER ANDROID CHAIN ORDER: ---------------------- \n";
		ss << " -> ";
		for (std::string step : renderChain->getChainOrder()) {
			RenderStep& s = renderChain->getStep(step);
			ss << s.identifier << " ( ";

			for (auto& attachment : s.attachments) {
				ss << "[" << attachment.first << "] ";
			}

			ss << " ) -> \n";
		}

		ss << "--------------------------------------------";
		GRAIL_LOG(WARNING, "CHAIN ORDER") << ss.str();
	}
}

void grail::Renderer::renderAndroid(SceneNode** rootNode, grail::Camera* camera, float delta) {
	frameCount += 1;
	passed_time += delta * 1.0;

	if (!currentRoot)
		currentRoot = *rootNode;

	baseRoot = rootNode;

	this->camera = camera;
	this->camera->update(!freezeFrustum);

	if (debugShadows) {
		this->camera->combined = uboVS.cascadeMatrices[shadowCascadeIndex];
	}

	std::vector<SceneNode*> renderables;

	(*rootNode)->updateTransform(true);

	std::stack<SceneNode*> nodes;
	nodes.push(*rootNode);

	while (!nodes.empty()) {
		SceneNode* node = nodes.top();
		nodes.pop();

		/*Mesh* mesh = node->mesh;

		if (mesh && (node->materials[0])) {
			renderables.push_back(node);
		}*/

		for (uint32_t i = 0; i < node->getChildCount(); i++)
			nodes.push(node->getChildByIndex(i));
	}

	std::vector<SceneNode*> culledNodes;
	std::vector<std::vector<SceneNode*>> shadowNodes;
	shadowNodes.resize(activeCascades);

	for (uint32_t i = 0; i < activeCascades; i++) {
		if (!freezeFrustum) {
			cascades[i].frustum.update(cascades[i].combinedMat);
		}
	}

	if (cull) {
		if (multithreadedCull) {
			uint32_t threadIndex = 0;

			for (uint32_t i = 0; i < activeCascades; i++) {
				threadPool->threads[i]->addJob([&, i] {
					for (grail::SceneNode* node : renderables) {
						/*if (cascades[i].frustum.AABBInFrustum(node->boundsMinWorld, node->boundsMaxWorld)) {
							shadowNodes[i].push_back(node);
						}*/
					}
					});
			}

			for (SceneNode* node : renderables) {
				/*if (camera->frustum.AABBInFrustum(node->boundsMinWorld, node->boundsMaxWorld)) {
					culledNodes.push_back(node);
				}*/
			}

			threadPool->wait();
		}
		else {
			for (SceneNode* node : renderables) {
				/*if (camera->frustum.AABBInFrustum(node->boundsMinWorld, node->boundsMaxWorld)) {
					culledNodes.push_back(node);
				}*/
			}

			for (uint32_t i = 0; i < activeCascades; i++) {
				for (SceneNode* node : renderables) {
					/*if (cascades[i].frustum.AABBInFrustum(node->boundsMinWorld, node->boundsMaxWorld)) {
						shadowNodes[i].push_back(node);
					}*/
				}
			}
		}
	}
	else {
		for (SceneNode* node : renderables) {
			culledNodes.push_back(node);
		}

		for (uint32_t i = 0; i < activeCascades; i++) {
			for (SceneNode* node : renderables) {
				shadowNodes[i].push_back(node);
			}
		}
	}



}

void grail::Renderer::updateUniformBuffers(grail::Camera* camera) {
	updateCascades();

	const unsigned SubsampleIdx = frameCount % SAMPLE_COUNT;

	const glm::vec2 TexSize(1.0f / Graphics::getWidth(), 1.0f / Graphics::getHeight()); // Texel size
	const glm::vec2 SubsampleSize = TexSize * 2.0f; // That is the size of the subsample in NDC

	const glm::vec2 S = SAMPLE_LOCS[SubsampleIdx]; // In [-1, 1]

	glm::vec2 Subsample = S * SubsampleSize; // In [-SubsampleSize, SubsampleSize] range
	Subsample *= 0.5f; // In [-SubsampleSize / 2, SubsampleSize / 2] range

	/*glm::vec4 jittOffset = glm::vec4(Subsample.x, Subsample.y, 0.0f, 1.0f);
	jittOffset = jittOffset * camera->combined;
	jittOffset /= jittOffset.w;*/

	if (!taa)
		Subsample = glm::vec2(0.0);

	taaJitter = SAMPLE_LOCS[SubsampleIdx] / TexSize;

	uboVS.combined = camera->combined;
	uboVS.jitter = glm::vec4(Subsample, Subsample);
	for (uint32_t i = 0; i < shadowMapCascadeSplits; i++) {
		uboVS.cascadeMatrices[i] = cascades[i].combinedMat;
	}
	uboVS.cameraPos = glm::vec4(camera->position, 1.0);
	uboVS.time.x = passed_time;
	uniformBuffers["gpass"].copyData(&uboVS, sizeof(uboVS));
	

	lightpassVS.invProj = glm::inverse(camera->projection);
	lightpassVS.view = camera->view;
	uniformBuffers["lightpassVS"].copyData(&lightpassVS, sizeof(lightpassVS));

	lightpassFS.cameraPos = glm::vec4(camera->position, 0.0);
	lightpassFS.invCombined = glm::inverse(camera->combined);
	for (uint32_t i = 0; i < shadowMapCascadeSplits; i++) {
		lightpassFS.cascadeSplits[i] = cascades[i].splitDepth;
		lightpassFS.cascadeViewProj[i] = cascades[i].combinedMat;
	}
	lightpassFS.lightDir = glm::normalize(glm::vec4(-lightPos, 0.0));
	lightpassFS.inverseProjection = glm::inverse(camera->projection);
	lightpassFS.inverseView = glm::inverse(camera->view);
	lightpassFS.combined = camera->combined;
	lightpassFS.lightColorIntensity = glm::vec4(lightColor, lightIntensity);
	lightpassFS.cascadeCount = activeCascades;
	lightpassFS.visualize = visualizeCascades ? 1 : 0;
	lightpassFS.brdfFunc = BRDFFunc;
	uniformBuffers["lightpassFS"].copyData(&lightpassFS, sizeof(lightpassFS));

	ssaoPassFSCam.inverseProjection = glm::inverse(camera->projection);
	ssaoPassFSCam.inverseView = glm::inverse(camera->view);
	ssaoPassFSCam.projection = camera->projection;
	ssaoPassFSCam.inverseCombined = glm::inverse(camera->combined);
	ssaoPassFSCam.cameraPos = glm::vec4(camera->position, 1.0);
	ssaoPassFSCam.view = camera->view;
	ssaoPassFSCam.jitter = Subsample * glm::vec2(1980, 1080);
	uniformBuffers["ssaoFSCam"].copyData(&ssaoPassFSCam, sizeof(ssaoPassFSCam));

	//uniformBuffers["cascadeFS"].copyData(&cascadeMatrices[0], sizeof(glm::mat4) * cascadeMatrices.size());


	SSRPassFS.cameraPos = glm::vec4(camera->position, 0.0);
	SSRPassFS.inverseCombined = glm::inverse(camera->combined);
	SSRPassFS.inverseProjection = glm::inverse(camera->projection);
	SSRPassFS.inverseView = glm::inverse(camera->view);
	SSRPassFS.combined = camera->combined;
	SSRPassFS.cameraScreen = glm::vec4(camera->nearPlane, camera->farPlane, Graphics::getWidth(), Graphics::getHeight());
	SSRPassFS.enableSharpen = sharpen ? 1 : 0;
	uniformBuffers["SSRFS"].copyData(&SSRPassFS , sizeof(SSRPassFS));
}

void grail::Renderer::initialize() {
	GRAIL_LOG(INFO, "RENDERER") << "Initializing renderer...";
	// Assign global variables
	this->mainGPU = VulkanContext::mainGPU;
	this->swapchain = VulkanContext::swapchain;
	this->instance = VulkanContext::instance;
	this->gpus = VulkanContext::gpus;

	this->graphicsQueue = mainGPU.graphicsQueue.handle;
	this->graphicsQueueIndex = mainGPU.graphicsQueue.index;

	this->depthFormat = mainGPU.depthFormat;
	this->colorFormat = mainGPU.colorFormat;
	this->colorSpace = mainGPU.colorSpace;

	this->cascades = std::vector<Cascade>(shadowMapCascadeSplits);


	pipelineFactory = new PipelineFactory();

	materialLibrary = new MaterialLibrary(pipelineFactory);

	concurency = std::thread::hardware_concurrency();
	GRAIL_LOG(INFO, "RENDERER") << "Available threads: " << concurency;
	
	threadPool = new ThreadPool();
	threadPool->allocateThreads(concurency);
}

void grail::Renderer::allocateMemory() {
	GRAIL_LOG(INFO, "RENDERER") << "Allocating memory...";

	/*stagingMemory = new MemoryRegion(mainGPU, 1024 * 1024 * 300, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, 1665);
	uniformMemory = new MemoryRegion(mainGPU, 1024 * 1024 * 32, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, 1665);
	bufferMemory = new MemoryRegion(mainGPU, 1024 * 1024 * 256, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, 1665);
	textureMemory = new MemoryRegion(mainGPU, 1024 * 1024 * 2000, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, 130);*/

	//stagingMemory = new MemoryRegion(mainGPU, "Staging Memory");
	//uniformMemory = new MemoryRegion(mainGPU, "Uniform Memory", 8);
	//bufferMemory = new MemoryRegion(mainGPU, "Buffer Memory", 32);
	//textureMemory = new MemoryRegion(mainGPU, "Texture Memory");

	Resources::setStagingMemoryAllocator(new MemoryAllocator(mainGPU, "Staging Memory", 64*1024*1024));
	Resources::setTextureMemoryAllocator(new MemoryAllocator(mainGPU, "Texture Memory", 128*1024*1024));
	Resources::setBufferMemoryAllocator(new MemoryAllocator(mainGPU, "Buffer Memory", 32*1024*1024));
	Resources::setUniformBufferMemoryAllocator(new MemoryAllocator(mainGPU, "Uniform Memory", 8*1024*1024));
}

void grail::Renderer::createCommandBuffers() {
	// COMMAND POOLS AND BUFFERS
	uint32_t imageCount = swapchain->imageCount;
	GRAIL_LOG(INFO, "RENDERER") << "Allocating command buffers...";
	{
		VkCommandPoolCreateInfo cmdPoolInfo = {};
		cmdPoolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
		cmdPoolInfo.pNext = nullptr;
		cmdPoolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT; // Reset command buffers from this pool after submit
		cmdPoolInfo.queueFamilyIndex = swapchain->queueNodeIndex;
		VK_VALIDATE_RESULT(vkCreateCommandPool(mainGPU.device, &cmdPoolInfo, nullptr, &renderCommandBufferPool));

		VkCommandBufferAllocateInfo cmdBufferInfo = {};
		cmdBufferInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		cmdBufferInfo.pNext = nullptr;
		cmdBufferInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
		cmdBufferInfo.commandBufferCount = 1;
		cmdBufferInfo.commandPool = renderCommandBufferPool;
		VK_VALIDATE_RESULT(vkAllocateCommandBuffers(mainGPU.device, &cmdBufferInfo, &setupCommandBuffer));
		VK_VALIDATE_RESULT(vkAllocateCommandBuffers(mainGPU.device, &cmdBufferInfo, &renderCommandBuffer));

		cmdBufferInfo.commandBufferCount = imageCount;
	}
}

void grail::Renderer::createSyncObjects() {
	uint32_t imageCount = swapchain->imageCount;

	GRAIL_LOG(INFO, "RENDERER") << "Creating sync objects...";
	{
		VkSemaphoreCreateInfo semaphoreInfo = {};
		semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
		semaphoreInfo.pNext = nullptr;
		semaphoreInfo.flags = 0;

		VK_VALIDATE_RESULT(vkCreateSemaphore(mainGPU.device, &semaphoreInfo, nullptr, &presentCompleteSemaphores[0]));
		VK_VALIDATE_RESULT(vkCreateSemaphore(mainGPU.device, &semaphoreInfo, nullptr, &presentCompleteSemaphores[1]));
		VK_VALIDATE_RESULT(vkCreateSemaphore(mainGPU.device, &semaphoreInfo, nullptr, &renderCompleteSemaphores[0]));
		VK_VALIDATE_RESULT(vkCreateSemaphore(mainGPU.device, &semaphoreInfo, nullptr, &renderCompleteSemaphores[1]));

		waitFences.resize(imageCount);
		VkFenceCreateInfo fenceInfo = {};
		fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
		fenceInfo.pNext = nullptr;
		fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

		for (VkFence& fence : waitFences) {
			VK_VALIDATE_RESULT(vkCreateFence(mainGPU.device, &fenceInfo, nullptr, &fence));
		}
	}
}

void grail::Renderer::createMeshes() {
	TextureCreateInfo info = {};
	info.generateMipmaps = false;
	info.bpp = 4;
	info.format = VK_FORMAT_R32G32B32A32_SFLOAT;

	//envMap = Resources::loadTexture("envMaps/cloudy.hdr", info);
	//envMap = Resources::loadTexture("envMaps/GCanyon_C_YumaPoint_3k.dds", info);


	GRAIL_LOG(INFO, "RENDERER") << "Creating meshes...";

	defaultVertexLayout = new VertexLayout({
		VertexAttribute(VertexAttributeType::POSITION, VK_FORMAT_R32G32B32_SFLOAT, 3, sizeof(float)),
		VertexAttribute(VertexAttributeType::NORMAL, VK_FORMAT_R32G32B32_SFLOAT, 3, sizeof(float)),
		VertexAttribute(VertexAttributeType::TANGENT, VK_FORMAT_R32G32B32_SFLOAT, 3, sizeof(float)),
		VertexAttribute(VertexAttributeType::UV, VK_FORMAT_R32G32_SFLOAT, 2, sizeof(float))
	});

	fullscreenVertexLayout = new VertexLayout({
		VertexAttribute(VertexAttributeType::POSITION, VK_FORMAT_R32G32B32_SFLOAT, 2, sizeof(float)),
		VertexAttribute(VertexAttributeType::UV, VK_FORMAT_R32G32_SFLOAT, 2, sizeof(float))
	});

	std::vector<float> vertices = {-1.0f, -1.0f, 0.0f, 0.0f, 1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, -1.0f, 1.0f, 0.0f, 1.0f};
    std::vector<uint32_t> indices = { 0, 1, 2, 2, 3, 0 };
	fullscreenMesh = Resources::createMesh("fullscreen_mesh",
		fullscreenVertexLayout,
		vertices,
		indices);
	{
		std::vector<float> verticesv = {
				-1.0f, -1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, // Bottom-left
				1.0f, 1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f, // top-right
				1.0f, -1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f, // bottom-right
				1.0f, 1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f,  // top-right
				-1.0f, -1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f,  // bottom-left
				-1.0f, 1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f,// top-left
				// Front face
				-1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, // bottom-left
				1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,  // bottom-right
				1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,  // top-right
				1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, // top-right
				-1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,  // top-left
				-1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,  // bottom-left
				// Left face
				-1.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f, // top-right
				-1.0f, 1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f, // top-left
				-1.0f, -1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f,  // bottom-left
				-1.0f, -1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f, // bottom-left
				-1.0f, -1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f,  // bottom-right
				-1.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f, // top-right
				// Right face
				1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, // top-left
				1.0f, -1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, // bottom-right
				1.0f, 1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, // top-right
				1.0f, -1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f,  // bottom-right
				1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,  // top-left
				1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, // bottom-left
				// Bottom face
				-1.0f, -1.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f, // top-right
				1.0f, -1.0f, -1.0f, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f, // top-left
				1.0f, -1.0f, 1.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f,// bottom-left
				1.0f, -1.0f, 1.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f, // bottom-left
				-1.0f, -1.0f, 1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, // bottom-right
				-1.0f, -1.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f, // top-right
				// Top face
				-1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,// top-left
				1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, // bottom-right
				1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, // top-right
				1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, // bottom-right
				-1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,// top-left
				-1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f // bottom-left
		};

		std::vector<uint32_t> indices = { 0, 1, 3, 3, 1, 2,
	1, 5, 2, 2, 5, 6,
	5, 4, 6, 6, 4, 7,
	4, 0, 7, 7, 0, 3,
	3, 2, 7, 7, 2, 6,
	4, 5, 0, 0, 5, 1 };

		VertexLayout* enviromentalLayout = new VertexLayout({
		VertexAttribute(VertexAttributeType::POSITION, VK_FORMAT_R32G32B32_SFLOAT, 3, sizeof(float)),
		VertexAttribute(VertexAttributeType::NORMAL, VK_FORMAT_R32G32B32_SFLOAT, 3, sizeof(float)),
		VertexAttribute(VertexAttributeType::UV, VK_FORMAT_R32G32_SFLOAT, 2, sizeof(float)),
			});

		Mesh* cubeMesh = Resources::createMesh("cubeMesh",
			enviromentalLayout,
			verticesv,
			indices);
	}
}

void grail::Renderer::createMaterials() {
	GRAIL_LOG(INFO, "RENDERER") << "Setting up pipeline data...";

	createUniformBuffer("gpass", sizeof(uboVS));
	createUniformBuffer("lightpassVS", sizeof(lightpassVS));
	createUniformBuffer("lightpassFS", sizeof(lightpassFS));
	createUniformBuffer("cascadeFS", sizeof(glm::mat4) * shadowMapCascadeSplits);
	createUniformBuffer("SSRFS", sizeof(SSRPassFS));

	std::default_random_engine rndEngine((unsigned)time(nullptr));
	std::uniform_real_distribution<float> rndDist(0.0f, 1.0f);

	const int SSAO_KERNEL_SIZE = 16;
	
	std::vector<glm::vec4> ssaoKernel(SSAO_KERNEL_SIZE);
	for (uint32_t i = 0; i < SSAO_KERNEL_SIZE; ++i) {
		glm::vec3 sample(rndDist(rndEngine) * 2.0 - 1.0, rndDist(rndEngine) * 2.0 - 1.0, rndDist(rndEngine));
		sample = glm::normalize(sample);
		sample *= rndDist(rndEngine);
		float scale = float(i) / float(SSAO_KERNEL_SIZE);
		scale = 0.1f + (scale * scale) * (1.0f - 0.1f);
		ssaoKernel[i] = glm::vec4(sample * scale, 0.0f);
	}

	createUniformBuffer("ssaoFS", SSAO_KERNEL_SIZE * sizeof(glm::vec4));
	uniformBuffers["ssaoFS"].copyData(&ssaoKernel[0], SSAO_KERNEL_SIZE * sizeof(glm::vec4));

	createUniformBuffer("ssaoFSCam", sizeof(ssaoPassFSCam));

	const int SSAO_NOISE_DIM = 4;
	std::vector<glm::vec4> ssaoNoise(SSAO_NOISE_DIM * SSAO_NOISE_DIM);
	for (uint32_t i = 0; i < static_cast<uint32_t>(ssaoNoise.size()); i++) {
		ssaoNoise[i] = glm::vec4(rndDist(rndEngine) * 2.0f - 1.0f, rndDist(rndEngine) * 2.0f - 1.0f, 0.0f, 0.0f);
	}

	TextureCreateInfo attachmentInfo = {};
	attachmentInfo.generateMipmaps = false;
	attachmentInfo.samplerInfo.addressModeU = SamplerAddressMode::REPEAT;
	attachmentInfo.samplerInfo.addressModeV = SamplerAddressMode::REPEAT;
	attachmentInfo.samplerInfo.magFilter = SamplerFilter::NEAREST;
	attachmentInfo.samplerInfo.minFilter = SamplerFilter::NEAREST;
	attachmentInfo.format = VK_FORMAT_R32G32B32A32_SFLOAT;
	VkTexture* ssaoNoiseTex = Resources::createTexture("ssaoNoise", SSAO_NOISE_DIM, SSAO_NOISE_DIM, ssaoNoise.data(), ssaoNoise.size() * sizeof(glm::vec4), attachmentInfo);

	//Resources::namedTextures.clear();
	/*{

		TextureCreateInfo attachmentInfo = {};
		attachmentInfo.generateMipmaps = true;
		attachmentInfo.samplerInfo.addressModeU = SamplerAddressMode::REPEAT;
		attachmentInfo.samplerInfo.addressModeV = SamplerAddressMode::REPEAT;
		attachmentInfo.samplerInfo.magFilter = SamplerFilter::LINEAR;
		attachmentInfo.samplerInfo.minFilter = SamplerFilter::LINEAR;
		attachmentInfo.format = VK_FORMAT_R8G8B8A8_UNORM;

		Resources::loadTexture("luts/bright_lut.png", attachmentInfo);

		RenderPassMaterialInfo passInfo;
		passInfo = RenderPassMaterialInfo{ { ShaderStageBits::FRAGMENT | ShaderStageBits::VERTEX, 0, sizeof(glm::vec4) }, "present", "present" };

		createPassMaterial("present_pass", passInfo,                 {
                { "color_texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("SSR_resolve") },
                { "velocity_buffer", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("gpass_velocity") },
                { "lut_texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, &Resources::getTexture("bump")->getDescriptor() },
        });
	}

    RenderPassMaterialInfo passInfo;
    passInfo = RenderPassMaterialInfo{ { ShaderStageBits::FRAGMENT | ShaderStageBits::VERTEX, 0, sizeof(glm::vec4) }, "present_fxaa", "present_fxaa" };
	createPassMaterial("fxaa_pass", 
		passInfo,
		{
			{ "color_texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("present") },
			{ "ui", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("ui") }
		}
	);

	passInfo = RenderPassMaterialInfo{ { ShaderStageBits::FRAGMENT, 0, sizeof(float) }, "present", "TAA_resolve" };
	createPassMaterial("SSR_pass",
		passInfo,
		{
			{ "uniform_buffer", DescriptorBindingType::BUFFER, VK_SHADER_STAGE_FRAGMENT_BIT, uniformDescriptor("SSRFS") },
			{ "color_texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("lightpass_accumulation") },
			{ "normal_texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("gpass_normals") },
			{ "velocity_texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("gpass_velocity") },
			{ "depth_texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("depth_halfRes") },
			{ "ssr_uvs", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("SSR_uvs") },
			{ "ssr_compute", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("SSR_uvs_compute") },
		}
	);

	/*createPassMaterial("SSR_uv_pass",
		RenderPassMaterialInfo{ { ShaderStageBits::FRAGMENT, 0, sizeof(glm::vec4) + sizeof(int) }, "present", "SSR_cast" },
		{
			{ "uniform_buffer", DescriptorBindingType::BUFFER, VK_SHADER_STAGE_FRAGMENT_BIT, uniformDescriptor("SSRFS") },
			{ "normal_texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("gpass_normals") },
			{ "albedo_texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("lightpass_accumulation") },
			{ "depth_texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("depth_halfRes") },
		}
	);*/
	/*passInfo = RenderPassMaterialInfo{ { ShaderStageBits::FRAGMENT, 0, sizeof(glm::vec4) + sizeof(glm::vec2) + sizeof(glm::mat4) + sizeof(glm::vec2) }, "present", "TAA" };
	createPassMaterial("TAA_pass",
		passInfo,
		{
			{ "color", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("SSR_resolve") },
			{ "color_history", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("lightpass_accumulation") },
			{ "velocity", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("gpass_velocity") },
			{ "depth", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("depth_prepass_depth") },
		}
	);

	passInfo = RenderPassMaterialInfo{ { ShaderStageBits::FRAGMENT, 0, sizeof(glm::vec2) }, "present", "ssao" };
	createPassMaterial("ssaopass",
		passInfo,
		{
			{ "uniform_bufferFS", DescriptorBindingType::BUFFER, VK_SHADER_STAGE_FRAGMENT_BIT, uniformDescriptor("ssaoFS") },
			{ "normals", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("gpass_normals") },
			{ "depth", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("depth_pyramid") },
			{ "randomNoise", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, &ssaoNoiseTex->getDescriptor() },
			{ "cam_uniform", DescriptorBindingType::BUFFER, VK_SHADER_STAGE_FRAGMENT_BIT, uniformDescriptor("ssaoFSCam") },
		}
	);

	passInfo = RenderPassMaterialInfo{ { ShaderStageBits::FRAGMENT, 0, sizeof(glm::vec2) }, "present", "ssao_blur" };
	createPassMaterial("ssaoblur",
		passInfo,
		{
			{ "ssao", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("ssao") },
			{ "depth", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("depth_pyramid") },
			{ "depthFS", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("depth_pyramid") },
		}
	);

	passInfo = RenderPassMaterialInfo{ {}, "present", "copy" };
	createPassMaterial("depth_copy",
		passInfo,
		{
			{ "texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("depth_prepass_depth") },
		}
	);

	passInfo = RenderPassMaterialInfo{ { ShaderStageBits::FRAGMENT, 0, sizeof(glm::vec2) }, "present", "depth_downsample" };
	createPassMaterial("depth_downsample",
		passInfo,
		{
			{ "lastMip", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("depth_pyramid", 0) },
		}
	);

	passInfo = RenderPassMaterialInfo{ { ShaderStageBits::FRAGMENT, 0, sizeof(glm::vec4) }, "light", "light" };
	createPassMaterial("lightpass",
		passInfo,
		{
			{ "uniform_bufferVS", DescriptorBindingType::BUFFER, VK_SHADER_STAGE_VERTEX_BIT, uniformDescriptor("lightpassVS") },
			{ "uniform_bufferFS", DescriptorBindingType::BUFFER, VK_SHADER_STAGE_FRAGMENT_BIT, uniformDescriptor("lightpassFS") },
			{ "albedo", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("gpass_albedo") },
			{ "normals", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("gpass_normals") },
			{ "envMap", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, &envMap->getDescriptor() },
			{ "brdfLUT", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, &Resources::getTexture("BRDF_LUT")->getDescriptor() },
			{ "envIrradiance", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, &Resources::getTexture("IrradianceCube")->getDescriptor() },
			{ "envSpec", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, &Resources::getTexture("SpecularCube")->getDescriptor() },
			{ "depth", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("depth_pyramid") },
			{ "ssao", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("ssao_blurred") },
			{ "cascadeMap", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("shadow_cascade_map") },
			{ "lastFrame", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("lightpass_accumulation") },
			{ "SSR", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("SSR_uvs") },
		}
	);

	passInfo = RenderPassMaterialInfo{ { }, "", "", "compute_test" };
	createPassMaterial("compute_test",
		passInfo,
		{
			{ "uniform_buffer", DescriptorBindingType::BUFFER, VK_SHADER_STAGE_COMPUTE_BIT, uniformDescriptor("SSRFS") },
			{ "normal_texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_COMPUTE_BIT, attachmentDescriptor("gpass_normals") },
			{ "albedo_texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_COMPUTE_BIT, attachmentDescriptor("lightpass_accumulation") },
			{ "depth_texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_COMPUTE_BIT, attachmentDescriptor("depth_pyramid") },
			{ "SSR_uvs_compute", DescriptorBindingType::STORAGE_TEXTURE, VK_SHADER_STAGE_COMPUTE_BIT, attachmentDescriptor("SSR_uvs_compute") },
			{ "gpass_albedo", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_COMPUTE_BIT, attachmentDescriptor("gpass_albedo") },
			{ "brdfLUT", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_COMPUTE_BIT, &Resources::getTexture("BRDF_LUT")->getDescriptor() }
		}
	);

	/*createPassMaterial("bloom_pass",
		RenderPassMaterialInfo{ {}, "present", "bloom", "" },
		{
			{ "texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("lightpass_accumulation") },
		}
		);


	createPassMaterial("bloom_pass_1", "bloom_pass_1/1",
		RenderPassMaterialInfo{ { ShaderStageBits::FRAGMENT, 0, sizeof(float) }, "present", "blur", "" },
		{
			{ "texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("bloom_1") },
		}
	);

	createPassMaterial("bloom_pass_1", "bloom_pass_1/2",
		RenderPassMaterialInfo{ { ShaderStageBits::FRAGMENT, 0, sizeof(float) }, "present", "blur", "" },
		{
			{ "texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("bloom_temp") },
		}
		);

	createPassMaterial("bloom_pass_2", "bloom_pass_2/1",
		RenderPassMaterialInfo{ { ShaderStageBits::FRAGMENT, 0, sizeof(float) }, "present", "blur", "" },
		{
			{ "texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("bloom_2") },
		}
		);

	createPassMaterial("bloom_pass_2", "bloom_pass_2/2",
		RenderPassMaterialInfo{ { ShaderStageBits::FRAGMENT, 0, sizeof(float) }, "present", "blur", "" },
		{
			{ "texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("bloom_temp") },
		}
		);

	createPassMaterial("bloom_pass_3", "bloom_pass_3/1",
		RenderPassMaterialInfo{ { ShaderStageBits::FRAGMENT, 0, sizeof(float) }, "present", "blur", "" },
		{
			{ "texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("bloom_3") },
		}
		);

	createPassMaterial("bloom_pass_3", "bloom_pass_3/2",
		RenderPassMaterialInfo{ { ShaderStageBits::FRAGMENT, 0, sizeof(float) }, "present", "blur", "" },
		{
			{ "texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("bloom_temp") },
		}
		);

	createPassMaterial("bloom_pass_4", "bloom_pass_4/1",
		RenderPassMaterialInfo{ { ShaderStageBits::FRAGMENT, 0, sizeof(float) }, "present", "blur", "" },
		{
			{ "texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("bloom_4") },
		}
		);

	createPassMaterial("bloom_pass_4", "bloom_pass_4/2",
		RenderPassMaterialInfo{ { ShaderStageBits::FRAGMENT, 0, sizeof(float) }, "present", "blur", "" },
		{
			{ "texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, attachmentDescriptor("bloom_temp") },
		}
		);*/

	// DEPTH PREPASS
	/*{

		PushConstantRange pushConstantRange = {};
		pushConstantRange.stageFlags = ShaderStageBits::FRAGMENT | ShaderStageBits::VERTEX;
		pushConstantRange.size = (sizeof(glm::mat4) * 2) + sizeof(glm::vec4) + sizeof(glm::vec2) + sizeof(uint32_t);
		pushConstantRange.offset = 0;

		GraphicsPipelineCreateInfo pipelineInfo = {};
		pipelineInfo.colorBlendState.attachmentCount = 0;
		pipelineInfo.renderPass = renderChain->getStep("depth_prepass").renderPass;
		pipelineInfo.shaderStageState.set(ShaderStageBits::VERTEX, Resources::loadShader("shaders/gpass_depth.vert.spv"));
		pipelineInfo.shaderStageState.set(ShaderStageBits::FRAGMENT, Resources::loadShader("shaders/gpass_depth.frag.spv"));
		pipelineInfo.rasterizationState.cullMode = CullMode::NONE;
		MaterialCreateInfo createInfo = {
			defaultVertexLayout,
			DescriptorSetGroup(pushConstantRange,{
				{
					{ "uniform_buffer", DescriptorBindingType::BUFFER, VK_SHADER_STAGE_VERTEX_BIT },
					{ "albedo_texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT },
					{ "normal_texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT },
					{ "roughness_texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT },
					{ "metallic_texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT }
				}
			}),
			pipelineInfo,
			pipelineFactory,
			MaterialType::MAT_DEFERRED
		};
		Material* material = new Material(createInfo);
		material->getDescriptors().getBinding("uniform_buffer").setDescriptor(&uniformBuffers["gpass"].descriptor);
		material->getDescriptors().getBinding("albedo_texture").setDescriptor(&defaultAlbedo->getDescriptor());
		material->getDescriptors().getBinding("normal_texture").setDescriptor(&defaultNormals->getDescriptor());
		material->getDescriptors().getBinding("roughness_texture").setDescriptor(&defaultRoughness->getDescriptor());
		material->getDescriptors().getBinding("metallic_texture").setDescriptor(&defaultMetallic->getDescriptor());
		material->getDescriptors().update();

		//materialLibrary->registerBaseMaterial("depth_prepass", material);

		//depthPrepassMaterial = materialLibrary->createMaterial("depth_prepass");
	}

	// CASCADE PASS
	{

		PushConstantRange pushConstantRange = {};
		pushConstantRange.stageFlags = ShaderStageBits::FRAGMENT | ShaderStageBits::VERTEX;
		pushConstantRange.size = (sizeof(glm::mat4) * 2) + sizeof(glm::vec4) + sizeof(glm::vec2) + sizeof(uint32_t);
		pushConstantRange.offset = 0;

		GraphicsPipelineCreateInfo pipelineInfo = {};
		pipelineInfo.colorBlendState.attachmentCount = 0;
		pipelineInfo.renderPass = renderChain->getStep("shadow_cascade_pass").renderPass;
		pipelineInfo.shaderStageState.set(ShaderStageBits::VERTEX, Resources::loadShader("shaders/cascade.vert.spv"));
		pipelineInfo.shaderStageState.set(ShaderStageBits::FRAGMENT, Resources::loadShader("shaders/cascade.frag.spv"));
		pipelineInfo.rasterizationState.cullMode = CullMode::BACK;
		pipelineInfo.depthStencilState.depthCompareOp = CompareOp::LESS_OR_EQUAL;
		pipelineInfo.rasterizationState.depthClampEnable = true;
		MaterialCreateInfo createInfo = {
			defaultVertexLayout,
			DescriptorSetGroup(pushConstantRange,{
				{
					{ "uniform_buffer", DescriptorBindingType::BUFFER, VK_SHADER_STAGE_VERTEX_BIT },
					{ "albedo_texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT },
					{ "normal_texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT },
					{ "roughness_texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT },
					{ "metallic_texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT }
				}
			}),
			pipelineInfo,
			pipelineFactory,
			MaterialType::MAT_DEFERRED
		};
		Material* material = new Material(createInfo);
		material->getDescriptors().getSet(0).getBinding(0).setDescriptor(&uniformBuffers["gpass"].descriptor);
		material->getDescriptors().getBinding("albedo_texture").setDescriptor(&defaultAlbedo->getDescriptor());
		material->getDescriptors().getBinding("normal_texture").setDescriptor(&defaultNormals->getDescriptor());
		material->getDescriptors().getBinding("roughness_texture").setDescriptor(&defaultRoughness->getDescriptor());
		material->getDescriptors().getBinding("metallic_texture").setDescriptor(&defaultMetallic->getDescriptor());
		material->getDescriptors().update();

		cascadeMaterial = material;
	}

	// GPASS
	{
		PushConstantRange pushConstantRange = {};
		pushConstantRange.stageFlags = ShaderStageBits::FRAGMENT | ShaderStageBits::VERTEX;
		pushConstantRange.size = (sizeof(glm::mat4) * 2) + sizeof(glm::vec4) + sizeof(glm::vec2) + sizeof(uint32_t);
		pushConstantRange.offset = 0;

		GraphicsPipelineCreateInfo pipelineInfo = {};
		pipelineInfo.colorBlendState.attachmentCount = 3;
		pipelineInfo.renderPass = renderChain->getStep("gpass").renderPass;
		pipelineInfo.shaderStageState.set(ShaderStageBits::VERTEX, Resources::loadShader("shaders/gpass.vert.spv"));
		pipelineInfo.shaderStageState.set(ShaderStageBits::FRAGMENT, Resources::loadShader("shaders/gpass.frag.spv"));
		pipelineInfo.rasterizationState.cullMode = CullMode::NONE;
		pipelineInfo.depthStencilState.depthCompareOp = CompareOp::EQUAL;
		pipelineInfo.depthStencilState.depthWriteEnable = false;
		MaterialCreateInfo createInfo = {
			defaultVertexLayout,
			DescriptorSetGroup(pushConstantRange,{
				{
					{ "uniform_buffer", DescriptorBindingType::BUFFER, VK_SHADER_STAGE_VERTEX_BIT },
					{ "albedo_texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT },
					{ "normal_texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT },
					{ "roughness_texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT },
					{ "metallic_texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT }
				}
			}),
			pipelineInfo,
			pipelineFactory,
			MaterialType::MAT_DEFERRED
		};
		Material* material = new Material(createInfo);
		material->getDescriptors().getBinding("uniform_buffer").setDescriptor(&uniformBuffers["gpass"].descriptor);
		material->getDescriptors().getBinding("albedo_texture").setDescriptor(&defaultAlbedo->getDescriptor());
		material->getDescriptors().getBinding("normal_texture").setDescriptor(&defaultNormals->getDescriptor());
		material->getDescriptors().getBinding("roughness_texture").setDescriptor(&defaultRoughness->getDescriptor());
		material->getDescriptors().getBinding("metallic_texture").setDescriptor(&defaultMetallic->getDescriptor());
		material->getDescriptors().update();

		//materialLibrary->registerBaseMaterial("default_deferred", material);
	}*/
}
void grail::Renderer::generateBRDFLUT() {
	uint32_t dimensions = 512;
	
	RenderChain tempChain = RenderChain();

	TextureCreateInfo attachmentInfo = {};
	attachmentInfo.generateMipmaps = false;
	attachmentInfo.samplerInfo.addressModeU = SamplerAddressMode::CLAMP_TO_EDGE;
	attachmentInfo.samplerInfo.addressModeV = SamplerAddressMode::CLAMP_TO_EDGE;
	attachmentInfo.usageBits = ImageUsageBits::COLOR_ATTACHMENT_BIT | ImageUsageBits::SAMPLED_BIT;
	attachmentInfo.format = VK_FORMAT_R32G32_SFLOAT;

	RenderStepAttachmentCreateInfo stepInfo;
	stepInfo = RenderStepAttachmentCreateInfo{ attachmentInfo, dimensions, dimensions };
	tempChain.createAttachment("BRDF_LUT", stepInfo);

	RenderStepDescription description = RenderStepDescription{ dimensions, dimensions, 2, 0,{ "BRDF_LUT" }, "" };
	tempChain.describeStep("BRDF_LUT_generation", description);

	tempChain.finalizeChain();
	
	GraphicsPipelineCreateInfo pipelineInfo = {};
	pipelineInfo.colorBlendState.attachmentCount = 1;
	pipelineInfo.renderPass = tempChain.getStep("BRDF_LUT_generation").renderPass;
	/*pipelineInfo.shaderStageState.set(ShaderStageBits::VERTEX, Resources::loadShader("shaders/present.vert.spv"));
	pipelineInfo.shaderStageState.set(ShaderStageBits::FRAGMENT, Resources::loadShader("shaders/brdfLUT.frag.spv"));*/
	pipelineInfo.rasterizationState.cullMode = CullMode::NONE;
	/*MaterialCreateInfo createInfo = {
		fullscreenVertexLayout,
		DescriptorSetGroup({}, { { { "color_texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT }} }),
		pipelineInfo,
		pipelineFactory,
		MaterialType::MAT_POST_PROCESS
	};

	Material* m = new Material(createInfo);
	m->getDescriptors().getBinding("color_texture").setDescriptor(&tempChain.getAttachment("BRDF_LUT").texture->getDescriptor());
	m->update();*/

	RenderStep* step = tempChain.beginChain();
	{
		VkCommandBuffer cmdBuff = step->cmdBuffers[0];


		step->setupStep(cmdBuff);

		//renderMesh(cmdBuff, fullscreenMesh, m, nullptr);
		step->endStep(mainGPU.graphicsQueue.handle, cmdBuff);
	}
}

void grail::Renderer::generateIrradianceCube() {
    GRAIL_LOG(INFO, "RENDERER") << "Generating irradiance cube...";

	uint32_t dimensions = 32;
	//uint32_t mipCount = static_cast<uint32_t>(std::floor(std::log2(dimensions))) + 1;

	TextureCreateInfo info = {};
	info.generateMipmaps = false;
	//info.mipLevels = mipCount;
	info.arrayLevels = 6;
	info.createFlags = VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;
	info.samplerInfo.addressModeU = SamplerAddressMode::CLAMP_TO_EDGE;
	info.samplerInfo.addressModeV = SamplerAddressMode::CLAMP_TO_EDGE;
	info.usageBits = ImageUsageBits::TRANSFER_DST_BIT | ImageUsageBits::SAMPLED_BIT;
	info.format = VK_FORMAT_R32G32B32A32_SFLOAT;

	VkTexture* cubemap = Resources::createTexture("IrradianceCube", dimensions, dimensions, info);
	cubemap->getDescriptor().imageLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;

	TextureCreateInfo attachmentInfo = {};
	attachmentInfo.generateMipmaps = false;
	attachmentInfo.samplerInfo.addressModeU = SamplerAddressMode::CLAMP_TO_EDGE;
	attachmentInfo.samplerInfo.addressModeV = SamplerAddressMode::CLAMP_TO_EDGE;
	attachmentInfo.usageBits = ImageUsageBits::COLOR_ATTACHMENT_BIT | ImageUsageBits::TRANSFER_SRC_BIT | ImageUsageBits::SAMPLED_BIT;
	attachmentInfo.format = VK_FORMAT_R32G32B32A32_SFLOAT;

	RenderChain tempChain = RenderChain();

	RenderStepAttachmentCreateInfo stepInfo = RenderStepAttachmentCreateInfo{ attachmentInfo, dimensions, dimensions };
	tempChain.createAttachment("IrradianceFace", stepInfo);

	RenderStepDescription description = RenderStepDescription{ dimensions, dimensions, 1, 0,{ "IrradianceFace" }, "" };
	tempChain.describeStep("IrradianceGeneration", description);

	tempChain.finalizeChain();

	struct PushBlock {
		glm::mat4 mvp;
		// Sampling deltas
		float deltaPhi = ((2.0f * float(glm::pi<float>())) / 180.0f);
		float deltaTheta = ((0.5f * float(glm::pi<float>())) / 64.0f);
	} pushBlock;

	PushConstantRange ps = {};
	ps.offset = 0;
	ps.size = sizeof(PushBlock);
	ps.stageFlags = ShaderStageBits::FRAGMENT | ShaderStageBits::VERTEX;

	VertexLayout* enviromentalLayout = new VertexLayout({
		VertexAttribute(VertexAttributeType::POSITION, VK_FORMAT_R32G32B32_SFLOAT, 3, sizeof(float)),
		VertexAttribute(VertexAttributeType::NORMAL, VK_FORMAT_R32G32B32_SFLOAT, 3, sizeof(float)),
		VertexAttribute(VertexAttributeType::UV, VK_FORMAT_R32G32_SFLOAT, 2, sizeof(float)),
	});

	GraphicsPipelineCreateInfo pipelineInfo = {};
	pipelineInfo.colorBlendState.attachmentCount = 1;
	pipelineInfo.renderPass = tempChain.getStep("IrradianceGeneration").renderPass;
	/*pipelineInfo.shaderStageState.set(ShaderStageBits::VERTEX, Resources::loadShader("shaders/cube.vert.spv"));
	pipelineInfo.shaderStageState.set(ShaderStageBits::FRAGMENT, Resources::loadShader("shaders/irradiance.frag.spv"));*/
	pipelineInfo.rasterizationState.cullMode = CullMode::NONE;
	/*MaterialCreateInfo createInfo = {
		enviromentalLayout,
		DescriptorSetGroup(ps, { { { "color_texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT } } }),
		pipelineInfo,
		pipelineFactory,
		MaterialType::MAT_POST_PROCESS
	};

	Material* mat = new Material(createInfo);
	mat->getDescriptors().getBinding("color_texture").setDescriptor(&envMap->getDescriptor());
	mat->update();*/

	std::vector<glm::mat4> matrices = {
		// POSITIVE_X
		glm::rotate(glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f)), glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
		// NEGATIVE_X
		glm::rotate(glm::rotate(glm::mat4(1.0f), glm::radians(-90.0f), glm::vec3(0.0f, 1.0f, 0.0f)), glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
		// POSITIVE_Y
		glm::rotate(glm::mat4(1.0f), glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
		// NEGATIVE_Y
		glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
		// POSITIVE_Z
		glm::rotate(glm::mat4(1.0f), glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
		// NEGATIVE_Z
		glm::rotate(glm::mat4(1.0f), glm::radians(180.0f), glm::vec3(0.0f, 0.0f, 1.0f)),
	};

	std::vector<float> vertices = {
            -1.0f, -1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, // Bottom-left
            1.0f, 1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f, // top-right
            1.0f, -1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f, // bottom-right
            1.0f, 1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f,  // top-right
            -1.0f, -1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f,  // bottom-left
            -1.0f, 1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f,// top-left
            // Front face
            -1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, // bottom-left
            1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,  // bottom-right
            1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,  // top-right
            1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, // top-right
            -1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,  // top-left
            -1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,  // bottom-left
            // Left face
            -1.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f, // top-right
            -1.0f, 1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f, // top-left
            -1.0f, -1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f,  // bottom-left
            -1.0f, -1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f, // bottom-left
            -1.0f, -1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f,  // bottom-right
            -1.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f, // top-right
            // Right face
            1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, // top-left
            1.0f, -1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, // bottom-right
            1.0f, 1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, // top-right
            1.0f, -1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f,  // bottom-right
            1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,  // top-left
            1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, // bottom-left
            // Bottom face
            -1.0f, -1.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f, // top-right
            1.0f, -1.0f, -1.0f, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f, // top-left
            1.0f, -1.0f, 1.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f,// bottom-left
            1.0f, -1.0f, 1.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f, // bottom-left
            -1.0f, -1.0f, 1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, // bottom-right
            -1.0f, -1.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f, // top-right
            // Top face
            -1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,// top-left
            1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, // bottom-right
            1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, // top-right
            1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, // bottom-right
            -1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,// top-left
            -1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f // bottom-left
	};

    std::vector<uint32_t> indices = { 0, 1, 2, 2, 3, 0 };

	Mesh* cubeMesh = Resources::createMesh("cubeMesh",
		enviromentalLayout,
		vertices,
		indices);

	RenderStep* step = tempChain.beginChain();
	{
		VkCommandBuffer cmdBuff = step->cmdBuffers[0];

		vkBeginCommandBuffer(cmdBuff, &step->defaultBeginInfo);

		vkCmdSetViewport(cmdBuff, 0, 1, &step->defaultViewport);
		vkCmdSetScissor(cmdBuff, 0, 1, &step->defaultScissor);

		VkImageSubresourceRange subresourceRange = {};
		subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		subresourceRange.baseMipLevel = 0;
		subresourceRange.levelCount = 1;
		subresourceRange.layerCount = 6;

		cubemap->setImageLayout(cmdBuff, VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, subresourceRange);
	
		//for (uint32_t m = 0; m < mipCount; m++) {
			for (uint32_t f = 0; f < 6; f++) {
				step->defaultViewport.width = static_cast<float>(dimensions);
				step->defaultViewport.height = static_cast<float>(dimensions);
				vkCmdSetViewport(cmdBuff, 0, 1, &step->defaultViewport);

				vkCmdBeginRenderPass(cmdBuff, &step->defaultRenderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

				pushBlock.mvp = glm::perspective((float)(glm::pi<float>() / 2.0), 1.0f, 0.1f, 512.0f) * matrices[f];

				//vkCmdPushConstants(cmdBuff, mat->getDescriptors().getPipelineLayout(), VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT, 0, sizeof(PushBlock), &pushBlock);

				//vkCmdBindPipeline(cmdBuff, VK_PIPELINE_BIND_POINT_GRAPHICS, mat->getPipeline());
				//vkCmdBindDescriptorSets(cmdBuff, VK_PIPELINE_BIND_POINT_GRAPHICS, mat->getDescriptors().getPipelineLayout(), 0, 1, mat->getDescriptors().getSetPtr(), 0, nullptr);

				vkCmdBindVertexBuffers(cmdBuff, 0, 1, &cubeMesh->buffer, &cubeMesh->vertexOffset);
				//vkCmdBindIndexBuffer(cmdBuff, cubeMesh->buffer, cube->mesh->indexOffset, VK_INDEX_TYPE_UINT32);

				vkCmdDraw(cmdBuff, cubeMesh->verticesCount, 1, 0, 1);
				//vkCmdDrawIndexed(cmdBuff, cube->mesh->indicesCount, 1, 0, 0, 1);

				vkCmdEndRenderPass(cmdBuff);

				tempChain.getAttachment("IrradianceFace").texture->setImageLayout(cmdBuff, VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
				{ VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 });

				VkImageCopy copyRegion = {};

				copyRegion.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
				copyRegion.srcSubresource.baseArrayLayer = 0;
				copyRegion.srcSubresource.mipLevel = 0;
				copyRegion.srcSubresource.layerCount = 1;
				copyRegion.srcOffset = { 0, 0, 0 };

				copyRegion.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
				copyRegion.dstSubresource.baseArrayLayer = f;
				copyRegion.dstSubresource.mipLevel = 0;
				copyRegion.dstSubresource.layerCount = 1;
				copyRegion.dstOffset = { 0, 0, 0 };

				copyRegion.extent.width = static_cast<uint32_t>(step->defaultViewport.width);
				copyRegion.extent.height = static_cast<uint32_t>(step->defaultViewport.height);
				copyRegion.extent.depth = 1;

				vkCmdCopyImage(
					cmdBuff,
					tempChain.getAttachment("IrradianceFace").texture->getImage(),
					VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
					cubemap->getImage(),
					VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
					1,
					&copyRegion);

				tempChain.getAttachment("IrradianceFace").texture->setImageLayout(cmdBuff, VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
				{ VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 });
			}
		//}

		cubemap->setImageLayout(cmdBuff, VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, subresourceRange);
		cubemap->getDescriptor().imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

		step->defaultSubmitInfo.commandBufferCount = 1;
		step->defaultSubmitInfo.pCommandBuffers = &cmdBuff;
		VK_VALIDATE_RESULT(vkEndCommandBuffer(cmdBuff));

		VK_VALIDATE_RESULT(vkQueueSubmit(mainGPU.graphicsQueue.handle, 1, &step->defaultSubmitInfo, VK_NULL_HANDLE));
	}
}
void grail::Renderer::generatePrefilteredCube() {
    GRAIL_LOG(INFO, "RENDERER") << "Generating specular cube...";

	uint32_t dimensions = 64;
	uint32_t mipCount = static_cast<uint32_t>(std::floor(std::log2(dimensions))) + 1;

	TextureCreateInfo info = {};
	info.generateMipmaps = true;
	info.mipLevels = mipCount;
	info.arrayLevels = 6;
	info.createFlags = VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;
	info.samplerInfo.addressModeU = SamplerAddressMode::CLAMP_TO_EDGE;
	info.samplerInfo.addressModeV = SamplerAddressMode::CLAMP_TO_EDGE;
	info.usageBits = ImageUsageBits::TRANSFER_DST_BIT | ImageUsageBits::SAMPLED_BIT;
	info.format = VK_FORMAT_R32G32B32A32_SFLOAT;

	VkTexture* cubemap = Resources::createTexture("SpecularCube", dimensions, dimensions, info);
	cubemap->getDescriptor().imageLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;

	TextureCreateInfo attachmentInfo = {};
	attachmentInfo.generateMipmaps = false;
	attachmentInfo.samplerInfo.addressModeU = SamplerAddressMode::CLAMP_TO_EDGE;
	attachmentInfo.samplerInfo.addressModeV = SamplerAddressMode::CLAMP_TO_EDGE;
	attachmentInfo.usageBits = ImageUsageBits::COLOR_ATTACHMENT_BIT | ImageUsageBits::TRANSFER_SRC_BIT | ImageUsageBits::SAMPLED_BIT;
	attachmentInfo.format = VK_FORMAT_R32G32B32A32_SFLOAT;

	RenderChain tempChain = RenderChain();
    RenderStepAttachmentCreateInfo stepInfo = RenderStepAttachmentCreateInfo{ attachmentInfo, dimensions, dimensions };
	tempChain.createAttachment("SpecularFace", stepInfo);

    RenderStepDescription description = RenderStepDescription{ dimensions, dimensions, 1, 0,{ "SpecularFace" }, "" };
	tempChain.describeStep("SpecularGeneration", description);

	tempChain.finalizeChain();

	struct PushBlock {
		glm::mat4 mvp;
		// Sampling deltas
		float roughness = 0.0;
		uint32_t numSamples = 512;
	} pushBlock;

	PushConstantRange ps = {};
	ps.offset = 0;
	ps.size = sizeof(PushBlock);
	ps.stageFlags = ShaderStageBits::FRAGMENT | ShaderStageBits::VERTEX;

	VertexLayout* enviromentalLayout = new VertexLayout({
		VertexAttribute(VertexAttributeType::POSITION, VK_FORMAT_R32G32B32_SFLOAT, 3, sizeof(float)),
		VertexAttribute(VertexAttributeType::NORMAL, VK_FORMAT_R32G32B32_SFLOAT, 3, sizeof(float)),
		VertexAttribute(VertexAttributeType::UV, VK_FORMAT_R32G32_SFLOAT, 2, sizeof(float)),
	});

	GraphicsPipelineCreateInfo pipelineInfo = {};
	pipelineInfo.colorBlendState.attachmentCount = 1;
	pipelineInfo.renderPass = tempChain.getStep("SpecularGeneration").renderPass;
	/*pipelineInfo.shaderStageState.set(ShaderStageBits::VERTEX, Resources::loadShader("shaders/cube.vert.spv"));
	pipelineInfo.shaderStageState.set(ShaderStageBits::FRAGMENT, Resources::loadShader("shaders/prefilterSpec.frag.spv"));*/
	pipelineInfo.rasterizationState.cullMode = CullMode::NONE;
	/*MaterialCreateInfo createInfo = {
		enviromentalLayout,
		DescriptorSetGroup(ps,{ { { "color_texture", DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT } } }),
		pipelineInfo,
		pipelineFactory,
		MaterialType::MAT_POST_PROCESS
	};

	Material* mat = new Material(createInfo);
	mat->getDescriptors().getBinding("color_texture").setDescriptor(&envMap->getDescriptor());
	mat->update();*/

	std::vector<glm::mat4> matrices = {
		// POSITIVE_X
		glm::rotate(glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f)), glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
		// NEGATIVE_X
		glm::rotate(glm::rotate(glm::mat4(1.0f), glm::radians(-90.0f), glm::vec3(0.0f, 1.0f, 0.0f)), glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
		// POSITIVE_Y
		glm::rotate(glm::mat4(1.0f), glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
		// NEGATIVE_Y
		glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
		// POSITIVE_Z
		glm::rotate(glm::mat4(1.0f), glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
		// NEGATIVE_Z
		glm::rotate(glm::mat4(1.0f), glm::radians(180.0f), glm::vec3(0.0f, 0.0f, 1.0f)),
	};

    std::vector<float> vertices = { // Back face
            -1.0f, -1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, // Bottom-left
            1.0f, 1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f, // top-right
            1.0f, -1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f, // bottom-right
            1.0f, 1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f,  // top-right
            -1.0f, -1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f,  // bottom-left
            -1.0f, 1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f,// top-left
            // Front face
            -1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, // bottom-left
            1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,  // bottom-right
            1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,  // top-right
            1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, // top-right
            -1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,  // top-left
            -1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,  // bottom-left
            // Left face
            -1.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f, // top-right
            -1.0f, 1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f, // top-left
            -1.0f, -1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f,  // bottom-left
            -1.0f, -1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f, // bottom-left
            -1.0f, -1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f,  // bottom-right
            -1.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f, // top-right
            // Right face
            1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, // top-left
            1.0f, -1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, // bottom-right
            1.0f, 1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, // top-right
            1.0f, -1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f,  // bottom-right
            1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,  // top-left
            1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, // bottom-left
            // Bottom face
            -1.0f, -1.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f, // top-right
            1.0f, -1.0f, -1.0f, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f, // top-left
            1.0f, -1.0f, 1.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f,// bottom-left
            1.0f, -1.0f, 1.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f, // bottom-left
            -1.0f, -1.0f, 1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, // bottom-right
            -1.0f, -1.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f, // top-right
            // Top face
            -1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,// top-left
            1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, // bottom-right
            1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, // top-right
            1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, // bottom-right
            -1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,// top-left
            -1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f // bottom-left
    };

    std::vector<uint32_t> indices = { 0, 1, 2, 2, 3, 0 };

	Mesh* cubeMesh = Resources::createMesh("cubeMesh",
		enviromentalLayout,
		vertices,
		indices);

	RenderStep* step = tempChain.beginChain();
	{
		VkCommandBuffer cmdBuff = step->cmdBuffers[0];

		vkBeginCommandBuffer(cmdBuff, &step->defaultBeginInfo);

		vkCmdSetViewport(cmdBuff, 0, 1, &step->defaultViewport);
		vkCmdSetScissor(cmdBuff, 0, 1, &step->defaultScissor);

		VkImageSubresourceRange subresourceRange = {};
		subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		subresourceRange.baseMipLevel = 0;
		subresourceRange.levelCount = mipCount;
		subresourceRange.layerCount = 6;

		cubemap->setImageLayout(cmdBuff, VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, subresourceRange);

		for (uint32_t m = 0; m < mipCount; m++) {
			pushBlock.roughness = (float)m / (float)(mipCount - 1);
			for (uint32_t f = 0; f < 6; f++) {

				step->defaultViewport.width = static_cast<float>(dimensions * std::pow(0.5f, m));
				step->defaultViewport.height = static_cast<float>(dimensions * std::pow(0.5f, m));
				vkCmdSetViewport(cmdBuff, 0, 1, &step->defaultViewport);

				vkCmdBeginRenderPass(cmdBuff, &step->defaultRenderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

				pushBlock.mvp = glm::perspective((float)(glm::pi<float>() / 2.0), 1.0f, 0.1f, 512.0f) * matrices[f];

				//vkCmdPushConstants(cmdBuff, mat->getDescriptors().getPipelineLayout(), VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT, 0, sizeof(PushBlock), &pushBlock);

				//vkCmdBindPipeline(cmdBuff, VK_PIPELINE_BIND_POINT_GRAPHICS, mat->getPipeline());
				//vkCmdBindDescriptorSets(cmdBuff, VK_PIPELINE_BIND_POINT_GRAPHICS, mat->getDescriptors().getPipelineLayout(), 0, 1, mat->getDescriptors().getSetPtr(), 0, nullptr);

				vkCmdBindVertexBuffers(cmdBuff, 0, 1, &cubeMesh->buffer, &cubeMesh->vertexOffset);
				//vkCmdBindIndexBuffer(cmdBuff, cubeMesh->buffer, cube->mesh->indexOffset, VK_INDEX_TYPE_UINT32);

				vkCmdDraw(cmdBuff, cubeMesh->verticesCount, 1, 0, 1);
				//vkCmdDrawIndexed(cmdBuff, cube->mesh->indicesCount, 1, 0, 0, 1);

				vkCmdEndRenderPass(cmdBuff);

				tempChain.getAttachment("SpecularFace").texture->setImageLayout(cmdBuff, VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
				{ VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 });

				VkImageCopy copyRegion = {};

				copyRegion.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
				copyRegion.srcSubresource.baseArrayLayer = 0;
				copyRegion.srcSubresource.mipLevel = 0;
				copyRegion.srcSubresource.layerCount = 1;
				copyRegion.srcOffset = { 0, 0, 0 };

				copyRegion.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
				copyRegion.dstSubresource.baseArrayLayer = f;
				copyRegion.dstSubresource.mipLevel = m;
				copyRegion.dstSubresource.layerCount = 1;
				copyRegion.dstOffset = { 0, 0, 0 };

				copyRegion.extent.width = static_cast<uint32_t>(step->defaultViewport.width);
				copyRegion.extent.height = static_cast<uint32_t>(step->defaultViewport.height);
				copyRegion.extent.depth = 1;

				vkCmdCopyImage(
					cmdBuff,
					tempChain.getAttachment("SpecularFace").texture->getImage(),
					VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
					cubemap->getImage(),
					VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
					1,
					&copyRegion);

				tempChain.getAttachment("SpecularFace").texture->setImageLayout(cmdBuff, VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
				{ VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 });
			}
		}

		cubemap->setImageLayout(cmdBuff, VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, subresourceRange);
		cubemap->getDescriptor().imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

		step->defaultSubmitInfo.commandBufferCount = 1;
		step->defaultSubmitInfo.pCommandBuffers = &cmdBuff;
		VK_VALIDATE_RESULT(vkEndCommandBuffer(cmdBuff));

		VK_VALIDATE_RESULT(vkQueueSubmit(mainGPU.graphicsQueue.handle, 1, &step->defaultSubmitInfo, VK_NULL_HANDLE));
	}
}
/******************  SHIT THAT I DON'T NEED RIGHT NOW, BUT MIGHT LOOK BACK ON FOR REFERENCE LATER  *********************/

/*

// PRESENT PASS
GRAIL_LOG(INFO, "RENDERER") << "Creating render passes...";
GRAIL_LOG(INFO, "RENDERER") << "PRESENT";
{
// Descriptors for the attachments used by this renderpass
std::array<VkAttachmentDescription, 1> attachments = {};

// Color attachment
attachments[0].format = colorFormat;									// Use the color format selected by the swapchain
attachments[0].samples = VK_SAMPLE_COUNT_1_BIT;									// We don't use multi sampling in this example
attachments[0].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;							// Clear this attachment at the start of the render pass
attachments[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;							// Keep it's contents after the render pass is finished (for displaying it)
attachments[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;					// We don't use stencil, so don't care for load
attachments[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;				// Same for store
attachments[0].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;						// Layout at render pass start. Initial doesn't matter, so we use undefined
attachments[0].finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;					// Layout to which the attachment is transitioned when the render pass is finished

// Setup attachment references
VkAttachmentReference colorReference = {};
colorReference.attachment = 0;													// Attachment 0 is color
colorReference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;				// Attachment layout used as color during the subpass
// Setup a single subpass reference
VkSubpassDescription subpassDescription = {};
subpassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
subpassDescription.colorAttachmentCount = 1;									// Subpass uses one color attachment
subpassDescription.pColorAttachments = &colorReference;							// Reference to the color attachment in slot 0
subpassDescription.pDepthStencilAttachment = nullptr;					// Reference to the depth attachment in slot 1
subpassDescription.inputAttachmentCount = 0;									// Input attachments can be used to sample from contents of a previous subpass
subpassDescription.pInputAttachments = nullptr;									// (Input attachments not used by this example)
subpassDescription.preserveAttachmentCount = 0;									// Preserved attachments can be used to loop (and preserve) attachments through subpasses
subpassDescription.pPreserveAttachments = nullptr;								// (Preserve attachments not used by this example)
subpassDescription.pResolveAttachments = nullptr;								// Resolve attachments are resolved at the end of a sub pass and can be used for e.g. multi sampling

// Setup subpass dependencies
// These will add the implicit ttachment layout transitionss specified by the attachment descriptions
// The actual usage layout is preserved through the layout specified in the attachment reference
// Each subpass dependency will introduce a memory and execution dependency between the source and dest subpass described by
// srcStageMask, dstStageMask, srcAccessMask, dstAccessMask (and dependencyFlags is set)
// Note: VK_SUBPASS_EXTERNAL is a special constant that refers to all commands executed outside of the actual renderpass)
std::array<VkSubpassDependency, 2> dependencies;

// First dependency at the start of the renderpass
// Does the transition from final to initial layout
dependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;								// Producer of the dependency
dependencies[0].dstSubpass = 0;													// Consumer is our single subpass that will wait for the execution depdendency
dependencies[0].srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
dependencies[0].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
dependencies[0].srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
dependencies[0].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
dependencies[0].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

// Second dependency at the end the renderpass
// Does the transition from the initial to the final layout
dependencies[1].srcSubpass = 0;													// Producer of the dependency is our single subpass
dependencies[1].dstSubpass = VK_SUBPASS_EXTERNAL;								// Consumer are all commands outside of the renderpass
dependencies[1].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
dependencies[1].dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
dependencies[1].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
dependencies[1].dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
dependencies[1].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

// Create the actual renderpass
VkRenderPassCreateInfo renderPassInfo = {};
renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
renderPassInfo.attachmentCount = static_cast<uint32_t>(attachments.size());		// Number of attachments used by this render pass
renderPassInfo.pAttachments = attachments.data();								// Descriptions of the attachments used by the render pass
renderPassInfo.subpassCount = 1;												// We only use one subpass in this example
renderPassInfo.pSubpasses = &subpassDescription;								// Description of that subpass
renderPassInfo.dependencyCount = static_cast<uint32_t>(dependencies.size());	// Number of subpass dependencies
renderPassInfo.pDependencies = dependencies.data();								// Subpass dependencies used by the render pass

VK_VALIDATE_RESULT(vkCreateRenderPass(mainGPU.device, &renderPassInfo, nullptr, &presentPass));
}

{
VkImageView imgAttachments[1];

VkFramebufferCreateInfo framebufferCreateInfo = {};
framebufferCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
framebufferCreateInfo.pNext = nullptr;
framebufferCreateInfo.renderPass = presentPass;
framebufferCreateInfo.attachmentCount = 1;
framebufferCreateInfo.pAttachments = &imgAttachments[0];
framebufferCreateInfo.width = Graphics::getWidth();
framebufferCreateInfo.height = Graphics::getHeight();
framebufferCreateInfo.layers = 1;

presentFramebuffers.resize(swapchain->imageCount);
for (uint32_t i = 0; i < presentFramebuffers.size(); i++) {
imgAttachments[0] = swapchain->buffers[i].view;
VK_VALIDATE_RESULT(vkCreateFramebuffer(mainGPU.device, &framebufferCreateInfo, nullptr, &presentFramebuffers[i]));
}
}

*/

/*
/*VkCommandBufferBeginInfo cmdBufferBeginInfo = {};
cmdBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
cmdBufferBeginInfo.pNext = nullptr;
cmdBufferBeginInfo.flags = 0;
cmdBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

VkClearValue clearValues[1];
clearValues[0].color = { { 0.0f, 0.0f, 0.0f, 1.0f } };

VkRenderPassBeginInfo renderPassBeginInfo = {};
renderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
renderPassBeginInfo.pNext = nullptr;
renderPassBeginInfo.renderPass = presentPass;
renderPassBeginInfo.renderArea.offset.x = 0;
renderPassBeginInfo.renderArea.offset.y = 0;
renderPassBeginInfo.renderArea.extent.width = Graphics::getWidth();
renderPassBeginInfo.renderArea.extent.height = Graphics::getHeight();
renderPassBeginInfo.clearValueCount = 1;
renderPassBeginInfo.pClearValues = &clearValues[0];
renderPassBeginInfo.framebuffer = presentFramebuffers[currentSwapchainBuffer];

VK_VALIDATE_RESULT(vkBeginCommandBuffer(renderCommandBuffer, &cmdBufferBeginInfo));

VkViewport viewport = {};
viewport.height = (float)Graphics::getHeight();
viewport.width = (float)Graphics::getWidth();
viewport.minDepth = (float) 0.0f;
viewport.maxDepth = (float) 1.0f;
vkCmdSetViewport(renderCommandBuffer, 0, 1, &viewport);

VkRect2D scissor = {};
scissor.extent.width = Graphics::getWidth();
scissor.extent.height = Graphics::getHeight();
scissor.offset.x = 0;
scissor.offset.y = 0;
vkCmdSetScissor(renderCommandBuffer, 0, 1, &scissor);


#ifdef MULTITHREADED_RENDERER

#else
vkCmdBeginRenderPass(renderCommandBuffer, &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);
renderMesh(cmdBuff, fullscreenMesh, presentMaterial, nullptr);
#endif
vkCmdEndRenderPass(renderCommandBuffer);
VK_VALIDATE_RESULT(vkEndCommandBuffer(renderCommandBuffer));

submitInfo = {};
submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
submitInfo.pNext = nullptr;
submitInfo.pWaitDstStageMask = &submitPipelineStages;
submitInfo.waitSemaphoreCount = 1;
submitInfo.pWaitSemaphores = &step->completeSemaphore;
submitInfo.signalSemaphoreCount = 1;
submitInfo.pSignalSemaphores = &renderCompleteSemaphore;
submitInfo.commandBufferCount = 1;
submitInfo.pCommandBuffers = &renderCommandBuffer;
VK_VALIDATE_RESULT(vkQueueSubmit(graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE));
}*/


/*// HIGHLY EXPERIMENTAL, HELP (DEPTH PREPASS)
{
VkCommandBufferBeginInfo cmdBufferBeginInfo = {};
cmdBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
cmdBufferBeginInfo.pNext = nullptr;
cmdBufferBeginInfo.flags = 0;
cmdBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

VkClearValue clearValues[1];
clearValues[0].depthStencil = { 1.0f, 0 };

VkRenderPassBeginInfo renderPassBeginInfo = {};
renderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
renderPassBeginInfo.pNext = nullptr;
renderPassBeginInfo.renderPass = depthPrepassBuffer->renderPass;
renderPassBeginInfo.renderArea.offset.x = 0;
renderPassBeginInfo.renderArea.offset.y = 0;
renderPassBeginInfo.renderArea.extent.width = Graphics::getWidth();
renderPassBeginInfo.renderArea.extent.height = Graphics::getHeight();
renderPassBeginInfo.clearValueCount = 1;
renderPassBeginInfo.pClearValues = &clearValues[0];
renderPassBeginInfo.framebuffer = depthPrepassBuffer->handle;

VK_VALIDATE_RESULT(vkBeginCommandBuffer(depthPrepassCommandBuffer, &cmdBufferBeginInfo));

VkViewport viewport = {};
viewport.height = (float)Graphics::getHeight();
viewport.width = (float)Graphics::getWidth();
viewport.minDepth = (float) 0.0f;
viewport.maxDepth = (float) 1.0f;
vkCmdSetViewport(depthPrepassCommandBuffer, 0, 1, &viewport);

VkRect2D scissor = {};
scissor.extent.width = Graphics::getWidth();
scissor.extent.height = Graphics::getHeight();
scissor.offset.x = 0;
scissor.offset.y = 0;
vkCmdSetScissor(depthPrepassCommandBuffer, 0, 1, &scissor);

vkCmdBeginRenderPass(depthPrepassCommandBuffer, &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

rootNode->updateTransform(true);

std::stack<SceneNode*> nodes;
nodes.push(rootNode);

Material* material = depthPrepassMaterial;

vkCmdBindPipeline(depthPrepassCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, material->getPipeline());
vkCmdBindPipeline(depthPrepassCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, material->getPipeline());
vkCmdBindDescriptorSets(depthPrepassCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, material->getDescriptors().getPipelineLayout(), 0, material->getDescriptors().getSetSize(), material->getDescriptors().getSetPtr(), 0, nullptr);

while (!nodes.empty()) {
SceneNode* node = nodes.top();
nodes.pop();

Mesh* mesh = node->mesh;

if (mesh && material) {
vkCmdBindVertexBuffers(depthPrepassCommandBuffer, 0, 1, &mesh->buffer, &mesh->vertexOffset);
vkCmdBindIndexBuffer(depthPrepassCommandBuffer, mesh->buffer, mesh->indexOffset, VK_INDEX_TYPE_UINT32);

if (material->getPushConstants().size > 0)
vkCmdPushConstants(depthPrepassCommandBuffer, material->getDescriptors().getPipelineLayout(), static_cast<VkShaderStageFlagBits>(material->getPushConstants().stageFlags), material->getPushConstants().offset, material->getPushConstants().size, &node->getTransform());

vkCmdDrawIndexed(depthPrepassCommandBuffer,  mesh->indicesCount, 1, 0, 0, 1);
}

for (uint32_t i = 0; i < node->getChildCount(); i++)
nodes.push(node->getChildByIndex(i));
}


vkCmdEndRenderPass(depthPrepassCommandBuffer);
VK_VALIDATE_RESULT(vkEndCommandBuffer(depthPrepassCommandBuffer));

submitInfo = {};
submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
submitInfo.pNext = nullptr;
submitInfo.pWaitDstStageMask = &submitPipelineStages;
submitInfo.waitSemaphoreCount = 1;
submitInfo.pWaitSemaphores = &presentCompleteSemaphore;
submitInfo.signalSemaphoreCount = 1;
submitInfo.pSignalSemaphores = &depthPrepassCompleteSemaphore;
submitInfo.commandBufferCount = 1;
submitInfo.pCommandBuffers = &depthPrepassCommandBuffer;
VK_VALIDATE_RESULT(vkQueueSubmit(graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE));
}

// G PASS
{
VkCommandBufferBeginInfo cmdBufferBeginInfo = {};
cmdBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
cmdBufferBeginInfo.pNext = nullptr;
cmdBufferBeginInfo.flags = 0;
cmdBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

VkClearValue clearValues[2];
clearValues[0].color = { {0.0f, 0.0f, 0.0f, 1.0f} };
clearValues[1].color = { { 0.0f, 0.0f, 0.0f, 1.0f } };

VkRenderPassBeginInfo renderPassBeginInfo = {};
renderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
renderPassBeginInfo.pNext = nullptr;
renderPassBeginInfo.renderPass = gRenderPass;
renderPassBeginInfo.renderArea.offset.x = 0;
renderPassBeginInfo.renderArea.offset.y = 0;
renderPassBeginInfo.renderArea.extent.width = Graphics::getWidth();
renderPassBeginInfo.renderArea.extent.height = Graphics::getHeight();
renderPassBeginInfo.clearValueCount = 2;
renderPassBeginInfo.pClearValues = &clearValues[0];
renderPassBeginInfo.framebuffer = gBuffer->handle;

VK_VALIDATE_RESULT(vkBeginCommandBuffer(gPassCommandBuffer, &cmdBufferBeginInfo));

VkViewport viewport = {};
viewport.height = (float)Graphics::getHeight();
viewport.width = (float)Graphics::getWidth();
viewport.minDepth = (float) 0.0f;
viewport.maxDepth = (float) 1.0f;
vkCmdSetViewport(gPassCommandBuffer, 0, 1, &viewport);

VkRect2D scissor = {};
scissor.extent.width = Graphics::getWidth();
scissor.extent.height = Graphics::getHeight();
scissor.offset.x = 0;
scissor.offset.y = 0;
vkCmdSetScissor(gPassCommandBuffer, 0, 1, &scissor);


#ifdef MULTITHREADED_RENDERER

#else
vkCmdBeginRenderPass(gPassCommandBuffer, &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

std::stack<SceneNode*> nodes;
nodes.push(rootNode);

while (!nodes.empty()) {
SceneNode* node = nodes.top();
nodes.pop();

Mesh* mesh = node->mesh;
Material* material = node->material;

if (mesh && material) {
vkCmdBindPipeline(gPassCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, material->getPipeline());

vkCmdBindVertexBuffers(gPassCommandBuffer, 0, 1, &mesh->buffer, &mesh->vertexOffset);
vkCmdBindIndexBuffer(gPassCommandBuffer, mesh->buffer, mesh->indexOffset, VK_INDEX_TYPE_UINT32);

vkCmdBindDescriptorSets(gPassCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, material->getDescriptors().getPipelineLayout(), 0, material->getDescriptors().getSetSize(), material->getDescriptors().getSetPtr(), 0, nullptr);

if(material->getPushConstants().size > 0)
vkCmdPushConstants(gPassCommandBuffer, material->getDescriptors().getPipelineLayout(), static_cast<VkShaderStageFlagBits>(material->getPushConstants().stageFlags), material->getPushConstants().offset, material->getPushConstants().size, &node->getTransform());

vkCmdDrawIndexed(gPassCommandBuffer, mesh->indicesCount, 1, 0, 0, 1);
}

for (uint32_t i = 0; i < node->getChildCount(); i++)
nodes.push(node->getChildByIndex(i));
}
#endif
vkCmdEndRenderPass(gPassCommandBuffer);
VK_VALIDATE_RESULT(vkEndCommandBuffer(gPassCommandBuffer));

submitInfo = {};
submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
submitInfo.pNext = nullptr;
submitInfo.pWaitDstStageMask = &submitPipelineStages;
submitInfo.waitSemaphoreCount = 1;
submitInfo.pWaitSemaphores = &depthPrepassCompleteSemaphore;
submitInfo.signalSemaphoreCount = 1;
submitInfo.pSignalSemaphores = &gPassCompleteSemaphore;
submitInfo.commandBufferCount = 1;
submitInfo.pCommandBuffers = &gPassCommandBuffer;
VK_VALIDATE_RESULT(vkQueueSubmit(graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE));
}*/

//VK_VALIDATE_RESULT(vkCreateSemaphore(mainGPU.device, &semaphoreInfo, nullptr, &depthPrepassCompleteSemaphore));
//VK_VALIDATE_RESULT(vkCreateSemaphore(mainGPU.device, &semaphoreInfo, nullptr, &gPassCompleteSemaphore));

/*
GRAIL_LOG(INFO, "RENDERER") << "G PASS";
// G RENDER PASS
{
std::array<VkAttachmentDescription, 3> attachments = {};

// COLOR
(*gBuffer)[0].description.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
(*gBuffer)[0].description.storeOp = VK_ATTACHMENT_STORE_OP_STORE;

// NORMALS
(*gBuffer)[1].description.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
(*gBuffer)[1].description.storeOp = VK_ATTACHMENT_STORE_OP_STORE;

// DEPTH
(*gBuffer)[2].description.loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
(*gBuffer)[2].description.storeOp = VK_ATTACHMENT_STORE_OP_STORE;

VkAttachmentReference depthReference = { 2, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL };

std::array<VkAttachmentReference, 2> colorReferences = {};
colorReferences[0] = { 0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL };
colorReferences[1] = { 1, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL };

VkSubpassDescription subpass = {};
subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
subpass.pColorAttachments = colorReferences.data();
subpass.colorAttachmentCount = static_cast<uint32_t>(colorReferences.size());
subpass.pDepthStencilAttachment = &depthReference;

std::array<VkSubpassDependency, 2> dependencies;

dependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
dependencies[0].dstSubpass = 0;
dependencies[0].srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
dependencies[0].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
dependencies[0].srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
dependencies[0].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
dependencies[0].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

dependencies[1].srcSubpass = 0;
dependencies[1].dstSubpass = VK_SUBPASS_EXTERNAL;
dependencies[1].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
dependencies[1].dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
dependencies[1].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
dependencies[1].dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
dependencies[1].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

std::vector<VkAttachmentDescription> descriptions = gBuffer->getAttachmentDescriptions();

VkRenderPassCreateInfo renderPassInfo = {};
renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
renderPassInfo.pAttachments = descriptions.data();
renderPassInfo.attachmentCount = static_cast<uint32_t>(descriptions.size());
renderPassInfo.subpassCount = 1;
renderPassInfo.pSubpasses = &subpass;
renderPassInfo.dependencyCount = 2;
renderPassInfo.pDependencies = dependencies.data();
VK_VALIDATE_RESULT(vkCreateRenderPass(mainGPU.device, &renderPassInfo, nullptr, &gRenderPass));

gBuffer->finalizeCreation(gRenderPass);
}

*/

/*// DEPTH-PREPASS
{
FramebufferCreateInfo bufferInfo = {};
bufferInfo.width = Graphics::getWidth();
bufferInfo.height = Graphics::getHeight();

TextureCreateInfo attachmentInfo = {};
attachmentInfo.generateMipmaps = false;
attachmentInfo.format = VK_FORMAT_D32_SFLOAT_S8_UINT;
attachmentInfo.usageBits = ImageUsageBits::DEPTH_STENCIL_ATTACHMENT_BIT | ImageUsageBits::SAMPLED_BIT;

depthPrepassBuffer = new Framebuffer(bufferInfo);
depthPrepassBuffer->createAttachment("prepass_depth", attachmentInfo);
depthPrepassBuffer->finalizeCreation();
}

//G-PASS
{
FramebufferCreateInfo bufferInfo = {};
bufferInfo.width = Graphics::getWidth();
bufferInfo.height = Graphics::getHeight();

TextureCreateInfo attachmentInfo = {};
attachmentInfo.generateMipmaps = false;
attachmentInfo.usageBits = ImageUsageBits::COLOR_ATTACHMENT_BIT | ImageUsageBits::SAMPLED_BIT;

// PREPASS DEPTH
gBuffer = new Framebuffer(bufferInfo);
attachmentInfo.format = VK_FORMAT_R8G8B8A8_UNORM;
gBuffer->createAttachment("gpass_albedo", attachmentInfo);

attachmentInfo.format = VK_FORMAT_R16G16B16A16_SFLOAT;
gBuffer->createAttachment("gpass_normals", attachmentInfo);

gBuffer->addAttachment(depthPrepassBuffer->attachments[0]);
}*/

/*{
	// Descriptors for the attachments used by this renderpass
	std::array<VkAttachmentDescription, 2> attachments = {};

	// Color attachment
	attachments[0].format = colorFormat;									// Use the color format selected by the swapchain
	attachments[0].samples = VK_SAMPLE_COUNT_1_BIT;									// We don't use multi sampling in this example
	attachments[0].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;							// Clear this attachment at the start of the render pass
	attachments[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;							// Keep it's contents after the render pass is finished (for displaying it)
	attachments[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;					// We don't use stencil, so don't care for load
	attachments[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;				// Same for store
	attachments[0].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;						// Layout at render pass start. Initial doesn't matter, so we use undefined
	attachments[0].finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;					// Layout to which the attachment is transitioned when the render pass is finished
																					// As we want to present the color buffer to the swapchain, we transition to PRESENT_KHR	
																					// Depth attachment
	attachments[1].format = depthFormat;											// A proper depth format is selected in the example base
	attachments[1].samples = VK_SAMPLE_COUNT_1_BIT;
	attachments[1].loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;							// Clear depth at start of first subpass
	attachments[1].storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;						// We don't need depth after render pass has finished (DONT_CARE may result in better performance)
	attachments[1].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;					// No stencil
	attachments[1].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;				// No Stencil
	attachments[1].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;						// Layout at render pass start. Initial doesn't matter, so we use undefined
	attachments[1].finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;	// Transition to depth/stencil attachment

																					// Setup attachment references
	VkAttachmentReference colorReference = {};
	colorReference.attachment = 0;													// Attachment 0 is color
	colorReference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;				// Attachment layout used as color during the subpass

	VkAttachmentReference depthReference = {};
	depthReference.attachment = 1;													// Attachment 1 is color
	depthReference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;		// Attachment used as depth/stemcil used during the subpass

																					// Setup a single subpass reference
	VkSubpassDescription subpassDescription = {};
	subpassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	subpassDescription.colorAttachmentCount = 1;									// Subpass uses one color attachment
	subpassDescription.pColorAttachments = &colorReference;							// Reference to the color attachment in slot 0
	subpassDescription.pDepthStencilAttachment = &depthReference;					// Reference to the depth attachment in slot 1
	subpassDescription.inputAttachmentCount = 0;									// Input attachments can be used to sample from contents of a previous subpass
	subpassDescription.pInputAttachments = nullptr;									// (Input attachments not used by this example)
	subpassDescription.preserveAttachmentCount = 0;									// Preserved attachments can be used to loop (and preserve) attachments through subpasses
	subpassDescription.pPreserveAttachments = nullptr;								// (Preserve attachments not used by this example)
	subpassDescription.pResolveAttachments = nullptr;								// Resolve attachments are resolved at the end of a sub pass and can be used for e.g. multi sampling

																					// Setup subpass dependencies
																					// These will add the implicit ttachment layout transitionss specified by the attachment descriptions
																					// The actual usage layout is preserved through the layout specified in the attachment reference		
																					// Each subpass dependency will introduce a memory and execution dependency between the source and dest subpass described by
																					// srcStageMask, dstStageMask, srcAccessMask, dstAccessMask (and dependencyFlags is set)
																					// Note: VK_SUBPASS_EXTERNAL is a special constant that refers to all commands executed outside of the actual renderpass)
	std::array<VkSubpassDependency, 2> dependencies;

	// First dependency at the start of the renderpass
	// Does the transition from final to initial layout 
	dependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;								// Producer of the dependency 
	dependencies[0].dstSubpass = 0;													// Consumer is our single subpass that will wait for the execution depdendency
	dependencies[0].srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
	dependencies[0].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependencies[0].srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
	dependencies[0].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	dependencies[0].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

	// Second dependency at the end the renderpass
	// Does the transition from the initial to the final layout
	dependencies[1].srcSubpass = 0;													// Producer of the dependency is our single subpass
	dependencies[1].dstSubpass = VK_SUBPASS_EXTERNAL;								// Consumer are all commands outside of the renderpass
	dependencies[1].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependencies[1].dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
	dependencies[1].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	dependencies[1].dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
	dependencies[1].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

	// Create the actual renderpass
	VkRenderPassCreateInfo renderPassInfo = {};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	renderPassInfo.attachmentCount = static_cast<uint32_t>(attachments.size());		// Number of attachments used by this render pass
	renderPassInfo.pAttachments = attachments.data();								// Descriptions of the attachments used by the render pass
	renderPassInfo.subpassCount = 1;												// We only use one subpass in this example
	renderPassInfo.pSubpasses = &subpassDescription;								// Description of that subpass
	renderPassInfo.dependencyCount = static_cast<uint32_t>(dependencies.size());	// Number of subpass dependencies
	renderPassInfo.pDependencies = dependencies.data();								// Subpass dependencies used by the render pass

	VK_VALIDATE_RESULT(vkCreateRenderPass(mainGPU.device, &renderPassInfo, nullptr, &presentPass));
	}*/




/*#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"*/



//#define MULTITHREADED_RENDERER

/*VkShaderModule loadSPIRVShader(grail::GPU gpu, std::string filename) {
uint32_t shaderSize;
char* shaderCode;

grail::FileIO::readBinaryFile(filename, &shaderCode, &shaderSize);

// Create a new shader module that will be used for pipeline creation
VkShaderModuleCreateInfo moduleCreateInfo = {};
moduleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
moduleCreateInfo.codeSize = shaderSize;
moduleCreateInfo.pCode = (uint32_t*)shaderCode;

VkShaderModule shaderModule;
VK_VALIDATE_RESULT(vkCreateShaderModule(gpu.device, &moduleCreateInfo, nullptr, &shaderModule));

delete[] shaderCode;

return shaderModule;
}*/

/*GRAIL_LOG(INFO, "RENDERER") << "Preparing submit info...";
{
submitInfo = {};
submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
submitInfo.pNext = nullptr;
submitInfo.pWaitDstStageMask = &submitPipelineStages;
submitInfo.waitSemaphoreCount = 1;
submitInfo.pWaitSemaphores = &presentCompleteSemaphore;
submitInfo.signalSemaphoreCount = 1;
submitInfo.pSignalSemaphores = &renderCompleteSemaphore;
}*/


/*VK_VALIDATE_RESULT(vkEndCommandBuffer(setupCommandBuffer));
VkSubmitInfo setupSubmitInfo = {};
setupSubmitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
setupSubmitInfo.pNext = nullptr;
setupSubmitInfo.commandBufferCount = 1;
setupSubmitInfo.pCommandBuffers = &setupCommandBuffer;
VK_VALIDATE_RESULT(vkQueueSubmit(graphicsQueue, 1, &setupSubmitInfo, VK_NULL_HANDLE));
VK_VALIDATE_RESULT(vkQueueWaitIdle(graphicsQueue));*/

/*uint32_t concurency = std::thread::hardware_concurrency();
GRAIL_LOG(INFO, "RENDERER") << "Hardware Concurency: " << concurency;

renderThreads.resize(concurency);
renderThreadPool = new ThreadPool();
renderThreadPool->allocateThreads(concurency);

for (uint32_t i = 0; i < concurency; i++) {
RenderThread& thread = renderThreads[i];

VkCommandPoolCreateInfo cmdPoolInfo = {};
cmdPoolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
cmdPoolInfo.pNext = nullptr;
cmdPoolInfo.queueFamilyIndex = graphicsQueueIndex;
cmdPoolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
VK_VALIDATE_RESULT(vkCreateCommandPool(mainGPU.device, &cmdPoolInfo, nullptr, &thread.cmdPool));

VkCommandBufferAllocateInfo cmdBufferInfo = {};
cmdBufferInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
cmdBufferInfo.pNext = nullptr;
cmdBufferInfo.level = VK_COMMAND_BUFFER_LEVEL_SECONDARY;
cmdBufferInfo.commandBufferCount = 1;
cmdBufferInfo.commandPool = thread.cmdPool;
VK_VALIDATE_RESULT(vkAllocateCommandBuffers(mainGPU.device, &cmdBufferInfo, &thread.cmdBuffer));
}*/

/*VkCommandBufferBeginInfo cmdBufferBeginInfo = {};
cmdBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
cmdBufferBeginInfo.pNext = nullptr;
cmdBufferBeginInfo.flags = 0;
VK_VALIDATE_RESULT(vkBeginCommandBuffer(setupCommandBuffer, &cmdBufferBeginInfo));
GRAIL_LOG(INFO, "RENDERER") << "Creating depth/stencil buffer...";
{
VkImageCreateInfo image = {};
image.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
image.pNext = nullptr;
image.imageType = VK_IMAGE_TYPE_2D;
image.format = depthFormat;
image.extent = { Graphics::getWidth(), Graphics::getHeight(), 1 };
image.mipLevels = 1;
image.arrayLayers = 1;
image.samples = VK_SAMPLE_COUNT_1_BIT;
image.tiling = VK_IMAGE_TILING_OPTIMAL;
image.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
image.flags = 0;

VkImageSubresourceRange subresourceRange = {};
subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT;
subresourceRange.baseMipLevel = 0;
subresourceRange.levelCount = 1;
subresourceRange.baseArrayLayer = 0;
subresourceRange.layerCount = 1;

VkImageViewCreateInfo imageView = {};
imageView.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
imageView.pNext = nullptr;
imageView.viewType = VK_IMAGE_VIEW_TYPE_2D;
imageView.format = depthFormat;
imageView.flags = 0;
imageView.subresourceRange = subresourceRange;

VkMemoryRequirements memReqs = {};
VK_VALIDATE_RESULT(vkCreateImage(mainGPU.device, &image, nullptr, &depthStencilStruct.image));
vkGetImageMemoryRequirements(mainGPU.device, depthStencilStruct.image, &memReqs);

VkMemoryAllocateInfo allocInfo = {};
allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
allocInfo.pNext = nullptr;
allocInfo.allocationSize = 0;
allocInfo.memoryTypeIndex = 0;
allocInfo.allocationSize = memReqs.size;
mainGPU.getMemoryType(memReqs.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, &allocInfo.memoryTypeIndex);
VK_VALIDATE_RESULT(vkAllocateMemory(mainGPU.device, &allocInfo, nullptr, &depthStencilStruct.deviceMem));

VK_VALIDATE_RESULT(vkBindImageMemory(mainGPU.device, depthStencilStruct.image, depthStencilStruct.deviceMem, 0));
/*setImageLayout(
setupCommandBuffer,
depthStencilStruct.image,
VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT,
VK_IMAGE_LAYOUT_UNDEFINED,
VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
);*/
/*imageView.image = depthStencilStruct.image;
VK_VALIDATE_RESULT(vkCreateImageView(mainGPU.device, &imageView, nullptr, &depthStencilStruct.imageView));
}*/

/*GRAIL_LOG(INFO, "RENDERER") << "Pre-recording frame submit command buffers...";
{
// Pre-record frame submit command buffers
for (uint32_t i = 0; i < imageCount; i++) {
// Post present
{
VkCommandBufferBeginInfo cmdBufInfo = {};
cmdBufInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
cmdBufInfo.pNext = nullptr;

VK_VALIDATE_RESULT(vkBeginCommandBuffer(postPresentCommandBuffers[i], &cmdBufInfo));

VkImageMemoryBarrier postPresentBarrier = {};
postPresentBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
postPresentBarrier.pNext = nullptr;
postPresentBarrier.srcAccessMask = 0;
postPresentBarrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
postPresentBarrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
postPresentBarrier.newLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
postPresentBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
postPresentBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
postPresentBarrier.subresourceRange = { VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 };
postPresentBarrier.image = swapchain->buffers[i].image;

vkCmdPipelineBarrier(
postPresentCommandBuffers[i],
VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
0,
0, nullptr, // No memory barriers,
0, nullptr, // No buffer barriers,
1, &postPresentBarrier);

VK_VALIDATE_RESULT(vkEndCommandBuffer(postPresentCommandBuffers[i]));
}

// Pre present
{
VkCommandBufferBeginInfo cmdBufInfo = {};
cmdBufInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
cmdBufInfo.pNext = nullptr;
VK_VALIDATE_RESULT(vkBeginCommandBuffer(prePresentCommandBuffers[i], &cmdBufInfo));

VkImageMemoryBarrier prePresentBarrier = {};
prePresentBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
prePresentBarrier.pNext = nullptr;
prePresentBarrier.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
prePresentBarrier.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
prePresentBarrier.oldLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
prePresentBarrier.newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
prePresentBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
prePresentBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
prePresentBarrier.subresourceRange = { VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 };
prePresentBarrier.image = swapchain->buffers[i].image;

vkCmdPipelineBarrier(
prePresentCommandBuffers[i],
VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
0, // No flags
0, nullptr, // No memory barriers
0, nullptr, // No buffer barriers
1, &prePresentBarrier);

VK_VALIDATE_RESULT(vkEndCommandBuffer(prePresentCommandBuffers[i]));
}
}
}*/


/*GRAIL_LOG(INFO, "RENDERER") << "Creating present render pass...";
{
VkAttachmentDescription attachments[2];
attachments[0].flags = 0;
attachments[0].format = colorFormat;
attachments[0].samples = VK_SAMPLE_COUNT_1_BIT;
attachments[0].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
attachments[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
attachments[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
attachments[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
attachments[0].initialLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
attachments[0].finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

attachments[1].flags = 0;
attachments[1].format = depthFormat;
attachments[1].samples = VK_SAMPLE_COUNT_1_BIT;
attachments[1].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
attachments[1].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
attachments[1].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
attachments[1].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
attachments[1].initialLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
attachments[1].finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

VkAttachmentReference colorRef = {};
colorRef.attachment = 0;
colorRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

VkAttachmentReference depthRef = {};
depthRef.attachment = 1;
depthRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

VkSubpassDescription subpass = {};
subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
subpass.flags = 0;
subpass.inputAttachmentCount = 0;
subpass.pInputAttachments = nullptr;
subpass.colorAttachmentCount = 1;
subpass.pColorAttachments = &colorRef;
subpass.pResolveAttachments = nullptr;
subpass.pDepthStencilAttachment = &depthRef;
subpass.preserveAttachmentCount = 0;
subpass.pPreserveAttachments = nullptr;

VkRenderPassCreateInfo renderPassInfo = {};
renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
renderPassInfo.pNext = nullptr;
renderPassInfo.attachmentCount = 2;
renderPassInfo.pAttachments = &attachments[0];
renderPassInfo.subpassCount = 1;
renderPassInfo.pSubpasses = &subpass;
renderPassInfo.dependencyCount = 0;
renderPassInfo.pDependencies = nullptr;

VK_VALIDATE_RESULT(vkCreateRenderPass(mainGPU.device, &renderPassInfo, nullptr, &presentPass));
}*/


// A note on memory management in Vulkan in general:
//	This is a very complex topic and while it's fine for an example application to to small individual memory allocations that is not
//	what should be done a real-world application, where you should allocate large chunkgs of memory at once isntead.

/*

for (size_t s = 0; s < shapes.size(); s++) {
// Loop over faces(polygon)
size_t index_offset = 0;

GRAIL_LOG(INFO, "FV") << shapes[s].mesh.num_face_vertices.size() * 3 * 6;
GRAIL_LOG(INFO, "FV") << shapes[s].mesh.num_face_vertices.size() * 3;

for (size_t f = 0; f < shapes[s].mesh.num_face_vertices.size(); f++) {
int fv = shapes[s].mesh.num_face_vertices[f];
// Loop over vertices in the face.
for (size_t v = 0; v < fv; v++) {
// access to vertex
tinyobj::index_t idx = shapes[s].mesh.indices[index_offset + v];

float vx = attrib.vertices[3 * idx.vertex_index + 0];
float vy = attrib.vertices[3 * idx.vertex_index + 1];
float vz = attrib.vertices[3 * idx.vertex_index + 2];

float nx = attrib.normals[3 * idx.normal_index + 0];
float ny = attrib.normals[3 * idx.normal_index + 1];
float nz = attrib.normals[3 * idx.normal_index + 2];

vertexBuffer.push_back(vx);
vertexBuffer.push_back(vy);
vertexBuffer.push_back(vz);

vertexBuffer.push_back(nx);
vertexBuffer.push_back(ny);
vertexBuffer.push_back(nz);

indexBuffer.push_back(index_offset + v);
}
index_offset += fv;
}
}



*/

/*std::vector<float>& vertexBuffer = meshes[1].vertices;
std::vector<uint32_t>& indexBuffer = meshes[1].indices;

GRAIL_LOG(INFO, "T") << meshes.size();

uint32_t vertexBufferSize = static_cast<uint32_t>(vertexBuffer.size()) * sizeof(float);
uint32_t indexBufferSize = static_cast<uint32_t>(indexBuffer.size()) * sizeof(uint32_t);*/

// NEW SHARED MEM TEST
/*{

}*/

// SHARED MEM TEST
/*
VkMemoryAllocateInfo memAlloc = {};
memAlloc.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
VkMemoryRequirements memReqs;

void *data;
{
struct StagingBuffer {
VkDeviceMemory memory;
VkBuffer buffer;
};

struct {
StagingBuffer vertices;
StagingBuffer indices;
} stagingBuffers;

VkBufferCreateInfo vertexBufferInfo = {};
vertexBufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
vertexBufferInfo.size = vertexBufferSize;
vertexBufferInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;

// VERTEX STAGING
VK_VALIDATE_RESULT(vkCreateBuffer(mainGPU.device, &vertexBufferInfo, nullptr, &stagingBuffers.vertices.buffer));
vkGetBufferMemoryRequirements(mainGPU.device, stagingBuffers.vertices.buffer, &memReqs);
memAlloc.allocationSize = memReqs.size;

GRAIL_LOG(DEBUG, "Staging Vertex buffer alignment") << memReqs.alignment;

memAlloc.memoryTypeIndex = mainGPU.getMemoryType(memReqs.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
VK_VALIDATE_RESULT(vkAllocateMemory(mainGPU.device, &memAlloc, nullptr, &stagingBuffers.vertices.memory));
VK_VALIDATE_RESULT(vkMapMemory(mainGPU.device, stagingBuffers.vertices.memory, 0, memAlloc.allocationSize, 0, &data));
memcpy(data, vertexBuffer.data(), vertexBufferSize);
vkUnmapMemory(mainGPU.device, stagingBuffers.vertices.memory);
VK_VALIDATE_RESULT(vkBindBufferMemory(mainGPU.device, stagingBuffers.vertices.buffer, stagingBuffers.vertices.memory, 0));
// VERTEX STAGING


// INDEX STAGING
VkBufferCreateInfo indexbufferInfo = {};
indexbufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
indexbufferInfo.size = indexBufferSize;
indexbufferInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
VK_VALIDATE_RESULT(vkCreateBuffer(mainGPU.device, &indexbufferInfo, nullptr, &stagingBuffers.indices.buffer));
vkGetBufferMemoryRequirements(mainGPU.device, stagingBuffers.indices.buffer, &memReqs);

GRAIL_LOG(DEBUG, "Staging Index buffer alignment") << memReqs.alignment;

memAlloc.allocationSize = memReqs.size;
memAlloc.memoryTypeIndex = mainGPU.getMemoryType(memReqs.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
VK_VALIDATE_RESULT(vkAllocateMemory(mainGPU.device, &memAlloc, nullptr, &stagingBuffers.indices.memory));
VK_VALIDATE_RESULT(vkMapMemory(mainGPU.device, stagingBuffers.indices.memory, 0, indexBufferSize, 0, &data));
memcpy(data, indexBuffer.data(), indexBufferSize);
vkUnmapMemory(mainGPU.device, stagingBuffers.indices.memory);
VK_VALIDATE_RESULT(vkBindBufferMemory(mainGPU.device, stagingBuffers.indices.buffer, stagingBuffers.indices.memory, 0));
// INDEX STAGING

//GLOBAL MEM POOL
{
VkMemoryAllocateInfo allocInfo = {};
allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
allocInfo.pNext = nullptr;
allocInfo.allocationSize = (1024 * 1024) * 150; // 150MB
allocInfo.memoryTypeIndex = mainGPU.getMemoryType(memReqs.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

VK_VALIDATE_RESULT(vkAllocateMemory(mainGPU.device, &allocInfo, nullptr, &meshBuffers.sharedMem));

// VERTEX BUFFER
vertexBufferInfo.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;

VK_VALIDATE_RESULT(vkCreateBuffer(mainGPU.device, &vertexBufferInfo, nullptr, &meshBuffers.vbBuffer));
VK_VALIDATE_RESULT(vkBindBufferMemory(mainGPU.device, meshBuffers.vbBuffer, meshBuffers.sharedMem, 0));
// VERTEX BUFFER

// INDEX BUFFER
indexbufferInfo.usage = VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;

VK_VALIDATE_RESULT(vkCreateBuffer(mainGPU.device, &indexbufferInfo, nullptr, &meshBuffers.ibBuffer));
VK_VALIDATE_RESULT(vkBindBufferMemory(mainGPU.device, meshBuffers.ibBuffer, meshBuffers.sharedMem, vertexBufferInfo.size + (vertexBufferInfo.size % 256)));
// INDEX BUFFER


// GLOBAL BUFFER
VkBufferCreateInfo globalBufferInfo = {};
globalBufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
globalBufferInfo.size = vertexBufferSize + indexBufferSize + 512;
globalBufferInfo.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;

VK_VALIDATE_RESULT(vkCreateBuffer(mainGPU.device, &globalBufferInfo, nullptr, &meshBuffers.sharedBuffer));
VK_VALIDATE_RESULT(vkBindBufferMemory(mainGPU.device, meshBuffers.sharedBuffer, meshBuffers.sharedMem, 0));
// GLOBAL BUFFER
}
//GLOBAL MEM POOL

// COPY
VkCommandBufferBeginInfo cmdBufferBeginInfo = {};
cmdBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
cmdBufferBeginInfo.pNext = nullptr;
cmdBufferBeginInfo.flags = 0;
VK_VALIDATE_RESULT(vkBeginCommandBuffer(setupCommandBuffer, &cmdBufferBeginInfo));

// Put buffer region copies into command buffer
VkBufferCopy copyRegion = {};

// Vertex buffer
copyRegion.srcOffset = 0;
copyRegion.dstOffset = 0;
copyRegion.size = vertexBufferSize;
vkCmdCopyBuffer(setupCommandBuffer, stagingBuffers.vertices.buffer, meshBuffers.sharedBuffer, 1, &copyRegion);
// Index buffer
copyRegion.srcOffset = 0;
copyRegion.dstOffset = vertexBufferSize + 256;

meshBuffers.offset = vertexBufferSize + 256;

copyRegion.size = indexBufferSize;
vkCmdCopyBuffer(setupCommandBuffer, stagingBuffers.indices.buffer, meshBuffers.sharedBuffer, 1, &copyRegion);

// Flushing the command buffer will also submit it to the queue and uses a fence to ensure that all commands have been executed before returning
VK_VALIDATE_RESULT(vkEndCommandBuffer(setupCommandBuffer));
VkSubmitInfo setupSubmitInfo = {};
setupSubmitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
setupSubmitInfo.pNext = nullptr;
setupSubmitInfo.commandBufferCount = 1;
setupSubmitInfo.pCommandBuffers = &setupCommandBuffer;
VK_VALIDATE_RESULT(vkQueueSubmit(graphicsQueue, 1, &setupSubmitInfo, VK_NULL_HANDLE));
VK_VALIDATE_RESULT(vkQueueWaitIdle(graphicsQueue));
// COPY

vkDestroyBuffer(mainGPU.device, stagingBuffers.vertices.buffer, nullptr);
vkFreeMemory(mainGPU.device, stagingBuffers.vertices.memory, nullptr);
vkDestroyBuffer(mainGPU.device, stagingBuffers.indices.buffer, nullptr);
vkFreeMemory(mainGPU.device, stagingBuffers.indices.memory, nullptr);
}

*/
// We need to tell the API the number of max. requested descriptors per type
/*VkDescriptorPoolSize typeCounts[2];
// This example only uses one descriptor type (uniform buffer) and only requests one descriptor of this type
typeCounts[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
typeCounts[0].descriptorCount = 1;
// For additional types you need to add new entries in the type count list
// E.g. for two combined image samplers :
typeCounts[1].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
typeCounts[1].descriptorCount = 1;*/

/*std::vector<VkDescriptorPoolSize> typeCounts;
typeCounts.resize(2);

typeCounts[0] = {};
typeCounts[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
typeCounts[0].descriptorCount = 1;

typeCounts[1] = {};
typeCounts[1].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
typeCounts[1].descriptorCount = 1;


// Create the global descriptor pool
// All descriptors used in this example are allocated from this pool
VkDescriptorPoolCreateInfo descriptorPoolInfo = {};
descriptorPoolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
descriptorPoolInfo.pNext = nullptr;
descriptorPoolInfo.poolSizeCount = typeCounts.size();
descriptorPoolInfo.pPoolSizes = &typeCounts[0];
// Set the max. number of descriptor sets that can be requested from this pool (requesting beyond this limit will result in an error)
descriptorPoolInfo.maxSets = 1;

VK_VALIDATE_RESULT(vkCreateDescriptorPool(mainGPU.device, &descriptorPoolInfo, nullptr, &descriptorPool));

GRAIL_LOG(INFO, "RENDERER") << "Creating descriptor set layout...";
// Setup layout of descriptors used in this example
// Basically connects the different shader stages to descriptors for binding uniform buffers, image samplers, etc.
// So every shader binding should map to one descriptor set layout binding

// Binding 0: Uniform buffer (Vertex shader)

std::vector<VkDescriptorSetLayoutBinding> layoutBindings;
layoutBindings.resize(2);

layoutBindings[0] = {};
layoutBindings[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
layoutBindings[0].binding = 0;
layoutBindings[0].descriptorCount = 1;
layoutBindings[0].stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
layoutBindings[0].pImmutableSamplers = nullptr;

layoutBindings[1] = {};
layoutBindings[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
layoutBindings[1].binding = 1;
layoutBindings[1].descriptorCount = 1;
layoutBindings[1].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
layoutBindings[1].pImmutableSamplers = nullptr;

VkDescriptorSetLayoutCreateInfo descriptorLayout = {};
descriptorLayout.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
descriptorLayout.pNext = nullptr;
descriptorLayout.bindingCount = layoutBindings.size();
descriptorLayout.pBindings = &layoutBindings[0];

VK_VALIDATE_RESULT(vkCreateDescriptorSetLayout(mainGPU.device, &descriptorLayout, nullptr, &descriptorSetLayout));

// Create the pipeline layout that is used to generate the rendering pipelines that are based on this descriptor set layout
// In a more complex scenario you would have different pipeline layouts for different descriptor set layouts that could be reused
VkPipelineLayoutCreateInfo pPipelineLayoutCreateInfo = {};
pPipelineLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
pPipelineLayoutCreateInfo.pNext = nullptr;
pPipelineLayoutCreateInfo.setLayoutCount = 1;
pPipelineLayoutCreateInfo.pSetLayouts = &descriptorSetLayout;

VK_VALIDATE_RESULT(vkCreatePipelineLayout(mainGPU.device, &pPipelineLayoutCreateInfo, nullptr, &pipelineLayout));

GRAIL_LOG(INFO, "RENDERER") << "Creating descriptor sets...";
// Allocate a new descriptor set from the global descriptor pool
VkDescriptorSetAllocateInfo allocInfo = {};
allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
allocInfo.descriptorPool = descriptorPool;
allocInfo.descriptorSetCount = 1;
allocInfo.pSetLayouts = &descriptorSetLayout;


VK_VALIDATE_RESULT(vkAllocateDescriptorSets(mainGPU.device, &allocInfo, &descriptorSet));*/
/*{
// Prepare and initialize a uniform buffer block containing shader uniforms
// Single uniforms like in OpenGL are no longer present in Vulkan. All Shader uniforms are passed via uniform buffer blocks
VkMemoryRequirements memReqs;

// Vertex shader uniform buffer block
VkBufferCreateInfo bufferInfo = {};
VkMemoryAllocateInfo allocInfo = {};
allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
allocInfo.pNext = nullptr;
allocInfo.allocationSize = 0;
allocInfo.memoryTypeIndex = 0;

bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
bufferInfo.size = sizeof(uboVS);
// This buffer will be used as a uniform buffer
bufferInfo.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;

// Create a new buffer
VK_VALIDATE_RESULT(vkCreateBuffer(mainGPU.device, &bufferInfo, nullptr, &uniformBufferVS.buffer));
// Get memory requirements including size, alignment and memory type
vkGetBufferMemoryRequirements(mainGPU.device, uniformBufferVS.buffer, &memReqs);
allocInfo.allocationSize = memReqs.size;
// Get the memory type index that supports host visibile memory access
// Most implementations offer multiple memory types and selecting the correct one to allocate memory from is crucial
// We also want the buffer to be host coherent so we don't have to flush (or sync after every update.
// Note: This may affect performance so you might not want to do this in a real world application that updates buffers on a regular base
allocInfo.memoryTypeIndex = mainGPU.getMemoryType(memReqs.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
// Allocate memory for the uniform buffer
VK_VALIDATE_RESULT(vkAllocateMemory(mainGPU.device, &allocInfo, nullptr, &(uniformBufferVS.memory)));
// Bind memory to buffer
VK_VALIDATE_RESULT(vkBindBufferMemory(mainGPU.device, uniformBufferVS.buffer, uniformBufferVS.memory, 0));

// Store information in the uniform's descriptor that is used by the descriptor set
uniformBufferVS.descriptor.buffer = uniformBufferVS.buffer;
uniformBufferVS.descriptor.offset = 0;
uniformBufferVS.descriptor.range = sizeof(uboVS);

//		updateUniformBuffers();
}*/

/*std::vector<VkWriteDescriptorSet> writeDescriptorSets;
writeDescriptorSets.resize(2);
// Binding 0 : Uniform buffer
writeDescriptorSets[0] = {};
writeDescriptorSets[0].pNext = nullptr;
writeDescriptorSets[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
writeDescriptorSets[0].dstSet = descriptorSet;
writeDescriptorSets[0].descriptorCount = 1;
writeDescriptorSets[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
writeDescriptorSets[0].pBufferInfo = &uniformBufferVS.descriptor;
// Binds this uniform buffer to binding point 0
writeDescriptorSets[0].dstBinding = 0;

writeDescriptorSets[1] = {};
writeDescriptorSets[1].pNext = nullptr;
writeDescriptorSets[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
writeDescriptorSets[1].dstSet = descriptorSet;
writeDescriptorSets[1].descriptorCount = 1;
writeDescriptorSets[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
writeDescriptorSets[1].pImageInfo = &testTexture.descriptorImageInfo;
// Binds this uniform buffer to binding point 0
writeDescriptorSets[1].dstBinding = 1;

vkUpdateDescriptorSets(mainGPU.device, writeDescriptorSets.size(), &writeDescriptorSets[0], 0, nullptr);
*/

/*// Create the graphics pipeline used in this example
// Vulkan uses the concept of rendering pipelines to encapsulate fixed states, replacing OpenGL's complex state machine
// A pipeline is then stored and hashed on the GPU making pipeline changes very fast
// Note: There are still a few dynamic states that are not directly part of the pipeline (but the info that they are used is)

VkGraphicsPipelineCreateInfo pipelineCreateInfo = {};
pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
// The layout used for this pipeline (can be shared among multiple pipelines using the same layout)
pipelineCreateInfo.layout = pipelineLayout;
// Renderpass this pipeline is attached to
pipelineCreateInfo.renderPass = presentPass;

// Construct the differnent states making up the pipeline

// Input assembly state describes how primitives are assembled
// This pipeline will assemble vertex data as a triangle lists (though we only use one triangle)
VkPipelineInputAssemblyStateCreateInfo inputAssemblyState = {};
inputAssemblyState.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
inputAssemblyState.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;

// Rasterization state
VkPipelineRasterizationStateCreateInfo rasterizationState = {};
rasterizationState.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
rasterizationState.polygonMode = VK_POLYGON_MODE_FILL;
rasterizationState.cullMode = VK_CULL_MODE_NONE;
rasterizationState.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
rasterizationState.depthClampEnable = VK_FALSE;
rasterizationState.rasterizerDiscardEnable = VK_FALSE;
rasterizationState.depthBiasEnable = VK_FALSE;
rasterizationState.lineWidth = 1.0f;

// Color blend state describes how blend factors are calculated (if used)
// We need one blend attachment state per color attachment (even if blending is not used
VkPipelineColorBlendAttachmentState blendAttachmentState[1] = {};
blendAttachmentState[0].colorWriteMask = 0xf;
blendAttachmentState[0].blendEnable = VK_FALSE;
VkPipelineColorBlendStateCreateInfo colorBlendState = {};
colorBlendState.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
colorBlendState.attachmentCount = 1;
colorBlendState.pAttachments = blendAttachmentState;

// Viewport state sets the number of viewports and scissor used in this pipeline
// Note: This is actually overriden by the dynamic states (see below)
VkPipelineViewportStateCreateInfo viewportState = {};
viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
viewportState.viewportCount = 1;
viewportState.scissorCount = 1;

// Enable dynamic states
// Most states are baked into the pipeline, but there are still a few dynamic states that can be changed within a command buffer
// To be able to change these we need do specify which dynamic states will be changed using this pipeline. Their actual states are set later on in the command buffer.
// For this example we will set the viewport and scissor using dynamic states
std::vector<VkDynamicState> dynamicStateEnables;
dynamicStateEnables.push_back(VK_DYNAMIC_STATE_VIEWPORT);
dynamicStateEnables.push_back(VK_DYNAMIC_STATE_SCISSOR);
VkPipelineDynamicStateCreateInfo dynamicState = {};
dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
dynamicState.pDynamicStates = dynamicStateEnables.data();
dynamicState.dynamicStateCount = static_cast<uint32_t>(dynamicStateEnables.size());

// Depth and stencil state containing depth and stencil compare and test operations
// We only use depth tests and want depth tests and writes to be enabled and compare with less or equal
VkPipelineDepthStencilStateCreateInfo depthStencilState = {};
depthStencilState.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
depthStencilState.depthTestEnable = VK_TRUE;
depthStencilState.depthWriteEnable = VK_TRUE;
depthStencilState.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
depthStencilState.depthBoundsTestEnable = VK_FALSE;
depthStencilState.back.failOp = VK_STENCIL_OP_KEEP;
depthStencilState.back.passOp = VK_STENCIL_OP_KEEP;
depthStencilState.back.compareOp = VK_COMPARE_OP_ALWAYS;
depthStencilState.stencilTestEnable = VK_FALSE;
depthStencilState.front = depthStencilState.back;

// Multi sampling state
// This example does not make use fo multi sampling (for anti-aliasing), the state must still be set and passed to the pipeline
VkPipelineMultisampleStateCreateInfo multisampleState = {};
multisampleState.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
multisampleState.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
multisampleState.pSampleMask = nullptr;

// Vertex input descriptions
// Specifies the vertex input parameters for a pipeline

// Vertex input binding
// This example uses a single vertex input binding at binding point 0 (see vkCmdBindVertexBuffers)
VkVertexInputBindingDescription vertexInputBinding = {};
vertexInputBinding.binding = 0;
vertexInputBinding.stride = sizeof(Vertex);
vertexInputBinding.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

// Inpute attribute bindings describe shader attribute locations and memory layouts
std::array<VkVertexInputAttributeDescription, 2> vertexInputAttributs;
// These match the following shader layout (see triangle.vert):
//	layout (location = 0) in vec3 inPos;
//	layout (location = 1) in vec3 inColor;
// Attribute location 0: Position
vertexInputAttributs[0].binding = 0;
vertexInputAttributs[0].location = 0;
// Position attribute is three 32 bit signed (SFLOAT) floats (R32 G32 B32)
vertexInputAttributs[0].format = VK_FORMAT_R32G32B32_SFLOAT;
vertexInputAttributs[0].offset = offsetof(Vertex, position);
// Attribute location 1: Color
vertexInputAttributs[1].binding = 0;
vertexInputAttributs[1].location = 1;
// Color attribute is three 32 bit signed (SFLOAT) floats (R32 G32 B32)
vertexInputAttributs[1].format = VK_FORMAT_R32G32B32_SFLOAT;
vertexInputAttributs[1].offset = offsetof(Vertex, color);

// Vertex input state used for pipeline creation
VkPipelineVertexInputStateCreateInfo vertexInputState = {};
vertexInputState.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
vertexInputState.vertexBindingDescriptionCount = 1;
vertexInputState.pVertexBindingDescriptions = &vertexInputBinding;
vertexInputState.vertexAttributeDescriptionCount = 2;
vertexInputState.pVertexAttributeDescriptions = vertexInputAttributs.data();

// Shaders
std::array<VkPipelineShaderStageCreateInfo, 2> shaderStages{};

// Vertex shader
shaderStages[0].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
// Set pipeline stage for this shader
shaderStages[0].stage = VK_SHADER_STAGE_VERTEX_BIT;
// Load binary SPIR-V shader
shaderStages[0].module = loadSPIRVShader(mainGPU, "shaders/triangle.vert.spv");
// Main entry point for the shader
shaderStages[0].pName = "main";
assert(shaderStages[0].module != VK_NULL_HANDLE);

// Fragment shader
shaderStages[1].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
// Set pipeline stage for this shader
shaderStages[1].stage = VK_SHADER_STAGE_FRAGMENT_BIT;
// Load binary SPIR-V shader
shaderStages[1].module = loadSPIRVShader(mainGPU, "shaders/triangle.frag.spv");
// Main entry point for the shader
shaderStages[1].pName = "main";
assert(shaderStages[1].module != VK_NULL_HANDLE);

// Set pipeline shader stage info
pipelineCreateInfo.stageCount = static_cast<uint32_t>(shaderStages.size());
pipelineCreateInfo.pStages = shaderStages.data();

// Assign the pipeline states to the pipeline creation info structure
pipelineCreateInfo.pVertexInputState = &layout.getVertexInputState();
pipelineCreateInfo.pInputAssemblyState = &inputAssemblyState;
pipelineCreateInfo.pRasterizationState = &rasterizationState;
pipelineCreateInfo.pColorBlendState = &colorBlendState;
pipelineCreateInfo.pMultisampleState = &multisampleState;
pipelineCreateInfo.pViewportState = &viewportState;
pipelineCreateInfo.pDepthStencilState = &depthStencilState;
pipelineCreateInfo.renderPass = presentPass;
pipelineCreateInfo.pDynamicState = &dynamicState;

// Create rendering pipeline using the specified states
//VK_VALIDATE_RESULT(vkCreateGraphicsPipelines(mainGPU.device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &pipeline));

// Shader modules are no longer needed once the graphics pipeline has been created
vkDestroyShaderModule(mainGPU.device, shaderStages[0].module, nullptr);
vkDestroyShaderModule(mainGPU.device, shaderStages[1].module, nullptr);*/

/*testGroup = new DescriptorSetGroup(pushConstantRange, {
{
DescriptorBinding(DescriptorBindingType::BUFFER, VK_SHADER_STAGE_VERTEX_BIT, &uniformBufferVS.descriptor),
DescriptorBinding(DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, &texture->descriptorImageInfo)
}
});
testGroup->getSet(0).getBinding(0).setDescriptor(&uniformBufferVS.descriptor);
testGroup->getSet(0).getBinding(1).setDescriptor(&texture->descriptorImageInfo);
testGroup->update();*/

/*// GLOBAL BUFFER
{
VkMemoryRequirements memReqs;

VkBufferCreateInfo globalBufferInfo = {};
globalBufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
globalBufferInfo.size = bufferMemory->getHeapSize();
globalBufferInfo.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;

VK_VALIDATE_RESULT(vkCreateBuffer(mainGPU.device, &globalBufferInfo, nullptr, &globalHostLocalBuffer));

vkGetBufferMemoryRequirements(mainGPU.device, globalHostLocalBuffer, &memReqs);

VK_VALIDATE_RESULT(vkBindBufferMemory(mainGPU.device, globalHostLocalBuffer, bufferMemory->getMemoryHandle(), 0));
}*/

/*ShaderStage vertexStage = {};
vertexStage.stageFlags = ShaderStageBits::VERTEX;
vertexStage.module = loadSPIRVShader(mainGPU, "shaders/triangle.vert.spv");

ShaderStage fragmentStage = {};
fragmentStage.stageFlags = ShaderStageBits::FRAGMENT;
fragmentStage.module = loadSPIRVShader(mainGPU, "shaders/triangle.frag.spv");*/


/*std::vector<MeshDataR> meshes = ModelLoader::loadFiles(layout, FileIO::getAssetPath("models/pumping_station/"), "");



auto a = std::chrono::high_resolution_clock::now();
for (MeshDataR& mData : meshes)
this->meshes.push_back(loadMesh(mData));

int index = 0;
for (MeshR& m : this->meshes)
m.material.diffuseColor = glm::vec3(index++ * 2, 0, 0);


auto b = std::chrono::high_resolution_clock::now();
GRAIL_LOG(DEBUG, "ASSET LOADING") << std::chrono::duration_cast<std::chrono::milliseconds>(b - a).count() << "ms";*/

//vkCmdBeginRenderPass(renderCommandBuffer, &renderPassBeginInfo, VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS);

/* ---------------------------------------------- */
/*VkCommandBufferInheritanceInfo inheritanceInfo = {};
inheritanceInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO;
inheritanceInfo.pNext = nullptr;
inheritanceInfo.framebuffer = presentFramebuffers[currentSwapchainBuffer];
inheritanceInfo.renderPass = presentPass;

uint32_t workingThreads = 0;
uint32_t renderableCount = meshes.size();
uint32_t threadCount = renderThreads.size();
if (renderableCount < threadCount)
	workingThreads = 1;
else
workingThreads = threadCount;

uint32_t distribution = meshes.size() / threadCount;

for (uint32_t i = 0; i < workingThreads; i++) {
	uint32_t sublistStart = (distribution * i);
	uint32_t sublistEnd = i == workingThreads - 1 ? renderableCount : (distribution * (i + 1));

	renderThreadPool->threads[i]->addJob([=] {
		RenderThread& thread = renderThreads[i];

		VkCommandBufferBeginInfo beginInfo = {};
		beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		beginInfo.pNext = nullptr;
		beginInfo.flags = VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT;
		beginInfo.pInheritanceInfo = &inheritanceInfo;

		VK_VALIDATE_RESULT(vkBeginCommandBuffer(thread.cmdBuffer, &beginInfo));

		vkCmdSetViewport(thread.cmdBuffer, 0, 1, &viewport);
		vkCmdSetScissor(thread.cmdBuffer, 0, 1, &scissor);

		for (uint32_t i = sublistStart; i < sublistEnd; i++) {
			Mesh& r = meshes[i];

			//if (!camera->frustum.sphereInFrustum(glm::vec3(-10, 0, 0), r.radius * 0.01f) && cull) continue;

			vkCmdBindPipeline(thread.cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);

			VkDeviceSize offsets[1] = { r.vertexRegionDescriptor.offset };

			vkCmdBindVertexBuffers(thread.cmdBuffer, 0, 1, &r.buffer, offsets);
			vkCmdBindIndexBuffer(thread.cmdBuffer, r.buffer, r.indexRegionDescriptor.offset, VK_INDEX_TYPE_UINT32);

			vkCmdBindDescriptorSets(thread.cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, r.descriptorGroup->getPipelineLayout(), 0, r.descriptorGroup->getSetSize(), r.descriptorGroup->getSetPtr(), 0, nullptr);
			vkCmdPushConstants(thread.cmdBuffer, r.descriptorGroup->getPipelineLayout(), VK_SHADER_STAGE_ALL, 0, sizeof(MeshPushConstants), &r.material);


			vkCmdDrawIndexed(thread.cmdBuffer, r.indicesCount, 1, 0, 0, 1);
		}

		VK_VALIDATE_RESULT(vkEndCommandBuffer(thread.cmdBuffer));
	});
}

renderThreadPool->wait();

std::vector<VkCommandBuffer> buffers;
for (uint32_t i = 0; i < workingThreads; i++)
	buffers.push_back(renderThreads[i].cmdBuffer);

vkCmdExecuteCommands(renderCommandBuffer, workingThreads, &buffers[0]);*/

/*for (uint32_t i = 0; i < meshes.size(); i++) {
Mesh& r = meshes[i];

//if (!camera->frustum.sphereInFrustum(glm::vec3(-10, 0, 0), r.radius * 0.01f) && cull) continue;

vkCmdBindPipeline(renderCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);

VkDeviceSize offsets[1] = { r.vertexRegionDescriptor.offset };

vkCmdBindVertexBuffers(renderCommandBuffer, 0, 1, &r.buffer, offsets);
vkCmdBindIndexBuffer(renderCommandBuffer, r.buffer, r.indexRegionDescriptor.offset, VK_INDEX_TYPE_UINT32);

vkCmdBindDescriptorSets(renderCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, r.descriptorGroup->getPipelineLayout(), 0, r.descriptorGroup->getSetSize(), r.descriptorGroup->getSetPtr(), 0, nullptr);
vkCmdPushConstants(renderCommandBuffer, r.descriptorGroup->getPipelineLayout(), VK_SHADER_STAGE_FRAGMENT_BIT, 0, sizeof(MeshPushConstants), &r.material.diffuseColor);


vkCmdDrawIndexed(renderCommandBuffer, r.indicesCount, 1, 0, 0, 1);
}*/

/* ---------------------------------------------- */

/*
// Bind descriptor sets describing shader binding points
vkCmdBindDescriptorSets(renderCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout, 0, 1, &descriptorSet, 0, nullptr);

// Bind the rendering pipeline
// The pipeline (state object) contains all states of the rendering pipeline, binding it will set all the states specified at pipeline creation time
vkCmdBindPipeline(renderCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);

// Bind triangle vertex buffer (contains position and colors)
for (Mesh& m : meshes) {
VkDeviceSize offsets[1] = { m.vertexRegionDescriptor.offset };
vkCmdBindVertexBuffers(renderCommandBuffer, 0, 1, &m.buffer, offsets);

// Bind triangle index buffer
vkCmdBindIndexBuffer(renderCommandBuffer, m.buffer, m.indexRegionDescriptor.offset, VK_INDEX_TYPE_UINT32);

// Draw indexed triangle
vkCmdDrawIndexed(renderCommandBuffer, m.indicesCount, 1, 0, 0, 1);
}
*/


/*//VK_VALIDATE_RESULT(vkWaitForFences(mainGPU.device, 1, &waitFences[currentSwapchainBuffer], VK_TRUE, UINT64_MAX));
//VK_VALIDATE_RESULT(vkResetFences(mainGPU.device, 1, &waitFences[currentSwapchainBuffer]));

VkSubmitInfo submitInfo = {};
submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
submitInfo.pNext = nullptr;
submitInfo.commandBufferCount = 1;
submitInfo.pCommandBuffers = &postPresentCommandBuffers[currentSwapchainBuffer];

VK_VALIDATE_RESULT(vkQueueSubmit(graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE));*/

/*VkSubmitInfo submitInfo = {};
submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
submitInfo.pNext = nullptr;
submitInfo.commandBufferCount = 1;
submitInfo.pCommandBuffers = &prePresentCommandBuffers[currentSwapchainBuffer];

VK_VALIDATE_RESULT(vkQueueSubmit(graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE));*/

/*grail::Renderer::MeshR grail::Renderer::loadMesh(MeshDataR & data) {
	std::vector<float>& vertexBuffer = data.vertices;
	std::vector<uint32_t>& indexBuffer = data.indices;

	uint32_t vertexBufferSize = static_cast<uint32_t>(vertexBuffer.size()) * sizeof(float);
	uint32_t indexBufferSize = static_cast<uint32_t>(indexBuffer.size()) * sizeof(uint32_t);

	void* ptr;

	VkMemoryRequirements memReqs;

	MemoryRegionDescriptor stageMemoryRegionV;
	MemoryRegionDescriptor stageMemoryRegionI;

	MemoryRegionDescriptor localMemoryRegionV;
	MemoryRegionDescriptor localMemoryRegionI;

	VkBuffer vertexStagingBuffer;
	VkBuffer indexStagingBuffer;

	// VERTEX STAGE
	VkBufferCreateInfo bufferInfo = {};
	bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferInfo.size = vertexBufferSize;
	bufferInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
	VK_VALIDATE_RESULT(vkCreateBuffer(mainGPU.device, &bufferInfo, nullptr, &vertexStagingBuffer));

	vkGetBufferMemoryRequirements(mainGPU.device, vertexStagingBuffer, &memReqs);

	stageMemoryRegionV = stagingMemory->allocateRegion(memReqs.size, memReqs.alignment);
	ptr = stagingMemory->mapMemory(stageMemoryRegionV);
	{
		memcpy(ptr, vertexBuffer.data(), vertexBufferSize);
	}
	stagingMemory->unmapMemory();
	VK_VALIDATE_RESULT(vkBindBufferMemory(mainGPU.device, vertexStagingBuffer, stagingMemory->getMemoryHandle(), stageMemoryRegionV.offset));

	// INDEX STAGE
	bufferInfo.size = indexBufferSize;
	VK_VALIDATE_RESULT(vkCreateBuffer(mainGPU.device, &bufferInfo, nullptr, &indexStagingBuffer));

	vkGetBufferMemoryRequirements(mainGPU.device, indexStagingBuffer, &memReqs);

	stageMemoryRegionI = stagingMemory->allocateRegion(memReqs.size, memReqs.alignment);
	ptr = stagingMemory->mapMemory(stageMemoryRegionI);
	{
		memcpy(ptr, indexBuffer.data(), indexBufferSize);
	}
	stagingMemory->unmapMemory();
	VK_VALIDATE_RESULT(vkBindBufferMemory(mainGPU.device, indexStagingBuffer, stagingMemory->getMemoryHandle(), stageMemoryRegionI.offset));

	// GLOBAL MEM MAP
	localMemoryRegionI = bufferMemory->allocateRegion(stageMemoryRegionI.size, stageMemoryRegionI.alignment);
	localMemoryRegionV = bufferMemory->allocateRegion(stageMemoryRegionV.size, stageMemoryRegionV.alignment);

	// COPY
	TemporaryCommandBuffer setupCommandBuffer = mainGPU.createTemporaryBuffer(mainGPU.transferQueue, VK_COMMAND_BUFFER_LEVEL_PRIMARY);
	setupCommandBuffer.begin();

	VkBufferCopy copyRegion = {};

	// Vertex buffer
	copyRegion.srcOffset = 0;
	copyRegion.dstOffset = localMemoryRegionV.offset;
	copyRegion.size = vertexBufferSize;
	vkCmdCopyBuffer(setupCommandBuffer.getHandle(), vertexStagingBuffer, globalHostLocalBuffer, 1, &copyRegion);

	// Index buffer
	copyRegion.srcOffset = 0;
	copyRegion.dstOffset = localMemoryRegionI.offset;
	copyRegion.size = indexBufferSize;
	vkCmdCopyBuffer(setupCommandBuffer.getHandle(), indexStagingBuffer, globalHostLocalBuffer, 1, &copyRegion);

	// Flushing the command buffer will also submit it to the queue and uses a fence to ensure that all commands have been executed before returning
	setupCommandBuffer.end();
	setupCommandBuffer.submit();
	setupCommandBuffer.dispose();
	// COPY 

	vkDestroyBuffer(mainGPU.device, vertexStagingBuffer, nullptr);
	vkDestroyBuffer(mainGPU.device, indexStagingBuffer, nullptr);

	TextureCreateInfo tInfo = {};
	tInfo.gpu = mainGPU;
	tInfo.generateMipmaps = true;

	std::string path = "textures/" + ((data.diffusePath.compare("none") == 0) ? "pebbles_diff.png" : data.diffusePath);

	VkTexture* diffuse = Resources::loadTexture(path, tInfo);

	MeshR mesh = {};
	//mesh.indicesCount = static_cast<uint32_t>(indexBuffer.size());
	mesh.buffer = globalHostLocalBuffer;
	//mesh.vertexRegionDescriptor = localMemoryRegionV;
	//mesh.indexRegionDescriptor = localMemoryRegionI;
	mesh.diffuseTexture = diffuse;
	mesh.material.diffuseColor = data.color;
	mesh.radius = data.radius;
	mesh.descriptorGroup = new DescriptorSetGroup(pushConstantRange, { 
		{
			DescriptorBinding(DescriptorBindingType::BUFFER, VK_SHADER_STAGE_VERTEX_BIT, 0, &uniformBufferVS.descriptor),
			DescriptorBinding(DescriptorBindingType::TEXTURE, VK_SHADER_STAGE_FRAGMENT_BIT, 1, &diffuse->descriptorImageInfo)
		} 
	});

	stagingMemory->clearRegions();

	return mesh;
}*/


/*void grail::Renderer::setImageLayout(VkCommandBuffer cmdbuffer, VkImage image, VkImageAspectFlags aspectMask, VkImageLayout oldImageLayout, VkImageLayout newImageLayout) {
VkImageSubresourceRange subresourceRange = {};
subresourceRange.aspectMask = aspectMask;
subresourceRange.baseMipLevel = 0;
subresourceRange.levelCount = 1;
subresourceRange.layerCount = 1;
setImageLayout(cmdbuffer, image, aspectMask, oldImageLayout, newImageLayout, subresourceRange);
}

void grail::Renderer::setImageLayout(VkCommandBuffer cmdbuffer, VkImage image, VkImageAspectFlags aspectMask, VkImageLayout oldImageLayout, VkImageLayout newImageLayout, VkImageSubresourceRange subresourceRange, VkPipelineStageFlags srcStageMask, VkPipelineStageFlags dstStageMask) {
// Create an image barrier object
VkImageMemoryBarrier imageMemoryBarrier = {};
imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
imageMemoryBarrier.pNext = nullptr;
imageMemoryBarrier.oldLayout = oldImageLayout;
imageMemoryBarrier.newLayout = newImageLayout;
imageMemoryBarrier.image = image;
imageMemoryBarrier.subresourceRange = subresourceRange;

// Source layouts (old)
// Source access mask controls actions that have to be finished on the old layout
// before it will be transitioned to the new layout
switch (oldImageLayout)
{
case VK_IMAGE_LAYOUT_UNDEFINED:
// Image layout is undefined (or does not matter)
// Only valid as initial layout
// No flags required, listed only for completeness
imageMemoryBarrier.srcAccessMask = 0;
break;

case VK_IMAGE_LAYOUT_PREINITIALIZED:
// Image is preinitialized
// Only valid as initial layout for linear images, preserves memory contents
// Make sure host writes have been finished
imageMemoryBarrier.srcAccessMask = VK_ACCESS_HOST_WRITE_BIT;
break;

case VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL:
// Image is a color attachment
// Make sure any writes to the color buffer have been finished
imageMemoryBarrier.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
break;

case VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL:
// Image is a depth/stencil attachment
// Make sure any writes to the depth/stencil buffer have been finished
imageMemoryBarrier.srcAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
break;

case VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL:
// Image is a transfer source
// Make sure any reads from the image have been finished
imageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
break;

case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:
// Image is a transfer destination
// Make sure any writes to the image have been finished
imageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
break;

case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL:
// Image is read by a shader
// Make sure any shader reads from the image have been finished
imageMemoryBarrier.srcAccessMask = VK_ACCESS_SHADER_READ_BIT;
break;
}

// Target layouts (new)
// Destination access mask controls the dependency for the new image layout
switch (newImageLayout)
{
case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:
// Image will be used as a transfer destination
// Make sure any writes to the image have been finished
imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
break;

case VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL:
// Image will be used as a transfer source
// Make sure any reads from and writes to the image have been finished
//imageMemoryBarrier.srcAccessMask = imageMemoryBarrier.srcAccessMask | VK_ACCESS_TRANSFER_READ_BIT;
imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
break;

case VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL:
// Image will be used as a color attachment
// Make sure any writes to the color buffer have been finished
imageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
imageMemoryBarrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
break;

case VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL:
// Image layout will be used as a depth/stencil attachment
// Make sure any writes to depth/stencil buffer have been finished
imageMemoryBarrier.dstAccessMask = imageMemoryBarrier.dstAccessMask | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
break;

case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL:
// Image will be read in a shader (sampler, input attachment)
// Make sure any writes to the image have been finished
if (imageMemoryBarrier.srcAccessMask == 0)
{
imageMemoryBarrier.srcAccessMask = VK_ACCESS_HOST_WRITE_BIT | VK_ACCESS_TRANSFER_WRITE_BIT;
}
imageMemoryBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
break;
}

// Put barrier inside setup command buffer
vkCmdPipelineBarrier(
cmdbuffer,
srcStageMask,
dstStageMask,
0,
0, nullptr,
0, nullptr,
1, &imageMemoryBarrier);
}*/

/*grail::VkTexture grail::Renderer::loadTexture(const std::string & path, TextureCreateInfo & createInfo) {
	VkTexture texture = VkTexture();
	
	uint32_t size;
	unsigned char* contents;
	
	int bpp;
	int width;
	int height;

	uint32_t mipLevels;

	grail::FileIO::readBinaryFile(path, &contents, &size);

	unsigned char* imageData = stbi_load_from_memory(contents, size, &width, &height, &bpp, STBI_rgb_alpha);

	GRAIL_LOG(INFO, "LOADING TEXTURE") << path;

	if (!imageData) {
		Log(LogType::ERROR, "TEXTURE LOADING") << "Failed to load texture \"" << path << "\": " << stbi_failure_reason();
		throw new exceptions::RuntimeException();
		return texture;
	}

	if (width <= 0 || height <= 0) {
		Log(LogType::ERROR, "TEXTURE LOADING") << "Failed to load texture \"" << path;
		throw new exceptions::RuntimeException();
		return texture;
	}

	mipLevels = createInfo.generateMipmaps ? std::floor(std::log2(std::max(width, height))) + 1 : 1;

	texture.width = width;
	texture.height = height;
	texture.mipLevels = mipLevels;

	VkFormatProperties formatProperties;
	vkGetPhysicalDeviceFormatProperties(createInfo.gpu.physicalDevice, createInfo.format, &formatProperties);

	VkMemoryAllocateInfo memAllocateInfo = {};
	memAllocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	memAllocateInfo.pNext = nullptr;
	VkMemoryRequirements memoryReqs;

	VkBuffer stageBuffer;

	VkBufferCreateInfo bufferInfo = {};
	bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferInfo.size = width * height * 4;
	bufferInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
	bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	VK_VALIDATE_RESULT(vkCreateBuffer(createInfo.gpu.device, &bufferInfo, nullptr, &stageBuffer));

	vkGetBufferMemoryRequirements(mainGPU.device, stageBuffer, &memoryReqs);
	MemoryRegionDescriptor stageRegion = stagingMemory->allocateRegion(memoryReqs.size, memoryReqs.alignment);
	VK_VALIDATE_RESULT(vkBindBufferMemory(createInfo.gpu.device, stageBuffer, stagingMemory->getMemoryHandle(), stageRegion.offset));

	void* ptr;
	ptr = stagingMemory->mapMemory(stageRegion);
	{
		memcpy(ptr, imageData, (width * height * 4));
	}
	stagingMemory->unmapMemory();


	VkImageCreateInfo imageCreateInfo = {};
	imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	imageCreateInfo.pNext = nullptr;
	imageCreateInfo.flags = 0;
	imageCreateInfo.imageType = VK_IMAGE_TYPE_2D;
	imageCreateInfo.format = createInfo.format;
	imageCreateInfo.mipLevels = mipLevels;
	imageCreateInfo.arrayLayers = 1;
	imageCreateInfo.samples = VK_SAMPLE_COUNT_1_BIT;
	imageCreateInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
	imageCreateInfo.usage = VK_IMAGE_USAGE_SAMPLED_BIT;
	imageCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	imageCreateInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	imageCreateInfo.extent = { static_cast<uint32_t>(width), static_cast<uint32_t>(height), 1 };
	imageCreateInfo.usage = createInfo.usageFlags;

	VK_VALIDATE_RESULT(vkCreateImage(createInfo.gpu.device, &imageCreateInfo, nullptr, &texture.image));

	vkGetImageMemoryRequirements(createInfo.gpu.device, texture.image, &memoryReqs);

	MemoryRegionDescriptor region = textureMemory->allocateRegion(memoryReqs.size, memoryReqs.alignment);

	VK_VALIDATE_RESULT(vkBindImageMemory(createInfo.gpu.device, texture.image, textureMemory->getMemoryHandle(), region.offset));

	VkCommandBufferBeginInfo cmdBufferBeginInfo = {};
	cmdBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	cmdBufferBeginInfo.pNext = nullptr;
	cmdBufferBeginInfo.flags = 0;
	VK_VALIDATE_RESULT(vkBeginCommandBuffer(setupCommandBuffer, &cmdBufferBeginInfo));

	VkImageSubresourceRange subresourceRange = {};
	subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	subresourceRange.baseMipLevel = 0;
	subresourceRange.levelCount = 1;
	subresourceRange.layerCount = 1;

	VkImageLayout imageLayout;

	setImageLayout(
		setupCommandBuffer,
		texture.image,
		VK_IMAGE_ASPECT_COLOR_BIT,
		VK_IMAGE_LAYOUT_UNDEFINED,
		VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
		subresourceRange
	);

	VkBufferImageCopy bufferCopyRegion = {};
	bufferCopyRegion.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	bufferCopyRegion.imageSubresource.mipLevel = 0;
	bufferCopyRegion.imageSubresource.baseArrayLayer = 0;
	bufferCopyRegion.imageSubresource.layerCount = 1;
	bufferCopyRegion.imageExtent.width = width;
	bufferCopyRegion.imageExtent.height = height;
	bufferCopyRegion.imageExtent.depth = 1;

	vkCmdCopyBufferToImage(
		setupCommandBuffer,
		stageBuffer,
		texture.image,
		VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
		1,
		&bufferCopyRegion
	);

	imageLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
	setImageLayout(
		setupCommandBuffer,
		texture.image,
		VK_IMAGE_ASPECT_COLOR_BIT,
		VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
		imageLayout,
		subresourceRange
	);

	VK_VALIDATE_RESULT(vkEndCommandBuffer(setupCommandBuffer));

	{
		VkSubmitInfo submitInfo = {};
		submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		submitInfo.commandBufferCount = 1;
		submitInfo.pCommandBuffers = &setupCommandBuffer;

		VK_VALIDATE_RESULT(vkQueueSubmit(graphicsQueue, 1, &submitInfo, (VkFence)nullptr));
		VK_VALIDATE_RESULT(vkQueueWaitIdle(graphicsQueue));
	}

	VK_VALIDATE_RESULT(vkBeginCommandBuffer(setupCommandBuffer, &cmdBufferBeginInfo));

	for (int i = 1; i < mipLevels; i++) {
		VkImageBlit imageBlit = {};

		imageBlit.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		imageBlit.srcSubresource.layerCount = 1;
		imageBlit.srcSubresource.mipLevel = i - 1;
		imageBlit.srcOffsets[1].x = int32_t(width >> (i - 1));
		imageBlit.srcOffsets[1].y = int32_t(height >> (i - 1));
		imageBlit.srcOffsets[1].z = 1;


		imageBlit.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		imageBlit.dstSubresource.layerCount = 1;
		imageBlit.dstSubresource.mipLevel = i;
		imageBlit.dstOffsets[1].x = int32_t(width >> i);
		imageBlit.dstOffsets[1].y = int32_t(height >> i);
		imageBlit.dstOffsets[1].z = 1;

		VkImageSubresourceRange mipSubRange = {};
		mipSubRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		mipSubRange.baseMipLevel = i;
		mipSubRange.levelCount = 1;
		mipSubRange.layerCount = 1;

		setImageLayout(
			setupCommandBuffer,
			texture.image,
			VK_IMAGE_ASPECT_COLOR_BIT,
			VK_IMAGE_LAYOUT_UNDEFINED,
			VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
			mipSubRange,
			VK_PIPELINE_STAGE_TRANSFER_BIT,
			VK_PIPELINE_STAGE_TRANSFER_BIT);

		// Blit from previous level
		vkCmdBlitImage(
			setupCommandBuffer,
			texture.image,
			VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
			texture.image,
			VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
			1,
			&imageBlit,
			VK_FILTER_LINEAR);

		// Transiton current mip level to transfer source for read in next iteration
		setImageLayout(
			setupCommandBuffer,
			texture.image,
			VK_IMAGE_ASPECT_COLOR_BIT,
			VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
			VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
			mipSubRange,
			VK_PIPELINE_STAGE_TRANSFER_BIT,
			VK_PIPELINE_STAGE_TRANSFER_BIT);
	}

	subresourceRange.levelCount = mipLevels;
	setImageLayout(
		setupCommandBuffer,
		texture.image,
		VK_IMAGE_ASPECT_COLOR_BIT,
		VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
		VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
		subresourceRange);

	VK_VALIDATE_RESULT(vkEndCommandBuffer(setupCommandBuffer));
	{
		VkSubmitInfo submitInfo = {};
		submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		submitInfo.commandBufferCount = 1;
		submitInfo.pCommandBuffers = &setupCommandBuffer;

		VK_VALIDATE_RESULT(vkQueueSubmit(graphicsQueue, 1, &submitInfo, (VkFence)nullptr));
		VK_VALIDATE_RESULT(vkQueueWaitIdle(graphicsQueue));
	}
	stagingMemory->clearRegions();

	stbi_image_free(imageData);

	VkSamplerCreateInfo samplerInfo = {};
	samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
	samplerInfo.flags = 0;
	samplerInfo.pNext = nullptr;
	samplerInfo.magFilter = static_cast<VkFilter>(createInfo.samplerInfo.magFilter);
	samplerInfo.minFilter = static_cast<VkFilter>(createInfo.samplerInfo.minFilter);
	samplerInfo.mipmapMode = static_cast<VkSamplerMipmapMode>(createInfo.samplerInfo.mipmapMode);
	samplerInfo.addressModeU = static_cast<VkSamplerAddressMode>(createInfo.samplerInfo.addressModeU);
	samplerInfo.addressModeV = static_cast<VkSamplerAddressMode>(createInfo.samplerInfo.addressModeV);
	samplerInfo.addressModeW = static_cast<VkSamplerAddressMode>(createInfo.samplerInfo.addressModeW);
	samplerInfo.mipLodBias = createInfo.samplerInfo.mipLodBias;
	samplerInfo.compareOp = static_cast<VkCompareOp>(createInfo.samplerInfo.compareOp);
	samplerInfo.minLod = createInfo.samplerInfo.minLod;
	samplerInfo.maxLod = (float)mipLevels;
	samplerInfo.maxAnisotropy = createInfo.samplerInfo.maxAnisotropy;
	samplerInfo.anisotropyEnable = createInfo.samplerInfo.anisotropyEnable;
	samplerInfo.borderColor = static_cast<VkBorderColor>(createInfo.samplerInfo.borderColor);
	VK_VALIDATE_RESULT(vkCreateSampler(createInfo.gpu.device, &samplerInfo, nullptr, &texture.sampler));

	VkImageViewCreateInfo view = {};
	view.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	view.pNext = nullptr;
	view.viewType = VK_IMAGE_VIEW_TYPE_2D;
	view.format = createInfo.format;
	view.components = { VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A };
	view.subresourceRange = { VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 };
	view.subresourceRange.levelCount = mipLevels;
	view.image = texture.image;
	VK_VALIDATE_RESULT(vkCreateImageView(createInfo.gpu.device, &view, nullptr, &texture.imageView));

	texture.imageLayout = imageLayout;

	texture.descriptorImageInfo = {};
	texture.descriptorImageInfo.imageLayout = texture.imageLayout;
	texture.descriptorImageInfo.imageView = texture.imageView;
	texture.descriptorImageInfo.sampler = texture.sampler;

	return texture;
}

void grail::Renderer::setImageLayout(VkCommandBuffer cmdbuffer, VkImage image, VkImageAspectFlags aspectMask, VkImageLayout oldImageLayout, VkImageLayout newImageLayout, VkImageSubresourceRange subresourceRange) {
	// Create an image barrier object
	VkImageMemoryBarrier imageMemoryBarrier = {};
	imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
	imageMemoryBarrier.pNext = nullptr;
	imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	imageMemoryBarrier.oldLayout = oldImageLayout;
	imageMemoryBarrier.newLayout = newImageLayout;
	imageMemoryBarrier.image = image;
	imageMemoryBarrier.subresourceRange = subresourceRange;

	// Source layouts (old)
	// Source access mask controls actions that have to be finished on the old layout
	// before it will be transitioned to the new layout
	switch (oldImageLayout)
	{
	case VK_IMAGE_LAYOUT_UNDEFINED:
		// Image layout is undefined (or does not matter)
		// Only valid as initial layout
		// No flags required, listed only for completeness
		imageMemoryBarrier.srcAccessMask = 0;
		break;

	case VK_IMAGE_LAYOUT_PREINITIALIZED:
		// Image is preinitialized
		// Only valid as initial layout for linear images, preserves memory contents
		// Make sure host writes have been finished
		imageMemoryBarrier.srcAccessMask = VK_ACCESS_HOST_WRITE_BIT;
		break;

	case VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL:
		// Image is a color attachment
		// Make sure any writes to the color buffer have been finished
		imageMemoryBarrier.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		break;

	case VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL:
		// Image is a depth/stencil attachment
		// Make sure any writes to the depth/stencil buffer have been finished
		imageMemoryBarrier.srcAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
		break;

	case VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL:
		// Image is a transfer source 
		// Make sure any reads from the image have been finished
		imageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
		break;

	case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:
		// Image is a transfer destination
		// Make sure any writes to the image have been finished
		imageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		break;

	case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL:
		// Image is read by a shader
		// Make sure any shader reads from the image have been finished
		imageMemoryBarrier.srcAccessMask = VK_ACCESS_SHADER_READ_BIT;
		break;
	}

	// Target layouts (new)
	// Destination access mask controls the dependency for the new image layout
	switch (newImageLayout)
	{
	case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:
		// Image will be used as a transfer destination
		// Make sure any writes to the image have been finished
		imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		break;

	case VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL:
		// Image will be used as a transfer source
		// Make sure any reads from and writes to the image have been finished
		//imageMemoryBarrier.srcAccessMask = imageMemoryBarrier.srcAccessMask | VK_ACCESS_TRANSFER_READ_BIT;
		imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
		break;

	case VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL:
		// Image will be used as a color attachment
		// Make sure any writes to the color buffer have been finished
		imageMemoryBarrier.srcAccessMask = VK_ACCESS_SHADER_READ_BIT;
		imageMemoryBarrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		break;

	case VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL:
		// Image layout will be used as a depth/stencil attachment
		// Make sure any writes to depth/stencil buffer have been finished
		imageMemoryBarrier.dstAccessMask = imageMemoryBarrier.dstAccessMask | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
		break;

	case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL:
		// Image will be read in a shader (sampler, input attachment)
		// Make sure any writes to the image have been finished
		if (imageMemoryBarrier.srcAccessMask == 0)
		{
			imageMemoryBarrier.srcAccessMask = VK_ACCESS_HOST_WRITE_BIT | VK_ACCESS_TRANSFER_WRITE_BIT;
		}
		imageMemoryBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
		break;
	}

	// Put barrier on top
	VkPipelineStageFlags srcStageFlags = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;
	VkPipelineStageFlags destStageFlags = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;

	/*Log(LogType::INFO, "IMG OLD LAYOUT") << oldImageLayout;
	Log(LogType::INFO, "IMG NEW LAYOUT") << newImageLayout;
	Log(LogType::INFO, "IMG LAYOUT SRC") << imageMemoryBarrier.srcAccessMask;
	Log(LogType::INFO, "IMG LAYOUT DST") << imageMemoryBarrier.dstAccessMask;

	// Put barrier inside setup command buffer
	vkCmdPipelineBarrier(
		cmdbuffer,
		srcStageFlags,
		destStageFlags,
		0,
		0, nullptr,
		0, nullptr,
		1, &imageMemoryBarrier);
}
*/