#include "MemoryRegion.h"
#include "Exceptions.h"

/*grail::MemoryRegion::MemoryRegion(GPU gpu, const std::string& identifier, uint32_t blockSize) {
	this->gpu = gpu;
	this->identifier = identifier;
	this->blockSize = blockSize;
}

/*bool grail::MemoryRegion::hasFlag(VkMemoryPropertyFlagBits flag) {
	return memoryFlags & flag;
}*/

/*grail::MemoryRegionDescriptor grail::MemoryRegion::allocateRegion(VkDeviceSize size, VkDeviceSize alignment, VkMemoryPropertyFlags flags, uint32_t memoryBits) {
	VkDeviceSize maxSize = size + alignment;

	MemoryBlock* availableBlock = nullptr;
	uint32_t requiredType = gpu.getMemoryType(memoryBits, flags);

	//GRAIL_LOG(INFO, "TYPE") << requiredType;

	for(MemoryBlock* block : blocks) {
		if(block->getRemainingMemory() >= maxSize) {
			if (block->memoryTypeIndex == requiredType) {
				availableBlock = block;
				break;
			}
		}
	}

	if(availableBlock == nullptr) {
		uint32_t allocationSize = blockSize * 1024 * 1024;

		if (allocationSize < maxSize)
			allocationSize = maxSize;

		availableBlock = allocateNewBlock(flags, memoryBits, allocationSize);
	}

	VkDeviceSize multiplier = (availableBlock->offset / alignment) + 1;
	VkDeviceSize alignedAddress = multiplier * alignment;
	VkDeviceSize padding = alignedAddress - availableBlock->offset;

	VkDeviceSize requiredSize = size + padding;

	/*GRAIL_LOG(INFO, "OFFSET") << availableBlock->offset << " " << availableBlock->offset / 1024.0 / 1024.0;
	GRAIL_LOG(INFO, "SIZE") << requiredSize << " " << requiredSize / 1024.0 / 1024.0;;
	GRAIL_LOG(INFO, "REMAINING") << availableBlock->getRemainingMemory() << " " << availableBlock->getRemainingMemory() / 1024.0 / 1024.0;;*/

	/*if (requiredSize > availableBlock->getRemainingMemory())
		throw new exceptions::OutOfMemoryException("Should not happen");

	MemoryRegionDescriptor descriptor = {};
	descriptor.alignedSize = requiredSize;
	descriptor.size = size;
	descriptor.alignment = alignment;
	descriptor.offset = availableBlock->offset + padding;
	descriptor.region = this;
	descriptor.block = availableBlock;

	availableBlock->allocations.push_back(descriptor);
	availableBlock->offset += requiredSize;

	//GRAIL_LOG(INFO, "hmmm") << identifier << "->" << getTotalUsedSpace() / 1024.0 / 1024.0;

	return descriptor;
}

VkDeviceSize grail::MemoryRegion::getTotalUsedSpace() {
	VkDeviceSize usedMem = 0;

	for (MemoryBlock* block : blocks) {
		usedMem += block->offset;
	}

	return usedMem;
}

VkDeviceSize grail::MemoryRegion::getTotalSpace() {
	VkDeviceSize usedMem = 0;

	for (MemoryBlock* block : blocks) {
		usedMem += block->size;
	}

	return usedMem;
}

std::string & grail::MemoryRegion::getName() {
	return identifier;
}

std::vector<grail::MemoryBlock*>& grail::MemoryRegion::getBlocks() {
	return blocks;
}

void grail::MemoryRegion::clearBlocks() {
	for (MemoryBlock* block : blocks) {
		block->offset = 0;
	}
}*/

/*grail::MemoryRegionDescriptor grail::MemoryRegion::allocateRegion(VkDeviceSize size, VkDeviceSize alignment) {
	VkDeviceSize multiplier = (offsetPointer / alignment) + 1;
	VkDeviceSize alignedAddress = multiplier * alignment;
	VkDeviceSize padding = alignedAddress - offsetPointer;

	VkDeviceSize requiredSize = size + padding;

	//GRAIL_LOG(INFO, "ALIGNMENT") << size << " " << padding;

	if (offsetPointer + requiredSize > this->size)
		throw new exceptions::OutOfMemoryException();

	MemoryRegionDescriptor descriptor = {};
	descriptor.alignedSize = requiredSize;
	descriptor.size = size;
	descriptor.alignment = alignment;
	descriptor.offset = offsetPointer + padding;
	descriptor.region = this;

	offsetPointer += requiredSize;

	return descriptor;
}*/

/*grail::MemoryBlock * grail::MemoryRegion::allocateNewBlock(VkMemoryPropertyFlags flags, uint32_t memoryBits, uint32_t blockSize) {
	GRAIL_LOG(INFO, "MEMORY REGION") << "Allocating new block on " << identifier << "...";

	MemoryBlock* block = new MemoryBlock();
	block->offset = 0;
	block->mapped = false;
	block->size = blockSize;
	block->flags = flags;
	block->memoryTypeIndex = gpu.getMemoryType(memoryBits, flags);
	block->region = this;

	VkMemoryAllocateInfo allocationInfo = {};
	allocationInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	allocationInfo.pNext = nullptr;
	allocationInfo.allocationSize = block->size;
	allocationInfo.memoryTypeIndex = block->memoryTypeIndex;

	VK_VALIDATE_RESULT(vkAllocateMemory(gpu.device, &allocationInfo, nullptr, &block->memoryHandle));

	this->blocks.push_back(block);

	return block;
}

/*void * grail::MemoryRegion::mapMemory() {
	MemoryRegionDescriptor descriptor = {};
	descriptor.size = size;
	descriptor.alignment = 0;
	descriptor.offset = 0;
	descriptor.region = this;

	return mapMemory(descriptor);
}*/

/*void * grail::MemoryRegion::mapMemory(MemoryRegionDescriptor & regionDescriptor) {
	if (!hasFlag(VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT))
		throw new exceptions::RuntimeException("Cannot map non host visible memory!");

	if (mapped)
		throw new exceptions::RuntimeException("Attempting to map already mapped memory!");

	void* mappedPointer;

	VK_VALIDATE_RESULT(vkMapMemory(gpu.device, memory, regionDescriptor.offset, regionDescriptor.size, 0, &mappedPointer));

	mapped = true;

	return mappedPointer;
}*/

/*void grail::MemoryRegion::unmapMemory() {
	if (!mapped)
		return;

	mapped = false;

	vkUnmapMemory(gpu.device, memory);
}*/

/*void grail::MemoryRegion::clearRegions() {
	offsetPointer = 0;
}*/

/*VkDeviceSize grail::MemoryRegion::getHeapSize() {
	return size;
}*/

/*VkDeviceMemory grail::MemoryRegion::getMemoryHandle() {
	return memory;
}*/

/*grail::GPU grail::MemoryRegion::getGPU() {
	return gpu;
}*/

/*VkDeviceSize grail::MemoryRegion::getOffsetPointer() {
	return offsetPointer;
}*/

/*VkDeviceSize grail::MemoryRegion::getRemainingMemory() {
	return size - offsetPointer;
}*/

/*void grail::MemoryRegion::dispose() {
	for (MemoryBlock* block : blocks) {
		if(block->memoryHandle != VK_NULL_HANDLE)
			vkFreeMemory(gpu.device, block->memoryHandle, nullptr);
	}

	blocks.clear();
}

VkDeviceSize grail::MemoryBlock::getRemainingMemory() {
	return size - offset;
}

void * grail::MemoryBlock::mapMemory() {
	MemoryRegionDescriptor descriptor = {};
	descriptor.size = size;
	descriptor.alignment = 0;
	descriptor.offset = 0;
	descriptor.region = this->region;
	descriptor.block = this;

	return mapMemory(descriptor);
}

void * grail::MemoryBlock::mapMemory(MemoryRegionDescriptor & range) {
	if (!(flags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT))
		throw new exceptions::RuntimeException("Cannot map non host visible memory!");

	if (mapped)
		unmapMemory();
		//throw new exceptions::RuntimeException("Attempting to map already mapped memory!");

	void* mappedPointer;

	VK_VALIDATE_RESULT(vkMapMemory(region->getGPU().device, memoryHandle, range.offset, range.size, 0, &mappedPointer));

	mapped = true;

	return mappedPointer;
}

void grail::MemoryBlock::unmapMemory() {
	if (!mapped)
		return;

	mapped = false;

	vkUnmapMemory(region->getGPU().device, memoryHandle);
}*/
