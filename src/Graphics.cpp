#include "Graphics.h"

float grail::Graphics::deltaTime = 0;
uint32_t grail::Graphics::width = 0;
uint32_t grail::Graphics::height = 0;
int grail::Graphics::framesPerSecond = 0;
int grail::Graphics::smoothedFPS = 0;
int grail::Graphics::framesInFlight = 0;
int grail::Graphics::frameIndex = 0;