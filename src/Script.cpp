#include "Script.h"

#include <angelscript.h>
#include <scriptstdstring/scriptstdstring.h>
#include <scriptbuilder/scriptbuilder.h>

#include "Scene.h"
#include "Utils.h"

std::string grail::Script::getScriptSource() {
	return scriptSource;
}

std::string grail::Script::getSectionName() {
	return sectionName;
}

std::string grail::Script::getName() {
	return name;
}

uint32_t grail::Script::getGlobalPosition() {
	return globalPosition;
}

asIScriptEngine* grail::Script::getEngine() {
	return engine;
}

asIScriptModule* grail::Script::getModule() {
	return scriptModule;
}

bool grail::Script::isCompiled() {
	return isScriptCompiled;
}

void grail::Script::setScriptSource(const std::string& source) {
	this->scriptSource = source;
	this->isScriptCompiled = false;
}

/*
void grail::Script::compile() {
	engine->SetUserData(this);

	errors.clear();
	compiled = false;

	if (module != nullptr) {
		engine->DiscardModule(module->GetName());
		module = nullptr;
	}
	
	int r = builder.StartNewModule(engine, moduleName.c_str());
/*	std::function<int(const char*, const char*, CScriptBuilder*, void*)> callback = [](const char *include, const char *from, CScriptBuilder *builder, void *userParam) -> int {
	
	};
	builder.SetIncludeCallback(&callback, nullptr);*/
/*

	if (r < 0) {
		GRAIL_LOG(ERROR, "SCRIPT") << "Error while starting new module " << r;
		return;
	}

	r = builder.AddSectionFromMemory("script_section", source.c_str(), source.size());
	if (r < 0) {
		GRAIL_LOG(ERROR, "SCRIPT") << "Error while adding a new section " << r;
		return;
	}

	r = builder.BuildModule();
	if (r < 0) {
		GRAIL_LOG(ERROR, "SCRIPT") << "Error while building module " << r;
		return;
	}

	compiled = true;
}

void grail::Script::setup(SceneNode * node) {
	exposedVars.clear();

	module = engine->GetModule(moduleName.c_str());

	if (context) {
		context->Release();
		context = nullptr;
	}
	context = engine->CreateContext();

	if (instance) {
		instance->Release();
		instance = nullptr;
	}


	asITypeInfo* type = nullptr;
	int tc = module->GetObjectTypeCount();

	for (int i = 0; i < tc; i++) {
		type = module->GetObjectTypeByIndex(i);

		if (type->GetBaseType()) {
			if (strcmp(type->GetBaseType()->GetName(), "NodeBehaviour") == 0) {

				updateFunction = type->GetMethodByDecl("void update(float)");
				initFunction = type->GetMethodByDecl("void start()");

				asIScriptFunction* constructor = type->GetFactoryByIndex(0);
				context->Prepare(constructor);
				context->Execute();
				asIScriptObject* obj = *((asIScriptObject**)context->GetAddressOfReturnValue());
				obj->AddRef();
				instance = obj;
				context->Unprepare();

				int varCount = obj->GetPropertyCount();
				for (int v = 0; v < varCount; v++) {
					if (strcmp(obj->GetPropertyName(v), "node") == 0) {
						SceneNode** address = (SceneNode**)obj->GetAddressOfProperty(v);
						*address = node;
					}
					else {
						const char* name = nullptr;
						int typeID;
						bool isPrivate;
						type->GetProperty(v, &name, &typeID, &isPrivate);


						if (!isPrivate) {
							//std::vector<std::string> metadata = builder.GetMetadataForTypeProperty(obj->GetTypeId(), v);
							//if (metadata.size() > 0) {
								//for (std::string& data : metadata) {
									//if (utils::string::equalsCaseInsensitive(data, "visible")) {
									//}
								//}
							//}

							ExposedScriptVar var;
							var.name = obj->GetPropertyName(v);
							var.address = obj->GetAddressOfProperty(v);

							int typeID = obj->GetPropertyTypeId(v);
							if (typeID == asTYPEID_FLOAT) {
								var.type = ScriptVarType::FLOAT;
							}

							if (typeID == asTYPEID_INT32) {
								var.type = ScriptVarType::INT;
							}

							if (typeID == asTYPEID_BOOL) {
								var.type = ScriptVarType::BOOL;
							}

							if (typeID == engine->GetTypeIdByDecl("string")) {
								var.type = ScriptVarType::STRING;
							}

							if (typeID == engine->GetTypeIdByDecl("Vec2")) {
								var.type = ScriptVarType::VEC2;
							}

							if (typeID == engine->GetTypeIdByDecl("Vec3")) {
								var.type = ScriptVarType::VEC3;
							}

							if (typeID == engine->GetTypeIdByDecl("Vec4")) {
								var.type = ScriptVarType::VEC4;
							}

							exposedVars.push_back(var);
						}
					}
				}
			}
		}
	}
}

void grail::Script::executeFunc() {;
	context->SetObject(instance);
	context->Execute();
}

void grail::Script::dispose() {
	if (module != nullptr) {
		engine->DiscardModule(module->GetName());
		module = nullptr;
	}

	if (context) {
		context->Release();
		context = nullptr;
	}

	if (instance) {
		instance->Release();
		instance = nullptr;
	}
}
*/