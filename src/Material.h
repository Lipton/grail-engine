#ifndef GRAIL_MATERIAL_H
#define GRAIL_MATERIAL_H

#include "DescriptorSet.h"
#include "VertexLayout.h"
#include "PipelineFactory.h"
#include "MemoryAllocator.h"
#include "VkBuffer.h"
#include "Shader.h"

namespace grail {
	class Shader;
	class ShaderPermutation;
	//class BaseMaterial;
	class Material;
	class VkTexture;

	// Types of material memory, useful for an editor, perhaps additional uses in the future
	enum class MaterialMemoryResourceType {
		COLOR,
		SLIDER,
		SCALAR,
		SCALAR_SLIDER
	};

	// Struct defining a material memory variable
	struct MaterialMemoryResource {
		// Default memory defined in the material description
		std::vector<float> defaultValues;

		// Resource Type
		MaterialMemoryResourceType type = MaterialMemoryResourceType::SCALAR;

		// Used by some resource typess
		float min = 0.0f;
		float max = 100.0f;
		float step = 1.0f;

		// Optional description to help in the editor
		std::string description;

		// Optional name to show in the editor
		std::string editorName;
	};

	struct MaterialMemoryBlock {

	};

	// Material types to compile permutations for
	// Shadow type also adds additional types for each cascade
	enum class MaterialType : uint32_t {
		GEOMETRY = 0,
		SHADOW = 1,
		SHADOW_CASCADE_0 = SHADOW,
		SHADOW_CASCADE_1 = SHADOW + 1,
		SHADOW_CASCADE_2 = SHADOW + 2,
		SHADOW_CASCADE_3 = SHADOW + 3
	};

	// Material description, used to create materials from, contains parsed default values, base shaders, options, etc.
	class MaterialDescription {
		friend class MaterialLibrary;
		friend class Resources;
		friend class Editor;
		friend class MaterialParser;
		friend class Material;
	public:
		MaterialDescription(const std::string& name);

		std::string getIdentifier();

		// Returns the base material under this description (derived id = 0)
		Material* getBaseMaterial();

		// Get a material based on the key
		Material* getMaterial(uint64_t key);

		// Does a material with the given key exists
		bool materialExists(uint64_t key);

		Material* createMaterial(
			std::vector<MaterialType>& types,
			std::vector<std::string>& options,
			VertexLayout vertexLayout, 
			DescriptorLayout descriptorLayout, 
			std::vector<ShaderPermutation*> shaderPermutations,
			std::vector<GraphicsPipelineCreateInfo>& pipelineInfos
		);

		// Used to create a material more easily, for instance for post process materials, might be moved away or made easier to use in the future
		Material* createMaterial(
			VertexLayout vertexLayout,
			DescriptorLayout descriptorLayout,
			std::vector<ShaderPermutation*> shaderPermutations,
			GraphicsPipelineCreateInfo& pipelineInfo
		);

		// Create a compute material
		Material* createComputeMaterial(DescriptorLayout descriptorLayout, std::vector<ShaderPermutation*> shaderPermutations, GraphicsPipelineCreateInfo& pipelineInfo);

		// Tries to get the default resource value by ID, returns an empty string if not found
		std::string getResourceDefault(const std::string& id);


	private:

		std::string identifier;

		// Description for defined memory types
		std::unordered_map<std::string, MaterialMemoryResource> memoryDescriptions;

		// Default handles for the resources
		// NOTE: Could be modified in the future to support resource definitions in descriptor instead of only handles
		std::unordered_map<std::string, std::string> resourceDefaults;

		// Defined options in the material descriptor
		std::vector<std::string> materialOptions;

		// Option indices by name
		std::unordered_map<std::string, uint64_t> optionIndices;

		// Base shaders used by the material
		std::vector<Shader*> baseShaders;

		// Unique derived materials
		std::unordered_map<uint64_t, Material*> derivedMaterials;

		// Defined material types
		std::vector<MaterialType> definedTypes;

		// Pipeline factory to create the pipelines
		PipelineFactory* pipelineFactory;

		// The memory region containing all of the material memory blocks
		// Memory region grows 16 MB per block
		//MemoryAllocator* materialMemoryRegion;
	};

	// Contains vertex layouts, descriptor layouts and more concrete information about a material, used to create material instances
	class Material {
		friend class MaterialDescription;
		friend class Editor;
	public:
		// gets the pipeline for the GEOMETRY type
		VkPipeline getPipeline();

		// get the pipeline for the specified type, crashes if requested type pipeline does not exist
		VkPipeline getPipeline(MaterialType type);

		class MaterialInstance* createInstance();

		// Get this materials composed key
		uint64_t getKey();

		BufferBlock& getMemoryBlockDescription();
	private:
		Material(VertexLayout vertexLayout);
		Material();

		// Description that this material is derived from
		MaterialDescription* description;

		VertexLayout vertexLayout;
		
		DescriptorLayout descriptorLayout;

		// Pemutations of base shaders used by the material
		std::vector<ShaderPermutation*> shaders;

		// Created pipelines, one pipeline per type with the exception of shadow
		std::vector<VkPipeline> pipelines;

		// Key composed of all activated options
		uint64_t key;

		// The uniform block that stores the material memory
		BufferBlock materialBlock;
	};

	// The actual material instance used in rendering
	class MaterialInstance {
		friend class Material;
	public:
		DescriptorGroup& getCurrentFrameDescriptor();

		void setDescriptorBinding(uint32_t set, uint32_t binding, void* resource, void* descriptor);
		void setDescriptorBinding(const std::string& identifier, void* resource, void* descriptor);

		void setDescriptorTexture(const std::string& identifier, VkTexture* texture);

		// updates only the next frame descriptors
		void updateDescriptors();

		// updates all of the descriptors, use only when needed
		void updateAllDescriptors();

		// gets the pipeline for the GEOMETRY type
		VkPipeline getPipeline();

		// get the pipeline for the specified type, crashes if requested type pipeline does not exist
		VkPipeline getPipeline(MaterialType type);

		Material* getBaseMaterial();

		MaterialDescription* getMaterialDescription();

		// Attempt to copy as many relevant mathcing information from the source instnace
		// Does nothing if the material description does not match
		void copyFrom(MaterialInstance* src);

		// Gives a pointer to the start of the material memory, returns nullptr if material does not have memory
		void* getMemoryPtr();

		BufferBlock& getMemoryBlockDescription();

		// Set float value of variable under name 
		void setFloat(const std::string& name, float value);

		void setVec2(const std::string& name, glm::vec2 value);
		void setVec2(const std::string& name, float value1, float value2);

		void setVec3(const std::string& name, glm::vec3 value);
		void setVec3(const std::string& name, float value1, float value2, float value3);

		void setVec4(const std::string& name, glm::vec4 value);
		void setVec4(const std::string& name, float value1, float value2, float value3, float value4);

		void setMat3(const std::string& name, glm::mat3 value);
		void setMat4(const std::string& name, glm::mat4 value);

		void dispose();
	private:
		// Material that this is derived from
		Material* baseMaterial;

		// Material description that this is derived from for convenience
		MaterialDescription* baseDescription;

		// Descriptor groups for the instance, one per pre rendered frame
		std::vector<DescriptorGroup> descriptorGroups;

		// Buffer holding the material memory
		Buffer* memoryBuffer;
		// Memory region handle of the buffer
		MemoryAllocation memoryRegion;

		MaterialInstance();

		void* getMemoryPtrOfVariable(const std::string& name, uint32_t& varSize);
	};
}

#endif