#include "PipelineFactory.h"
#include "VertexLayout.h"

VkPipeline grail::PipelineFactory::createPipeline(GraphicsPipelineCreateInfo & info, VkPipeline basePipeline) {
	VkPipelineInputAssemblyStateCreateInfo inputAssemblyState = {};
	inputAssemblyState.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	inputAssemblyState.flags = 0;
	inputAssemblyState.pNext = nullptr;
	inputAssemblyState.topology = static_cast<VkPrimitiveTopology>(info.assemblyState.topology);
	inputAssemblyState.primitiveRestartEnable = info.assemblyState.primitiveRestartEnable;

	VkPipelineRasterizationStateCreateInfo rasterizationState = {};
	rasterizationState.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	rasterizationState.flags = 0;
	rasterizationState.pNext = nullptr;
	rasterizationState.depthClampEnable = info.rasterizationState.depthClampEnable;
	rasterizationState.rasterizerDiscardEnable = info.rasterizationState.rasterizerDiscardEnable;
	rasterizationState.polygonMode = static_cast<VkPolygonMode>(info.rasterizationState.polygonMode);
	rasterizationState.cullMode = static_cast<VkCullModeFlagBits>(info.rasterizationState.cullMode);
	rasterizationState.frontFace = static_cast<VkFrontFace>(info.rasterizationState.frontFace);
	rasterizationState.depthBiasClamp = info.rasterizationState.depthBiasClamp;
	rasterizationState.depthBiasConstantFactor = info.rasterizationState.depthBiasConstantFactor;
	rasterizationState.depthBiasClamp = info.rasterizationState.depthBiasClamp;
	rasterizationState.depthBiasSlopeFactor = info.rasterizationState.depthBiasSlopeFactor;
	rasterizationState.lineWidth = info.rasterizationState.lineWidth;

	std::vector<VkPipelineColorBlendAttachmentState> blendAttachments;
	blendAttachments.resize(info.colorBlendState.attachmentCount);
	for (uint32_t i = 0; i < blendAttachments.size(); i++) {
		VkPipelineColorBlendAttachmentState state = {};
		state.blendEnable = info.colorBlendState.blendEnable;
		state.srcColorBlendFactor = static_cast<VkBlendFactor>(info.colorBlendState.srcColorBlendFactor);
		state.dstColorBlendFactor = static_cast<VkBlendFactor>(info.colorBlendState.dstColorBlendFactor);
		state.colorBlendOp = static_cast<VkBlendOp>(info.colorBlendState.colorBlendOp);

		state.srcAlphaBlendFactor = static_cast<VkBlendFactor>(info.colorBlendState.srcAlphaBlendFactor);
		state.dstAlphaBlendFactor = static_cast<VkBlendFactor>(info.colorBlendState.dstAlphaBlendFactor);
		state.alphaBlendOp = static_cast<VkBlendOp>(info.colorBlendState.alphaBlendOp);

		state.colorWriteMask = static_cast<VkColorComponentFlagBits>(info.colorBlendState.colorWriteMask);
	
		blendAttachments[i] = state;
	}
	VkPipelineColorBlendStateCreateInfo colorBlendState = {};
	colorBlendState.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	colorBlendState.flags = 0;
	colorBlendState.pNext = nullptr;
	colorBlendState.attachmentCount = static_cast<uint32_t>(blendAttachments.size());
	colorBlendState.pAttachments = blendAttachments.size() > 0 ? &blendAttachments[0] : nullptr;

	VkPipelineViewportStateCreateInfo viewportState = {};
	viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	viewportState.flags = 0;
	viewportState.pNext = nullptr;
	viewportState.viewportCount = 1;
	viewportState.scissorCount = 1;

	std::vector<VkDynamicState> dynamicStates;
	if (info.dynamicState.viewport) dynamicStates.push_back(VK_DYNAMIC_STATE_VIEWPORT);
	if (info.dynamicState.scissor) dynamicStates.push_back(VK_DYNAMIC_STATE_SCISSOR);
	if (info.dynamicState.lineWidth) dynamicStates.push_back(VK_DYNAMIC_STATE_LINE_WIDTH);
	if (info.dynamicState.depthBias) dynamicStates.push_back(VK_DYNAMIC_STATE_DEPTH_BIAS);
	if (info.dynamicState.blendConstants) dynamicStates.push_back(VK_DYNAMIC_STATE_BLEND_CONSTANTS);
	if (info.dynamicState.depthBounds) dynamicStates.push_back(VK_DYNAMIC_STATE_DEPTH_BOUNDS);
	if (info.dynamicState.stencilCompareMask) dynamicStates.push_back(VK_DYNAMIC_STATE_STENCIL_COMPARE_MASK);
	if (info.dynamicState.stencilWriteMask) dynamicStates.push_back(VK_DYNAMIC_STATE_STENCIL_WRITE_MASK);
	if (info.dynamicState.stencilReference) dynamicStates.push_back(VK_DYNAMIC_STATE_STENCIL_REFERENCE);
	VkPipelineDynamicStateCreateInfo dynamicState = {};
	dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
	dynamicState.flags = 0;
	dynamicState.pNext = nullptr;
	dynamicState.dynamicStateCount = static_cast<uint32_t>(dynamicStates.size());
	dynamicState.pDynamicStates = &dynamicStates[0];

	VkPipelineDepthStencilStateCreateInfo depthStencilState = {};
	depthStencilState.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
	depthStencilState.flags = 0;
	depthStencilState.pNext = nullptr;
	depthStencilState.depthTestEnable = info.depthStencilState.depthTestEnable;
	depthStencilState.depthWriteEnable = info.depthStencilState.depthWriteEnable;
	depthStencilState.depthCompareOp = static_cast<VkCompareOp>(info.depthStencilState.depthCompareOp);
	depthStencilState.depthBoundsTestEnable = info.depthStencilState.depthBoundsTestEnable;
	depthStencilState.stencilTestEnable = info.depthStencilState.stencilTestEnable;
	depthStencilState.front.failOp = static_cast<VkStencilOp>(info.depthStencilState.front.failOp);
	depthStencilState.front.passOp = static_cast<VkStencilOp>(info.depthStencilState.front.passOp);
	depthStencilState.front.depthFailOp = static_cast<VkStencilOp>(info.depthStencilState.front.depthFailOp);
	depthStencilState.front.compareOp = static_cast<VkCompareOp>(info.depthStencilState.front.compareOp);
	depthStencilState.front.compareMask = info.depthStencilState.front.compareMask;
	depthStencilState.front.writeMask = info.depthStencilState.front.writeMask;
	depthStencilState.front.reference = info.depthStencilState.front.reference;
	depthStencilState.back.failOp = static_cast<VkStencilOp>(info.depthStencilState.back.failOp);
	depthStencilState.back.passOp = static_cast<VkStencilOp>(info.depthStencilState.back.passOp);
	depthStencilState.back.depthFailOp = static_cast<VkStencilOp>(info.depthStencilState.back.depthFailOp);
	depthStencilState.back.compareOp = static_cast<VkCompareOp>(info.depthStencilState.back.compareOp);
	depthStencilState.back.compareMask = info.depthStencilState.back.compareMask;
	depthStencilState.back.writeMask = info.depthStencilState.back.writeMask;
	depthStencilState.back.reference = info.depthStencilState.back.reference;
	depthStencilState.minDepthBounds = info.depthStencilState.minDepthBounds;
	depthStencilState.maxDepthBounds = info.depthStencilState.maxDepthBounds;

	VkPipelineMultisampleStateCreateInfo multisampleState = {};
	multisampleState.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	multisampleState.flags = 0;
	multisampleState.pNext = nullptr;
	multisampleState.rasterizationSamples = static_cast<VkSampleCountFlagBits>(info.multisampleState.rasterizationSamples);
	multisampleState.sampleShadingEnable = info.multisampleState.sampleShadingEnable;
	multisampleState.pSampleMask = info.multisampleState.pSampleMask;
	multisampleState.alphaToCoverageEnable = info.multisampleState.alphaToCoverageEnable;
	multisampleState.alphaToOneEnable = info.multisampleState.alphaToOneEnable;

	std::vector<ShaderStage> activeStages;
	for (ShaderStage& stage : info.shaderStageState.stages) {
		if (stage.module != VK_NULL_HANDLE) {
			activeStages.push_back(stage);
		}
	}

	std::vector<VkPipelineShaderStageCreateInfo> shaderStages;
	shaderStages.resize(activeStages.size());
	for (uint32_t i = 0; i < shaderStages.size(); i++) {
		VkPipelineShaderStageCreateInfo shaderInfo = {};
		shaderInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		shaderInfo.flags = 0;
		shaderInfo.pNext = nullptr;
		shaderInfo.stage = static_cast<VkShaderStageFlagBits>(activeStages[i].stageFlags);
		shaderInfo.module = activeStages[i].module;
		shaderInfo.pName = activeStages[i].pName;

		shaderStages[i] = shaderInfo;
	}

	VkPipelineVertexInputStateCreateInfo vertexInputState = info.vertexLayout->getVertexInputState();

	VkPipelineTessellationStateCreateInfo tesselationInfo = {};
	tesselationInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO;
	tesselationInfo.pNext = nullptr;
	tesselationInfo.flags = 0;
	tesselationInfo.patchControlPoints = 0;

	VkGraphicsPipelineCreateInfo pipelineCreateInfo = {};
	pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	pipelineCreateInfo.flags = (basePipeline == VK_NULL_HANDLE) ? (info.allowDerivatives ? VK_PIPELINE_CREATE_ALLOW_DERIVATIVES_BIT : 0) : VK_PIPELINE_CREATE_DERIVATIVE_BIT;
	pipelineCreateInfo.pNext = nullptr;
	//
	pipelineCreateInfo.layout = info.pipelineLayout;
	//
	pipelineCreateInfo.renderPass = info.renderPass;
	pipelineCreateInfo.pVertexInputState = &vertexInputState;
	pipelineCreateInfo.pInputAssemblyState = &inputAssemblyState;
	pipelineCreateInfo.pRasterizationState = &rasterizationState;
	pipelineCreateInfo.pColorBlendState = &colorBlendState;
	pipelineCreateInfo.pMultisampleState = &multisampleState;
	pipelineCreateInfo.pDepthStencilState = &depthStencilState;
	pipelineCreateInfo.pViewportState = &viewportState;
	pipelineCreateInfo.pDynamicState = &dynamicState;
	pipelineCreateInfo.pTessellationState = &tesselationInfo;
	pipelineCreateInfo.stageCount = static_cast<uint32_t>(shaderStages.size());
	pipelineCreateInfo.pStages = &shaderStages[0];
	pipelineCreateInfo.basePipelineHandle = (basePipeline == VK_NULL_HANDLE) ? VK_NULL_HANDLE : basePipeline;

	VkPipeline pipeline = VK_NULL_HANDLE;
	VK_VALIDATE_RESULT(vkCreateGraphicsPipelines(info.gpu.device, cache, 1, &pipelineCreateInfo, nullptr, &pipeline));

	return pipeline;
}

VkPipeline grail::PipelineFactory::createComputePipeline(GraphicsPipelineCreateInfo & info, VkPipeline basePipeline) {
	ShaderStage& computeStage = info.shaderStageState.get(ShaderStageBits::COMPUTE);

	VkPipelineShaderStageCreateInfo shaderInfo = {};
	shaderInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	shaderInfo.flags = 0;
	shaderInfo.pNext = nullptr;
	shaderInfo.stage = static_cast<VkShaderStageFlagBits>(computeStage.stageFlags);
	shaderInfo.module = computeStage.module;
	shaderInfo.pName = computeStage.pName;

	VkComputePipelineCreateInfo pipelineCreateInfo = {};
	pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO;
	pipelineCreateInfo.flags = (basePipeline == VK_NULL_HANDLE) ? (info.allowDerivatives ? VK_PIPELINE_CREATE_ALLOW_DERIVATIVES_BIT : 0) : VK_PIPELINE_CREATE_DERIVATIVE_BIT;
	pipelineCreateInfo.pNext = nullptr;
	pipelineCreateInfo.stage = shaderInfo;
	pipelineCreateInfo.layout = info.pipelineLayout;
	pipelineCreateInfo.basePipelineHandle = (basePipeline == VK_NULL_HANDLE) ? VK_NULL_HANDLE : basePipeline;

	VkPipeline pipeline = VK_NULL_HANDLE;
	VK_VALIDATE_RESULT(vkCreateComputePipelines(info.gpu.device, cache, 1, &pipelineCreateInfo, nullptr, &pipeline));

	return pipeline;
}
