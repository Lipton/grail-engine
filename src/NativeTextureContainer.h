#ifndef GRAIL_NATIVE_TEXTURE_CONTAINER_H
#define GRAIL_NATIVE_TEXTURE_CONTAINER_H

#include "Common.h"

#include <gli/gli.hpp>

namespace grail {
	
	class NativeTextureContainer {
	public:
		NativeTextureContainer(const std::string& path);
		~NativeTextureContainer();

		bool isFloatFormat();
		bool hasMips();

		void* getDataPointer();
		uint32_t getDataSize();

		uint32_t getWidth();
		uint32_t getHeight();

		VkFormat getFormat();

		uint32_t getMipLevels();

		gli::extent2d getMipExtents(uint32_t index);
		uint32_t getMipSize(uint32_t index);

		ComponentSwizzle getSwizzleMask(uint32_t index);
	private:
		gli::texture2d* gliHandle = nullptr;

		void* dataPointer;
		uint32_t size;

		uint32_t width;
		uint32_t height;

		uint32_t mipLevels;

		VkFormat format;

		bool floatFormat;

		ComponentSwizzle swizzleMasks[4] = { ComponentSwizzle::RED, ComponentSwizzle::GREEN, ComponentSwizzle::BLUE, ComponentSwizzle::ALPHA };
	};
}

#endif