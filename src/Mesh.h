#ifndef GRAIL_MESH_H
#define GRAIL_MESH_H

#include "MemoryAllocator.h"
#include "Resource.h"

namespace grail {
	class Mesh {
		friend class MeshLoader;
	public:
		glm::vec3 boundsMin = glm::vec3(FLT_MAX);
		glm::vec3 boundsMax = glm::vec3(-FLT_MAX);

		ResourceType resourceType = ResourceType::UNKNOWN;

		std::string identifier;
		std::string baseAsset;

		std::vector<float> vertices;
		std::vector<uint32_t> indices;

		uint32_t indicesCount;
		uint32_t verticesCount;

		VkBuffer buffer;

		VkDeviceSize indexOffset;
		VkDeviceSize vertexOffset;

		MemoryAllocation memoryRegion;

		uint32_t vertexBufferSize = 0;
		uint32_t indexBufferSize = 0;
	private:
	};
}

#endif