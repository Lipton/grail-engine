#include "Grail.h"

#include "ScreenManager.h"
#include "ApplicationWindow.h"
#include "FileIO.h"
#include "Scene.h"
#include "ScriptProvider.h"
#include "Resources.h"
#include "MemoryAllocator.h"
#include "VR.h"

bool grail::Grail::running = false;

grail::Grail::Grail(EngineInitializationInfo & initInfo) {
	this->initInfo = initInfo;

	GRAIL_LOG(INFO, "ENGINE") << "Initializing engine...";

#if defined(OS_ANDROID)
	FileIO::android_app = initInfo.android_app;
#endif

	GRAIL_LOG(INFO, "SCREEN MANAGER") << "Initializing Screen Manager...";
	screenManager = new ScreenManager();

	GRAIL_LOG(INFO, "WINDOW") << "Initializing Application Window...";
	window = new ApplicationWindow();
	if (!window->initialize(initInfo, screenManager)) {
		GRAIL_LOG(ERROR, "WINDOW") << "Could not create application window";
		exit();
 
		return;
	}

	// Stall initialization until android is finished loading, does nothing on desktop platforms
	while (!window->load()) {
		window->processFrame();
	}

#if defined(OS_ANDROID)
	Graphics::width = ANativeWindow_getWidth(initInfo.android_app->window);
	Graphics::height = ANativeWindow_getHeight(initInfo.android_app->window);
#else
	Graphics::width = initInfo.engineConfiguration->windowWidth;
	Graphics::height = initInfo.engineConfiguration->windowHeight;
#endif
	Graphics::framesInFlight = initInfo.engineConfiguration->framesInFlight;

	GRAIL_LOG(INFO, "VULKAN CONTEXT") << "Initializing Vulkan Context...";
	vulkanContext = new VulkanContext();
	if (!vulkanContext->initializeContext(initInfo, window)) {
		GRAIL_LOG(ERROR, "VULKAN CONTEXT") << "Could not create Vulkan context";
		exit();

		return;
	}
	GRAIL_LOG(INFO, "ENGINE") << "Starting Engine...";
	Scene::Initialize();

	startEngine();
}

#include <queue>
 int frameTimesSize = 60;
 std::queue<float> frameTimes;
 float __frameTimesSum = 0;
 bool _fpsIgnoreNextFrame = false;

 int GetSmoothedFPS() {
	 return (int)(frameTimesSize / __frameTimesSum * 1.0f);
 }

void grail::Grail::startEngine() {
	running = true;

	Physics::initialize();
	ScriptProvider::initialize();

	Resources::setStagingMemoryAllocator(new MemoryAllocator(VulkanContext::mainGPU, "Staging Memory", 64 * 1024 * 1024));
	Resources::setTextureMemoryAllocator(new MemoryAllocator(VulkanContext::mainGPU, "Texture Memory", 128 * 1024 * 1024));
	Resources::setBufferMemoryAllocator(new MemoryAllocator(VulkanContext::mainGPU, "Buffer Memory", 32 * 1024 * 1024));
	Resources::setUniformBufferMemoryAllocator(new MemoryAllocator(VulkanContext::mainGPU, "Uniform Memory", 8 * 1024 * 1024));

	screenManager->registerScreen(initInfo.screenName, *initInfo.screen);
	screenManager->setScreen(initInfo.screenName);

	int frames = 0;
	float delta = 0;
	float timer = 0;

	while (!window->shouldClose() && running) {
		auto startTime = std::chrono::high_resolution_clock::now();
		window->processFrame();
		VR::processEvents();

		screenManager->render(delta);

		frames++;
		auto endTime = std::chrono::high_resolution_clock::now();
		auto timeDiff = std::chrono::duration<double, std::milli>(endTime - startTime).count();
		delta = (float)timeDiff * 0.001f;

		Graphics::deltaTime = delta;

		timer += timeDiff;

		if (timer > 1000.0f) {
			window->setTitle(std::to_string(frames).c_str());

			Graphics::framesPerSecond = frames;

			timer = 0;
			frames = 0;
		}


		while (frameTimes.size() >= frameTimesSize) {
			__frameTimesSum -= frameTimes.front();
			frameTimes.pop();
		}
		while (frameTimes.size() < frameTimesSize) {
			__frameTimesSum += delta;
			frameTimes.emplace(delta);
		}

		Graphics::smoothedFPS = GetSmoothedFPS();

		Graphics::frameIndex = (Graphics::frameIndex + 1) % Graphics::framesInFlight;

		VR::updatePoses();
	}

	exit();
}

void grail::Grail::exit() {
	logger::detail::dispose();

	if (vulkanContext != nullptr)
		delete vulkanContext;

	if (window != nullptr)
		delete window;
}
