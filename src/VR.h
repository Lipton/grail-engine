#ifndef GRAIL_VR_H
#define GRAIL_VR_H

#include "GLMIncludes.h"
#include "Platform.h"

#ifdef OS_NOT_SUPPORTED
#include <openvr.h>
#endif

namespace vr {
	class IVRSystem;
}


enum class HMD_Eye {
	LEFT = 0,
	RIGHT = 1
};

namespace grail {
	class VR {
		friend class Grail;
	public:
		static bool isHMDPresent();

		static void initialize();
		static bool isInitialized();
		
		static glm::mat4 getHMDPose();
		static glm::mat4 getProjectionForEye(HMD_Eye eye, float nearPlane, float farPlane);
		static glm::mat4 getPoseForEye(HMD_Eye eye);

		static void getPreferedRenderSize(uint32_t* w, uint32_t* h);
	private:
#ifdef OS_NOT_SUPPORTED
		static vr::IVRSystem* hmd;
		static vr::TrackedDevicePose_t trackedDevicePose[vr::k_unMaxTrackedDeviceCount];
		static glm::mat4 devicePose[vr::k_unMaxTrackedDeviceCount];

		static glm::mat4 hmdPose;
#endif

		static bool initialized;

		static void processEvents();
		
		// Not to be called
		static void updatePoses();

		VR& operator = (const VR&) = delete;
		VR(const VR&) = delete;
		VR() = default;
	};
}

#endif
