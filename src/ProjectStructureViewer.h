#ifndef GRAIL_PROJECT_STRUCTURE_VIEWER
#define GRAIL_PROJECT_STRUCTURE_VIEWER

#include "ImGuiHelper.h"
#include "ImGuizmo.h"

namespace grail {
	class ProjectStructureViewer {
	public:
		ProjectStructureViewer();

		void render();
	private:
		std::string currentPath = "./";
	};
}

#endif