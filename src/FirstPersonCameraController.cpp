#include "FirstPersonCameraController.h"
#include "PerspectiveCamera.h"

grail::FirstPersonCameraController::FirstPersonCameraController(PerspectiveCamera * camera) {
	this->camera = camera;
	this->velocity = 5.0f;
	this->degreesPerPixel = 0.002f;
	this->speedMod = 2.25f;

	firstPass = true;
}

void grail::FirstPersonCameraController::update(float delta, bool updateFrustrum) {
	movementVelocity = velocity;

	if (Input::isKeyPressed(Keys::LEFT_SHIFT)) {
		movementVelocity *= speedMod;
	}

	if (Input::isKeyPressed(Keys::W)) {
		glm::vec3 temp(camera->direction);
		temp = glm::normalize(temp);
		temp = temp * (delta * movementVelocity);
		camera->translate(temp);
	}

	if (Input::isKeyPressed(Keys::S)) {
		glm::vec3 temp(camera->direction);
		temp = glm::normalize(temp);
		temp = temp * (delta * -movementVelocity);
		camera->translate(temp);
	}

	if (Input::isKeyPressed(Keys::A)) {
		glm::vec3 temp(camera->direction);
		temp = glm::cross(temp, camera->up);
		temp = glm::normalize(temp);
		temp = temp * (delta * -movementVelocity);
		camera->translate(temp);
	}

	if (Input::isKeyPressed(Keys::D)) {
		glm::vec3 temp(camera->direction);
		temp = glm::cross(temp, camera->up);
		temp = glm::normalize(temp);
		temp = temp * (delta * movementVelocity);
		camera->translate(temp);
	}

	if (Input::isKeyPressed(Keys::E)) {
		glm::vec3 temp(camera->up);
		temp = glm::normalize(temp);
		temp = temp * (delta * movementVelocity);
		camera->translate(temp);
	}

	if (Input::isKeyPressed(Keys::Q)) {
		glm::vec3 temp(camera->up);
		temp = glm::normalize(temp);
		temp = temp * (delta * -movementVelocity);
		camera->translate(temp);
	}

	camera->update(updateFrustrum);
}

void grail::FirstPersonCameraController::updateMouseMoved(float x, float y) {
	y *= -1; // DIRTY HACK

	if (firstPass) {
		lastMouseX = x;
		lastMouseY = y;
		firstPass = false;
	}

	mouseDeltaX = -(x - lastMouseX) * degreesPerPixel;
	mouseDeltaY = -(y - lastMouseY) * degreesPerPixel;

	lastMouseX = x;
	lastMouseY = y;

	glm::mat4 rotationMat(1.0);
	rotationMat = glm::rotate(rotationMat, mouseDeltaX, camera->up);
	camera->direction = glm::vec3(rotationMat * glm::vec4(camera->direction, 1.0));

	glm::vec3 temp(camera->direction);
	temp = glm::cross(temp, camera->up);
	temp = glm::normalize(temp);
	rotationMat = glm::mat4(1.0);
	rotationMat = glm::rotate(rotationMat, mouseDeltaY, temp);
	camera->direction = glm::vec3(rotationMat * glm::vec4(camera->direction, 1.0));
}

void grail::FirstPersonCameraController::resetMouse() {
	firstPass = true;
}

void grail::FirstPersonCameraController::setVelocity(float velocity) {
	this->velocity = velocity;
}

float grail::FirstPersonCameraController::getVeloctiy() const {
	return velocity;
}

void grail::FirstPersonCameraController::setMouseSensitivity(float degreesPerPixel) {
	this->degreesPerPixel = degreesPerPixel;
}

float grail::FirstPersonCameraController::getMouseSensitivity() const {
	return degreesPerPixel;
}

void grail::FirstPersonCameraController::setCrouchModifier(float crouchModifier) {
	this->speedMod = crouchModifier;
}

float grail::FirstPersonCameraController::getCrouchModifier() const {
	return speedMod;
}