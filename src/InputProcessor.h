#ifndef GRAIL_INPUT_PROCESSOR_H
#define GRAIL_INPUT_PROCESSOR_H

#include "Common.h"

namespace grail {
	class InputProcessor {
		friend class Screen;
		friend class ScreenManager;
	public:
		inline void setOnKeyPress(std::function<void(int, int)> keyEvent) { this->keyEvent = keyEvent; }
		inline void setOnMousePress(std::function<void(int, int)> mouseEvent) { this->mouseEvent = mouseEvent; }
		inline void setOnMouseMoved(std::function<void(float, float)> mouseMoved) { this->mouseMoved = mouseMoved; }
		inline void setOnDroppedFiles(std::function<void(int, const char**)> filesDropped) { this->filesDropped = filesDropped; }
		inline void setOnTypedChar(std::function<void(unsigned int, int)> charTyped) { this->charTyped = charTyped; }
	private:
		std::function<void(int, int)> keyEvent;
		std::function<void(int, int)> mouseEvent;
		std::function<void(float, float)> mouseMoved;
		std::function<void(int, const char**)> filesDropped;
		std::function<void(unsigned int, int)> charTyped;

		InputProcessor() {
			this->keyEvent = [](int, int) {};
			this->mouseEvent = [](int, int) {};
			this->mouseMoved = [](float, float) {};
			this->filesDropped = [](int, const char**) {};
			this->charTyped = [](unsigned int, int) {};
		}
	};
}

#endif