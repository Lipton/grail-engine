#include "Intersector.h"

#include "Mesh.h"

bool grail::Intersector::intersectMeshRay(glm::vec3 rayOrigin, glm::vec3 rayVector, Mesh * mesh, glm::mat4 meshTransform, glm::vec3& intersection) {
	const float EPSILON = 0.0000001;

	glm::vec3 closestIntersection = glm::vec3();
	int inter = false;


	for (int i = 0; i < mesh->indicesCount; i += 3) {
		uint32_t i0 = mesh->indices[i];
		uint32_t i1 = mesh->indices[i + 1];
		uint32_t i2 = mesh->indices[i + 2];

		glm::vec3 vertex0 = meshTransform * glm::vec4(mesh->vertices[((i0) * 12) + 0], mesh->vertices[((i0) * 12) + 1], mesh->vertices[((i0) * 12) + 2], 1.0);
		glm::vec3 vertex1 = meshTransform * glm::vec4(mesh->vertices[((i1) * 12) + 0], mesh->vertices[((i1) * 12) + 1], mesh->vertices[((i1) * 12) + 2], 1.0);
		glm::vec3 vertex2 = meshTransform * glm::vec4(mesh->vertices[((i2) * 12) + 0], mesh->vertices[((i2) * 12) + 1], mesh->vertices[((i2) * 12) + 2], 1.0);

		glm::vec3 edge1, edge2, h, s, q;
		float a, f, u, v;
		edge1 = vertex1 - vertex0;
		edge2 = vertex2 - vertex0;
		h = glm::cross(rayVector, edge2);
		a = glm::dot(edge1, h);
		if (a > -EPSILON && a < EPSILON)
			continue;    // This ray is parallel to this triangle.
		f = 1.0 / a;
		s = rayOrigin - vertex0;
		u = f * (glm::dot(s, h));
		if (u < 0.0 || u > 1.0)
			continue;
		q = glm::cross(s, edge1);
		v = f * glm::dot(rayVector, q);
		if (v < 0.0 || u + v > 1.0)
			continue;
		// At this stage we can compute t to find out where the intersection point is on the line.
		float t = f * glm::dot(edge2, q);
		if (t > EPSILON) // ray intersection
		{
			if (inter == false) {
				closestIntersection = rayOrigin + rayVector * t;
				inter = true;

				continue;
			}
			else {
				if (glm::distance(rayOrigin, closestIntersection) > glm::distance(rayOrigin, rayOrigin + rayVector * t)) {
					closestIntersection = rayOrigin + rayVector * t;

					continue;
				}
			}
			continue;
		}
		else // This means that there is a line intersection but not a ray intersection.
			continue;
	}

	if (inter == true) {
		intersection = closestIntersection;
		return true;
	}

	return false;
}
