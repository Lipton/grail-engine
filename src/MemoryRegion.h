#ifndef GRAIL_MEMORY_REGION_H
#define GRAIL_MEMORY_REGION_H

#include "Common.h"

namespace grail {
	/*class MemoryRegion;
	struct MemoryBlock;

	struct MemoryRegionDescriptor {
		// Chunk size
		VkDeviceSize size;

		//Full size
		VkDeviceSize alignedSize;

		//Offset in global chunk (base pointer)
		VkDeviceSize offset;

		//Alignment requirment
		VkDeviceSize alignment;

		//Memory chunk allocated from
		MemoryRegion* region;

		//Memory block allocated on
		MemoryBlock* block;
	};

	struct MemoryBlock {
		//Memory Handle
		VkDeviceMemory memoryHandle;

		//Size of the allocated block
		VkDeviceSize size;

		//Current offset in the block
		VkDeviceSize offset;

		//Region the block is allocated on
		MemoryRegion* region;

		//Memory block flags
		VkFlags flags;

		//Memory type
		uint32_t memoryTypeIndex;

		//Is Memory Mapped
		bool mapped;

		std::vector<MemoryRegionDescriptor> allocations;

		VkDeviceSize getRemainingMemory();

		void* mapMemory();
		void* mapMemory(MemoryRegionDescriptor& range);
		void unmapMemory();
	};

	class MemoryRegion {
	public:
		MemoryRegion(GPU gpu, const std::string& identifier, uint32_t blockSize = 257);

		//bool hasFlag(VkMemoryPropertyFlagBits flag);

		MemoryRegionDescriptor allocateRegion(VkDeviceSize size, VkDeviceSize alignment, VkMemoryPropertyFlags flags, uint32_t memoryBits);

		VkDeviceSize getTotalUsedSpace();
		VkDeviceSize getTotalSpace();

		std::string& getName();

		std::vector<MemoryBlock*>& getBlocks();

		void clearBlocks();

		GPU getGPU();

		void dispose();
	private:
		std::string identifier;
		std::vector<MemoryBlock*> blocks;

		MemoryBlock* allocateNewBlock(VkMemoryPropertyFlags flags, uint32_t memoryBits, uint32_t blockSize);

		GPU gpu;

		uint32_t blockSize;
	};*/
}

#endif