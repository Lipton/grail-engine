#ifndef GRAIL_PIPELINE_FACTORY_H
#define GRAIL_PIPELINE_FACTORY_H

#include "Common.h"
#include "Utils.h"

namespace grail {
	struct InputAssemplyState {
		PrimitiveTopology topology = PrimitiveTopology::TRIANGLE_LIST;
		VkBool32 primitiveRestartEnable = false;

		size_t toHash() {
			size_t hash = 0;

			utils::hash::combineHash(hash, (uint32_t)topology);
			utils::hash::combineHash(hash, primitiveRestartEnable);

			return hash;
		}
	};

	struct RasterizationState {
		VkBool32 depthClampEnable = false;
		VkBool32 rasterizerDiscardEnable = false;

		PolygonMode polygonMode = PolygonMode::FILL;
		CullMode cullMode = CullMode::BACK;
		FrontFace frontFace = FrontFace::CLOCKWISE;

		VkBool32 depthBiasEnable = false;

		float depthBiasConstantFactor = 0.0f;
		float depthBiasClamp = 0.0f;
		float depthBiasSlopeFactor = 0.0f;
		float lineWidth = 1.0f;

		size_t toHash() {
			size_t hash = 0;

			utils::hash::combineHash(hash, depthClampEnable);
			utils::hash::combineHash(hash, rasterizerDiscardEnable);
			utils::hash::combineHash(hash, (uint32_t)polygonMode);
			utils::hash::combineHash(hash, (uint32_t)cullMode);
			utils::hash::combineHash(hash, (uint32_t)frontFace);
			utils::hash::combineHash(hash, depthBiasEnable);
			utils::hash::combineHash(hash, depthBiasConstantFactor);
			utils::hash::combineHash(hash, depthBiasClamp);
			utils::hash::combineHash(hash, depthBiasSlopeFactor);
			utils::hash::combineHash(hash, lineWidth);

			return hash;
		}
	};

	struct ColorBlendState {
		uint32_t attachmentCount = 1;

		VkBool32 blendEnable = false;

		BlendFactor srcColorBlendFactor = BlendFactor::ONE;
		BlendFactor dstColorBlendFactor = BlendFactor::ONE;
		BlendOp colorBlendOp = BlendOp::ADD;

		BlendFactor srcAlphaBlendFactor = BlendFactor::ONE;
		BlendFactor dstAlphaBlendFactor = BlendFactor::ONE;
		BlendOp alphaBlendOp = BlendOp::ADD;

		ColorComponentFlags colorWriteMask = ColorComponentFlags::RGBA;

		size_t toHash() {
			size_t hash = 0;

			utils::hash::combineHash(hash, attachmentCount);
			utils::hash::combineHash(hash, blendEnable);
			utils::hash::combineHash(hash, (uint32_t)srcColorBlendFactor);
			utils::hash::combineHash(hash, (uint32_t)dstColorBlendFactor);
			utils::hash::combineHash(hash, (uint32_t)colorBlendOp);
			utils::hash::combineHash(hash, (uint32_t)srcAlphaBlendFactor);
			utils::hash::combineHash(hash, (uint32_t)dstAlphaBlendFactor);
			utils::hash::combineHash(hash, (uint32_t)alphaBlendOp);
			utils::hash::combineHash(hash, (uint32_t)colorWriteMask);

			return hash;
		}
	};

	struct DynamicState {
		bool viewport = true;
		bool scissor = true;
		bool lineWidth = false;
		bool depthBias = false;
		bool blendConstants = false;
		bool depthBounds = false;
		bool stencilCompareMask = false;
		bool stencilWriteMask = false;
		bool stencilReference = false;

		size_t toHash() {
			size_t hash = 0;

			utils::hash::combineHash(hash, viewport);
			utils::hash::combineHash(hash, scissor);
			utils::hash::combineHash(hash, lineWidth);
			utils::hash::combineHash(hash, depthBias);
			utils::hash::combineHash(hash, blendConstants);
			utils::hash::combineHash(hash, depthBounds);
			utils::hash::combineHash(hash, stencilCompareMask);
			utils::hash::combineHash(hash, stencilWriteMask);
			utils::hash::combineHash(hash, stencilReference);

			return hash;
		}
	};

	struct StencilOpState {
		StencilOp failOp = StencilOp::KEEP;
		StencilOp passOp = StencilOp::KEEP;
		StencilOp depthFailOp = StencilOp::KEEP;

		CompareOp compareOp = CompareOp::ALWAYS;

		uint32_t compareMask;
		uint32_t writeMask;
		uint32_t reference;

		size_t toHash() {
			size_t hash = 0;

			utils::hash::combineHash(hash, (uint32_t)failOp);
			utils::hash::combineHash(hash, (uint32_t)passOp);
			utils::hash::combineHash(hash, (uint32_t)depthFailOp);
			utils::hash::combineHash(hash, (uint32_t)compareOp);
			utils::hash::combineHash(hash, compareMask);
			utils::hash::combineHash(hash, writeMask);
			utils::hash::combineHash(hash, reference);

			return hash;
		}
	};

	struct DepthStencilState {
		VkBool32 depthTestEnable = true;
		VkBool32 depthWriteEnable = true;
		CompareOp depthCompareOp = CompareOp::LESS_OR_EQUAL;

		VkBool32 depthBoundsTestEnable = false;

		VkBool32 stencilTestEnable = false;

		StencilOpState front = {};
		StencilOpState back = {};

		float minDepthBounds = 0.0f;
		float maxDepthBounds = 1.0f;

		size_t toHash() {
			size_t hash = 0;

			utils::hash::combineHash(hash, depthTestEnable);
			utils::hash::combineHash(hash, depthWriteEnable);
			utils::hash::combineHash(hash, (uint32_t)depthCompareOp);
			utils::hash::combineHash(hash, depthBoundsTestEnable);
			utils::hash::combineHash(hash, stencilTestEnable);
			utils::hash::combineHash(hash, front.toHash());
			utils::hash::combineHash(hash, back.toHash());
			utils::hash::combineHash(hash, minDepthBounds);
			utils::hash::combineHash(hash, maxDepthBounds);

			return hash;
		}
	};

	struct MultisampleState {
		SampleCountBits rasterizationSamples = SampleCountBits::COUNT_1_BIT;

		VkBool32 sampleShadingEnable = false;

		float minSampleShading = 0.0f;

		const VkSampleMask* pSampleMask;

		VkBool32 alphaToCoverageEnable = false;
		VkBool32 alphaToOneEnable = false;

		size_t toHash() {
			size_t hash = 0;

			utils::hash::combineHash(hash, (uint32_t)rasterizationSamples);
			utils::hash::combineHash(hash, sampleShadingEnable);
			utils::hash::combineHash(hash, minSampleShading);
			utils::hash::combineHash(hash, pSampleMask);
			utils::hash::combineHash(hash, alphaToCoverageEnable);
			utils::hash::combineHash(hash, alphaToOneEnable);

			return hash;
		}
	};

	struct ShaderStage {
		ShaderStageBits stageFlags = ShaderStageBits::ALL;

		VkShaderModule module = VK_NULL_HANDLE;

		const char* pName = "main";
	};

	struct ShaderStageState {
		friend class PipelineFactory;
	public:
		ShaderStageState() {
			stages.resize(6);

			{
				ShaderStage stage = ShaderStage();
				stage.stageFlags = ShaderStageBits::VERTEX;
				stage.module = VK_NULL_HANDLE;
				stage.pName = "main";
				stages[mappings.at(static_cast<int>(VK_SHADER_STAGE_VERTEX_BIT))] = stage;
			}

			{
				ShaderStage stage = ShaderStage();
				stage.stageFlags = ShaderStageBits::TESSELLATION_CONTROL;
				stage.module = VK_NULL_HANDLE;
				stage.pName = "main";
				stages[mappings.at(static_cast<int>(VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT))] = stage;
			}

			{
				ShaderStage stage = ShaderStage();
				stage.stageFlags = ShaderStageBits::TESSELLATION_EVALUATION;
				stage.module = VK_NULL_HANDLE;
				stage.pName = "main";
				stages[mappings.at(static_cast<int>(VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT))] = stage;
			}

			{
				ShaderStage stage = ShaderStage();
				stage.stageFlags = ShaderStageBits::GEOMETRY;
				stage.module = VK_NULL_HANDLE;
				stage.pName = "main";
				stages[mappings.at(static_cast<int>(VK_SHADER_STAGE_GEOMETRY_BIT))] = stage;
			}

			{
				ShaderStage stage = ShaderStage();
				stage.stageFlags = ShaderStageBits::FRAGMENT;
				stage.module = VK_NULL_HANDLE;
				stage.pName = "main";
				stages[mappings.at(static_cast<int>(VK_SHADER_STAGE_FRAGMENT_BIT))] = stage;
			}

			{
				ShaderStage stage = ShaderStage();
				stage.stageFlags = ShaderStageBits::COMPUTE;
				stage.module = VK_NULL_HANDLE;
				stage.pName = "main";
				stages[mappings.at(static_cast<int>(VK_SHADER_STAGE_COMPUTE_BIT))] = stage;
			}
		}

		~ShaderStageState() {}

		void set(ShaderStageBits shaderStage, VkShaderModule* module) {
			stages[mappings.at(static_cast<int>(shaderStage))].module = *module;
		}

		ShaderStage& get(ShaderStageBits shaderStage) {
			return stages[mappings.at(static_cast<int>(shaderStage))];
		}

		size_t toHash() {
			size_t hash = 0;

			for (uint32_t i = 0; i < 6; i++) {
				if (stages.at(i).module != VK_NULL_HANDLE) {
					utils::hash::combineHash(hash, stages[i].module);
				}
			}

			return hash;
		}
	private:
		std::vector<ShaderStage> stages;

		const std::map<int, int> mappings = {
			{ VK_SHADER_STAGE_VERTEX_BIT, 0 },
			{ VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT, 1 },
			{ VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT, 2 },
			{ VK_SHADER_STAGE_GEOMETRY_BIT, 3 },
			{ VK_SHADER_STAGE_FRAGMENT_BIT, 4 },
			{ VK_SHADER_STAGE_COMPUTE_BIT, 5 }
		};
	};

	class VertexLayout;

	struct GraphicsPipelineCreateInfo {
		InputAssemplyState assemblyState = {};
		RasterizationState rasterizationState = {};
		ColorBlendState colorBlendState = {};
		DynamicState dynamicState = {};
		DepthStencilState depthStencilState = {};
		MultisampleState multisampleState = {};
		ShaderStageState shaderStageState = {};

		VkBool32 allowDerivatives = false;

		VkRenderPass renderPass;
		VkPipelineLayout pipelineLayout;

		VertexLayout* vertexLayout = nullptr;

		GPU gpu = VulkanContext::mainGPU;

		size_t toHash() {
			size_t hash = 0;

			utils::hash::combineHash(hash, assemblyState.toHash());
			utils::hash::combineHash(hash, rasterizationState.toHash());
			utils::hash::combineHash(hash, colorBlendState.toHash());
			utils::hash::combineHash(hash, dynamicState.toHash());
			utils::hash::combineHash(hash, depthStencilState.toHash());
			utils::hash::combineHash(hash, multisampleState.toHash());
			utils::hash::combineHash(hash, shaderStageState.toHash());
			utils::hash::combineHash(hash, allowDerivatives);
			utils::hash::combineHash(hash, renderPass);
			utils::hash::combineHash(hash, pipelineLayout);
			utils::hash::combineHash(hash, vertexLayout);
			utils::hash::combineHash(hash, gpu.device);

			return hash;
		}
	};

	struct ComputePipelineCreateInfo {
		ShaderStageState shaderStageState = {};

		VkBool32 allowDerivatives = false;

		VkPipelineLayout pipelineLayout;

		GPU gpu = VulkanContext::mainGPU;
	};

	class PipelineFactory {
	public:
		VkPipeline createPipeline(GraphicsPipelineCreateInfo& info, VkPipeline basePipeline = VK_NULL_HANDLE);
		VkPipeline createComputePipeline(GraphicsPipelineCreateInfo& info, VkPipeline basePipeline = VK_NULL_HANDLE);
	private:
		VkPipelineCache cache;	
		VkPipeline derivative = VK_NULL_HANDLE;
	};
}

#endif