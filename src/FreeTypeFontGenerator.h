#ifndef GRAIL_FREE_TYPE_FONT_GENERATOR_H
#define GRAIL_FREE_TYPE_FONT_GENERATOR_H

#include "Common.h"


#include <ft2build.h>
#include FT_FREETYPE_H

//#include "Texture2D.h"

namespace grail {
	struct Glyph {
		char c;

		uint32_t x;
		uint32_t y;

		uint32_t width;
		uint32_t height;
	};
}

#endif