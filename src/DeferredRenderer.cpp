#include "DeferredRenderer.h"
#include "Resources.h"
#include "FrameGraph.h"
#include "Mesh.h"
#include "Material.h"
#include "Scene.h"
#include "Camera.h"
#include "PerspectiveCamera.h"
#include "VR.h"

grail::PerspectiveCamera* grail::DeferredRenderer::camera = nullptr;
grail::DeferredRenderer::RendererSettings grail::DeferredRenderer::settings = {};

grail::DeferredRenderer::DeferredRenderer(PerspectiveCamera* camera) {
	setCamera(camera);

	createDefaultAssets();
	createBuffers();

	generateBRDFLookupTexture();
	generateIrradianceMap();
	generateSpecularMap();

	initializeFrameGraphs();
}

void grail::DeferredRenderer::setCamera(PerspectiveCamera* camera) {
	this->camera = camera;
}

grail::PerspectiveCamera* grail::DeferredRenderer::getCamera() {
	return DeferredRenderer::camera;
}

grail::DeferredRenderer::RendererSettings& grail::DeferredRenderer::getSettings() {
	return settings;
}

void grail::DeferredRenderer::setOutputViewport(Viewport viewport) {
	this->viewport = viewport;
}

void grail::DeferredRenderer::resetOutputViewport() {
	setOutputViewport({ 0, 0, (float)Graphics::getWidth(), (float)Graphics::getHeight(), 0.0f, 1.0f });
}

void grail::DeferredRenderer::render(float delta) {
	//camera->update(true);

	time += delta;

	cameraInfo.projection = camera->projection;
	cameraInfo.inverseProjection = glm::inverse(camera->projection);
	cameraInfo.view = camera->view;
	cameraInfo.inverseView = glm::inverse(camera->view);
	cameraInfo.combined = camera->combined;
	cameraInfo.inverseCombined = glm::inverse(camera->combined);
	cameraInfo.position = glm::vec4(camera->position, 1.0);
	cameraInfo.viewDirection = glm::vec4(camera->direction, 1.0);
	cameraInfo.viewport = glm::vec4(camera->viewportWidth, camera->viewportHeight, camera->nearPlane, camera->farPlane);
	cameraInfo.time.x = time;
	cameraInfo.time.y = lastFrameTime;

	RenderPass* taaPass = frameGraph->getPass("TXAA");

	if (taaPass && settings.TAAEnabled) {
		int subsampleIndex = frameIndex % 16;
		const glm::vec2 texSize(
			1.0f / (float)taaPass->getWidth(),
			1.0f / (float)taaPass->getHeight());

		const glm::vec2 ndcSize = texSize * 2.0f;
		const glm::vec2 sample = taaSampleMap[subsampleIndex];

		cameraInfo.subsample = (sample * ndcSize) * 0.5f;
		cameraInfo.subsample *= settings.jitterIntensity;
	}
	else {
		cameraInfo.subsample = glm::vec2(0, 0);
	}

	updateShadowCascades();
	cameraInfoBuffer->updateData(&cameraInfo, sizeof(CameraInfo));
	cameraInfoBuffer->flush();

	frameGraph->buildFrame(grail::VulkanContext::swapchain);
	frameGraph->submitFrame(grail::VulkanContext::swapchain);

#ifdef OS_NOT_SUPPORTED
	if (VR::isInitialized()) {
		//VK_VALIDATE_RESULT(vkQueueWaitIdle(VulkanContext::mainGPU.graphicsQueue.handle));

		//fxaa_result.getTexture()->transitionImage(cmd, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);

		vr::VRTextureBounds_t bounds;
		bounds.uMin = 0.0f;
		bounds.uMax = 1.0f;
		bounds.vMin = 1.0f;
		bounds.vMax = 0.0f;

		vr::VRVulkanTextureData_t vulkanData;
		vulkanData.m_nImage = (uint64_t)fxaaPassOuput->getImage();
		vulkanData.m_pDevice = (VkDevice_T*)VulkanContext::mainGPU.device;
		vulkanData.m_pPhysicalDevice = (VkPhysicalDevice_T*)VulkanContext::mainGPU.physicalDevice;
		vulkanData.m_pInstance = (VkInstance_T*)VulkanContext::instance;
		vulkanData.m_pQueue = (VkQueue_T*)VulkanContext::mainGPU.graphicsQueue.handle;
		vulkanData.m_nQueueFamilyIndex = VulkanContext::mainGPU.graphicsQueue.index;
		//2368 2628
		vulkanData.m_nWidth = 2368;
		vulkanData.m_nHeight = 2628;
		vulkanData.m_nFormat = VK_FORMAT_R8G8B8A8_SRGB;
		vulkanData.m_nSampleCount = 1;

		vr::Texture_t texture = { &vulkanData, vr::TextureType_Vulkan, vr::ColorSpace_Auto };
		vr::VRCompositorError e = vr::VRCompositorError::VRCompositorError_None;

		e = vr::VRCompositor()->Submit(vr::Eye_Left, &texture, &bounds);

		if (e != vr::VRCompositorError::VRCompositorError_None) {
			GRAIL_LOG(ERROR, "COMPOSITOR ERROR LEFT") << (uint32_t)e;
		}

		e = vr::VRCompositor()->Submit(vr::Eye_Right, &texture, &bounds);

		if (e != vr::VRCompositorError::VRCompositorError_None) {
			GRAIL_LOG(ERROR, "COMPOSITOR ERROR RIGHT") << (uint32_t)e;
		}

		//vulkanData.m_nImage = (uint64_t)m_rightEyeDesc.m_pImage;
		//vr::VRCompositor()->Submit(vr::Eye_Right, &texture, &bounds);
	
		eyeIndex = (eyeIndex + 1) % 2;
	}
#endif

	cameraInfo.lastCombined = camera->combined;
	lastFrameTime = time;
	lastCameraDirection = camera->direction;
	frameIndex += 1;
}

void grail::DeferredRenderer::setPostRenderFunction(std::function<void(RenderPass*, VkCommandBuffer&)> fun) {
	this->postRender = fun;
}

grail::FrameGraph* grail::DeferredRenderer::getFrameGraph() {
	return frameGraph;
}

void grail::DeferredRenderer::createDefaultAssets() {
	Resources::loadModel("models/sphere.obj");

	TextureCreateInfo attachmentInfo = {};
	attachmentInfo.generateMipmaps = true;
	attachmentInfo.samplerInfo.addressModeU = SamplerAddressMode::REPEAT;
	attachmentInfo.samplerInfo.addressModeV = SamplerAddressMode::REPEAT;
	attachmentInfo.samplerInfo.magFilter = SamplerFilter::LINEAR;
	attachmentInfo.samplerInfo.minFilter = SamplerFilter::LINEAR;
	attachmentInfo.format = VK_FORMAT_R8G8B8A8_UNORM;

	{
		unsigned char data[] = { 255, 255, 255, 255 };
		Resources::createTexture("white", 1, 1, &data[0], 4, attachmentInfo);
	}

	{
		unsigned char data[] = { 0, 0, 0, 255 };
		Resources::createTexture("black", 1, 1, &data[0], 4, attachmentInfo);
	}

	{
		unsigned char data[] = { 127, 127, 127, 255 };
		Resources::createTexture("gray", 1, 1, &data[0], 4, attachmentInfo);
	}

	{
		unsigned char data[] = { 127, 127, 255, 255 };
		Resources::createTexture("bump", 1, 1, &data[0], 4, attachmentInfo);
	}

	{
		unsigned char data[] = { 255, 0, 0, 255 };
		Resources::createTexture("red", 1, 1, &data[0], 4, attachmentInfo);
	}

	std::vector<float> vertices = { -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, -1.0f, 1.0f, 0.0f, 1.0f };
	std::vector<uint32_t> indices = { 0, 1, 2, 2, 3, 0 };
	fullscreenMesh = Resources::createMesh("fullscreen_mesh",
		nullptr, vertices, indices
	);

	vertices = {
		-1.0f, -1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, // Bottom-left
		1.0f, 1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f, // top-right
		1.0f, -1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f, // bottom-right
		1.0f, 1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f,  // top-right
		-1.0f, -1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f,  // bottom-left
		-1.0f, 1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f,// top-left
		// Front face
		-1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, // bottom-left
		1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,  // bottom-right
		1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,  // top-right
		1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, // top-right
		-1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,  // top-left
		-1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,  // bottom-left
		// Left face
		-1.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f, // top-right
		-1.0f, 1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f, // top-left
		-1.0f, -1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f,  // bottom-left
		-1.0f, -1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f, // bottom-left
		-1.0f, -1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f,  // bottom-right
		-1.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f, // top-right
		// Right face
		1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, // top-left
		1.0f, -1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, // bottom-right
		1.0f, 1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, // top-right
		1.0f, -1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f,  // bottom-right
		1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,  // top-left
		1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, // bottom-left
		// Bottom face
		-1.0f, -1.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f, // top-right
		1.0f, -1.0f, -1.0f, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f, // top-left
		1.0f, -1.0f, 1.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f,// bottom-left
		1.0f, -1.0f, 1.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f, // bottom-left
		-1.0f, -1.0f, 1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, // bottom-right
		-1.0f, -1.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f, // top-right
		// Top face
		-1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,// top-left
		1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, // bottom-right
		1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, // top-right
		1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, // bottom-right
		-1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,// top-left
		-1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f // bottom-left
	};

	indices = {
		0, 1, 3, 3, 1, 2,
		1, 5, 2, 2, 5, 6,
		5, 4, 6, 6, 4, 7,
		4, 0, 7, 7, 0, 3,
		3, 2, 7, 7, 2, 6,
		4, 5, 0, 0, 5, 1 
	};

	Mesh* cubeMesh = Resources::createMesh("cubeMesh",
		nullptr, vertices, indices
	);
}

void grail::DeferredRenderer::createBuffers() {
	BufferCreateInfo bufferInfo = {};
	bufferInfo.usageBits = BufferUsageBits::UNIFORM_BUFFER_BIT;
	bufferInfo.memoryProperties = MemoryPropertyFlags::HOST_VISIBLE | MemoryPropertyFlags::HOST_COHERENT;
	bufferInfo.size = sizeof(CameraInfo);
	bufferInfo.memoryType = MemoryType::DYNAMIC;
	cameraInfoBuffer = Resources::createBuffer("camera", bufferInfo, Resources::getUniformBufferMemoryAllocator());
	cameraInfoBuffer->map();
}

void grail::DeferredRenderer::initializeFrameGraphs() {
	uint32_t w = Graphics::getWidth();
	uint32_t h = Graphics::getHeight();

	setOutputViewport({ 0, 0, (float)w, (float)h, 0.0f, 1.0f });

	frameGraph = new FrameGraph(VulkanContext::mainGPU);

	AttachmentInfo attachmentInfo = {};
#ifdef OS_ANDROID
	attachmentInfo.width = w * 0.5f;
	attachmentInfo.height = h * 0.5f;
#else
	attachmentInfo.width = w;
	attachmentInfo.height = h;
#endif

	attachmentInfo.format = VK_FORMAT_R16G16B16A16_SFLOAT;
	RenderAttachment& p_light = frameGraph->createAttachment("graph_light", attachmentInfo);

	RenderAttachment& taa_result = frameGraph->createAttachment("graph_TAA", attachmentInfo);

	attachmentInfo.initialLayout = ImageLayout::SHADER_READ_ONLY_OPTIMAL;
	RenderAttachment& taa_history = frameGraph->createAttachment("graph_TAA_History", attachmentInfo);

	attachmentInfo.initialLayout = ImageLayout::UNDEFINED;
	attachmentInfo.format = VK_FORMAT_R8G8B8A8_UNORM;
	RenderAttachment& tonemapped_result = frameGraph->createAttachment("graph_Tonemapped", attachmentInfo);
	RenderAttachment& fxaa_result = frameGraph->createAttachment("graph_FXAA", attachmentInfo);
	RenderAttachment& g_color = frameGraph->createAttachment("graph_G_Color", attachmentInfo);

	attachmentInfo.format = VK_FORMAT_R16G16B16A16_SFLOAT;
	RenderAttachment& g_normal = frameGraph->createAttachment("graph_G_Normal", attachmentInfo);

	attachmentInfo.format = VK_FORMAT_R8G8B8A8_UNORM;
	RenderAttachment& g_properties = frameGraph->createAttachment("graph_G_Properties", attachmentInfo);

	attachmentInfo.format = VK_FORMAT_R16G16_SFLOAT;
	RenderAttachment& g_vel = frameGraph->createAttachment("graph_G_Velocity", attachmentInfo);

	attachmentInfo.format = VK_FORMAT_D32_SFLOAT;
	attachmentInfo.magFilter = SamplerFilter::NEAREST;
	attachmentInfo.minFilter = SamplerFilter::NEAREST;
	RenderAttachment& g_depth = frameGraph->createAttachment("graph_G_Depth", attachmentInfo);

	attachmentInfo.format = VK_FORMAT_R16_SFLOAT;
	attachmentInfo.mipmapped = true;
	RenderAttachment& g_depthMip = frameGraph->createAttachment("graph_G_Depth_Mip", attachmentInfo);
	attachmentInfo.mipmapped = false;

	attachmentInfo.width = w * 0.5f;
	attachmentInfo.height = h * 0.5f;
	attachmentInfo.format = VK_FORMAT_R16G16_SFLOAT;
	RenderAttachment& ssao_result = frameGraph->createAttachment("graph_GTAO", attachmentInfo);
	attachmentInfo.format = VK_FORMAT_R8_UNORM;
	RenderAttachment& ssao_spacial = frameGraph->createAttachment("graph_GTAO_Spacial", attachmentInfo);
	attachmentInfo.format = VK_FORMAT_R8G8_UNORM;
	RenderAttachment& ssao_spacialHistory = frameGraph->createAttachment("graph_GTAO_Temporal_History", attachmentInfo);
	RenderAttachment& ssao_temporal = frameGraph->createAttachment("graph_GTAO_Temporal", attachmentInfo);

	attachmentInfo.format = VK_FORMAT_D16_UNORM;
	attachmentInfo.magFilter = SamplerFilter::LINEAR;
	attachmentInfo.minFilter = SamplerFilter::LINEAR;
	attachmentInfo.samplerCompareEnable = true;
	attachmentInfo.samplerCompareOP = CompareOp::LESS;
	attachmentInfo.layers = shadowMapCascadeSplits;
	attachmentInfo.width = shadowCascadeMapSize;
	attachmentInfo.height = shadowCascadeMapSize;
	RenderAttachment& cascade_result = frameGraph->createAttachment("graph_Shadow_Cascades", attachmentInfo);

	RenderPass& depthPyramidPass = frameGraph->createPass("Depth Downsample", FrameGraphQueueBits::GRAPHICS);

	depthPyramidPass.createMaterialFromShaders("shaders/graph/bare.vert", "shaders/graph/depth_blit.frag");
	depthPyramidPass.addInputAttachment(g_depth);
	depthPyramidPass.addOutputAttachment(g_depthMip);
	depthPyramidPass.setOnRender([&](VkCommandBuffer& cmd) {
		// Blit the depth buffer to the mip texture

		MaterialInstance* material = depthPyramidPass.getDefaultMaterial();
		Mesh* mesh = fullscreenMesh;

		struct Push {
			glm::vec2 cameraMinMax;
		} push;

		push.cameraMinMax = glm::vec2(camera->nearPlane, camera->farPlane);

		depthPyramidPass.beginRenderPass(cmd, 0, g_depthMip.getTexture()->getWidth(), g_depthMip.getTexture()->getHeight());
		depthPyramidPass.bindMaterial(cmd, material);
		depthPyramidPass.setPushConstants(cmd, material, &push);
		depthPyramidPass.drawMesh(cmd, mesh);
		depthPyramidPass.endRenderPass(cmd);

		VkImageSubresourceRange range;
		range.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		range.baseArrayLayer = 0;
		range.layerCount = 1;
		range.levelCount = g_depthMip.getTexture()->getMipCount() - 1;
		range.baseMipLevel = 1;
		g_depthMip.getTexture()->setImageLayout(cmd, VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, range);

		// Compute lower depth levels
		for (uint32_t mip = 1; mip < g_depthMip.getTexture()->getMipCount(); mip++) {
			int32_t width = g_depthMip.getTexture()->getWidth() >> mip;
			int32_t height = g_depthMip.getTexture()->getHeight() >> mip;

			if (width < 1) width = 1;
			if (height < 1) height = 1;

			{
				VkImageSubresourceRange range;
				range.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
				range.baseArrayLayer = 0;
				range.layerCount = 1;
				range.levelCount = 1;
				range.baseMipLevel = mip;

				// Transition the dst image to color attacment optimal
				range.baseMipLevel = mip;
				g_depthMip.getTexture()->setImageLayout(cmd, VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, range);
			}

			depthPyramidPass.beginRenderPass(cmd, mip,
				width,
				height
			);

			VkViewport viewport = {};
			viewport.minDepth = 0.0f;
			viewport.maxDepth = 1.0f;
			viewport.x = 0;
			viewport.y = 0;
			viewport.width = width;
			viewport.height = height;

			VkRect2D scissor = {};
			scissor.extent.width = width;
			scissor.extent.height = height;
			scissor.offset.x = 0;
			scissor.offset.y = 0;

			vkCmdSetViewport(cmd, 0, 1, &viewport);
			vkCmdSetScissor(cmd, 0, 1, &scissor);

			vkCmdBindPipeline(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, depthMipMaterialInstance->getPipeline());
			vkCmdBindDescriptorSets(
				cmd,
				VK_PIPELINE_BIND_POINT_GRAPHICS,
				depthMipDescriptors[mip - 1].getPipelineLayout(),
				0,
				depthMipDescriptors[mip - 1].getDescriptorCount(),
				depthMipDescriptors[mip - 1].getHandlePtr(),
				0,
				nullptr
			);

			struct {
				glm::vec2 texelSize;
				glm::vec2 size;
			} push;
			push.texelSize = glm::vec2(1.0f / (float)width, 1.0f / (float)height);
			push.size = glm::vec2(width, height);

			vkCmdPushConstants(cmd, depthMipDescriptors[mip - 1].getPipelineLayout(),
				static_cast<VkShaderStageFlags>(depthMipDescriptors[mip - 1].getPushConstant(0).stageFlags),
				depthMipDescriptors[mip - 1].getPushConstant(0).offset,
				depthMipDescriptors[mip - 1].getPushConstant(0).size,
				&push);

			depthPyramidPass.drawMesh(cmd, mesh);

			depthPyramidPass.endRenderPass(cmd);
		}
		});

	RenderPass& taa = frameGraph->createPass("TXAA", FrameGraphQueueBits::GRAPHICS);
	taa.createMaterialFromShaders("shaders/graph/bare.vert", "shaders/graph/txaa.frag");
	taa.addInputAttachment(p_light);
	taa.addInputAttachment(taa_history);
	taa.addInputAttachment(g_vel);
	taa.addInputAttachment(g_depth);
	taa.addOutputAttachment(taa_result);
	taa.addHistoryAttachment(taa_result, taa_history);

	taa.setOnRender([&](VkCommandBuffer& cmd) {
		MaterialInstance* material = taa.getDefaultMaterial();
		Mesh* mesh = fullscreenMesh;

		struct Push {
			glm::vec4 texelSize;
			glm::vec4 subsample;
			glm::mat4 projection;
			glm::vec2 taaEnabled;
		} push;

		push.texelSize = glm::vec4(1.0f / (float)taa.getWidth(), 1.0f / (float)taa.getHeight(), (float)taa.getWidth(), (float)taa.getHeight());
		push.subsample = glm::vec4(cameraInfo.subsample.x, cameraInfo.subsample.y, 0, 0);
		push.projection = camera->projection;
		push.taaEnabled = glm::vec2((settings.TAAEnabled ? 1.0f : 0.0), (settings.advancedTAA ? 1.0f : 0.0));

		taa.beginRenderPass(cmd);
		taa.bindMaterial(cmd, material);
		taa.setPushConstants(cmd, material, &push);
		taa.drawMesh(cmd, mesh);
		taa.endRenderPass(cmd);
	});

	RenderPass& tonemapPass = frameGraph->createPass("Tonemap", FrameGraphQueueBits::GRAPHICS);
	tonemapPass.createMaterialFromShaders("shaders/graph/bare.vert", "shaders/graph/tonemap.frag");
	tonemapPass.addInputAttachment(taa_result);
	tonemapPass.addInputAttachment(g_vel);
	tonemapPass.addInputAttachment(g_depth);
	tonemapPass.addOutputAttachment(tonemapped_result);
	tonemapPass.setOnRender([&](VkCommandBuffer& cmd) {
		MaterialInstance* material = tonemapPass.getDefaultMaterial();
		Mesh* mesh = fullscreenMesh;

		float ev100 = glm::log2((settings.cameraAperture * settings.cameraAperture) / settings.cameraShutterSpeed * 100.0f / settings.cameraSensitivity);
		float exposure = 1.0f / (glm::pow(2.0, ev100) * 1.2f);

		settings.computedExposure = exposure;

		glm::mat4 view = glm::lookAt(glm::vec3(0.0), camera->direction, camera->up);
		glm::mat4 lastView = glm::lookAt(glm::vec3(0.0), lastCameraDirection, camera->up);

		glm::vec4 a = view * glm::vec4(glm::vec3(camera->nearPlane), 1);
		glm::vec4 b = lastView * glm::vec4(glm::vec3(camera->nearPlane), 1);

		glm::vec2 va = glm::vec2(a.x / a.w, a.y / a.w) * glm::vec2(0.5) + glm::vec2(0.5);
		glm::vec2 vb = glm::vec2(b.x / b.w, b.y / b.w) * glm::vec2(0.5) + glm::vec2(0.5);

		glm::vec2 vel = (a - b);

		float data[] = {
			1.0f / Graphics::getWidth(), // Texel size X
			1.0f / Graphics::getHeight(), // Texel size Y

			(float)Graphics::getSmoothedFPS(), // FPS
			(float)Graphics::getSmoothedFPS() / 60.0f, // Time,

			settings.cameraAperture,
			settings.cameraShutterSpeed,
			settings.cameraSensitivity,
			exposure, // Computed exposure,

			vel.x,
			vel.y,

			(float)settings.motionBlurSamples,
			settings.useNewMotionBlur ? 1.0f : 0.0f
		};

		tonemapPass.beginRenderPass(cmd);
		tonemapPass.bindMaterial(cmd, material);
		tonemapPass.setPushConstants(cmd, material, &data[0]);
		tonemapPass.drawMesh(cmd, mesh);
		tonemapPass.endRenderPass(cmd);
	});

	RenderPass& present = frameGraph->createPass("Present", FrameGraphQueueBits::PRESENT);
	present.createMaterialFromShaders("shaders/graph/bare.vert", "shaders/graph/present.frag");
	present.addInputAttachment(fxaa_result);

	present.setOnRender([&](VkCommandBuffer& cmd) {
		MaterialInstance* material = present.getDefaultMaterial();
		Mesh* mesh = fullscreenMesh;

		glm::vec4 texelSize = glm::vec4(1.0f / (float)present.getWidth(), 1.0f / (float)present.getHeight(), (float)present.getWidth(), (float)present.getHeight());

		VkViewport port = {};
		port.minDepth = viewport.minDepth;
		port.maxDepth = viewport.maxDepth;
		port.x = viewport.x;
		port.y = viewport.y;
		port.width = viewport.width;
		port.height = viewport.height;

		vkCmdSetViewport(cmd, 0, 1, &port);

		present.beginRenderPass(cmd);
		present.bindMaterial(cmd, material);
		present.setPushConstants(cmd, material, &texelSize);
		present.drawMesh(cmd, mesh);

		if(postRender)
			postRender(&present, cmd);

		present.endRenderPass(cmd);
	});

	RenderPass& fxaa = frameGraph->createPass("FXAA", FrameGraphQueueBits::GRAPHICS);
	fxaa.createMaterialFromShaders("shaders/graph/fxaa.vert", "shaders/graph/fxaa.frag");
	fxaa.addOutputAttachment(fxaa_result);
	fxaa.addInputAttachment(tonemapped_result);

	fxaa.setOnRender([&](VkCommandBuffer& cmd) {
		MaterialInstance* material = fxaa.getDefaultMaterial();
		Mesh* mesh = fullscreenMesh;

		glm::vec2 texelSize(fxaa.getWidth(), fxaa.getHeight());

		fxaa.beginRenderPass(cmd);
		fxaa.bindMaterial(cmd, material);
		fxaa.setPushConstants(cmd, material, &texelSize);
		fxaa.drawMesh(cmd, mesh);
		fxaa.endRenderPass(cmd);
		});

	RenderPass& light = frameGraph->createPass("Light", FrameGraphQueueBits::GRAPHICS);
	light.createMaterialFromShaders("shaders/graph/light.vert", "shaders/graph/light.frag");
	light.addInputBuffer(cameraInfoBuffer);
	light.addInputAttachment(g_color);
	light.addInputAttachment(g_normal);
	light.addInputAttachment(g_properties);
	light.addInputAttachment(g_depth);
	light.addInputAttachment(cascade_result);
	light.addInputAttachment(ssao_temporal);
	//light.addInputAttachment(ssao_normals);
	light.addInputTexture(Resources::getTexture("IrradianceCube"));
	light.addInputTexture(Resources::getTexture("SpecularCube"));
	light.addInputTexture(Resources::getTexture("brdf_lut"));

	light.addOutputAttachment(p_light);


	RenderPass& ssaoPass = frameGraph->createPass("GTAO", FrameGraphQueueBits::GRAPHICS);
	ssaoPass.createMaterialFromShaders("shaders/graph/bare.vert", "shaders/graph/ssao.frag");
	ssaoPass.addInputAttachment(g_depthMip);
	ssaoPass.addInputAttachment(g_normal);
	ssaoPass.addInputBuffer(cameraInfoBuffer);
	ssaoPass.addOutputAttachment(ssao_result);
	//ssaoPass.addOutputAttachment(ssao_normals);

	ssaoPass.setOnRender([&](VkCommandBuffer& cmd) {
		MaterialInstance* material = ssaoPass.getDefaultMaterial();
		Mesh* mesh = fullscreenMesh;

		float rotations[] = { 60.0f, 300.0f, 180.0f, 240.0f, 120.0f, 0.0f };
		float rotation = rotations[frameIndex % 6] / 360.0f;

		float offsets[] = { 0.0f, 0.5f, 0.25f, 0.75f };
		float offset = offsets[(frameIndex / 6) % 4];


		struct {
			glm::vec4 texelSize;
			glm::vec2 params;
			glm::vec4 UVToView;
			glm::vec2 projScale;
		} push;

		push.texelSize = glm::vec4(1.0f / (float)ssaoPass.getWidth(), 1.0f / (float)ssaoPass.getHeight(),
			ssaoPass.getWidth(), ssaoPass.getHeight());
		push.params = glm::vec2(rotation, offset);

		float fovRad = glm::radians(camera->fov);
		float invHalfTanFov = 1.0f / glm::tan(fovRad * 0.5f);
		glm::vec2 focalLen = glm::vec2(invHalfTanFov * ((float)Graphics::getHeight() / (float)Graphics::getWidth()), invHalfTanFov);
		glm::vec2 invFocalLen = glm::vec2(1 / focalLen.x, 1 / focalLen.y);

		push.UVToView = glm::vec4(2 * invFocalLen.x, 2 * invFocalLen.y, -1 * invFocalLen.x, -1 * invFocalLen.y);

		float projScale;
		projScale = (float)Graphics::getHeight() / (glm::tan(fovRad * 0.5f) * 2) * 0.5f;
		push.projScale = glm::vec2(projScale, projScale);

		ssaoPass.beginRenderPass(cmd);
		ssaoPass.bindMaterial(cmd, material);
		ssaoPass.setPushConstants(cmd, material, &push);
		ssaoPass.drawMesh(cmd, mesh);
		ssaoPass.endRenderPass(cmd);
		});

	RenderPass& ssaoSpacialPass = frameGraph->createPass("GTAO Spacial", FrameGraphQueueBits::GRAPHICS);
	ssaoSpacialPass.createMaterialFromShaders("shaders/graph/bare.vert", "shaders/graph/gtao_spacial.frag");
	ssaoSpacialPass.addInputAttachment(ssao_result);
	ssaoSpacialPass.addInputAttachment(g_depthMip);
	ssaoSpacialPass.addOutputAttachment(ssao_spacial);
	ssaoSpacialPass.addInputBuffer(cameraInfoBuffer);

	ssaoSpacialPass.setOnRender([&](VkCommandBuffer& cmd) {
		MaterialInstance* material = ssaoSpacialPass.getDefaultMaterial();
		Mesh* mesh = fullscreenMesh;

		struct {
			glm::vec4 texelSize;
			glm::vec2 direction;
			glm::vec2 offset;
		} push;

		push.texelSize = glm::vec4(1.0f / (float)ssaoSpacialPass.getWidth(), 1.0f / (float)ssaoSpacialPass.getHeight(),
			ssaoSpacialPass.getWidth(), ssaoSpacialPass.getHeight());

		glm::vec2 offsets[] = {
			glm::vec2(1, 1),
			glm::vec2(1, -1),
			glm::vec2(-1, 1),
			glm::vec2(-1, -1),
		};

		push.offset = glm::vec2(
			offsets[frameIndex % 4].x / (float)ssaoSpacialPass.getWidth(),
			offsets[frameIndex % 4].y / (float)ssaoSpacialPass.getHeight()
		);

		ssaoSpacialPass.beginRenderPass(cmd);
		ssaoSpacialPass.bindMaterial(cmd, material);

		push.direction = glm::vec2(1.0f / (float)ssaoSpacialPass.getWidth(), 1.0f / (float)ssaoSpacialPass.getHeight());
		ssaoSpacialPass.setPushConstants(cmd, material, &push);
		ssaoSpacialPass.drawMesh(cmd, mesh);


		ssaoSpacialPass.endRenderPass(cmd);
		});

	RenderPass& ssaoTemporalPass = frameGraph->createPass("GTAO Temporal", FrameGraphQueueBits::GRAPHICS);
	ssaoTemporalPass.createMaterialFromShaders("shaders/graph/bare.vert", "shaders/graph/gtao_temporal.frag");
	ssaoTemporalPass.addInputAttachment(ssao_spacial);
	ssaoTemporalPass.addInputAttachment(ssao_spacialHistory);
	ssaoTemporalPass.addInputAttachment(g_vel);
	ssaoTemporalPass.addInputAttachment(g_depthMip);
	ssaoTemporalPass.addOutputAttachment(ssao_temporal);
	ssaoTemporalPass.addHistoryAttachment(ssao_temporal, ssao_spacialHistory);

	ssaoTemporalPass.setOnRender([&](VkCommandBuffer& cmd) {
		MaterialInstance* material = ssaoTemporalPass.getDefaultMaterial();
		Mesh* mesh = fullscreenMesh;

		struct {
			glm::vec4 texelSize;
		} push;

		push.texelSize = glm::vec4(1.0f / (float)ssaoTemporalPass.getWidth(), 1.0f / (float)ssaoTemporalPass.getHeight(),
			ssaoTemporalPass.getWidth(), ssaoTemporalPass.getHeight());

		ssaoTemporalPass.beginRenderPass(cmd);
		ssaoTemporalPass.bindMaterial(cmd, material);
		ssaoTemporalPass.setPushConstants(cmd, material, &push);
		ssaoTemporalPass.drawMesh(cmd, mesh);
		ssaoTemporalPass.endRenderPass(cmd);
	});

	light.setOnRender([&](VkCommandBuffer& cmd) {
		MaterialInstance* material = light.getDefaultMaterial();
		Mesh* mesh = fullscreenMesh;

		struct {
			glm::vec4 direction;
			glm::vec3 color;
			float intensity;
		} consts;

		consts.direction = settings.directionalLightDirection;
		consts.color = settings.directionalLightColor;
		consts.intensity = settings.directionalLightIntensity;

		light.beginRenderPass(cmd);
		light.bindMaterial(cmd, material);
		light.setPushConstants(cmd, material, &consts);
		light.drawMesh(cmd, mesh);
		light.endRenderPass(cmd);
	});

	RenderPass& gpass = frameGraph->createPass("Geometry", FrameGraphQueueBits::GRAPHICS);
	gpass.setDepthStencilOutputAttachment(g_depth);
	gpass.addOutputAttachment(g_color);
	gpass.addOutputAttachment(g_normal);
	gpass.addOutputAttachment(g_properties);
	gpass.addOutputAttachment(g_vel);
	//gpass.addHistoryAttachment(g_depth, g_last_depth);

	gpass.setOnRender([this, &gpass](VkCommandBuffer& cmd) {
		gpass.beginRenderPass(cmd);

		VkPipeline lastPipeline = VK_NULL_HANDLE;

		Scene::getNodesWithComponents<nodeComponents::Render>().each([&cmd, &lastPipeline, this](auto& c) {
			if (!c.render) return;

			Mesh* mesh = c.mesh;
			MaterialInstance* material = c.material;
			SceneNode* node = c.node;

			if (!mesh || !material || !node) return;

			material->updateDescriptors();

			//if (!camera->frustum.AABBInFrustum(node->transform.getWorldBoundsMin(), node->transform.getWorldBoundsMax())) return;

			if (lastPipeline != material->getPipeline()) {
				vkCmdBindPipeline(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, material->getPipeline());
				lastPipeline = material->getPipeline();
			}

			vkCmdBindDescriptorSets(
				cmd,
				VK_PIPELINE_BIND_POINT_GRAPHICS,
				material->getCurrentFrameDescriptor().getPipelineLayout(),
				0,
				material->getCurrentFrameDescriptor().getDescriptorCount(),
				material->getCurrentFrameDescriptor().getHandlePtr(),
				0,
				nullptr
			);

			pushConsts.mat = node->transform.getWorldMatrix();
			pushConsts.lastMat = node->transform.getLastFrameWorldMatrix();

			vkCmdPushConstants(cmd,
				material->getCurrentFrameDescriptor().getPipelineLayout(),
				VK_SHADER_STAGE_VERTEX_BIT,
				0,
				sizeof(glm::mat4) * 2,
				&pushConsts);

			vkCmdBindVertexBuffers(cmd, 0, 1, &mesh->buffer, &mesh->vertexOffset);
			vkCmdBindIndexBuffer(cmd, mesh->buffer, mesh->indexOffset, VK_INDEX_TYPE_UINT32);
			vkCmdDrawIndexed(cmd, mesh->indicesCount, 1, 0, 0, 1);
			});
		gpass.endRenderPass(cmd);
		});

	RenderPass& shadowpass = frameGraph->createPass("Shadow Geometry", FrameGraphQueueBits::GRAPHICS);
	shadowpass.setDepthStencilOutputAttachment(cascade_result);

	shadowpass.setOnRender([this, &shadowpass](VkCommandBuffer& cmd) {
		for (uint32_t i = 0; i < this->shadowMapCascadeSplits; i++) {
			shadowpass.beginRenderPass(cmd, i);

			VkPipeline lastPipeline = VK_NULL_HANDLE;

			Frustum f = cascadeFrustums[i];

			Scene::getNodesWithComponents<nodeComponents::Render>().each([&cmd, &lastPipeline, &f, this, i](auto& c) {
				if (!c.render) return;

				Mesh* mesh = c.mesh;
				MaterialInstance* material = c.material;
				SceneNode* node = c.node;

				if (!mesh || !material || !node) return;

				//material->updateDescriptors();

				if (!f.AABBInFrustum(node->transform.getWorldBoundsMin(), node->transform.getWorldBoundsMax())) return;

				VkPipeline pipeline = material->getPipeline
				(static_cast<MaterialType>(static_cast<uint32_t>(MaterialType::SHADOW_CASCADE_0) + i));

				if (lastPipeline != pipeline) {
					vkCmdBindPipeline(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);
					lastPipeline = pipeline;
				}

				vkCmdBindDescriptorSets(
					cmd,
					VK_PIPELINE_BIND_POINT_GRAPHICS,
					material->getCurrentFrameDescriptor().getPipelineLayout(),
					0,
					material->getCurrentFrameDescriptor().getDescriptorCount(),
					material->getCurrentFrameDescriptor().getHandlePtr(),
					0,
					nullptr
				);

				pushConsts.mat = node->transform.getWorldMatrix();
				//pushConsts.lastMat = node->transform.getLastFrameWorldMatrix();

				vkCmdPushConstants(cmd,
					material->getCurrentFrameDescriptor().getPipelineLayout(),
					VK_SHADER_STAGE_VERTEX_BIT,
					0,
					sizeof(glm::mat4),
					&pushConsts);

				vkCmdBindVertexBuffers(cmd, 0, 1, &mesh->buffer, &mesh->vertexOffset);
				vkCmdBindIndexBuffer(cmd, mesh->buffer, mesh->indexOffset, VK_INDEX_TYPE_UINT32);
				vkCmdDrawIndexed(cmd, mesh->indicesCount, 1, 0, 0, 1);
				});

			shadowpass.endRenderPass(cmd);
		}
		});

#ifdef ENABLE_RTX
	{
		AttachmentInfo ainfo = {};
		ainfo.width = w;
		ainfo.height = h;
		ainfo.initialLayout = ImageLayout::GENERAL;
		ainfo.format = VK_FORMAT_R16G16B16A16_SFLOAT;

		RenderAttachment& attachment = frameGraph->createAttachment("rtx_output", ainfo);
	}

	rtContext = new RayTracingContext();
	rtContext->initialize(nullptr);
#endif

	frameGraph->buildGraph();

	fxaaPassOuput = fxaa_result.getTexture();

	{
		//depthMipDescriptors.resize(g_depthMip.getTexture()->getMipCount() - 1);

		DescriptorLayout mipLayout;
		mipLayout.addDescriptor({ "lastMip", ShaderStageBits::FRAGMENT, DescriptorBindingType::COMBINED_IMAGE_SAMPLER, 0, 0 });
		mipLayout.addPushConstant({ ShaderStageBits::FRAGMENT, 0, sizeof(glm::vec4) });
		mipLayout.finalize();

		for (uint32_t mip = 1; mip < g_depthMip.getTexture()->getMipCount(); mip++) {
			int32_t width = g_depthMip.getTexture()->getWidth() >> mip;
			int32_t height = g_depthMip.getTexture()->getHeight() >> mip;

			if (width < 1) width = 1;
			if (height < 1) height = 1;

			DescriptorGroup group = DescriptorGroup(mipLayout);

			VkImageSubresourceRange range;
			range.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			range.baseArrayLayer = 0;
			range.baseMipLevel = mip - 1;
			range.layerCount = 1;
			range.levelCount = 1;

			VkImageView view = g_depthMip.getTexture()->getImageView(range);

			group.getBinding("lastMip").setBinding(
				g_depthMip.getTexture(),
				&g_depthMip.getTexture()->getDescriptor(view)
			);
			group.update();

			range.baseMipLevel = mip;
			VkImageView fboView = g_depthMip.getTexture()->getImageView(range);

			VkFramebufferCreateInfo bufferInfo = {};
			bufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
			bufferInfo.pNext = nullptr;
			bufferInfo.flags = 0;
			bufferInfo.attachmentCount = 1;
			bufferInfo.width = width;
			bufferInfo.height = height;
			bufferInfo.layers = 1;
			bufferInfo.pAttachments = &fboView;
			bufferInfo.renderPass = depthPyramidPass.getRenderPass();
			depthPyramidPass.createFramebuffer(bufferInfo);

			depthMipDescriptors.push_back(group);
		}

		depthMipMaterial = new MaterialDescription("depth_mip");
		VertexLayout vertexLayout = VertexLayout({
			VertexAttribute(VertexAttributeType::POSITION, VK_FORMAT_R32G32_SFLOAT, 2, sizeof(float)),
			VertexAttribute(VertexAttributeType::UV, VK_FORMAT_R32G32_SFLOAT, 2, sizeof(float))
			});

		GraphicsPipelineCreateInfo pipelineInfo = {};
		pipelineInfo.colorBlendState.attachmentCount = 1;
		pipelineInfo.renderPass = depthPyramidPass.getRenderPass();

		Shader* vS = Resources::loadSPIRVShader("shaders/graph/bare.vert");
		Shader* fS = Resources::loadSPIRVShader("shaders/graph/depth_pyramid.frag");

		pipelineInfo.rasterizationState.cullMode = CullMode::NONE;

		Material* base = depthMipMaterial->createMaterial(vertexLayout, mipLayout, { vS->getBasePermutation(), fS->getBasePermutation() }, pipelineInfo);
		depthMipMaterialInstance = base->createInstance();
	}

	MaterialDescription* description = Resources::loadMaterial("default", "materials/default.mat", frameGraph);

	GRAIL_LOG(INFO, "MATERIAL") << "loaded";
}

void grail::DeferredRenderer::updateShadowCascades() {
	std::vector<float> cascadeSplits = std::vector<float>(shadowMapCascadeSplits);

	float nearClip = camera->nearPlane;
	float farClip = camera->farPlane;
	float clipRange = farClip - nearClip;

	float minZ = nearClip;
	float maxZ = nearClip + clipRange;

	float range = maxZ - minZ;
	float ratio = maxZ / minZ;

	// Calculate split depths based on view camera furstum
	// Based on method presentd in https://developer.nvidia.com/gpugems/GPUGems3/gpugems3_ch10.html
	for (uint32_t i = 0; i < shadowMapCascadeSplits; i++) {
		float p = (i + 1) / static_cast<float>(shadowMapCascadeSplits);
		float log = minZ * std::pow(std::abs(ratio), p);
		float uniform = minZ + range * p;
		float d = settings.cascadeSplitLambda * (log - uniform) + uniform;
		cascadeSplits[i] = (d - nearClip) / clipRange;
	}

	auto inverseScaleTranslation = [](glm::mat4& in) -> glm::mat4 {
		glm::mat4 inv = glm::mat4(1.0);

		inv[0][0] = 1.0f / in[0][0];
		inv[1][1] = 1.0f / in[1][1];
		inv[2][2] = 1.0f / in[2][2];

		inv[3][0] = -in[3][0] * in[0][0];
		inv[3][1] = -in[3][1] * in[1][1];
		inv[3][2] = -in[3][2] * in[2][2];

		return inv;
	};

	// Calculate orthographic projection matrix for each cascade
	float lastSplitDist = 0.0;
	for (uint32_t i = 0; i < shadowMapCascadeSplits; i++) {
		float splitDist = cascadeSplits[i];

		glm::vec3 frustumCorners[8] = {
			glm::vec3(-1.0f,  1.0f, -1.0f),
			glm::vec3(1.0f,  1.0f, -1.0f),
			glm::vec3(1.0f, -1.0f, -1.0f),
			glm::vec3(-1.0f, -1.0f, -1.0f),
			glm::vec3(-1.0f,  1.0f,  1.0f),
			glm::vec3(1.0f,  1.0f,  1.0f),
			glm::vec3(1.0f, -1.0f,  1.0f),
			glm::vec3(-1.0f, -1.0f,  1.0f),
		};

		// Project frustum corners into world space
		glm::mat4 invCam = glm::inverse(camera->projection * camera->view);
		for (uint32_t i = 0; i < 8; i++) {
			glm::vec4 invCorner = invCam * glm::vec4(frustumCorners[i], 1.0f);
			frustumCorners[i] = invCorner / invCorner.w;
		}

		for (uint32_t i = 0; i < 4; i++) {
			glm::vec3 dist = frustumCorners[i + 4] - frustumCorners[i];
			frustumCorners[i + 4] = frustumCorners[i] + (dist * splitDist);
			frustumCorners[i] = frustumCorners[i] + (dist * lastSplitDist);
		}

		// Get frustum center
		glm::vec3 frustumCenter = glm::vec3(0.0f);
		for (uint32_t i = 0; i < 8; i++) {
			frustumCenter += frustumCorners[i];
		}
		frustumCenter /= 8.0f;

		float radius = 0.0f;
		for (uint32_t i = 0; i < 8; i++) {
			float distance = glm::length(frustumCorners[i] - frustumCenter);
			radius = glm::max(radius, distance);
		}
		radius = std::ceil(radius * 16.0f) / 16.0f;

		glm::vec3 maxExtents = glm::vec3(radius);
		glm::vec3 minExtents = -maxExtents;

		// THIS NEEDS TO BE FIXED
		glm::vec3 lightDir = glm::normalize(-settings.directionalLightDirection);
		glm::mat4 lightViewMatrix = glm::lookAt(frustumCenter - lightDir * -minExtents.z, frustumCenter, glm::vec3(0.0f, 1.0f, 0.0f));
		glm::mat4 lightOrthoMatrix = glm::ortho(minExtents.x, maxExtents.x, minExtents.y, maxExtents.y, 0.0f, maxExtents.z - minExtents.z);

		glm::mat4 shadowMatrix = lightOrthoMatrix * lightViewMatrix;
		glm::vec4 shadowOrigin = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
		shadowOrigin = shadowMatrix * shadowOrigin;
		shadowOrigin = shadowOrigin * static_cast<float>(shadowCascadeMapSize) / 2.0f;

		glm::vec4 roundedOrigin = glm::round(shadowOrigin);
		glm::vec4 roundOffset = roundedOrigin - shadowOrigin;
		roundOffset = roundOffset * 2.0f / static_cast<float>(shadowCascadeMapSize);
		roundOffset.z = 0.0f;
		roundOffset.w = 0.0f;

		glm::mat4 shadowProj = lightOrthoMatrix;
		shadowProj[3] += roundOffset;
		lightOrthoMatrix = shadowProj;

		// Store split distance and matrix in cascade
		cameraInfo.cascadeSplits[i].x = (camera->nearPlane + splitDist * clipRange) * -1.0f;
		cameraInfo.cascadeMats[i] = lightOrthoMatrix * lightViewMatrix;

		if (!settings.freezeViewFrustrum)
			cascadeFrustums[i].update(cameraInfo.cascadeMats[i]);

		lastSplitDist = cascadeSplits[i];
	}
}

void grail::DeferredRenderer::generateBRDFLookupTexture() {
	FrameGraph lutGraph = FrameGraph(VulkanContext::mainGPU);

	uint32_t dimensions = 512;

	AttachmentInfo info = {};
	info.width = dimensions;
	info.height = dimensions;
	info.format = VK_FORMAT_R32G32_SFLOAT;

	RenderAttachment& attachment = lutGraph.createAttachment("brdf_lut", info);

	RenderPass& pass = lutGraph.createPass("lut_gen", FrameGraphQueueBits::GRAPHICS);
	pass.createMaterialFromShaders("shaders/graph/bare.vert", "shaders/graph/brdflut.frag");
	pass.addOutputAttachment(attachment);
	//pass.addInputTexture(Resources::getTexture("red"));
	pass.setOnRender([&pass, &attachment](VkCommandBuffer& cmd) {
		MaterialInstance* material = pass.getDefaultMaterial();
		Mesh* mesh = Resources::getMesh("fullscreen_mesh");

		pass.beginRenderPass(cmd);
		pass.bindMaterial(cmd, material);
		pass.drawMesh(cmd, mesh);
		pass.endRenderPass(cmd);

		Resources::getTexture("brdf_lut")->transitionImage(cmd, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
	});

	lutGraph.setFinalPass(pass);
	lutGraph.buildGraph();

	lutGraph.buildFrame();
	lutGraph.submitFrame();
}

void grail::DeferredRenderer::generateIrradianceMap() {
	FrameGraph irrGraph = FrameGraph(VulkanContext::mainGPU);

	uint32_t dimensions = 32;

	AttachmentInfo info = {};
	info.width = dimensions;
	info.height = dimensions;
	info.format = VK_FORMAT_R32G32B32A32_SFLOAT;

	VkTexture* cubemap;
	{
		TextureCreateInfo info = {};
		info.generateMipmaps = false;
		info.arrayLevels = 6;
		info.createFlags = VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;
		info.samplerInfo.addressModeU = SamplerAddressMode::CLAMP_TO_EDGE;
		info.samplerInfo.addressModeV = SamplerAddressMode::CLAMP_TO_EDGE;
		info.usageBits = ImageUsageBits::TRANSFER_DST_BIT | ImageUsageBits::SAMPLED_BIT;
		info.format = VK_FORMAT_R32G32B32A32_SFLOAT;
		info.initialLayout = ImageLayout::TRANSFER_DST_OPTIMAL;

		cubemap = Resources::createTexture("IrradianceCube", dimensions, dimensions, info);
	}

	TextureCreateInfo teInfo = {};
	teInfo.generateMipmaps = true;
	teInfo.bpp = 4;
	teInfo.format = VK_FORMAT_R32G32B32A32_SFLOAT;
	teInfo.samplerInfo.anisotropyEnable = false;
	teInfo.samplerInfo.maxAnisotropy = 1.0f;

	RenderAttachment& attachment = irrGraph.createAttachment("irradiance_gen", info);

	RenderPass& pass = irrGraph.createPass("irradiance_framebuffer", FrameGraphQueueBits::GRAPHICS);
	pass.createMaterialFromShaders("shaders/graph/cube.vert", "shaders/graph/irradiance.frag");
	pass.addOutputAttachment(attachment);

	pass.addInputTexture(Resources::loadTexture("textures/envMaps/lebombo_2k.hdr", teInfo));


	pass.setOnRender([&pass, &attachment, dimensions, cubemap](VkCommandBuffer& cmd) {
		MaterialInstance* material = pass.getDefaultMaterial();
		Mesh* mesh = Resources::getMesh("cubeMesh");

		VkImageSubresourceRange subresourceRange = {};
		subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		subresourceRange.baseMipLevel = 0;
		subresourceRange.levelCount = 1;
		subresourceRange.layerCount = 6;

		for (uint32_t f = 0; f < 6; f++) {
			pass.beginRenderPass(cmd);

			std::vector<glm::mat4> matrices = {
				// POSITIVE_X
				glm::rotate(glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f)), glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
				// NEGATIVE_X
				glm::rotate(glm::rotate(glm::mat4(1.0f), glm::radians(-90.0f), glm::vec3(0.0f, 1.0f, 0.0f)), glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
				// POSITIVE_Y
				glm::rotate(glm::mat4(1.0f), glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
				// NEGATIVE_Y
				glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
				// POSITIVE_Z
				glm::rotate(glm::mat4(1.0f), glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
				// NEGATIVE_Z
				glm::rotate(glm::mat4(1.0f), glm::radians(180.0f), glm::vec3(0.0f, 0.0f, 1.0f)),
			};

			struct PushBlock {
				glm::mat4 mvp = glm::mat4(1.0f);
				// Sampling deltas
				float deltaPhi = ((2.0f * float(glm::pi<float>())) / 180.0f);
				float deltaTheta = ((0.5f * float(glm::pi<float>())) / 64.0f);
				float boost = 1.0;
			} pushBlock;

			pushBlock.mvp = glm::perspective((float)(glm::pi<float>() / 2.0), 1.0f, 0.1f, 256.0f) * matrices[f];

			pass.bindMaterial(cmd, material);
			pass.setPushConstants(cmd, material, &pushBlock);

			vkCmdBindVertexBuffers(cmd, 0, 1, &mesh->buffer, &mesh->vertexOffset);
			vkCmdDraw(cmd, mesh->verticesCount, 1, 0, 1);

			pass.endRenderPass(cmd);

			attachment.getTexture()->transitionImage(cmd, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);

			VkImageBlit blitRegion = {};

			blitRegion.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			blitRegion.srcSubresource.baseArrayLayer = 0;
			blitRegion.srcSubresource.mipLevel = 0;
			blitRegion.srcSubresource.layerCount = 1;
			blitRegion.srcOffsets[0] = { 0, 0, 0 };
			blitRegion.srcOffsets[1] = { int32_t(dimensions), int32_t(dimensions), 1 };

			blitRegion.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			blitRegion.dstSubresource.baseArrayLayer = f;
			blitRegion.dstSubresource.mipLevel = 0;
			blitRegion.dstSubresource.layerCount = 1;
			blitRegion.dstOffsets[0] = { 0, 0, 0 };
			blitRegion.dstOffsets[1] = { int32_t(dimensions), int32_t(dimensions), 1 };

			VkFilter filter = VK_FILTER_LINEAR;
			if (platform::getPlatformType() == platform::PlatformType::MOBILE) {
				filter = VK_FILTER_NEAREST;
			}

			vkCmdBlitImage(
				cmd,
				attachment.getTexture()->getImage(),
				VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
				cubemap->getImage(),
				VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
				1,
				&blitRegion, filter);

			attachment.getTexture()->transitionImage(cmd, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);
		}

		cubemap->transitionImage(cmd, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		attachment.getTexture()->transitionImage(cmd, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		});

	irrGraph.setFinalPass(pass);

	irrGraph.buildGraph();

	irrGraph.buildFrame();
	irrGraph.submitFrame();
}

void grail::DeferredRenderer::generateSpecularMap() {
	FrameGraph irrGraph = FrameGraph(VulkanContext::mainGPU);

	uint32_t dimensions = 128;
	uint32_t mipCount = static_cast<uint32_t>(std::floor(std::log2(dimensions))) + 1;

	AttachmentInfo info = {};
	info.width = dimensions;
	info.height = dimensions;
	info.levels = mipCount;
	info.format = VK_FORMAT_R32G32B32A32_SFLOAT;

	VkTexture* cubemap;
	{
		TextureCreateInfo info = {};
		info.generateMipmaps = true;
		info.arrayLevels = 6;
		info.createFlags = VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;
		info.samplerInfo.addressModeU = SamplerAddressMode::CLAMP_TO_EDGE;
		info.samplerInfo.addressModeV = SamplerAddressMode::CLAMP_TO_EDGE;
		info.usageBits = ImageUsageBits::TRANSFER_DST_BIT | ImageUsageBits::SAMPLED_BIT;
		info.format = VK_FORMAT_R32G32B32A32_SFLOAT;
		info.initialLayout = ImageLayout::TRANSFER_DST_OPTIMAL;

		cubemap = Resources::createTexture("SpecularCube", dimensions, dimensions, info);
	}

	TextureCreateInfo teInfo = {};
	teInfo.generateMipmaps = true;
	teInfo.bpp = 4;
	teInfo.format = VK_FORMAT_R32G32B32A32_SFLOAT;
	teInfo.samplerInfo.anisotropyEnable = false;
	teInfo.samplerInfo.maxAnisotropy = 1.0f;

	RenderAttachment& attachment = irrGraph.createAttachment("spec_gen", info);

	struct PushBlock {
		glm::mat4 mvp;
		// Sampling deltas
		float roughness = 0.0;
		uint32_t numSamples = 128;
		float boost = 1.0;
	} pushBlock;

	RenderPass& pass = irrGraph.createPass("spec_pass", FrameGraphQueueBits::GRAPHICS);
	pass.createMaterialFromShaders("shaders/graph/cube.vert", "shaders/graph/prefilterSpec.frag");
	pass.addOutputAttachment(attachment);

	pass.addInputTexture(Resources::loadTexture("textures/envMaps/lebombo_2k.hdr", teInfo));

	pass.setOnRender([&pass, &attachment, dimensions, cubemap, &pushBlock, mipCount](VkCommandBuffer& cmd) {
		MaterialInstance* material = pass.getDefaultMaterial();
		Mesh* mesh = Resources::getMesh("cubeMesh");

		VkImageSubresourceRange subresourceRange = {};
		subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		subresourceRange.baseMipLevel = 0;
		subresourceRange.levelCount = 1;
		subresourceRange.layerCount = 6;
		for (uint32_t m = 0; m < mipCount; m++) {
			pushBlock.roughness = (float)m / (float)(mipCount - 1);
			for (uint32_t f = 0; f < 6; f++) {

				VkViewport viewport = {};
				viewport.minDepth = 0.0f;
				viewport.maxDepth = 1.0f;
				viewport.x = 0;
				viewport.y = 0;
				viewport.width = static_cast<float>(dimensions * std::pow(0.5f, m));
				viewport.height = static_cast<float>(dimensions * std::pow(0.5f, m));

				vkCmdSetViewport(cmd, 0, 1, &viewport);

				pass.beginRenderPass(cmd);

				std::vector<glm::mat4> matrices = {
					// POSITIVE_X
					glm::rotate(glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f)), glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
					// NEGATIVE_X
					glm::rotate(glm::rotate(glm::mat4(1.0f), glm::radians(-90.0f), glm::vec3(0.0f, 1.0f, 0.0f)), glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
					// POSITIVE_Y
					glm::rotate(glm::mat4(1.0f), glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
					// NEGATIVE_Y
					glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
					// POSITIVE_Z
					glm::rotate(glm::mat4(1.0f), glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
					// NEGATIVE_Z
					glm::rotate(glm::mat4(1.0f), glm::radians(180.0f), glm::vec3(0.0f, 0.0f, 1.0f)),
				};

				pushBlock.mvp = glm::perspective((float)(glm::pi<float>() / 2.0), 1.0f, 0.1f, 256.0f) * matrices[f];

				pass.bindMaterial(cmd, material);
				pass.setPushConstants(cmd, material, &pushBlock);

				vkCmdBindVertexBuffers(cmd, 0, 1, &mesh->buffer, &mesh->vertexOffset);
				vkCmdDraw(cmd, mesh->verticesCount, 1, 0, 1);

				pass.endRenderPass(cmd);

				attachment.getTexture()->transitionImage(cmd, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);

				VkImageBlit blitRegion = {};

				blitRegion.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
				blitRegion.srcSubresource.baseArrayLayer = 0;
				blitRegion.srcSubresource.mipLevel = 0;
				blitRegion.srcSubresource.layerCount = 1;
				blitRegion.srcOffsets[0] = { 0, 0, 0 };
				blitRegion.srcOffsets[1] = { int32_t(viewport.width), int32_t(viewport.height), 1 };

				blitRegion.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
				blitRegion.dstSubresource.baseArrayLayer = f;
				blitRegion.dstSubresource.mipLevel = m;
				blitRegion.dstSubresource.layerCount = 1;
				blitRegion.dstOffsets[0] = { 0, 0, 0 };
				blitRegion.dstOffsets[1] = { int32_t(viewport.width), int32_t(viewport.height), 1 };

				VkFilter filter = VK_FILTER_LINEAR;
				if (platform::getPlatformType() == platform::PlatformType::MOBILE) {
					filter = VK_FILTER_NEAREST;
				}

				vkCmdBlitImage(
					cmd,
					attachment.getTexture()->getImage(),
					VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
					cubemap->getImage(),
					VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
					1,
					&blitRegion, filter);

				attachment.getTexture()->transitionImage(cmd, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);
			}
		}

		cubemap->transitionImage(cmd, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		attachment.getTexture()->transitionImage(cmd, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		});


	irrGraph.setFinalPass(pass);

	irrGraph.buildGraph();

	irrGraph.buildFrame();
	irrGraph.submitFrame();
}
