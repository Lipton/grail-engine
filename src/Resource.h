#ifndef GRAIL_RESOURCE_H
#define GRAIL_RESOURCE_H

#include "Common.h"

namespace grail {
	enum class ResourceType {
		UNKNOWN,
		INTERNAL,
		EXTERNAL
	};

	enum class MemoryType {
		STATIC,
		DYNAMIC,
	};

	class Resource {
	public:
		ResourceType getResourceType();

		std::string getIdentifier();

		virtual void dispose() = 0;
	private:
		std::string identifier;

		ResourceType resourceType;
	};
}

#endif