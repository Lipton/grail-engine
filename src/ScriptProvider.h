#ifndef GRAIL_SCRIPT_PROVIDER_H
#define GRAIL_SCRIPT_PROVIDER_H

#include <string>
#include <functional>
#include <vector>

class asIScriptEngine;
class asSMessageInfo;
class asIScriptModule;
class CScriptBuilder;

namespace grail {
	class Script;
	class Mesh;

	class ScriptProvider {
		friend class Grail;
		friend class Script;
	public:
		// Create a default script under a given name
		static Script* createScript(const std::string& name);
		
		// Create a script under a given name and source
		static Script* createScript(const std::string& name, const std::string& source);

		// Removes the script from the engine
		static void removeScript(Script* script);

		// Recompile all of the script sections, only does it if needed
		static void recompile();

		static void executeString(const std::string& code);

		static bool isCompiled();

	private:
		static ::asIScriptEngine* engine;
		// Global module for all of the scripts
		static ::asIScriptModule* mainModule;
		static ::asIScriptModule* dynamicModule;

		// Scripts that belong to the provider
		static std::vector<Script*> scripts;
		static uint64_t scriptID;

		static bool needsRecompilation;

		static ::CScriptBuilder* builder;

		static void callback(const asSMessageInfo* msg, void* param);

		static void initialize();

		template<class A, class B>
		inline static B* cast(A* a) {
			return dynamic_cast<B*>(a);
		};

		// ABSOLUTELY EXTREMELY TEMPORARY
		static class FreeTypeFont* temp_font;
		static Mesh* temp_create_text_mesh(const std::string& assetName, const std::string& textContent);

	};
}

#endif