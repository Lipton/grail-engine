#ifndef GRAIL_INTERSECTOR_H
#define GRAIL_INTERSECTOR_H

#include "Common.h"

namespace grail {
	class Mesh;

	class Intersector {
	public:
		static bool intersectMeshRay(glm::vec3 rayOrigin, glm::vec3 rayVector, Mesh* mesh, glm::mat4 meshTransform, glm::vec3& intersection);
	};
}

#endif