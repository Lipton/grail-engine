#ifndef GRAIL_PHYSICS_H
#define GRAIL_PHYSICS_H

#include "Common.h"

/*#include <btBulletDynamicsCommon.h>
#include <btBulletCollisionCommon.h>
#include <BulletCollision/CollisionShapes/btShapeHull.h>
*/

namespace grail {
	namespace nodeComponents {
		class PhysicsComponent;
	}

	class Mesh;
	class SceneNode;
	class Buffer;
	class MaterialDescription;
	class MaterialInstance;
	class VertexLayout;
	class Camera;

	class PhysicsDebugRenderer /*: public btIDebugDraw*/ {
	public:
		PhysicsDebugRenderer(VkRenderPass renderPass);

		//void drawLine(const btVector3& from, const btVector3& to, const btVector3& color);
		void drawLine(glm::mat4 model, glm::vec3 from, glm::vec3 to, glm::vec3 color);
		void drawBox(glm::mat4 model, glm::vec3 min, glm::vec3 max, glm::vec3 color);
	
		//void drawContactPoint(const btVector3& PointOnB, const btVector3& normalOnB, btScalar distance, int lifeTime, const btVector3& color);

		//void reportErrorWarning(const char* warningString);

		//void draw3dText(const btVector3& location, const char* textString);

		//void setDebugMode(int debugMode);

		//int getDebugMode() const;

		void begin(Camera* camera, VkCommandBuffer& cmd);
		void end(Camera* camera, VkCommandBuffer& cmd);
	private:
		Buffer* vertexBuffer;
		MaterialDescription* material;
		MaterialInstance* materialInstace;
		VertexLayout* vertexLayout;

		float* vtxDst = nullptr;
		uint32_t vtxCount = 0;

		Camera* cam;
		VkCommandBuffer cmdBuff;

		uint32_t bufferVertexCount;
	};
	/*
	enum class PhysicsShapeType : uint32_t {
		BOX = 0,
		SPHERE,
		CAPSULE,
		CYLINDER,
		CONE,
		CONVEX_HULL,
		TRIANGLE_MESH
	};*/

	class Physics {
		friend class Editor;
		friend class nodeComponents::PhysicsComponent;
	public:
		static void initialize();
		static void initializeDebug(VkRenderPass renderPass);

		//static void createShapeForMesh(SceneNode* node, Mesh* mesh, btCollisionShape** shape, btRigidBody** body);
	
		static void simulate();
		static void drawDebug(Camera* camera, VkCommandBuffer cmd);

		//static void removeBody(btRigidBody* body);
	private:
		/*static btDefaultCollisionConfiguration* collisionConfiguration;
		static btCollisionDispatcher* collisionDispatcher;
		static btBroadphaseInterface* overlappingPairCache;
		static btSequentialImpulseConstraintSolver* impulseSolver;
		static btDiscreteDynamicsWorld* dynamicsWorld;
		static btAlignedObjectArray<btCollisionShape*> collisionShapes;*/

		// Debug related
	public:
		static PhysicsDebugRenderer* debugRenderer;
	private:
		Physics& operator = (const Physics&) = delete;
		Physics(const Physics&) = delete;
		Physics() = default;
	};
}

#endif