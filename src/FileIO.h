#ifndef GRAIL_FILE_IO_H
#define GRAIL_FILE_IO_H

#include "Common.h"

namespace grail {
	enum class PathType {
		FILE,
		DIRECTORY,
		NON_EXISTANT,
		UNKNOWN
	};

	struct PathInfo {
		std::string path;
		PathType type;
	};

	class FileIO {
		friend class Grail;
	public:
		static const char DIRECTORY_SEPARATOR;

		static void readBinaryFile(const std::string& path, char** contents, uint32_t* size);
		static void readBinaryFile(const std::string& path, unsigned char** contents, uint32_t* size);

		static void readBinaryFileExternal(const std::string& path, char** contents, uint32_t* size);
		static void readBinaryFileExternal(const std::string& path, unsigned char** contents, uint32_t* size);
	
		static bool fileExists(const std::string& filename);
		static std::string getExtension(const std::string& path);
		static std::string getFilename(const std::string& path);
		static std::string getBasePath(const std::string& path);
		// The type of object the path points to
		static PathType getPathType(const std::string& path);

		// Very simple check, will pass if the path has a dot anywhere
		static bool hasExtension(const std::string& path);

		static void getDirectoryContents(const std::string& path, std::vector<PathInfo>& contents);

		static std::string getAssetPath();
		static std::string getAssetPath(std::string path);

		static bool copyFile(const std::string& src, const std::string& dst);
	private:
		FileIO& operator = (const FileIO&) = delete;
		FileIO(const FileIO&) = delete;
		FileIO() = default;

#if defined(OS_ANDROID)
		static struct android_app* android_app;
#endif
	};
}

#endif