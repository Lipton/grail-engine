#ifndef GRAIL_TEXTURE_ASSET_IMPORTER_H
#define GRAIL_TEXTURE_ASSET_IMPORTER_H

#include "Common.h"
#include "AssetImporter.h"
#include "UUID.h"

namespace grail {
	class TextureAssetImporter : public AssetImporter {
	public:
		void importAsset(const std::string& path) override;
		bool showImportMenu(AssetRef<Asset>, class ImGuiHelper* uiHelper) override;
	protected:
		//void getDescriptor(const std::string& descriptorPath, const std::string& assetPath) /*override*/;
		//void saveDescriptor(const std::string& descriptorPath);
	private:
		//TextureAssetDescriptor tempDescriptor;
	};
}

#endif