#include "OrthographicCamera.h"

grail::OrthographicCamera::OrthographicCamera() {
	this->nearPlane = 0;
}

grail::OrthographicCamera::OrthographicCamera(float viewportWidth, float viewportHeight) {
	this->viewportWidth = viewportWidth;
	this->viewportHeight = viewportHeight;
	this->nearPlane = 0;
	update();
}

void grail::OrthographicCamera::update(bool updateFrustum) {
	projection = glm::ortho(
		zoom * -viewportWidth / 2.0f,
		zoom * (viewportWidth / 2.0f),
		zoom * -(viewportHeight / 2.0f),
		zoom * viewportHeight / 2.0f,
		nearPlane, farPlane);

	view = glm::lookAt(position, position + direction, up);

	combined = projection * view;

	if (updateFrustum)
		frustum.update(combined);
}

void grail::OrthographicCamera::setToOrtho(bool yDown) {
	setToOrtho(yDown, Graphics::getWidth(), Graphics::getHeight());
}

void grail::OrthographicCamera::setToOrtho(bool yDown, float viewportWidth, float viewportHeight) {
	if (yDown) {
		up.x = 0;
		up.y = -1;
		up.z = 0;

		direction.x = 0;
		direction.y = 0;
		direction.z = 1;
	}
	else {
		up.x = 0;
		up.y = 1;
		up.z = 0;

		direction.x = 0;
		direction.y = 0;
		direction.z = -1;
	}

	position.x = zoom * viewportWidth / 2.0f;
	position.y = zoom * viewportHeight / 2.0f;
	position.z = 0;

	this->viewportWidth = viewportWidth;
	this->viewportHeight = viewportHeight;

	update();
}

void grail::OrthographicCamera::translate(float x, float y) {
	Camera::translate(x, y, 0);
}

void grail::OrthographicCamera::translate(glm::vec2 translation) {
	Camera::translate(translation.x, translation.y, 0);
}

