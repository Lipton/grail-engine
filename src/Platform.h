#ifndef GRAIL_PLATFORM_H
#define GRAIL_PLATFORM_H

#if defined(__WIN32__) || defined(_WIN32) || defined(WIN32) || defined(__WINDOWS__) || defined(__TOS_WIN__)
#define OS_WINDOWS
#define WIN32_LEAN_AND_MEAN
#elif defined(__ANDROID__) || defined (ANDROID)
#define OS_ANDROID
#elif defined(__LINUX__) || defined (__linux) || defined(linux) || defined(__linux__)
#define OS_LINUX
#else
#error OS_NOT_SUPPORTED
#endif

#if defined (OS_WINDOWS) || defined (OS_LINUX)
#define PLATFORM_DESKTOP
#else
#define PLATFORM_MOBILE
#endif

namespace grail {
	namespace platform {
		enum class Platform {
			TYPE_WINDOWS,
			TYPE_LINUX,
			TYPE_ANDROID,
			TYPE_UNDEFINED
		};

		enum class PlatformType {
			DESKTOP,
			MOBILE,
			UNDEFINED
		};

		inline const char* getPlatformName() {
#if defined(OS_WINDOWS)
			return "Windows";
#elif defined (OS_LINUX)
			return "Linux";
#elif defined (OS_ANDROID)
			return "Android";
#else
			return "Undefined";
#endif
		}

		inline Platform getPlatform() {
#if defined(OS_WINDOWS)
			return Platform::TYPE_WINDOWS;
#elif defined(OS_LINUX)
			return Platform::TYPE_LINUX;
#elif defined(OS_ANDROID)
			return Platform::TYPE_ANDROID;
#else
			return Platform::TYPE_UNDEFINED;
#endif
		}

		inline PlatformType getPlatformType() {
#if defined(OS_WINDOWS) | defined(OS_LINUX)
			return PlatformType::DESKTOP;
#elif defined(OS_ANDROID)
			return PlatformType::MOBILE;
#else
			return PlatformType::UNDEFINED;
#endif
		}
	}
}

#endif