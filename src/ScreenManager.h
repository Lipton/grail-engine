#ifndef GRAIL_SCREEN_MANAGER
#define GRAIL_SCREEN_MANAGER

#include "Common.h"

namespace grail {
	class Screen;

	class ScreenManager {
		friend class Grail;
		friend class ApplicationWindow;
	public:
		void setScreen(const std::string& screenName);
		void registerScreen(const std::string& name, Screen& screen);
		void deregisterScreen(const std::string& screen);
	private:
		ScreenManager();
		~ScreenManager();

		std::unordered_map<std::string, Screen*> screenMap;

		Screen* currentScreen = nullptr;
		Screen* lastScreen = nullptr;

		void render(float delta);
		void resize(int widht, int height);
		void cursorMoved(float x, float y);
		void keyPressEvent(int keycode, int action);
		void mousePressEvent(int keyCode, int action);
		void dropCallback(int count, const char** paths);
		void charCallback(unsigned int c, int mods);
	};
}

#endif
