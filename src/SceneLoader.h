#ifndef GRAIL_SCENE_LOADER_H
#define GRAIL_SCENE_LOADER_H

#include "Common.h"
#include "SceneNode.h"

#include <json.hpp>

#include "Renderer.h"

namespace grail {
	class Scene;
	class SceneLoader {
	public:
		static void saveSceneToFile(const std::string& file);
		static void saveSceneToString(std::string& string);

		static void loadSceneFromFile(const std::string& file);
		static void loadSceneFromMemory(const std::string& scene);
	private:
		static void saveInternal(nlohmann::json& resultNode);
		static void loadInternal(const std::string& contents);

		static void saveNode(nlohmann::json& jsonNode, SceneNode* node);
		static void loadNode(nlohmann::json& jsonNode, SceneNode* node);
	};
}

#endif