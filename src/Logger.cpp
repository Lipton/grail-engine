#include "Logger.h"

const char* grail::logger::detail::logTypes[5] = {
	"INFO",
	"DEBUG",
	"WARNING",
	"ERROR",
	"GENERIC"
};
grail::logger::Implementation* grail::logger::detail::logImplementation = new grail::logger::detail::DefaultImplemenentation();
std::chrono::high_resolution_clock::time_point grail::logger::detail::startTime = std::chrono::high_resolution_clock::now();
int grail::logger::detail::LOGGER_LOG_LEVEL = (int)grail::LogType::INFO;

void grail::logger::info(const std::string& tag, const std::string& message) {
	Log(LogType::INFO, tag.c_str()) << message;
}

void grail::logger::debug(const std::string& tag, const std::string& message) {
	Log(LogType::DEBUG, tag.c_str()) << message;
}

void grail::logger::warning(const std::string& tag, const std::string& message) {
	Log(LogType::WARNING, tag.c_str()) << message;
}

void grail::logger::error(const std::string& tag, const std::string& message) {
	Log(LogType::ERROR, tag.c_str()) << message;
}

const char * grail::logger::logTypeToString(LogType type) {
	return grail::logger::detail::logTypes[(int)type];
}

void grail::logger::setLoggerImplementation(Implementation * implementation) {
	if (logger::detail::logImplementation != nullptr)
		delete logger::detail::logImplementation;

	logger::detail::logImplementation = implementation;
}

void grail::logger::setLogLevel(LogType type) {
	logger::detail::LOGGER_LOG_LEVEL = (int)type;
}

void grail::logger::detail::dispose() {
	if (logger::detail::logImplementation != nullptr) {
		delete logger::detail::logImplementation;
	}
}
