#ifndef GRAIL_SHADER_LOADER_H
#define GRAIL_SHADER_LOADER_H

#include <vulkan.h>
#include <string>

//#include <spirv_cross.hpp>
#include "Shader.h"
#include <shaderc/shaderc.h>
#include <spirv_cross.hpp>
#include <spirv_glsl.hpp>

namespace grail {
	class ShaderLoader {
		friend class Shader;
	public:
		struct ShaderCompilationOptions {
			
		};

		enum ShaderCompilerOptimizationLevel {
		};

		static VkShaderModule loadShader(std::string path);
		static Shader* loadSPIRVShader(std::string path, std::vector<std::string>& defines);
		static Shader* loadSPIRVShader(std::string path);
	private:
		static ShaderPermutation* parsePermutation(Shader* base, std::vector<std::string>& defines, bool remapDescriptors = false);

		static void parseVariable(const spirv_cross::Resource& res, spirv_cross::Compiler& comp, grail::ShaderVarT& var);

		static std::map<shaderc_shader_kind, ShaderStageBits> kindToStage;
		static std::map<ShaderStageBits, shaderc_shader_kind> stageToKind;

		ShaderLoader& operator = (const ShaderLoader&) = delete;
		ShaderLoader(const ShaderLoader&) = delete;
		ShaderLoader() = default;
	};
}

#endif