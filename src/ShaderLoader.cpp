#include "ShaderLoader.h"

#include "FileIO.h"
#include "Utils.h"

#include <shaderc/shaderc.hpp>
//#include <spirv_cross.hpp>

#include "Shader.h"

std::map<shaderc_shader_kind, grail::ShaderStageBits> grail::ShaderLoader::kindToStage = std::map<shaderc_shader_kind, ShaderStageBits> {
					{ shaderc_shader_kind::shaderc_fragment_shader, ShaderStageBits::FRAGMENT },
					{ shaderc_shader_kind::shaderc_vertex_shader, ShaderStageBits::VERTEX },
					{ shaderc_shader_kind::shaderc_tess_control_shader, ShaderStageBits::TESSELLATION_CONTROL },
					{ shaderc_shader_kind::shaderc_tess_evaluation_shader, ShaderStageBits::TESSELLATION_EVALUATION },
					{ shaderc_shader_kind::shaderc_geometry_shader, ShaderStageBits::GEOMETRY },
					{ shaderc_shader_kind::shaderc_compute_shader, ShaderStageBits::COMPUTE },
};

std::map<grail::ShaderStageBits, shaderc_shader_kind> grail::ShaderLoader::stageToKind = std::map<grail::ShaderStageBits, shaderc_shader_kind>{
					{ ShaderStageBits::FRAGMENT, shaderc_shader_kind::shaderc_fragment_shader },
					{ ShaderStageBits::VERTEX, shaderc_shader_kind::shaderc_vertex_shader },
					{ ShaderStageBits::TESSELLATION_CONTROL, shaderc_shader_kind::shaderc_tess_control_shader },
					{ ShaderStageBits::TESSELLATION_EVALUATION, shaderc_shader_kind::shaderc_tess_evaluation_shader },
					{ ShaderStageBits::GEOMETRY, shaderc_shader_kind::shaderc_geometry_shader },
					{ ShaderStageBits::COMPUTE, shaderc_shader_kind::shaderc_compute_shader },
};


VkShaderModule grail::ShaderLoader::loadShader(std::string path) {
	std::string extension = grail::FileIO::getExtension(path);

	VkShaderModuleCreateInfo moduleCreateInfo = {};
	moduleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;

	VkShaderModule shaderModule;

	if (utils::string::equalsCaseInsensitive(extension, "spv")) {	
		GRAIL_LOG(WARNING, "SHADER LOADER") << "Loading shader from SPIRV binary, default preprocessor settings will not be applied!";

		uint32_t shaderSize;
		char* shaderCode;

		grail::FileIO::readBinaryFile(path, &shaderCode, &shaderSize);

		moduleCreateInfo.codeSize = shaderSize;
		moduleCreateInfo.pCode = (uint32_t*)shaderCode;

		VK_VALIDATE_RESULT(vkCreateShaderModule(VulkanContext::mainGPU.device, &moduleCreateInfo, nullptr, &shaderModule));

		delete[] shaderCode;
	}
	else {
		shaderc_shader_kind shaderKind{};

		if (grail::utils::string::equalsCaseInsensitive(extension, "frag"))
			shaderKind = shaderc_shader_kind::shaderc_fragment_shader;
		else if (grail::utils::string::equalsCaseInsensitive(extension, "vert"))
			shaderKind = shaderc_shader_kind::shaderc_vertex_shader;
		else if (grail::utils::string::equalsCaseInsensitive(extension, "tcs"))
			shaderKind = shaderc_shader_kind::shaderc_tess_control_shader;
		else if (grail::utils::string::equalsCaseInsensitive(extension, "tes"))
			shaderKind = shaderc_shader_kind::shaderc_tess_evaluation_shader;
		else if (grail::utils::string::equalsCaseInsensitive(extension, "geom"))
			shaderKind = shaderc_shader_kind::shaderc_geometry_shader;
		else if (grail::utils::string::equalsCaseInsensitive(extension, "comp"))
			shaderKind = shaderc_shader_kind::shaderc_compute_shader;

		shaderc::Compiler compiler;
		shaderc::CompileOptions options;

		char* contents;
		uint32_t size;

		grail:FileIO::readBinaryFile(path, &contents, &size);

		shaderc::SpvCompilationResult module = compiler.CompileGlslToSpv(contents, size, shaderKind, "main", options);
		delete[] contents;

		if (module.GetCompilationStatus() != shaderc_compilation_status_success) {
			GRAIL_LOG(ERROR, "SHADER LOADER") << "Error compiling: " << path << ":\n" << module.GetErrorMessage();
			throw new grail::exceptions::RuntimeException("Shader Compilation Failed");
		}

		std::vector<uint32_t> result(module.cbegin(), module.cend());

		moduleCreateInfo.codeSize = result.size() * sizeof(uint32_t);
		moduleCreateInfo.pCode = (uint32_t*)result.data();
	
		VK_VALIDATE_RESULT(vkCreateShaderModule(VulkanContext::mainGPU.device, &moduleCreateInfo, nullptr, &shaderModule));
	}

	return shaderModule;
}

grail::Shader* grail::ShaderLoader::loadSPIRVShader(std::string path, std::vector<std::string>& defines) {
	std::string extension = grail::FileIO::getExtension(path);

	shaderc_shader_kind shaderKind;

	if (grail::utils::string::equalsCaseInsensitive(extension, "frag"))
		shaderKind = shaderc_shader_kind::shaderc_fragment_shader;
	else if (grail::utils::string::equalsCaseInsensitive(extension, "vert"))
		shaderKind = shaderc_shader_kind::shaderc_vertex_shader;
	else if (grail::utils::string::equalsCaseInsensitive(extension, "tcs"))
		shaderKind = shaderc_shader_kind::shaderc_tess_control_shader;
	else if (grail::utils::string::equalsCaseInsensitive(extension, "tes"))
		shaderKind = shaderc_shader_kind::shaderc_tess_evaluation_shader;
	else if (grail::utils::string::equalsCaseInsensitive(extension, "geom"))
		shaderKind = shaderc_shader_kind::shaderc_geometry_shader;
	else if (grail::utils::string::equalsCaseInsensitive(extension, "comp"))
		shaderKind = shaderc_shader_kind::shaderc_compute_shader;
	else
		shaderKind = shaderc_shader_kind::shaderc_glsl_infer_from_source;

	char* contents;
	uint32_t size;
	grail::FileIO::readBinaryFile(path, &contents, &size);
	std::string shaderCode = std::string(contents, size);
	delete[] contents;

	Shader* shader = new Shader();
	shader->identifier = path;
	shader->stage = kindToStage[shaderKind];
	shader->source = shaderCode;

	//GRAIL_LOG(INFO, "SRC") << shaderCode;

	shader->compilePermutation(defines);

	return shader;
}

grail::Shader * grail::ShaderLoader::loadSPIRVShader(std::string path) {
	std::vector<std::string> list;
	return loadSPIRVShader(path, list);
}

grail::ShaderPermutation* grail::ShaderLoader::parsePermutation(Shader* base, std::vector<std::string>& defines, bool remapDescriptors) {
	std::string defs = "";
	for (std::string& define : defines) {
		defs += define + " ";
	}

	//GRAIL_LOG(INFO, "SHADER LOADER") << "Compiling " << base->identifier << " defines : {\n " << defs << "\n}";

	// TEST
	/*{
		std::string optmized;
		std::string nonOptimized;

		{
			shaderc::Compiler glslCompiler;
			shaderc::CompileOptions glslOptions;
			glslOptions.SetTargetEnvironment(shaderc_target_env_vulkan, 0);
			glslOptions.SetSourceLanguage(shaderc_source_language_glsl);
			glslOptions.SetOptimizationLevel(shaderc_optimization_level_performance);

			shaderc::SpvCompilationResult glslCompilation = glslCompiler.CompileGlslToSpv(base->source, stageToKind[base->stage], "main", glslOptions);

			std::vector<uint32_t> spirvBinary(glslCompilation.begin(), glslCompilation.end());

			spirv_cross::CompilerGLSL comp(spirvBinary);
			spirv_cross::CompilerGLSL::Options opt = comp.get_common_options();
			opt.vulkan_semantics = true;
			comp.set_common_options(opt);

			optmized = comp.compile();
		}

		{
			shaderc::Compiler glslCompiler;
			shaderc::CompileOptions glslOptions;
			glslOptions.SetTargetEnvironment(shaderc_target_env_vulkan, 0);
			glslOptions.SetSourceLanguage(shaderc_source_language_glsl);
			//glslOptions.SetOptimizationLevel(shaderc_optimization_level_performance);

			shaderc::SpvCompilationResult glslCompilation = glslCompiler.CompileGlslToSpv(base->source, stageToKind[base->stage], "main", glslOptions);

			std::vector<uint32_t> spirvBinary(glslCompilation.begin(), glslCompilation.end());

			spirv_cross::CompilerGLSL comp(spirvBinary);
			spirv_cross::CompilerGLSL::Options opt = comp.get_common_options();
			opt.vulkan_semantics = true;
			comp.set_common_options(opt);

			nonOptimized = comp.compile();
		}

		GRAIL_LOG(DEBUG, "SHADER TEST") << "\nBASE:\n" << base->source << "\n\nNON-OPTIMIZED:\n" << nonOptimized << "\n\nOPTIMIZED:\n" << optmized << "\n-----------------------------------";
	}*/
	
	shaderc::Compiler glslCompiler;
	shaderc::CompileOptions glslOptions;
	glslOptions.SetTargetEnvironment(shaderc_target_env_vulkan, 0);
	glslOptions.SetSourceLanguage(shaderc_source_language_glsl);
	//glslOptions.SetOptimizationLevel(shaderc_optimization_level_performance);
	for (std::string& define : defines) {
		glslOptions.AddMacroDefinition(define);
	}

	if (platform::getPlatformType() == platform::PlatformType::MOBILE) {
		glslOptions.AddMacroDefinition("PLATFORM_MOBILE");
	}
	else if(platform::getPlatformType() == platform::PlatformType::DESKTOP) {
		glslOptions.AddMacroDefinition("PLATFORM_DESKTOP");
	}

	if (platform::getPlatform() == platform::Platform::TYPE_WINDOWS) {
		glslOptions.AddMacroDefinition("PLATFORM_WINDOWS");
	}

	if (platform::getPlatform() == platform::Platform::TYPE_ANDROID) {
		glslOptions.AddMacroDefinition("PLATFORM_ANDROID");
	}

	if (platform::getPlatform() == platform::Platform::TYPE_LINUX) {
		glslOptions.AddMacroDefinition("PLATFORM_LINUX");
	}

	shaderc::SpvCompilationResult glslCompilation = glslCompiler.CompileGlslToSpv(base->source, stageToKind[base->stage], "main", glslOptions);

	if (glslCompilation.GetCompilationStatus() != shaderc_compilation_status_success) {
		GRAIL_LOG(ERROR, "SHADER LOADER") << "Error compiling: " << base->identifier << " permutation " << base->permutationMap.size() << ": " << glslCompilation.GetErrorMessage();
		throw new grail::exceptions::RuntimeException("Shader Compilation Failed");
		return nullptr;
	}

	ShaderPermutation* permutation = new ShaderPermutation();
	permutation->stage = base->stage;
	permutation->defines = defines;
	
	std::vector<uint32_t> spirvBinary(glslCompilation.begin(), glslCompilation.end());

	spirv_cross::CompilerGLSL comp(spirvBinary);
	spirv_cross::ShaderResources resources = comp.get_shader_resources();

	std::stringstream log;

	log << "\n" << base->identifier << "[" << base->permutationMap.size() << "] " << "{\n";
	log << "\tType: " << static_cast<uint32_t>(base->stage) << ",\n";
	
	//GATHER INPUTS
	log << "\tInputs {\n";
	for (const spirv_cross::Resource& res : resources.stage_inputs) {
		IOShaderVar var = IOShaderVar();
		parseVariable(res, comp, var);

		uint32_t location = comp.get_decoration(res.id, spv::DecorationLocation);
		var.location = location;

		permutation->inputs.push_back(var);

		log << "\t\t" << var.name << " -> base_type: " << static_cast<uint32_t>(var.baseType) << ", is_array: " << (var.dimensions.size() == 0 ? "false" : "true") << ", dimensions: [ ";
		for (uint32_t size : var.dimensions) {
			log << size << " ";
		}
		if (var.dimensions.size() == 0) log << " ";
		log << "], type: " << static_cast<uint32_t>(var.type) << ", rows: " << var.rows << ", columns: " << var.columns << ";\n";
	}
	log << "\t}\n\n";

	//GATHER OUTPUTS
	log << "\tOutputs {\n";
	for (const spirv_cross::Resource& res : resources.stage_outputs) {
		IOShaderVar var = IOShaderVar();
		parseVariable(res, comp, var);

		uint32_t location = comp.get_decoration(res.id, spv::DecorationLocation);
		var.location = location;

		permutation->outputs.push_back(var);

		log << "\t\t" << var.name << " -> base_type: " << static_cast<uint32_t>(var.baseType) << ", is_array: " << (var.dimensions.size() == 0 ? "false" : "true") << ", dimensions: [ ";
		for (uint32_t size : var.dimensions) {
			log << size << " ";
		}
		if (var.dimensions.size() == 0) log << " ";
		log << "], type: " << static_cast<uint32_t>(var.type) << ", rows: " << var.rows << ", columns: " << var.columns << ";\n";
	}
	log << "\t}\n\n";

	//GATHER UNIFORM BUFFERS
	log << "\tUniform Buffers {\n";
	for (const spirv_cross::Resource& res : resources.uniform_buffers) {
		BufferBlock block = BufferBlock();
		
		auto& type = comp.get_type(res.base_type_id);

		std::string name = comp.get_name(res.id);
		uint32_t memberCount = type.member_types.size();
		uint32_t size = comp.get_declared_struct_size(type);
		uint32_t set = comp.get_decoration(res.id, spv::DecorationDescriptorSet);
		uint32_t binding = comp.get_decoration(res.id, spv::DecorationBinding);

		block.name = name;
		block.memberCount = memberCount;
		block.size = size;
		block.set = set;
		block.binding = binding;

		log << "\t\t" << name << " {\n";
		log << "\t\t\t" << "member_count:" << memberCount << ",\n";
		log << "\t\t\t" << "size:" << size << ",\n";
		log << "\t\t\t" << "set:" << set << ",\n";
		log << "\t\t\t" << "binding:" << binding << ",\n\n";
		log << "\t\t\t" << "Members {\n";

		for (uint32_t i = 0; i < memberCount; i++) {
			auto& member_type = comp.get_type(type.member_types[i]);

			BufferBlockShaderVar var = BufferBlockShaderVar();
			var.name = comp.get_member_name(type.self, i);
			var.baseType = static_cast<ShaderVarBaseTypeT>(comp.get_type(type.member_types[i]).basetype);
			var.dimensions = member_type.array;
			var.bufferName = name;
			var.size = comp.get_declared_struct_member_size(type, i);
			var.offset = comp.type_struct_member_offset(type, i);
			var.rows = member_type.vecsize;
			var.columns = member_type.columns;

			if (var.rows == 1) var.type = ShaderVarTypeT::SCALAR;
			if (var.rows > 1) var.type = ShaderVarTypeT::VECTOR;
			if (var.columns > 1) var.type = ShaderVarTypeT::MATRIX;

			if (!var.dimensions.empty()) {
				var.stride = comp.type_struct_member_array_stride(type, i);
			}

			if (member_type.columns > 1) {
				//var.stride = comp.type_struct_member_matrix_stride(type, 1);
			}

			block.variables.push_back(var);

			log << "\t\t\t\t" << var.name << " -> base_type: " << static_cast<uint32_t>(var.type) << ", is_array: " << (var.dimensions.size() == 0 ? "false" : "true") << ", dimensions: [ ";
			for (uint32_t size : var.dimensions) {
				log << size << " ";
			}
			if (var.dimensions.size() == 0) log << " ";
			log << "], size: " << var.size << ", offset: " << var.offset << ", stride: " << var.stride << ", type: " << static_cast<uint32_t>(var.type) << ", rows : " << var.rows << ", columns : " << var.columns << "\n";
		}

		permutation->uniformBuffers.push_back(block);

		log << "\t\t\t}\n";
		log << "\t\t}\n";
	}
	log << "\t}\n\n";

	//GATHER PUSH CONSTANTS
	log << "\tPush Constants {\n";
	for (const spirv_cross::Resource& res : resources.push_constant_buffers) {
		BufferBlock block = BufferBlock();

		auto& type = comp.get_type(res.base_type_id);

		std::string name = res.name;
		uint32_t memberCount = type.member_types.size();
		uint32_t size = comp.get_declared_struct_size(type);
		uint32_t set = comp.get_decoration(res.id, spv::DecorationDescriptorSet);
		uint32_t binding = comp.get_decoration(res.id, spv::DecorationBinding);

		block.name = name;
		block.memberCount = memberCount;
		block.size = size;
		block.set = set;
		block.binding = binding;

		log << "\t\t" << name << " {\n";
		log << "\t\t\t" << "member_count:" << memberCount << ",\n";
		log << "\t\t\t" << "size:" << size << ",\n";
		log << "\t\t\t" << "set:" << set << ",\n";
		log << "\t\t\t" << "binding:" << binding << ",\n\n";
		log << "\t\t\t" << "Members {\n";

		for (uint32_t i = 0; i < memberCount; i++) {
			auto& member_type = comp.get_type(type.member_types[i]);

			BufferBlockShaderVar var = BufferBlockShaderVar();
			var.name = comp.get_member_name(type.self, i);
			var.baseType = static_cast<ShaderVarBaseTypeT>(comp.get_type(type.member_types[i]).basetype);
			var.dimensions = member_type.array;
			var.bufferName = name;
			var.size = comp.get_declared_struct_member_size(type, i);
			var.offset = comp.type_struct_member_offset(type, i);
			var.rows = member_type.vecsize;
			var.columns = member_type.columns;

			if (var.rows == 1) var.type = ShaderVarTypeT::SCALAR;
			if (var.rows > 1) var.type = ShaderVarTypeT::VECTOR;
			if (var.columns > 1) var.type = ShaderVarTypeT::MATRIX;

			if (!var.dimensions.empty()) {
				var.stride = comp.type_struct_member_array_stride(type, i);
			}

			if (member_type.columns > 1) {			
				var.stride = comp.type_struct_member_matrix_stride(type, i);
			}

			block.variables.push_back(var);

			log << "\t\t\t\t" << var.name << " -> base_type: " << static_cast<uint32_t>(var.type) << ", is_array: " << (var.dimensions.size() == 0 ? "false" : "true") << ", dimensions: [ ";
			for (uint32_t size : var.dimensions) {
				log << size << " ";
			}
			if (var.dimensions.size() == 0) log << " ";
			log << "] size: " << var.size << ", offset: " << var.offset << ", stride: " << var.stride << ", type: " << static_cast<uint32_t>(var.type) << ", rows : " << var.rows << ", columns : " << var.columns << "\n";
		}

		permutation->pushConstants = block;

		log << "\t\t\t}\n";
		log << "\t\t}\n";
	}
	log << "\t}\n\n";

	//GATHER STORAGE IMAGES
	log << "\Storage Images {\n";
	for (const spirv_cross::Resource& res : resources.storage_images) {
		ImageShaderVar var = ImageShaderVar();
		parseVariable(res, comp, var);

		auto& base_type_id = comp.get_type(res.base_type_id);

		uint32_t binding = comp.get_decoration(res.id, spv::DecorationBinding);
		uint32_t set = comp.get_decoration(res.id, spv::DecorationDescriptorSet);

		var.resourceID = res.id;
		var.set = set;
		var.binding = binding;
		var.dim = static_cast<ShaderImageDim>(base_type_id.image.dim);
		var.arrayed = base_type_id.image.arrayed;

		permutation->storageImages.push_back(var);

		log << "\t\t" << var.name << " -> set: " << set << ", binding: " << binding << ", base_type: " << static_cast<uint32_t>(var.baseType) << ", is_array: " << (var.dimensions.size() == 0 ? "false" : "true") << ", dimensions: [ ";
		for (uint32_t size : var.dimensions) {
			log << size << " ";
		}
		if (var.dimensions.size() == 0) log << " ";
		log << "], type: " << static_cast<uint32_t>(var.type) << ", rows: " << var.rows << ", columns: " << var.columns << ", dim:" << static_cast<uint32_t>(var.dim) << ", arrayed: " << var.arrayed << "\n";
	}
	log << "\t}\n";

	// Gather storage buffers
	log << "\tStorage Buffers {\n";
	for (const spirv_cross::Resource& res : resources.storage_buffers) {
		BufferBlock block = BufferBlock();

		auto& type = comp.get_type(res.base_type_id);

		std::string name = comp.get_name(res.id);
		uint32_t memberCount = type.member_types.size();
		uint32_t size = comp.get_declared_struct_size(type);
		uint32_t set = comp.get_decoration(res.id, spv::DecorationDescriptorSet);
		uint32_t binding = comp.get_decoration(res.id, spv::DecorationBinding);

		block.name = name;
		block.memberCount = memberCount;
		block.size = size;
		block.set = set;
		block.binding = binding;

		log << "\t\t" << name << " {\n";
		log << "\t\t\t" << "member_count:" << memberCount << ",\n";
		log << "\t\t\t" << "size:" << size << ",\n";
		log << "\t\t\t" << "set:" << set << ",\n";
		log << "\t\t\t" << "binding:" << binding << ",\n\n";
		log << "\t\t\t" << "Members {\n";

		for (uint32_t i = 0; i < memberCount; i++) {
			auto& member_type = comp.get_type(type.member_types[i]);

			BufferBlockShaderVar var = BufferBlockShaderVar();
			var.name = comp.get_member_name(type.self, i);
			var.baseType = static_cast<ShaderVarBaseTypeT>(comp.get_type(type.member_types[i]).basetype);
			var.dimensions = member_type.array;
			var.bufferName = name;
			var.size = comp.get_declared_struct_member_size(type, i);
			var.offset = comp.type_struct_member_offset(type, i);
			var.rows = member_type.vecsize;
			var.columns = member_type.columns;

			if (var.rows == 1) var.type = ShaderVarTypeT::SCALAR;
			if (var.rows > 1) var.type = ShaderVarTypeT::VECTOR;
			if (var.columns > 1) var.type = ShaderVarTypeT::MATRIX;

			if (!var.dimensions.empty()) {
				var.stride = comp.type_struct_member_array_stride(type, i);
			}

			if (member_type.columns > 1) {
				//var.stride = comp.type_struct_member_matrix_stride(type, 1);
			}

			block.variables.push_back(var);

			log << "\t\t\t\t" << var.name << " -> base_type: " << static_cast<uint32_t>(var.type) << ", is_array: " << (var.dimensions.size() == 0 ? "false" : "true") << ", dimensions: [ ";
			for (uint32_t size : var.dimensions) {
				log << size << " ";
			}
			if (var.dimensions.size() == 0) log << " ";
			log << "], size: " << var.size << ", offset: " << var.offset << ", stride: " << var.stride << ", type: " << static_cast<uint32_t>(var.type) << ", rows : " << var.rows << ", columns : " << var.columns << "\n";
		}

		permutation->storageBuffers.push_back(block);

		log << "\t\t\t}\n";
		log << "\t\t}\n";
	}
	log << "\t}\n\n";



	//GATHER IMAGES
	log << "\tImages {\n";

	bool imageDescriptorsClashing = false;
	for (const spirv_cross::Resource& res : resources.sampled_images) {
		ImageShaderVar var = ImageShaderVar();
		parseVariable(res, comp, var);

		auto& base_type_id = comp.get_type(res.base_type_id);

		uint32_t binding = comp.get_decoration(res.id, spv::DecorationBinding);
		uint32_t set = comp.get_decoration(res.id, spv::DecorationDescriptorSet);

		var.resourceID = res.id;
		var.set = set;
		var.binding = binding;
		var.dim = static_cast<ShaderImageDim>(base_type_id.image.dim);
		var.arrayed = base_type_id.image.arrayed;

		permutation->sampledImages.push_back(var);

		for (ImageShaderVar& existing : permutation->sampledImages) {
			if (var.set == existing.set && existing.binding == var.binding) {
				imageDescriptorsClashing = true;
			}
		}

		log << "\t\t" << var.name << " -> set: " << set << ", binding: " << binding << ", base_type: " << static_cast<uint32_t>(var.baseType) << ", is_array: " << (var.dimensions.size() == 0 ? "false" : "true") << ", dimensions: [ ";
		for (uint32_t size : var.dimensions) {
			log << size << " ";
		}
		if (var.dimensions.size() == 0) log << " ";
		log << "], type: " << static_cast<uint32_t>(var.type) << ", rows: " << var.rows << ", columns: " << var.columns << ", dim:" << static_cast<uint32_t>(var.dim) << ", arrayed: " << var.arrayed << "\n";
	}
	log << "\t}\n";


	std::sort(permutation->sampledImages.begin(), permutation->sampledImages.end(), [](ImageShaderVar& a, ImageShaderVar& b) {
		return a.binding < b.binding;
	});

	if (remapDescriptors && imageDescriptorsClashing) {
			if (permutation->sampledImages.size() > 1) {
				ImageShaderVar* lastVar = &permutation->sampledImages[0];

				for (int i = 1; i < permutation->sampledImages.size(); i++) {
					ImageShaderVar* currentVar = &permutation->sampledImages[i];

					if ((currentVar->binding - lastVar->binding) != 1) {
						currentVar->binding = lastVar->binding + 1;

						comp.set_decoration(currentVar->resourceID, spv::DecorationBinding, currentVar->binding);
					}

					lastVar = &permutation->sampledImages[i];
				}
			}

			spirv_cross::CompilerGLSL::Options opt = comp.get_common_options();
			opt.vulkan_semantics = true;
			comp.set_common_options(opt);

			std::string result = comp.compile();

			shaderc::SpvCompilationResult recompilation = glslCompiler.CompileGlslToSpv(result, stageToKind[base->stage], "main", glslOptions);
			std::vector<uint32_t> newBinary(recompilation.begin(), recompilation.end());

			if (recompilation.GetCompilationStatus() != shaderc_compilation_status_success) {
				GRAIL_LOG(ERROR, "SHADER LOADER") << "Error compiling: " << base->identifier << " permutation " << base->permutationMap.size() << ": " << recompilation.GetErrorMessage();
				throw new grail::exceptions::RuntimeException("Shader Compilation Failed");
				return nullptr;
			}

			VkShaderModuleCreateInfo moduleCreateInfo = {};
			moduleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
			moduleCreateInfo.codeSize = newBinary.size() * sizeof(uint32_t);
			moduleCreateInfo.pCode = (uint32_t*)newBinary.data();
			VK_VALIDATE_RESULT(vkCreateShaderModule(VulkanContext::mainGPU.device, &moduleCreateInfo, nullptr, &permutation->handle));
	}
	else {
		VkShaderModuleCreateInfo moduleCreateInfo = {};
		moduleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
		moduleCreateInfo.codeSize = spirvBinary.size() * sizeof(uint32_t);
		moduleCreateInfo.pCode = (uint32_t*)spirvBinary.data();
		VK_VALIDATE_RESULT(vkCreateShaderModule(VulkanContext::mainGPU.device, &moduleCreateInfo, nullptr, &permutation->handle));
	}

	log << "}";
	//GRAIL_LOG(INFO, "SHADER") << log.str();

	return permutation;
}

void grail::ShaderLoader::parseVariable(const spirv_cross::Resource& res, spirv_cross::Compiler& comp, grail::ShaderVarT& var) {
	// base type, ignoring array mods
	auto& base_type_id = comp.get_type(res.base_type_id);
	// actual type, including array mods
	auto& type_id = comp.get_type(res.type_id);

	std::string name = res.name;
	ShaderVarBaseTypeT baseType = static_cast<ShaderVarBaseTypeT>(base_type_id.basetype);

	var.name = name;
	var.baseType = baseType;
	var.dimensions = type_id.array;
	var.rows = base_type_id.vecsize;
	var.columns = base_type_id.columns;

	if (var.rows == 1) var.type = ShaderVarTypeT::SCALAR;
	if (var.rows > 1) var.type = ShaderVarTypeT::VECTOR;
	if (var.columns > 1) var.type = ShaderVarTypeT::MATRIX;
}
