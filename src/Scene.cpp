#include "Scene.h"
#include "SceneNodeComponents.h"
#include "SceneNode.h"
#include "Intersector.h"

#include "Utils.h"
#include "Mesh.h"

#include "Script.h"
#include <angelscript.h>

grail::SceneNode* grail::Scene::rootNode = nullptr;
std::vector<grail::SceneNode*> grail::Scene::nodes = std::vector<grail::SceneNode*>();
std::unordered_map<std::string, grail::SceneNode*> grail::Scene::nodeMap = std::unordered_map<std::string, SceneNode*>();
entt::registry grail::Scene::entityRegistry = entt::registry();
std::vector<grail::SceneNode*> grail::Scene::removeQueue = std::vector<grail::SceneNode*>();


void grail::Scene::update(float delta) {
	rootNode->updateTransform();
	
	getNodesWithComponents<nodeComponents::ScriptComponent>().each([delta](auto& c) {
		c.callUpdateFunc(delta);
	});

	processNodes();
}

void grail::Scene::processNodes() {
	for (SceneNode* node : removeQueue) {
		deleteNode(node);
	}

	removeQueue.clear();
}

void grail::Scene::start() {
	getNodesWithComponents<nodeComponents::ScriptComponent>().each([](auto& c) {
		c.callStartFunc();
	});
}

grail::SceneNode* grail::Scene::getRootNode() {
	return rootNode;
}

grail::SceneNode* grail::Scene::getNode(const std::string& name) {
	auto item = nodeMap.find(name);

	if (item != nodeMap.end())
		return item->second;

	return nullptr;
}

grail::SceneNode* grail::Scene::createNode(const std::string& name) {
	SceneNode* node = nullptr;

	std::string assignedName = name;

	if (name.length() == 0) {
		assignedName = "New Node";
	}

	if (getNode(assignedName) != nullptr) {
		uint32_t index = 0;
		std::stringstream rename;
		while (true) {
			rename.str("");

			rename << assignedName << "_" << ++index;

			if (getNode(rename.str()) == nullptr) {
				break;
			}
		}

		assignedName = rename.str();
	}

	node = new SceneNode(assignedName);
	rootNode->addChild(node);

	addNode(node);

	node->updateTransform(true);

	return node;
}

grail::SceneNode * grail::Scene::cloneNode(SceneNode * node) {
	if (node == nullptr) return nullptr;

	SceneNode* base = cloneInternal(node->parent, node);

	base->updateTransform();

	return base;
}

void grail::Scene::removeNode(SceneNode* node) {
	//deleteNode(node);

	if(node)
		removeQueue.push_back(node);
}

void grail::Scene::renameNode(SceneNode * node, const std::string & newName) {
	if (node->name.compare(newName) == 0) return;

	std::string name = newName;

	if (getNode(name)) {
		name = assignNewName(name);
	}

	node->name = name;
	nodeMap.erase(node->name);
	nodeMap.insert({ name, node });
}

void grail::Scene::removeNode(const std::string& name) {
	SceneNode* node = getNode(name);

	if (node) {
		removeNode(node);
	}
}

void grail::Scene::assignComponentByName(SceneNode * node, const std::string & componentName) {
	if (getComponentByName(node, componentName) == nullptr) {
		nodeComponents::createComponentFor(componentName, node->entity, entityRegistry);
		getComponentByName(node, componentName)->node = node;
	}
}

grail::nodeComponents::NodeComponent * grail::Scene::getComponentByName(SceneNode * node, const std::string & componentName) {
	return nodeComponents::retrieveComponent(componentName, node->entity, entityRegistry);
}

grail::SceneNode * grail::Scene::castRay(glm::vec3 rayOrigin, glm::vec3 rayDirection, glm::vec3& intersectionPoint) {
	SceneNode* intersectedNode = nullptr;
	glm::vec3 closestIntersection = glm::vec3();

	Scene::getNodesWithComponents<nodeComponents::Render>().each([=, &intersectedNode, &closestIntersection](auto& c) {
		if (!c.mesh) return;
		


		glm::vec3 point;
		bool intersection = Intersector::intersectMeshRay(rayOrigin, rayDirection, c.mesh, c.node->transform.getWorldMatrix(), point);

		if (intersection && intersectedNode == nullptr) {
			intersectedNode = c.node;
			closestIntersection = point;
		}
		else if (intersection && intersectedNode != nullptr) {
			if (glm::distance(rayOrigin, point) <= glm::distance(rayOrigin, closestIntersection)) {
				intersectedNode = c.node;
				closestIntersection = point;
			}
		}
	});

	intersectionPoint = closestIntersection;

	return intersectedNode;
}

grail::SceneNode* grail::Scene::castRayOptimized(glm::vec3 rayOrigin, glm::vec3 rayDirection, glm::vec3& intersectionPoint) {
	SceneNode* intersectedNode = nullptr;
	glm::vec3 closestIntersection = glm::vec3();

	Scene::getNodesWithComponents<nodeComponents::Render>().each([=, &intersectedNode, &closestIntersection](auto& c) {
		if (!c.mesh) return;
		
		bool aabbCheck = true;

		if (c.mesh->boundsMin == glm::vec3(FLT_MAX) || c.mesh->boundsMax == glm::vec3(-FLT_MAX)) aabbCheck = false;
		// r.dir is unit direction vector of ray
		
		if (aabbCheck) {
			glm::vec3 dirfrac = 1.0f / rayDirection;
			glm::vec3 lb = c.node->transform.getWorldBoundsMin();
			glm::vec3 rt = c.node->transform.getWorldBoundsMax();


			// lb is the corner of AABB with minimal coordinates - left bottom, rt is maximal corner
			// r.org is origin of ray
			float t1 = (lb.x - rayOrigin.x) * dirfrac.x;
			float t2 = (rt.x - rayOrigin.x) * dirfrac.x;
			float t3 = (lb.y - rayOrigin.y) * dirfrac.y;
			float t4 = (rt.y - rayOrigin.y) * dirfrac.y;
			float t5 = (lb.z - rayOrigin.z) * dirfrac.z;
			float t6 = (rt.z - rayOrigin.z) * dirfrac.z;

			float tmin = glm::max(glm::max(glm::min(t1, t2), glm::min(t3, t4)), glm::min(t5, t6));
			float tmax = glm::min(glm::min(glm::max(t1, t2), glm::max(t3, t4)), glm::max(t5, t6));

			// if tmax < 0, ray (line) is intersecting AABB, but the whole AABB is behind us
			if (tmax < 0) {
				return;
			}

			// if tmin > tmax, ray doesn't intersect AABB
			if (tmin > tmax) {
				return;
			}
		}


		glm::vec3 point;
		bool intersection = Intersector::intersectMeshRay(rayOrigin, rayDirection, c.mesh, c.node->transform.getWorldMatrix(), point);

		if (intersection && intersectedNode == nullptr) {
			intersectedNode = c.node;
			closestIntersection = point;
		}
		else if (intersection && intersectedNode != nullptr) {
			if (glm::distance(rayOrigin, point) <= glm::distance(rayOrigin, closestIntersection)) {
				intersectedNode = c.node;
				closestIntersection = point;
			}
		}
	});

	intersectionPoint = closestIntersection;

	return intersectedNode;
}

void grail::Scene::clear() {
	deleteNode(rootNode);
}

const std::vector<grail::SceneNode*> grail::Scene::getAllNodes() {
	return nodes;
}

void grail::Scene::addNode(SceneNode* node) {
	node->entity = entityRegistry.create();

	nodes.push_back(node);
	nodeMap.insert({ node->name, node });
}

void grail::Scene::deleteNode(SceneNode* node) {
	if (node != rootNode) {

		if (node->isPrefab()) {

			for (std::string name : node->getPrefabClones()) {
				SceneNode* node = getNode(name);

				if (!node) continue;
				
				node->derivedPrefab = nullptr;
			}
			node->getPrefabClones().clear();
		}

		if (node->getDerivedPrefab() != nullptr) {
			std::vector<std::string> clones = node->getDerivedPrefab()->getPrefabClones();
			clones.erase(std::find(clones.begin(), clones.end(), node->name));
		}

		for (auto& item : nodeComponents::detail::componentAssignMap) {
			nodeComponents::NodeComponent* comp = getComponentByName(node, item.first);

			if (comp) {
				comp->dispose();
			}

			//delete comp;
		}

		entityRegistry.destroy(node->entity);
	}

	for (SceneNode* child : node->getChildren()) {
		deleteNode(child);
	}

	if (node->getParent()) {
		node->getParent()->removeChild(node);
	}

	if (node != rootNode) {
		nodes.erase(std::remove(nodes.begin(), nodes.end(), node), nodes.end());
		nodeMap.erase(node->name);

		delete node;
	}
}

grail::SceneNode * grail::Scene::cloneInternal(SceneNode * parent, SceneNode * node) {
	SceneNode* copy = createCopy(node);

	if (copy == nullptr) return nullptr;

	if (parent != getRootNode()) {
		parent->addChild(copy);
	}

	/*if (parent == getRootNode()) {
		getRootNode()->addChild(copy);
	}*/

	copy->updateTransform(true);
	node->transform.copyTo(copy->transform);

	for (SceneNode* child : node->getChildren()) {
		SceneNode* childCopy = cloneInternal(copy, child);
	}

	return copy;
}

grail::SceneNode* grail::Scene::createCopy(SceneNode * src) {
	if (src == nullptr) return nullptr;

	std::string assignedName = src->name;

	if (getNode(assignedName) != nullptr) {
		std::stringstream rename;
		uint32_t index = 0;
		while (true) {
			rename.str("");

			size_t lastIndex = assignedName.find_last_of("_");

			if (lastIndex != std::string::npos) {
				std::string sub = assignedName.substr(lastIndex + 1);
				if (utils::string::isStringNumeric(sub)) {
					size_t newIndex = std::stoi(sub);

					while (true) {
						newIndex += 1;

						rename.str("");
						rename << assignedName.substr(0, lastIndex) << "_" << newIndex;

						if (getNode(rename.str()) == nullptr) {
							break;
						}
					}
				}
				else {
					rename << assignedName << "_" << ++index;
				}
			}
			else {
				rename << assignedName << "_" << ++index;
			}

			if (getNode(rename.str()) == nullptr) {
				break;
			}
		}

		assignedName = rename.str();
	}

	SceneNode* dst = createNode(assignedName);

	for (auto& val : nodeComponents::detail::componentRetrieveMap) {
		if (src->getComponentFromMap(val.first)) {
			dst->createComponentFromMap(val.first);

			nodeComponents::NodeComponent* newComponent = dst->getComponentFromMap(val.first);

			if (newComponent) {
				newComponent->node = dst;
				newComponent->copy(src->getComponentFromMap(val.first));
			}
		}
	}

	if (src->isPrefab()) {
		dst->derivedPrefab = src;
		src->prefabClones.push_back(dst->name);
	}

	return dst;
}

std::string grail::Scene::assignNewName(const std::string& input) {
	std::stringstream rename;
	uint32_t index = 0;
	while (true) {
		rename.str("");

		size_t lastIndex = input.find_last_of("_");

		if (lastIndex != std::string::npos) {
			std::string sub = input.substr(lastIndex + 1);
			if (utils::string::isStringNumeric(sub)) {
				size_t newIndex = std::stoi(sub);

				while (true) {
					newIndex += 1;

					rename.str("");
					rename << input.substr(0, lastIndex) << "_" << newIndex;

					if (getNode(rename.str()) == nullptr) {
						break;
					}
				}
			}
			else {
				rename << input << "_" << ++index;
			}
		}
		else {
			rename << input << "_" << ++index;
		}

		if (getNode(rename.str()) == nullptr) {
			break;
		}
	}

	return rename.str();
}

void grail::Scene::Initialize() {
	rootNode = new SceneNode("New Scene");
	rootNode->entity = entityRegistry.create();

	nodes.push_back(rootNode);
	nodeMap.insert({ rootNode->name, rootNode });
}
