#include "DescriptorSet.h"

void grail::DescriptorLayout::addDescriptor(DescriptorBindingDescription description) {
	if (finalized) return;

	if (description.identifier.length() == 0) {
		description.identifier = std::to_string(description.set) + "_" + std::to_string(description.index);
	}

	bool containsBinding = false;
	for (DescriptorBindingDescription& desc : descriptions) {
		if (desc.set == description.set && desc.index == description.index) {
			if (desc.identifier.compare(description.identifier) == 0) {
				if (desc.type == description.type) {
					containsBinding = true;
					desc.shaderStageFlags |= description.shaderStageFlags;
				}
				else {
					throw new exceptions::RuntimeException("Descriptor layout bindings clash");
				}
			}
			else {
				throw new exceptions::RuntimeException("Descriptor layout bindings clash");
			}
		}
	}

	if(!containsBinding)
		this->descriptions.push_back(description);

	this->setMap.push_back(description.set);	
	this->setMap.unique();

	std::sort(this->descriptions.begin(), this->descriptions.end(), [](DescriptorBindingDescription& a, DescriptorBindingDescription& b) {
		return (a.set < b.set) || ((a.set == b.set) && (a.index < b.index));
	});
}

void grail::DescriptorLayout::addPushConstant(PushConstantRange range) {
	if (finalized) return;

	bool containsPushConstant = false;
	for (PushConstantRange& con : pushConstants) {
		if (range.size == con.size && range.offset == con.offset) {
			containsPushConstant = true;
			range.stageFlags |= con.stageFlags;
		}
	}

	if(!containsPushConstant)
		this->pushConstants.push_back(range);
}

void grail::DescriptorLayout::finalize() {
	if (finalized) return;

	finalized = true;
}

void grail::DescriptorBinding::setBinding(void* resource, void* binding) {
	if (resourceType == DescriptorResourceType::IMAGE) {
		VkDescriptorImageInfo* info = static_cast<VkDescriptorImageInfo*>(binding);
		imageInfo.imageLayout = info->imageLayout;
		imageInfo.imageView = info->imageView;
		imageInfo.sampler = info->sampler;

		writeSet.pImageInfo = &imageInfo;
	}
	else if (resourceType == DescriptorResourceType::BUFFER) {
		VkDescriptorBufferInfo* info = static_cast<VkDescriptorBufferInfo*>(binding);
		bufferInfo.buffer = info->buffer;
		bufferInfo.offset = info->offset;
		bufferInfo.range = info->range;

		writeSet.pBufferInfo = &bufferInfo;
	}
	else {
		throw new grail::exceptions::RuntimeException("Unknown binding resource type!");
	}

	this->resource = resource;
	this->descriptor = binding;

	this->dirty = true;
	this->parentSet->dirty = true;
	this->parentSet->parentGroup->dirty = true;
}

//grail::DescriptorBinding::DescriptorBinding() { }

grail::DescriptorGroup::DescriptorGroup(DescriptorLayout& layout) {
	initialize(layout);
}

grail::DescriptorGroup::DescriptorGroup() {
}

void grail::DescriptorGroup::initialize(DescriptorLayout & layout) {
	// This was a stupid idea now that I think about it
/*if (layout.descriptions.size() == 0) {
	throw new grail::exceptions::RuntimeException("Descriptor layout cannot be empty!");
}*/

	if (pipelineLayout != VK_NULL_HANDLE) {
		return;
	}

	if (!layout.finalized) {
		throw new grail::exceptions::RuntimeException("Descriptor layout has to be finalized!");
	}


	for (PushConstantRange range : layout.pushConstants) {
		pushConstants.push_back(range);
	}


	// If we have any descriptor bindings
	if (layout.descriptions.size() > 0) {
		// First calculate required pool sizes	
		struct Counter {
			uint32_t count = 1;
		};

		std::map<DescriptorBindingType, Counter> typeMap;
		for (DescriptorBindingDescription& description : layout.descriptions) {
			if (typeMap.find(description.type) == typeMap.end()) {
				typeMap.insert({ description.type, Counter{} });
			}
			else {
				typeMap[description.type].count += 1;
			}
		}

		std::vector<VkDescriptorPoolSize> poolSizes;
		for (auto& val : typeMap) {
			VkDescriptorPoolSize size = {};
			size.type = static_cast<VkDescriptorType>(val.first);
			size.descriptorCount = val.second.count;

			poolSizes.push_back(size);
		}

		// Then create the pool
		VkDescriptorPoolCreateInfo poolCreateInfo = {};
		poolCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
		poolCreateInfo.pNext = nullptr;
		poolCreateInfo.flags = 0;
		poolCreateInfo.poolSizeCount = (uint32_t)typeMap.size();
		poolCreateInfo.pPoolSizes = &poolSizes[0];
		poolCreateInfo.maxSets = layout.setMap.size();
		VK_VALIDATE_RESULT(vkCreateDescriptorPool(VulkanContext::mainGPU.device, &poolCreateInfo, nullptr, &pool));
		// Create descriptor sets
		uint32_t currentIndex = UINT32_MAX;
		for (DescriptorBindingDescription& description : layout.descriptions) {
			if (description.set != currentIndex) {
				currentIndex = description.set;

				DescriptorSet set;
				set.setIndex = currentIndex;
				set.parentGroup = this;

				descriptorSets.push_back(set);
			}

			DescriptorSet& set = descriptorSets[descriptorSets.size() - 1];

			DescriptorBinding binding;
			binding.set = set.setIndex;
			binding.bindingIndex = description.index;
			binding.identifier = description.identifier;
			binding.type = description.type;
			binding.shaderStages = description.shaderStageFlags;

			binding.writeSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			binding.writeSet.pNext = nullptr;
			binding.writeSet.descriptorType = static_cast<VkDescriptorType>(binding.type);
			binding.writeSet.dstBinding = binding.bindingIndex;
			binding.writeSet.descriptorCount = 1;

			if (binding.type == DescriptorBindingType::COMBINED_IMAGE_SAMPLER ||
				binding.type == DescriptorBindingType::SAMPLED_IMAGE ||
				binding.type == DescriptorBindingType::STORAGE_IMAGE ||
				// Not sure about this one
				binding.type == DescriptorBindingType::INPUT_ATTACHMENT) {

				binding.resourceType = DescriptorResourceType::IMAGE;
			}

			else if (binding.type == DescriptorBindingType::UNIFORM_TEXEL_BUFFER ||
				binding.type == DescriptorBindingType::STORAGE_TEXEL_BUFFER ||
				binding.type == DescriptorBindingType::UNIFORM_BUFFER ||
				binding.type == DescriptorBindingType::STORAGE_BUFFER ||
				binding.type == DescriptorBindingType::UNIFORM_BUFFER_DYNAMIC ||
				binding.type == DescriptorBindingType::STORAGE_BUFFER_DYNAMIC ||
				binding.type == DescriptorBindingType::INLINE_UNIFORM_BLOCK_EXT) {

				binding.resourceType = DescriptorResourceType::BUFFER;
			}
			else {
				throw new grail::exceptions::RuntimeException("Unknown binding type!");
			}

			// NOTE: not sure
			binding.writeSet.dstArrayElement = 0;
			binding.parentSet = &set;

			set.bindings.insert({ description.identifier, binding });
		}

		// Initialize descriptor sets
		for (DescriptorSet& set : descriptorSets) {
			std::vector<VkDescriptorSetLayoutBinding> layout;

			for (auto& val : set.bindings) {
				VkDescriptorSetLayoutBinding bindingInfo = {};
				bindingInfo.descriptorCount = 1;
				bindingInfo.descriptorType = static_cast<VkDescriptorType>(val.second.type);
				bindingInfo.stageFlags = static_cast<VkShaderStageFlags>(val.second.shaderStages);
				bindingInfo.binding = val.second.bindingIndex;

				layout.push_back(bindingInfo);
			}

			VkDescriptorSetLayoutCreateInfo layoutInfo = {};
			layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
			layoutInfo.pNext = nullptr;
			layoutInfo.flags = 0;
			layoutInfo.bindingCount = (uint32_t)set.bindings.size();
			layoutInfo.pBindings = &layout[0];
			VK_VALIDATE_RESULT(vkCreateDescriptorSetLayout(VulkanContext::mainGPU.device, &layoutInfo, nullptr, &set.setLayout));

			// Allocate descriptor set
			VkDescriptorSetAllocateInfo descriptorSetInfo = {};
			descriptorSetInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
			descriptorSetInfo.pNext = nullptr;
			descriptorSetInfo.descriptorPool = pool;
			descriptorSetInfo.descriptorSetCount = 1;
			descriptorSetInfo.pSetLayouts = &set.setLayout;
			VK_VALIDATE_RESULT(vkAllocateDescriptorSets(VulkanContext::mainGPU.device, &descriptorSetInfo, &set.setHandle));

			for (auto& val : set.bindings) {
				val.second.writeSet.dstSet = set.setHandle;
			}

			descriptorSetHandles.push_back(set.setHandle);
		}
	}

	// Create the pipeline layout
	std::vector<VkDescriptorSetLayout> layouts;
	for (DescriptorSet& set : descriptorSets) {
		layouts.push_back(set.setLayout);
	}

	std::vector<VkPushConstantRange> ranges;

	VkPipelineLayoutCreateInfo pipelineLayoutInfo = {};
	pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipelineLayoutInfo.pNext = nullptr;
	pipelineLayoutInfo.flags = 0;

	if (layouts.size() > 0) {
		pipelineLayoutInfo.setLayoutCount = descriptorSets.size();
		pipelineLayoutInfo.pSetLayouts = &layouts[0];
	}

	if (pushConstants.size() > 0) {
		for (PushConstantRange range : pushConstants) {
			VkPushConstantRange vkRange = {};
			vkRange.offset = range.offset;
			vkRange.size = range.size;
			vkRange.stageFlags = static_cast<VkShaderStageFlags>(range.stageFlags);

			ranges.push_back(vkRange);
		}

		pipelineLayoutInfo.pushConstantRangeCount = ranges.size();
		pipelineLayoutInfo.pPushConstantRanges = &ranges[0];
	}
	else {
		pipelineLayoutInfo.pushConstantRangeCount = 0;
		pipelineLayoutInfo.pPushConstantRanges = nullptr;
	}

	VK_VALIDATE_RESULT(vkCreatePipelineLayout(VulkanContext::mainGPU.device, &pipelineLayoutInfo, nullptr, &pipelineLayout));
}

uint32_t grail::DescriptorGroup::getDescriptorCount() {
	return descriptorSets.size();
}

grail::DescriptorSet& grail::DescriptorGroup::getSet(uint32_t index) {
	return descriptorSets[index];
}

VkDescriptorSet* grail::DescriptorGroup::getHandlePtr() {
	return descriptorSetHandles.data();
}

grail::DescriptorBinding& grail::DescriptorGroup::getBinding(const std::string& identifier) {
	for (DescriptorSet& set : descriptorSets) {
		if (set.bindings.find(identifier) != set.bindings.end()) {
			return set.bindings[identifier];
		}
	}

	throw new exceptions::RuntimeException("Descriptor binding with identifier: " + identifier + " does not exist!");
}

grail::DescriptorBinding* grail::DescriptorGroup::tryGetBinding(const std::string& identifier) {
	for (DescriptorSet& set : descriptorSets) {
		if (set.bindings.find(identifier) != set.bindings.end()) {
			return &set.bindings[identifier];
		}
	}

	return nullptr;
}

grail::PushConstantRange grail::DescriptorGroup::getPushConstant(uint32_t index) {
	return pushConstants[index];
}

VkPipelineLayout grail::DescriptorGroup::getPipelineLayout() {
	return pipelineLayout;
}

void grail::DescriptorGroup::update() {
	if (!dirty) return;

	for (DescriptorSet& set : descriptorSets) {
		set.update();
	}

	dirty = false;
}

bool grail::DescriptorGroup::isEmpty() {
	return descriptorSets.empty();
}

void grail::DescriptorGroup::dispose() {
	if (pool != VK_NULL_HANDLE) {
		vkDestroyDescriptorPool(VulkanContext::mainGPU.device, pool, nullptr);
		vkDestroyPipelineLayout(VulkanContext::mainGPU.device, pipelineLayout, nullptr);

		for (DescriptorSet& set : descriptorSets) {
			vkDestroyDescriptorSetLayout(VulkanContext::mainGPU.device, set.setLayout, nullptr);
		}
	}

	descriptorSets.clear();
	descriptorSetHandles.clear();
	pushConstants.clear();
}

grail::DescriptorSet::DescriptorSet() { }

uint32_t grail::DescriptorSet::getBindingCount() {
	return bindings.size();
}

grail::DescriptorBinding& grail::DescriptorSet::getBinding(uint32_t index) {
	for (auto& val : bindings) {
		if (val.second.bindingIndex == index) {
			return val.second;
		}
	}

	throw new exceptions::RuntimeException("Descriptor with index: " + std::to_string(index) + " does not exist!");
}

grail::DescriptorBinding& grail::DescriptorSet::getBinding(const std::string& identifier) {
	return bindings[identifier];
}

void grail::DescriptorSet::update() {
	if (!dirty) return;

	std::vector<VkWriteDescriptorSet> writeSets;
	for (auto& val : bindings) {
		if (val.second.dirty) {
			writeSets.push_back(val.second.writeSet);
			val.second.dirty = false;
		}
	}

	if (writeSets.size() > 0)
		vkUpdateDescriptorSets(VulkanContext::mainGPU.device, (uint32_t)writeSets.size(), &writeSets[0], 0, nullptr);

	dirty = false;
}

grail::DescriptorResourceType grail::DescriptorBinding::getResourceType() {
	return resourceType;
}

void* grail::DescriptorBinding::getResource() {
	return resource;
}

void * grail::DescriptorBinding::getDescriptor() {
	return descriptor;
}

std::string grail::DescriptorBinding::getIdentifier() {
	return identifier;
}

uint32_t grail::DescriptorBinding::setSetIndex() {
	return set;
}

uint32_t grail::DescriptorBinding::getBindingIndex() {
	return bindingIndex;
}

void grail::DescriptorBinding::copyBinding(DescriptorBinding & src) {
	if (resourceType == DescriptorResourceType::IMAGE) {
		setBinding(src.resource, &src.imageInfo);
	}
	else if (resourceType == DescriptorResourceType::BUFFER) {
		setBinding(src.resource, &src.bufferInfo);
	}
}
