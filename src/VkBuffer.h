#ifndef GRAIL_BUFFER_H
#define GRAIL_BUFFER_H

#include "Common.h"
#include "MemoryAllocator.h"
#include "Resource.h"

namespace grail {
	struct BufferCreateInfo {
		GPU gpu = VulkanContext::mainGPU;

		MemoryPropertyFlags memoryProperties = MemoryPropertyFlags::HOST_VISIBLE;

		BufferCreateFlags createFlags = BufferCreateFlags::NONE;
		SharingMode sharingMode = SharingMode::EXCLUSIVE;

		BufferUsageBits usageBits = BufferUsageBits::INDEX_BUFFER_BIT;

		VkDeviceSize size = 0;

		uint32_t queueFamilyIndexCount = 0;
		uint32_t* queueFamilyIndices = nullptr;

		// THIS NEEDS TO STOP
		VkMemoryRequirements2 specialMemoryReqs;

		MemoryType memoryType = MemoryType::STATIC;
	};

	class Buffer {
	public:
		Buffer(BufferCreateInfo& createInfo, MemoryAllocator* memoryRegion, void* data = nullptr, VkDeviceSize dataSize = 0);

		VkBuffer& getHandle();
		VkMemoryRequirements getMemoryRequirements();
		MemoryAllocation getMemoryRegionDescriptor();

		// The copy operation copies the same data in all instances
		// For a static buffer, this is the same as updateData
		// Does nothing if memory is not host visible
		void copyData(void* dataPtr, uint32_t size);

		// The update operation copies the data to the next available instance
		// For a static buffer, this is the same as copyData
		// Does nothing if memory is not host visible
		void updateData(void* dataPtr, uint32_t size);
		
		// Does nothing if memory is not host visible or mapped
		void map();

		// Does nothing if memory is unmapped or is persistant
		void unmap();

		// https://www.khronos.org/registry/vulkan/specs/1.1-extensions/man/html/vkFlushMappedMemoryRanges.html
		// Not sure about the usage of flush combined with memory flags, automatically calls unmap
		void flush();

		void dispose();

		void* getMappedPtr();

		VkDescriptorBufferInfo& getDescriptor();
		VkDescriptorBufferInfo& getDescriptor(uint32_t frameIndex);

		MemoryType getMemoryType();

		VkBuffer handle;

		std::string identifier;

		ResourceType resourceType = ResourceType::INTERNAL;

		VkDeviceSize getOffset();
	private:
		GPU gpu;
		VkMemoryRequirements memoryRequirements;
		MemoryAllocation memoryDescriptor;

		MemoryPropertyFlags memoryProperties;

		// How many instances of the same memory are contained in this object
		// Relevant for dynamic buffers 
		uint32_t instanceCount;
		VkDeviceSize instanceSize;
		std::vector<VkDescriptorBufferInfo> descriptors;
		MemoryType memoryType;

		bool mapped = false;
		void* mappedPtr = nullptr;
	};
}

#endif