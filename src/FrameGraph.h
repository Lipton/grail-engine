#ifndef GRAIL_FRAME_GRAPH_H
#define GRAIL_FRAME_GRAPH_H

#include "Common.h"

namespace grail {
	
#undef ABSOLUTE
	enum class SizeType {
		ABSOLUTE,
		SWAPCAHIN_RELATIVE,
		INPUT_RELATIVE
	};
#define ABSOLUTE 1

	enum class FrameGraphQueueBits : uint32_t {
		GRAPHICS = 0,
		COMPUTE = 1,
		PRESENT = 2,
		ASYNC_COMPUTE = 4,
		ASYNC_GRAPHICS = 8,
		RAYTRACE = 16,
	};

	enum class CommandPoolType : uint32_t {
		GRAPHICS = 0,
		COMPUTE = 1,
		TRANSFER = 2,

		LAST_FLAG = TRANSFER
	};

	struct AttachmentInfo {
		VkFormat format = VK_FORMAT_UNDEFINED;

		uint32_t width = 1;
		uint32_t height = 1;
		uint32_t depth = 1;

		SamplerFilter minFilter = SamplerFilter::LINEAR;
		SamplerFilter magFilter = SamplerFilter::LINEAR;
		bool samplerCompareEnable = false;
		CompareOp samplerCompareOP = CompareOp::NEVER;

		uint32_t levels = 1;
		uint32_t layers = 1;
		uint32_t samples = 1;
		uint32_t faces = 1;

		bool mipmapped = false;

		ImageUsageBits additionalUsageBits = ImageUsageBits::COLOR_ATTACHMENT_BIT;

		ImageLayout initialLayout = ImageLayout::UNDEFINED;
	};

	class Material;
	class VkTexture;
	class Buffer;
	class MaterialInstance;
	class MaterialDescription;

	class RenderResource {
	public:
	private:
	};

	class RenderTextureResource {
		friend class FrameGraph;
	public:
	private:

		uint32_t index;

		AttachmentInfo properties;
	};

	class RenderAttachment {
		friend class FrameGraph;
		friend class RenderPass;
	public:
		VkTexture* getTexture();
	private:
		std::string name;

		uint32_t index = 0;
		AttachmentInfo properties = {};

		std::vector<uint32_t> readInPasses = std::vector<uint32_t>();
		std::vector<uint32_t> writtenInPasses = std::vector<uint32_t>();

		VkImageLayout initialLayout;
		VkImageLayout finalLayout;

		VkTexture* texture = nullptr;
	};

	class RenderPass {
		friend class FrameGraph;


		// TEMP
		friend class Editor;
	public:
		void addInputAttachment(RenderAttachment& attachment);
		void addOutputAttachment(RenderAttachment& attachment);
		void addHistoryAttachment(RenderAttachment& src, RenderAttachment& dst);

		void setDepthStencilOutputAttachment(RenderAttachment& attachment);

		void addInputBuffer(Buffer* buffer);
		void addInputTexture(VkTexture* texture);

		void setOnRender(std::function<void(VkCommandBuffer&)> func);
		void setGetClearColor(std::function<VkClearColorValue()> func);
		void setGetDepthStencilClearColor(std::function<VkClearDepthStencilValue()> func);

		void createMaterialFromShaders(const std::string vertexShader, const std::string fragmentShader);
		void createMaterialFromShaders(const std::string computeShader);

		std::string getName() const;

		uint32_t getOutputCount() const;
		uint32_t getColorOutputCount() const;
		uint32_t getInputCount() const;

		double getMinGPUTime() const;
		double getMaxGPUTime() const;
		double getAvgGPUTime() const;
		double getAvgGPUTimeTrue() const;

		double getMinCPUTime() const;
		double getMaxCPUTime() const;
		double getAvgCPUTime() const;
		double getAvgCPUTimeTrue() const;

		uint32_t getWidth() const;
		uint32_t getHeight() const;

		VkRenderPass getRenderPass() const;

		MaterialInstance* getDefaultMaterial();

		void beginRenderPass(VkCommandBuffer cmd, uint32_t framebufferIndex = 0, uint32_t width = 0, uint32_t height = 0);
		void endRenderPass(VkCommandBuffer cmd);

		void bindMaterial(VkCommandBuffer cmd, MaterialInstance* material);
		void setPushConstants(VkCommandBuffer cmd, MaterialInstance* material, void* data);
		void drawMesh(VkCommandBuffer cmd, class Mesh* mesh);

		void createFramebuffer(VkFramebufferCreateInfo createInfo);
	private: 
		uint32_t index = 0;
		FrameGraphQueueBits queueFlags = FrameGraphQueueBits::GRAPHICS;

		uint32_t width = 0;
		uint32_t height = 0;

		class FrameGraph* graph;

		std::string name;

		std::vector<uint32_t> inputs;
		std::vector<uint32_t> outputs;
		std::vector<std::tuple<uint32_t, uint32_t>> historyAttachments;

		std::vector<Buffer*> bufferInputs;
		std::vector<VkTexture*> textureInputs;

		uint32_t depthStencilOutput = UINT32_MAX;

		std::function<void(VkCommandBuffer&)> onRender = nullptr;
		std::function<VkClearColorValue()> onClearColor = []() -> VkClearColorValue { return { 0.0f, 0.0f, 0.0f, 1.0f }; };
		std::function<VkClearDepthStencilValue()> onClearDepthStencil = []() ->VkClearDepthStencilValue { return { 1.0f, 0 }; };

		uint32_t frameIndex = 0;

		std::vector<double> cpuExecutionTimes;
		std::vector<double> gpuExecutionTimes;

		const uint32_t timeMetricSampleCount = 100;
		uint32_t currentMetricSample = 0;

		double gpuTimeSum = 0.0;
		double gpuTimeAvg = 0.0;
		double gpuTimeMin = DBL_MAX;
		double gpuTimeMax = 0.0;

		double cpuTimeSum = 0.0;
		double cpuTimeAvg = 0.0;
		double cpuTimeMin = DBL_MAX;
		double cpuTimeMax = 0.0;

		std::vector<VkCommandBuffer> primaryCmdBuffers;
		std::vector<VkFramebuffer> framebuffers;
		VkRenderPass renderPass;

		void createCmdBufferBeginInfo();
		void createRenderPassBeginInfo();
		void createViewport();
		void createScissor();
		void createSubmitInfo();

		void prepareInfos();
		struct {
			VkCommandBufferBeginInfo cmdBeginInfo;
			VkRenderPassBeginInfo renderPassBeginInfo;
			VkViewport viewport;
			VkRect2D scissor;
			VkSubmitInfo submitInfo;

			VkPipelineStageFlags submitPipelineStages = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

			std::vector<VkClearValue> clearValues;
		} renderInfo;

		MaterialDescription* material = nullptr;
		MaterialInstance* materialInstance = nullptr;

		// NOTE: this is a temporary convenience tool to defer pipeline creation after the graph has been created
		struct {
			std::string computeShaderPath;

			std::string vertexShaderPath;
			std::string fragmentShaderPath;
		} shaderInformation;

		void createGraphicsPipeline();
		void createComputePipeline();

		void createMaterial();

		bool active = false;

		VkImageLayout initialLayout;
		VkImageLayout finalLayout;
	};

	class FrameGraph {
		friend class RenderPass;
	public:
		FrameGraph(GPU gpu);

		RenderPass& createPass(std::string name, FrameGraphQueueBits queue);

		RenderAttachment& createAttachment(const std::string& name, AttachmentInfo& info);

		// Compute the order and initialize the passes
		void buildGraph();

		// Build the render and transition command buffers
		void buildFrame(Swapchain* swapchain = nullptr);

		// Submit everything to the respective queues
		void submitFrame(Swapchain* swapchain = nullptr);

		VkCommandBuffer allocateCommandBuffer(CommandPoolType type);

		// Use when dealing with non-presenting passes
		void setFinalPass(RenderPass& pass);

		// Return all of the active passes in order of execution
		void getActivePasses(std::vector<const RenderPass*>& passes);

		// Attempt to retrieve a pass based on name
		// Searches trough all of the active and innactive passes
		RenderPass* getPass(const std::string& name);

	private:
		uint32_t framesInFlight = Graphics::getPrerenderedFrameCount(); // NOTE: current implementation allows *2* in flight frames to avoid resource copies
		uint32_t frameIndex = 0;

		std::vector<VkCommandPool> commandPools;

		std::vector<RenderAttachment*> attachments;
		
		std::vector<uint32_t> renderPassOrder;
		std::vector<RenderPass*> renderPasses;

		// Only used in the case of non-presenting graphs
		uint32_t finalPass = UINT32_MAX;

		std::vector<VkFence> frameFences;
		std::vector<VkSemaphore> renderAvailableSemaphores;
		std::vector<VkSemaphore> renderFinishedSemaphores;

		std::vector<VkCommandBuffer> transitionBuffers;

		std::vector<VkSubmitInfo> perFrameSubmitGraphics;
		std::vector<VkSubmitInfo> perFrameSubmitCompute;
		std::vector<VkSubmitInfo> perFrameSubmitTransfer;
		uint32_t swapchainImageIndex = 0;

		// Query pool to measure GPU timings;
		VkQueryPool timestampQueryPool;

		GPU gpu;
	};
}

#endif