#ifndef GRAIL_DESCRIPTOR_SET_H
#define GRAIL_DESCRIPTOR_SET_H

#include "Common.h"
#include "PipelineFactory.h"

#include <list>

namespace grail {
	enum class DescriptorBindingType {
		SAMPLER = VK_DESCRIPTOR_TYPE_SAMPLER,
		COMBINED_IMAGE_SAMPLER = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
		SAMPLED_IMAGE = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE,
		STORAGE_IMAGE = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE,
		UNIFORM_TEXEL_BUFFER = VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER,
		STORAGE_TEXEL_BUFFER = VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER,
		UNIFORM_BUFFER = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
		STORAGE_BUFFER = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
		UNIFORM_BUFFER_DYNAMIC = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC,
		STORAGE_BUFFER_DYNAMIC = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC,
		INPUT_ATTACHMENT = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT,
		INLINE_UNIFORM_BLOCK_EXT = VK_DESCRIPTOR_TYPE_INLINE_UNIFORM_BLOCK_EXT,
		ACCELERATION_STRUCTURE_NV = VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_NV,
	};

	enum class DescriptorResourceType {
		BUFFER,
		IMAGE
	};

	struct DescriptorBindingDescription {
		std::string identifier;

		ShaderStageBits shaderStageFlags;
		DescriptorBindingType type;

		uint32_t set;
		uint32_t index;
	};

	struct PushConstantRange {
		ShaderStageBits stageFlags = ShaderStageBits::ALL;

		uint32_t offset = 0;
		uint32_t size = 0;
	};

	class DescriptorLayout {
		friend class DescriptorGroup;
	public:
		void addDescriptor(DescriptorBindingDescription description);
		void addPushConstant(PushConstantRange range);

		void finalize();
	private:
		std::vector<DescriptorBindingDescription> descriptions;
		std::vector<PushConstantRange> pushConstants;

		std::list<uint32_t> setMap;

		VkPipelineLayout pipelineLayout;

		bool finalized = false;
	};

	class DescriptorBinding {
		friend class DescriptorGroup;
		friend class DescriptorSet;
	public:
		// Resource accepts Texture or Buffer resrouce to keep track
		// Binding accepts VkDescriptorBufferInfo or VkDescriptorImageInfo, stores a copy of parameters, does not need a persistant object
		void setBinding(void* resource, void* binding);

		DescriptorResourceType getResourceType();

		void* getResource();
		void* getDescriptor();

		std::string getIdentifier();
		
		uint32_t setSetIndex();
		uint32_t getBindingIndex();

		void copyBinding(DescriptorBinding& src);
	private:
		//DescriptorBinding();

		uint32_t set = 0;
		uint32_t bindingIndex = 0;

		std::string identifier;

		ShaderStageBits shaderStages;
		DescriptorBindingType type;

		bool dirty = false;

		VkDescriptorBufferInfo bufferInfo;
		VkDescriptorImageInfo imageInfo;

		VkWriteDescriptorSet writeSet;

		DescriptorResourceType resourceType;

		void* resource = nullptr;
		void* descriptor = nullptr;

		class DescriptorSet* parentSet = nullptr;
	};

	class DescriptorSet {
		friend class DescriptorGroup;

		friend class DescriptorBinding;
	public:
		uint32_t getBindingCount();

		DescriptorBinding& getBinding(uint32_t index);
		DescriptorBinding& getBinding(const std::string& identifier);

		void update();
	private:
		DescriptorSet();

		std::map<std::string, DescriptorBinding> bindings;

		VkDescriptorSet setHandle = VK_NULL_HANDLE;
		VkDescriptorSetLayout setLayout = VK_NULL_HANDLE;

		uint32_t setIndex = 0;

		bool dirty = false;

		class DescriptorGroup* parentGroup = nullptr;
	};

	class DescriptorGroup {
		friend class DescriptorBinding;
	public:
		DescriptorGroup(DescriptorLayout& layout);
		DescriptorGroup();

		void initialize(DescriptorLayout& layout);

		uint32_t getDescriptorCount();

		DescriptorSet& getSet(uint32_t index);
		VkDescriptorSet* getHandlePtr();

		// Comparatively slow, should use for convenience only
		DescriptorBinding& getBinding(const std::string& identifier);

		// Same as getBinding but returns nullptr if not found instead of throwing
		DescriptorBinding* tryGetBinding(const std::string& identifier);

		PushConstantRange getPushConstant(uint32_t index);

		VkPipelineLayout getPipelineLayout();
	
		void update();

		// Returns if there are no descriptor bindings, does not look at push constants
		bool isEmpty();

		void dispose();
	private:
		std::vector<PushConstantRange> pushConstants;

		std::vector<DescriptorSet> descriptorSets;
		std::vector<VkDescriptorSet> descriptorSetHandles;

		VkDescriptorPool pool = VK_NULL_HANDLE;
		VkPipelineLayout pipelineLayout = VK_NULL_HANDLE;

		bool dirty = false;
	};
}

#endif