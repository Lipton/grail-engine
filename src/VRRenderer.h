#ifndef GRAIL_DEFERRED_RENDERER_H
#define GRAIL_DEFERRED_RENDERER_H

#include "Common.h"
#include "DescriptorSet.h"
#include "Frustum.h"

namespace grail {
	class FrameGraph;
	class MaterialDescription;
	class MaterialInstance;
	class Buffer;
	class Mesh;
	class Camera;
	class PerspectiveCamera;
	class RenderPass;
	class RayTracingContext;

	struct Viewport {
		float x;
		float y;
		float width;
		float height;
		float minDepth = 0.0f;
		float maxDepth = 1.0f;
	};

	class VRRenderer {
	public:
		VRRenderer(PerspectiveCamera* camera);

		static struct RendererSettings {
			bool freezeViewFrustrum = false;

			float cameraAperture = 4.0;
			float cameraShutterSpeed = 60.0;
			float cameraSensitivity = 100.0;

			float computedExposure = 0.0f;

			glm::vec4 directionalLightDirection = glm::vec4(0.68, 1.0, 1.0, 0.0);
			glm::vec3 directionalLightColor = glm::vec3(1.0);
			float directionalLightIntensity = 10.0;

			float jitterIntensity = 1.0f;

			bool TAAEnabled = true;
			bool advancedTAA = true;

			float cascadeSplitLambda = 0.777f;

			int motionBlurSamples = 15;
			bool useNewMotionBlur = false;
		} settings;

		void setCamera(PerspectiveCamera* camera);
		static PerspectiveCamera* getCamera();

		static RendererSettings& getSettings();

		void setOutputViewport(Viewport viewport);
		void resetOutputViewport();

		void render(float delta);

		void setPostRenderFunction(std::function<void(RenderPass*, VkCommandBuffer&)> fun);

		FrameGraph* getFrameGraph();
	private:
		std::function<void(RenderPass*, VkCommandBuffer&)> postRender = {};

		// Viewport for the final rendererPass
		Viewport viewport;

		float time = 0;
		float lastFrameTime = 0;
		uint64_t frameIndex = 0;

		glm::vec3 lastCameraDirection;
		static PerspectiveCamera* camera;

		// TODO: move these to the initialization or make them dynanmic
		const int shadowMapCascadeSplits = 3;
#if defined(PLATFORM_MOBILE)
		uint32_t shadowCascadeMapSize = 1024;
#else
		uint32_t shadowCascadeMapSize = 2048;
#endif
		Frustum cascadeFrustums[4];

		Mesh* fullscreenMesh;

		// Additional variables used for depth mip generation
		std::vector<grail::DescriptorGroup> depthMipDescriptors;
		grail::MaterialDescription* depthMipMaterial;
		grail::MaterialInstance* depthMipMaterialInstance;

		// Main framegraph
		FrameGraph* frameGraph;
		class VkTexture* fxaaPassOuput;
		int eyeIndex = 0;

		// Graph used to generate precumputed irradiance and specular
		FrameGraph* envProbeGraph;

		// Camera information passed down to multiple effects and passes
		struct CameraInfo {
			glm::mat4 projection;
			glm::mat4 inverseProjection;

			glm::mat4 view;
			glm::mat4 inverseView;

			glm::mat4 combined;
			glm::mat4 lastCombined;
			glm::mat4 inverseCombined;

			glm::vec4 position;
			glm::vec4 viewDirection;
			glm::vec4 viewport;

			glm::mat4 cascadeMats[4];
			glm::vec4 cascadeSplits[4];

			glm::vec2 time;
			glm::vec2 subsample;
		} cameraInfo;
		// GPU buffer used to store the info
		Buffer* cameraInfoBuffer;

		// Sample positions for taa X16
		const glm::vec2 taaSampleMap[16] = {
			glm::vec2(-8.0f, 0.0f) / 8.0f,
			glm::vec2(-6.0f, -4.0f) / 8.0f,
			glm::vec2(-3.0f, -2.0f) / 8.0f,
			glm::vec2(-2.0f, -6.0f) / 8.0f,
			glm::vec2(1.0f, -1.0f) / 8.0f,
			glm::vec2(2.0f, -5.0f) / 8.0f,
			glm::vec2(6.0f, -7.0f) / 8.0f,
			glm::vec2(5.0f, -3.0f) / 8.0f,
			glm::vec2(4.0f, 1.0f) / 8.0f,
			glm::vec2(7.0f, 4.0f) / 8.0f,
			glm::vec2(3.0f, 5.0f) / 8.0f,
			glm::vec2(0.0f, 7.0f) / 8.0f,
			glm::vec2(-1.0f, 3.0f) / 8.0f,
			glm::vec2(-4.0f, 6.0f) / 8.0f,
			glm::vec2(-7.0f, 8.0f) / 8.0f,
			glm::vec2(-5.0f, 2.0f) / 8.0f
		};

		// Initialization functions
		void createDefaultAssets();
		void createBuffers();
		void initializeFrameGraphs();

		// Helper functions
		void updateShadowCascades();

		// TODO: make this work properly
		void generateBRDFLookupTexture();
		void generateIrradianceMap();
		void generateSpecularMap();

		// TODO: find a better way than this
		struct {
			glm::mat4 mat;
			glm::mat4 lastMat;
		} pushConsts;
	};
}

#endif