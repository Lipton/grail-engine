#ifndef GRAIL_SWAPCHAIN_H
#define GRAIL_SWAPCHAIN_H

#include "Common.h"

#define GET_INSTANCE_PROC_ADDR(inst, entrypoint) {											\
	fp##entrypoint = (PFN_vk##entrypoint) vkGetInstanceProcAddr(inst, "vk"#entrypoint);		\
	if (fp##entrypoint == NULL)	{															\
		assert(false);																		\
	}																						\
}

#define GET_DEVICE_PROC_ADDR(dev, entrypoint) {												\
	fp##entrypoint = (PFN_vk##entrypoint) vkGetDeviceProcAddr(dev, "vk"#entrypoint);		\
	if (fp##entrypoint == NULL) {															\
		assert(false);																		\
	}																						\
}

namespace grail {
	struct SwapchainBuffer {
		VkImage image;
		VkImageView view;
	};

	class Swapchain {
	public:
		VkSwapchainKHR handle = VK_NULL_HANDLE;

		GPU* gpu;

		uint32_t imageCount;
		std::vector<VkImage> images;
		std::vector<SwapchainBuffer> buffers;
		std::vector<VkFramebuffer> framebuffers;

		uint32_t width;
		uint32_t height;

		uint32_t queueNodeIndex = UINT32_MAX;

		Swapchain(class ApplicationWindow* window, GPU& gpu);
		void create(VkCommandBuffer commandBuffer, uint32_t* width, uint32_t* height, bool vSync);
		void createFramebuffers(GPU& gpu, VkRenderPass renderPass);
		void recreate();

		VkResult aquireNextImage(VkSemaphore semaphore, uint32_t* currentBuffer, VkFence fence);
		VkResult queuePresent(VkQueue queue, uint32_t currentBuffer);
		VkResult queuePresent(VkQueue queue, uint32_t currentBuffer, VkSemaphore waitSemaphore);
	private:
		const char* tag = "SWAPCHAIN";

		VkSurfaceKHR surface;

		PFN_vkGetPhysicalDeviceSurfaceSupportKHR fpGetPhysicalDeviceSurfaceSupportKHR;
		PFN_vkGetPhysicalDeviceSurfaceCapabilitiesKHR fpGetPhysicalDeviceSurfaceCapabilitiesKHR;
		PFN_vkGetPhysicalDeviceSurfaceFormatsKHR fpGetPhysicalDeviceSurfaceFormatsKHR;
		PFN_vkGetPhysicalDeviceSurfacePresentModesKHR fpGetPhysicalDeviceSurfacePresentModesKHR;
		PFN_vkCreateSwapchainKHR fpCreateSwapchainKHR;
		PFN_vkDestroySwapchainKHR fpDestroySwapchainKHR;
		PFN_vkGetSwapchainImagesKHR fpGetSwapchainImagesKHR;
		PFN_vkAcquireNextImageKHR fpAcquireNextImageKHR;
		PFN_vkQueuePresentKHR fpQueuePresentKHR;
	};
}

#endif