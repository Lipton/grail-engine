#include "MaterialParser.h"

#include <json.hpp>
#include <shaderc/shaderc.hpp>
#include "FileIO.h"
#include "Utils.h"
#include "FrameGraph.h"
#include "ShaderLoader.h"
#include "DescriptorSet.h"
#include "VkBuffer.h"
#include "VkTexture.h"
#include "Resources.h"

#include <stack>

std::map<std::string, grail::CullMode> grail::MaterialParser::cullModeMappings = {
	{ "back", grail::CullMode::BACK },
	{ "front", grail::CullMode::FRONT },
	{ "none", grail::CullMode::NONE },
	{ "front_and_back", grail::CullMode::FRONT_AND_BACK }
};

std::map<std::string, grail::FrontFace> grail::MaterialParser::frontFaceMappings = {
	{ "clockwise", grail::FrontFace::CLOCKWISE },
	{ "counter_clockwise", grail::FrontFace::COUNTER_CLOCKWISE },
};

std::map<std::string, VkBool32> grail::MaterialParser::booleanMappings = {
	{ "true", VK_TRUE },
	{ "false", VK_FALSE },
};

grail::MaterialDescription* grail::MaterialParser::parseMaterial(const std::string& descriptorFile, FrameGraph* graph) {
	nlohmann::json description;

	// Read the file
	{
		char* contents;
		uint32_t size;

		grail::FileIO::readBinaryFile(descriptorFile, &contents, &size);
		description = nlohmann::json::parse(std::string(contents, size));
		delete[] contents;
	}

	// compute permutations
	/*std::vector<std::vector<std::string>> sets;
	std::vector<std::vector<std::string>> permutations;
	if(description["pipeline"].find("permutations") != description["pipeline"].end()) {
		nlohmann::json nest = description["pipeline"]["permutations"];

		uint32_t structureDepth = -1;

		std::function<void(nlohmann::json&)> traverse = [&](nlohmann::json& base) {
			structureDepth++;
			
			for (nlohmann::json& node : base) {
				if (node.is_array()) {
					traverse(node);
				}
				else {
					if (sets.size() < structureDepth) {
						sets.push_back(std::vector<std::string>());
					}

					sets[sets.size() - 1].push_back(node);
				}
			}
		};
		traverse(nest);
		
		std::vector<uint32_t> indices;
		indices.resize(sets.size());

		for (uint32_t i = 0; i < indices.size(); i++) {
			indices[i] = 0;
		}

		while (true) {
			permutations.push_back(std::vector<std::string>());
			for (uint32_t i = 0; i < sets.size(); i++) {
				permutations[permutations.size() - 1].push_back(sets[i][indices[i]]);
			}

			int next = sets.size() - 1;
			while (next >= 0 && (indices[next] + 1 >= sets[next].size())) {
				next--;
			}

			if (next < 0) {
				break;
			}

			indices[next]++;

			for (uint32_t i = next + 1; i < sets.size(); i++) {
				indices[i] = 0;
			}
		}
	}*/

	std::string materialName = description["name"];
	nlohmann::json& pipelineNode = description["pipeline"];

	MaterialDescription* baseMaterialDescription = new MaterialDescription(materialName);

	// First we parse the types
	std::vector<MaterialType> types;
	std::vector<std::string> typeStrings;
	if (pipelineNode.find("types") == pipelineNode.end()) {
		// If there are no defined types, add a geometry type
		typeStrings.push_back("GEOMETRY");
		types.push_back(MaterialType::GEOMETRY);
	}
	else {
		// Else parse and add defined types
		typeStrings = pipelineNode["types"].get<std::vector<std::string>>();

		for (std::string& type : typeStrings) {
			if (utils::string::equalsCaseInsensitive(type, "geometry")) {
				types.push_back(MaterialType::GEOMETRY);
			}

			if (utils::string::equalsCaseInsensitive(type, "shadow")) {
				types.push_back(MaterialType::SHADOW);
			}
		}
	}
	baseMaterialDescription->definedTypes = types;


	//Then we parse and construct all of the options
	std::vector<std::vector<std::string>> optionSets;
	std::vector<std::string> options;

	// If we have options, parse them and compute the possible sets
	if (pipelineNode.find("options") != pipelineNode.end()) {
		options = pipelineNode["options"].get<std::vector<std::string>>();
		optionSets.resize(std::pow(2, options.size()) + 1);

		std::function<bool(std::vector<bool>&)> increase = [](std::vector<bool>& bs) -> bool {
			for (std::size_t i = 0; i != bs.size(); ++i) {
				bs[i] = !bs[i];
				if (bs[i] == true) {
					return true;
				}
			}
			return false;
		};

		std::function <void(const std::vector<std::string> & options, std::vector<std::vector<std::string>>&)> compute =
			[=](const std::vector<std::string>& options, std::vector<std::vector<std::string>>& vec) {
			std::vector<bool> bitset(options.size());

			uint32_t setIndex = 1;
			do {
				for (std::size_t i = 0; i != options.size(); ++i) {
					if (bitset[i]) {
						vec[setIndex].push_back(options[i]);
					}
				}
				setIndex++;
			} while (increase(bitset));
		};

		compute(options, optionSets);
	}
	baseMaterialDescription->materialOptions = options;
	for (uint32_t option = 0; option < options.size(); option++) {
		//GRAIL_LOG(DEBUG, "OPTION") << options[option] << " = " << (static_cast<uint64_t>(1) << option);
		baseMaterialDescription->optionIndices.insert({ options[option], (static_cast<uint64_t>(1) << option) });
	}

	// Parse the base pipeline information
	GraphicsPipelineCreateInfo createInfo = {};
	parsePipeline(createInfo, pipelineNode, graph);

	// Retrieve and compile the shaders
	std::vector<std::string> declaredShaders;
	pipelineNode["shaders"].get_to(declaredShaders);

	std::vector<Shader*> baseShaders;
	// Compile the base shaders first
	for (std::string& shaderPath : declaredShaders) {
		baseShaders.push_back(ShaderLoader::loadSPIRVShader(shaderPath));
	}
	baseMaterialDescription->baseShaders = baseShaders;

	// Compile and parse all of the permutations, this will create a new material per set
	for (std::vector<std::string>& set : optionSets) {
		std::vector<VertexAttribute> vertexAttributes;
		std::vector<ShaderPermutation*> shaderPermutations;
		std::vector<GraphicsPipelineCreateInfo> pipelineInfos;
		DescriptorLayout descriptorLayout;

		// First compile the needed shader permutations
		for (Shader* shader : baseShaders) {
			for (std::string& type : typeStrings) {
				// Compute the needed defines
				std::vector<std::string> defines;

				defines.push_back(type);
				for (std::string& option : set) {
					defines.push_back(option);
				}

				{
					if (utils::string::equalsCaseInsensitive(type, "shadow")) {
						for (uint32_t cascade = 0; cascade < 4; cascade++) {
							GraphicsPipelineCreateInfo info = {};
							parsePipeline(info, pipelineNode, graph, defines);

							pipelineInfos.push_back(info);
						}
					}
					else {
						GraphicsPipelineCreateInfo info = {};
						parsePipeline(info, pipelineNode, graph, defines);

						pipelineInfos.push_back(info);
					}
				}

				if (utils::string::equalsCaseInsensitive(type, "shadow")) {
					defines.push_back("");
					for (uint32_t cascade = 0; cascade < 4; cascade++) {
						defines[defines.size() - 1] = "CASCADE_" + std::to_string(cascade);

						ShaderPermutation* permutation = shader->compilePermutation(defines, true);
						shaderPermutations.push_back(permutation);
					}
				}
				else {
					ShaderPermutation* permutation = shader->compilePermutation(defines, true);
					shaderPermutations.push_back(permutation);

					// parse the needed descriptor and vertex layouts if this is the geometry type of shader
					if (utils::string::equalsCaseInsensitive(type, "geometry")) {
						DescriptorBindingDescription desc = {};

						for (ImageShaderVar& tex : permutation->sampledImages) {
							desc.identifier = tex.name;
							desc.index = tex.binding;
							desc.set = tex.set;
							desc.shaderStageFlags = permutation->stage;

							// This needs to be worked out
							desc.type = DescriptorBindingType::COMBINED_IMAGE_SAMPLER;

							descriptorLayout.addDescriptor(desc);
						}

						for (BufferBlock& buffer : permutation->uniformBuffers) {
							desc.identifier = buffer.name;
							desc.index = buffer.binding;
							desc.set = buffer.set;
							desc.shaderStageFlags = permutation->stage;

							// This needs to be worked out
							desc.type = DescriptorBindingType::UNIFORM_BUFFER;

							descriptorLayout.addDescriptor(desc);
						}

						BufferBlock push = permutation->pushConstants;
						PushConstantRange pushRange = {};
						pushRange.size = push.size;
						pushRange.offset = 0;
						pushRange.stageFlags = permutation->stage;

						descriptorLayout.addPushConstant(pushRange);

						if (permutation->stage == ShaderStageBits::VERTEX) {
							// NOTE: only works for float based attributes
							for (IOShaderVar& input : permutation->inputs) {
								VkFormat format = VK_FORMAT_UNDEFINED;

								if (input.rows == 1) {
									format = VK_FORMAT_R32_SFLOAT;
								}
								else if (input.rows == 2) {
									format = VK_FORMAT_R32G32_SFLOAT;
								}
								else if (input.rows == 3) {
									format = VK_FORMAT_R32G32B32_SFLOAT;
								}
								else if (input.rows == 4) {
									format = VK_FORMAT_R32G32B32A32_SFLOAT;
								}

								vertexAttributes.push_back(VertexAttribute(VertexAttributeType::GENERIC, format, input.rows, sizeof(float)));
							}
						}
					}
				}
			}
		}
		descriptorLayout.finalize();

		VertexLayout vertexLayout = VertexLayout(vertexAttributes);

		baseMaterialDescription->createMaterial(types, set, vertexLayout, descriptorLayout, shaderPermutations, pipelineInfos);
	}

	// Parse default material memory values
	if (description.find("memory") != description.end()) {
		for (auto& resource : description["memory"]) {
			MaterialMemoryResource memoryDescription = {};
			memoryDescription.defaultValues = resource["default"].get <std::vector<float>>();

			if (utils::string::equalsCaseInsensitive(resource["type"], "color")) {
				memoryDescription.type = MaterialMemoryResourceType::COLOR;
			}

			if (utils::string::equalsCaseInsensitive(resource["type"], "slider")) {
				memoryDescription.type = MaterialMemoryResourceType::SLIDER;
			}

			if (utils::string::equalsCaseInsensitive(resource["type"], "scalar")) {
				memoryDescription.type = MaterialMemoryResourceType::SCALAR;
			}

			if (utils::string::equalsCaseInsensitive(resource["type"], "scalar_slider")) {
				memoryDescription.type = MaterialMemoryResourceType::SCALAR_SLIDER;
			}

			if (resource.find("min") != resource.end()) memoryDescription.min = resource["min"];
			if (resource.find("max") != resource.end()) memoryDescription.max = resource["max"];
			if (resource.find("step") != resource.end()) memoryDescription.step = resource["step"];
			if (resource.find("description") != resource.end()) memoryDescription.description = resource["description"];
			if (resource.find("name") != resource.end()) memoryDescription.editorName = resource["name"];

			baseMaterialDescription->memoryDescriptions.insert({ resource["id"], memoryDescription });
		}
	}

	// Parse default descriptor values
	if (description.find("resources") != description.end()) {
		for (auto& resource : description["resources"].items()) {
			std::string id = resource.key();
			std::string val = resource.value();

			baseMaterialDescription->resourceDefaults.insert({ id, val });
		}
	}

	return baseMaterialDescription;
}

void grail::MaterialParser::parsePipeline(GraphicsPipelineCreateInfo& info, nlohmann::json & baseNode, FrameGraph * graph) {
	getMapping<CullMode>(baseNode, "cull_mode", cullModeMappings, &info.rasterizationState.cullMode);
	getMapping<FrontFace>(baseNode, "front_face", frontFaceMappings, &info.rasterizationState.frontFace);
	getMapping<VkBool32>(baseNode, "depth_clamp", booleanMappings, &info.rasterizationState.depthClampEnable);

	if (baseNode.find("pass") != baseNode.end()) {
		info.renderPass = graph->getPass(baseNode["pass"])->getRenderPass();
		info.colorBlendState.attachmentCount = graph->getPass(baseNode["pass"])->getColorOutputCount();
	}
}

void grail::MaterialParser::parsePipeline(GraphicsPipelineCreateInfo& info, nlohmann::json& baseNode, FrameGraph* graph, std::vector<std::string>& overloads) {
	parsePipeline(info, baseNode, graph);

	if (baseNode.find("overrides") != baseNode.end()) {
		for (const std::string& overloadID : overloads) {
			//GRAIL_LOG(INFO, "id") << overloadID;
			if (baseNode["overrides"].find(overloadID) != baseNode["overrides"].end()) {
				parsePipeline(info, baseNode["overrides"][overloadID], graph);
			}
		}
	}
}

grail::MaterialDescriptor grail::MaterialParser::parseMaterial_(const std::string & descriptorFile) {
	MaterialDescriptor info = {};

	char* contents;
	uint32_t size;

	CullMode cullMode = CullMode::BACK;
	FrontFace frontFace = FrontFace::CLOCKWISE;

	{
		grail::FileIO::readBinaryFile(descriptorFile, &contents, &size);
		nlohmann::json descriptor = nlohmann::json::parse(std::string(contents, size));
		delete[] contents;

		if (descriptor.find("cullMode") != descriptor.end()) {
			//getMapping<CullMode>(descriptor["cullMode"], cullModeMappings, &cullMode);
		}

		if (descriptor.find("frontFace") != descriptor.end()) {
			//getMapping<FrontFace>(descriptor["frontFace"], frontFaceMappings, &frontFace);
		}

		if (descriptor.find("shaders") != descriptor.end()) {
			std::vector<std::string> declaredShaders;
			descriptor.at("shaders").get_to(declaredShaders);

			std::map<std::string, ShaderTexture> sampledTextures;

			for (std::string& s : declaredShaders) {
				info.shaders.push_back(s);

				std::string extension = FileIO::getExtension(s);

				shaderc_shader_kind shaderKind;

				if (grail::utils::string::equalsCaseInsensitive(extension, "frag"))
					shaderKind = shaderc_shader_kind::shaderc_fragment_shader;
				else if (grail::utils::string::equalsCaseInsensitive(extension, "vert"))
					shaderKind = shaderc_shader_kind::shaderc_vertex_shader;
				else if (grail::utils::string::equalsCaseInsensitive(extension, "tcs"))
					shaderKind = shaderc_shader_kind::shaderc_tess_control_shader;
				else if (grail::utils::string::equalsCaseInsensitive(extension, "tes"))
					shaderKind = shaderc_shader_kind::shaderc_tess_evaluation_shader;
				else if (grail::utils::string::equalsCaseInsensitive(extension, "geom"))
					shaderKind = shaderc_shader_kind::shaderc_geometry_shader;
				else if (grail::utils::string::equalsCaseInsensitive(extension, "comp"))
					shaderKind = shaderc_shader_kind::shaderc_compute_shader;

				std::map<shaderc_shader_kind, ShaderStageBits> kindMapping = {
					{ shaderc_shader_kind::shaderc_fragment_shader, ShaderStageBits::FRAGMENT },
					{ shaderc_shader_kind::shaderc_vertex_shader, ShaderStageBits::VERTEX },
					{ shaderc_shader_kind::shaderc_tess_control_shader, ShaderStageBits::TESSELLATION_CONTROL },
					{ shaderc_shader_kind::shaderc_tess_evaluation_shader, ShaderStageBits::TESSELLATION_EVALUATION },
					{ shaderc_shader_kind::shaderc_geometry_shader, ShaderStageBits::GEOMETRY },
					{ shaderc_shader_kind::shaderc_compute_shader, ShaderStageBits::COMPUTE },
				};

				grail::FileIO::readBinaryFile(s, &contents, &size);
				std::string shaderCode = std::string(contents, size);
				delete[] contents;

				shaderc::Compiler glslCompiler;
				shaderc::CompileOptions glslOptions;
				shaderc::SpvCompilationResult glslCompilation = glslCompiler.CompileGlslToSpv(shaderCode, shaderKind, "main", glslOptions);

				if (glslCompilation.GetCompilationStatus() != shaderc_compilation_status_success) {
					GRAIL_LOG(ERROR, "SHADER LOADER") << "Error compiling: " << s << ":\n" << glslCompilation.GetErrorMessage();
					throw new grail::exceptions::RuntimeException("Shader Compilation Failed");
				}
				std::vector<uint32_t> spirvBinary(glslCompilation.begin(), glslCompilation.end());
				spirv_cross::Compiler comp(std::move(spirvBinary));

				spirv_cross::ShaderResources resources = comp.get_shader_resources();

				// TEXTURES
				for (const spirv_cross::Resource &resource : resources.sampled_images) {
					unsigned set = comp.get_decoration(resource.id, spv::DecorationDescriptorSet);
					unsigned binding = comp.get_decoration(resource.id, spv::DecorationBinding);

					if (sampledTextures.find(resource.name) == sampledTextures.end()) {
						sampledTextures.insert({ resource.name, { resource.name, kindMapping[shaderKind], binding, nullptr } });
					}
					else {
						sampledTextures[resource.name].stages |= kindMapping[shaderKind];
					}
				}

				// PUSH CONSTANTS
				if (resources.push_constant_buffers.size() > 0) {
					if (info.pushBlockVars.size() == 0) {
						std::vector<ModifiableShaderVar> modfiableValues = getModifiableValues(shaderCode);

						PushConstantRange pushConstantRange = {};

						spirv_cross::Resource& res = resources.push_constant_buffers[0];

						pushConstantRange.stageFlags = ShaderStageBits::ALL;
						pushConstantRange.offset = 0;
						pushConstantRange.size = comp.get_declared_struct_size(comp.get_type(res.base_type_id));

						auto &type = comp.get_type(res.base_type_id);
						unsigned member_count = type.member_types.size();

						for (unsigned i = 0; i < member_count; i++) {
							spirv_cross::SPIRType member_type = comp.get_type(type.member_types[i]);
							size_t member_size = comp.get_declared_struct_member_size(type, i);
							size_t offset = comp.type_struct_member_offset(type, i);

							if (!member_type.array.empty()) {
								size_t array_stride = comp.type_struct_member_array_stride(type, i);
							}

							if (member_type.columns > 1) {
								size_t matrix_stride = comp.type_struct_member_matrix_stride(type, i);
							}

							const std::string &name = comp.get_member_name(type.self, i);

							info.pushBlockVars.push_back({ name, ShaderVarType::FLOAT, static_cast<uint32_t>(offset), static_cast<uint32_t>(member_size) });

							ModifiableShaderVar* var = findParam(modfiableValues, name);
							if (var) {
								var->offset = offset;
								var->size = member_size;
							}
						}

						info.pushConstantRange = pushConstantRange;
						info.modifiableVars = modfiableValues;
					}
				}
			}

			for (auto& it : sampledTextures) {
				info.textures.push_back(it.second);
			}

			std::sort(info.textures.begin(), info.textures.end(), [](const ShaderTexture& a, const ShaderTexture& b) -> bool { return b.binding > a.binding; });
		}

		if (descriptor.find("defaults") != descriptor.end()) {
			for (ModifiableShaderVar& var : info.modifiableVars) {
				if (descriptor["defaults"].find(var.shaderName) != descriptor["defaults"].end()) {
					info.defaults.insert({ var.shaderName, descriptor["defaults"][var.shaderName].get<std::string>() });
				}
			}

			for (ShaderTexture& var : info.textures) {
				if (descriptor["defaults"].find(var.name) != descriptor["defaults"].end()) {
					info.defaults.insert({ var.name, descriptor["defaults"][var.name].get<std::string>() });
				}
			}
		}
	}

	/*info.createInfo.pipelineInformation.rasterizationState.cullMode = cullMode;
	info.createInfo.pipelineInformation.rasterizationState.frontFace = frontFace;*/

	return info;
}

grail::MaterialDescription grail::MaterialParser::parseMaterialFromFile(const std::string& descriptorFile, const std::string& staticDefines) {
	// PARSE MATERIAL DESCRIPTOR
		// PROBLEMS:
			//	assumes that all of the push constant buffers are the same accross all shaders ----------------
			//	does not track in which stages the constants are used ---------------
			//	does not map out all of the buffer members and types + sizes ----------
			//	does not map out uniforms ------------------
			//	does not generate a vertex layout
			//  does not return complied modules -----------------
			//  support for vars is wonky
			//  no instancing ----------------------
			//  multi_compile ------------------------
	GRAIL_LOG(INFO, "MATERIAL PARSER") << "Parsing material " << descriptorFile << "...";


	//MaterialDescription description = {};

	char* contents;
	uint32_t size;

	grail::FileIO::readBinaryFile(descriptorFile, &contents, &size);
	nlohmann::json matFile = nlohmann::json::parse(std::string(contents, size));
	delete[] contents;

	CullMode cullMode = CullMode::BACK;
	FrontFace frontFace = FrontFace::CLOCKWISE;

	//description.name = getDescriptorValueRequired(&matFile, "name");

	if (matFile.find("cullMode") != matFile.end()) {
		//getMapping<CullMode>(matFile["cullMode"], cullModeMappings, &cullMode);
	}

	if (matFile.find("frontFace") != matFile.end()) {
		//getMapping<FrontFace>(matFile["frontFace"], frontFaceMappings, &frontFace);
	}

	if (matFile.find("shaders") != matFile.end()) {
		std::vector<std::string> declaredShaders;
		matFile.at("shaders").get_to(declaredShaders);
	}

	//return nullptr;
	return MaterialDescription("");
}

std::string grail::MaterialParser::getDescriptorValueRequired(nlohmann::json* node, const std::string& id) {
	if (node->find(id) != node->end()) {
		return (*node)[id];
	}

	throw new grail::exceptions::RuntimeException("Required material property [" + id + "] was not found");
}

void grail::MaterialParser::gatherResources(std::vector<std::string>& input, MaterialDescription& description) {
	std::map<shaderc_shader_kind, ShaderStageBits> kindMapping = {
			{ shaderc_shader_kind::shaderc_fragment_shader, ShaderStageBits::FRAGMENT },
			{ shaderc_shader_kind::shaderc_vertex_shader, ShaderStageBits::VERTEX },
			{ shaderc_shader_kind::shaderc_tess_control_shader, ShaderStageBits::TESSELLATION_CONTROL },
			{ shaderc_shader_kind::shaderc_tess_evaluation_shader, ShaderStageBits::TESSELLATION_EVALUATION },
			{ shaderc_shader_kind::shaderc_geometry_shader, ShaderStageBits::GEOMETRY },
			{ shaderc_shader_kind::shaderc_compute_shader, ShaderStageBits::COMPUTE },
	};

	char* contents;
	uint32_t size;

	for (std::string& shader : input) {

		std::string extension = FileIO::getExtension(shader);

		shaderc_shader_kind shaderKind;

		if (grail::utils::string::equalsCaseInsensitive(extension, "frag"))
			shaderKind = shaderc_shader_kind::shaderc_fragment_shader;
		else if (grail::utils::string::equalsCaseInsensitive(extension, "vert"))
			shaderKind = shaderc_shader_kind::shaderc_vertex_shader;
		else if (grail::utils::string::equalsCaseInsensitive(extension, "tcs"))
			shaderKind = shaderc_shader_kind::shaderc_tess_control_shader;
		else if (grail::utils::string::equalsCaseInsensitive(extension, "tes"))
			shaderKind = shaderc_shader_kind::shaderc_tess_evaluation_shader;
		else if (grail::utils::string::equalsCaseInsensitive(extension, "geom"))
			shaderKind = shaderc_shader_kind::shaderc_geometry_shader;
		else if (grail::utils::string::equalsCaseInsensitive(extension, "comp"))
			shaderKind = shaderc_shader_kind::shaderc_compute_shader;
		else {
			throw new grail::exceptions::RuntimeException("Unknown shader type");
		}

		grail::FileIO::readBinaryFile(shader, &contents, &size);
		std::string shaderCode = std::string(contents, size);
		delete[] contents;

		shaderc::CompileOptions glslOptions;
		glslOptions.SetOptimizationLevel(shaderc_optimization_level_performance);

		shaderc::Compiler glslCompiler;
		shaderc::SpvCompilationResult glslCompilation = glslCompiler.CompileGlslToSpv(shaderCode, shaderKind, "main", glslOptions);

		if (glslCompilation.GetCompilationStatus() != shaderc_compilation_status_success) {
			GRAIL_LOG(ERROR, "SHADER LOADER") << "Error compiling shader [" << shader << "] VARIATON []:\n" << glslCompilation.GetErrorMessage();
			throw new grail::exceptions::RuntimeException("Shader Compilation Failed");
		}

		std::vector<uint32_t> spirvBinary(glslCompilation.begin(), glslCompilation.end());

		spirv_cross::Compiler comp(std::move(spirvBinary));
		spirv_cross::ShaderResources resources = comp.get_shader_resources();

	}
}

grail::ModifiableShaderVar * grail::findParam(std::vector<ModifiableShaderVar>& params, std::string name) {
	for (ModifiableShaderVar& v : params) {
		if (utils::string::equalsCaseInsensitive(v.shaderName, name)) {
			return &v;
		}
	}

	return nullptr;
}

std::vector<grail::ModifiableShaderVar> grail::getModifiableValues(std::string & code) {
	std::vector<ModifiableShaderVar> params;
	bool parseParam = false;

	std::string lastLine;
	auto lines = utils::string::splitByRegex(code, "\n");
	for (std::string& line : lines) {
		std::string lowerLine = std::string(line);
		utils::string::toLower(lowerLine);

		if (parseParam) {
			std::vector<std::string> definitions = utils::string::splitByRegex(utils::string::trim(line), " ");
			if (definitions.size() >= 2) {
				utils::string::replaceAll(definitions[1], ";", "");

				utils::string::replaceAll(lastLine, "//param", " ");

				std::map<std::string, std::string> paramMap;
				auto args = utils::string::splitByRegex(utils::string::trim(lastLine), ",");
				for (std::string& s : args) {
					auto vals = utils::string::splitByRegex(s, ":");

					if (vals.size() == 2) {
						paramMap[utils::string::trim(vals[0])] = utils::string::trim(vals[1]);
					}
				}

				ModifiableShaderVar var = {};
				var.shaderName = definitions[1];

				auto split = utils::string::splitByRegex(paramMap["type"], "\\|");

				for (auto& typeString : split) {
					std::string trimmedType = utils::string::trim(typeString);

					if (trimmedType.size() > 0) {
						if (utils::string::equalsCaseInsensitive(trimmedType, "color")) var.editType |= VarType::COLOR;
						if (utils::string::equalsCaseInsensitive(trimmedType, "scalar")) var.editType |= VarType::SCALAR;
						if (utils::string::equalsCaseInsensitive(trimmedType, "component")) var.editType |= VarType::SCALAR_SEPARATE;
						if (utils::string::equalsCaseInsensitive(trimmedType, "bool")) var.editType |= VarType::BOOLEAN;
						if (utils::string::equalsCaseInsensitive(trimmedType, "list")) { 
							var.editType |= VarType::LIST;
							paramMap.insert({ "listItem", utils::string::splitByRegex(paramMap["list"], ";")[0] });
						}
					}
				}

				(paramMap.find("name") != paramMap.end()) ? var.name = paramMap["name"] : var.shaderName;
				(paramMap.find("step") != paramMap.end()) ? var.step = std::stof(paramMap["step"]) : 0.1f;
				(paramMap.find("min") != paramMap.end()) ? var.min = std::stof(paramMap["min"]) : 0.0f;
				(paramMap.find("max") != paramMap.end()) ? var.max = std::stof(paramMap["max"]) : 0.0f;
				var.properties = paramMap;

				params.push_back(var);
			}

			parseParam = false;
		}

		if (lowerLine.find("//param") != std::string::npos) {
			parseParam = true;
		}

		lastLine = line;
	}

	return params;
}
