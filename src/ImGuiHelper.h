#ifndef GRAIL_IMGUI_HELPER_H
#define GRAIL_IMGUI_HELPER_H

#include "Common.h"
#include "imgui.h"

#include "DescriptorSet.h"

namespace ImGui {

	// ImGui::InputText() with std::string
	// Because text input needs dynamic resizing, we need to setup a callback to grow the capacity
	IMGUI_API bool  InputText(const char* label, std::string* str, ImGuiInputTextFlags flags = 0, ImGuiInputTextCallback callback = NULL, void* user_data = NULL);
	IMGUI_API bool  InputTextMultiline(const char* label, std::string* str, const ImVec2& size = ImVec2(0, 0), ImGuiInputTextFlags flags = 0, ImGuiInputTextCallback callback = NULL, void* user_data = NULL);
	IMGUI_API bool  InputTextWithHint(const char* label, const char* hint, std::string* str, ImGuiInputTextFlags flags = 0, ImGuiInputTextCallback callback = NULL, void* user_data = NULL);

	IMGUI_API void HelpMarker(const char* desc);

	IMGUI_API bool SceneTreeNode(const char* label);

	// From ImGuiHelper - the global scale of all of the ui items to have the same UI size under all resolutions, use to scale custom sized ui elements
	extern float scale;
}


// Alias the namespace to make it more comfortable to use
namespace UI = ImGui;

namespace grail {
	class Buffer;
	class VkTexture;
	class Material;
	class VertexLayout;
	class MaterialInstance;
	class MaterialDescription;

	class ImGuiConsole {
	public:
		ImGuiConsole();

		void clear();
		void log(LogType type, const std::string label, const std::string& message);

		void draw(bool* open);
	private:
		struct ConsoleLogItem {
			LogType type;
			std::string label;
			std::string message;

			ImVec4 color;
		};

		std::string inputBuffer;

		std::vector<ConsoleLogItem> items;
		std::vector<std::string> history;

		bool autoScroll = true;
		bool scrollToBottom = false;

		int historyPos = -1;
	};

	// Helps with creation and management of an IMGUI context and provides helper functions to ease using IMGUI
	class ImGuiHelper {
	private:
		::ImGuiContext* context = nullptr;

		Buffer* indexBuffer = nullptr;
		Buffer* vertexBuffer = nullptr;

		VkTexture* fontTexture = nullptr;

		VertexLayout* vertexLayout = nullptr;

		MaterialDescription* material = nullptr;
		MaterialInstance* matInstance = nullptr;

		uint32_t imageDescriptorIndex = 0;
		uint32_t frameIndex = 1;
		std::array<DescriptorGroup*, 300> imageDescriptors;

		std::unordered_map<VkTexture*, uint32_t> textureIndices;
	public:
		void initialize(VkRenderPass renderPass, const std::string& settingsFile = "");
		void render(VkCommandBuffer cmd, std::function<void()> renderFunc);
		void build(std::function<void()> buildFunc);
		void updateBuffers();

		void handleKeyPress(int keycode, int action);
		void handleKeyTyped(unsigned int c, int mods);

		VkTexture* imageID(class VkTexture* texture);
		VkTexture* imageID(class VkTexture* texture, VkDescriptorImageInfo& descriptor);
	};
}

#endif