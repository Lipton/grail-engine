#include "NativeTextureContainer.h"

#include "FileIO.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include <gli/generate_mipmaps.hpp>

grail::NativeTextureContainer::NativeTextureContainer(const std::string & path) {
	std::string extension = grail::FileIO::getExtension(path);

	uint32_t binarySize;
	unsigned char* binary = nullptr;

	if(FileIO::fileExists(FileIO::getAssetPath() + path))
		grail::FileIO::readBinaryFile(path, &binary, &binarySize);
	else 
		grail::FileIO::readBinaryFileExternal(path, &binary, &binarySize);

	floatFormat = false;

	if (extension.compare("dds") == 0 || extension.compare("ktx") == 0 || extension.compare("kmg") == 0) {
		gliHandle = new gli::texture2d(gli::load((const char*)binary, binarySize));

		this->width = static_cast<uint32_t>(gliHandle[0].extent().x);
		this->height = static_cast<uint32_t>(gliHandle[0].extent().y);
		this->mipLevels = static_cast<uint32_t>(gliHandle->levels());
		this->size = static_cast<uint32_t>(gliHandle->size());
		this->format = static_cast<VkFormat>((int)gliHandle->format());
		this->dataPointer = gliHandle->data();

		gli::gl GL(gli::gl::PROFILE_GL33);
		gli::gl::format const Format = GL.translate(gliHandle->format(), gliHandle->swizzles());

		std::map<int, ComponentSwizzle> swizzleMappings;
		swizzleMappings.insert({ 0x1903, ComponentSwizzle::RED });
		swizzleMappings.insert({ 0x1904, ComponentSwizzle::GREEN });
		swizzleMappings.insert({ 0x1905, ComponentSwizzle::BLUE });
		swizzleMappings.insert({ 0x1906, ComponentSwizzle::ALPHA });
		swizzleMappings.insert({ 0x0000, ComponentSwizzle::ZERO });
		swizzleMappings.insert({ 0x0001, ComponentSwizzle::ONE });

		for (uint32_t i = 0; i < 4; i++) {
			//swizzleMasks[i] = swizzleMappings[(Format.Swizzles[i])];
		}
	}
	else {
		int height;
		int width;
		int bpp;

		if (extension.compare("hdr") == 0) {
			floatFormat = true;

			dataPointer = stbi_loadf_from_memory(binary, binarySize, &width, &height, &bpp, STBI_rgb_alpha);

			if (!dataPointer) {
				GRAIL_LOG(ERROR, "IMAGE LOADING") << stbi_failure_reason();
			}

			this->size = width * height * 4 * sizeof(float);
			this->format = VK_FORMAT_R32G32B32A32_SFLOAT;
		}
		else {
			dataPointer = stbi_load_from_memory(binary, binarySize, &width, &height, &bpp, STBI_rgb_alpha);
			
			if (!dataPointer) {
				GRAIL_LOG(ERROR, "IMAGE LOADING") << stbi_failure_reason();
			}

			this->size = width * height * 4;
			this->format = VK_FORMAT_R8G8B8A8_UNORM;
		}

		this->width = width;
		this->height = height;
		this->mipLevels = static_cast<uint32_t>(std::floor(std::log2(std::max(width, height))) + 1);
	}

	delete[] binary;
}

grail::NativeTextureContainer::~NativeTextureContainer() {
	if (gliHandle != nullptr) {
		delete gliHandle;
	} else {
		stbi_image_free(dataPointer);
	}
}

bool grail::NativeTextureContainer::isFloatFormat() {
	return this->floatFormat;
}

bool grail::NativeTextureContainer::hasMips() {
	if (gliHandle != nullptr) {
		if (gliHandle->levels() > 1) {
			return true;
		}
	}

	return false;
}

void * grail::NativeTextureContainer::getDataPointer() {
	return this->dataPointer;
}

uint32_t grail::NativeTextureContainer::getDataSize() {
	return this->size;
}

uint32_t grail::NativeTextureContainer::getWidth() {
	return this->width;
}

uint32_t grail::NativeTextureContainer::getHeight() {
	return this->height;
}

VkFormat grail::NativeTextureContainer::getFormat() {
	return this->format;
}

uint32_t grail::NativeTextureContainer::getMipLevels() {
	return this->mipLevels;
}

gli::extent2d grail::NativeTextureContainer::getMipExtents(uint32_t index) {
	return gliHandle->extent(index);
}

uint32_t grail::NativeTextureContainer::getMipSize(uint32_t index) {
	return gliHandle->size(index);
}

grail::ComponentSwizzle grail::NativeTextureContainer::getSwizzleMask(uint32_t index) {
	return swizzleMasks[index];
}
