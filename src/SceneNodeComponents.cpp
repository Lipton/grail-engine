#include "SceneNodeComponents.h"
#include "Mesh.h"
#include "MaterialInstance.h"
#include "Material.h"
#include "Resources.h"
#include "Script.h"
#include "Physics.h"
#include "SceneNode.h"
#include "Scene.h"

#include <scripthandle/scripthandle.h>

std::map<std::string, std::function<void(entt::registry&, entt::entity&)>> grail::nodeComponents::detail::componentAssignMap = {
	REGISTER_COMPONENT_ASSIGN("Render", grail::nodeComponents::Render),
	REGISTER_COMPONENT_ASSIGN("Script", grail::nodeComponents::ScriptComponent),
	REGISTER_COMPONENT_ASSIGN("Physics", grail::nodeComponents::PhysicsComponent),
};

std::map<std::string, std::function<grail::nodeComponents::NodeComponent*(entt::registry&, entt::entity&)>> grail::nodeComponents::detail::componentRetrieveMap = {
	REGISTER_COMPONENT_RETRIEVE("Render", grail::nodeComponents::Render),
	REGISTER_COMPONENT_RETRIEVE("Script", grail::nodeComponents::ScriptComponent),
	REGISTER_COMPONENT_RETRIEVE("Physics", grail::nodeComponents::PhysicsComponent),
};

void grail::nodeComponents::createComponentFor(const std::string& componentName, entt::entity& entity, entt::registry& registry) {
	detail::componentAssignMap[componentName](registry, entity);
}

grail::nodeComponents::NodeComponent * grail::nodeComponents::retrieveComponent(const std::string & componentName, entt::entity & entity, entt::registry & registry) {
	return detail::componentRetrieveMap[componentName](registry, entity);
}

grail::nodeComponents::Render::Render() {
}

void grail::nodeComponents::Render::initDefault() {
	dispose();

	mesh = Resources::getMesh("sphere");
	material = Resources::getMaterial("default")->getBaseMaterial()->createInstance();
}

void grail::nodeComponents::Render::save(nlohmann::json & node) {
	node["Render"]["name"] = "Render";
	node["Render"]["mesh"] = mesh->identifier;
	node["Render"]["material"] = nlohmann::json::object();
	node["Render"]["material"]["base"] = material->getMaterialDescription()->getIdentifier();
	node["Render"]["material"]["key"] = material->getBaseMaterial()->getKey();
	node["Render"]["render"] = render;

	std::vector<std::tuple<std::string, std::string>> textures;
	std::vector<std::tuple<std::string, std::string>> buffers;

	for (uint32_t i = 0; i < material->getCurrentFrameDescriptor().getDescriptorCount(); i++) {
		for (uint32_t j = 0; j < material->getCurrentFrameDescriptor().getSet(i).getBindingCount(); j++) {
			DescriptorBinding& binding = material->getCurrentFrameDescriptor().getSet(i).getBinding(j);

			if (binding.getResourceType() == DescriptorResourceType::IMAGE) {
				VkTexture* texture = reinterpret_cast<VkTexture*>(binding.getResource());
				textures.push_back(std::make_tuple(binding.getIdentifier(), texture->getIdentifier()));
			}
			else if (binding.getResourceType() == DescriptorResourceType::BUFFER) {
				if (binding.getIdentifier().compare("material") == 0) continue;

				Buffer* buffer = reinterpret_cast<Buffer*>(binding.getResource());
				buffers.push_back(std::make_tuple(binding.getIdentifier(), buffer->identifier));
			}
		}
	}

	for (auto& texture : textures) {
		std::string tex = std::get<1>(texture);
		utils::string::replaceAll(tex, "\\", "/");
		node["Render"]["material"]["textures"][std::get<0>(texture)] = tex;
	}

	for (auto& buffer : buffers) {
		node["Render"]["material"]["buffers"][std::get<0>(buffer)] = std::get<1>(buffer);
	}

	if (material->getMemoryPtr()) {
		std::vector<float> mem;
		float* ptr = static_cast<float*>(material->getMemoryPtr());

		for (uint32_t i = 0; i < material->getMemoryBlockDescription().size / sizeof(float); i++) {
			mem.push_back(*ptr);
			ptr++;
		}

		node["Render"]["material"]["memory"] = mem;
	}
}

void grail::nodeComponents::Render::load(nlohmann::json& node) {
	mesh = Resources::getMesh(node["mesh"]);

	if (!mesh) mesh = Resources::getMesh("sphere");

	render = node["render"];

	MaterialDescription* description = Resources::getMaterial(node["material"]["base"]);
	Material* derived = nullptr;

	if (node["material"].find("key") != node["material"].end()) {
		if (description->materialExists(node["material"]["key"])) {
			derived = description->getMaterial(node["material"]["key"]);
		}
		else {
			derived = description->getBaseMaterial();
		}
	}
	else {
		derived = description->getBaseMaterial();
	}

	material = derived->createInstance();

	for (auto node : node["material"]["textures"].items()) {
		VkTexture* texture = Resources::getTexture(node.value());

		if (!texture)
			texture = Resources::getTexture("red");

		material->setDescriptorBinding(node.key(), texture, &texture->getDescriptor());
	}

	for (auto node : node["material"]["buffers"].items()) {
		if (node.key().compare("material") == 0) continue;

		Buffer* buffer = Resources::getBuffer(node.value());

		if(buffer)
			material->setDescriptorBinding(node.key(), buffer, &buffer->getDescriptor());
	}

	if (node["material"].find("memory") != node["material"].end()) {
		if (material->getMemoryPtr()) {
			std::vector<float> mem = node["material"]["memory"].get<std::vector<float>>();
			float* ptr = static_cast<float*>(material->getMemoryPtr());

			memcpy(ptr, &mem[0], std::min(mem.size() * sizeof(float), static_cast<size_t>(material->getMemoryBlockDescription().size)));
		}
	}

	material->updateAllDescriptors();
}

void grail::nodeComponents::Render::copy(NodeComponent * src) {
	dispose();

	Render* c = dynamic_cast<Render*>(src);

	this->mesh = c->mesh;
	
	this->material = c->material->getBaseMaterial()->createInstance();
	this->material->copyFrom(c->material);
}

void grail::nodeComponents::Render::dispose() {
	mesh = nullptr;
	
	if (material) {
		material->dispose();
		delete material;
	}
}
void grail::nodeComponents::ScriptComponent::setScript(Script* script) {
	if (this->script)
		cleanup();

	this->script = script;

	instanciate();
}

/*void grail::nodeComponents::ScriptComponent::setup() {
	if (!script) return;

	if (!context) {
		context = script->getEngine()->CreateContext();
	}

	if (classInstance) { classInstance->Release(); classInstance = nullptr; }

	asITypeInfo* type = nullptr;
	int tc = script->getModule()->GetObjectTypeCount();

	for (int i = 0; i < tc; i++) {
		type = script->getModule()->GetObjectTypeByIndex(i);

		if (type->GetBaseType()) {
			if (strcmp(type->GetBaseType()->GetName(), "NodeBehaviour") == 0) {
				if (strcmp(type->GetName(), script->getName().c_str()) == 0) {
					updateFunction = type->GetMethodByDecl("void update(float)");
					startFunction = type->GetMethodByDecl("void start()");

					/*asIScriptObject* obj = reinterpret_cast<asIScriptObject*>(script->getEngine()->CreateScriptObject(type));
					classInstance = obj;*/


					/*asIScriptFunction* constructor = type->GetFactoryByIndex(0);
					context->Prepare(constructor);
					context->Execute();
					asIScriptObject* obj = *((asIScriptObject**)context->GetAddressOfReturnValue());
					obj->AddRef();
					classInstance = obj;
					context->Unprepare();



					int varCount = obj->GetPropertyCount();
					for (int v = 0; v < varCount; v++) {
						if (strcmp(obj->GetPropertyName(v), "node") == 0) {
							SceneNode** address = (SceneNode**)obj->GetAddressOfProperty(v);
							*address = node;
						}
						else {
							/*const char* name = nullptr;
							int typeID;
							bool isPrivate;
							bool isProtected;
							int offset;
							bool isReference;
							asDWORD accessMask;
							int compositeOffset;
							bool isCompositeIndirect;

							type->GetProperty(v, &name, &typeID, &isPrivate, &isProtected, &offset, &isReference, &accessMask, &compositeOffset, &isCompositeIndirect);


							if (!isPrivate && !isProtected) {
								//std::vector<std::string> metadata = builder.GetMetadataForTypeProperty(obj->GetTypeId(), v);
								//if (metadata.size() > 0) {
									//for (std::string& data : metadata) {
										//if (utils::string::equalsCaseInsensitive(data, "visible")) {
										//}
									//}
								//}

								SerializedScriptVar var;
								var.name = obj->GetPropertyName(v);
								var.address = obj->GetAddressOfProperty(v);

								int typeID = obj->GetPropertyTypeId(v);

								if (typeID == asTYPEID_FLOAT) {
									var.type = ScriptVarType::FLOAT;
								}

								else if (typeID == asTYPEID_INT32) {
									var.type = ScriptVarType::INT;
								}

								else if (typeID == asTYPEID_BOOL) {
									var.type = ScriptVarType::BOOL;
								}

								else if (typeID == script->getEngine()->GetTypeIdByDecl("string")) {
									var.type = ScriptVarType::STRING;
								}

								else if (typeID == script->getEngine()->GetTypeIdByDecl("Vec2")) {
									var.type = ScriptVarType::VEC2;
								}

								else if (typeID == script->getEngine()->GetTypeIdByDecl("Vec3")) {
									var.type = ScriptVarType::VEC3;
								}

								else if (typeID == script->getEngine()->GetTypeIdByDecl("Vec4")) {
									var.type = ScriptVarType::VEC4;
								}
								else if (typeID == script->getEngine()->GetTypeIdByDecl("Node") && isReference) {
									var.type = ScriptVarType::NODE;
								}
								else {
									var.type = ScriptVarType::UNKNOWN;
								}

								serializedVars.push_back(var);
							}*/
					/*	}
					}
				}
			}
		}
	}
}*/

CScriptHandle grail::nodeComponents::ScriptComponent::get() {
	CScriptHandle handle;
	handle.Set(classInstance, classInstance->GetObjectType());

	return handle;
}

bool grail::nodeComponents::ScriptComponent::canCallFunc() {
	if (script && instanciated) {
		if (context && classInstance && script->isCompiled()) {
			return true;
		}
	}

	return false;
}

void grail::nodeComponents::ScriptComponent::cleanup() {
	if (context) { context->Release(); context = nullptr; }

	if (classInstance) { classInstance->Release(); classInstance = nullptr; }

	script = nullptr;
	instanciated = false;
}

void grail::nodeComponents::ScriptComponent::serializeScriptVar(SerializedScriptVar& var, nlohmann::json& node) {
	std::vector<float> data;
	SceneNode* ref = nullptr;

	node["type"] = (int)var.type;

	switch (var.type)
	{
	case ScriptVarType::INT:
		node["val"] = *reinterpret_cast<int*>(var.address);
		break;
	case ScriptVarType::FLOAT:
		node["val"] = *reinterpret_cast<float*>(var.address);
		break;
	case ScriptVarType::BOOL:
		node["val"] = *reinterpret_cast<bool*>(var.address);
		break;
	case ScriptVarType::DOUBLE:
		node["val"] = *reinterpret_cast<double*>(var.address);
		break;
	case ScriptVarType::STRING:
		node["val"] = reinterpret_cast<std::string*>(var.address)->c_str();
		break;
	case ScriptVarType::VEC2:
		for (int i = 0; i < 2; i++) { data.push_back(glm::value_ptr(*reinterpret_cast<glm::vec2*>(var.address))[i]); }
		node["val"] = data;
		break;
	case ScriptVarType::VEC3:
		for (int i = 0; i < 3; i++) { data.push_back(glm::value_ptr(*reinterpret_cast<glm::vec2*>(var.address))[i]); }
		node["val"] = data;
		break;
	case ScriptVarType::VEC4:
		for (int i = 0; i < 4; i++) { data.push_back(glm::value_ptr(*reinterpret_cast<glm::vec2*>(var.address))[i]); }
		node["val"] = data;
		break;
	case ScriptVarType::NODE:
		ref = (*reinterpret_cast<SceneNode**>(var.address));

		if (ref)
			node["val"] = ref->name;
		else
			node["type"] = 0;
		break;
	default:
		break;
	}
}

void grail::nodeComponents::ScriptComponent::deserializeScriptVar(const std::string& varName, nlohmann::json& node) {
	SerializedScriptVar v;

	ScriptVarType type = (ScriptVarType)node["type"].get<int>();
	v.type = type;
	v.name = varName;

	std::vector<float> data;
	switch (type)
	{
	case ScriptVarType::INT:
		v.value = malloc(sizeof(int));
		*reinterpret_cast<int*>(v.value) = node["val"].get<int>();
		break;
	case ScriptVarType::FLOAT:
		v.value = malloc(sizeof(float));
		*reinterpret_cast<float*>(v.value) = node["val"].get<float>();
		break;
	case ScriptVarType::BOOL:
		v.value = malloc(sizeof(bool));
		*reinterpret_cast<bool*>(v.value) = node["val"].get<bool>();
		break;
	case ScriptVarType::DOUBLE:
		v.value = malloc(sizeof(double));
		*reinterpret_cast<double*>(v.value) = node["val"].get<double>();
		break;
	case ScriptVarType::STRING:
		v.value = new std::string();
		reinterpret_cast<std::string*>(v.value)->assign(node["val"].get<std::string>());
		break;
	case ScriptVarType::VEC2:
		v.value = malloc(sizeof(glm::vec2));
		data = node["val"].get<std::vector<float>>();
		memcpy(reinterpret_cast<float*>(v.value), &data[0], sizeof(glm::vec2));
		break;
	case ScriptVarType::VEC3:
		v.value = malloc(sizeof(glm::vec3));
		data = node["val"].get<std::vector<float>>();
		memcpy(reinterpret_cast<float*>(v.value), &data[0], sizeof(glm::vec3));
		break;
	case ScriptVarType::VEC4:
		v.value = malloc(sizeof(glm::vec4));
		data = node["val"].get<std::vector<float>>();
		memcpy(reinterpret_cast<float*>(v.value), &data[0], sizeof(glm::vec4));
		break;
	case ScriptVarType::NODE:
		v.value = new std::string();
		GRAIL_LOG(INFO, "VARIABLE") << varName;
		reinterpret_cast<std::string*>(v.value)->assign(node["val"].get<std::string>());
		break;
	default:
		break;
	}

	serializedVals.push_back(v);
}

void grail::nodeComponents::ScriptComponent::instanciate() {
	if (!script) return;
	if (!script->isCompiled()) return;
	if (instanciated) return;

	//GRAIL_LOG(INFO, "?!?!?!?!") << "instanciating - " << node->name;

	serializedVars.clear();

	if (!context) {
		context = script->getEngine()->CreateContext();
	}

	if (classInstance) { 
		classInstance->Release(); 
		classInstance = nullptr;
	}

	asITypeInfo* type = nullptr;
	int tc = script->getModule()->GetObjectTypeCount();

	for (int i = 0; i < tc; i++) {
		type = script->getModule()->GetObjectTypeByIndex(i);

		if (type->GetBaseType()) {
			if (strcmp(type->GetBaseType()->GetName(), "NodeBehaviour") == 0) {
				if (strcmp(type->GetName(), script->getName().c_str()) == 0) {
					updateFunction = type->GetMethodByDecl("void update(float)");
					startFunction = type->GetMethodByDecl("void start()");

					asIScriptFunction* constructor = type->GetFactoryByIndex(0);
					context->Prepare(constructor);
					context->Execute();
					asIScriptObject* obj = *((asIScriptObject**)context->GetAddressOfReturnValue());
					obj->AddRef();
					classInstance = obj;
					context->Unprepare();

					int varCount = obj->GetPropertyCount();
					for (int v = 0; v < varCount; v++) {
						if (strcmp(obj->GetPropertyName(v), "node") == 0) {
							SceneNode** address = (SceneNode**)obj->GetAddressOfProperty(v);
							*address = node;
						}
						else {
							const char* name = nullptr;
							int typeID;
							bool isPrivate;
							bool isProtected;
							int offset;
							bool isReference;
							asDWORD accessMask;
							int compositeOffset;
							bool isCompositeIndirect;

							type->GetProperty(v, &name, &typeID, &isPrivate, &isProtected, &offset, &isReference, &accessMask, &compositeOffset, &isCompositeIndirect);

							if (!isPrivate && !isProtected) {
								//std::vector<std::string> metadata = builder.GetMetadataForTypeProperty(obj->GetTypeId(), v);
								//if (metadata.size() > 0) {
									//for (std::string& data : metadata) {
										//if (utils::string::equalsCaseInsensitive(data, "visible")) {
										//}
									//}
								//}

								SerializedScriptVar var;
								var.name = obj->GetPropertyName(v);
								var.address = obj->GetAddressOfProperty(v);

								int typeID = obj->GetPropertyTypeId(v);

								int varSize = 0;

								if (typeID == asTYPEID_FLOAT) {
									var.type = ScriptVarType::FLOAT;
									varSize = sizeof(float);
								}

								else if (typeID == asTYPEID_INT32) {
									var.type = ScriptVarType::INT;
									varSize = sizeof(int);
								}

								else if (typeID == asTYPEID_BOOL) {
									var.type = ScriptVarType::BOOL;
									varSize = sizeof(bool);
								}

								else if (typeID == script->getEngine()->GetTypeIdByDecl("string")) {
									var.type = ScriptVarType::STRING;
									varSize = sizeof(std::string);
								}

								else if (typeID == script->getEngine()->GetTypeIdByDecl("Vec2")) {
									var.type = ScriptVarType::VEC2;
									varSize = sizeof(glm::vec2);
								}

								else if (typeID == script->getEngine()->GetTypeIdByDecl("Vec3")) {
									var.type = ScriptVarType::VEC3;
									varSize = sizeof(glm::vec3);
								}

								else if (typeID == script->getEngine()->GetTypeIdByDecl("Vec4")) {
									var.type = ScriptVarType::VEC4;
									varSize = sizeof(glm::vec4);
								}
								else if (typeID == script->getEngine()->GetTypeIdByDecl("Node@")) {
									var.type = ScriptVarType::NODE;
									varSize = sizeof(std::string);
								}
								else {
									var.type = ScriptVarType::UNKNOWN;
								}

								if(var.type != ScriptVarType::UNKNOWN)
									serializedVars.push_back(var);

								for (SerializedScriptVar& saved : serializedVals) {
									if (saved.value) {
										if (saved.type == var.type) {
											if (saved.name.compare(var.name) == 0) {
												if (saved.type != ScriptVarType::NODE) {
													memcpy(var.address, saved.value, varSize);
												}
												else {
													SceneNode** ref = reinterpret_cast<SceneNode**>(var.address);
													*ref = grail::Scene::getNode(*reinterpret_cast<std::string*>(saved.value));
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	instanciated = true;
}

grail::Script* grail::nodeComponents::ScriptComponent::getScript() {
	return script;
}

void grail::nodeComponents::ScriptComponent::callUpdateFunc(float delta) {
	if (canCallFunc()) {
		if (updateFunction) {
			context->Prepare(updateFunction);
			context->SetObject(classInstance);
			context->SetArgFloat(0, delta);
			int result = context->Execute();

			/*if(result == asEXECUTION_EXCEPTION) {
				GRAIL_LOG(ERROR, "SCRIPT EXCEPTION") << "Exception while executing update function in " << this->script->getName() << " on node " << node->name << ": [" << context->GetExceptionLineNumber() << "] " << context->GetExceptionString();

			}*/
			//context->Unprepare();
		}
	}
}

void grail::nodeComponents::ScriptComponent::callStartFunc() {
	if (canCallFunc()) {
		if (startFunction) {
			context->Prepare(startFunction);
			context->SetObject(classInstance);
			int result = context->Execute();
			//context->Unprepare();


			/*if (result == asEXECUTION_EXCEPTION) {
				GRAIL_LOG(ERROR, "SCRIPT EXCEPTION") << "Exception while executing start function in " << this->script->getName() << " on node " << node->name << ": [" << context->GetExceptionLineNumber() << "] " << context->GetExceptionString();

			}*/
		}
	}
}

void grail::nodeComponents::ScriptComponent::save(nlohmann::json & node) {
	node["Script"]["name"] = "Script";
	node["Script"]["script"] = script ? script->getName() : "";

	if (script && serializedVars.size() > 0) {
		node["Script"]["data"] = nlohmann::json::object();
		for (SerializedScriptVar& var : serializedVars) {
			node["Script"]["data"][var.name] = nlohmann::json::object();
			serializeScriptVar(var, node["Script"]["data"][var.name]);
		}
	}
}

void grail::nodeComponents::ScriptComponent::load(nlohmann::json & node) {
	if (node["script"].size() > 0) {
		if (node.find("data") != node.end()) {
			for (auto node : node["data"].items()) {
				deserializeScriptVar(node.key(), node.value());
			}
		}

		setScript(Resources::getScript(node["script"]));
	}
}

void grail::nodeComponents::ScriptComponent::copy(NodeComponent * src) {
	cleanup();

	ScriptComponent* c = dynamic_cast<ScriptComponent*>(src);
	this->setScript(c->getScript());
}

void grail::nodeComponents::ScriptComponent::dispose() {
	cleanup();

	for (SerializedScriptVar& v : serializedVals) {
		if (v.value) {
			free(v.value);
			v.value = nullptr;
		}
	}

	serializedVals.clear();
	serializedVars.clear();
}

/*float grail::nodeComponents::PhysicsComponent::getFriction() {
	return body->getFriction();
}

void grail::nodeComponents::PhysicsComponent::setFriction(float friction) {
	body->setFriction(friction);
}

float grail::nodeComponents::PhysicsComponent::getMass() {
	return body->getMass();
}

void grail::nodeComponents::PhysicsComponent::setMass(float mass) {
	btVector3 localInertia;

	Physics::dynamicsWorld->removeRigidBody(body);

	if (mass > 0.0) {
		body->getCollisionShape()->calculateLocalInertia(mass, localInertia);
		body->setActivationState(DISABLE_DEACTIVATION);
	}
	else {
		// If body is kinematic
		body->setActivationState(ISLAND_SLEEPING);
	}
	body->setMassProps(mass, localInertia);
	body->updateInertiaTensor();
	body->clearForces();

	Physics::dynamicsWorld->addRigidBody(body);
}

float grail::nodeComponents::PhysicsComponent::getRestitution() {
	return body->getRestitution();
}

void grail::nodeComponents::PhysicsComponent::setRestitution(float restitution) {
	body->setRestitution(restitution);
}

float grail::nodeComponents::PhysicsComponent::getRollingFriction() {
	return body->getRollingFriction();
}

void grail::nodeComponents::PhysicsComponent::setRollingFriction(float friction) {
	body->setRollingFriction(friction);
}

glm::vec3 grail::nodeComponents::PhysicsComponent::getAngularFactor() {
	btVector3 factor = body->getAngularFactor();

	return glm::vec3(factor.getX(), factor.getY(), factor.getZ());
}

void grail::nodeComponents::PhysicsComponent::setAngularFactor(glm::vec3 factor) {
	body->setAngularFactor(btVector3(factor.x, factor.y, factor.z));
}

void grail::nodeComponents::PhysicsComponent::setAngularFactor(float factor) {
	body->setAngularFactor(factor);
}

glm::vec3 grail::nodeComponents::PhysicsComponent::getLinearFactor() {
	btVector3 factor = body->getLinearFactor();

	return glm::vec3(factor.getX(), factor.getY(), factor.getZ());
}

void grail::nodeComponents::PhysicsComponent::setLinearFactor(glm::vec3 factor) {
	body->setLinearFactor(btVector3(factor.x, factor.y, factor.z));
}

void grail::nodeComponents::PhysicsComponent::setLinearFactor(float factor) {
	setLinearFactor(glm::vec3(factor));
}

void grail::nodeComponents::PhysicsComponent::syncTransform(bool clearForces) {
	glm::vec3 position = node->transform.getWorldPosition();
	glm::quat rotation = node->transform.getWorldRotation();
	glm::vec3 scale = node->transform.getWorldScale();

	shape->setLocalScaling(btVector3(scale.x, scale.y, scale.z));

	btTransform transform;
	transform.setOrigin(btVector3(position.x, position.y, position.z));
	transform.setRotation(btQuaternion(
		(rotation.x),
		(rotation.y),
		(rotation.z),
		(rotation.w)));
	body->setWorldTransform(transform);
	body->getMotionState()->setWorldTransform(transform);

	if (clearForces) {
		body->setLinearVelocity(btVector3(0.0f, 0.0f, 0.0f));
		body->setAngularVelocity(btVector3(0.0f, 0.0f, 0.0f));
		body->clearForces();
	}
}

void grail::nodeComponents::PhysicsComponent::setShape(btCollisionShape* newShape) {
	if (shape) {
		delete shape;
	}

	body->setCollisionShape(newShape);
	shape = newShape;

	syncTransform();
}

void grail::nodeComponents::PhysicsComponent::createCapsuleShape(float radius, float height) {
	float r = radius;
	float h = height;
	
	if (r == 0 && h == 0) {
		glm::vec3 size = glm::abs(node->transform.getLocalBoundsMin() - node->transform.getLocalBoundsMax()) * 0.5f;
		r = glm::max(size.x, size.y);
		h = size.y;
	}
		 
	setShape(new btCapsuleShape(r, h));
}

void grail::nodeComponents::PhysicsComponent::createConvexHullShape() {
	nodeComponents::Render* r = (nodeComponents::Render*)node->getComponentFromMap("Render");
	if (r) {
		std::vector<float> points;

		btConvexHullShape convexHull = btConvexHullShape();
		for (int i = 0; i < r->mesh->verticesCount; i += 12) {
			convexHull.addPoint(btVector3(
				r->mesh->vertices[i + 0],
				r->mesh->vertices[i + 1],
				r->mesh->vertices[i + 2])
			);
		}

		convexHull.setMargin(0);

		btConvexHullShape* shape = new btConvexHullShape(convexHull);
		setShape(shape);
	}
}

void grail::nodeComponents::PhysicsComponent::applyCentralForce(glm::vec3 force) {
	body->applyCentralForce(btVector3(force.x, force.y, force.z));
}*/

void grail::nodeComponents::PhysicsComponent::save(nlohmann::json& node) {
}

void grail::nodeComponents::PhysicsComponent::load(nlohmann::json& node) {
}

void grail::nodeComponents::PhysicsComponent::copy(NodeComponent* src) {
}

void grail::nodeComponents::PhysicsComponent::dispose() {
	/*if (shape) {
		delete shape;
	}

	if (body) {
		Physics::dynamicsWorld->removeRigidBody(body);
		delete body;
	}*/
}
