#include "FrameGraph.h"
#include "Swapchain.h"
#include "ShaderLoader.h"
#include "Resources.h"
#include "Material.h"
#include "VkTexture.h"
#include "VkBuffer.h"
#include "MaterialInstance.h"
#include "Mesh.h"

grail::FrameGraph::FrameGraph(GPU gpu) {
	this->gpu = gpu;

	commandPools = std::vector<VkCommandPool>(3); // Perhaps not hardcode this value

	VkCommandPoolCreateInfo poolInfo = {};
	poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	poolInfo.pNext = nullptr;
	poolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT; // will be reset after a frame, look into transient bit or protected bit as well
	
	// Graphics queue
	poolInfo.queueFamilyIndex = gpu.graphicsQueue.index;
	VK_VALIDATE_RESULT(vkCreateCommandPool(gpu.device, &poolInfo, nullptr, &commandPools[static_cast<uint32_t>(CommandPoolType::GRAPHICS)]));


	// Compute queue
	poolInfo.queueFamilyIndex = gpu.computeQueue.index;
	VK_VALIDATE_RESULT(vkCreateCommandPool(gpu.device, &poolInfo, nullptr, &commandPools[static_cast<uint32_t>(CommandPoolType::COMPUTE)]));


	// Transfer queue
	poolInfo.queueFamilyIndex = gpu.transferQueue.index;
	VK_VALIDATE_RESULT(vkCreateCommandPool(gpu.device, &poolInfo, nullptr, &commandPools[static_cast<uint32_t>(CommandPoolType::TRANSFER)]));

	///NOTE: Implement checks for the existance of compute and transfer queues


	// Create sync semaphores for the beginning and end of frame
	VkSemaphoreCreateInfo semaphoreInfo = {};
	semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
	semaphoreInfo.pNext = nullptr;

	VkFenceCreateInfo fenceInfo = {};
	fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	fenceInfo.pNext = nullptr;
	fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;
	//VK_VALIDATE_RESULT(vkCreateSemaphore(gpu.device, &semaphoreInfo, nullptr, &renderFinishedSemaphore));

	frameFences.resize(framesInFlight * 3);
	renderAvailableSemaphores.resize(framesInFlight);
	renderFinishedSemaphores.resize(framesInFlight);

	// Create a command buffer to transition all of the pass outputs to attachment layout
	transitionBuffers.resize(framesInFlight);
	for (uint32_t i = 0; i < transitionBuffers.size(); i++) {
		transitionBuffers[i] = allocateCommandBuffer(CommandPoolType::GRAPHICS);
		VK_VALIDATE_RESULT(vkCreateSemaphore(gpu.device, &semaphoreInfo, nullptr, &renderAvailableSemaphores[i]));
		VK_VALIDATE_RESULT(vkCreateSemaphore(gpu.device, &semaphoreInfo, nullptr, &renderFinishedSemaphores[i]));
	}

	// One fence per queue per frame
	for (uint32_t i = 0; i < frameFences.size(); i++) {
		VK_VALIDATE_RESULT(vkCreateFence(gpu.device, &fenceInfo, nullptr, &frameFences[i]));
	}
}

grail::RenderPass & grail::FrameGraph::createPass(std::string name, FrameGraphQueueBits queue) {
	RenderPass* pass = new RenderPass();
	pass->name = name;
	pass->index = renderPasses.size();
	pass->queueFlags = queue;
	pass->graph = this;

	this->renderPasses.push_back(pass);

	return *this->renderPasses[this->renderPasses.size() - 1];
}

grail::RenderAttachment& grail::FrameGraph::createAttachment(const std::string& name, AttachmentInfo& info) {
	RenderAttachment* attachment = new RenderAttachment();
	attachment->properties = AttachmentInfo(info);
	attachment->index = this->attachments.size();
	attachment->name = name;

	TextureCreateInfo tInfo = {};
	tInfo.format = info.format;
	tInfo.generateMipmaps = info.mipmapped;
	if (info.initialLayout == ImageLayout::UNDEFINED)
		tInfo.initialLayout = (VulkanContext::isDepthStencil(info.format) ? ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL : ImageLayout::COLOR_ATTACHMENT_OPTIMAL);
	else
		tInfo.initialLayout = info.initialLayout;

	if (info.faces > 1) {
		tInfo.createFlags |= VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;
		tInfo.arrayLevels = info.faces;
	}
	else {
		tInfo.arrayLevels = info.layers;
	}

	tInfo.samplerInfo.anisotropyEnable = false;
	tInfo.samplerInfo.maxAnisotropy = 1.0f;
	tInfo.samplerInfo.autoMaxLod = false;
	tInfo.samplerInfo.mipLodBias = 0.0f;
	tInfo.samplerInfo.minLod = 0.0f;
	tInfo.samplerInfo.maxLod = 1.0f;
	tInfo.samplerInfo.magFilter = info.magFilter;
	tInfo.samplerInfo.minFilter = info.minFilter;
	tInfo.samplerInfo.mipmapMode = SamplerMipmapMode::LINEAR;
	tInfo.samplerInfo.compareEnable = info.samplerCompareEnable;
	tInfo.samplerInfo.compareOp = info.samplerCompareOP;

	tInfo.samplerInfo.addressModeU = SamplerAddressMode::CLAMP_TO_EDGE;
	tInfo.samplerInfo.addressModeV = SamplerAddressMode::CLAMP_TO_EDGE;
	tInfo.samplerInfo.addressModeW = SamplerAddressMode::CLAMP_TO_EDGE;

	tInfo.usageBits = 
		(
			VulkanContext::isDepthStencil(info.format) ?
			ImageUsageBits::DEPTH_STENCIL_ATTACHMENT_BIT : 
			ImageUsageBits::COLOR_ATTACHMENT_BIT
		) |
		ImageUsageBits::SAMPLED_BIT | ImageUsageBits::TRANSFER_DST_BIT | ImageUsageBits::TRANSFER_SRC_BIT;

	attachment->texture = Resources::createTexture(name, info.width, info.height, tInfo);

	this->attachments.push_back(attachment);
	
	return *this->attachments[this->attachments.size() - 1];
}

void grail::FrameGraph::buildGraph() {
	{ std::string composite = ""; for (RenderPass* pass : renderPasses) { composite += pass->name + " -> "; } /*GRAIL_LOG(INFO, "RENDER GRAPH") << "Initial Order: \n\t" << composite;*/ }

	// Oder all of the passes based on dependancies
	// NOTE: Implement shifting of ordered steps to have less synchronization time between different queues
	// Start from the backbuffer and go upwards
	std::vector<uint32_t> order;

	std::function<void(RenderPass*)> compute = [&](RenderPass* pass) {
		order.push_back(pass->index);
		for (uint32_t attachmentIndex : pass->inputs) {
			RenderAttachment* attachment = attachments[attachmentIndex];
			for (uint32_t i : attachment->writtenInPasses) {
				if(pass->index != i)
					compute(renderPasses[i]);
			}
		}
	};

	// If there is a present pass, find it and compute order from it 
	for (RenderPass* pass : renderPasses) {
		if (static_cast<uint32_t>(pass->queueFlags) & static_cast<uint32_t>(FrameGraphQueueBits::PRESENT)) {
			compute(pass);
		}
	}

	// If there was no present pass, use a set final pass to compute order
	if (order.size() == 0) {
		compute(renderPasses[finalPass]);
	}

	std::reverse(order.begin(), order.end());

	std::vector<uint32_t> sorted;

	for (uint32_t index : order) {
		if (std::find(sorted.begin(), sorted.end(), index) == sorted.end()) {
			sorted.push_back(index);
		}
	}

	{ std::string composite = ""; for (uint32_t pass : sorted) { composite += renderPasses[pass]->name + " -> "; } /*GRAIL_LOG(INFO, "RENDER GRAPH") << "Computed Order: \n\t" << composite;*/  }

	renderPassOrder = sorted;

	/// Initialize NOTE: unusable at the moment
	/// Slightly more usable
	for (uint32_t pass_id : sorted) {
		RenderPass* pass = renderPasses[pass_id];
		
		// Mark active passes for the purpose of material creation
		pass->active = true;
		pass->primaryCmdBuffers.resize(framesInFlight); // Does this needs to be user definable?
		
		// Resize timing vectors
		pass->cpuExecutionTimes.resize(pass->timeMetricSampleCount);
		std::fill(pass->cpuExecutionTimes.begin(), pass->cpuExecutionTimes.end(), 0.0);

		pass->gpuExecutionTimes.resize(pass->timeMetricSampleCount);
		std::fill(pass->gpuExecutionTimes.begin(), pass->gpuExecutionTimes.end(), 0.0);
		
		// NOTE: perhaps merge graphics and present queue buffer allocation
		CommandPoolType commandPoolType = CommandPoolType::GRAPHICS;

		if (static_cast<uint32_t>(pass->queueFlags) & static_cast<uint32_t>(FrameGraphQueueBits::ASYNC_COMPUTE) || static_cast<uint32_t>(pass->queueFlags) & static_cast<uint32_t>(FrameGraphQueueBits::COMPUTE)) {
			commandPoolType = CommandPoolType::COMPUTE;
		}

		for (uint32_t i = 0; i < pass->primaryCmdBuffers.size(); i++) {
			pass->primaryCmdBuffers[i] = allocateCommandBuffer(commandPoolType);
		}

		// Setup render passes NOTE: unusable
		if (pass->queueFlags != FrameGraphQueueBits::PRESENT) {
			std::vector<VkAttachmentReference> colorAttachments;
			colorAttachments.clear();
			VkAttachmentReference depthAttachment = { UINT32_MAX, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL };

			uint32_t a_index = 0;
			if (pass->depthStencilOutput != UINT32_MAX) {
				depthAttachment.attachment = a_index++;
			}
			
			for (uint32_t index : pass->outputs) {
				VkAttachmentReference ref = {};
				ref.attachment = a_index++;
				ref.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

				colorAttachments.push_back(ref);
			}

			VkPipelineBindPoint bindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;

			VkSubpassDescription subpass = {};
			subpass.pipelineBindPoint = bindPoint;
			if (colorAttachments.size() != 0) {
				subpass.pColorAttachments = &colorAttachments[0];
				subpass.colorAttachmentCount = colorAttachments.size();
			}
			else {
				subpass.colorAttachmentCount = 0;
				subpass.pColorAttachments = nullptr;
			}
			if (depthAttachment.attachment != UINT32_MAX) {
				subpass.pDepthStencilAttachment = &depthAttachment;
			}
			subpass.inputAttachmentCount = 0;
			subpass.preserveAttachmentCount = 0;

			std::vector<VkSubpassDependency> dependencies;
			dependencies.resize(2);

			dependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
			dependencies[0].dstSubpass = 0;
			dependencies[0].srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
			dependencies[0].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
			dependencies[0].srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
			dependencies[0].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
			dependencies[0].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

			dependencies[1].srcSubpass = 0;
			dependencies[1].dstSubpass = VK_SUBPASS_EXTERNAL;
			dependencies[1].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
			dependencies[1].dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
			dependencies[1].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
			dependencies[1].dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
			dependencies[1].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

			std::vector<VkAttachmentDescription> descriptions;
			descriptions.clear();

			std::vector<uint32_t> mergedOutputs;
			if (pass->depthStencilOutput != UINT32_MAX) {
				mergedOutputs.push_back(pass->depthStencilOutput);
			}
			mergedOutputs.insert(mergedOutputs.end(), pass->outputs.begin(), pass->outputs.end());
						
			for (uint32_t index : mergedOutputs) {
				RenderAttachment* attachment = attachments[index];

				pass->width = attachment->texture->getWidth();
				pass->height = attachment->texture->getHeight();

				// Find which pass in the chain this attachment belongs to
				uint32_t passIndex = 0;
				for (uint32_t index : sorted) {
					if (index == pass->index) {
						break;
					}
					else
						passIndex++;
				}

				bool isDepthAttachment = VulkanContext::isDepthStencil(attachment->properties.format);

				VkAttachmentDescription desc = {};
				desc.flags = 0;
				desc.samples = VK_SAMPLE_COUNT_1_BIT; // NOTE: dont't hardcode this

				desc.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
				desc.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;

				uint32_t firstUse = UINT32_MAX;
				for (uint32_t i = 0; i < sorted.size(); i++) {
					uint32_t orderIndex = static_cast<size_t>((sorted[passIndex] + i)) % sorted.size();
					uint32_t pass_index = sorted[orderIndex];

					if (std::find(attachment->writtenInPasses.begin(), attachment->writtenInPasses.end(), pass_index) != attachment->writtenInPasses.end()) {
						if (firstUse == UINT32_MAX) {
							firstUse = orderIndex;
							break;
						}
					}
				}

				// Clear at the first use and load on future uses
				desc.loadOp = (attachment->properties.mipmapped ? VK_ATTACHMENT_LOAD_OP_LOAD : (passIndex == firstUse ? VK_ATTACHMENT_LOAD_OP_CLEAR : VK_ATTACHMENT_LOAD_OP_LOAD));

				// Store after every pass NOTE: look into after aliasing and temporary
				desc.storeOp = VK_ATTACHMENT_STORE_OP_STORE;

				// Get the next pass that write to and reads from the attachment
				uint32_t nextWrite = UINT32_MAX;
				uint32_t nextRead = UINT32_MAX;
				for (uint32_t i = 0; i < sorted.size(); i++) {
					// Get the index of the next pass with loop around

					uint32_t orderIndex = static_cast<size_t>((sorted[passIndex] + i)) % sorted.size();
					uint32_t pass_index = sorted[orderIndex];

					if (std::find(attachment->readInPasses.begin(), attachment->readInPasses.end(), pass_index) != attachment->readInPasses.end()) {
						if(nextRead == UINT32_MAX && pass_index != pass->index)
							nextRead = orderIndex;
					}

					if (std::find(attachment->writtenInPasses.begin(), attachment->writtenInPasses.end(), pass_index) != attachment->writtenInPasses.end()) {
						if (nextWrite == UINT32_MAX && pass_index != pass->index)
							nextWrite = orderIndex;
					}
				}


				// Figure out the next op on this attachment during the same frame
				bool nextReadCurrentFrame = true;
				bool nextWriteCurrentFrame = true;

				if (nextRead == UINT32_MAX || nextRead <= passIndex)
					nextReadCurrentFrame = false;

				if (nextWrite == UINT32_MAX || nextWrite <= passIndex)
					nextWriteCurrentFrame = false;

				//GRAIL_LOG(INFO, "INFO") << attachment->name << "READ:" << nextReadCurrentFrame << " WRITE:" << nextWriteCurrentFrame;

				uint32_t readWrite = UINT32_MAX;
				uint32_t closestOpIndex = UINT32_MAX;


				if (nextReadCurrentFrame || nextWriteCurrentFrame) {
					if (nextReadCurrentFrame && nextWriteCurrentFrame) {
						if (nextRead > nextWrite) {
							closestOpIndex = nextRead;
							readWrite = 0;
						}
						else {
							closestOpIndex = nextWrite;
							readWrite = 1;
						}
					}
					else {
						if (nextReadCurrentFrame) {
							closestOpIndex = nextRead;
							readWrite = 0;
						}
						else {
							closestOpIndex = nextWrite;
							readWrite = 1;
						}
					}
				}

				// Initial layout of a attachment should be attachment optimal since it is an output attachment
				desc.initialLayout = isDepthAttachment ? VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL : VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

				// If this is a raytracing pipeline, we expect the input to be in the GENERAL layout, as this is the requirment for storage images
				if (pass->queueFlags == FrameGraphQueueBits::RAYTRACE) {
					desc.initialLayout = VK_IMAGE_LAYOUT_GENERAL;
				}

				if (passIndex == sorted.size() - 1) { 
					// If this is the final pass, transition to attachment for the next use
					desc.finalLayout = isDepthAttachment ? VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL : VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
				}
				else {
					// If the attachment is used later on in the chain
					if (closestOpIndex != UINT32_MAX) {
						// If it is read on the next frame
						if (readWrite == 0) {
							desc.finalLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
						}

						// If it is written to on the next frame
						if (readWrite == 1) {
							desc.finalLayout = isDepthAttachment ? VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL : VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
						}
					}
					else {
						// If the attachment is not used anymore during the frame
						// Transition to shader read layout to make the final layout transition easier

						/// NOTE: this might not be efficient enough and at SOME point definitely needs to be fixed
						desc.finalLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
					}
				}

				//GRAIL_LOG(INFO, "TRANSITION INFO") << attachment->name << " " << closestOpIndex << " " << readWrite << " " << desc.initialLayout << " -> " << desc.finalLayout << "    " << nextRead << "   " << nextWrite;

				attachment->initialLayout = desc.initialLayout;
				attachment->finalLayout = desc.finalLayout;

				desc.format = attachment->properties.format;
				descriptions.push_back(desc);
			}

			VkRenderPassCreateInfo renderPassInfo = {};
			renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
			renderPassInfo.attachmentCount = descriptions.size();
			renderPassInfo.pAttachments = &descriptions[0];
			renderPassInfo.subpassCount = 1;
			renderPassInfo.pSubpasses = &subpass;
			renderPassInfo.dependencyCount = 2;
			renderPassInfo.pDependencies = dependencies.data();
			renderPassInfo.pNext = nullptr;
			VK_VALIDATE_RESULT(vkCreateRenderPass(gpu.device, &renderPassInfo, nullptr, &pass->renderPass));

			std::vector<VkImageView> viewAttachments;
			
			// We create a framebuffer per texture layer to be able to render to all of the layers of the texture
			struct RenderAttachmentRef {
				RenderAttachment* attachment = nullptr;
				uint32_t index = 0;
			};

			std::vector<RenderAttachmentRef> multiLayerAttachments;

			uint32_t position = 0;
			for (uint32_t index : mergedOutputs) {
				RenderAttachment* attachment = attachments[index];
			
				if (attachment->properties.layers == 1) {
					VkImageSubresourceRange range;
					range.baseArrayLayer = 0;
					range.baseMipLevel = 0;
					range.layerCount = 1;
					range.levelCount = 1;
					viewAttachments.push_back(attachment->texture->getImageView(range));
				}
				else
					multiLayerAttachments.push_back({ attachment, position });

				position++;
			}

			// If there are no multilayer attachments, we only need one framebuffer
			if (multiLayerAttachments.size() == 0) {
				VkFramebufferCreateInfo framebufferCreateInfo = {};
				framebufferCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
				framebufferCreateInfo.pNext = nullptr;
				framebufferCreateInfo.renderPass = pass->renderPass;
				framebufferCreateInfo.attachmentCount = viewAttachments.size();
				framebufferCreateInfo.pAttachments = &viewAttachments[0];
				framebufferCreateInfo.width = pass->width;
				framebufferCreateInfo.height = pass->height;
				framebufferCreateInfo.layers = 1;

				pass->framebuffers.resize(1);
				VK_VALIDATE_RESULT(vkCreateFramebuffer(gpu.device, &framebufferCreateInfo, nullptr, &pass->framebuffers[0]));
			}
			else {
				// Else we create the whole permutation set of framebuffers
				// If there is only one multilayer attachment, permutation count is the number of layers
				if (multiLayerAttachments.size() == 1) {
					RenderAttachmentRef ref = multiLayerAttachments[0];

					pass->framebuffers.resize(ref.attachment->properties.layers);
					for (uint32_t layer = 0; layer < ref.attachment->properties.layers; layer++) {
						std::vector<VkImageView> layeredAttachments;
						layeredAttachments.resize(viewAttachments.size() + 1);

						uint32_t position = 0;
						for (uint32_t index = 0; index < viewAttachments.size() + 1; index++) {
							if (position == ref.index) {
								VkImageViewCreateInfo viewInfo = {};
								viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
								viewInfo.pNext = nullptr;
								viewInfo.flags = 0;
								viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D_ARRAY;
								viewInfo.format = ref.attachment->properties.format;
								viewInfo.subresourceRange = {};
								viewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
								viewInfo.subresourceRange.baseMipLevel = 0;
								viewInfo.subresourceRange.levelCount = 1;
								viewInfo.subresourceRange.baseArrayLayer = layer;
								viewInfo.subresourceRange.layerCount = 1;
								viewInfo.image = ref.attachment->texture->getImage();

								VK_VALIDATE_RESULT(vkCreateImageView(gpu.device, &viewInfo, nullptr, &layeredAttachments[index]));
							}
							else {
								layeredAttachments[index] = viewAttachments[position];
								position++;
							}
						}

						VkFramebufferCreateInfo framebufferCreateInfo = {};
						framebufferCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
						framebufferCreateInfo.pNext = nullptr;
						framebufferCreateInfo.renderPass = pass->renderPass;
						framebufferCreateInfo.attachmentCount = layeredAttachments.size();
						framebufferCreateInfo.pAttachments = &layeredAttachments[0];
						framebufferCreateInfo.width = pass->width;
						framebufferCreateInfo.height = pass->height;
						framebufferCreateInfo.layers = 1;

						VK_VALIDATE_RESULT(vkCreateFramebuffer(gpu.device, &framebufferCreateInfo, nullptr, &pass->framebuffers[layer]));
					}
				}
				else {
					// this is actually questionable, throw an exception and run away from your problems
					throw new exceptions::RuntimeException("Multiple multilayered attachments: I don't think this is a good idea");
				}
			}
		}

		if (pass->queueFlags == FrameGraphQueueBits::PRESENT) {
			Swapchain* swapchain = VulkanContext::swapchain;

			pass->width = swapchain->width;
			pass->height = swapchain->height;

			VkAttachmentReference colorAttachment = { 0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL };

			VkSubpassDescription subpass = {};
			subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
			subpass.pColorAttachments = &colorAttachment;
			subpass.colorAttachmentCount = 1;

			std::array<VkSubpassDependency, 2> dependencies;

			dependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
			dependencies[0].dstSubpass = 0;
			dependencies[0].srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
			dependencies[0].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
			dependencies[0].srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
			dependencies[0].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
			dependencies[0].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

			dependencies[1].srcSubpass = 0;
			dependencies[1].dstSubpass = VK_SUBPASS_EXTERNAL;
			dependencies[1].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
			dependencies[1].dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
			dependencies[1].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
			dependencies[1].dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
			dependencies[1].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

			VkAttachmentDescription desc[1];
			desc[0].flags = 0;
			desc[0].samples = VK_SAMPLE_COUNT_1_BIT;
			desc[0].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
			desc[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
			desc[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
			desc[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
			desc[0].format = gpu.colorFormat;
			desc[0].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
			desc[0].finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

			VkRenderPassCreateInfo renderPassInfo = {};
			renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
			renderPassInfo.pAttachments = &desc[0];
			renderPassInfo.attachmentCount = 1;
			renderPassInfo.subpassCount = 1;
			renderPassInfo.pSubpasses = &subpass;
			renderPassInfo.dependencyCount = 2;
			renderPassInfo.pDependencies = dependencies.data();
			VK_VALIDATE_RESULT(vkCreateRenderPass(gpu.device, &renderPassInfo, nullptr, &pass->renderPass));

			VkImageView imgAttachments[1];

			VkFramebufferCreateInfo framebufferCreateInfo = {};
			framebufferCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
			framebufferCreateInfo.pNext = nullptr;
			framebufferCreateInfo.renderPass = pass->renderPass;
			framebufferCreateInfo.attachmentCount = 1;
			framebufferCreateInfo.pAttachments = &imgAttachments[0];
			framebufferCreateInfo.width = swapchain->width;
			framebufferCreateInfo.height = swapchain->height;
			framebufferCreateInfo.layers = 1;

			pass->framebuffers.resize(swapchain->imageCount);
			for (uint32_t i = 0; i < pass->framebuffers.size(); i++) {
				imgAttachments[0] = swapchain->buffers[i].view;
				VK_VALIDATE_RESULT(vkCreateFramebuffer(gpu.device, &framebufferCreateInfo, nullptr, &pass->framebuffers[i]));
			}
		}

		pass->prepareInfos();
		pass->createMaterial();
	}

	// after everything has been initialized, build the query pool
	VkQueryPoolCreateInfo queryPoolCreateInfo = {};
	queryPoolCreateInfo.sType = VK_STRUCTURE_TYPE_QUERY_POOL_CREATE_INFO;
	queryPoolCreateInfo.pNext = nullptr;
	queryPoolCreateInfo.flags = 0;
	queryPoolCreateInfo.queryType = VK_QUERY_TYPE_TIMESTAMP;
	// Two queries [begin, end] per frame per pass
	queryPoolCreateInfo.queryCount = (renderPassOrder.size() * framesInFlight) * 2;
	VK_VALIDATE_RESULT(vkCreateQueryPool(gpu.device, &queryPoolCreateInfo, nullptr, &timestampQueryPool));

	// Reset the pool to make it ready for use
	{
		TemporaryCommandBuffer resetBuffer = gpu.createTemporaryBuffer(gpu.graphicsQueue, VK_COMMAND_BUFFER_LEVEL_PRIMARY);
		resetBuffer.begin();

		vkCmdResetQueryPool(resetBuffer.getHandle(), timestampQueryPool, 0, (renderPassOrder.size() * framesInFlight) * 2);

		resetBuffer.end();
		resetBuffer.submit();
		resetBuffer.dispose();
	}
}

void grail::FrameGraph::buildFrame(Swapchain* swapchain) {
	//GRAIL_LOG(DEBUG, "BUILT") << frameIndex;
	std::vector<VkFence> waitFences;

	if (perFrameSubmitGraphics.size() > 0) {
		waitFences.push_back(frameFences[frameIndex * 3 + 0]);
	}

	if (perFrameSubmitCompute.size() > 0) {
		waitFences.push_back(frameFences[frameIndex * 3 + 1]);
	}

	if (perFrameSubmitTransfer.size() > 0) {
		waitFences.push_back(frameFences[frameIndex * 3 + 2]);
	}

	if(waitFences.size() > 0)
		VK_VALIDATE_RESULT(vkWaitForFences(gpu.device, waitFences.size(), &waitFences[0], VK_TRUE, UINT64_MAX));

	perFrameSubmitGraphics.clear();
	perFrameSubmitCompute.clear();
	perFrameSubmitTransfer.clear();

	if (swapchain) {
		VK_VALIDATE_RESULT(swapchain->aquireNextImage(renderAvailableSemaphores[frameIndex], &swapchainImageIndex, VK_NULL_HANDLE));
	}

	// First build and fill the command buffers of each pass
	uint32_t passNum = 0;
	for (uint32_t index : renderPassOrder) {
		auto startTime = std::chrono::high_resolution_clock::now();

		RenderPass* pass = renderPasses[index];

		// Store the current frame index in the pass to retrieve the current timings
		pass->frameIndex = frameIndex;

		// Setup the initial state

		// NOTE: don't hardcode this
		VkCommandBuffer buffer = pass->primaryCmdBuffers[frameIndex];

		VkCommandBufferBeginInfo cmdBeginInfo = pass->renderInfo.cmdBeginInfo;
		VkViewport viewport = pass->renderInfo.viewport;
		VkRect2D scissor = pass->renderInfo.scissor;
		VkRenderPassBeginInfo& renderBeginInfo = pass->renderInfo.renderPassBeginInfo;

		if (pass->queueFlags == FrameGraphQueueBits::PRESENT)
			renderBeginInfo.framebuffer = pass->framebuffers[swapchainImageIndex];
		else
			renderBeginInfo.framebuffer = pass->framebuffers[0];

		// Start recording
		VK_VALIDATE_RESULT(vkBeginCommandBuffer(buffer, &cmdBeginInfo));

		// Reset the queries
		vkCmdResetQueryPool(buffer, timestampQueryPool, ((frameIndex * (renderPassOrder.size() * 2) + passNum * 2) + 0), 2);

		// Write the starting timestamp
		vkCmdWriteTimestamp(buffer, VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, timestampQueryPool, ((frameIndex * (renderPassOrder.size() * 2) + passNum * 2) + 0));

		if (pass->queueFlags == FrameGraphQueueBits::GRAPHICS ||
			pass->queueFlags == FrameGraphQueueBits::ASYNC_GRAPHICS ||
			pass->queueFlags == FrameGraphQueueBits::PRESENT) {
			
			vkCmdSetViewport(buffer, 0, 1, &viewport);
			vkCmdSetScissor(buffer, 0, 1, &scissor);
		}

		//vkCmdBeginRenderPass(buffer, &renderBeginInfo, VK_SUBPASS_CONTENTS_INLINE); // NOTE: see usage of subpass contents later
		pass->onRender(buffer);
		//vkCmdEndRenderPass(buffer);

		// Write the ending timestamp
		vkCmdWriteTimestamp(buffer, VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, timestampQueryPool, ((frameIndex * (renderPassOrder.size() * 2) + passNum * 2) + 1));

		VK_VALIDATE_RESULT(vkEndCommandBuffer(buffer));

		std::vector<uint64_t> timestamps;
		timestamps.resize(2);
		timestamps[0] = timestamps[1] = 0;

		// NOT_READY is also a success code
		VkResult result = (vkGetQueryPoolResults(
			gpu.device,
			timestampQueryPool,
			((((frameIndex)) * (renderPassOrder.size() * 2) + passNum * 2) + 0),
			2,
			sizeof(uint64_t) * timestamps.size(),
			&timestamps[0],
			sizeof(uint64_t),
			VK_QUERY_RESULT_64_BIT));

		if (result == VK_SUCCESS) {
			double gpuTime = (static_cast<double>(timestamps[1] - timestamps[0]) * gpu.limits.timestampPeriod) * 1e-6;

			if (gpuTime < pass->gpuTimeMin && gpuTime > 0.0) {
				pass->gpuTimeMin = gpuTime;
			}

			if (gpuTime > pass->gpuTimeMax) {
				pass->gpuTimeMax = gpuTime;
			}

			pass->gpuTimeSum += gpuTime - pass->gpuExecutionTimes[pass->currentMetricSample];
			pass->gpuExecutionTimes[pass->currentMetricSample] = gpuTime;
		}

		if (result != VK_NOT_READY && result != VK_SUCCESS) {
			GRAIL_LOG(ERROR, "VK_VALIDATE_RESULT") << "On line: " << __LINE__ << " In: " << __FILE__ << " (" << grail::vkTools::errorToString(result).c_str() << ")";
			throw std::runtime_error("VK_VALIDATION_FAILED");
		}

		double cpuTime = std::chrono::duration<double, std::milli>(std::chrono::high_resolution_clock::now() - startTime).count();

		if (cpuTime < pass->cpuTimeMin) {
			pass->cpuTimeMin = cpuTime;
		}

		if (cpuTime > pass->cpuTimeMax) {
			pass->cpuTimeMax = cpuTime;
		}

		pass->cpuTimeSum += cpuTime - pass->cpuExecutionTimes[pass->currentMetricSample];
		pass->cpuExecutionTimes[pass->currentMetricSample] = cpuTime;
	

		pass->currentMetricSample = (pass->currentMetricSample + 1) % pass->timeMetricSampleCount;
		passNum++;
	}

	// Build submit infomation for the passes
	/// NOTE: need to implement proper synchronization
	passNum = 0;
	for (uint32_t index : renderPassOrder) {
		RenderPass* pass = renderPasses[index];

		//GRAIL_LOG(INFO, "Current Pass Begin") << pass->name;

		VkSubmitInfo submitInfo = {};
		submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		submitInfo.pNext = nullptr;

		submitInfo.commandBufferCount = 1;
		submitInfo.pCommandBuffers = &pass->primaryCmdBuffers[frameIndex];

		submitInfo.waitSemaphoreCount = 0;
		submitInfo.signalSemaphoreCount = 0;

		submitInfo.pWaitDstStageMask = &pass->renderInfo.submitPipelineStages;

		if (swapchain) {
			if (passNum == 0) {
				submitInfo.waitSemaphoreCount = 1;
				submitInfo.pWaitSemaphores = &renderAvailableSemaphores[frameIndex];
			}

			if (static_cast<uint32_t>(pass->queueFlags) & static_cast<uint32_t>(FrameGraphQueueBits::PRESENT)) {
				submitInfo.signalSemaphoreCount = 1;
				submitInfo.pSignalSemaphores = &renderFinishedSemaphores[frameIndex];
			}
		}

		//GRAIL_LOG(INFO, "SUBMIT") << pass->name;
		//VK_VALIDATE_RESULT(vkQueueSubmit(gpu.graphicsQueue.handle, 1, &submitInfo, VK_NULL_HANDLE)); // NOTE: don't hardcode the queue
		//VK_VALIDATE_RESULT(vkQueueWaitIdle(gpu.graphicsQueue.handle));

		//GRAIL_LOG(INFO, "Current Pass End") << pass->name << "\n";
		if (pass->queueFlags == FrameGraphQueueBits::RAYTRACE ||
			pass->queueFlags == FrameGraphQueueBits::GRAPHICS ||
			pass->queueFlags == FrameGraphQueueBits::ASYNC_GRAPHICS ||
			pass->queueFlags == FrameGraphQueueBits::PRESENT) {
			perFrameSubmitGraphics.push_back(submitInfo);
		}

		if (pass->queueFlags == FrameGraphQueueBits::COMPUTE ||
			pass->queueFlags == FrameGraphQueueBits::ASYNC_COMPUTE) {
			perFrameSubmitCompute.push_back(submitInfo);
		}

		passNum++;
	}

	// Transition all of the output attachments of all of the active render passes to the attachment optimal layout for the next frame
	/// NOTE: synchronization needs attention here
	VkCommandBufferBeginInfo beginInfo = {};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
	beginInfo.pNext = nullptr;

	VK_VALIDATE_RESULT(vkBeginCommandBuffer(transitionBuffers[frameIndex], &beginInfo));
	for (uint32_t p_index : renderPassOrder) {
		RenderPass* pass = renderPasses[p_index];

		// Copy over all of the history attachments
		if (pass->historyAttachments.size() > 0) {
			for (uint32_t h = 0; h < pass->historyAttachments.size(); h++) {
				// From
				RenderAttachment* src = attachments[std::get<0>(pass->historyAttachments[h])];
				
				// To
				RenderAttachment* dst = attachments[std::get<1>(pass->historyAttachments[h])];

				VkImageLayout oldLayout = src->getTexture()->imageLayout;
				src->getTexture()->transitionImage(transitionBuffers[frameIndex], VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);
				dst->getTexture()->transitionImage(transitionBuffers[frameIndex], VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);

				VkImageCopy copyRegion = {};

				copyRegion.srcSubresource.aspectMask = VulkanContext::hasDepth(src->properties.format) ?
					VK_IMAGE_ASPECT_DEPTH_BIT : VK_IMAGE_ASPECT_COLOR_BIT;
				copyRegion.srcSubresource.baseArrayLayer = 0;
				copyRegion.srcSubresource.mipLevel = 0;
				copyRegion.srcSubresource.layerCount = 1;
				copyRegion.srcOffset = { 0, 0, 0 };

				copyRegion.dstSubresource.aspectMask = VulkanContext::hasDepth(dst->properties.format) ?
					VK_IMAGE_ASPECT_DEPTH_BIT : VK_IMAGE_ASPECT_COLOR_BIT;
				copyRegion.dstSubresource.baseArrayLayer = 0;
				copyRegion.dstSubresource.mipLevel = 0;
				copyRegion.dstSubresource.layerCount = 1;
				copyRegion.dstOffset = { 0, 0, 0 };

				copyRegion.extent.width = src->getTexture()->width;
				copyRegion.extent.height = src->getTexture()->height;
				copyRegion.extent.depth = src->getTexture()->depth;

				vkCmdCopyImage(transitionBuffers[frameIndex],
					src->getTexture()->getImage(),
					VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
					dst->getTexture()->getImage(),
					VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
					1, &copyRegion);

				src->getTexture()->transitionImage(transitionBuffers[frameIndex], oldLayout);
				dst->getTexture()->transitionImage(transitionBuffers[frameIndex], VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
			}
		}

		std::vector<uint32_t> mergedOutputs = pass->outputs;
		if (pass->depthStencilOutput != UINT32_MAX)
			mergedOutputs.push_back(pass->depthStencilOutput);

		for (uint32_t a_index : mergedOutputs) {
			RenderAttachment* attachment = attachments[a_index];

			VkImageAspectFlags aspect = VK_IMAGE_ASPECT_COLOR_BIT;

			if (VulkanContext::hasDepth(attachment->properties.format)) {
				aspect = VK_IMAGE_ASPECT_DEPTH_BIT;
			}

			if (VulkanContext::hasStencil(attachment->properties.format)) {
				aspect |= VK_IMAGE_ASPECT_STENCIL_BIT;
			}

			VkImageSubresourceRange range = {};
			range.aspectMask = aspect;
			range.baseArrayLayer = 0;
			range.baseMipLevel = 0;
			range.layerCount = attachment->properties.layers;
			range.levelCount = attachment->properties.levels;

			/// NOTE (2020-06-10 20:20): perhaps at some point, there can be different layouts to transition from, perhaps from a shared attachment that is not used as a read target
			/// NOTE (2020-06-10 21:12): This bit me fast
			/// I am not meant for debug mode it seems

			VkImageLayout transitionTo;

			if (pass->queueFlags == FrameGraphQueueBits::RAYTRACE || 
				pass->queueFlags == FrameGraphQueueBits::COMPUTE ||
				pass->queueFlags == FrameGraphQueueBits::ASYNC_COMPUTE) {
				//GRAIL_LOG(INFO, "PASS => TRANSITION") << pass->name << " " << attachment->name;

				transitionTo = VK_IMAGE_LAYOUT_GENERAL;
			}
			else {
				if (swapchain) {
					transitionTo = VulkanContext::isDepthStencil(attachment->properties.format) ? VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL : VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
				}
				else {
					// If don't have a swapchain to display to, most likely the next use of the image if any will be shader read
					transitionTo = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
				}
			}	
			
			//GRAIL_LOG(INFO, "ATTACHMENT") << attachment->name;
			attachment->texture->transitionImage(transitionBuffers[frameIndex], transitionTo);
			//GRAIL_LOG(INFO, "ATTACHMENT") << "\n";
			//attachment->texture->imageLayout = transitionTo;
		}
	}
	VK_VALIDATE_RESULT(vkEndCommandBuffer(transitionBuffers[frameIndex]));

	VkPipelineStageFlags submitPipelineStages = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

	VkSubmitInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	info.pNext = nullptr;
	info.pWaitDstStageMask = &submitPipelineStages;
	info.commandBufferCount = 1;
	info.pCommandBuffers = &transitionBuffers[frameIndex];

	perFrameSubmitGraphics.push_back(info);

	//VK_VALIDATE_RESULT(vkQueueSubmit(gpu.graphicsQueue.handle, 1, &info, frameFences[0]));
	//VK_VALIDATE_RESULT(vkQueueWaitIdle(gpu.graphicsQueue.handle));

		// Wait for previous frame to finish
}

void grail::FrameGraph::submitFrame(Swapchain* swapchain) {
	// Submit everything
	//GRAIL_LOG(DEBUG, "SUBMIT") << frameIndex << "\n";
	if (perFrameSubmitGraphics.size() > 0) {
		VK_VALIDATE_RESULT(vkResetFences(gpu.device, 1, &frameFences[frameIndex * 3 + 0]));
		VK_VALIDATE_RESULT(vkQueueSubmit(gpu.graphicsQueue.handle, perFrameSubmitGraphics.size(), &perFrameSubmitGraphics[0], frameFences[frameIndex * 3 + 0]));
	}

	if (perFrameSubmitCompute.size() > 0) {
		VK_VALIDATE_RESULT(vkResetFences(gpu.device, 1, &frameFences[frameIndex * 3 + 1]));
		VK_VALIDATE_RESULT(vkQueueSubmit(gpu.computeQueue.handle, perFrameSubmitCompute.size(), &perFrameSubmitCompute[0], frameFences[frameIndex * 3 + 1]));
	}

	if (perFrameSubmitTransfer.size() > 0) {
		VK_VALIDATE_RESULT(vkResetFences(gpu.device, 1, &frameFences[frameIndex * 3 + 2]));
		VK_VALIDATE_RESULT(vkQueueSubmit(gpu.transferQueue.handle, perFrameSubmitTransfer.size(), &perFrameSubmitTransfer[0], frameFences[frameIndex * 3 + 2]));
	}

	if (swapchain) {
		// Swapchain can return non-failing codes that are not VK_SUCCESS, so we don't do our normal validation here
		/// NOTE: add support for swapchain recreation
		VkResult result = (swapchain->queuePresent(gpu.graphicsQueue.handle, swapchainImageIndex, renderFinishedSemaphores[frameIndex]));
		if (result == VK_ERROR_OUT_OF_HOST_MEMORY || result == VK_ERROR_OUT_OF_DEVICE_MEMORY ||
			result == VK_ERROR_DEVICE_LOST || result == VK_ERROR_OUT_OF_DATE_KHR ||
			result == VK_ERROR_SURFACE_LOST_KHR) {
			//VK_VALIDATE_RESULT(result);
		}

		if(!swapchain)
			VK_VALIDATE_RESULT(vkQueueWaitIdle(gpu.graphicsQueue.handle));
	}

	if(swapchain)
		frameIndex = (frameIndex + 1) % framesInFlight;
}


VkCommandBuffer grail::FrameGraph::allocateCommandBuffer(CommandPoolType type) {
	VkCommandBuffer buffer = VK_NULL_HANDLE;

	VkCommandBufferAllocateInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	info.pNext = nullptr;
	info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	info.commandBufferCount = 1;
	info.commandPool = commandPools[static_cast<uint32_t>(type)];
	VK_VALIDATE_RESULT(vkAllocateCommandBuffers(gpu.device, &info, &buffer));

	return buffer;
}

void grail::FrameGraph::setFinalPass(RenderPass & pass) {
	this->finalPass = pass.index;
}

void grail::FrameGraph::getActivePasses(std::vector<const RenderPass*>& passes) {
	for (uint32_t index : renderPassOrder) {
		passes.push_back(renderPasses[index]);
	}
}

grail::RenderPass * grail::FrameGraph::getPass(const std::string & name) {
	for (RenderPass* pass : renderPasses) {
		if (pass->name.compare(name) == 0) {
			return pass;
		}
	}

	return nullptr;
}

grail::VkTexture* grail::RenderAttachment::getTexture() {
	return texture;
}

void grail::RenderPass::addInputAttachment(RenderAttachment& attachment) {
	inputs.push_back(attachment.index);
	attachment.readInPasses.push_back(index);
}

void grail::RenderPass::addOutputAttachment(RenderAttachment& attachment) {
	outputs.push_back(attachment.index);
	attachment.writtenInPasses.push_back(index);
}

void grail::RenderPass::addHistoryAttachment(RenderAttachment& src, RenderAttachment& dst) {
	historyAttachments.push_back({ src.index, dst.index });
}

void grail::RenderPass::addInputBuffer(Buffer* buffer) {
	bufferInputs.push_back(buffer);
}

void grail::RenderPass::addInputTexture(VkTexture* texture) {
	textureInputs.push_back(texture);
}

void grail::RenderPass::setDepthStencilOutputAttachment(RenderAttachment& attachment) {
	depthStencilOutput = attachment.index;

	attachment.writtenInPasses.push_back(index);
}

void grail::RenderPass::setOnRender(std::function<void(VkCommandBuffer&)> func) {
	onRender = std::move(func);
}

void grail::RenderPass::setGetClearColor(std::function<VkClearColorValue()> func) {
	onClearColor = std::move(func);
}

void grail::RenderPass::setGetDepthStencilClearColor(std::function<VkClearDepthStencilValue()> func) {
	onClearDepthStencil = std::move(func);
}

void grail::RenderPass::createMaterialFromShaders(const std::string vertexShader, const std::string fragmentShader) {
	shaderInformation.vertexShaderPath = vertexShader;
	shaderInformation.fragmentShaderPath = fragmentShader;
}

void grail::RenderPass::createMaterialFromShaders(const std::string computeShader) {
	shaderInformation.computeShaderPath = computeShader;
}

std::string grail::RenderPass::getName() const {
	return name;
}

uint32_t grail::RenderPass::getOutputCount() const {
	return outputs.size() + (depthStencilOutput == UINT32_MAX ? 0 : 1);
}

uint32_t grail::RenderPass::getColorOutputCount() const {
	return outputs.size();
}

uint32_t grail::RenderPass::getInputCount() const {
	return inputs.size();
}

double grail::RenderPass::getMinGPUTime() const {
	return (gpuTimeMin == DBL_MAX ? 0.0 : gpuTimeMin);
}

double grail::RenderPass::getMaxGPUTime() const {
	return gpuTimeMax;
}

double grail::RenderPass::getAvgGPUTime() const {
	return (gpuTimeSum == 0.0 ? 0.0 : gpuTimeSum / static_cast<double>(timeMetricSampleCount));
}

double grail::RenderPass::getAvgGPUTimeTrue() const {
	double sum = 0.0;

	for (double t : gpuExecutionTimes) {
		sum += t;
	}

	return (sum == 0.0 ? 0.0 : sum / static_cast<double>(timeMetricSampleCount));
}

double grail::RenderPass::getMinCPUTime() const {
	return (cpuTimeMin == DBL_MAX ? 0.0 : cpuTimeMin);
}

double grail::RenderPass::getMaxCPUTime() const {
	return cpuTimeMax;
}

double grail::RenderPass::getAvgCPUTime() const {
	return (cpuTimeSum == 0.0 ? 0.0 : cpuTimeSum / static_cast<double>(timeMetricSampleCount));
}

double grail::RenderPass::getAvgCPUTimeTrue() const {
	double sum = 0.0;

	for (double t : cpuExecutionTimes) {
		sum += t;
	}

	return (sum == 0.0 ? 0.0 : sum / static_cast<double>(timeMetricSampleCount));
}

uint32_t grail::RenderPass::getWidth() const {
	return width;
}

uint32_t grail::RenderPass::getHeight() const {
	return height;
}

VkRenderPass grail::RenderPass::getRenderPass() const {
	return renderPass;
}

grail::MaterialInstance * grail::RenderPass::getDefaultMaterial() {
	return materialInstance;
}

void grail::RenderPass::beginRenderPass(VkCommandBuffer cmd, uint32_t framebufferIndex, uint32_t width, uint32_t height) {
	if (framebufferIndex != 0 || width != 0 && height != 0) {
		renderInfo.renderPassBeginInfo.framebuffer = framebuffers[framebufferIndex];
		renderInfo.renderPassBeginInfo.renderArea.extent.width = (width == 0 ? this->width : width);
		renderInfo.renderPassBeginInfo.renderArea.extent.height = (height == 0 ? this->height : height);
	}
	
	for (uint32_t attachmentIndex : outputs) {
		RenderAttachment* attachment = graph->attachments[attachmentIndex];

		/*if (attachment->properties.mipmapped) {
			VkImageSubresourceRange range;
			range.baseArrayLayer = 0;
			range.baseMipLevel = 1;
			range.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			range.layerCount = attachment->getTexture()->getLayerCount();
			range.levelCount = attachment->getTexture()->getMipCount() - 1;
			attachment->getTexture()->transitionImage(cmd, attachment->initialLayout, range);
		}*/

		attachment->getTexture()->imageLayout = attachment->initialLayout;
	}

	if (depthStencilOutput != UINT32_MAX) {
		RenderAttachment* attachment = graph->attachments[depthStencilOutput];

		/*if (attachment->properties.mipmapped) {
			VkImageSubresourceRange range;
			range.baseArrayLayer = 0;
			range.baseMipLevel = 1;
			range.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
			range.layerCount = attachment->getTexture()->getLayerCount();
			range.levelCount = attachment->getTexture()->getMipCount() - 1;
			attachment->getTexture()->transitionImage(cmd, attachment->initialLayout, range);
		}*/

		attachment->getTexture()->imageLayout = attachment->initialLayout;
	}

	vkCmdBeginRenderPass(cmd, &renderInfo.renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);
}

void grail::RenderPass::endRenderPass(VkCommandBuffer cmd) {
	vkCmdEndRenderPass(cmd);

	for (uint32_t attachmentIndex : outputs) {
		RenderAttachment* attachment = graph->attachments[attachmentIndex];

		/*if (attachment->properties.mipmapped) {
			VkImageSubresourceRange range;
			range.baseArrayLayer = 0;
			range.baseMipLevel = 1;
			range.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			range.layerCount = attachment->getTexture()->getLayerCount();
			range.levelCount = attachment->getTexture()->getMipCount() - 1;
			attachment->getTexture()->transitionImage(cmd, attachment->finalLayout, range);
		}*/

		attachment->getTexture()->imageLayout = attachment->finalLayout;
	}

	if (depthStencilOutput != UINT32_MAX) {
		RenderAttachment* attachment = graph->attachments[depthStencilOutput];

		/*if (attachment->properties.mipmapped) {
			VkImageSubresourceRange range;
			range.baseArrayLayer = 0;
			range.baseMipLevel = 1;
			range.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
			range.layerCount = attachment->getTexture()->getLayerCount();
			range.levelCount = attachment->getTexture()->getMipCount() - 1;
			attachment->getTexture()->transitionImage(cmd, attachment->finalLayout, range);
		}*/

		attachment->getTexture()->imageLayout = attachment->finalLayout;
	}
}

void grail::RenderPass::bindMaterial(VkCommandBuffer cmd, MaterialInstance * material) {
	VkPipelineBindPoint bindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;

	if (queueFlags == FrameGraphQueueBits::COMPUTE ||
		queueFlags == FrameGraphQueueBits::ASYNC_COMPUTE) {
		bindPoint = VK_PIPELINE_BIND_POINT_COMPUTE;
	}

	vkCmdBindPipeline(cmd, bindPoint, material->getPipeline());
	
	if (!material->getCurrentFrameDescriptor().isEmpty()) {
		vkCmdBindDescriptorSets(
			cmd,
			bindPoint,
			material->getCurrentFrameDescriptor().getPipelineLayout(),
			0,
			material->getCurrentFrameDescriptor().getDescriptorCount(),
			material->getCurrentFrameDescriptor().getHandlePtr(),
			0,
			nullptr
		);
	}
}

void grail::RenderPass::setPushConstants(VkCommandBuffer cmd, MaterialInstance * material, void * data) {
	vkCmdPushConstants(cmd, material->getCurrentFrameDescriptor().getPipelineLayout(),
		static_cast<VkShaderStageFlags>(material->getCurrentFrameDescriptor().getPushConstant(0).stageFlags),
		material->getCurrentFrameDescriptor().getPushConstant(0).offset,
		material->getCurrentFrameDescriptor().getPushConstant(0).size,
		data);
}

void grail::RenderPass::drawMesh(VkCommandBuffer cmd, Mesh * mesh) {
	vkCmdBindVertexBuffers(cmd, 0, 1, &mesh->buffer, &mesh->vertexOffset);
	vkCmdBindIndexBuffer(cmd, mesh->buffer, mesh->indexOffset, VK_INDEX_TYPE_UINT32);
	vkCmdDrawIndexed(cmd, mesh->indicesCount, 1, 0, 0, 1);
}

void grail::RenderPass::createFramebuffer(VkFramebufferCreateInfo createInfo) {
	uint32_t size = framebuffers.size();
	framebuffers.push_back(VK_NULL_HANDLE);

	VK_VALIDATE_RESULT(vkCreateFramebuffer(VulkanContext::mainGPU.device, &createInfo, nullptr, &framebuffers[size]));
}

void grail::RenderPass::createCmdBufferBeginInfo() {
	VkCommandBufferBeginInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	info.pNext = nullptr;
	info.flags = 0;
	info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

	renderInfo.cmdBeginInfo = info;
}

void grail::RenderPass::createRenderPassBeginInfo() {
	VkRenderPassBeginInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	info.pNext = nullptr;
	info.renderPass = renderPass;
	info.renderArea.offset.x = 0;
	info.renderArea.offset.y = 0;
	info.renderArea.extent.width = width;
	info.renderArea.extent.height = height;

	if (queueFlags == FrameGraphQueueBits::PRESENT) {
		renderInfo.clearValues.resize(1);
		renderInfo.clearValues[0].color = onClearColor();
	}
	else {
		if (depthStencilOutput != UINT32_MAX) {
			VkClearValue clearVal = {};
			clearVal.depthStencil = onClearDepthStencil();

			renderInfo.clearValues.push_back(clearVal);
		}

		for (uint32_t i = 0; i < outputs.size(); i++) {
			VkClearValue clearVal = {};
			clearVal.color = onClearColor();

			renderInfo.clearValues.push_back(clearVal);
		}
	}


	info.clearValueCount = renderInfo.clearValues.size();
	info.pClearValues = &renderInfo.clearValues[0];

	renderInfo.renderPassBeginInfo = info;
}

void grail::RenderPass::createViewport() {
	VkViewport viewport = {};
	viewport.width = static_cast<float>(width);
	viewport.height = static_cast<float>(height);
	viewport.minDepth = 0.0f;
	viewport.maxDepth = 1.0f;

	renderInfo.viewport = viewport;
}

void grail::RenderPass::createScissor() {
	VkRect2D scissor = {};
	scissor.extent.width = static_cast<float>(width);
	scissor.extent.height = static_cast<float>(height);
	scissor.offset.x = 0;
	scissor.offset.y = 0;

	renderInfo.scissor = scissor;
}

void grail::RenderPass::createSubmitInfo() {
	VkSubmitInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	info.pNext = nullptr;
	info.pWaitDstStageMask = &renderInfo.submitPipelineStages;
	info.commandBufferCount = 1;
	info.signalSemaphoreCount = 1;
	info.waitSemaphoreCount = 1;

	renderInfo.submitInfo = info;
}

void grail::RenderPass::prepareInfos() {
	createCmdBufferBeginInfo();
	createRenderPassBeginInfo();
	createViewport();
	createScissor();
	createSubmitInfo();
}

void grail::RenderPass::createGraphicsPipeline() {
	if (!active) return;

	Shader* vertex = nullptr;
	Shader* fragment = nullptr;

	try {
		vertex = ShaderLoader::loadSPIRVShader(shaderInformation.vertexShaderPath);
		fragment = ShaderLoader::loadSPIRVShader(shaderInformation.fragmentShaderPath);
	} catch (exceptions::RuntimeException* e) {
		GRAIL_LOG(ERROR, "SHADER COMP") << e->what();
	}

	if (!vertex || !fragment) {
		return;
	}

	std::vector<Shader*> shaders;
	shaders.push_back(vertex);
	shaders.push_back(fragment);

	ShaderPermutation* v = vertex->getBasePermutation();
	ShaderPermutation* f = fragment->getBasePermutation();

	std::vector<ShaderPermutation*> shaderPerms;
	shaderPerms.push_back(v);
	shaderPerms.push_back(f);

	std::vector<VertexAttribute> attributes;

	// NOTE: only works for float based attributes
	for (IOShaderVar& input : v->inputs) {
		VkFormat format = VK_FORMAT_UNDEFINED;

		if (input.rows == 1) {
			format = VK_FORMAT_R32_SFLOAT;
		}
		else if (input.rows == 2) {
			format = VK_FORMAT_R32G32_SFLOAT;
		}
		else if (input.rows == 3) {
			format = VK_FORMAT_R32G32B32_SFLOAT;
		}
		else if (input.rows == 4) {
			format = VK_FORMAT_R32G32B32A32_SFLOAT;
		}

		attributes.push_back(VertexAttribute(VertexAttributeType::GENERIC, format, input.rows, sizeof(float)));
	}

	VertexLayout layout = VertexLayout(attributes);

	GraphicsPipelineCreateInfo info = {};
	info.renderPass = getRenderPass();
	info.shaderStageState.set(ShaderStageBits::VERTEX, v->getModule());
	info.shaderStageState.set(ShaderStageBits::FRAGMENT, f->getModule());
	info.rasterizationState.cullMode = CullMode::NONE;

	PushConstantRange pushRange = {};
	pushRange.offset = 0;
	if (f->pushConstants.size > 0) {
		pushRange.stageFlags |= ShaderStageBits::FRAGMENT;
		pushRange.size = f->pushConstants.size;
	}
	if (v->pushConstants.size > 0) {
		pushRange.size = v->pushConstants.size;
		pushRange.stageFlags |= ShaderStageBits::VERTEX;
	}

	// NOTE: Assume only one set is used
	DescriptorLayout descriptorLayout;

	if(pushRange.size > 0)
		descriptorLayout.addPushConstant(pushRange);

	// NOTE: parse stage flags
	// NOTE: definitely need to improve descriptor set creation 
	for (ImageShaderVar& image : f->sampledImages) {
		DescriptorBindingDescription description;
		description.identifier = image.name;
		description.shaderStageFlags = ShaderStageBits::FRAGMENT | ShaderStageBits::VERTEX;
		description.set = image.set;
		description.index = image.binding;
		description.type = DescriptorBindingType::COMBINED_IMAGE_SAMPLER;

		//GRAIL_LOG(INFO, "WHAT") << description.identifier << " " << description.index << " " << description.set;

		descriptorLayout.addDescriptor(description);
	}

	for (BufferBlock& buffer : f->uniformBuffers) {
		DescriptorBindingDescription description;
		description.identifier = buffer.name;
		description.shaderStageFlags = ShaderStageBits::FRAGMENT | ShaderStageBits::VERTEX;
		description.set = buffer.set;
		description.index = buffer.binding;
		description.type = DescriptorBindingType::UNIFORM_BUFFER;

		//GRAIL_LOG(INFO, "WHAT BUFFERS") << description.identifier << " " << description.index;

		descriptorLayout.addDescriptor(description);
	}

	descriptorLayout.finalize();

	material = new MaterialDescription(name + "_default");
	Material* base = material->createMaterial(layout, descriptorLayout, shaderPerms, info);
	materialInstance = base->createInstance();

	uint32_t itemIndex = 0;
	for (uint32_t index : inputs) {
		RenderAttachment* attachment = graph->attachments[index];
		// NOTE: hardcoded, bad
		//GRAIL_LOG(INFO, "??lol") << itemIndex << " " << attachment->texture->identifier;
		
		materialInstance->setDescriptorBinding(0, itemIndex++, attachment->texture, &attachment->texture->getDescriptor());
	}

	for (VkTexture* texture : textureInputs) {
		// NOTE: also not implemented very well
		//GRAIL_LOG(INFO, "??") << itemIndex << " " << texture->identifier;
		//GRAIL_LOG(INFO, "??") << itemIndex << " " << texture->getIdentifier() << " " << texture->getDescriptor().imageLayout;

		materialInstance->setDescriptorBinding(0, itemIndex++, texture, &texture->getDescriptor());
	}

	for (Buffer* buffer : bufferInputs) {
		// NOTE: this is getting out of control now
		//GRAIL_LOG(INFO, "??") << itemIndex << " " << buffer->identifier;
		materialInstance->setDescriptorBinding(0, itemIndex++, buffer, &buffer->getDescriptor());
	}

	materialInstance->updateAllDescriptors();
}

/// NOTE: :( 
void grail::RenderPass::createComputePipeline() {
	if (!active) return;

	Shader* compute = nullptr;

	try {
		compute = ShaderLoader::loadSPIRVShader(shaderInformation.computeShaderPath);
	}
	catch (exceptions::RuntimeException* e) {
		GRAIL_LOG(ERROR, "SHADER COMP") << e->what();
	}

	if (!compute) {
		return;
	}

	std::vector<Shader*> shaders;
	shaders.push_back(compute);

	ShaderPermutation* c = compute->getBasePermutation();

	std::vector<ShaderPermutation*> shaderPerms;
	shaderPerms.push_back(c);

	PushConstantRange pushRange = {};
	pushRange.offset = 0;
	if (c->pushConstants.size > 0) {
		pushRange.stageFlags |= ShaderStageBits::COMPUTE;
		pushRange.size = c->pushConstants.size;
	}

	// NOTE: Assume only one set is used
	DescriptorLayout descriptorLayout;

	if (pushRange.size > 0)
		descriptorLayout.addPushConstant(pushRange);

	// NOTE: parse stage flags
	// NOTE: definitely need to improve descriptor set creation 
	for (ImageShaderVar& image : c->sampledImages) {
		DescriptorBindingDescription description;
		description.identifier = image.name;
		description.shaderStageFlags = ShaderStageBits::COMPUTE;
		description.set = image.set;
		description.index = image.binding;
		description.type = DescriptorBindingType::COMBINED_IMAGE_SAMPLER;

		//GRAIL_LOG(INFO, "WHAT") << description.identifier << " " << description.index << " " << description.set;

		descriptorLayout.addDescriptor(description);
	}

	for (BufferBlock& buffer : c->uniformBuffers) {
		DescriptorBindingDescription description;
		description.identifier = buffer.name;
		description.shaderStageFlags = ShaderStageBits::COMPUTE;
		description.set = buffer.set;
		description.index = buffer.binding;
		description.type = DescriptorBindingType::UNIFORM_BUFFER;

		//GRAIL_LOG(INFO, "WHAT BUFFERS") << description.identifier << " " << description.index;

		descriptorLayout.addDescriptor(description);
	}

	for (ImageShaderVar& image : c->storageImages) {
		DescriptorBindingDescription description;
		description.identifier = image.name;
		description.shaderStageFlags = ShaderStageBits::COMPUTE;
		description.set = image.set;
		description.index = image.binding;
		description.type = DescriptorBindingType::STORAGE_IMAGE;

		//GRAIL_LOG(INFO, "WHAT") << description.identifier << " " << description.index << " " << description.set;

		descriptorLayout.addDescriptor(description);
	}

	descriptorLayout.finalize();

	GraphicsPipelineCreateInfo info = {};
	info.renderPass = getRenderPass();
	info.shaderStageState.set(ShaderStageBits::COMPUTE, c->getModule());

	material = new MaterialDescription(name + "_default");
	Material* base = material->createComputeMaterial(descriptorLayout, shaderPerms, info);
	materialInstance = base->createInstance();

	uint32_t itemIndex = 0;
	for (uint32_t index : inputs) {
		RenderAttachment* attachment = graph->attachments[index];
		// NOTE: hardcoded, bad
		//GRAIL_LOG(INFO, "??lol") << itemIndex << " " << attachment->texture->identifier;

		materialInstance->setDescriptorBinding(0, itemIndex++, attachment->texture, &attachment->texture->getDescriptor());
	}

	for (VkTexture* texture : textureInputs) {
		// NOTE: also not implemented very well
		//GRAIL_LOG(INFO, "??") << itemIndex << " " << texture->identifier;
		//GRAIL_LOG(INFO, "??") << itemIndex << " " << texture->getIdentifier() << " " << texture->getDescriptor().imageLayout;

		materialInstance->setDescriptorBinding(0, itemIndex++, texture, &texture->getDescriptor());
	}

	for (Buffer* buffer : bufferInputs) {
		// NOTE: this is getting out of control now
		//GRAIL_LOG(INFO, "??") << itemIndex << " " << buffer->identifier;
		materialInstance->setDescriptorBinding(0, itemIndex++, buffer, &buffer->getDescriptor());
	}

	for (uint32_t i : outputs) {
		// NOTE: also not implemented very well
		//GRAIL_LOG(INFO, "??") << itemIndex << " " << texture->identifier;
		//GRAIL_LOG(INFO, "??") << itemIndex << " " << texture->getIdentifier() << " " << texture->getDescriptor().imageLayout;
		RenderAttachment* attachment = graph->attachments[i];
		VkTexture* texture = attachment->getTexture();


		materialInstance->setDescriptorBinding(0, itemIndex++, texture, &texture->getDescriptor(VK_IMAGE_LAYOUT_GENERAL));
	}

	materialInstance->updateAllDescriptors();

}

void grail::RenderPass::createMaterial() {
	if(shaderInformation.computeShaderPath.length() != 0) {
		createComputePipeline();
	}
	else if(shaderInformation.fragmentShaderPath.length() != 0 && shaderInformation.vertexShaderPath.length() != 0) {
		createGraphicsPipeline();
	}
}
