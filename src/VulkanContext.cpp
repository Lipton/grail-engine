#include "VulkanContext.h"
#include "Swapchain.h"
#include "Constants.h"
#include "Grail.h"
#include <algorithm>

grail::Swapchain* grail::VulkanContext::swapchain = nullptr;
VkInstance grail::VulkanContext::instance = {};

grail::GPU grail::VulkanContext::mainGPU = {};
std::vector<grail::GPU> grail::VulkanContext::gpus = std::vector<grail::GPU>();

bool grail::VulkanContext::hasDepth(VkFormat format) {
	std::vector<VkFormat> formats = {
		VK_FORMAT_D16_UNORM,
		VK_FORMAT_X8_D24_UNORM_PACK32,
		VK_FORMAT_D32_SFLOAT,
		VK_FORMAT_D16_UNORM_S8_UINT,
		VK_FORMAT_D24_UNORM_S8_UINT,
		VK_FORMAT_D32_SFLOAT_S8_UINT,
	};
    
	return std::find(formats.begin(), formats.end(), (VkFormat)format) != std::end(formats);
}

bool grail::VulkanContext::hasStencil(VkFormat format) {
	std::vector<VkFormat> formats = {
		VK_FORMAT_S8_UINT,
		VK_FORMAT_D16_UNORM_S8_UINT,
		VK_FORMAT_D24_UNORM_S8_UINT,
		VK_FORMAT_D32_SFLOAT_S8_UINT,
	};

	return std::find(formats.begin(), formats.end(), format) != std::end(formats);
}

bool grail::VulkanContext::isDepthStencil(VkFormat format) {
	return (hasDepth(format) || hasStencil(format));
}

grail::VulkanContext::VulkanContext() {

}

grail::VulkanContext::~VulkanContext() {
}

uint32_t findQueue(grail::GPU& gpu, uint32_t queueCount, VkQueueFlags queueFlags) {
	uint32_t index = UINT32_MAX;
	uint32_t dedicatedIndex = UINT32_MAX;

	for (uint32_t i = 0; i < queueCount; i++) {
		if (gpu.queueFamilyProperties[i].queueFlags == queueFlags) {
			if (dedicatedIndex == UINT32_MAX) {
				dedicatedIndex = i;
				break;
			}
		}
	}

	if (dedicatedIndex != UINT32_MAX)
		return dedicatedIndex;

	for (uint32_t i = 0; i < queueCount; i++) {
		if (gpu.queueFamilyProperties[i].queueFlags & queueFlags) {
			if (index == UINT32_MAX) {
				index = i;
				break;
			}
		}
	}

	return index;
}

bool grail::VulkanContext::initializeContext(EngineInitializationInfo & initInfo, ApplicationWindow * window) {
	if (!vulkanLoader::loadLibrary()) {
		GRAIL_LOG(ERROR, "VULKAN CONTEXT") << "Could not load Vulkan library";
		return false;
	}

	if (initInfo.engineConfiguration->enableValidationLayers)
		GRAIL_LOG(INFO, "VULKAN CONTEXT") << "*** Creating Vulkan context using validation layers ***";

	uint32_t apiVersion = 0;
	//vkEnumerateInstanceVersion(&apiVersion);

	GRAIL_LOG(INFO, "VULKAN CONTEXT") << "Creating Vulkan instance...";
	VkApplicationInfo applicationInfo = {};
	applicationInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	applicationInfo.pNext = nullptr;
	applicationInfo.pApplicationName = "Untitled";
	applicationInfo.pEngineName = "Grail";
	applicationInfo.engineVersion = engineConstants::ENGINE_VERSION;
	applicationInfo.apiVersion = VK_MAKE_VERSION(
		VK_VERSION_MAJOR(1),
		VK_VERSION_MINOR(1),
		VK_VERSION_PATCH(0)
	);

	uint32_t instanceExtensionCount = 0;
	vkEnumerateInstanceExtensionProperties(nullptr, &instanceExtensionCount, nullptr);

	std::vector<VkExtensionProperties> instanceExtensionProperties;
	instanceExtensionProperties.resize(instanceExtensionCount);
	vkEnumerateInstanceExtensionProperties(nullptr, &instanceExtensionCount, &instanceExtensionProperties[0]);
	
	{
		std::stringstream ss;
		ss << "Available Instance Extensions: \n";
		for (int i = 0; i < instanceExtensionCount; i++) {
			ss << "\t" << instanceExtensionProperties[i].extensionName << "(" << instanceExtensionProperties[i].specVersion << ")\n";
		}

		GRAIL_LOG(INFO, "VULKAN CONTEXT") << ss.str();
	}

	VkInstanceCreateInfo instanceInfo = {};
	instanceInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	instanceInfo.pNext = nullptr;
	instanceInfo.flags = 0;
	instanceInfo.pApplicationInfo = &applicationInfo;

	std::vector<const char*> enabledExtensions;
	enabledExtensions.push_back(VK_KHR_SURFACE_EXTENSION_NAME);
	
#if defined(OS_WINDOWS)
	enabledExtensions.push_back(VK_KHR_WIN32_SURFACE_EXTENSION_NAME);
#elif defined(OS_LINUX)
	enabledExtensions.push_back(VK_KHR_XCB_SURFACE_EXTENSION_NAME);
#elif defined(OS_ANDROID)
	enabledExtensions.push_back(VK_KHR_ANDROID_SURFACE_EXTENSION_NAME);
#endif
	if (initInfo.engineConfiguration->enableValidationLayers) {
		enabledExtensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
		
		std::stringstream ss;
		uint32_t layerCount = 0;

		ss << "\nAvailable validation layers:\n";

		VK_VALIDATE_RESULT(vkEnumerateInstanceLayerProperties(&layerCount, nullptr));
		std::vector<VkLayerProperties> layerProps;
		layerProps.resize(layerCount);

		VK_VALIDATE_RESULT(vkEnumerateInstanceLayerProperties(&layerCount, &layerProps[0]));

		for (uint32_t i = 0; i < layerCount; i++) {
			ss << "\t-" << layerProps[i].layerName << "\n";
		}

		GRAIL_LOG(INFO, "VULKAN CONTEXT") << ss.str();
	}

	for (std::string& extension : initInfo.engineConfiguration->requestedInstanceExtensions) {
		enabledExtensions.push_back(extension.c_str());
	}

	{
		std::stringstream ss;
		
		ss << "Requested Instance Extensions: \n";

		for (const char* extension : enabledExtensions) {
			ss << "\t-" << std::string(extension) << "\n";
		}

		GRAIL_LOG(INFO, "VULKAN CONTEXT") << ss.str();
	}

	instanceInfo.enabledExtensionCount = (uint32_t)enabledExtensions.size();
	instanceInfo.ppEnabledExtensionNames = &enabledExtensions[0];

	if (initInfo.engineConfiguration->enableValidationLayers) {
		instanceInfo.enabledLayerCount = (uint32_t)validationLayers::validationLayerNames.size();
		instanceInfo.ppEnabledLayerNames = &validationLayers::validationLayerNames[0];
	}

	VK_VALIDATE_RESULT(vkCreateInstance(&instanceInfo, nullptr, &instance));

	vulkanLoader::loadInstanceFunctions(instance);

	if (initInfo.engineConfiguration->enableValidationLayers) {
		GRAIL_LOG(INFO, "VULKAN CONTEXT") << "Setting up validation layers...";
		validationLayers::setupDebugging(instance, initInfo.engineConfiguration->validationLayerFlags, (VkDebugReportCallbackEXT)nullptr);
	}

	uint32_t deviceCount = 0;
	GRAIL_LOG(INFO, "VULKAN CONTEXT") << "Enumerating Physical Devices...";
	VK_VALIDATE_RESULT(vkEnumeratePhysicalDevices(instance, &deviceCount, nullptr));

	if (deviceCount <= 0) {
		GRAIL_LOG(ERROR, "VULKAN CONTEXT") << "Could not find any physical devices!";
		return false;
	}

	std::vector<VkPhysicalDevice> pDevices(deviceCount);
	VK_VALIDATE_RESULT(vkEnumeratePhysicalDevices(instance, &deviceCount, &pDevices[0]));

	GRAIL_LOG(INFO, "VULKAN CONTEXT") << "Found " << deviceCount << " Vulkan Compatible Physical Device" << (deviceCount > 1 ? "s" : "");

	const char* deviceTypes[] = {
		"Other",
		"Integrated GPU",
		"Discrete GPU",
		"Virtual GPU",
		"CPU"
	};

	std::stringstream info;
	for (uint32_t i = 0; i < deviceCount; i++) {
		VkPhysicalDevice& device = pDevices[i];

		GPU gpu = {};
		gpu.physicalDevice = device;
		vkGetPhysicalDeviceProperties(device, &gpu.properties);
		vkGetPhysicalDeviceFeatures(device, &gpu.features);
		vkGetPhysicalDeviceMemoryProperties(device, &gpu.memoryProperties);
		gpu.limits = gpu.properties.limits;
		gpu.sparseProperties = gpu.properties.sparseProperties;

		uint32_t queueCount = 0;
		vkGetPhysicalDeviceQueueFamilyProperties(device, &queueCount, nullptr);

		gpu.queueFamilyProperties.resize(queueCount);
		vkGetPhysicalDeviceQueueFamilyProperties(device, &queueCount, &gpu.queueFamilyProperties[0]);

		gpus.push_back(gpu);

		info << "\n---------------------- " << (i + 1) << " ---------------------- \n"
			<< "----- Device Properties: " << "\n"
			<< "Device Name:\t" << gpus[i].properties.deviceName << "\n"
			<< "Device Type:\t" << gpus[i].properties.deviceType << " (" << deviceTypes[gpus[i].properties.deviceType] << ")" << "\n"
			<< "Driver Version:\t" << gpus[i].properties.driverVersion << "\n"
			<< "API Version:\t" << ((gpus[i].properties.apiVersion >> 22) & 0x3FF) << "." << ((gpus[i].properties.apiVersion >> 12) & 0x3FF) << "." << ((gpus[i].properties.apiVersion & 0xFFF)) << "\n \n"
			<< "----- Queue Family Information: " << "\n"; 

		for (uint32_t j = 0; j < queueCount; j++) {
			info << "\t--- " << (j) << " ---\n"
				<< "Queue count:\t" << gpus[i].queueFamilyProperties[j].queueCount << "\n"
				<< "Supported Queue operations:" << "\n";

			if (gpus[i].queueFamilyProperties[j].queueFlags & VK_QUEUE_GRAPHICS_BIT)
				info << "\t- Graphics" << "\n";
			if (gpus[i].queueFamilyProperties[j].queueFlags & VK_QUEUE_COMPUTE_BIT)
				info << "\t- Compute" << "\n";
			if (gpus[i].queueFamilyProperties[j].queueFlags & VK_QUEUE_TRANSFER_BIT)
				info << "\t- Transfer" << "\n";
			if (gpus[i].queueFamilyProperties[j].queueFlags & VK_QUEUE_SPARSE_BINDING_BIT)
				info << "\t- Sparse Binding" << "\n";
		}
		info << "\n";

		gpus[i].graphicsQueue.index = findQueue(gpu, queueCount, VK_QUEUE_GRAPHICS_BIT);
		gpus[i].computeQueue.index = findQueue(gpu, queueCount, VK_QUEUE_COMPUTE_BIT);
		gpus[i].transferQueue.index = findQueue(gpu, queueCount, VK_QUEUE_TRANSFER_BIT);
		gpus[i].sparseBindingQueue.index = findQueue(gpu, queueCount, VK_QUEUE_SPARSE_BINDING_BIT);

		info << "Graphics Queue Index: " << gpus[i].graphicsQueue.index << "\n";
		info << "Compute Queue Index: " << gpus[i].computeQueue.index << "\n";
		info << "Transfer Queue Index: " << gpus[i].transferQueue.index << "\n";
		info << "Sparse Queue Index: " << gpus[i].sparseBindingQueue.index << "\n";

		info << "-----------------------------------------------" << "\n";
	}
	GRAIL_LOG(INFO, "VULKAN CONTEXT") << info.str();

	GRAIL_LOG(INFO, "VULKAN CONTEXT") << "Using the first available GPU with graphics and compute capabilities";
	bool eligible = false;
	for (uint32_t i = 0; i < gpus.size(); i++) {
		if ((gpus[i].graphicsQueue.index != UINT32_MAX) && (gpus[i].computeQueue.index != UINT32_MAX)) {
			eligible = true;
			mainGPU = gpus[i];
			break;
		}
	}

	if (!eligible) {
		GRAIL_LOG(INFO, "VULKAN CONTEXT") << ("Could not find eligible GPU");
		return false;
	}

	std::vector<VkDeviceQueueCreateInfo> queueInformation;

	uint32_t queueCount = 1;

	bool transferAvailable = mainGPU.transferQueue.index == UINT32_MAX ? false : true;
	bool computeAvailable = mainGPU.computeQueue.index == UINT32_MAX ? false : true;

	bool dedicatedCompute = false;
	bool dedicatedTransfer = false;

	if ((mainGPU.graphicsQueue.index != mainGPU.computeQueue.index) && computeAvailable) {
		queueCount++;
		dedicatedCompute = true;
	}

	if ((mainGPU.graphicsQueue.index != mainGPU.transferQueue.index) && transferAvailable) {
		queueCount++;
		dedicatedTransfer = true;
	}

	GRAIL_LOG(INFO, "VULKAN CONTEXT") << "Transfer Queue Available: " << (transferAvailable ? "True" : "False");
    GRAIL_LOG(INFO, "VULKAN CONTEXT") << "Compute Queue Available: " << (computeAvailable ? "True" : "False");

	GRAIL_LOG(INFO, "VULKAN CONTEXT") << "Dedicated Compute Queue: " << (dedicatedCompute ? "True" : "False");
	GRAIL_LOG(INFO, "VULKAN CONTEXT") << "Dedicated Trasfer Queue: " << (dedicatedTransfer ? "True" : "False");

	GRAIL_LOG(INFO, "VULKAN CONTEXT") << "Creating Vulkan device...";

	queueInformation.resize(queueCount);

	for (uint32_t i = 0; i < queueInformation.size(); i++) {
		VkDeviceQueueCreateInfo& deviceQueueInfo = queueInformation[i];
		deviceQueueInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		deviceQueueInfo.pNext = nullptr;
		deviceQueueInfo.flags = 0;
		float priorities[1] = { 0.0f };
		deviceQueueInfo.queueCount = 1;
		deviceQueueInfo.pQueuePriorities = priorities;
	}
	queueInformation[0].queueFamilyIndex = mainGPU.graphicsQueue.index;
	
	uint32_t indexOffset = 1;

	if(dedicatedCompute) queueInformation[indexOffset++].queueFamilyIndex = mainGPU.computeQueue.index;
	if(dedicatedTransfer && transferAvailable) queueInformation[indexOffset++].queueFamilyIndex = mainGPU.transferQueue.index;

	VkDeviceCreateInfo deviceInfo = {};
	deviceInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	deviceInfo.pNext = nullptr;
	deviceInfo.flags = 0;

	std::vector<const char*> deviceExtensions = { VK_KHR_SWAPCHAIN_EXTENSION_NAME };

	for (std::string& extension : initInfo.engineConfiguration->requestedDeviceExtensions) {
		deviceExtensions.push_back(extension.c_str());
	}

	deviceInfo.enabledExtensionCount = (uint32_t)deviceExtensions.size();
	deviceInfo.ppEnabledExtensionNames = &deviceExtensions[0];
	deviceInfo.pEnabledFeatures = &initInfo.engineConfiguration->requestedFeatures;

	if (initInfo.engineConfiguration->enableValidationLayers) {
		deviceInfo.enabledLayerCount = (uint32_t)validationLayers::validationLayerNames.size();
		deviceInfo.ppEnabledLayerNames = &validationLayers::validationLayerNames[0];
	}

	deviceInfo.queueCreateInfoCount = queueInformation.size();
	deviceInfo.pQueueCreateInfos = &queueInformation[0];

	{
		std::stringstream ss;

		ss << "Requested Device Extensions: \n";

		for (const char* extension : deviceExtensions) {
			ss << "\t-" << std::string(extension) << "\n";
		}

		GRAIL_LOG(INFO, "VULKAN CONTEXT") << ss.str();
	}

	VK_VALIDATE_RESULT(vkCreateDevice(mainGPU.physicalDevice, &deviceInfo, nullptr, &mainGPU.device));

	uint32_t queueInfoCount = 0;
	std::vector<VkQueueFamilyProperties> queueProperties;
	vkGetPhysicalDeviceQueueFamilyProperties(mainGPU.physicalDevice, &queueInfoCount, nullptr);
	queueProperties.resize(queueInfoCount);
	vkGetPhysicalDeviceQueueFamilyProperties(mainGPU.physicalDevice, &queueInfoCount, &queueProperties[0]);

	vkGetDeviceQueue(mainGPU.device, mainGPU.graphicsQueue.index, 0, &mainGPU.graphicsQueue.handle);
	
	if (dedicatedCompute) {
		vkGetDeviceQueue(mainGPU.device, mainGPU.computeQueue.index, 0, &mainGPU.computeQueue.handle);
	}
	else {
	    mainGPU.computeQueue.handle = mainGPU.graphicsQueue.handle;
	    mainGPU.computeQueue.index = mainGPU.graphicsQueue.index;
	}

	if(dedicatedTransfer) vkGetDeviceQueue(mainGPU.device, mainGPU.transferQueue.index, 0, &mainGPU.transferQueue.handle);
	else {
	    mainGPU.transferQueue.handle = mainGPU.graphicsQueue.handle;
	    mainGPU.transferQueue.index = mainGPU.graphicsQueue.index;
	}

	mainGPU.graphicsQueue.properties = queueProperties[mainGPU.graphicsQueue.index];
	mainGPU.computeQueue.properties = queueProperties[mainGPU.computeQueue.index];
	mainGPU.transferQueue.properties = queueProperties[mainGPU.transferQueue.index];

	GRAIL_LOG(INFO, "VULKAN CONTEXT") << "Creating setup command pools...";
	mainGPU.poolMap.insert({ mainGPU.graphicsQueue.index , {} });
	mainGPU.poolMap.insert({ mainGPU.computeQueue.index, {} });
	mainGPU.poolMap.insert({ mainGPU.transferQueue.index, {} });

	VkCommandPoolCreateInfo cmdPoolInfo = {};
	cmdPoolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	cmdPoolInfo.pNext = nullptr;
	cmdPoolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

	for (auto& entry : mainGPU.poolMap) {
		cmdPoolInfo.queueFamilyIndex = entry.first;
		VK_VALIDATE_RESULT(vkCreateCommandPool(mainGPU.device, &cmdPoolInfo, nullptr, &entry.second));
	}


	GRAIL_LOG(INFO, "INDEX") << mainGPU.transferQueue.index;


	GRAIL_LOG(INFO, "VULKAN CONTEXT") << "Creating swapchain...";
	swapchain = new Swapchain(window, mainGPU);

	/*VkCommandBuffer setupCommandBuffer;
	VkCommandBufferAllocateInfo commandBufferAllocateInfo = {};
	commandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	commandBufferAllocateInfo.commandPool = setupCommandPool;
	commandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	commandBufferAllocateInfo.commandBufferCount = 1;
	VK_VALIDATE_RESULT(vkAllocateCommandBuffers(mainGPU.device, &commandBufferAllocateInfo, &setupCommandBuffer));

	VkCommandBufferBeginInfo cmdBufferInfo = {};
	cmdBufferInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	VK_VALIDATE_RESULT(vkBeginCommandBuffer(setupCommandBuffer, &cmdBufferInfo));

	swapchain->create(setupCommandBuffer, &Graphics::width, &Graphics::height, initInfo.engineConfiguration->vsync);

	VK_VALIDATE_RESULT(vkEndCommandBuffer(setupCommandBuffer));
	VkSubmitInfo setupSubmitInfo = {};
	setupSubmitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	setupSubmitInfo.commandBufferCount = 1;
	setupSubmitInfo.pCommandBuffers = &setupCommandBuffer;
	VK_VALIDATE_RESULT(vkQueueSubmit(mainGPU.graphicsQueue, 1, &setupSubmitInfo, VK_NULL_HANDLE));
	VK_VALIDATE_RESULT(vkQueueWaitIdle(mainGPU.graphicsQueue));*/

	TemporaryCommandBuffer setupBuffer = mainGPU.createTemporaryBuffer(mainGPU.graphicsQueue, VK_COMMAND_BUFFER_LEVEL_PRIMARY);
	setupBuffer.begin();
	swapchain->create(setupBuffer.getHandle(), &Graphics::width, &Graphics::height, initInfo.engineConfiguration->vsync);
	setupBuffer.end();
	setupBuffer.submit();

	return true;
}

uint32_t grail::GPU::getMemoryType(uint32_t typeBits, VkFlags properties) {
	for (uint32_t i = 0; i < 32; i++) {
		if ((typeBits & 1) == 1) {
			if ((memoryProperties.memoryTypes[i].propertyFlags & properties) == properties) {
				return i;
			}
		}
		typeBits >>= 1;
	}

	assert(false && "Could not get memory type");

	return 0;
}

VkBool32 grail::GPU::getMemoryType(uint32_t typeBits, VkFlags properties, uint32_t * typeIndex) {
	for (uint32_t i = 0; i < 32; i++) {
		if ((typeBits & 1) == 1) {
			if ((memoryProperties.memoryTypes[i].propertyFlags & properties) == properties) {
				*typeIndex = i;
				return true;
			}
		}
		typeBits >>= 1;
	}

	assert(false && "Could not get memory type");

	return false;
}

VkBool32 grail::GPU::getSupportedDepthFormat(VkFormat * depthFormat) {
	std::vector<VkFormat> depthFormats = {
		VK_FORMAT_D32_SFLOAT_S8_UINT,
		VK_FORMAT_D32_SFLOAT,
		VK_FORMAT_D24_UNORM_S8_UINT,
		VK_FORMAT_D16_UNORM_S8_UINT,
		VK_FORMAT_D16_UNORM
	};

	for (VkFormat& format : depthFormats) {
		VkFormatProperties formatProps;
		vkGetPhysicalDeviceFormatProperties(physicalDevice, format, &formatProps);
		if (formatProps.optimalTilingFeatures & VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT) {
			*depthFormat = format;
			return true;
		}
	}

	return false;
}

grail::TemporaryCommandBuffer grail::GPU::createTemporaryBuffer(VulkanQueue queue, VkCommandBufferLevel level) {
	TemporaryCommandBuffer buffer = {};
	buffer.queue = queue.handle;
	buffer.gpu = *this;
	buffer.pool = poolMap.at(queue.index);

	VkCommandBufferAllocateInfo allocateInfo = {};
	allocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocateInfo.pNext = nullptr;
	allocateInfo.commandPool = poolMap.at(queue.index);
	allocateInfo.level = level;
	allocateInfo.commandBufferCount = 1;
	VK_VALIDATE_RESULT(vkAllocateCommandBuffers(device, &allocateInfo, &buffer.handle));

	return buffer;
}

void grail::GPU::createSemaphore(VkSemaphore* pSemaphore, uint32_t count) {
	VkSemaphoreCreateInfo semaphoreInfo = {};
	semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
	semaphoreInfo.pNext = nullptr;
	semaphoreInfo.flags = 0;
	
	for (uint32_t i = 0; i < count; i++) {
		VK_VALIDATE_RESULT(vkCreateSemaphore(device, &semaphoreInfo, nullptr, pSemaphore++));
	}
}

void grail::GPU::createFence(VkFence * pFence, uint32_t count) {
	VkFenceCreateInfo fenceInfo = {};
	fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	fenceInfo.pNext = nullptr;
	fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

	for (uint32_t i = 0; i < count; i++) {
		VK_VALIDATE_RESULT(vkCreateFence(device, &fenceInfo, nullptr, pFence++));
	}
}

std::string grail::vkTools::errorToString(VkResult result) {
	switch (result) {
#define STR(r) case VK_ ##r: return #r
		STR(NOT_READY);
		STR(TIMEOUT);
		STR(EVENT_SET);
		STR(EVENT_RESET);
		STR(INCOMPLETE);
		STR(ERROR_OUT_OF_HOST_MEMORY);
		STR(ERROR_OUT_OF_DEVICE_MEMORY);
		STR(ERROR_INITIALIZATION_FAILED);
		STR(ERROR_DEVICE_LOST);
		STR(ERROR_MEMORY_MAP_FAILED);
		STR(ERROR_LAYER_NOT_PRESENT);
		STR(ERROR_EXTENSION_NOT_PRESENT);
		STR(ERROR_FEATURE_NOT_PRESENT);
		STR(ERROR_INCOMPATIBLE_DRIVER);
		STR(ERROR_TOO_MANY_OBJECTS);
		STR(ERROR_FORMAT_NOT_SUPPORTED);
		STR(ERROR_SURFACE_LOST_KHR);
		STR(ERROR_NATIVE_WINDOW_IN_USE_KHR);
		STR(SUBOPTIMAL_KHR);
		STR(ERROR_OUT_OF_DATE_KHR);
		STR(ERROR_INCOMPATIBLE_DISPLAY_KHR);
		STR(ERROR_VALIDATION_FAILED_EXT);
		STR(ERROR_INVALID_SHADER_NV);
#undef STR
	default:
		return "UNKNOWN_ERROR";
	}
}

void grail::TemporaryCommandBuffer::begin() {
	VkCommandBufferBeginInfo beginInfo = {};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.pNext = nullptr;
	beginInfo.flags = 0;
	VK_VALIDATE_RESULT(vkBeginCommandBuffer(handle, &beginInfo));
}

void grail::TemporaryCommandBuffer::end() {
	VK_VALIDATE_RESULT(vkEndCommandBuffer(handle));
}

void grail::TemporaryCommandBuffer::submit() {
	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.pNext = nullptr;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &handle;

	VK_VALIDATE_RESULT(vkQueueSubmit(queue, 1, &submitInfo, VK_NULL_HANDLE));
	VK_VALIDATE_RESULT(vkQueueWaitIdle(queue));
}

void grail::TemporaryCommandBuffer::dispose() {
	vkFreeCommandBuffers(gpu.device, pool, 1, &handle);
}

const VkCommandBuffer & grail::TemporaryCommandBuffer::getHandle() {
	return handle;
}
