#include "ApplicationWindow.h"

#if defined(OS_ANDROID)
#include <android_native_app_glue.h>
#include <android/input.h>
#endif

void grail::ApplicationWindow::captureCursor(bool capture) {
#if defined(OS_ANDROID)

#else
	if (capture) {
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		Input::mouseCaptured = true;
	}
	else {
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		Input::mouseCaptured = false;
	}
#endif
}

void grail::ApplicationWindow::setTitle(const char * title) {
#if defined (OS_ANDROID)

#else
	glfwSetWindowTitle(window, title);
#endif
}


grail::ApplicationWindow::ApplicationWindow() {

}

grail::ApplicationWindow::~ApplicationWindow() {
#if defined(OS_ANDROID)
#else
	glfwTerminate();
#endif
}

bool grail::ApplicationWindow::initialize(EngineInitializationInfo & initInfo, ScreenManager * screenManager) {
	this->screenManager = screenManager;

	// Set the default just pressed key values to avoid false positives at startup
	for (uint32_t i = 0; i < Buttons::LAST; i++) {
		Input::justPressedButtons[i] = true;
	}

	for (uint32_t i = 0; i < Keys::LAST; i++) {
		Input::justPressedKeys[i] = true;
	}

#if defined(OS_ANDROID)
	android_app = initInfo.android_app;

	android_app->userData = this;
	android_app->onInputEvent = handleInput;
	android_app->onAppCmd = cmdCallback;
#else 
	if (!glfwInit()) {
		GRAIL_LOG(ERROR, "WINDOW") << "Could not initialize GLFW!";
		return false;
	}
	
	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	glfwWindowHint(GLFW_REFRESH_RATE, GLFW_DONT_CARE);
	glfwWindowHint(GLFW_RESIZABLE, (initInfo.engineConfiguration->resizableWindow) ? GLFW_TRUE : GLFW_FALSE);

	window = glfwCreateWindow(
		initInfo.engineConfiguration->windowWidth,
		initInfo.engineConfiguration->windowHeight,
		initInfo.engineConfiguration->windowTitle,
		initInfo.engineConfiguration->fullscreen ? glfwGetPrimaryMonitor() : nullptr,
		nullptr
	);


	// Center the damn window for gods sake, took me only 4 years of development to actually implement
	const GLFWvidmode* vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
	if (vidmode) {
		int monitorX, monitorY;
		glfwGetMonitorPos(glfwGetPrimaryMonitor(), &monitorX, &monitorY);

		int windowWidth, windowHeight;
		glfwGetWindowSize(window, &windowWidth, &windowHeight);

		glfwSetWindowPos(window,
			monitorX + (vidmode->width - windowWidth) / 2,
			monitorY + (vidmode->height + 5 - windowHeight) / 2);
	}

	glfwSetWindowUserPointer(window, this);
	glfwSetKeyCallback(window, keyPressCallback);
	glfwSetMouseButtonCallback(window, mouseClickCallback);
	glfwSetCursorPosCallback(window, mouseMovementCallback);
	glfwSetScrollCallback(window, mouseScrollCallback);
	glfwSetDropCallback(window, dropCallback);
	glfwSetCharModsCallback(window, charCallback);

	if (!window) {
		glfwTerminate();
		return false;
	}
#endif

	Input::window = this;

	return true;
}

bool grail::ApplicationWindow::load() {
#if defined(OS_ANDROID)
	return initialized;
#else
	return true;
#endif
}

void grail::ApplicationWindow::processFrame() {
#if defined(OS_ANDROID)
	while ((ident = ALooper_pollAll(0, nullptr, &events, (void**)&source)) >= 0) {
		if (source != nullptr) {
			source->process(android_app, source);
		}
	}
#else 
	Input::mouseScrollX = 0;
	Input::mouseScrollY = 0;

	glfwPollEvents();
#endif
}

bool grail::ApplicationWindow::shouldClose() {
#if defined(OS_ANDROID)
	return android_app->destroyRequested;
#else
	return glfwWindowShouldClose(window);
#endif
}

VkSurfaceKHR grail::ApplicationWindow::createSurface() {
uint32_t count;
const char** extensions = glfwGetRequiredInstanceExtensions(&count);

for(int i = 0; i < count; i++) {
	GRAIL_LOG(INFO, "EXT") << extensions[i];
}


	VkSurfaceKHR surface;

#if defined (OS_ANDROID)
	VkAndroidSurfaceCreateInfoKHR surfaceCreateInfo = {};
	surfaceCreateInfo.sType = VK_STRUCTURE_TYPE_ANDROID_SURFACE_CREATE_INFO_KHR;
	surfaceCreateInfo.window = android_app->window;
	VK_VALIDATE_RESULT(vkCreateAndroidSurfaceKHR(VulkanContext::instance, &surfaceCreateInfo, nullptr, &surface));
#else
	VK_VALIDATE_RESULT(glfwCreateWindowSurface(VulkanContext::instance, window, nullptr, &surface));
#endif

	return surface;
}

#if defined(OS_ANDROID)
int32_t grail::ApplicationWindow::handleInput(struct android_app * app, AInputEvent * event) {
	ApplicationWindow* self = static_cast<ApplicationWindow*>(app->userData);

	if (AInputEvent_getType(event) == AINPUT_EVENT_TYPE_MOTION) {
		int32_t eventSource = AInputEvent_getSource(event);

		switch(eventSource) {
		case AINPUT_SOURCE_TOUCHSCREEN:
			int32_t action = AMotionEvent_getAction(event);

			if (action == AMOTION_EVENT_ACTION_UP) {
				mouseClickCallback(self, Buttons::LEFT, GRAIL_MOUSE_RELEASE, 0);
			}

			if (action == AMOTION_EVENT_ACTION_DOWN) {
				mouseClickCallback(self, Buttons::LEFT, GRAIL_MOUSE_PRESS, 0);
				//mouseMovementCallback(self, static_cast<float>(AMotionEvent_getX(event, 0)), static_cast<float>(AMotionEvent_getY(event, 0)));
				Input::mouseX = static_cast<float>(AMotionEvent_getX(event, 0));
				Input::mouseY = static_cast<float>(AMotionEvent_getY(event, 0));

				Input::mouseTouchX = static_cast<float>(AMotionEvent_getX(event, 0));
				Input::mouseTouchY = static_cast<float>(AMotionEvent_getY(event, 0));
			}

			if (action == AMOTION_EVENT_ACTION_MOVE)
				mouseMovementCallback(self, static_cast<float>(AMotionEvent_getX(event, 0)), static_cast<float>(AMotionEvent_getY(event, 0)));

			return 0;
			break;
		}
	}

	return 0;
}

void grail::ApplicationWindow::cmdCallback(struct android_app *android_app, int32_t cmd) {
	(void)android_app;
	ApplicationWindow* self = static_cast<ApplicationWindow*>(android_app->userData);

	switch (cmd)
	{
	case APP_CMD_INIT_WINDOW:
		self->initialized = true;
		break;
	default:
		break;
	}
}
#else 
void grail::ApplicationWindow::mouseMovementCallback(GLFWwindow * window, double posX, double posY) {
	ApplicationWindow* instance = static_cast<ApplicationWindow*>(glfwGetWindowUserPointer(window));

	mouseMovementCallback(instance, static_cast<float>(posX), static_cast<float>(posY));
}

void grail::ApplicationWindow::mouseClickCallback(GLFWwindow * window, int button, int action, int mods) {
	ApplicationWindow* instance = static_cast<ApplicationWindow*>(glfwGetWindowUserPointer(window));

	int ac = GRAIL_MOUSE_PRESS;

	if (action == GLFW_PRESS)
		ac = GRAIL_MOUSE_PRESS;
	if (action == GLFW_RELEASE)
		ac = GRAIL_MOUSE_RELEASE;

	mouseClickCallback(instance, button, ac, mods);
}

void grail::ApplicationWindow::keyPressCallback(GLFWwindow * window, int key, int scancode, int action, int mods) {
	ApplicationWindow* instance = static_cast<ApplicationWindow*>(glfwGetWindowUserPointer(window));

	int ac = GRAIL_KEY_PRESS;

	if (action == GLFW_PRESS)
		ac = GRAIL_KEY_PRESS;
	if (action == GLFW_REPEAT)
		ac = GRAIL_KEY_REPEAT;
	if (action == GLFW_RELEASE)
		ac = GRAIL_KEY_RELEASE;

	keyPressCallback(instance, key, scancode, ac, mods);
}

void grail::ApplicationWindow::mouseScrollCallback(GLFWwindow * window, double xoffset, double yoffset) {
	ApplicationWindow* instance = static_cast<ApplicationWindow*>(glfwGetWindowUserPointer(window));

	mouseScrollCallback(instance, xoffset, yoffset);
}

void grail::ApplicationWindow::dropCallback(GLFWwindow * window, int count, const char ** paths) {
	ApplicationWindow* instance = static_cast<ApplicationWindow*>(glfwGetWindowUserPointer(window));

	dropCallback(instance, count, paths);
}

void grail::ApplicationWindow::charCallback(GLFWwindow * window, unsigned int c, int mods) {
	ApplicationWindow* instance = static_cast<ApplicationWindow*>(glfwGetWindowUserPointer(window));

	charCallback(instance, c, mods);
}

#endif

void grail::ApplicationWindow::mouseMovementCallback(ApplicationWindow* self, float posX, float posY) {
	Input::mouseX = posX;
	Input::mouseY = posY;

	self->screenManager->cursorMoved(posX, posY);
}

void grail::ApplicationWindow::mouseClickCallback(ApplicationWindow * self, int button, int action, int mods) {
	if (action == GRAIL_MOUSE_PRESS) {
		Input::buttons[button] = true;
		self->screenManager->mousePressEvent(button, action);
	}

	if (action == GRAIL_MOUSE_RELEASE) {
		Input::buttons[button] = false;
		Input::justPressedButtons[button] = false;
		self->screenManager->mousePressEvent(button, action);
	}
}

void grail::ApplicationWindow::keyPressCallback(ApplicationWindow * self, int key, int scancode, int action, int mods) {
	if (action == GRAIL_KEY_PRESS || action == GRAIL_KEY_REPEAT) {
		Input::keys[key] = true;
		self->screenManager->keyPressEvent(key, GRAIL_KEY_PRESS);
	}

	if (action == GRAIL_KEY_RELEASE) {
		Input::keys[key] = false;
		Input::justPressedKeys[key] = false;
		self->screenManager->keyPressEvent(key, GRAIL_KEY_RELEASE);
	}
}

void grail::ApplicationWindow::mouseScrollCallback(ApplicationWindow * self, float xoffset, float yoffset) {
	Input::mouseScrollX = xoffset;
	Input::mouseScrollY = yoffset;
}

void grail::ApplicationWindow::dropCallback(ApplicationWindow * self, int count, const char ** paths) {
	self->screenManager->dropCallback(count, paths);
}

void grail::ApplicationWindow::charCallback(ApplicationWindow * self, unsigned int c, int mods) {
	self->screenManager->charCallback(c, mods);
}
