#include "Resource.h"

grail::ResourceType grail::Resource::getResourceType() {
	return resourceType;
}

std::string grail::Resource::getIdentifier() {
	return identifier;
}
