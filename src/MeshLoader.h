#ifndef GRAIL_MESH_LOADER_H
#define GRAIL_MESH_LOADER_H

#include "Mesh.h"

#include <string>
#include <vector>


struct aiNode;
struct aiScene;
struct aiMaterial;
struct aiMesh;

namespace grail {
	class VertexLayout;
	class SceneNode;
	class Renderer;
	class Material;
	class MemoryAllocator;

	class MeshLoader {
	public:
		static SceneNode* loadMesh(std::string path, VertexLayout* vertexLayout, Renderer* renderer, bool loadMaterials, std::string customMaterialFile, MemoryAllocator* stagingMemory, MemoryAllocator* dedicatedMemory);
		static void loadBaseMesh(std::string path, VertexLayout* vertexLayout, Renderer* renderer, bool loadMaterials, std::string customMaterialFile, MemoryAllocator* stagingMemory, MemoryAllocator* dedicatedMemory);
		static Mesh* createMesh(std::string identifier, VertexLayout* vertexLayout, std::vector<float>& vertices, std::vector<uint32_t>& indices, MemoryAllocator* stagingMemory, MemoryAllocator* dedicatedMemory);


		// Still unused for now, need to fix
		static void loadModel(std::string path, VertexLayout* vertexLayout, MemoryAllocator* statingMemory, MemoryAllocator* dedicatedMemory, bool insertInScene);
	private:

		struct MeshData {
			Mesh* mesh;

			std::string identifier;

			std::vector<float> vertices;
			std::vector<uint32_t> indices;

			glm::vec3 boundMin = glm::vec3(std::numeric_limits<float>().min());
			glm::vec3 boundsMax = glm::vec3(std::numeric_limits<float>().max());
		};

		static void processNode(std::string path, VertexLayout* vertexLayout, aiNode* node, const aiScene* scene, MemoryAllocator* stagingRegion, MemoryAllocator* dedicatedMemory, SceneNode* baseNode = nullptr);
		static Mesh* parseMesh(aiMesh* node, VertexLayout* vertexLayout, MemoryAllocator* stagingMemory, MemoryAllocator* dedicatedMemory);

		static SceneNode* processNode(std::string path, std::string directory, VertexLayout* vertexLayout, Renderer* renderer, bool loadMaterials, aiNode* node, const aiScene* scene, MemoryAllocator* stagingMemory, MemoryAllocator* dedicatedMemory);
		static void processBaseNode(std::string path, std::string directory, VertexLayout* vertexLayout, Renderer* renderer, bool loadMaterials, aiNode* node, const aiScene* scene, MemoryAllocator* stagingMemory, MemoryAllocator* dedicatedMemory);
		static Material* processMaterial(Renderer* renderer, aiMaterial* aMaterial, const aiScene* scene, std::string directory, SceneNode* node);
		static Mesh* parseMesh(aiMesh* aMesh, const aiScene* scene, glm::vec3& boundsMin, glm::vec3& boundMax, MemoryAllocator* stagingMemory, MemoryAllocator* dedicatedMemory);

		static void loadMeshFromFile(std::string path, std::vector<MeshData>& data, VertexLayout* layout);

		MeshLoader& operator = (const MeshLoader&) = delete;
		MeshLoader(const MeshLoader&) = delete;
		MeshLoader() = default;
	};
}

#endif