#ifndef GRAIL_FRUSTUM_H
#define GRAIL_FRUSTUM_H

#include "Common.h"

#include <array>

namespace grail {
	class Frustum {
	public:
		Frustum();

		void update(glm::mat4 combinedMatrix);

		bool pointInFrustum(float x, float y, float z);
		bool pointInFrustum(glm::vec3 point);

		bool sphereInFrustum(float x, float y, float z, float radius);
		bool sphereInFrustum(glm::vec3 position, float radius);

		bool AABBInFrustum(glm::vec3 min, glm::vec3 max);
	private:
		glm::vec4 normalizePlane(const glm::vec4 plane);

		enum PlaneSide { LEFT = 0, RIGHT = 1, TOP = 2, BOTTOM = 3, BACK = 4, FRONT = 5 };
		glm::vec4 planes[6];
	};
}

#endif