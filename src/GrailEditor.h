#ifndef GRAIL_ENGINE_EDITOR_H
#define GRAIL_ENGINE_EDITOR_H

#include "Screen.h"

#include "Common.h"
#include "ImGuiHelper.h"
#include "Asset.h"
#include "ImGuizmo.h"

class TextEditor;

namespace grail {

	class PerspectiveCamera;
	class FirstPersonCameraController;
	class ImGuiHelper;
	class ImGuiConsole;
	class SceneNode;
	class Script;
	class DeferredRenderer;
	class FrameGraph;
	class Mesh;
	class TextureAssetImporter;
	class AssetImporter;

	namespace editor {
		static VkTexture* iconAtlas = nullptr;

		struct Icon {
			float u;
			float v;
			float u2;
			float v2;

			ImVec2 uv0;
			ImVec2 uv1;
		};

		static std::unordered_map<std::string, Icon> icons;
		
		void loadIcons();
		Icon& getIcon(const std::string& id);
	}

	class GrailEditor : public grail::Screen {
	public:
		GrailEditor();

		void init();
		void render(float delta);

	private:
		std::string projectDirectory = "./projects/test/";

		void rescanAssets(const std::string& path);

		struct {
			TextureAssetImporter* texture;
		} importers;
		AssetImporter* getImporterByExtension(const std::string& extension);
		void handleFileImport(const std::string& absolutePath);

		void renderUI();

		PerspectiveCamera* camera = nullptr;
		FirstPersonCameraController* cameraController = nullptr;

		DeferredRenderer* renderer = nullptr;
		ImGuiHelper* imguiHelper = nullptr;

		std::vector<std::string> pendingImports;

		std::vector<std::string> openPopups;
		void openPopup(const std::string& id);
		bool endPopup(const std::string& id);

		void drawUIImage(const std::string& id, glm::vec2 size);
		void drawTexture(const AssetRef<TextureAsset>& texture, glm::vec2 size);


		// PROJECT STRUCTURE
		struct {
			float itemSize = 94.0f;
			float innerPadding = 5.0f;
			std::string currentDir = "";

			std::vector<std::string> selectedFiles;

			float renderTime;
		} structureViewer;
		void renderStructureViewer();


		// SCENE HIERARCHY
		struct {
			SceneNode* selectedNode = nullptr;
			bool renaming = false;

			std::string renameString;
		} sceneHierarchy;
		void renderSceneHierarchy();

		// TRANSFORM GIZMO
		struct {
			ImGuizmo::OPERATION op = ImGuizmo::OPERATION::DISABLED;

			bool useSnapping = false;
			bool transforming = false;
			bool transformWorld = true;

			glm::vec3 positionSnap = glm::vec3(1.0);
			glm::vec3 rotationSnap = glm::vec3(1.0);
			glm::vec3 scaleSnap = glm::vec3(1.0);

			glm::mat4 localCopy;
			glm::mat4 worldCopy;
			glm::vec3 scaleCopy;
			glm::mat4 delta = glm::mat4(1.0);
			glm::vec3 temp;
		} gizmo;
		void renderTransformGizmo();

		// INSPECTOR
		enum class InspectedItemType {
			NONE,
			TEXTURE,
			NODE,
			MESH,
			SHADER,
			SCRIPT,
			FILE
		};

		struct {
			void* inspectedItem = nullptr;
			InspectedItemType type = InspectedItemType::NONE;
		} inspector;
		void renderInspector();
		void inspectNode();

		void saveEditorLayout();

		void createDockspace();
	};
}


#endif