#include "Swapchain.h"
#include "Logger.h"
#include "ApplicationWindow.h"

grail::Swapchain::Swapchain(ApplicationWindow* window, GPU& gpu) {
	this->gpu = &gpu;

	GRAIL_LOG(INFO, tag) << "Loading swapchain function pointers...";
	GET_INSTANCE_PROC_ADDR(VulkanContext::instance, GetPhysicalDeviceSurfaceSupportKHR);
	GET_INSTANCE_PROC_ADDR(VulkanContext::instance, GetPhysicalDeviceSurfaceCapabilitiesKHR);
	GET_INSTANCE_PROC_ADDR(VulkanContext::instance, GetPhysicalDeviceSurfaceFormatsKHR);
	GET_INSTANCE_PROC_ADDR(VulkanContext::instance, GetPhysicalDeviceSurfacePresentModesKHR);

	GRAIL_LOG(INFO, tag) << "Loading swapchain device function pointers...";
	GET_DEVICE_PROC_ADDR(gpu.device, CreateSwapchainKHR);
	GET_DEVICE_PROC_ADDR(gpu.device, DestroySwapchainKHR);
	GET_DEVICE_PROC_ADDR(gpu.device, GetSwapchainImagesKHR);
	GET_DEVICE_PROC_ADDR(gpu.device, AcquireNextImageKHR);
	GET_DEVICE_PROC_ADDR(gpu.device, QueuePresentKHR);

	surface = window->createSurface();

	GRAIL_LOG(INFO, tag) << "Looking for a present supporting queue...";
	uint32_t queueCount;
	vkGetPhysicalDeviceQueueFamilyProperties(gpu.physicalDevice, &queueCount, nullptr);
	if (queueCount <= 0) {
		GRAIL_LOG(INFO, tag) << "Could not find any queues!";
		assert(false);
	}

	std::vector<VkQueueFamilyProperties> queueProps(queueCount);
	vkGetPhysicalDeviceQueueFamilyProperties(gpu.physicalDevice, &queueCount, &queueProps[0]);

	std::vector<VkBool32> supportsPresent(queueCount);
	for (uint32_t i = 0; i < queueCount; i++) {
		fpGetPhysicalDeviceSurfaceSupportKHR(gpu.physicalDevice, i, surface, &supportsPresent[i]);
	}

	uint32_t presentQueueNodeIndex = UINT32_MAX;
	for (uint32_t i = 0; i < queueCount; i++) {
		if ((queueProps[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) != 0) {
			if (supportsPresent[i] == VK_TRUE) {
				presentQueueNodeIndex = i;
				break;
			}
		}
	}

	if (gpu.graphicsQueue.index == UINT32_MAX || presentQueueNodeIndex == UINT32_MAX) {
		GRAIL_LOG(INFO, tag) << "Could not find a graphics and/or presenting queue!";
		assert(false);
	}

	if (gpu.graphicsQueue.index != presentQueueNodeIndex) {
		GRAIL_LOG(INFO, tag) << "Separate graphics and presenting queues are not supported yet!";
		assert(false);
	}
	queueNodeIndex = gpu.graphicsQueue.index;

	GRAIL_LOG(INFO, tag) << "Searching for an optimal color space and format...";
	uint32_t formatCount;
	VK_VALIDATE_RESULT(fpGetPhysicalDeviceSurfaceFormatsKHR(gpu.physicalDevice, surface, &formatCount, nullptr));

	std::vector<VkSurfaceFormatKHR> surfaceFormats(formatCount);
	VK_VALIDATE_RESULT(fpGetPhysicalDeviceSurfaceFormatsKHR(gpu.physicalDevice, surface, &formatCount, surfaceFormats.data()));

	//GRAIL_LOG(INFO, "TT") << gpu.colorFormat;

	if ((formatCount == 1) && (surfaceFormats[0].format == VK_FORMAT_UNDEFINED))
		gpu.colorFormat = VK_FORMAT_B8G8R8A8_UNORM;
	else
		gpu.colorFormat = surfaceFormats[0].format;

	gpu.colorSpace = surfaceFormats[0].colorSpace;

	//GRAIL_LOG(INFO, "TT") << gpu.colorFormat;
	
	GRAIL_LOG(INFO, tag) << "Searching for an optimal depth format...";
	VkBool32 validDepthFormat = gpu.getSupportedDepthFormat(&gpu.depthFormat);

	if (!validDepthFormat) {
		GRAIL_LOG(INFO, tag) << "Could not find a valid depth format";
		assert(false);
	}
}

void grail::Swapchain::create(VkCommandBuffer commandBuffer, uint32_t * width, uint32_t * height, bool vSync) {
	VkSwapchainKHR oldSwapchain = handle;

	VkSurfaceCapabilitiesKHR surfaceCaps;
	VK_VALIDATE_RESULT(fpGetPhysicalDeviceSurfaceCapabilitiesKHR(gpu->physicalDevice, surface, &surfaceCaps));

	uint32_t presentModeCount;
	VK_VALIDATE_RESULT(fpGetPhysicalDeviceSurfacePresentModesKHR(gpu->physicalDevice, surface, &presentModeCount, nullptr));
	if (presentModeCount <= 0) {
		Log(LogType::ERROR, tag) << "Could not find any present modes";
		assert(false);
	}

	GRAIL_LOG(INFO, tag) << "Searching for available present modes...";
	std::vector<VkPresentModeKHR> presentModes(presentModeCount);
	VK_VALIDATE_RESULT(fpGetPhysicalDeviceSurfacePresentModesKHR(gpu->physicalDevice, surface, &presentModeCount, &presentModes[0]));

	VkExtent2D extent = {};
	if (surfaceCaps.currentExtent.width == (uint32_t)-1) {
		extent.width = *width;
		extent.height = *height;
	}
	else {
		extent = surfaceCaps.currentExtent;
		*width = surfaceCaps.currentExtent.width;
		*height = surfaceCaps.currentExtent.height;
	}

	this->width = *width;
	this->height = *height;

	VkPresentModeKHR presentMode = VK_PRESENT_MODE_FIFO_KHR;

	if (!vSync) {
		for (size_t i = 0; i < presentModeCount; i++) {
            GRAIL_LOG(INFO, "Available present modes") << (int)presentModes[i];
            
			if (presentModes[i] == VK_PRESENT_MODE_MAILBOX_KHR) {
				presentMode = VK_PRESENT_MODE_MAILBOX_KHR;
				break;
			}

			if ((presentMode != VK_PRESENT_MODE_MAILBOX_KHR) && (presentModes[i] == VK_PRESENT_MODE_FIFO_RELAXED_KHR)) {
				presentMode = VK_PRESENT_MODE_IMMEDIATE_KHR;
			}
		}
	}
	
    GRAIL_LOG(INFO, "SWAPCHAIN MODE") << (int)presentMode;

	uint32_t desiredNumberOfSwapchainImages = surfaceCaps.minImageCount + 1;
	if ((surfaceCaps.maxImageCount > 0) && (desiredNumberOfSwapchainImages > surfaceCaps.maxImageCount)) {
		desiredNumberOfSwapchainImages = surfaceCaps.maxImageCount;
	}

	VkSurfaceTransformFlagsKHR preTransform;
	if (surfaceCaps.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR) {
		preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
	}
	else {
		preTransform = surfaceCaps.currentTransform;
	}

	GRAIL_LOG(INFO, "SWAPCHAIN") << "Available Swapchain Images: " << desiredNumberOfSwapchainImages;

	VkSwapchainCreateInfoKHR swapchainCI = {};
	swapchainCI.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	swapchainCI.pNext = nullptr;
	swapchainCI.surface = surface;
	swapchainCI.minImageCount = desiredNumberOfSwapchainImages;
	swapchainCI.imageFormat = gpu->colorFormat;
	swapchainCI.imageColorSpace = gpu->colorSpace;
	swapchainCI.imageExtent = { extent.width, extent.height };
	swapchainCI.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
	swapchainCI.preTransform = (VkSurfaceTransformFlagBitsKHR)preTransform;
	swapchainCI.imageArrayLayers = 1;
	swapchainCI.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
	swapchainCI.queueFamilyIndexCount = 0;
	swapchainCI.pQueueFamilyIndices = nullptr;
	swapchainCI.presentMode = presentMode;
	swapchainCI.oldSwapchain = oldSwapchain;
	swapchainCI.clipped = true;
	swapchainCI.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
#if defined (OS_ANDROID)
	swapchainCI.compositeAlpha = VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR;
#endif
	VK_VALIDATE_RESULT(fpCreateSwapchainKHR(gpu->device, &swapchainCI, nullptr, &handle));

	if (oldSwapchain != VK_NULL_HANDLE) {
		for (uint32_t i = 0; i < imageCount; i++) {
			vkDestroyImageView(gpu->device, buffers[i].view, nullptr);
		}
		fpDestroySwapchainKHR(gpu->device, oldSwapchain, nullptr);
	}

	VK_VALIDATE_RESULT(fpGetSwapchainImagesKHR(gpu->device, handle, &imageCount, nullptr));

	images.resize(imageCount);
	VK_VALIDATE_RESULT(fpGetSwapchainImagesKHR(gpu->device, handle, &imageCount, &images[0]));

	buffers.resize(imageCount);
	for (uint32_t i = 0; i < imageCount; i++) {
		VkImageViewCreateInfo colorAttachmentView = {};
		colorAttachmentView.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		colorAttachmentView.pNext = nullptr;
		colorAttachmentView.format = gpu->colorFormat;
		colorAttachmentView.components = {
			VK_COMPONENT_SWIZZLE_R,
			VK_COMPONENT_SWIZZLE_G,
			VK_COMPONENT_SWIZZLE_B,
			VK_COMPONENT_SWIZZLE_A
		};
		colorAttachmentView.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		colorAttachmentView.subresourceRange.baseMipLevel = 0;
		colorAttachmentView.subresourceRange.levelCount = 1;
		colorAttachmentView.subresourceRange.baseArrayLayer = 0;
		colorAttachmentView.subresourceRange.layerCount = 1;
		colorAttachmentView.viewType = VK_IMAGE_VIEW_TYPE_2D;
		colorAttachmentView.flags = 0;

		buffers[i].image = images[i];

		/*vulkanTools::setImageLayout(
		commandBuffer,
		buffers[i].image,
		VK_IMAGE_ASPECT_COLOR_BIT,
		VK_IMAGE_LAYOUT_UNDEFINED,
		VK_IMAGE_LAYOUT_PRESENT_SRC_KHR);*/

		colorAttachmentView.image = buffers[i].image;

		VK_VALIDATE_RESULT(vkCreateImageView(gpu->device, &colorAttachmentView, nullptr, &buffers[i].view));
	}
}

void grail::Swapchain::createFramebuffers(GPU& gpu, VkRenderPass renderPass) {
	VkImageView imgAttachments[1];

	VkFramebufferCreateInfo framebufferInfo = {};
	framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
	framebufferInfo.pNext = nullptr;
	framebufferInfo.renderPass = renderPass;
	framebufferInfo.attachmentCount = 1;
	framebufferInfo.pAttachments = &imgAttachments[0];
	framebufferInfo.width = width;
	framebufferInfo.height = height;
	framebufferInfo.layers = 1;

	framebuffers.resize(imageCount);
	for (uint32_t i = 0; i < framebuffers.size(); i++) {
		imgAttachments[0] = buffers[i].view;
		VK_VALIDATE_RESULT(vkCreateFramebuffer(gpu.device, &framebufferInfo, nullptr, &framebuffers[i]));
	}
}

void grail::Swapchain::recreate() {
	create(VK_NULL_HANDLE, &Graphics::width, &Graphics::height, true);
}

VkResult grail::Swapchain::aquireNextImage(VkSemaphore semaphore, uint32_t * currentBuffer, VkFence fence) {
	return fpAcquireNextImageKHR(gpu->device, handle, UINT64_MAX, semaphore, fence, currentBuffer);
}

VkResult grail::Swapchain::queuePresent(VkQueue queue, uint32_t currentBuffer) {
	VkPresentInfoKHR presentInfo = {};
	presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
	presentInfo.pNext = NULL;
	presentInfo.swapchainCount = 1;
	presentInfo.pSwapchains = &handle;
	presentInfo.pImageIndices = &currentBuffer;
	return fpQueuePresentKHR(queue, &presentInfo);
}

VkResult grail::Swapchain::queuePresent(VkQueue queue, uint32_t currentBuffer, VkSemaphore waitSemaphore) {
	VkPresentInfoKHR presentInfo = {};
	presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
	presentInfo.pNext = nullptr;
	presentInfo.swapchainCount = 1;
	presentInfo.pSwapchains = &handle;
	presentInfo.pImageIndices = &currentBuffer;
	if (waitSemaphore != VK_NULL_HANDLE) {
		presentInfo.pWaitSemaphores = &waitSemaphore;
		presentInfo.waitSemaphoreCount = 1;
	}
	return fpQueuePresentKHR(queue, &presentInfo);
}

/*void grail::Swapchain::dispose() {
	for (uint32_t i = 0; i < imageCount; i++) {
		vkDestroyImageView(gpu.device, buffers[i].view, nullptr);
	}
	fpDestroySwapchainKHR(gpu.device, handle, nullptr);
	vkDestroySurfaceKHR(VulkanContext::instance, surface, nullptr);
}*/
