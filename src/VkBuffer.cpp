#include "VkBuffer.h"
#include "Logger.h"

#include "RayTracingContext.h"

grail::Buffer::Buffer(BufferCreateInfo & createInfo, MemoryAllocator * memoryRegion, void * data, VkDeviceSize dataSize) {
	this->memoryType = createInfo.memoryType;
	this->instanceCount = (memoryType == MemoryType::DYNAMIC ? Graphics::getPrerenderedFrameCount() : 1);
	this->instanceSize = createInfo.size;

	VkBufferCreateInfo bufferCreateInfo = {};
	bufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferCreateInfo.pNext = nullptr;
	bufferCreateInfo.flags = static_cast<VkBufferCreateFlags>(createInfo.createFlags);
	bufferCreateInfo.size = createInfo.size * instanceCount + (instanceCount > 1 ? 256 * (instanceCount - 1) : 0);
	bufferCreateInfo.usage = static_cast<VkBufferUsageFlags>(createInfo.usageBits);
	bufferCreateInfo.sharingMode = static_cast<VkSharingMode>(createInfo.sharingMode);
	bufferCreateInfo.queueFamilyIndexCount = createInfo.queueFamilyIndexCount;
	bufferCreateInfo.pQueueFamilyIndices = createInfo.queueFamilyIndices;
	VK_VALIDATE_RESULT(vkCreateBuffer(createInfo.gpu.device, &bufferCreateInfo, nullptr, &handle));

	vkGetBufferMemoryRequirements(createInfo.gpu.device, handle, &memoryRequirements);

	if (createInfo.specialMemoryReqs.memoryRequirements.size > 0) {
		memoryRequirements = createInfo.specialMemoryReqs.memoryRequirements;
	}

	// If we plan to use more instances of the same data, all instances have to be aligned
	if (instanceCount > 1) {
		uint32_t additionalSize = (instanceCount - 1) * memoryRequirements.alignment;
		bufferCreateInfo.size += additionalSize;
	}

	memoryDescriptor = 
		memoryRegion->allocate(
			bufferCreateInfo.size, 
			memoryRequirements.alignment,
			static_cast<VkMemoryPropertyFlagBits>(createInfo.memoryProperties),
			memoryRequirements.memoryTypeBits
	);
	VK_VALIDATE_RESULT(vkBindBufferMemory(createInfo.gpu.device, handle, memoryDescriptor.page->memoryHandle, memoryDescriptor.baseAddressAligned));

	// Setup ranges
	descriptors.push_back({ handle, 0, instanceSize });
	uint32_t current = instanceSize;
	if (instanceCount > 1) {
		for (uint32_t i = 1; i < instanceCount; i++) {
			VkDeviceSize remainder = current % memoryRequirements.alignment;
			VkDeviceSize alignedOffset = current + (memoryRequirements.alignment - remainder);

			VkDescriptorBufferInfo descriptor = {};
			descriptor.buffer = handle;
			descriptor.offset = alignedOffset;
			descriptor.range = instanceSize;

			current += alignedOffset;

			descriptors.push_back(descriptor);
		}
	}

	if (data != nullptr && dataSize > 0) {
		void* ptr = memoryDescriptor.mapMemory();

		if (memoryType == MemoryType::DYNAMIC) {
			for (uint32_t i = 0; i < Graphics::getPrerenderedFrameCount(); i++) {
				memcpy(static_cast<void*>(static_cast<char*>(ptr) + (i * (bufferCreateInfo.size / Graphics::getPrerenderedFrameCount()))), data, dataSize);
			}
		}
		else {
			memcpy(ptr, data, dataSize);
		}
		memoryDescriptor.unmapMemory();
	}


	this->memoryProperties = createInfo.memoryProperties;
	this->gpu = createInfo.gpu;
}

VkBuffer& grail::Buffer::getHandle() {
	return handle;
}

VkMemoryRequirements grail::Buffer::getMemoryRequirements() {
	return memoryRequirements;
}

grail::MemoryAllocation grail::Buffer::getMemoryRegionDescriptor() {
	return memoryDescriptor;
}

void grail::Buffer::copyData(void* dataPtr, uint32_t size) {
	if (static_cast<int>(memoryProperties & MemoryPropertyFlags::HOST_VISIBLE)) {
		//if (!memoryDescriptor.block->mapped) {
			map();
		//}

		if ( mappedPtr != nullptr) {
			for (uint32_t i = 0; i < instanceCount; i++) {
				memcpy(static_cast<void*>(static_cast<char*>(mappedPtr) + descriptors[i].offset), dataPtr, size);
			}
		}
		else {
			GRAIL_LOG(WARNING, "Buffer") << "Attempting to copy data to unmapped buffer!";
		}
	}
	else {
		GRAIL_LOG(WARNING, "BUFFER") << "Attempting to copy data to memory not visible by host!";
	}
}

void grail::Buffer::updateData(void * dataPtr, uint32_t size) {
	if (static_cast<int>(memoryProperties & MemoryPropertyFlags::HOST_VISIBLE)) {
		//if (!memoryDescriptor.block->mapped) {
			map();
		//}

		if (mappedPtr != nullptr) {
			uint32_t index = (instanceCount > 1) ?
				(Graphics::getFrameIndex()) % Graphics::getPrerenderedFrameCount() :
				0;
				 
			memcpy(static_cast<void*>(static_cast<char*>(mappedPtr) + descriptors[index].offset), dataPtr, size);
		}
		else {
			GRAIL_LOG(WARNING, "Buffer") << "Attempting to copy data to unmapped buffer!";
		}
	}
	else {
		GRAIL_LOG(WARNING, "BUFFER") << "Attempting to copy data to memory not visible by host!";
	}
}

void grail::Buffer::map() {
	mappedPtr = memoryDescriptor.mapMemory();

	/*if (static_cast<int>(memoryProperties & MemoryPropertyFlags::HOST_VISIBLE)) {
		if (!memoryDescriptor.block->mapped) {
			mappedPtr = memoryDescriptor.block->mapMemory(memoryDescriptor);
		}
		else {
			//GRAIL_LOG(WARNING, "BUFFER") << "Attempting to map already mapped memory";
			memoryDescriptor.block->unmapMemory();
			mappedPtr = memoryDescriptor.block->mapMemory(memoryDescriptor);
		}
	}
	else {
		GRAIL_LOG(WARNING, "BUFFER") << "Attempting to map buffer memory not visible by host!";
	}*/
}

void grail::Buffer::unmap() {
	memoryDescriptor.unmapMemory();
	mappedPtr = nullptr;

	/*if (memoryDescriptor.block->mapped) {
		if (static_cast<int>(memoryProperties & MemoryPropertyFlags::HOST_VISIBLE)) {
			//GRAIL_LOG(WARNING, "BUFFER") << "Cannot unmap host coherent memory";
			if (memoryDescriptor.block->mapped) {
				memoryDescriptor.block->unmapMemory();
				mappedPtr = nullptr;
			}
			else {
				mappedPtr = nullptr;
			}
		}
		else {
			
		}
	}
	else {
		GRAIL_LOG(WARNING, "BUFFER") << "Attempting to unmap unmapped memory!";
	}*/
}

void grail::Buffer::flush() {
	if (mapped) {
		if (static_cast<int>(memoryProperties & MemoryPropertyFlags::HOST_COHERENT)) {
			unmap();
			//return;
		}
		else {
			unmap();
		}
	}

	VkMappedMemoryRange mappedRange = {};
	mappedRange.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
	mappedRange.memory = memoryDescriptor.page->memoryHandle;
	mappedRange.offset = memoryDescriptor.baseAddressAligned;
	mappedRange.size = VK_WHOLE_SIZE;

	VK_VALIDATE_RESULT(vkFlushMappedMemoryRanges(memoryDescriptor.page->allocator->getGPU().device, 1, &mappedRange));
}

VkDescriptorBufferInfo& grail::Buffer::getDescriptor() {
	return descriptors[0];
}

VkDescriptorBufferInfo & grail::Buffer::getDescriptor(uint32_t frameIndex) {
	uint32_t bounded = frameIndex;
	if (bounded < 0) bounded = 0;
	if (bounded >= descriptors.size()) bounded = descriptors.size() - 1;

	return descriptors[bounded];
}

grail::MemoryType grail::Buffer::getMemoryType() {
	return memoryType;
}

VkDeviceSize grail::Buffer::getOffset() {
	if (instanceCount == 1) {
		return descriptors[0].offset;
	}
	else {
		return descriptors[Graphics::getFrameIndex()].offset;
	}
}

void grail::Buffer::dispose() {
	vkDestroyBuffer(gpu.device, handle, nullptr);

	if (memoryDescriptor.size > 0) {
		memoryDescriptor.page->allocator->deallocate(memoryDescriptor);
	}
}

void* grail::Buffer::getMappedPtr() {
	return mappedPtr;
}
