#include "MaterialLibrary.h"
//#include "Material.h"

grail::MaterialLibrary::MaterialLibrary(PipelineFactory * factory) {
	this->factory = factory;
}

grail::MaterialLibrary::~MaterialLibrary() {
	for (auto it = library.begin(); it != library.end(); it++) {
		delete it->second;
		it->second = nullptr;
	}

	for (unsigned int i = 0; i < materials.size(); i++) {
		delete materials[i];
	}
}

void grail::MaterialLibrary::registerBaseMaterial(std::string name, Material * material) {
	library[GRAIL_SID(name)] = material;
}

grail::Material * grail::MaterialLibrary::createMaterial(std::string baseName) {
	auto item = library.find(GRAIL_SID(baseName));

	if (item != library.end()) {
		/*Material* base = item->second;

		MaterialCreateInfo createInfo = {};
		createInfo.layout = base->vertexLayout;
		//createInfo.descriptors = DescriptorSetGroup(base->descriptors);
		//createInfo.pipelineInformation = base->pipelineInformation;
		createInfo.pipelineFactory = factory;
		createInfo.type = base->type;

		Material* copy = new Material(createInfo);
		copy->getDescriptors().update();
		materials.push_back(copy);*/

		/*return copy;*/
	}

	throw new exceptions::RuntimeException("Base material: " + baseName + " not found");
	return nullptr;
}
