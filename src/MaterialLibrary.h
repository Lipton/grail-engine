#ifndef GRAIL_MATERIAL_LIBRARY_H
#define GRAIL_MATERIAL_LIBRARY_H

#include <map>
#include <vector>
#include <vulkan.h>

#include "PipelineFactory.h"
#include "Utils.h"

namespace grail {
	class Material;

	class MaterialLibrary {
	public:
		MaterialLibrary(PipelineFactory* factory);
		~MaterialLibrary();

		void registerBaseMaterial(std::string name, Material* material);

		Material* createMaterial(std::string baseName);
	private:
		std::map<uint32_t, Material*> library;
		std::vector<Material*> materials;

		PipelineFactory* factory;
	};
}

#endif