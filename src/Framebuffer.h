#ifndef GRAIL_FRAMEBUFFER_H
#define GRAIL_FRAMEBUFFER_H

#include "VulkanContext.h"
#include "VkTexture.h"
#include "Resources.h"

namespace grail {
	struct FramebufferCreateInfo {
		uint32_t width;
		uint32_t height;

		GPU gpu = VulkanContext::mainGPU;
	};

	struct FramebufferAttachment {
		uint32_t width;
		uint32_t height;

		VkTexture* texture;
		VkAttachmentDescription description;
	};

	class Framebuffer {
	public:
		uint32_t width;
		uint32_t height;

		GPU gpu;

		VkRenderPass renderPass;
		VkFramebuffer handle;

		std::vector<FramebufferAttachment> attachments;

		Framebuffer(FramebufferCreateInfo& createInfo) {
			this->width = createInfo.width;
			this->height = createInfo.height;

			this->gpu = createInfo.gpu;
		}

		void addAttachment(FramebufferAttachment attachment) {
			attachments.push_back(attachment);
		}

		void createAttachment(std::string identifier, TextureCreateInfo& createInfo) {
			VkTexture* texture = Resources::createTexture(identifier, static_cast<int>(width), static_cast<int>(height), createInfo);
			VkAttachmentDescription desc = {};
			desc.flags = 0;
			desc.samples = static_cast<VkSampleCountFlagBits>(createInfo.sampleCount);
			desc.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
			desc.storeOp = (static_cast<uint32_t>(createInfo.usageBits) & static_cast<uint32_t>(ImageUsageBits::SAMPLED_BIT)) ? VK_ATTACHMENT_STORE_OP_STORE : VK_ATTACHMENT_STORE_OP_DONT_CARE;
			desc.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
			desc.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
			desc.format = createInfo.format;
			desc.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

			if (VulkanContext::hasDepth(desc.format) || VulkanContext::hasStencil(desc.format)) {
				desc.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
			}
			else {
				desc.finalLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
			}

			FramebufferAttachment att = {};
			att.description = desc;
			att.texture = texture;

			attachments.push_back(att);
		}

		void finalizeCreation() {
			std::vector<VkAttachmentDescription> attachmentDescriptions;
			for (auto& attachment : attachments) {
				attachmentDescriptions.push_back(attachment.description);
			};

			std::vector<VkAttachmentReference> colorReferences;
			VkAttachmentReference depthReference = {};
			bool hasDepth = false;
			bool hasColor = false;

			uint32_t attachmentIndex = 0;

			for (auto& attachment : attachments) {
				if (VulkanContext::isDepthStencil(attachment.description.format)) {
					assert(!hasDepth);
					depthReference.attachment = attachmentIndex;
					depthReference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
					hasDepth = true;
				}
				else {
					colorReferences.push_back({ attachmentIndex, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL });
					hasColor = true;
				}
				attachmentIndex++;
			};

			VkSubpassDescription subpass = {};
			subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
			if (hasColor) {
				subpass.pColorAttachments = colorReferences.data();
				subpass.colorAttachmentCount = static_cast<uint32_t>(colorReferences.size());
			}
			if (hasDepth) {
				subpass.pDepthStencilAttachment = &depthReference;
			}

			std::array<VkSubpassDependency, 2> dependencies;

			dependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
			dependencies[0].dstSubpass = 0;
			dependencies[0].srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
			dependencies[0].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
			dependencies[0].srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
			dependencies[0].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
			dependencies[0].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

			dependencies[1].srcSubpass = 0;
			dependencies[1].dstSubpass = VK_SUBPASS_EXTERNAL;
			dependencies[1].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
			dependencies[1].dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
			dependencies[1].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
			dependencies[1].dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
			dependencies[1].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

			VkRenderPassCreateInfo renderPassInfo = {};
			renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
			renderPassInfo.pAttachments = attachmentDescriptions.data();
			renderPassInfo.attachmentCount = static_cast<uint32_t>(attachmentDescriptions.size());
			renderPassInfo.subpassCount = 1;
			renderPassInfo.pSubpasses = &subpass;
			renderPassInfo.dependencyCount = 2;
			renderPassInfo.pDependencies = dependencies.data();
			VK_VALIDATE_RESULT(vkCreateRenderPass(gpu.device, &renderPassInfo, nullptr, &renderPass));

			std::vector<VkImageView> attachmentViews;
			for (auto attachment : attachments) {
				attachmentViews.push_back(attachment.texture->getImageView());
			}

			uint32_t maxLayers = 0;
			for (auto attachment : attachments) {
				if (attachment.texture->getLayerCount() > maxLayers) {
					maxLayers = attachment.texture->getLayerCount();
				}
			}

			VkFramebufferCreateInfo framebufferInfo = {};
			framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
			framebufferInfo.renderPass = renderPass;
			framebufferInfo.pAttachments = attachmentViews.data();
			framebufferInfo.attachmentCount = static_cast<uint32_t>(attachmentViews.size());
			framebufferInfo.width = width;
			framebufferInfo.height = height;
			framebufferInfo.layers = maxLayers;
			VK_VALIDATE_RESULT(vkCreateFramebuffer(gpu.device, &framebufferInfo, nullptr, &handle));
		}

		void finalizeCreation(VkRenderPass renderPass) {
			std::vector<VkImageView> attachmentViews;
			for (auto attachment : attachments) {
				attachmentViews.push_back(attachment.texture->getImageView());
			}

			uint32_t maxLayers = 0;
			for (auto attachment : attachments) {
				if (attachment.texture->getLayerCount() > maxLayers) {
					maxLayers = attachment.texture->getLayerCount();
				}
			}

			VkFramebufferCreateInfo framebufferInfo = {};
			framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
			framebufferInfo.renderPass = renderPass;
			framebufferInfo.pAttachments = attachmentViews.data();
			framebufferInfo.attachmentCount = static_cast<uint32_t>(attachmentViews.size());
			framebufferInfo.width = width;
			framebufferInfo.height = height;
			framebufferInfo.layers = maxLayers;
			VK_VALIDATE_RESULT(vkCreateFramebuffer(gpu.device, &framebufferInfo, nullptr, &handle));
		}

		FramebufferAttachment& operator [] (uint32_t index) {
			return attachments[index];
		}

		std::vector<VkAttachmentDescription> getAttachmentDescriptions() {
			std::vector<VkAttachmentDescription> descriptions;

			for (FramebufferAttachment attachment : this->attachments) {
				descriptions.push_back(attachment.description);
			}

			return descriptions;
		}
	};
}

#endif