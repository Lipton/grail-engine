#include "Resources.h"
#include "MemoryRegion.h"
#include "Utils.h"
#include "SceneNode.h"

#include "TextureLoader.h"
#include "MeshLoader.h"
#include "ShaderLoader.h"

#include "MaterialInstance.h"
#include "MaterialParser.h"

#include "Shader.h"

#include "ScriptProvider.h"
#include "Script.h"

#include "MemoryAllocator.h"

#include "Asset.h"
#include "FileIO.h"

grail::MemoryAllocator* grail::Resources::stagingMemoryAllocator = nullptr;
grail::MemoryAllocator* grail::Resources::textureMemoryAllocator = nullptr;
grail::MemoryAllocator* grail::Resources::bufferMemoryAllocator = nullptr;
grail::MemoryAllocator* grail::Resources::uniformBufferMemoryAllocator = nullptr;

std::unordered_map<uint32_t, void*> grail::Resources::meshAssets = std::unordered_map<uint32_t, void*>();

std::unordered_map<uint32_t, grail::VkTexture*> grail::Resources::textures = std::unordered_map<uint32_t, grail::VkTexture*>();
std::unordered_map<uint32_t, grail::SceneNode*> grail::Resources::meshNodes = std::unordered_map<uint32_t, grail::SceneNode*>();
std::unordered_map<uint32_t, VkShaderModule> grail::Resources::shaders = std::unordered_map<uint32_t, VkShaderModule>();
std::unordered_map<uint32_t, grail::Mesh*> grail::Resources::meshes = std::unordered_map<uint32_t, grail::Mesh*>();
std::unordered_map<uint32_t, grail::Shader*> grail::Resources::nshaders = std::unordered_map<uint32_t, grail::Shader*>();
std::unordered_map<uint32_t, grail::MaterialDescription*> grail::Resources::materials = std::unordered_map<uint32_t, grail::MaterialDescription*>();
std::unordered_map<uint32_t, grail::Buffer*> grail::Resources::buffers = std::unordered_map<uint32_t, grail::Buffer*>();
std::unordered_map<uint32_t, grail::Script*> grail::Resources::scripts = std::unordered_map<uint32_t, grail::Script*>();

//std::unordered_map<grail::uuid, std::shared_ptr<grail::AssetBase>> grail::Resources::textureAssets = std::unordered_map<grail::uuid, std::shared_ptr<grail::AssetBase>>();

std::unordered_map<grail::uuid, grail::AssetRef<grail::Asset>> grail::Resources::assets = std::unordered_map<grail::uuid, grail::AssetRef<grail::Asset>>();
std::mutex grail::Resources::assetsLock = std::mutex();

grail::VkTexture * grail::Resources::getTexture(const std::string& identifier) {
	unsigned int id = GRAIL_SID(identifier);

	if (Resources::textures.find(id) != Resources::textures.end())
		return Resources::textures[id];

	GRAIL_LOG(ERROR, "RESOURCES") << "Texture: " << identifier << " was not found";

	return nullptr;
}

grail::Mesh * grail::Resources::getMesh(const std::string& identifier) {
	unsigned int id = GRAIL_SID(identifier);

	if (Resources::meshes.find(id) != Resources::meshes.end())
		return Resources::meshes[id];

	GRAIL_LOG(ERROR, "RESOURCES") << "Mesh: " << identifier << " was not found";

	return nullptr;
}

grail::VkTexture * grail::Resources::loadTexture(std::string path, TextureCreateInfo & createInfo) {
	unsigned int id = GRAIL_SID(path);

	if (Resources::textures.find(id) != Resources::textures.end())		
		return Resources::textures[id];

	GRAIL_LOG(INFO, "RESOURCES") << "Loading texture at: \"" + path + "\"";

	VkTexture* texture = TextureLoader::loadTexture(path, createInfo, stagingMemoryAllocator, textureMemoryAllocator);
	Resources::textures[id] = texture;

	return Resources::textures[id];
}

grail::VkTexture * grail::Resources::loadTexture(std::string path) {
	TextureCreateInfo info = {};
	return loadTexture(path, info);
}

grail::VkTexture* grail::Resources::loadTextureAsync(std::string path, TextureCreateInfo& createInfo) {
	unsigned int id = GRAIL_SID(path);

	if (Resources::textures.find(id) != Resources::textures.end())
		return Resources::textures[id];

	GRAIL_LOG(INFO, "RESOURCES") << "Loading texture at: \"" + path + "\"";

	VkTexture* texture = TextureLoader::loadTexture(path, createInfo, stagingMemoryAllocator, textureMemoryAllocator);

	//textureLock.lock();
	Resources::textures[id] = texture;
	//textureLock.unlock();

	return Resources::textures[id];
}

grail::VkTexture* grail::Resources::loadTextureAsync(std::string path) {
	TextureCreateInfo info = {};
	return loadTextureAsync(path, info);
}

grail::VkTexture * grail::Resources::createTexture(std::string name, uint32_t width, uint32_t height, TextureCreateInfo & createInfo) {
	unsigned int id = GRAIL_SID(name);

	if (Resources::textures.find(id) != Resources::textures.end())
		return Resources::textures[id];

	GRAIL_LOG(INFO, "RESOURCES") << "Creating texture: \"" + name + "\"";

	VkTexture* texture = TextureLoader::createTexture(name, width, height, createInfo, textureMemoryAllocator);
	Resources::textures[id] = texture;

	return Resources::textures[id];
}

grail::VkTexture * grail::Resources::createTexture(std::string name, uint32_t width, uint32_t height) {
	TextureCreateInfo info = {};
	return createTexture(name, width, height, info);
}

grail::VkTexture * grail::Resources::createTexture(std::string name, uint32_t width, uint32_t height, void * initialData, uint32_t size, TextureCreateInfo & createInfo) {
	unsigned int id = GRAIL_SID(name);

	if (Resources::textures.find(id) != Resources::textures.end())
		return Resources::textures[id];

	GRAIL_LOG(INFO, "RESOURCES") << "Creating texture: \"" + name + "\"";

	VkTexture* texture = TextureLoader::createTexture(name, width, height, initialData, size, createInfo, textureMemoryAllocator, stagingMemoryAllocator);
	Resources::textures[id] = texture;

	return Resources::textures[id];
}

grail::VkTexture * grail::Resources::createTexture(std::string name, uint32_t width, uint32_t height, void * initialData, uint32_t size) {
	TextureCreateInfo info = {};
	return createTexture(name, width, height, initialData, size, info);
}

void grail::Resources::loadModel(const std::string& path, VertexLayout* vertexLayout, bool insertInScene) {
	unsigned int id = GRAIL_SID(path);

	if (Resources::meshAssets.find(id) != Resources::meshAssets.end())
		return;

	GRAIL_LOG(INFO, "RESOURCES") << "Loading model at: \"" + path + "\"";

	MeshLoader::loadModel(path, vertexLayout, stagingMemoryAllocator, bufferMemoryAllocator, insertInScene);
	Resources::meshAssets[id] = nullptr;
}

grail::SceneNode * grail::Resources::loadMesh(std::string path, VertexLayout * vertexLayout, Renderer* renderer, bool loadMaterials, std::string customMaterialFile) {
	unsigned int id = GRAIL_SID(path);

	if (Resources::meshNodes.find(id) != Resources::meshNodes.end())
		return new SceneNode(Resources::meshNodes[id]);

	GRAIL_LOG(INFO, "RESOURCES") << "Loading mesh at: \"" + path + "\"";
	
	SceneNode* node = MeshLoader::loadMesh(path, vertexLayout, renderer, loadMaterials, customMaterialFile, stagingMemoryAllocator, bufferMemoryAllocator);
	Resources::meshNodes[id] = node;

	return node;
}

void grail::Resources::loadBaseMesh(std::string path, VertexLayout * vertexLayout, Renderer * renderer, bool loadMaterials, std::string customMaterialFile) {
	unsigned int id = GRAIL_SID(path);

	if (Resources::meshes.find(id) != Resources::meshes.end())
		return;

	GRAIL_LOG(INFO, "RESOURCES") << "Loading base model at: \"" + path + "\"";

	MeshLoader::loadBaseMesh(path, vertexLayout, renderer, loadMaterials, customMaterialFile, stagingMemoryAllocator, bufferMemoryAllocator);
}

grail::MaterialDescription* grail::Resources::loadMaterial(const std::string & name, const std::string & descriptorPath, FrameGraph* graph) {
	unsigned int id = GRAIL_SID(name);

	if (Resources::materials.find(id) != Resources::materials.end())
		return materials[id];

	GRAIL_LOG(INFO, "RESROUCES") << "Loading material: \"" + name + "\"";

	MaterialDescription* material = MaterialParser::parseMaterial(descriptorPath, graph);
	material->identifier = name;
	Resources::materials[id] = material;

	return materials[id];
}

grail::MaterialDescription* grail::Resources::getMaterial(const std::string & name) {
	unsigned int id = GRAIL_SID(name);

	if (Resources::materials.find(id) != Resources::materials.end())
		return materials[id];

	return nullptr;
}

/*grail::MaterialInstance* grail::Resources::createMaterial(const std::string& name, MaterialCreateInfo& createInfo) {
	unsigned int id = GRAIL_SID(name);

	if (Resources::materials.find(id) != Resources::materials.end())
		return new MaterialInstance(Resources::materials[id]);

	GRAIL_LOG(INFO, "RESROUCES") << "Loading material: \"" + name + "\"";

	Material* material = new Material(createInfo);
	material->identifier = name;
	Resources::materials[id] = material;

	return new MaterialInstance(material);
}

grail::MaterialInstance* grail::Resources::getMaterial(const std::string& name) {
	unsigned int id = GRAIL_SID(name);

	if (Resources::materials.find(id) != Resources::materials.end())
		return new MaterialInstance(Resources::materials[id]);

	return nullptr;
}*/

grail::Buffer* grail::Resources::createBuffer(const std::string& name, BufferCreateInfo info, MemoryAllocator* memoryRegion) {
	unsigned int id = GRAIL_SID(name);

	if (Resources::buffers.find(id) != Resources::buffers.end())
		return Resources::buffers[id];

	Buffer* buffer = new Buffer(info, memoryRegion);
	buffer->identifier = name;
	Resources::buffers[id] = buffer;

	return Resources::buffers[id];
}

grail::Buffer* grail::Resources::getBuffer(const std::string& name) {
	unsigned int id = GRAIL_SID(name);

	if (Resources::buffers.find(id) != Resources::buffers.end())
		return Resources::buffers[id];

	return nullptr;
}

grail::Mesh * grail::Resources::createMesh(std::string name, VertexLayout * vertexLayout, std::vector<float>& vertices, std::vector<uint32_t>& indices) {
	unsigned int id = GRAIL_SID(name);

	if (Resources::meshes.find(id) != Resources::meshes.end())
		return Resources::meshes[id];

	GRAIL_LOG(INFO, "RESOURCES") << "Creating mesh: \"" + name + "\"";

	Mesh* mesh = MeshLoader::createMesh(name, vertexLayout, vertices, indices, stagingMemoryAllocator, bufferMemoryAllocator);
	Resources::meshes[id] = mesh;

	return Resources::meshes[id];
}

grail::Mesh * grail::Resources::createMesh(std::string name, VertexLayout * vertexLayout, std::vector<float>& vertices) {
	std::vector<uint32_t> indices;
	return createMesh(name, vertexLayout, vertices, indices);
}

grail::Script* grail::Resources::createScript(const std::string& name) {
	unsigned int id = GRAIL_SID(name);

	if (Resources::scripts.find(id) != Resources::scripts.end())
		return Resources::scripts[id];

	GRAIL_LOG(INFO, "RESOURCES") << "Creating script: \"" + name + "\"";

	Script* script = ScriptProvider::createScript(name);
	Resources::scripts[id] = script;

	return Resources::scripts[id];
}

grail::Script* grail::Resources::createScript(const std::string& name, const std::string& source) {
	unsigned int id = GRAIL_SID(name);

	if (Resources::scripts.find(id) != Resources::scripts.end())
		return Resources::scripts[id];

	GRAIL_LOG(INFO, "RESOURCES") << "Creating script: \"" + name + "\"";

	Script* script = ScriptProvider::createScript(name, source);
	Resources::scripts[id] = script;

	return Resources::scripts[id];
}

grail::Script* grail::Resources::getScript(const std::string& name) {
	unsigned int id = GRAIL_SID(name);

	if (Resources::scripts.find(id) != Resources::scripts.end())
		return Resources::scripts[id];

	return nullptr;
}

VkShaderModule grail::Resources::loadShader(std::string path) {
	unsigned int id = GRAIL_SID(path);

	if (Resources::shaders.find(id) != Resources::shaders.end())
		return Resources::shaders[id];

	GRAIL_LOG(INFO, "RESOURCES") << "Loading SPIRV shader at: \"" + path + "\"";

	VkShaderModule module = ShaderLoader::loadShader(path);

	Resources::shaders[id] = module;

	return Resources::shaders[id];
}

grail::Shader* grail::Resources::loadSPIRVShader(std::string path, std::vector<std::string>& defines) {
	unsigned int id = GRAIL_SID(path);

	/*if (Resources::nshaders.find(id) != Resources::nshaders.end())
		return Resources::nshaders[id];*/

	GRAIL_LOG(INFO, "RESOURCES") << "Loading SPIRV shader at: \"" + path + "\"";

	Shader* module = ShaderLoader::loadSPIRVShader(path, defines);

	Resources::nshaders[id] = module;

	return Resources::nshaders[id];
}

grail::Shader * grail::Resources::loadSPIRVShader(std::string path) {
	std::vector<std::string> defines;
	return loadSPIRVShader(path, defines);
}

void grail::Resources::setStagingMemoryAllocator(MemoryAllocator* allocator) {
	Resources::stagingMemoryAllocator = allocator;
}

void grail::Resources::setTextureMemoryAllocator(MemoryAllocator* allocator) {
	Resources::textureMemoryAllocator = allocator;
}

void grail::Resources::setBufferMemoryAllocator(MemoryAllocator* allocator) {
	Resources::bufferMemoryAllocator = allocator;
}

void grail::Resources::setUniformBufferMemoryAllocator(MemoryAllocator* allocator) {
	Resources::uniformBufferMemoryAllocator = allocator;
}

grail::MemoryAllocator* grail::Resources::getStagingMemoryAllocator() {
	return stagingMemoryAllocator;
}

grail::MemoryAllocator* grail::Resources::getTextureMemoryAllocator() {
	return textureMemoryAllocator;
}

grail::MemoryAllocator* grail::Resources::getBufferMemoryAllocator() {
	return bufferMemoryAllocator;
}

grail::MemoryAllocator* grail::Resources::getUniformBufferMemoryAllocator() {
	return uniformBufferMemoryAllocator;
}

grail::AssetRef<grail::Asset> grail::Resources::loadAsset(const std::string& path) {
	return AssetRef<Asset>();
}

grail::AssetRef<grail::TextureAsset> grail::Resources::loadTextureAsset(TextureAssetDescriptor* descriptor) {
	if (assets.find(descriptor->uuid) != assets.end()) {
		return std::dynamic_pointer_cast<grail::TextureAsset>(assets[descriptor->uuid]);
	}

	TextureCreateInfo createInfo = {};
	createInfo.samplerInfo.minFilter = descriptor->minFilter;
	createInfo.samplerInfo.magFilter = descriptor->magFilter;

	createInfo.samplerInfo.mipmapMode = descriptor->mipmapMode;

	createInfo.samplerInfo.addressModeU = descriptor->addressModeU;
	createInfo.samplerInfo.addressModeV = descriptor->addressModeV;
	createInfo.samplerInfo.addressModeW = descriptor->addressModeW;

	createInfo.generateMipmaps = descriptor->generateMipmaps;
	createInfo.samplerInfo.maxAnisotropy = descriptor->maxAnisotropy;

	VkTexture* texture = TextureLoader::loadTexture(descriptor->assetPath, createInfo);

	auto asset = std::make_shared<TextureAsset>();
	asset->handle = texture;
	asset->name = FileIO::getFilename(descriptor->assetPath) + "." + FileIO::getExtension(descriptor->assetPath);
	asset->idString = descriptor->uuid.str();
	asset->id = descriptor->uuid;
	asset->path = descriptor->assetPath;
	asset->extension = descriptor->assetExtension;
	asset->descriptor = descriptor;

	assets.insert({ descriptor->uuid, asset });

	return std::dynamic_pointer_cast<grail::TextureAsset>(assets[descriptor->uuid]);
	//return AssetRef<grail::TextureAsset>();
}

void grail::Resources::unloadAsset(const AssetRef<Asset>& asset) {
	asset->dispose();

	uuid id = asset->getAssetUUID();
	GRAIL_LOG(INFO, "UNLOADING") << id.str();

	//delete assets[id].get();
	assets.erase(id);
}

bool grail::Resources::parseAsset(AssetRef<Asset>& asset, const std::string& path) {
	if (FileIO::fileExists(path)) {
		asset->path = path;
		asset->name = FileIO::getFilename(path);
		asset->directory = FileIO::getBasePath(path);
		asset->extension = FileIO::getExtension(path);

		return true;
	}

	return false;
}

//grail::AssetRef<grail::TextureAsset> grail::Resources::loadTextureAsset(TextureAssetDescriptor& descriptor, const std::string& basePath) {
/*	if (textureAssets.find(descriptor.id) != textureAssets.end()) {
		return std::make_shared<grail::TextureAsset>(textureAssets[descriptor.id]);
	}

	TextureCreateInfo createInfo = {};
	createInfo.samplerInfo.minFilter = descriptor.minFilter;
	createInfo.samplerInfo.magFilter = descriptor.magFilter;

	createInfo.samplerInfo.mipmapMode = descriptor.mipmapMode;

	createInfo.samplerInfo.addressModeU = descriptor.addressModeU;
	createInfo.samplerInfo.addressModeV = descriptor.addressModeV;
	createInfo.samplerInfo.addressModeW = descriptor.addressModeW;
	
	createInfo.generateMipmaps = descriptor.generateMipmaps;
	createInfo.samplerInfo.maxAnisotropy = descriptor.maxAnisotropy;

	VkTexture* texture = TextureLoader::loadTexture(basePath + descriptor.filePath, createInfo);

	auto asset = std::make_shared<TextureAsset>();
	asset->handle = texture;
	asset->name = descriptor.filePath;
	asset->idString = descriptor.id.str();
	asset->id = descriptor.id;
	asset->path = basePath + descriptor.filePath;

	textureAssets.insert({ descriptor.id, asset });

	return std::make_shared<grail::TextureAsset>(textureAssets[descriptor.id]);*/
//}

//void grail::Resources::unloadAsset(AssetRef<TextureAsset> asset) {

//}

