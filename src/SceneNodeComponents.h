#ifndef GRAIL_SCENE_NODE_COMPONENTS_H
#define GRAIL_SCENE_NODE_COMPONENTS_H

#include <map>
#include <vector>
#include <string>

#include "entt.hpp"
#include "json.hpp"

#include "ScriptProvider.h"
#include "Physics.h"

#include "Script.h"

#define REGISTER_COMPONENT_ASSIGN(Identifier, Component) {Identifier, [](entt::registry & registry, entt::entity & entity) { registry.assign<Component>(entity); } }
#define REGISTER_COMPONENT_RETRIEVE(Identifier, Component) {Identifier, [](entt::registry & registry, entt::entity & entity) { return registry.try_get<Component>(entity); } }

class btCollisionShape;
class btRigidBody;

class asIScriptContext;
class asIScriptObject;
class asIScriptFunction;
class CScriptHandle;

namespace grail {
	class Mesh;
	class Material;
	class MaterialInstance;
	class SceneNode;

	namespace nodeComponents {
		class NodeComponent {
			friend class Scene;
			friend class SceneLoader;
		public:
			SceneNode* node;

			virtual void save(nlohmann::json& node) = 0;
			virtual void load(nlohmann::json& node) = 0;
			virtual void copy(NodeComponent* src) = 0;
			virtual void dispose() = 0;
		};

		class Render : public NodeComponent {
		public:
			Render();

			Mesh* mesh = nullptr;
			MaterialInstance* material = nullptr;
			bool render = true;

			// Temporary function to initialize a default material and mesh
			void initDefault();
		protected:
			void save(nlohmann::json& node);
			void load(nlohmann::json& node);
			void copy(NodeComponent* src);
			void dispose();
		private:
			std::string name;
		};

		class ScriptComponent : public NodeComponent {
			friend class ScriptProvider;
			friend class Editor;
			friend class SceneLoader;
		public:
			// sets the current script of the component and instanciates the class instance if it exists
			void setScript(Script* script);
			Script* getScript();

			void callUpdateFunc(float delta);
			void callStartFunc();

			//void setup();

			// Used by the script to get the class instance
			CScriptHandle get();
		protected:
			void save(nlohmann::json& node);
			void load(nlohmann::json& node);
			void copy(NodeComponent* src);
			void dispose();
		private:
			::asIScriptContext* context = nullptr;
			::asIScriptObject* classInstance = nullptr;
			::asIScriptFunction* startFunction = nullptr;
			::asIScriptFunction* updateFunction = nullptr;

			Script* script = nullptr;
        public:
			bool instanciated = false;
        public:
			std::vector<SerializedScriptVar> serializedVars;
			std::vector<SerializedScriptVar> serializedVals;
        private:
			bool canCallFunc();

			void cleanup();

			void serializeScriptVar(SerializedScriptVar& var, nlohmann::json& node);
			void deserializeScriptVar(const std::string& varName, nlohmann::json& node);

			// Instanciate the class instance if needed and gather serialiazable variables
        public:
			void instanciate();
		};

		class PhysicsComponent : public NodeComponent {
		public:
			/*btCollisionShape* shape = nullptr;
			btRigidBody* body = nullptr;

			float getFriction();
			void setFriction(float friction);

			float getMass();
			void setMass(float mass);

			float getRestitution();
			void setRestitution(float restitution);

			float getRollingFriction();
			void setRollingFriction(float friction);

			glm::vec3 getAngularFactor();
			void setAngularFactor(glm::vec3 factor);
			void setAngularFactor(float factor);

			glm::vec3 getLinearFactor();
			void setLinearFactor(glm::vec3 factor);
			void setLinearFactor(float factor);

			// Sync transform with node
			void syncTransform(bool clearForces = true);

			void applyCentralForce(glm::vec3 force);

			void setShape(btCollisionShape* newShape);
			void createCapsuleShape(float radius = 0, float height = 0);
			void createConvexHullShape();*/
		protected:
			//PhysicsShapeType shapeType = PhysicsShapeType::BOX;

			void save(nlohmann::json& node);
			void load(nlohmann::json& node);
			void copy(NodeComponent* src);
			void dispose();
		private:
		};

		namespace detail {
			extern std::map<std::string, std::function<void(entt::registry&, entt::entity&)>> componentAssignMap;
			extern std::map<std::string, std::function<NodeComponent*(entt::registry&, entt::entity&)>> componentRetrieveMap;
		}

		void createComponentFor(const std::string& componentName, entt::entity& entity, entt::registry& registry);
		NodeComponent* retrieveComponent(const std::string& componentName, entt::entity& entity, entt::registry& registry);
	}
}

#endif
