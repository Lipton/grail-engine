#ifndef GRAIL_UUID_H
#define GRAIL_UUID_H

#include <sole.hpp>

namespace grail {
	using uuid = sole::uuid;
	
	namespace uuid_utils {
		uuid v4();
	}
}

#endif