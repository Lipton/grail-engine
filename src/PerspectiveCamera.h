#ifndef GRAIL_PERSPECTIVE_CAMERA_H
#define GRAIL_PERSPECTIVE_CAMERA_H

#include "Camera.h"

namespace grail {
	class PerspectiveCamera : public Camera {
	public:
		float fov;

		PerspectiveCamera();
		PerspectiveCamera(float fov, float viewportWidth, float viewportHeight);

		void update(bool updateFrustum = false);
	};
}

#endif