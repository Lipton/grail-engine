#include "VertexLayout.h"

grail::VertexLayout::VertexLayout(std::initializer_list<VertexAttribute> attributes) {
	for (VertexAttribute attrib : attributes) {
		layout.push_back(attrib);
	}

	uint32_t offset = 0;
	elementCount = 0;
	for (uint32_t i = 0; i < this->layout.size(); i++) {
		this->layout[i].setBinding(i);
		this->layout[i].setOffset(offset);

		offset += this->layout[i].getSize();
		elementCount += this->layout[i].getElementCount();
	}

	totalSize = offset;

	bindingDescription = {};
	bindingDescription.binding = 0;
	bindingDescription.stride = totalSize;
	bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

	attributeDescriptions.clear();
	for (uint32_t i = 0; i < layout.size(); i++) {
		VertexAttribute& attrib = layout[i];

		VkVertexInputAttributeDescription description = {};
		description.binding = 0;
		description.location = attrib.getBinding();
		description.format = attrib.getFormat();
		description.offset = attrib.getOffset();

		attributeDescriptions.push_back(description);
	}

	vertexInputState = {};
	vertexInputState.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	vertexInputState.pNext = nullptr;
	vertexInputState.flags = 0;
	vertexInputState.vertexBindingDescriptionCount = 1;
	vertexInputState.pVertexBindingDescriptions = &bindingDescription;
	vertexInputState.vertexAttributeDescriptionCount = (uint32_t)attributeDescriptions.size();
	vertexInputState.pVertexAttributeDescriptions = &attributeDescriptions[0];
}

grail::VertexLayout::VertexLayout(std::vector<VertexAttribute> attributes) {
	for (VertexAttribute attrib : attributes) {
		layout.push_back(attrib);
	}

	uint32_t offset = 0;
	elementCount = 0;
	for (uint32_t i = 0; i < this->layout.size(); i++) {
		this->layout[i].setBinding(i);
		this->layout[i].setOffset(offset);

		offset += this->layout[i].getSize();
		elementCount += this->layout[i].getElementCount();
	}

	totalSize = offset;

	bindingDescription = {};
	bindingDescription.binding = 0;
	bindingDescription.stride = totalSize;
	bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

	attributeDescriptions.clear();
	for (uint32_t i = 0; i < layout.size(); i++) {
		VertexAttribute& attrib = layout[i];

		VkVertexInputAttributeDescription description = {};
		description.binding = 0;
		description.location = attrib.getBinding();
		description.format = attrib.getFormat();
		description.offset = attrib.getOffset();

		attributeDescriptions.push_back(description);
	}

	vertexInputState = {};
	vertexInputState.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	vertexInputState.pNext = nullptr;
	vertexInputState.flags = 0;
	vertexInputState.vertexBindingDescriptionCount = 1;
	vertexInputState.pVertexBindingDescriptions = &bindingDescription;
	vertexInputState.vertexAttributeDescriptionCount = (uint32_t)attributeDescriptions.size();
	vertexInputState.pVertexAttributeDescriptions = &attributeDescriptions[0];
}

grail::VertexLayout::VertexLayout() {
}

grail::VertexAttribute & grail::VertexLayout::getVertexAttribute(uint32_t index) {
	return layout[index];
}

uint32_t grail::VertexLayout::getVertexSize() {
	return totalSize;
}

uint32_t grail::VertexLayout::getVertexElementCount() {
	return elementCount;
}

uint32_t grail::VertexLayout::getVertexAttributeCount() {
	return (uint32_t)layout.size();
}

bool grail::VertexLayout::hasAttributeType(VertexAttributeType type) {
	for (uint32_t i = 0; i < layout.size(); i++) {
		VertexAttribute& attrib = layout[i];
	
		if (attrib.getType() == type)
			return true;
	}

	return false;
}

VkPipelineVertexInputStateCreateInfo grail::VertexLayout::getVertexInputState() {
	return vertexInputState;
}

grail::VertexAttribute::VertexAttribute(VertexAttributeType type, VkFormat format, uint32_t elementCount, uint32_t elementSize, bool packed) {
	this->type = type;
	this->format = format;
	this->elementCount = elementCount;
	this->packed = packed;
	this->elementSize = elementSize;
}

grail::VertexAttribute::VertexAttribute(VertexAttributeType type, VkFormat format, uint32_t elementCount, uint32_t elementSize) {
	this->type = type;
	this->format = format;
	this->elementCount = elementCount;
	this->packed = false;
	this->elementSize = elementSize;
}

grail::VertexAttribute::VertexAttribute(VkFormat format, uint32_t elementCount, uint32_t elementSize, bool packed) {
	this->type = VertexAttributeType::GENERIC;
	this->format = format;
	this->elementCount = elementCount;
	this->packed = packed;
	this->elementSize = elementSize;
}

grail::VertexAttribute::VertexAttribute(VkFormat format, uint32_t elementCount, uint32_t elementSize) {
	this->type = VertexAttributeType::GENERIC;
	this->format = format;
	this->elementCount = elementCount;
	this->packed = false;
	this->elementSize = elementSize;
}

grail::VertexAttributeType grail::VertexAttribute::getType() {
	return type;
}

VkFormat grail::VertexAttribute::getFormat() {
	return format;
}

bool grail::VertexAttribute::isPacked() {
	return packed;
}

uint32_t grail::VertexAttribute::getElementCount() {
	return elementCount;
}

uint32_t grail::VertexAttribute::getElementSize() {
	return elementSize;
}

uint32_t grail::VertexAttribute::getSize() {
	return (isPacked() ? getElementSize() : getElementSize() * getElementCount());
}

uint32_t grail::VertexAttribute::getOffset() {
	return offset;
}

void grail::VertexAttribute::setOffset(uint32_t offset) {
	this->offset = offset;
}

uint32_t grail::VertexAttribute::getBinding() {
	return binding;
}

void grail::VertexAttribute::setBinding(uint32_t binding) {
	this->binding = binding;
}
