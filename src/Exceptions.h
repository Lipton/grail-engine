#ifndef GRAIL_EXCEPTIONS_H
#define GRAIL_EXCEPTIONS_H

#include <stdexcept>
#include "Logger.h"

namespace grail {
	namespace exceptions {
		class FileNotFoundException : public std::runtime_error {
		public:
			FileNotFoundException() : runtime_error("File not found") {
				GRAIL_LOG(ERROR, "EXCEPTION") << "File not found!";
			};

			FileNotFoundException(const std::string& msg) : runtime_error(msg.c_str()) {
				GRAIL_LOG(ERROR, "EXCEPTION") << msg;
			};
		};

		class OutOfMemoryException : public std::runtime_error {
		public:
			OutOfMemoryException() : runtime_error("Out of memory") {
				GRAIL_LOG(ERROR, "EXCEPTION") << "Out of memory!";
			};

			OutOfMemoryException(const std::string& msg) : runtime_error(msg.c_str()) {
				GRAIL_LOG(ERROR, "EXCEPTION") << msg;
			};
		};

		class RuntimeException : public std::runtime_error {
		public:
			RuntimeException() : runtime_error("Runtime exception") {
				GRAIL_LOG(ERROR, "EXCEPTION") << "Runtime exception!";
			};

			RuntimeException(const std::string& msg) : runtime_error(msg.c_str()) {
				GRAIL_LOG(ERROR, "EXCEPTION") << msg;
			};
		};
	}
}

#endif