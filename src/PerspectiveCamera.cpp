#include "PerspectiveCamera.h"

grail::PerspectiveCamera::PerspectiveCamera() : Camera() {
	this->fov = 64;
	this->viewportWidth = (float)Graphics::getWidth();
	this->viewportHeight = (float)Graphics::getHeight();
	update(true);
}

grail::PerspectiveCamera::PerspectiveCamera(float fov, float viewportWidth, float viewportHeight) : Camera() {
	this->fov = fov;
	this->viewportWidth = viewportWidth;
	this->viewportHeight = viewportHeight;
	update(true);
}

glm::mat4 MakeInfReversedZProjRH(float fovY_radians, float aspectWbyH, float zNear)
{
	float f = 1.0f / tan(fovY_radians / 2.0f);
	return glm::mat4(
		f / aspectWbyH, 0.0f, 0.0f, 0.0f,
		0.0f, f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, -1.0f,
		0.0f, 0.0f, zNear, 0.0f);
}

void grail::PerspectiveCamera::update(bool updateFrustum) {
	if (farPlane > 0.0f) {
		projection = glm::perspectiveFov(glm::radians(fov), viewportWidth, viewportHeight, nearPlane, farPlane);
	}
	else {
		float f = 1.0f / tan(glm::radians(fov) / 2.0f);
		projection =  glm::mat4(
			f / (viewportWidth / viewportHeight), 0.0f, 0.0f, 0.0f,
			0.0f, f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.0f, -1.0f,
			0.0f, 0.0f, nearPlane, 0.0f);
	}

	view = glm::lookAt(position, position + direction, up);

	combined = projection * view;

	if (updateFrustum)
		frustum.update((combined));
}
