#include "GrailEditor.h"

#include <json.hpp>

#include "DeferredRenderer.h"
#include "PerspectiveCamera.h"
#include "FirstPersonCameraController.h"
#include "Scene.h"
#include "FrameGraph.h"
#include "FileIO.h"
#include "Asset.h"
#include "TextureLoader.h"
#include "TextureAssetImporter.h"
#include "Material.h"

grail::GrailEditor::GrailEditor() {
	
}

void grail::GrailEditor::init() {
	importers.texture = new TextureAssetImporter();

	camera = new PerspectiveCamera(90, Graphics::getWidth(), Graphics::getHeight());
	camera->nearPlane = 0.1f;
	camera->farPlane = 256.0f;
	cameraController = new FirstPersonCameraController(camera);

	getInputProcessor().setOnKeyPress([this](int keycode, int action) {
		imguiHelper->handleKeyPress(keycode, action);

		if (action == GRAIL_KEY_RELEASE) {
			if (!UI::GetIO().WantTextInput) {
				if (!ImGuizmo::IsUsing()) {
					if (keycode == (Keys::R)) {
						gizmo.op = ImGuizmo::OPERATION::ROTATE;
					}

					if (keycode == (Keys::T)) {
						gizmo.op = ImGuizmo::OPERATION::TRANSLATE;
					}

					if (keycode == (Keys::V)) {
						gizmo.op = ImGuizmo::OPERATION::SCALE;
					}

					if (keycode == (Keys::B)) {
						gizmo.op = ImGuizmo::OPERATION::BOUNDS;
					}

					if (keycode == (Keys::C)) {
						gizmo.op = ImGuizmo::OPERATION::DISABLED;
					}
				}
			}
		}
	});

	getInputProcessor().setOnTypedChar([this](unsigned int c, int mods) {
		imguiHelper->handleKeyTyped(c, mods);
	});

	getInputProcessor().setOnMouseMoved([this](float x, float y) {
		if(Input::isMouseCaptured())
			cameraController->updateMouseMoved(x, y);
	});

	getInputProcessor().setOnDroppedFiles([this](int count, const char** paths) {
		for (int i = 0; i < count; i++) {
			std::string path = paths[i];
			utils::string::replaceAll(path, "\\", "/");

			PathType type = FileIO::getPathType(path);
			
			if (type == PathType::FILE)
				handleFileImport(path);
			else if (type == PathType::DIRECTORY) {
				GRAIL_LOG(WARNING, "ASSET IMPORT") << "Folders currently not supported";
			}
		}

		rescanAssets(projectDirectory);
	});

	renderer = new DeferredRenderer(camera);
	imguiHelper = new ImGuiHelper();

	const VkRenderPass& presentPass = renderer->getFrameGraph()->getPass("Present")->getRenderPass();
	imguiHelper->initialize(presentPass, projectDirectory + "ui.ini");
	renderer->setPostRenderFunction([&](RenderPass* pass, VkCommandBuffer& cmd) {
		imguiHelper->render(cmd, [&] {
			renderUI();
		});
	});

	editor::loadIcons();

	structureViewer.currentDir = projectDirectory;
	rescanAssets(structureViewer.currentDir);
	//openPopup("Select Project##grail_editor");
}

void grail::GrailEditor::render(float delta) {
	camera->update(true);
	cameraController->update(delta);

	Scene::update(delta);
	renderer->render(delta);
}

void grail::GrailEditor::rescanAssets(const std::string& path) {
	std::vector<PathInfo> contents;
	FileIO::getDirectoryContents(path, contents);

	for (PathInfo& item : contents) {
		if (item.type == PathType::DIRECTORY) {
			if(item.path.compare("..") != 0)
				rescanAssets(path + item.path + "/");
		}
		else if (item.type == PathType::FILE) {
			std::string extension = FileIO::getExtension(item.path);

			if (extension.compare("meta") != 0) {
				AssetImporter* importer = getImporterByExtension(extension);

				if (importer)
					importer->importAsset(path + item.path);
				else
					GRAIL_LOG(INFO, "IMPORT ERROR") << "Failed to find importer for file: " << path + item.path;
			}
		}
	}
}

void grail::GrailEditor::handleFileImport(const std::string& absolutePath) {
	std::string filename = FileIO::getFilename(absolutePath);
	std::string extension = FileIO::getExtension(absolutePath);

	std::string finalName = filename + "." + extension;

	if (std::find(assets::textureFormats.begin(), assets::textureFormats.end(), extension) != assets::textureFormats.end()) {
		std::string destination = projectDirectory + "Assets/Textures/" + finalName;
		
		if (FileIO::copyFile(absolutePath, destination)) {
//			importers.texture->importAsset(destination);
		}
	}

	else if (std::find(assets::meshFormats.begin(), assets::meshFormats.end(), extension) != assets::meshFormats.end()) {
		std::string destination = projectDirectory + "Assets/Models/" + finalName;
		FileIO::copyFile(absolutePath, destination);
	}

	else if (std::find(assets::scriptFormats.begin(), assets::scriptFormats.end(), extension) != assets::scriptFormats.end()) {
		std::string destination = projectDirectory + "Assets/Scripts/" + finalName;
		FileIO::copyFile(absolutePath, destination);
	}

	else {
		//GRAIL_LOG(INFO, "UNKNOWN FILE") << absolutePath;
		//FileIO::copyFile(absolutePath, projectDirectory + "Assets/" + filename + "." + extension);
	}
}

struct PtrTest {
	std::string value = "default";

	PtrTest() {
		GRAIL_LOG(INFO, "CONSTRUCTING PTR TEST");
	}

	~PtrTest() {
		GRAIL_LOG(INFO, "DESTRUCTING PTR TEST") << value;
	}
};

template <class T>
class Aref {
public:
	std::shared_ptr<std::unique_ptr<T>> ptr;

	T& operator()() {
		return *(*ptr).get();
	}

	bool isUnique() {
		return ptr.unique();
	}

	size_t count() {
		return ptr.use_count();
	}
};

template <typename T>
using Ref = std::shared_ptr<std::unique_ptr<T>>;



void grail::GrailEditor::renderUI() {
	createDockspace();

	UI::Begin("Pointer Test");

	if(UI::Button("Test Single Shared Ptr")) {
		GRAIL_LOG(INFO, "SINGLE TEST") << "-----------------------------";
		PtrTest* obj = new PtrTest();
		obj->value = "Object one";

		PtrTest* obj2 = new PtrTest();
		obj2->value = "Object two";
		
		std::shared_ptr<PtrTest> base = std::make_shared<PtrTest>(*obj);

		std::shared_ptr<PtrTest> a = base;
		std::shared_ptr<PtrTest> b = base;

		GRAIL_LOG(INFO, "base val") << base->value;
		GRAIL_LOG(INFO, "a val") << a->value;
		GRAIL_LOG(INFO, "b val") << b->value;

		GRAIL_LOG(INFO, "--");

		base = std::make_shared<PtrTest>(*obj2);

		GRAIL_LOG(INFO, "base val") << base->value;
		GRAIL_LOG(INFO, "a val") << a->value;
		GRAIL_LOG(INFO, "b val") << b->value;

		GRAIL_LOG(INFO, "SINGLE TEST") << "-----------------------------\n";
	}

	if (UI::Button("Test Multiple Shared Ptr")) {
		GRAIL_LOG(INFO, "MULTIPLE TEST") << "-----------------------------";
		PtrTest* obj = new PtrTest();
		obj->value = "Object one";

		PtrTest* obj2 = new PtrTest();
		obj2->value = "Object two";

		Ref<PtrTest> base = std::make_shared<std::unique_ptr<PtrTest>>(obj);

		Ref<PtrTest> a = base;
		Ref<PtrTest> b = base;

		GRAIL_LOG(INFO, "base val") << base->get()->value;
		GRAIL_LOG(INFO, "a val") << (*a)->value;
		GRAIL_LOG(INFO, "b val") << b->get()->value;

		GRAIL_LOG(INFO, "--");

		base->reset(obj2);

		GRAIL_LOG(INFO, "base val") << base->get()->value;
		GRAIL_LOG(INFO, "a val") << (*a)->value;
		GRAIL_LOG(INFO, "b val") << b->get()->value;

		GRAIL_LOG(INFO, "MULTIPLE TEST") << "-----------------------------\n";
	}

	if (UI::Button("Test Custom Ref")) {
		GRAIL_LOG(INFO, "CUSTOM TEST") << "-----------------------------";
		PtrTest* obj = new PtrTest();
		obj->value = "Object one";

		PtrTest* obj2 = new PtrTest();
		obj2->value = "Object two";

		Aref<PtrTest> base;
		base.ptr = std::make_shared<std::unique_ptr<PtrTest>>(obj);

		GRAIL_LOG(INFO, "CUSTOM TEST") << "Is Unique: " << (base.isUnique() ? "True" : "False");
		GRAIL_LOG(INFO, "CUSTOM TEST") << "Is Unique: " << base.count();

		Aref<PtrTest> a = base;

		GRAIL_LOG(INFO, "CUSTOM TEST") << "Is Unique: " << (base.isUnique() ? "True" : "False");
		GRAIL_LOG(INFO, "CUSTOM TEST") << "Is Unique: " << base.count();
		Aref<PtrTest> b = base;

		GRAIL_LOG(INFO, "CUSTOM TEST") << "Is Unique: " << base.count();

		GRAIL_LOG(INFO, "base val") << base().value;
		GRAIL_LOG(INFO, "a val") << a().value;
		GRAIL_LOG(INFO, "b val") << b().value;

		GRAIL_LOG(INFO, "---------");

		base.ptr->reset(obj2);

		GRAIL_LOG(INFO, "CUSTOM TEST") << "Is Unique: " << (base.isUnique() ? "True" : "False");
		GRAIL_LOG(INFO, "CUSTOM TEST") << "Is Unique: " << base.count();

		GRAIL_LOG(INFO, "base val") << base().value;
		GRAIL_LOG(INFO, "a val") << a().value;
		GRAIL_LOG(INFO, "b val") << b().value;

		GRAIL_LOG(INFO, "CUSTOM TEST") << "-----------------------------\n";
	}
	
	UI::End();

	UI::Begin("Output##grail_editor_output");
	float w = renderer->fxaaPassOuput->getWidth();
	float h = renderer->fxaaPassOuput->getHeight();

	float h1 = UI::GetWindowWidth() * (h / w);
	float w2 = UI::GetWindowHeight() * (w / h);

	ImVec2 size;

	if (h1 <= UI::GetWindowHeight()) {
		size.x = UI::GetWindowWidth();
		size.y = h1;
	}
	else {
		size.x = w2;
		size.y = UI::GetWindowHeight();
	}

	size.y -= 32;

	UI::Image(imguiHelper->imageID(renderer->fxaaPassOuput), size);
	UI::End();

	for (const std::string& s : openPopups)
		UI::OpenPopup(s.c_str());

	UI::BeginMainMenuBar();
	UI::TextColored(ImVec4(0.7, 0.7, 0.7, 1.0), "%s", ("Grail Engine - " + projectDirectory).c_str());
	UI::Separator();
	if (UI::BeginMenu("Project##grail_editor_main_menu")) {
		UI::EndMenu();
	}
	if (UI::BeginMenu("Editor##grail_editor_main_menu")) {
		if (UI::MenuItem("Save Layout##grail_editor_main_menu")) {
			saveEditorLayout();
		}
		UI::EndMenu();
	}

	UI::EndMainMenuBar();

	renderSceneHierarchy();
	renderStructureViewer();
	renderTransformGizmo();
	renderInspector();

	if (UI::BeginPopupModal("Select Project##grail_editor")) {
		if (UI::Button("Close##Project_Selection")) {
			endPopup("Select Project##grail_editor");
		}
		UI::EndPopup();
	}
}

void grail::GrailEditor::openPopup(const std::string& id) {
	if(std::find(openPopups.begin(), openPopups.end(), id) == openPopups.end())
		openPopups.push_back(id);
}

bool grail::GrailEditor::endPopup(const std::string& id) {
	auto ref = std::find(openPopups.begin(), openPopups.end(), id);

	if (ref != openPopups.end()) {
		openPopups.erase(std::find(openPopups.begin(), openPopups.end(), id));
		UI::CloseCurrentPopup();
	}
	return true;
}

void grail::GrailEditor::drawUIImage(const std::string& id, glm::vec2 size) {
	editor::Icon& icon = editor::getIcon(id);
	UI::Image(imguiHelper->imageID(editor::iconAtlas), ImVec2(size.x, size.y), icon.uv0, icon.uv1);
}

void grail::GrailEditor::drawTexture(const AssetRef<TextureAsset>& texture, glm::vec2 size) {
	UI::Image(imguiHelper->imageID(&texture->getHandle()), ImVec2(size.x, size.y));
}

void grail::GrailEditor::renderStructureViewer() {
	std::chrono::time_point start = std::chrono::high_resolution_clock::now();

	float innerPadding = structureViewer.innerPadding * UI::scale;
	float itemSize = structureViewer.itemSize * UI::scale;

	UI::SetNextWindowSize(ImVec2(800 * UI::scale, 350 * UI::scale), ImGuiCond_FirstUseEver);
	UI::Begin("Project Explorer##grail_editor");

	float windowWidth = UI::GetWindowWidth();
	int columns = (int)(windowWidth / (itemSize + (innerPadding * 4)));
	columns = columns < 1 ? 1 : columns;

	UI::Text("Path: %s", structureViewer.currentDir.c_str());
	UI::SameLine();
	UI::Text("Render Time: %f", structureViewer.renderTime);
	if (UI::Button("Rescan Assets")) {
		rescanAssets(projectDirectory);
	}
	UI::BeginChild("project_explorer_grail_editor");
	UI::Columns(columns, "project_explorer_grail_editor_columns", false);

	std::vector<PathInfo> contents;
	FileIO::getDirectoryContents(structureViewer.currentDir, contents);

	if (UI::IsWindowHovered()) {
		if (UI::IsMouseClicked(0)) {
			structureViewer.selectedFiles.clear();
		}
	}

	for (PathInfo& item : contents) {
		std::string extension = FileIO::getExtension(item.path);
		if (extension.compare("meta") == 0) continue;
		if (structureViewer.currentDir.compare(projectDirectory) == 0 && item.path.compare("..") == 0) continue;

		std::string fullPath = structureViewer.currentDir + item.path;

		std::string iconID = "file";
		if (item.type == PathType::DIRECTORY) {
			iconID = "folder";
		}

		ImVec2 cursor = UI::GetCursorScreenPos();
		UI::SetCursorScreenPos(ImVec2(cursor.x + innerPadding, cursor.y + innerPadding));
		UI::BeginGroup();
		if (item.type == PathType::FILE) {
			bool found = false;
			for (auto const& it : Resources::assets) {
				if (it.second->getAssetName().compare(item.path) == 0) {
					drawTexture(std::dynamic_pointer_cast<grail::TextureAsset>(it.second), glm::vec2(itemSize));
					found = true; 
					break;
				}
			}

			if (!found) {
				drawUIImage(iconID, glm::vec2(itemSize));
			}
		}
		else {
			drawUIImage(iconID, glm::vec2(itemSize));
		}
		UI::TextWrapped("%s", FileIO::getFilename(item.path).c_str());
		UI::EndGroup();

		uint32_t color = IM_COL32(0, 0, 0, 0);

		if (UI::IsItemHovered()) {
			color = IM_COL32(255, 255, 255, 50);

			if (UI::IsMouseClicked(0)) {
				structureViewer.selectedFiles.push_back(fullPath);
			}
				
			if (UI::IsMouseDoubleClicked(0)) {
				if (item.type == PathType::DIRECTORY) {
					if (item.path.compare("..") == 0) {
						if (structureViewer.currentDir.compare(projectDirectory) != 0) {
							structureViewer.currentDir = structureViewer.currentDir.substr(0, structureViewer.currentDir.substr(0, structureViewer.currentDir.length() - 1).rfind("/") + 1);
						}
					}
					else {
						structureViewer.currentDir += item.path + "/";
					}
				} else if (item.type == PathType::FILE) {
					for (auto& it : Resources::assets) {
						if (it.second->getAssetName().compare(item.path) == 0) {
							inspector.type = InspectedItemType::TEXTURE;
							inspector.inspectedItem = reinterpret_cast<void*>(&it.second);
							break;
						}
					}
				}
			}
		}

		if (std::find(structureViewer.selectedFiles.begin(), structureViewer.selectedFiles.end(), fullPath) != structureViewer.selectedFiles.end()) {
			color = IM_COL32(255, 255, 255, 100);
		}

		ImVec2 p1 = ImVec2(UI::GetItemRectMax().x + innerPadding, UI::GetItemRectMax().y + innerPadding);
		UI::Dummy(ImVec2(0.0f, innerPadding));
		UI::GetWindowDrawList()->AddRectFilled(cursor, p1, color);
		UI::NextColumn();
	}
	UI::EndChild();

	UI::End();

	std::chrono::time_point end = std::chrono::high_resolution_clock::now();
	structureViewer.renderTime = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
}

void grail::GrailEditor::renderTransformGizmo() {
	SceneNode* node = sceneHierarchy.selectedNode;

	if (node == nullptr) return;

	glm::mat4 projection = camera->projection;
	glm::mat4 view = camera->view;
	projection = glm::scale(projection, glm::vec3(1, -1, 1));

	if (gizmo.op != ImGuizmo::DISABLED && node != Scene::getRootNode()) {
		if (ImGuizmo::IsUsing()) {
			if (!gizmo.transforming) {
				gizmo.worldCopy = node->transform.getWorldMatrix();
				gizmo.scaleCopy = node->transform.getWorldScale();
				gizmo.delta = glm::mat4();
				gizmo.transforming = true;
			}
		}
		else {
			gizmo.transforming = false;
			gizmo.worldCopy = node->transform.getWorldMatrix();
			gizmo.scaleCopy = node->transform.getWorldScale();
		}

		glm::vec3 min = node->transform.getLocalBoundsMin();
		glm::vec3 max = node->transform.getLocalBoundsMax();

		float bounds[] = {
			min.x, min.y, min.z, max.x, max.y, max.z
		};

		//ImGuizmo::SetRect(ui.mainViewBounds.x, ui.mainViewBounds.y, ui.mainViewBounds.z, ui.mainViewBounds.w);

		void* snapSizes = nullptr;

		if (gizmo.useSnapping) {
			if (gizmo.op == ImGuizmo::OPERATION::TRANSLATE) {
				snapSizes = &gizmo.positionSnap[0];
			}

			if (gizmo.op == ImGuizmo::OPERATION::ROTATE) {
				snapSizes = &gizmo.rotationSnap[0];
			}

			if (gizmo.op == ImGuizmo::OPERATION::SCALE) {
				snapSizes = &gizmo.scaleSnap[0];
			}
		}

		ImGuizmo::Manipulate(
			glm::value_ptr(view),
			glm::value_ptr(projection),
			gizmo.op,
			gizmo.transformWorld ? ImGuizmo::MODE::WORLD : ImGuizmo::MODE::LOCAL,
			glm::value_ptr(gizmo.worldCopy),
			glm::value_ptr(gizmo.delta),
			reinterpret_cast<float*>(snapSizes),
			nullptr);

		if (ImGuizmo::IsUsing() && gizmo.transforming) {
			glm::vec3 translation;
			glm::vec3 rotation;
			glm::vec3 scale;

			ImGuizmo::DecomposeMatrixToComponents(
				glm::value_ptr(gizmo.delta),
				&translation[0],
				&rotation[0],
				&scale[0]);

			if (gizmo.op == ImGuizmo::TRANSLATE) {
				node->transform.translateWorld(translation);
			}

			if (gizmo.op == ImGuizmo::ROTATE) {
				node->transform.rotateWorldEuler(rotation);
			}

			if (gizmo.op == ImGuizmo::SCALE) {
				glm::vec3 newScale = glm::vec3(
					gizmo.scaleCopy.x * scale[0],
					gizmo.scaleCopy.y * scale[1],
					gizmo.scaleCopy.z * scale[2]
				);

				node->transform.setWorldScale(newScale);
			}
		}
	}
}

void grail::GrailEditor::renderSceneHierarchy() {
	UI::Begin("Scene Hierarchy##grail_editor");

	static std::function<void(SceneNode*)> parseNode = [this](SceneNode* node) {
		ImGuiTreeNodeFlags node_flags =
			ImGuiTreeNodeFlags_OpenOnArrow |
			ImGuiTreeNodeFlags_OpenOnDoubleClick |
			(node == sceneHierarchy.selectedNode ? ImGuiTreeNodeFlags_Selected : ImGuiTreeNodeFlags_None);

		if (node->getChildCount() == 0) {
			node_flags |= ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen | ImGuiTreeNodeFlags_Bullet;
		}

		std::string name = "";
		ImVec4 color = ImVec4(1.0, 1.0, 0.0, 1.0);

		nodeComponents::Render* renderComponent = Scene::getComponent<nodeComponents::Render>(node);

		if (node->isPrefab()) {
			color = ImVec4(0.2, 0.2, 1.0, 1.0);
		}
		else if (renderComponent) {
			if (renderComponent->render)
				color = ImVec4(1.0, 1.0, 1.0, 1.0);
			else
				color = ImVec4(0.3, 0.3, 0.3, 1.0);
		}

		static grail::SceneNode* from;
		static grail::SceneNode* to;

		name.append(node->name.c_str());

		ImVec2 cursorPos = UI::GetCursorPos();

		UI::PushStyleColor(ImGuiCol_Text, color);
		bool node_open = UI::TreeNodeEx((name + "##grail_editor_scene_hierarchy").c_str(), node_flags);
		UI::PopStyleColor();
		
		if (sceneHierarchy.renaming && node == sceneHierarchy.selectedNode) {
			UI::SetCursorPos(cursorPos);
			UI::InputText(("##grail_editor_node_rename" + node->name).c_str(), &sceneHierarchy.renameString);
			if (UI::IsWindowHovered()) {
				if (!UI::IsItemActive()) {
					UI::SetKeyboardFocusHere(0);
				}

				if ((UI::IsMouseClicked(0) || UI::IsMouseClicked(1)) && !UI::IsItemHovered()) {
					if (sceneHierarchy.renaming) {
						sceneHierarchy.renaming = false;
						Scene::renameNode(sceneHierarchy.selectedNode, sceneHierarchy.renameString);
					}
				}
			}
			else if (!UI::IsWindowFocused()) {
				if (sceneHierarchy.renaming) {
					sceneHierarchy.renaming = false;
					Scene::renameNode(sceneHierarchy.selectedNode, sceneHierarchy.renameString);
				}
			}
		}
		else {
			if (node != Scene::getRootNode()) {
				if (UI::BeginDragDropSource()) {
					UI::Text("\"%s\"", name.c_str());
					UI::SetDragDropPayload("scene_hierarchy_reorder", &node, sizeof(grail::SceneNode**));
					UI::EndDragDropSource();
				}
			}
			if (UI::BeginDragDropTarget()) {
				if (const ImGuiPayload* payload = UI::AcceptDragDropPayload("scene_hierarchy_reorder", 0)) {
					from = *(grail::SceneNode**)payload->Data;
					to = node;
				}
				UI::EndDragDropTarget();
			}

			if (UI::IsItemClicked()) {
				sceneHierarchy.selectedNode = node;

				inspector.type = InspectedItemType::NODE;
				inspector.inspectedItem = node;
			}
			if (UI::BeginPopupContextItem()) {
				sceneHierarchy.selectedNode = node;
				if (sceneHierarchy.selectedNode == nullptr) return;

				if (UI::Selectable("Rename Node")) {
					sceneHierarchy.renaming = true;
					sceneHierarchy.renameString = sceneHierarchy.selectedNode->name;
					UI::EndPopup();
					return;
				}

				UI::Separator();

				if (UI::Selectable("Add New Child")) {
					sceneHierarchy.selectedNode->addChild(Scene::createNode());
					UI::EndPopup();
					return;
				}

				if (sceneHierarchy.selectedNode != Scene::getRootNode()) {
					if (UI::Selectable("Delete Node")) {
						if (sceneHierarchy.selectedNode == node) {
							if (node->getParent()) {
								sceneHierarchy.selectedNode = node->getParent();
							}
							else {
								sceneHierarchy.selectedNode = Scene::getRootNode();
							}
						}

						Scene::removeNode(node);
						UI::EndPopup();
						return;
					}

					if (UI::Selectable("Clone Node")) {
						SceneNode* copy = Scene::cloneNode(sceneHierarchy.selectedNode);
						sceneHierarchy.selectedNode = copy;
						UI::EndPopup();
						UI::TreePop();
						return;
					}
				}

				UI::Separator();

				if (sceneHierarchy.selectedNode != Scene::getRootNode()) {
					if (UI::BeginMenu("Assign Component")) {
						for (auto& item : nodeComponents::detail::componentAssignMap) {
							bool present = sceneHierarchy.selectedNode->getComponentFromMap(item.first);
							if (ImGui::Selectable((item.first + " Component").c_str(), &present, present ? ImGuiSelectableFlags_Disabled : 0)) {
								sceneHierarchy.selectedNode->createComponentFromMap(item.first);

								if (!item.first.compare("Render")) {
									nodeComponents::Render* renderComponent = Scene::getComponent<nodeComponents::Render>(sceneHierarchy.selectedNode);
									renderComponent->mesh = Resources::getMesh("sphere");
									renderComponent->material = Resources::getMaterial("default")->getBaseMaterial()->createInstance();
								}
							}
						}
						UI::EndMenu();
					}
				}
				UI::EndPopup();
			}
		}

		if (node_open) {
			if (node->getChildren().size() > 0) {
				for (grail::SceneNode* n : node->getChildren()) {
					parseNode(n);
				}
				UI::TreePop();
			}
		}

		// from = src
		// to = dst
		if (from && to) {
			if (to->descendantOf(from)) {
				from->getParent()->addChild(to);
				to->addChild(from);
			}
			else {
				if (from->getParent()) {
					from->getParent()->removeChild(from);
				}

				to->addChild(from, true);
			}

			UI::SetDragDropPayload("scene_hierarchy_reorder", &to, sizeof(grail::SceneNode**));

			from = nullptr;
			to = nullptr;
		}
	};

	parseNode(Scene::getRootNode());

	UI::End();
}

void grail::GrailEditor::renderInspector() {
	UI::Begin("Inspector##grail_editor");
	if (inspector.inspectedItem != nullptr) {
		if (inspector.type == InspectedItemType::NODE) {
			inspectNode();
		}

		if (inspector.type == InspectedItemType::TEXTURE) {
			AssetRef<TextureAsset>& ref = *reinterpret_cast<AssetRef<TextureAsset>*>(inspector.inspectedItem);
			uuid id = ref->getAssetUUID();

			if(ref.get())
				if (importers.texture->showImportMenu(ref, imguiHelper)) {
					inspector.inspectedItem = &Resources::assets[id];
				}
		}
	}
	UI::End();
}

void grail::GrailEditor::inspectNode() {
	SceneNode* node = reinterpret_cast<SceneNode*>(inspector.inspectedItem);

	UI::Text("Node: %s", node->name.c_str());

	ImGui::Separator();

	if (UI::CollapsingHeader("Transform", ImGuiTreeNodeFlags_DefaultOpen)) {
		glm::vec3 localPosition = node->transform.getLocalPosition();
		if (UI::InputFloat3("Local Position", &localPosition[0])) {
			node->transform.setLocalPosition(localPosition);
		}

		glm::vec3 worldPosition = node->transform.getWorldPosition();
		if (UI::InputFloat3("World Position", &worldPosition[0])) {
			node->transform.setWorldPosition(worldPosition);
		}

		glm::vec3 localEuler = node->transform.getLocalEuler();
		if (UI::InputFloat3("Local Euler", &localEuler[0])) {
			node->transform.setLocalEuler(localEuler);
		};

		glm::vec3 worldEuler = node->transform.getWorldEuler();
		if (UI::InputFloat3("World Euler", &worldEuler[0])) {
			node->transform.setWorldEuler(worldEuler);
		};

		glm::vec3 localScale = node->transform.getLocalScale();
		if (UI::InputFloat3("Local Scale", &localScale[0])) {
			node->transform.setLocalScale(localScale);
		};

		glm::vec3 worldScale = node->transform.getWorldScale();
		if (UI::InputFloat3("World Scale", &worldScale[0]));

		glm::vec3 right = node->transform.getRight();
		UI::Text("Right: [%.1f, %.1f, %.1f]", right.x, right.y, right.z);

		glm::vec3 up = node->transform.getUp();
		UI::Text("Up: [%.1f, %.1f, %.1f]", up.x, up.y, up.z);

		glm::vec3 foward = node->transform.getForward();
		UI::Text("Foward: [%.1f, %.1f, %.1f]", foward.x, foward.y, foward.z);

		UI::NewLine();
		UI::Text("%s", "Gizmo Mode");
		UI::Separator();
		if (UI::RadioButton("Disabled", gizmo.op == ImGuizmo::DISABLED))
			gizmo.op = ImGuizmo::DISABLED;
		if (UI::RadioButton("Translate", gizmo.op == ImGuizmo::TRANSLATE))
			gizmo.op = ImGuizmo::TRANSLATE;
		if (UI::RadioButton("Rotate", gizmo.op == ImGuizmo::ROTATE))
			gizmo.op = ImGuizmo::ROTATE;
		if (UI::RadioButton("Scale", gizmo.op == ImGuizmo::SCALE))
			gizmo.op = ImGuizmo::SCALE;
		UI::NewLine();
		UI::Checkbox("Snap##use_snapping", &gizmo.useSnapping);
		UI::Checkbox("Transform In World Space##transform_world", &gizmo.transformWorld);
		switch (gizmo.op)
		{
		case ImGuizmo::ROTATE:
			UI::InputFloat3(" Angle Snap", &gizmo.rotationSnap[0]);
			break;
		case ImGuizmo::SCALE:
			UI::InputFloat3(" Scale Snap", &gizmo.scaleSnap[0]);
			break;
		case ImGuizmo::TRANSLATE:
			UI::InputFloat3(" Position Snap", &gizmo.positionSnap[0]);
			break;
		}
	}
}

void grail::GrailEditor::saveEditorLayout() {
	size_t size = 0;
	const char* settings = ImGui::SaveIniSettingsToMemory(&size);

	std::ofstream file;
	file.open(projectDirectory + "ui.ini");
	file << settings;
	file.close();
}

void grail::GrailEditor::createDockspace() {
	static bool opt_fullscreen = true;
	static bool opt_padding = false;
	static ImGuiDockNodeFlags dockspace_flags = ImGuiDockNodeFlags_None;

	ImGuiWindowFlags window_flags = ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking;
	if (opt_fullscreen)
	{
		ImGuiViewport* viewport = ImGui::GetMainViewport();
		ImGui::SetNextWindowPos(viewport->Pos);
		ImGui::SetNextWindowSize(viewport->Size);
		ImGui::SetNextWindowViewport(viewport->ID);
		ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
		ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
		window_flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
		window_flags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;
	}
	else
	{
		dockspace_flags &= ~ImGuiDockNodeFlags_PassthruCentralNode;
	}

	//dockspace_flags &= ~ImGuiDockNodeFlags_PassthruCentralNode;

	// When using ImGuiDockNodeFlags_PassthruCentralNode, DockSpace() will render our background
	// and handle the pass-thru hole, so we ask Begin() to not render a background.
	if (dockspace_flags & ImGuiDockNodeFlags_PassthruCentralNode)
		window_flags |= ImGuiWindowFlags_NoBackground;

	// Important: note that we proceed even if Begin() returns false (aka window is collapsed).
	// This is because we want to keep our DockSpace() active. If a DockSpace() is inactive,
	// all active windows docked into it will lose their parent and become undocked.
	// We cannot preserve the docking relationship between an active window and an inactive docking, otherwise
	// any change of dockspace/settings would lead to windows being stuck in limbo and never being visible.
	if (!opt_padding)
		ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
	ImGui::Begin("Dockspace", nullptr, window_flags);
	if (!opt_padding)
		ImGui::PopStyleVar();

	if (opt_fullscreen)
		ImGui::PopStyleVar(2);

	ImGuiIO& io = ImGui::GetIO();
	if (io.ConfigFlags & ImGuiConfigFlags_DockingEnable)
	{
		ImGuiID dockspace_id = ImGui::GetID("Main Dockspace");
		ImGui::DockSpace(dockspace_id, ImVec2(0.0f, 0.0f), dockspace_flags);
	}

	ImGui::End();
}

void grail::editor::loadIcons() {
	TextureCreateInfo info = {};
	info.samplerInfo.magFilter = SamplerFilter::LINEAR;
	info.samplerInfo.minFilter = SamplerFilter::LINEAR;
	info.generateMipmaps = false;

	iconAtlas = TextureLoader::loadTexture("../internal/UI.png", info);

	char* contents = nullptr;
	uint32_t size;
	FileIO::readBinaryFile("../internal/UI.json", &contents, &size);

	nlohmann::json atlasInfo = nlohmann::json::parse(std::string(contents, size));

	for (auto it = atlasInfo.begin(); it != atlasInfo.end(); ++it) {
		float x = (float)it.value()["x"];
		float y = (float)it.value()["y"];
		float width = (float)it.value()["width"];
		float height = (float)it.value()["height"];

		float srcWidth = (float)iconAtlas->getWidth();
		float srcHeight = (float)iconAtlas->getHeight();

		Icon i = {};
		i.u = x / srcWidth;
		i.v = y / srcHeight;

		i.u2 = (x + width) / srcWidth;
		i.v2 = (y + height) / srcHeight;

		i.uv0 = ImVec2(i.u, i.v);
		i.uv1 = ImVec2(i.u2, i.v2);

		icons.insert({ it.key(), i });
	}

	delete[] contents;
}

grail::editor::Icon& grail::editor::getIcon(const std::string& id) {
	return editor::icons[id];
}

grail::AssetImporter* grail::GrailEditor::getImporterByExtension(const std::string& extension) {
	if (std::find(assets::textureFormats.begin(), assets::textureFormats.end(), extension) != assets::textureFormats.end()) {
		return importers.texture;
	}

	return nullptr;
}
