#include "MeshLoader.h"
#include "MemoryAllocator.h"
#include "VertexLayout.h"
#include "SceneNode.h"
#include "Renderer.h"
#include "Material.h"
#include "Resources.h"
#include "Utils.h"
#include "FileIO.h"
#include "Scene.h"
#include "SceneNodeComponents.h"

#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>

void replace(std::string& subject, const std::string& search,
	const std::string& replace) {
	size_t pos = 0;
	while ((pos = subject.find(search, pos)) != std::string::npos) {
		subject.replace(pos, search.length(), replace);
		pos += replace.length();
	}
}

grail::SceneNode* grail::MeshLoader::loadMesh(std::string path, VertexLayout * vertexLayout, Renderer * renderer, bool loadMaterials, std::string customMaterialFile, MemoryAllocator* stagingMemory, MemoryAllocator* dedicatedMemory) {
	utils::string::replaceAll(path, "\\", "/");

	unsigned char* contents;
	uint32_t size;
	FileIO::readBinaryFile(path, &contents, &size);

	Assimp::Importer importer = Assimp::Importer();
	const aiScene* scene = importer.ReadFileFromMemory(contents, size,
		aiProcess_JoinIdenticalVertices |
		aiProcess_ValidateDataStructure |
		aiProcess_ImproveCacheLocality |
		aiProcess_RemoveRedundantMaterials |
		aiProcess_FindDegenerates |
		aiProcess_FindInvalidData |
		aiProcess_GenUVCoords |
		aiProcess_FlipUVs |
		aiProcess_FindInstances |
		//aiProcess_GenUVCoords |
		//aiProcess_OptimizeMeshes |
		//aiProcess_OptimizeGraph |

		aiProcess_GenSmoothNormals |
		aiProcess_Triangulate |
		aiProcess_SortByPType |
		aiProcess_CalcTangentSpace |
		aiProcess_MakeLeftHanded |
		aiProcess_FlipWindingOrder
	, path.c_str());

	if ((!scene) || (scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE) || (!scene->mRootNode))
		throw new exceptions::RuntimeException("Could not load Model \"" + path + "\": " + importer.GetErrorString());

	std::string directory = path.substr(0, path.find_last_of("/"));

	return MeshLoader::processNode(path, directory, vertexLayout, renderer, loadMaterials, scene->mRootNode, scene, stagingMemory, dedicatedMemory);
}

void grail::MeshLoader::loadBaseMesh(std::string path, VertexLayout * vertexLayout, Renderer * renderer, bool loadMaterials, std::string customMaterialFile, MemoryAllocator* stagingMemory, MemoryAllocator* dedicatedMemory) {
	utils::string::replaceAll(path, "\\", "/");

	// NOTE: need proper file absolute and relative path handling
	std::string properPath = path;
	utils::string::replaceAll(properPath, "assets/", "");

	unsigned char* contents;
	uint32_t size;
	FileIO::readBinaryFile(properPath, &contents, &size);

	Assimp::Importer importer = Assimp::Importer();
	const aiScene* scene = importer.ReadFileFromMemory(contents, size,
		aiProcess_JoinIdenticalVertices |
		aiProcess_ValidateDataStructure |
		aiProcess_ImproveCacheLocality |
		aiProcess_RemoveRedundantMaterials |
		aiProcess_FindDegenerates |
		aiProcess_FindInvalidData |
		aiProcess_GenUVCoords |
		aiProcess_FlipUVs |
		aiProcess_FindInstances |
		//aiProcess_GenUVCoords |

		//aiProcess_OptimizeMeshes |
		//aiProcess_OptimizeGraph |

		aiProcess_GenSmoothNormals |
		aiProcess_Triangulate |
		aiProcess_SortByPType |
		aiProcess_CalcTangentSpace |
		aiProcess_MakeLeftHanded |
		aiProcess_FlipWindingOrder
	, FileIO::getExtension(properPath).c_str());

	if ((!scene) || (scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE) || (!scene->mRootNode))
		throw new exceptions::RuntimeException("Could not load Model \"" + path + "\": " + importer.GetErrorString());

	std::string directory = path.substr(0, path.find_last_of("/"));

	MeshLoader::processBaseNode(path, directory, vertexLayout, renderer, loadMaterials, scene->mRootNode, scene, stagingMemory, dedicatedMemory);
}

grail::Mesh* grail::MeshLoader::createMesh(std::string identifier, VertexLayout * vertexLayout, std::vector<float>& vertices, std::vector<uint32_t>& indices, MemoryAllocator* stagingMemory, MemoryAllocator* dedicatedMemory) {
	Mesh* mesh = new Mesh();

	mesh->identifier = identifier;

	GPU mainGPU = VulkanContext::mainGPU;

	uint32_t vertexBufferSize = static_cast<uint32_t>(vertices.size()) * sizeof(float);
	uint32_t indexBufferSize = static_cast<uint32_t>(indices.size()) * sizeof(uint32_t);

	void* ptr;

	VkMemoryRequirements memReqs;

	MemoryAllocation stageMemoryRegionV;
	MemoryAllocation stageMemoryRegionI;

	MemoryAllocation localMemoryRegion;
	//MemoryRegionDescriptor localMemoryRegionI;

	VkBuffer vertexStagingBuffer;
	VkBuffer indexStagingBuffer;

	// VERTEX STAGE
	VkBufferCreateInfo bufferInfo = {};
	bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferInfo.size = vertexBufferSize;
	bufferInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
	VK_VALIDATE_RESULT(vkCreateBuffer(mainGPU.device, &bufferInfo, nullptr, &vertexStagingBuffer));

	vkGetBufferMemoryRequirements(mainGPU.device, vertexStagingBuffer, &memReqs);

	stageMemoryRegionV = stagingMemory->allocate(memReqs.size, memReqs.alignment, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, memReqs.memoryTypeBits);
	ptr = stageMemoryRegionV.mapMemory();
	{
		memcpy(ptr, vertices.data(), vertexBufferSize);
	}
	stageMemoryRegionV.unmapMemory();
	VK_VALIDATE_RESULT(vkBindBufferMemory(mainGPU.device, vertexStagingBuffer, stageMemoryRegionV.page->memoryHandle, stageMemoryRegionV.baseAddressAligned));

	// INDEX STAGE
	bufferInfo.size = indexBufferSize;
	VK_VALIDATE_RESULT(vkCreateBuffer(mainGPU.device, &bufferInfo, nullptr, &indexStagingBuffer));

	vkGetBufferMemoryRequirements(mainGPU.device, indexStagingBuffer, &memReqs);

	stageMemoryRegionI = stagingMemory->allocate(memReqs.size, memReqs.alignment, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, memReqs.memoryTypeBits);
	ptr = stageMemoryRegionI.mapMemory();
	{
		memcpy(ptr, indices.data(), indexBufferSize);
	}
	stageMemoryRegionI.unmapMemory();
	VK_VALIDATE_RESULT(vkBindBufferMemory(mainGPU.device, indexStagingBuffer, stageMemoryRegionI.page->memoryHandle, stageMemoryRegionI.baseAddressAligned));

	// MESH BUFFFER
	{
		VkMemoryRequirements memReqs;

		VkBufferCreateInfo globalBufferInfo = {};
		globalBufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
		globalBufferInfo.size = stageMemoryRegionV.size + stageMemoryRegionI.size;
		globalBufferInfo.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;

		VK_VALIDATE_RESULT(vkCreateBuffer(mainGPU.device, &globalBufferInfo, nullptr, &mesh->buffer));

		vkGetBufferMemoryRequirements(mainGPU.device, mesh->buffer, &memReqs);

		// GLOBAL MEM MAP
		localMemoryRegion = dedicatedMemory->allocate(stageMemoryRegionV.size + stageMemoryRegionI.size, stageMemoryRegionV.alignment, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, memReqs.memoryTypeBits);
		//localMemoryRegionI = dedicatedMemory->allocateRegion(stageMemoryRegionI.size, stageMemoryRegionI.alignment, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, memReqs.memoryTypeBits);

		VK_VALIDATE_RESULT(vkBindBufferMemory(mainGPU.device, mesh->buffer, localMemoryRegion.page->memoryHandle, localMemoryRegion.baseAddressAligned));
	}

	// COPY
	TemporaryCommandBuffer setupCommandBuffer = mainGPU.createTemporaryBuffer(mainGPU.transferQueue, VK_COMMAND_BUFFER_LEVEL_PRIMARY);
	setupCommandBuffer.begin();

	VkBufferCopy copyRegion = {};

	// Vertex buffer
	copyRegion.srcOffset = 0;
	copyRegion.dstOffset = 0;
	copyRegion.size = vertexBufferSize;
	vkCmdCopyBuffer(setupCommandBuffer.getHandle(), vertexStagingBuffer, mesh->buffer, 1, &copyRegion);

	// Index buffer
	copyRegion.srcOffset = 0;
	copyRegion.dstOffset = vertexBufferSize;
	copyRegion.size = indexBufferSize;
	vkCmdCopyBuffer(setupCommandBuffer.getHandle(), indexStagingBuffer, mesh->buffer, 1, &copyRegion);

	// Flushing the command buffer will also submit it to the queue and uses a fence to ensure that all commands have been executed before returning
	setupCommandBuffer.end();
	setupCommandBuffer.submit();
	setupCommandBuffer.dispose();
	// COPY 

	vkDestroyBuffer(mainGPU.device, vertexStagingBuffer, nullptr);
	vkDestroyBuffer(mainGPU.device, indexStagingBuffer, nullptr);

	mesh->indicesCount = static_cast<uint32_t>(indices.size());
	mesh->verticesCount = static_cast<uint32_t>(vertices.size());

	mesh->vertices = vertices;
	mesh->indices = indices;

	mesh->memoryRegion = localMemoryRegion;

/*	{
		MemoryRegionDescriptor localMemoryRegionV = {};
		localMemoryRegionV

		mesh.vertexRegionDescriptor = localMemoryRegionV;
		mesh.indexRegionDescriptor = localMemoryRegionI;
	}
*/

	mesh->vertexOffset = 0;
	mesh->indexOffset = vertexBufferSize;

	mesh->vertexBufferSize = vertexBufferSize;
	mesh->indexBufferSize = indexBufferSize;

	stagingMemory->clearAllPages();

	return mesh;
}

void grail::MeshLoader::loadModel(std::string path, VertexLayout* vertexLayout, MemoryAllocator* statingMemory, MemoryAllocator* dedicatedMemory, bool insertInScene) {
	utils::string::replaceAll(path, "\\", "/");

	unsigned char* contents;
	uint32_t size;
	FileIO::readBinaryFile(path, &contents, &size);

	Assimp::Importer importer = Assimp::Importer();
	const aiScene* scene = importer.ReadFileFromMemory(contents, size,
		aiProcess_JoinIdenticalVertices |
		aiProcess_ValidateDataStructure |
		aiProcess_ImproveCacheLocality |
		aiProcess_RemoveRedundantMaterials |
		aiProcess_FindDegenerates |
		aiProcess_FindInvalidData |
		aiProcess_GenUVCoords |
		aiProcess_FlipUVs |
		aiProcess_FindInstances |
		aiProcess_GenSmoothNormals |
		aiProcess_Triangulate |
		aiProcess_SortByPType |
		aiProcess_CalcTangentSpace |
		aiProcess_MakeLeftHanded |
		aiProcess_FlipWindingOrder
		, path.c_str());

	if ((!scene) || (scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE) || (!scene->mRootNode))
		throw new exceptions::RuntimeException("Could not load Model \"" + path + "\": " + importer.GetErrorString());

	std::string directory = path.substr(0, path.find_last_of("/"));

	SceneNode* baseNode = nullptr;

	if (insertInScene) baseNode = Scene::createNode();

	MeshLoader::processNode(path, vertexLayout, scene->mRootNode, scene, statingMemory, dedicatedMemory, baseNode);

	delete[] contents;
}

void grail::MeshLoader::processNode(std::string path, VertexLayout* vertexLayout, aiNode* node, const aiScene* scene, MemoryAllocator* stagingRegion, MemoryAllocator* dedicatedMemory, SceneNode* baseNode) {
	for (uint32_t i = 0; i < node->mNumMeshes; i++) {
		aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
		aiString nodeName = mesh->mName;

		Mesh* parsed = MeshLoader::parseMesh(mesh, vertexLayout, stagingRegion, dedicatedMemory);

		if (nodeName.length > 0) {
			parsed->identifier = nodeName.C_Str();
			parsed->baseAsset = path;
		}
		else {
			std::string assignedName = std::to_string(GRAIL_SID(path));

			uint32_t index = 0;
			std::stringstream rename;
			while (true) {
				rename.str("");

				rename << assignedName << "_" << ++index;

				if (Resources::meshes.find(GRAIL_SID(rename.str())) == Resources::meshes.end()) {
					break;
				}
			}

			parsed->identifier = rename.str();
			parsed->baseAsset = path;
		}

		Resources::meshes[GRAIL_SID(parsed->identifier)] = parsed;

		if (baseNode) {
			SceneNode* child = Scene::createNode(parsed->identifier);
			Scene::assignComponent<nodeComponents::Render>(child);

			nodeComponents::Render* render = Scene::getComponent<nodeComponents::Render>(child);
			render->initDefault();
			render->mesh = parsed;

			baseNode->addChild(child);

			aiMaterial* aMat = scene->mMaterials[mesh->mMaterialIndex];

			if (aMat) {
				/*std::string loc = FileIO::getBasePath(path);

				{
					aiString file;
					aMat->GetTexture(aiTextureType_DIFFUSE, 0, &file);
					std::string diffPath = loc + std::string(file.C_Str());
					replace(diffPath, "/", "\\");

					VkTexture* diffuse = nullptr;

					if (diffPath.length() > 0) {
						TextureCreateInfo info = {};
						info.generateMipmaps = true;

						diffuse = Resources::loadTexture(diffPath, info);
					}

					if (diffuse) {
						render->material->setDescriptorTexture("albedoTexture", diffuse);
					}
				}

				{
					aiString file;
					aMat->GetTexture(aiTextureType_NORMALS, 0, &file);
					std::string diffPath = loc + std::string(file.C_Str());
					replace(diffPath, "/", "\\");

					VkTexture* normal = nullptr;

					if (diffPath.length() > 0) {
						TextureCreateInfo info = {};
						info.generateMipmaps = true;

						normal = Resources::loadTexture(diffPath, info);
					}

					if (normal) {
						render->material->setDescriptorTexture("normalTexture", normal);
					}
				}

				render->material->updateAllDescriptors();*/
			}
		}
	}

	for (uint32_t i = 0; i < node->mNumChildren; i++) {
		MeshLoader::processNode(path, vertexLayout, node->mChildren[i], scene, stagingRegion, dedicatedMemory, baseNode);
	}
}

grail::Mesh* grail::MeshLoader::parseMesh(aiMesh* node, VertexLayout* vertexLayout, MemoryAllocator* stagingMemory, MemoryAllocator* dedicatedMemory) {
	Mesh* mesh = new Mesh();

	std::vector<float> vertexBuffer;
	std::vector<uint32_t> indexBuffer;

	bool hasUvs = node->mTextureCoords[0] ? true : false;
	bool hasNormals = node->mNormals ? true : false;
	bool hasPositions = node->mVertices ? true : false;
	bool hasTangents = node->mTangents ? true : false;

	//GRAIL_LOG(INFO, "INFO") << hasUvs << " " << hasNormals << " " << hasPositions << " " << hasTangents;

	uint32_t elementCount = node->mNumVertices * (vertexLayout ? vertexLayout->getVertexElementCount() : 12);
	vertexBuffer.resize(elementCount);

	uint32_t v_index = 0;
	for (unsigned int j = 0; j < node->mNumVertices; ++j) {
		glm::vec3 pos = glm::vec3(node->mVertices[j].x, node->mVertices[j].y, node->mVertices[j].z);

		mesh->boundsMin = glm::min(mesh->boundsMin, pos);
		mesh->boundsMax = glm::max(mesh->boundsMax, pos);

		vertexBuffer[v_index++] = pos.x;
		vertexBuffer[v_index++] = pos.y;
		vertexBuffer[v_index++] = pos.z;

		if (hasNormals) {
			vertexBuffer[v_index++] = node->mNormals[j].x;
			vertexBuffer[v_index++] = node->mNormals[j].y;
			vertexBuffer[v_index++] = node->mNormals[j].z;
		}
		else {
			vertexBuffer[v_index++] = 0;
			vertexBuffer[v_index++] = 0;
			vertexBuffer[v_index++] = 0;
		}

		if (hasTangents) {
			vertexBuffer[v_index++] = node->mTangents[j].x;
			vertexBuffer[v_index++] = node->mTangents[j].y;
			vertexBuffer[v_index++] = node->mTangents[j].z;
		}
		else {
			vertexBuffer[v_index++] = 0;
			vertexBuffer[v_index++] = 0;
			vertexBuffer[v_index++] = 0;
		}

		if (hasUvs) {
			vertexBuffer[v_index++] = node->mTextureCoords[0][j].x;
			vertexBuffer[v_index++] = node->mTextureCoords[0][j].y;
		}
		else {
			vertexBuffer[v_index++] = 0.01f * j;
			vertexBuffer[v_index++] = 0.01f * j;
		}

		vertexBuffer[v_index++] = (0);
	}

	for (unsigned int j = 0; j < node->mNumFaces; ++j) {
		aiFace face = node->mFaces[j];
		for (unsigned int k = 0; k < face.mNumIndices; ++k) { indexBuffer.push_back(face.mIndices[k]); }
	}

	////

	GPU mainGPU = VulkanContext::mainGPU;

	////

	uint32_t vertexBufferSize = static_cast<uint32_t>(vertexBuffer.size()) * sizeof(float);
	uint32_t indexBufferSize = static_cast<uint32_t>(indexBuffer.size()) * sizeof(uint32_t);

	void* ptr;

	VkMemoryRequirements memReqs;

	MemoryAllocation stageMemoryRegionV;
	MemoryAllocation stageMemoryRegionI;

	MemoryAllocation localMemoryRegion;
	//MemoryRegionDescriptor localMemoryRegionI;

	VkBuffer vertexStagingBuffer;
	VkBuffer indexStagingBuffer;

	// VERTEX STAGE
	VkBufferCreateInfo bufferInfo = {};
	bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferInfo.size = vertexBufferSize;
	bufferInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
	VK_VALIDATE_RESULT(vkCreateBuffer(mainGPU.device, &bufferInfo, nullptr, &vertexStagingBuffer));

	vkGetBufferMemoryRequirements(mainGPU.device, vertexStagingBuffer, &memReqs);

	stageMemoryRegionV = stagingMemory->allocate(memReqs.size, memReqs.alignment, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, memReqs.memoryTypeBits);
	ptr = stageMemoryRegionV.mapMemory();
	{
		memcpy(ptr, vertexBuffer.data(), vertexBufferSize);
	}
	stageMemoryRegionV.unmapMemory();
	VK_VALIDATE_RESULT(vkBindBufferMemory(mainGPU.device, vertexStagingBuffer, stageMemoryRegionV.page->memoryHandle, stageMemoryRegionV.baseAddressAligned));

	// INDEX STAGE
	bufferInfo.size = indexBufferSize;
	VK_VALIDATE_RESULT(vkCreateBuffer(mainGPU.device, &bufferInfo, nullptr, &indexStagingBuffer));

	vkGetBufferMemoryRequirements(mainGPU.device, indexStagingBuffer, &memReqs);

	stageMemoryRegionI = stagingMemory->allocate(memReqs.size, memReqs.alignment, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, memReqs.memoryTypeBits);
	ptr = stageMemoryRegionI.mapMemory();
	{
		memcpy(ptr, indexBuffer.data(), indexBufferSize);
	}
	stageMemoryRegionI.unmapMemory();
	VK_VALIDATE_RESULT(vkBindBufferMemory(mainGPU.device, indexStagingBuffer, stageMemoryRegionI.page->memoryHandle, stageMemoryRegionI.baseAddressAligned));

	// MESH BUFFFER
	{
		VkMemoryRequirements memReqs;

		VkBufferCreateInfo globalBufferInfo = {};
		globalBufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
		globalBufferInfo.size = stageMemoryRegionV.size + stageMemoryRegionI.size;
		globalBufferInfo.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;

		VK_VALIDATE_RESULT(vkCreateBuffer(mainGPU.device, &globalBufferInfo, nullptr, &mesh->buffer));

		vkGetBufferMemoryRequirements(mainGPU.device, mesh->buffer, &memReqs);

		// GLOBAL MEM MAP
		localMemoryRegion = dedicatedMemory->allocate(stageMemoryRegionV.size + stageMemoryRegionI.size, stageMemoryRegionV.alignment, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, memReqs.memoryTypeBits);
		//localMemoryRegionI = dedicatedMemory->allocateRegion(stageMemoryRegionI.size, stageMemoryRegionI.alignment, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, memReqs.memoryTypeBits);

		VK_VALIDATE_RESULT(vkBindBufferMemory(mainGPU.device, mesh->buffer, localMemoryRegion.page->memoryHandle, localMemoryRegion.baseAddressAligned));
	}

	// COPY
	TemporaryCommandBuffer setupCommandBuffer = mainGPU.createTemporaryBuffer(mainGPU.transferQueue, VK_COMMAND_BUFFER_LEVEL_PRIMARY);
	setupCommandBuffer.begin();

	VkBufferCopy copyRegion = {};

	// Vertex buffer
	copyRegion.srcOffset = 0;
	copyRegion.dstOffset = 0;
	copyRegion.size = vertexBufferSize;
	vkCmdCopyBuffer(setupCommandBuffer.getHandle(), vertexStagingBuffer, mesh->buffer, 1, &copyRegion);

	// Index buffer
	copyRegion.srcOffset = 0;
	copyRegion.dstOffset = vertexBufferSize;
	copyRegion.size = indexBufferSize;
	vkCmdCopyBuffer(setupCommandBuffer.getHandle(), indexStagingBuffer, mesh->buffer, 1, &copyRegion);

	// Flushing the command buffer will also submit it to the queue and uses a fence to ensure that all commands have been executed before returning
	setupCommandBuffer.end();
	setupCommandBuffer.submit();
	setupCommandBuffer.dispose();
	// COPY 

	vkDestroyBuffer(mainGPU.device, vertexStagingBuffer, nullptr);
	vkDestroyBuffer(mainGPU.device, indexStagingBuffer, nullptr);

	mesh->indicesCount = static_cast<uint32_t>(indexBuffer.size());
	mesh->verticesCount = static_cast<uint32_t>(vertexBuffer.size());
	{
		//mesh->vertexRegionDescriptor = localMemoryRegionV;
		//mesh->indexRegionDescriptor = localMemoryRegionI;
	}
	mesh->vertexOffset = 0;
	mesh->indexOffset = vertexBufferSize;

	mesh->vertices = vertexBuffer;
	mesh->indices = indexBuffer;
	mesh->vertexBufferSize = vertexBufferSize;
	mesh->indexBufferSize = indexBufferSize;

	mesh->memoryRegion = localMemoryRegion;

	stagingMemory->clearAllPages();

	return mesh;
}

grail::SceneNode * grail::MeshLoader::processNode(std::string path, std::string directory, VertexLayout * vertexLayout, Renderer * renderer, bool loadMaterials, aiNode * node, const aiScene * scene, MemoryAllocator* stagingMemory, MemoryAllocator* dedicatedMemory) {
	SceneNode* rootNode = new SceneNode();

	for (unsigned int i = 0; i < node->mNumMeshes; i++) {
		glm::vec3 boxMin;
		glm::vec3 boxMax;
		aiMesh* aMesh = scene->mMeshes[node->mMeshes[i]];
		aiMaterial* aMat = scene->mMaterials[aMesh->mMaterialIndex];
		aiString name = aMesh->mName;

		Mesh* mesh = MeshLoader::parseMesh(aMesh, scene, boxMin, boxMax, stagingMemory, dedicatedMemory);
		mesh->boundsMin = boxMin;
		mesh->boundsMax = boxMax;

		std::string meshID = path + "_" + name.C_Str();
		Resources::meshes[GRAIL_SID(meshID)] = mesh;

		Material* material = nullptr;

		if (loadMaterials && aMat) {
			//material = MeshLoader::processMaterial(renderer, aMat, scene, directory, rootNode);
		}
		else {
			//material = renderer->createDefaultDeferredMaterial();
		}

		glm::vec4 color = glm::vec4(1, 1, 1, 1);

		if (aMat) {
			aiColor3D diffuse(1.0, 1.0, 1.0);
			aMat->Get(AI_MATKEY_COLOR_DIFFUSE, diffuse);

			color.r = diffuse.r;
			color.g = diffuse.g;
			color.b = diffuse.b;
		}

		color = glm::vec4(1);

		/*rootNode->baseMeshPath = path;

		if (node->mNumMeshes == 1) {
			rootNode->mesh = mesh;
			rootNode->boundsMin = boxMin;
			rootNode->boundsMax = boxMax;
			rootNode->name = std::string(name.C_Str());
			//rootNode->aiMaterial = new aiMaterial();
			if (loadMaterials && aMat) {
				MeshLoader::processMaterial(renderer, aMat, scene, directory, rootNode);
			}
			//aMat->CopyPropertyList(rootNode->aiMaterial, aMat);

			rootNode->mesh->identifier = rootNode->name;
		}
		else {
			SceneNode* child = new SceneNode();
			child->mesh = mesh;
			child->boundsMin = boxMin;
			child->boundsMax = boxMax;
			child->name = std::string(name.C_Str());
			//child->aiMaterial = new aiMaterial();
			if (loadMaterials && aMat) {
				MeshLoader::processMaterial(renderer, aMat, scene, directory, child);
			}
			//aMat->CopyPropertyList(child->aiMaterial, aMat);
			rootNode->addChild(child);

			child->mesh->identifier = child->name;
		}*/
	}

	for (unsigned int i = 0; i < node->mNumChildren; i++) {
		rootNode->addChild(MeshLoader::processNode(path, directory, vertexLayout, renderer, loadMaterials, node->mChildren[i], scene, stagingMemory, dedicatedMemory));
	}
	
	return rootNode;
}

void grail::MeshLoader::processBaseNode(std::string path, std::string directory, VertexLayout * vertexLayout, Renderer * renderer, bool loadMaterials, aiNode * node, const aiScene * scene, MemoryAllocator* stagingMemory, MemoryAllocator* dedicatedMemory) {
	for (unsigned int i = 0; i < node->mNumMeshes; i++) {
		glm::vec3 boxMin;
		glm::vec3 boxMax;
		aiMesh* aMesh = scene->mMeshes[node->mMeshes[i]];
		aiMaterial* aMat = scene->mMaterials[aMesh->mMaterialIndex];
		aiString name = aMesh->mName;

		Mesh* mesh = MeshLoader::parseMesh(aMesh, scene, boxMin, boxMax, stagingMemory, dedicatedMemory);
		mesh->boundsMin = boxMin;
		mesh->boundsMax = boxMax;


		std::string meshID = path + "_" + name.C_Str();
		Resources::meshes[GRAIL_SID(meshID)] = mesh;

		Material* material = nullptr;

		if (loadMaterials && aMat) {
			//material = MeshLoader::processMaterial(renderer, aMat, scene, directory);
		}
		else {
			//material = renderer->createDefaultDeferredMaterial();
		}

		glm::vec4 color = glm::vec4(1, 1, 1, 1);

		if (aMat) {
			aiColor3D diffuse(1.0, 1.0, 1.0);
			aMat->Get(AI_MATKEY_COLOR_DIFFUSE, diffuse);

			color.r = diffuse.r;
			color.g = diffuse.g;
			color.b = diffuse.b;
		}

		color = glm::vec4(1);
	}

	for (unsigned int i = 0; i < node->mNumChildren; i++) {
		MeshLoader::processBaseNode(path, directory, vertexLayout, renderer, loadMaterials, node->mChildren[i], scene, stagingMemory, dedicatedMemory);
	}
}

grail::Material * grail::MeshLoader::processMaterial(Renderer * renderer, aiMaterial * aMaterial, const aiScene * scene, std::string directory, SceneNode* node) {
	
	renderer->createDefaultDeferredMaterial(node);
	
	//Material* material = node->materials.at(0);

	{
		aiString file;
		aMaterial->GetTexture(aiTextureType_DIFFUSE, 0, &file);
		std::string diffPath = std::string(file.C_Str());
		replace(diffPath, "/", "\\");

		VkTexture* diffuse = nullptr;

		if (diffPath.length() > 0) {
			TextureCreateInfo info = {};
			/*info.samplerInfo.magFilter = SamplerFilter::NEAREST;
			info.samplerInfo.minFilter = SamplerFilter::NEAREST;
			info.samplerInfo.addressModeU = SamplerAddressMode::CLAMP_TO_EDGE;
			info.samplerInfo.addressModeV = SamplerAddressMode::CLAMP_TO_EDGE;*/
			//info.samplerInfo.unnormalizedCoordinates = true;
			info.generateMipmaps = true;

			diffuse = Resources::loadTexture(diffPath, info);
		}

		if (diffuse) {
			//node->materials.at(0)->getDescriptors().getBinding("albedoTexture").setDescriptor(&diffuse->getDescriptor());
			//node->materials.at(1)->getDescriptors().getBinding("albedoTexture").setDescriptor(&diffuse->getDescriptor());
			//node->materials.at(2)->getDescriptors().getBinding("albedoTexture").setDescriptor(&diffuse->getDescriptor());
		}
	}

	{
		//aiTextureType_NORMALS
		//aiTextureType_HEIGHT

		aiString file;
		aMaterial->GetTexture(aiTextureType_NORMALS, 0, &file);
		std::string diffPath = std::string(file.C_Str());
		replace(diffPath, "/", "\\");

		VkTexture* normal = nullptr;

		if (diffPath.length() > 0) {
			TextureCreateInfo info = {};
			info.generateMipmaps = true;

			normal = Resources::loadTexture(diffPath, info);
		}

		if (normal) {
			//node->materials.at(0)->getDescriptors().getBinding("normalTexture").setDescriptor(&normal->getDescriptor());
			//node->materials.at(1)->getDescriptors().getBinding("normalTexture").setDescriptor(&normal->getDescriptor());
			//node->materials.at(2)->getDescriptors().getBinding("normalTexture").setDescriptor(&normal->getDescriptor());
		}
	}

	{
		//aiTextureType_SPECULAR
		//aiTextureType_AMBIENT

		aiString file;
		aMaterial->GetTexture(aiTextureType_SPECULAR, 0, &file);
		std::string diffPath = std::string(file.C_Str());
		replace(diffPath, "/", "\\");

		VkTexture* roughness = nullptr;

		if (diffPath.length() > 0) {
			TextureCreateInfo info = {};
			info.generateMipmaps = true;

			roughness = Resources::loadTexture(diffPath, info);
		}

		if (roughness) {
			//node->materials.at(0)->getDescriptors().getBinding("roughnessTexture").setDescriptor(&roughness->getDescriptor());
			//node->materials.at(1)->getDescriptors().getBinding("roughnessTexture").setDescriptor(&roughness->getDescriptor());
			//node->materials.at(2)->getDescriptors().getBinding("roughnessTexture").setDescriptor(&roughness->getDescriptor());
		}
	}

	{
		//aiTextureType_AMBIENT

		aiString file;
		aMaterial->GetTexture(aiTextureType_SPECULAR, 0, &file);
		std::string diffPath = std::string(file.C_Str());
		replace(diffPath, "/", "\\");

		VkTexture* metallic = nullptr;

		if (diffPath.length() > 0) {
			TextureCreateInfo info = {};
			info.generateMipmaps = true;

			metallic = Resources::loadTexture(diffPath, info);
		}

		if (metallic) {
			//node->materials.at(0)->getDescriptors().getBinding("metallicTexture").setDescriptor(&metallic->getDescriptor());
			//node->materials.at(1)->getDescriptors().getBinding("metallicTexture").setDescriptor(&metallic->getDescriptor());
			//node->materials.at(2)->getDescriptors().getBinding("metallicTexture").setDescriptor(&metallic->getDescriptor());
		}
	}

	//material->dirty = true;

	return nullptr;
}

grail::Mesh * grail::MeshLoader::parseMesh(aiMesh * aMesh, const aiScene * scene, glm::vec3 & boundsMin, glm::vec3 & boundsMax, MemoryAllocator* stagingMemory, MemoryAllocator* dedicatedMemory) {
	//GRAIL_LOG(INFO, "MESH PARSE");

	std::vector<float> vertexBuffer;
	std::vector<uint32_t> indexBuffer;

	boundsMin.x = std::numeric_limits<float>::max();
	boundsMin.y = std::numeric_limits<float>::max();
	boundsMin.z = std::numeric_limits<float>::max();

	boundsMax.x = std::numeric_limits<float>::min();
	boundsMax.y = std::numeric_limits<float>::min();
	boundsMax.z = std::numeric_limits<float>::min();

	bool hasUvs = aMesh->mTextureCoords[0] ? true : false;
	bool hasNormals = aMesh->mNormals ? true : false;
	bool hasPositions = aMesh->mVertices ? true : false;
	bool hasTangents = aMesh->mTangents ? true : false;

	//GRAIL_LOG(INFO, "HM") << hasUvs << " " << hasNormals << " " << hasPositions << " " << ((aMesh->mTangents) ? "1" : "0");

	uint32_t elementCount = aMesh->mNumVertices * 12;
	vertexBuffer.resize(elementCount);

	uint32_t v_index = 0;
	for (unsigned int j = 0; j < aMesh->mNumVertices; ++j) {
		glm::vec3 pos = glm::vec3(aMesh->mVertices[j].x, aMesh->mVertices[j].y, aMesh->mVertices[j].z);

		if (boundsMin.x > pos.x) boundsMin.x = pos.x;
		if (boundsMin.y > pos.y) boundsMin.y = pos.y;
		if (boundsMin.z > pos.z) boundsMin.z = pos.z;

		if (boundsMax.x < pos.x) boundsMax.x = pos.x;
		if (boundsMax.y < pos.y) boundsMax.y = pos.y;
		if (boundsMax.z < pos.z) boundsMax.z = pos.z;

		vertexBuffer[v_index++] = (pos.x);
		vertexBuffer[v_index++] = (pos.y);
		vertexBuffer[v_index++] = (pos.z);

		//GRAIL_LOG(INFO, "NRML") << hasNormals;

		if (hasNormals) {
			vertexBuffer[v_index++] = (aMesh->mNormals[j].x);
			vertexBuffer[v_index++] = (aMesh->mNormals[j].y);
			vertexBuffer[v_index++] = (aMesh->mNormals[j].z);
		}
		else {
			vertexBuffer[v_index++] = (0);
			vertexBuffer[v_index++] = (0);
			vertexBuffer[v_index++] = (0);
		}

		if (hasTangents) {
			vertexBuffer[v_index++] = (aMesh->mTangents[j].x);
			vertexBuffer[v_index++] = (aMesh->mTangents[j].y);
			vertexBuffer[v_index++] = (aMesh->mTangents[j].z);
		}
		else {
			vertexBuffer[v_index++] = (0);
			vertexBuffer[v_index++] = (0);
			vertexBuffer[v_index++] = (0);
		}

		if (hasUvs) {
			vertexBuffer[v_index++] = (aMesh->mTextureCoords[0][j].x);
			vertexBuffer[v_index++] = (aMesh->mTextureCoords[0][j].y);
		} else {
			vertexBuffer[v_index++] = (0.01f * j);
			vertexBuffer[v_index++] = (0.01f * j);
		}

		vertexBuffer[v_index++] = (0);
	}

	for (unsigned int j = 0; j < aMesh->mNumFaces; ++j) {
		aiFace face = aMesh->mFaces[j];
		for (unsigned int k = 0; k < face.mNumIndices; ++k) { indexBuffer.push_back(face.mIndices[k]); }
	}

	////

	GPU mainGPU = VulkanContext::mainGPU;

	////

	Mesh* mesh = new Mesh();

	uint32_t vertexBufferSize = static_cast<uint32_t>(vertexBuffer.size()) * sizeof(float);
	uint32_t indexBufferSize = static_cast<uint32_t>(indexBuffer.size()) * sizeof(uint32_t);

	void* ptr;

	VkMemoryRequirements memReqs;

	MemoryAllocation stageMemoryRegionV;
	MemoryAllocation stageMemoryRegionI;

	MemoryAllocation localMemoryRegion;
	//MemoryRegionDescriptor localMemoryRegionI;

	VkBuffer vertexStagingBuffer;
	VkBuffer indexStagingBuffer;

	// VERTEX STAGE
	VkBufferCreateInfo bufferInfo = {};
	bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferInfo.size = vertexBufferSize;
	bufferInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
	VK_VALIDATE_RESULT(vkCreateBuffer(mainGPU.device, &bufferInfo, nullptr, &vertexStagingBuffer));

	vkGetBufferMemoryRequirements(mainGPU.device, vertexStagingBuffer, &memReqs);

	stageMemoryRegionV = stagingMemory->allocate(memReqs.size, memReqs.alignment, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, memReqs.memoryTypeBits);
	ptr = stageMemoryRegionV.mapMemory();
	{
		memcpy(ptr, vertexBuffer.data(), vertexBufferSize);
	}
	stageMemoryRegionV.unmapMemory();
	VK_VALIDATE_RESULT(vkBindBufferMemory(mainGPU.device, vertexStagingBuffer, stageMemoryRegionV.page->memoryHandle, stageMemoryRegionV.baseAddressAligned));

	// INDEX STAGE
	bufferInfo.size = indexBufferSize;
	VK_VALIDATE_RESULT(vkCreateBuffer(mainGPU.device, &bufferInfo, nullptr, &indexStagingBuffer));

	vkGetBufferMemoryRequirements(mainGPU.device, indexStagingBuffer, &memReqs);

	stageMemoryRegionI = stagingMemory->allocate(memReqs.size, memReqs.alignment, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, memReqs.memoryTypeBits);
	ptr = stageMemoryRegionI.mapMemory();
	{
		memcpy(ptr, indexBuffer.data(), indexBufferSize);
	}
	stageMemoryRegionI.unmapMemory();
	VK_VALIDATE_RESULT(vkBindBufferMemory(mainGPU.device, indexStagingBuffer, stageMemoryRegionI.page->memoryHandle, stageMemoryRegionI.baseAddressAligned));

	// MESH BUFFFER
	{
		VkMemoryRequirements memReqs;

		VkBufferCreateInfo globalBufferInfo = {};
		globalBufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
		globalBufferInfo.size = stageMemoryRegionV.size + stageMemoryRegionI.size;
		globalBufferInfo.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;

		VK_VALIDATE_RESULT(vkCreateBuffer(mainGPU.device, &globalBufferInfo, nullptr, &mesh->buffer));

		vkGetBufferMemoryRequirements(mainGPU.device, mesh->buffer, &memReqs);

		// GLOBAL MEM MAP
		localMemoryRegion = dedicatedMemory->allocate(stageMemoryRegionV.size + stageMemoryRegionI.size, stageMemoryRegionV.alignment, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, memReqs.memoryTypeBits);
		//localMemoryRegionI = dedicatedMemory->allocateRegion(stageMemoryRegionI.size, stageMemoryRegionI.alignment, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, memReqs.memoryTypeBits);

		VK_VALIDATE_RESULT(vkBindBufferMemory(mainGPU.device, mesh->buffer, localMemoryRegion.page->memoryHandle, localMemoryRegion.baseAddressAligned));
	}

	// COPY
	TemporaryCommandBuffer setupCommandBuffer = mainGPU.createTemporaryBuffer(mainGPU.transferQueue, VK_COMMAND_BUFFER_LEVEL_PRIMARY);
	setupCommandBuffer.begin();

	VkBufferCopy copyRegion = {};

	// Vertex buffer
	copyRegion.srcOffset = 0;
	copyRegion.dstOffset = 0;
	copyRegion.size = vertexBufferSize;
	vkCmdCopyBuffer(setupCommandBuffer.getHandle(), vertexStagingBuffer, mesh->buffer, 1, &copyRegion);

	// Index buffer
	copyRegion.srcOffset = 0;
	copyRegion.dstOffset = vertexBufferSize;
	copyRegion.size = indexBufferSize;
	vkCmdCopyBuffer(setupCommandBuffer.getHandle(), indexStagingBuffer, mesh->buffer, 1, &copyRegion);

	// Flushing the command buffer will also submit it to the queue and uses a fence to ensure that all commands have been executed before returning
	setupCommandBuffer.end();
	setupCommandBuffer.submit();
	setupCommandBuffer.dispose();
	// COPY 

	vkDestroyBuffer(mainGPU.device, vertexStagingBuffer, nullptr);
	vkDestroyBuffer(mainGPU.device, indexStagingBuffer, nullptr);

	mesh->indicesCount = static_cast<uint32_t>(indexBuffer.size());
	mesh->verticesCount = static_cast<uint32_t>(vertexBuffer.size());
	{
		//mesh->vertexRegionDescriptor = localMemoryRegionV;
		//mesh->indexRegionDescriptor = localMemoryRegionI;
	}
	mesh->vertexOffset = 0;
	mesh->indexOffset = vertexBufferSize;

	mesh->memoryRegion = localMemoryRegion;

	mesh->vertices = vertexBuffer;
	mesh->indices = indexBuffer;
	mesh->vertexBufferSize = vertexBufferSize;
	mesh->indexBufferSize = indexBufferSize;

	stagingMemory->clearAllPages();

	return mesh;
}

void grail::MeshLoader::loadMeshFromFile(std::string path, std::vector<MeshData> & data, VertexLayout * layout) {
	Assimp::Importer importer = Assimp::Importer();
	const aiScene* scene = importer.ReadFile(path,
		aiProcess_JoinIdenticalVertices |
		aiProcess_ValidateDataStructure |
		aiProcess_ImproveCacheLocality |
		aiProcess_RemoveRedundantMaterials |
		aiProcess_FindDegenerates |
		aiProcess_FindInvalidData |
		aiProcess_GenUVCoords |
		aiProcess_FlipUVs |
		aiProcess_FindInstances |
		aiProcess_OptimizeMeshes |
		aiProcess_OptimizeGraph |
		aiProcess_GenSmoothNormals |
		aiProcess_Triangulate |
		aiProcess_SortByPType |
		aiProcess_CalcTangentSpace
	);

	if ((!scene) || (scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE) || (!scene->mRootNode))
		throw new exceptions::RuntimeException("Could not load Model \"" + path + "\": " + importer.GetErrorString());

	data.resize(scene->mNumMeshes);

	for (unsigned int i = 0; i < scene->mNumMeshes; ++i) {
		aiMesh* mesh = scene->mMeshes[i];
		data[i].identifier = path + "/" + std::to_string(i);

		for (unsigned int j = 0; j < mesh->mNumVertices; ++j) {
			data[i].vertices.push_back(mesh->mVertices[j].x);
			data[i].vertices.push_back(mesh->mVertices[j].y);
			data[i].vertices.push_back(mesh->mVertices[j].z);

			data[i].vertices.push_back(mesh->mNormals[j].x);
			data[i].vertices.push_back(mesh->mNormals[j].y);
			data[i].vertices.push_back(mesh->mNormals[j].z);

			data[i].vertices.push_back(mesh->mTextureCoords[0][j].x);
			data[i].vertices.push_back(mesh->mTextureCoords[0][j].y);
		}

		for (unsigned int j = 0; j < mesh->mNumFaces; ++j) {
			aiFace face = mesh->mFaces[j];
			for (unsigned int k = 0; k < face.mNumIndices; ++k) { data[i].indices.push_back(face.mIndices[k]); }
		}
	}
}
