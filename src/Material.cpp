#include "Material.h"
#include "Shader.h"
#include "VkTexture.h"
#include "VkBuffer.h"
#include "Resources.h"
#include "Common.h"

/*grail::Material::Material(MaterialCreateInfo& createInfo) : vertexLayout(createInfo.layout), pipelineInformation(createInfo.pipelineInformation), group(createInfo.descriptorLayout) {

	if (!createInfo.pipelineFactory) {
		createInfo.pipelineFactory = new PipelineFactory();
	}

	this->descriptorLayout = createInfo.descriptorLayout;

	pipelineInformation.vertexLayout = vertexLayout;
	pipelineInformation.pipelineLayout = group.getPipelineLayout();


	if (createInfo.pipelineInformation.shaderStageState.get(ShaderStageBits::COMPUTE).module != VK_NULL_HANDLE) {
		pipeline = createInfo.pipelineFactory->createComputePipeline(pipelineInformation);
	}
	else {
		pipeline = createInfo.pipelineFactory->createPipeline(pipelineInformation);
	}

	this->shaders = createInfo.shaders;
}*/

/*grail::DescriptorLayout& grail::BaseMaterial::getDescriptorLayout() {
	return descriptorLayout;
}

std::string grail::BaseMaterial::getIdentifier() {
	return identifier;
}

grail::Material* grail::BaseMaterial::createPermutation(
	const std::string& name,
	GraphicsPipelineCreateInfo& createInfo,
	std::vector<ShaderPermutation*> permutations,
	VertexLayout* vertexLayout) {

	BufferBlock materialBlock;

	for (ShaderPermutation* p : permutations) {
		createInfo.shaderStageState.set(p->stage, p->getModule());
	
		if (p->hasUniformBuffer("material")) {
			materialBlock = p->getUniformBuffer("material");
		}
	}

	createInfo.vertexLayout = vertexLayout;
	createInfo.pipelineLayout = descriptorGroup.getPipelineLayout();

	Material* material = new Material();
	material->permutationName = name;
	material->shaders = permutations;
	material->vertexLayout = vertexLayout;
	material->pipeline = pipelineFactory->createPipeline(createInfo);
	material->base = this;

	if (materialBlock.size > 0) {
		material->memorySize = materialBlock.size;
		material->materialBlock = materialBlock;
	}

	materialPermutations[name] = material;
	return material;
}

grail::DescriptorGroup & grail::BaseMaterial::getDescriptorGroup() {
	return descriptorGroup;
}

grail::Material * grail::BaseMaterial::getPermutation(const std::string & name) {
	if (materialPermutations.find(name) != materialPermutations.end()) {
		return materialPermutations[name];
	}
	
	return nullptr;
}

grail::Material * grail::BaseMaterial::getFirstPermutation() {
	return materialPermutations.begin()->second;
}

void grail::BaseMaterial::setAvailablePermutations(std::vector<std::vector<std::string>>& vec) {
	this->availablePermutations = vec;
}

std::vector<std::vector<std::string>>& grail::BaseMaterial::getAvailablePermutations() {
	return availablePermutations;
}

void grail::BaseMaterial::setMemoryDefault(const std::string& name, std::vector<float> mem) {
	if (memoryDefaults.find(name) == memoryDefaults.end()) {
		memoryDefaults[name] = std::vector<float>();
	}

	memoryDefaults[name] = mem;
}

grail::BaseMaterial::BaseMaterial(const std::string & name, std::vector<Shader*> shaders, DescriptorLayout descriptorLayout)
: descriptorGroup(descriptorLayout) {
	this->pipelineFactory = new PipelineFactory();
	this->descriptorLayout = descriptorLayout;

	this->baseShaders = shaders;

	this->identifier = name;
	this->descriptorGroup.initialize(descriptorLayout);
	for (uint32_t i = 0; i < descriptorGroup.getDescriptorCount(); i++) {
		for (uint32_t j = 0; j < descriptorGroup.getSet(i).getBindingCount(); j++) {
			DescriptorBinding& binding = descriptorGroup.getSet(i).getBinding(j);
			if (binding.getResourceType() == DescriptorResourceType::IMAGE) {
				VkTexture* texture = Resources::getTexture("red");
				binding.setBinding(texture, &texture->getDescriptor());
			}

			else if (binding.getResourceType() == DescriptorResourceType::BUFFER) {
				Buffer* buffer = Resources::getBuffer("camera");
				binding.setBinding(buffer, &buffer->getDescriptor(0));
			}
		}
	}

	this->materialMemory = new MemoryRegion(VulkanContext::mainGPU, name + "_memory", 12);
}

grail::BaseMaterial * grail::Material::getBase() {
	return base;
}

VkPipeline grail::Material::getPipeline() {
	return pipeline;
}

grail::VertexLayout * grail::Material::getVertexLayout() {
	return vertexLayout;
}

std::string grail::Material::getIdentifier() {
	return permutationName;
}

grail::MaterialInstance::MaterialInstance(Material * material) {
	this->baseMaterial = material;

	this->descriptorGroups.resize(Graphics::getPrerenderedFrameCount());

	for (uint32_t g = 0; g < Graphics::getPrerenderedFrameCount(); g++) {
		DescriptorGroup& descriptorGroup = this->descriptorGroups[g];

		descriptorGroup.initialize(material->getBase()->getDescriptorLayout());
	}

	DescriptorGroup& descriptorGroup = material->getBase()->getDescriptorGroup();
	for (uint32_t i = 0; i < descriptorGroup.getDescriptorCount(); i++) {
		for (uint32_t j = 0; j < descriptorGroup.getSet(i).getBindingCount(); j++) {
			DescriptorBinding& srcBinding = material->getBase()->getDescriptorGroup().getSet(i).getBinding(j);

			setDescriptorBinding(i, j, srcBinding.getResource(), srcBinding.getDescriptor());
		}
	}

	if (material->memorySize > 0) {
		BufferCreateInfo info = {};
		info.usageBits = BufferUsageBits::UNIFORM_BUFFER_BIT;
		info.memoryProperties = MemoryPropertyFlags::HOST_VISIBLE | MemoryPropertyFlags::HOST_COHERENT;
		info.size = material->memorySize;
		info.memoryType = MemoryType::STATIC;

		memoryBuffer = new Buffer(info, material->base->materialMemory, nullptr, 0);
		memoryBuffer->map();

		float* base = static_cast<float*>(memoryBuffer->getMappedPtr());
		for (BufferBlockShaderVar& var : material->materialBlock.variables) {
			if (material->base->memoryDefaults.find(var.name) != material->base->memoryDefaults.end()) {
				float* location = base + (var.offset / sizeof(float));

				memcpy(location, material->base->memoryDefaults[var.name].data(), var.size);
			}
		}

		for (uint32_t g = 0; g < Graphics::getPrerenderedFrameCount(); g++) {
			DescriptorGroup& descriptorGroup = this->descriptorGroups[g];
			
			descriptorGroup.getBinding("material").setBinding(memoryBuffer, &memoryBuffer->getDescriptor());
		}
	}

	updateAllDescriptors();

	pipelineCache.resize(10);
}*/

/*void grail::MaterialInstance::setBaseMaterial(Material * material) {
	this->baseMaterial = material;
}

grail::Material * grail::MaterialInstance::getBaseMaterial() {
	return baseMaterial;
}

VkPipeline grail::MaterialInstance::getPipeline() {
	return baseMaterial->getPipeline();
}

VkPipeline grail::MaterialInstance::getPipeline(const std::string & permutation) {
	return baseMaterial->getBase()->getPermutation(permutation)->getPipeline();
}

grail::VertexLayout * grail::MaterialInstance::getVertexLayout() {
	return baseMaterial->getVertexLayout();
}

grail::DescriptorGroup & grail::MaterialInstance::getCurrentFrameDescriptor() {
	return descriptorGroups[Graphics::getFrameIndex()];
}

void grail::MaterialInstance::setDescriptorBinding(uint32_t set, uint32_t binding, void * resource, void * descriptor) {
	uint32_t groupIndex = 0;
	for (DescriptorGroup& group : descriptorGroups) {
		DescriptorBinding& b = group.getSet(set).getBinding(binding);

		if (b.getResourceType() == DescriptorResourceType::BUFFER) {
			Buffer* buffer = reinterpret_cast<Buffer*>(resource);
			b.setBinding(resource, &buffer->getDescriptor(groupIndex));
		}
		else {
			b.setBinding(resource, descriptor);
		}

		groupIndex++;
	}
}

void grail::MaterialInstance::setDescriptorBinding(const std::string & identifier, void * resource, void * descriptor) {
	uint32_t groupIndex = 0;
	for (DescriptorGroup& group : descriptorGroups) {
		DescriptorBinding& b = group.getBinding(identifier);

		if (b.getResourceType() == DescriptorResourceType::BUFFER) {
			Buffer* buffer = reinterpret_cast<Buffer*>(resource);
			b.setBinding(resource, &buffer->getDescriptor(groupIndex));
		}
		else {
			b.setBinding(resource, descriptor);
		}

		groupIndex++;
	}
}

void grail::MaterialInstance::updateDescriptors() {
	uint32_t nextFrame = (Graphics::getFrameIndex() + 1) % Graphics::getPrerenderedFrameCount();

	descriptorGroups[nextFrame].update();
}

void grail::MaterialInstance::updateAllDescriptors() {
	for (DescriptorGroup& group : descriptorGroups) {
		group.update();
	}
}

void grail::MaterialInstance::copyDescriptorsTo(MaterialInstance * instance) {
	for (uint32_t g = 0; g < Graphics::getPrerenderedFrameCount(); g++) {
		DescriptorGroup& srcGroup = descriptorGroups[g];
		DescriptorGroup& dstGroup = instance->descriptorGroups[g];

		for (uint32_t i = 0; i < srcGroup.getDescriptorCount(); i++) {
			for (uint32_t j = 0; j < srcGroup.getSet(i).getBindingCount(); j++) {

				DescriptorBinding& srcDescriptor = srcGroup.getSet(i).getBinding(j);
				DescriptorBinding& dstDescriptor = dstGroup.getSet(i).getBinding(j);

				if(srcDescriptor.getIdentifier().compare("material") != 0)
					dstDescriptor.copyBinding(srcDescriptor);
			}
		}
	}

	instance->updateAllDescriptors();
}

/*grail::DescriptorGroup & grail::MaterialInstance::getDescriptors() {
	return descriptorGroups[Graphics::getFrameIndex()];
}*/

/*VkPipeline grail::MaterialInstance::getCachedPipeline(uint32_t id) {
	return pipelineCache[id];
}

void grail::MaterialInstance::cachePipeline(const std::string & permutation, uint32_t id) {
	pipelineCache[id] = getPipeline(permutation);
}

void grail::MaterialInstance::clearCache() {
	pipelineCache.clear();
	pipelineCache.resize(10);
}

grail::BufferBlock& grail::MaterialInstance::getMaterialMemoryBlock() {
	return baseMaterial->materialBlock;
}

void* grail::MaterialInstance::getMaterialMemoryPointer() {
	return memoryBuffer->getMappedPtr();
}
*/
grail::MaterialDescription::MaterialDescription(const std::string& name) {
	this->identifier = name;

	this->pipelineFactory = new PipelineFactory();

	//this->materialMemoryRegion = new MemoryAllocator(VulkanContext::mainGPU, name + " material memory", 16*1024*1024);
}

std::string grail::MaterialDescription::getIdentifier() {
	return identifier;
}

grail::Material* grail::MaterialDescription::getBaseMaterial() {
	return derivedMaterials[0];
}

grail::Material* grail::MaterialDescription::getMaterial(uint64_t key) {
	return derivedMaterials[key];
}

bool grail::MaterialDescription::materialExists(uint64_t key) {
	return !(derivedMaterials.find(key) == derivedMaterials.end());
}

grail::Material* grail::MaterialDescription::createMaterial(std::vector<MaterialType>& types, std::vector<std::string>& options, VertexLayout vertexLayout, DescriptorLayout descriptorLayout, std::vector<ShaderPermutation*> shaderPermutations, std::vector<GraphicsPipelineCreateInfo>& pipelineInfos) {
	// Initialize the material and store it in the map based on the options
	
	Material* material = new Material(vertexLayout);
	material->description = this;
	material->vertexLayout = vertexLayout;
	material->descriptorLayout = descriptorLayout;
	material->shaders = shaderPermutations;

	uint64_t key = 0;

	if (options.size() > 0) {
		for (std::string& option : options) {
			uint64_t val = optionIndices[option];

			key |= val;
		}
	}

	material->key = key;
	derivedMaterials[key] = material;

	// Now we create the required pipelines
	// NOTE THIS NEEDS TO BE FIXED AS SOON AS EVERYTHING ELSE WORKS
	DescriptorGroup temp = DescriptorGroup(descriptorLayout);

	std::vector<MaterialType> derivedTypes;
	for (MaterialType type : types) {
		if (type == MaterialType::SHADOW) {
			derivedTypes.push_back(MaterialType::SHADOW_CASCADE_0);
			derivedTypes.push_back(MaterialType::SHADOW_CASCADE_1);
			derivedTypes.push_back(MaterialType::SHADOW_CASCADE_2);
			derivedTypes.push_back(MaterialType::SHADOW_CASCADE_3);
		}
		else {
			derivedTypes.push_back(type);
		}
	}
	material->pipelines.resize(derivedTypes.size());

	uint32_t shaderIndex = 0;
	uint32_t pipelineIndex = 0;
	for (MaterialType type : derivedTypes) {
		ShaderPermutation* vertex = shaderPermutations[shaderIndex];
		ShaderPermutation* fragment = shaderPermutations[shaderIndex + material->pipelines.size()];
		shaderIndex++;

		if (vertex->hasUniformBuffer("material") && fragment->hasUniformBuffer("material")) {
			BufferBlock vm = vertex->getUniformBuffer("material");
			BufferBlock fm = vertex->getUniformBuffer("material");

			if (vm.binding == fm.binding &&
				vm.memberCount == fm.memberCount &&
				vm.set == fm.set &&
				vm.size == fm.size) {

				material->materialBlock = fm;
			}
			else {
				throw new exceptions::RuntimeException("Material block missmatch between shaders");
			}
		}
		else {
			if (!vertex->hasUniformBuffer("material") && !vertex->hasUniformBuffer("material")) {
			
			}
			else {
				throw new exceptions::RuntimeException("All or none of the shaders are required to have a material block");
			}
		}

		GraphicsPipelineCreateInfo& pipelineInfo = pipelineInfos[pipelineIndex++];

		pipelineInfo.shaderStageState.set(vertex->stage, vertex->getModule());
		pipelineInfo.shaderStageState.set(fragment->stage, fragment->getModule());
		pipelineInfo.vertexLayout = &vertexLayout;
		pipelineInfo.pipelineLayout = temp.getPipelineLayout();

		material->pipelines[static_cast<uint32_t>(type)] = pipelineFactory->createPipeline(pipelineInfo);
	}

	return material;
}

grail::Material* grail::MaterialDescription::createMaterial(VertexLayout vertexLayout, DescriptorLayout descriptorLayout, std::vector<ShaderPermutation*> shaderPermutations, GraphicsPipelineCreateInfo& pipelineInfo) {
	std::vector<MaterialType> types = { MaterialType::GEOMETRY };
	std::vector<std::string> options = {};
	std::vector<GraphicsPipelineCreateInfo> pipelineInfos;
	pipelineInfos.push_back(pipelineInfo);

	return createMaterial(types, options, vertexLayout, descriptorLayout, shaderPermutations, pipelineInfos);
}

grail::Material* grail::MaterialDescription::createComputeMaterial(DescriptorLayout descriptorLayout, std::vector<ShaderPermutation*> shaderPermutations, GraphicsPipelineCreateInfo& pipelineInfo) {
	Material* material = new Material();
	material->description = this;
	material->descriptorLayout = descriptorLayout;
	material->shaders = shaderPermutations;


	// Compute materials cant have permutations for now 
	uint64_t key = 0;
	material->key = key;
	derivedMaterials[key] = material;

	// Now we create the required pipelines
	// NOTE THIS NEEDS TO BE FIXED AS SOON AS EVERYTHING ELSE WORKS
	DescriptorGroup temp = DescriptorGroup(descriptorLayout);
	material->pipelines.resize(1);

	uint32_t shaderIndex = 0;
	uint32_t pipelineIndex = 0;

	pipelineInfo.pipelineLayout = temp.getPipelineLayout();

	material->pipelines[static_cast<uint32_t>(MaterialType::GEOMETRY)] = pipelineFactory->createComputePipeline(pipelineInfo);

	return material;
}

std::string grail::MaterialDescription::getResourceDefault(const std::string& id) {
	if (resourceDefaults.find(id) != resourceDefaults.end()) {
		return resourceDefaults[id];
	}
	
	return "";
}

grail::DescriptorGroup& grail::MaterialInstance::getCurrentFrameDescriptor() {
	return descriptorGroups[Graphics::getFrameIndex()];
}

void grail::MaterialInstance::setDescriptorBinding(uint32_t set, uint32_t binding, void* resource, void* descriptor) {
	uint32_t groupIndex = 0;
	for (DescriptorGroup& group : descriptorGroups) {
		DescriptorBinding& b = group.getSet(set).getBinding(binding);

		if (b.getResourceType() == DescriptorResourceType::BUFFER) {
			Buffer* buffer = reinterpret_cast<Buffer*>(resource);
			b.setBinding(resource, &buffer->getDescriptor(groupIndex));
		}
		else {
			b.setBinding(resource, descriptor);
		}

		groupIndex++;
	}
}

void grail::MaterialInstance::setDescriptorBinding(const std::string& identifier, void* resource, void* descriptor) {
	uint32_t groupIndex = 0;
	for (DescriptorGroup& group : descriptorGroups) {
		DescriptorBinding* b = group.tryGetBinding(identifier);

		if (!b) return;

		if (b->getResourceType() == DescriptorResourceType::BUFFER) {
			Buffer* buffer = reinterpret_cast<Buffer*>(resource);
			b->setBinding(resource, &buffer->getDescriptor(groupIndex));
		}
		else {
			b->setBinding(resource, descriptor);
		}

		groupIndex++;
	}
}

void grail::MaterialInstance::setDescriptorTexture(const std::string& identifier, VkTexture* texture) {
	setDescriptorBinding(identifier, texture, &texture->getDescriptor());
}

void grail::MaterialInstance::updateDescriptors() {
	uint32_t nextFrame = (Graphics::getFrameIndex()) % Graphics::getPrerenderedFrameCount();

	descriptorGroups[nextFrame].update();
}

void grail::MaterialInstance::updateAllDescriptors() {
	for (DescriptorGroup& group : descriptorGroups) {
		group.update();
	}
}

VkPipeline grail::MaterialInstance::getPipeline() {
	return baseMaterial->getPipeline();
}

VkPipeline grail::MaterialInstance::getPipeline(MaterialType type) {
	return baseMaterial->getPipeline(type);
}

grail::Material* grail::MaterialInstance::getBaseMaterial() {
	return baseMaterial;
}

grail::MaterialDescription* grail::MaterialInstance::getMaterialDescription() {
	return baseDescription;
}

void grail::MaterialInstance::copyFrom(MaterialInstance* src) {
	if (src->baseDescription != baseDescription) {
		return;
	}

	// Copy all descriptors with matching names and types
	for (uint32_t g = 0; g < Graphics::getPrerenderedFrameCount(); g++) {
		DescriptorGroup& srcGroup = src->descriptorGroups[g];
		DescriptorGroup& dstGroup = descriptorGroups[g];

		for (uint32_t i = 0; i < dstGroup.getDescriptorCount(); i++) {
			for (uint32_t j = 0; j < dstGroup.getSet(i).getBindingCount(); j++) {
				DescriptorBinding& dstDescriptor = dstGroup.getSet(i).getBinding(j);

				if (dstDescriptor.getIdentifier().compare("material") == 0)
					continue;

				DescriptorBinding* srcDescriptor = srcGroup.tryGetBinding(dstDescriptor.getIdentifier());
				if (srcDescriptor) {
					dstDescriptor.copyBinding(*srcDescriptor);
				}
			}
		}
	}

	// Copy as much matching material memory as possible
	if (this->memoryBuffer && src->getMemoryPtr()) {
		float* srcBase = static_cast<float*>(src->getMemoryPtr());
		float* dstBase = static_cast<float*>(getMemoryPtr());

		for (BufferBlockShaderVar& srcVar : src->getMemoryBlockDescription().variables) {
			for (BufferBlockShaderVar& dstVar : getMemoryBlockDescription().variables) {
				if (srcVar.name.compare(dstVar.name) == 0 &&
					srcVar.baseType == dstVar.baseType &&
					srcVar.columns == dstVar.columns &&
					srcVar.rows == dstVar.rows &&
					srcVar.size == dstVar.size &&
					srcVar.type == dstVar.type) {

					memcpy(dstBase + (dstVar.offset / sizeof(float)), srcBase + (srcVar.offset / sizeof(float)), dstVar.size);
				}
			}
		}
	}

	this->updateAllDescriptors();
}

void* grail::MaterialInstance::getMemoryPtr() {
	if (memoryBuffer) {
		return memoryBuffer->getMappedPtr();
	}
	
	return nullptr;
}

grail::BufferBlock& grail::MaterialInstance::getMemoryBlockDescription() {
	return baseMaterial->getMemoryBlockDescription();
}

void grail::MaterialInstance::setFloat(const std::string& name, float value) {
	uint32_t size = 0;
	float* ptr = reinterpret_cast<float*>(getMemoryPtrOfVariable(name, size));

	if (ptr) {
		*ptr = value;
	}
}

void grail::MaterialInstance::setVec2(const std::string& name, glm::vec2 value) {
	uint32_t size = 0;
	uint32_t actualSize = sizeof(glm::vec2);
	float* ptr = reinterpret_cast<float*>(getMemoryPtrOfVariable(name, size));

	if (ptr) {
		memcpy(ptr, &value[0], glm::min(actualSize, size));
	}
}

void grail::MaterialInstance::setVec2(const std::string& name, float value1, float value2) {
	setVec2(name, glm::vec2(value1, value2));
}

void grail::MaterialInstance::setVec3(const std::string& name, glm::vec3 value) {
	uint32_t size = 0;
    uint32_t actualSize = sizeof(glm::vec3);
	float* ptr = reinterpret_cast<float*>(getMemoryPtrOfVariable(name, size));

	if (ptr) {
		memcpy(ptr, &value[0], glm::min(actualSize, size));
	}
}

void grail::MaterialInstance::setVec3(const std::string& name, float value1, float value2, float value3) {
	setVec3(name, glm::vec3(value1, value2, value3));
}

void grail::MaterialInstance::setVec4(const std::string& name, glm::vec4 value) {
	uint32_t size = 0;
    uint32_t actualSize = sizeof(glm::vec4);
	float* ptr = reinterpret_cast<float*>(getMemoryPtrOfVariable(name, size));

	if (ptr) {
		memcpy(ptr, &value[0], glm::min(actualSize, size));
	}
}

void grail::MaterialInstance::setVec4(const std::string& name, float value1, float value2, float value3, float value4) {
	setVec4(name, glm::vec4(value1, value2, value3, value4));
}

void grail::MaterialInstance::setMat3(const std::string& name, glm::mat3 value) {
	uint32_t size = 0;
    uint32_t actualSize = sizeof(glm::mat3);
	float* ptr = reinterpret_cast<float*>(getMemoryPtrOfVariable(name, size));

	if (ptr) {
		memcpy(ptr, glm::value_ptr(value), glm::min(actualSize, size));
	}
}

void grail::MaterialInstance::setMat4(const std::string& name, glm::mat4 value) {
	uint32_t size = 0;
    uint32_t actualSize = sizeof(glm::mat4);
	float* ptr = reinterpret_cast<float*>(getMemoryPtrOfVariable(name, size));

	if (ptr) {
		memcpy(ptr, glm::value_ptr(value), glm::min(actualSize, size));
	}
}

void grail::MaterialInstance::dispose() {
	if (memoryBuffer) {
		memoryBuffer->dispose();
		delete memoryBuffer;
	}


	for (DescriptorGroup& group : descriptorGroups) {
		group.dispose();
	}
}

VkPipeline grail::Material::getPipeline() {
	return pipelines[static_cast<uint32_t>(MaterialType::GEOMETRY)];
}

VkPipeline grail::Material::getPipeline(MaterialType type) {
	return pipelines[static_cast<uint32_t>(type)];
}

grail::MaterialInstance* grail::Material::createInstance() {
	MaterialInstance* instance = new MaterialInstance();
	instance->baseDescription = this->description;
	instance->baseMaterial = this;

	if (materialBlock.size > 0) {
		BufferCreateInfo info = {};
		info.usageBits = BufferUsageBits::UNIFORM_BUFFER_BIT;
		info.memoryProperties = MemoryPropertyFlags::HOST_VISIBLE | MemoryPropertyFlags::HOST_COHERENT;
		info.size = materialBlock.size;
		info.memoryType = MemoryType::STATIC;

		instance->memoryBuffer = new Buffer(info, Resources::getUniformBufferMemoryAllocator(), nullptr, 0);
		instance->memoryBuffer->map();

		float* base = static_cast<float*>(instance->memoryBuffer->getMappedPtr());
		for (BufferBlockShaderVar& var : materialBlock.variables) {
			if (description->memoryDescriptions.find(var.name) != description->memoryDescriptions.end()) {
				float* location = base + (var.offset / sizeof(float));

				memcpy(location, description->memoryDescriptions[var.name].defaultValues.data(), var.size);
			}
		}
	}

	instance->descriptorGroups.resize(Graphics::getPrerenderedFrameCount());
	for (uint32_t g = 0; g < Graphics::getPrerenderedFrameCount(); g++) {
		DescriptorGroup& descriptorGroup = instance->descriptorGroups[g];

		descriptorGroup.initialize(descriptorLayout);
	}

	for (uint32_t g = 0; g < Graphics::getPrerenderedFrameCount(); g++) {
		DescriptorGroup& descriptorGroup = instance->descriptorGroups[g];

		for (uint32_t i = 0; i < descriptorGroup.getDescriptorCount(); i++) {
			for (uint32_t j = 0; j < descriptorGroup.getSet(i).getBindingCount(); j++) {
				DescriptorBinding& binding = descriptorGroup.getSet(i).getBinding(j);

				if (binding.getResourceType() == DescriptorResourceType::IMAGE) {
					std::string id = description->getResourceDefault(binding.getIdentifier());

					if (id.length() == 0) {
						id = "red";
					}

					VkTexture* texture = Resources::getTexture(id);

					binding.setBinding(texture, &texture->getDescriptor());
				}

				else if (binding.getResourceType() == DescriptorResourceType::BUFFER) {
					if (materialBlock.size > 0) {
						if (binding.getIdentifier().compare("material") == 0) {
							binding.setBinding(instance->memoryBuffer, &instance->memoryBuffer->getDescriptor(g));
							continue;
						}
					}

					std::string id = description->getResourceDefault(binding.getIdentifier());

					if (id.length() == 0) {
						id = "camera";
					}

					Buffer* buffer = Resources::getBuffer(id);
					binding.setBinding(buffer, &buffer->getDescriptor(g));
				}
			}
		}
	}

	instance->updateAllDescriptors();

	return instance;
}

uint64_t grail::Material::getKey() {
	return key;
}

grail::BufferBlock& grail::Material::getMemoryBlockDescription() {
	return materialBlock;
}

grail::Material::Material(grail::VertexLayout vertexLayout) :
vertexLayout(vertexLayout) {

}

grail::Material::Material() {

}

grail::MaterialInstance::MaterialInstance() {

}

void* grail::MaterialInstance::getMemoryPtrOfVariable(const std::string& name, uint32_t& varSize) {
	BufferBlockShaderVar* var = getMemoryBlockDescription().getVarByName(name);

	if (var) {
		void* ptr = getMemoryPtr();
		
		if (ptr) {
			varSize = var->size;
			return reinterpret_cast<void*>(reinterpret_cast<unsigned char*>(ptr) + var->offset);
		}
	}

	return nullptr;
}
