#include "Camera.h"

grail::Camera::Camera() {
	this->position = glm::vec3(0.0f, 0.0f, 0.0f);
	this->direction = glm::vec3(0.0f, 0.0f, 1.0f);
	this->up = glm::vec3(0.0f, -1.0f, 0.0f);

	this->projection = glm::mat4(1.0f);
	this->view = glm::mat4(1.0f);
	this->combined = glm::mat4(1.0f);
	this->invProjectionView = glm::mat4(1.0f);

	this->frustum = Frustum();
}

void grail::Camera::normalizeUp() {
	glm::vec3 temp;
	temp.x = direction.x;
	temp.y = direction.y;
	temp.z = direction.z;
	temp = glm::cross(temp, up);
	temp = glm::normalize(temp);

	up.x = temp.x;
	up.y = temp.y;
	up.z = temp.z;
	up = glm::cross(up, direction);

	up = glm::normalize(up);
}

void grail::Camera::lookAt(float x, float y, float z) {
	glm::vec3 temp;
	temp.x = x;
	temp.y = y;
	temp.z = z;
	temp = temp - position;
	temp = glm::normalize(temp);


	if (!(temp.x == 0 && temp.y == 0 && temp.z == 0)) {
		float dot = glm::dot(temp, up);
		if (std::abs(dot - 1) < 0.000000001f) {
			up.x = direction.x;
			up.y = direction.y;
			up.z = direction.z;
			up = up * -1.0f;
		}
		else if (std::abs(dot + 1) < 0.000000001f) {
			up.x = direction.x;
			up.y = direction.y;
			up.z = direction.z;
		}
		direction.x = temp.x;
		direction.y = temp.y;
		direction.z = temp.z;

		//normalizeUp();
	}
}

void grail::Camera::lookAt(const glm::vec3 & target) {
	lookAt(target.x, target.y, target.z);
}

void grail::Camera::translate(float x, float y, float z) {
	position.x += x;
	position.y += y;
	position.z += z;
}

void grail::Camera::translate(const glm::vec3 & translation) {
	translate(translation.x, translation.y, translation.z);
}

glm::vec3 grail::Camera::unprojectViewport(glm::vec3 screenCoords, float viewportX, float viewportY, float viewportWidth, float viewportHeight) {
	return glm::unProject(screenCoords, view, projection, glm::vec4(viewportX, viewportY, viewportWidth, viewportHeight));
}

glm::vec3 grail::Camera::unproject(glm::vec3 screenCoords){
	return unprojectViewport(screenCoords, 0, 0, viewportWidth, viewportHeight);
}


glm::vec3 grail::Camera::projectViewport(glm::vec3 worldCoords, float viewportX, float viewportY, float viewportWidth, float viewportHeight) {
	return glm::project(worldCoords, view, projection, glm::vec4(viewportX, viewportY, viewportWidth, viewportHeight));
}

glm::vec3 grail::Camera::project(glm::vec3 worldCoords) {
	return projectViewport(worldCoords, 0, 0, viewportWidth, viewportHeight);
}