#include "VR.h"
#include "Logger.h"
#include <vector>

#ifdef OS_NOT_SUPPORTED
vr::IVRSystem* grail::VR::hmd = nullptr;
bool grail::VR::initialized = false;
vr::TrackedDevicePose_t grail::VR::trackedDevicePose[vr::k_unMaxTrackedDeviceCount];
glm::mat4 grail::VR::devicePose[vr::k_unMaxTrackedDeviceCount];
glm::mat4 grail::VR::hmdPose;

#define VR_CHECK_ERROR(err) {																																				    \
    vr::EVRInitError r = err; 																																									\
	if (r != vr::VRInitError_None) {																																		\
        char buf[1024];                                                                                                                                                     \
        int size = sprintf_s(buf, sizeof(buf), "Unable to init VR runtime: %s", vr::VR_GetVRInitErrorAsEnglishDescription(r));                                                            \
		GRAIL_LOG(ERROR, "VR_CHECK_ERROR") << "On line: " << __LINE__ << " In: " << __FILE__ << " (" << std::string(buf, size)  << ")";															\
	}																																										\
}

#endif

bool grail::VR::isHMDPresent() {
#ifdef OS_NOT_SUPPORTED
    return vr::VR_IsHmdPresent();
#else
    return false;
#endif
}

void grail::VR::initialize() {
#ifdef OS_NOT_SUPPORTED
    if (isHMDPresent()) {
        GRAIL_LOG(INFO, "VR") << "Initializing VR...";

        vr::EVRInitError error = vr::VRInitError_None;
        hmd = vr::VR_Init(&error, vr::VRApplication_Scene);

        if (error != vr::VRInitError_None) {
            char buf[1024];
            int size = sprintf_s(buf, sizeof(buf), "Unable to init VR runtime: %s", vr::VR_GetVRInitErrorAsEnglishDescription(error));
            GRAIL_LOG(ERROR, "VR_INIT") << "On line: " << __LINE__ << " In: " << __FILE__ << " (" << std::string(buf, size) << ")";
            return;
        }

        if (!vr::VRCompositor()) {
            GRAIL_LOG(ERROR, "VR_INIT") << "On line: " << __LINE__ << " In: " << __FILE__ << " (Failed to initialize VR Compositor!)";
            
            if (hmd) {
                vr::VR_Shutdown();
                return;
            }
        }

        initialized = true;

        {
            std::vector<std::string> outInstanceExtensionList;
            uint32_t nBufferSize = vr::VRCompositor()->GetVulkanInstanceExtensionsRequired(nullptr, 0);
            if (nBufferSize > 0)
            {
                // Allocate memory for the space separated list and query for it
                char* pExtensionStr = new char[nBufferSize];
                pExtensionStr[0] = 0;
                vr::VRCompositor()->GetVulkanInstanceExtensionsRequired(pExtensionStr, nBufferSize);

                // Break up the space separated list into entries on the CUtlStringList
                std::string curExtStr;
                uint32_t nIndex = 0;
                while (pExtensionStr[nIndex] != 0 && (nIndex < nBufferSize))
                {
                    if (pExtensionStr[nIndex] == ' ')
                    {
                        outInstanceExtensionList.push_back(curExtStr);
                        curExtStr.clear();
                    }
                    else
                    {
                        curExtStr += pExtensionStr[nIndex];
                    }
                    nIndex++;
                }
                if (curExtStr.size() > 0)
                {
                    outInstanceExtensionList.push_back(curExtStr);
                }

                delete[] pExtensionStr;
            }


            for(std::string e : outInstanceExtensionList) {
                GRAIL_LOG(INFO, "INSTANCE EXTENSIONS") << e;
            }
        }
    }
#endif
}

bool grail::VR::isInitialized() {
#ifdef OS_NOT_SUPPORTED
    return initialized;
#else
    return false;
#endif
}

glm::mat4 grail::VR::getHMDPose() {
#ifdef OS_NOT_SUPPORTED
    return hmdPose;
#else
    return glm::mat4(1.0);
#endif
}

glm::mat4 grail::VR::getProjectionForEye(HMD_Eye eye, float nearPlane, float farPlane) {
#ifdef OS_NOT_SUPPORTED
    if (!isInitialized()) return glm::mat4(1.0);

    vr::HmdMatrix44_t mat = hmd->GetProjectionMatrix(static_cast<vr::Hmd_Eye>(eye), nearPlane, farPlane);

    return glm::mat4(
        mat.m[0][0], mat.m[1][0], mat.m[2][0], mat.m[3][0],
        mat.m[0][1], mat.m[1][1], mat.m[2][1], mat.m[3][1],
        mat.m[0][2], mat.m[1][2], mat.m[2][2], mat.m[3][2],
        mat.m[0][3], mat.m[1][3], mat.m[2][3], mat.m[3][3]
    );
#else
    return glm::mat4(1.0);
#endif
}

glm::mat4 grail::VR::getPoseForEye(HMD_Eye eye) {
#ifdef OS_NOT_SUPPORTED
    if (!isInitialized()) return glm::mat4(1.0);

    vr::HmdMatrix34_t matEyeRight = hmd->GetEyeToHeadTransform(static_cast<vr::Hmd_Eye>(eye));

    glm::mat4 matrixObj(
        matEyeRight.m[0][0], matEyeRight.m[1][0], matEyeRight.m[2][0], 0.0,
        matEyeRight.m[0][1], matEyeRight.m[1][1], matEyeRight.m[2][1], 0.0,
        matEyeRight.m[0][2], matEyeRight.m[1][2], matEyeRight.m[2][2], 0.0,
        matEyeRight.m[0][3], matEyeRight.m[1][3], matEyeRight.m[2][3], 1.0f
    );

    return glm::inverse(matrixObj);
#else
    return glm::mat4(1.0);
#endif
}

void grail::VR::getPreferedRenderSize(uint32_t* w, uint32_t* h){
#ifdef OS_NOT_SUPPORTED
    if (isInitialized()) return;

    hmd->GetRecommendedRenderTargetSize(w, h);
#else
    *w = 0;
    *h = 0;
#endif
}

void grail::VR::processEvents() {
#ifdef OS_NOT_SUPPORTED
    if (!isInitialized()) return;

    vr::VREvent_t event;
    while (hmd->PollNextEvent(&event, sizeof(event))) {
        switch (event.eventType) {
        case vr::VREvent_TrackedDeviceActivated:
            GRAIL_LOG(INFO, "VR") << "Device: " << event.trackedDeviceIndex << " activated";
        break;
        case vr::VREvent_TrackedDeviceDeactivated:
            GRAIL_LOG(INFO, "VR") << "Device: " << event.trackedDeviceIndex << " deactivated";
        break;
        case vr::VREvent_TrackedDeviceUpdated:
            GRAIL_LOG(INFO, "VR") << "Device: " << event.trackedDeviceIndex << " updated";
        break;
        }
    }
#endif
}

#ifdef OS_NOT_SUPPORTED
glm::mat4 convertSteamVRMatrixToMatrix4(const vr::HmdMatrix34_t& matPose) {
    glm::mat4 matrixObj(
        matPose.m[0][0], matPose.m[1][0], matPose.m[2][0], 0.0,
        matPose.m[0][1], matPose.m[1][1], matPose.m[2][1], 0.0,
        matPose.m[0][2], matPose.m[1][2], matPose.m[2][2], 0.0,
        matPose.m[0][3], matPose.m[1][3], matPose.m[2][3], 1.0f
    );
    return matrixObj;
}
#endif

void grail::VR::updatePoses() {
#ifdef OS_NOT_SUPPORTED
    if (!isInitialized()) return;

    vr::VRCompositor()->WaitGetPoses(trackedDevicePose, vr::k_unMaxTrackedDeviceCount, NULL, 0);

    for (int nDevice = 0; nDevice < vr::k_unMaxTrackedDeviceCount; ++nDevice) {
        if (trackedDevicePose[nDevice].bPoseIsValid) {
            devicePose[nDevice] = convertSteamVRMatrixToMatrix4(trackedDevicePose[nDevice].mDeviceToAbsoluteTracking);
            /*if (m_rDevClassChar[nDevice] == 0)
            {
                switch (vrSystem->GetTrackedDeviceClass(nDevice))
                {
                case vr::TrackedDeviceClass_Controller:        m_rDevClassChar[nDevice] = 'C'; break;
                case vr::TrackedDeviceClass_HMD:               m_rDevClassChar[nDevice] = 'H'; break;
                case vr::TrackedDeviceClass_Invalid:           m_rDevClassChar[nDevice] = 'I'; break;
                case vr::TrackedDeviceClass_GenericTracker:    m_rDevClassChar[nDevice] = 'G'; break;
                case vr::TrackedDeviceClass_TrackingReference: m_rDevClassChar[nDevice] = 'T'; break;
                default:                                       m_rDevClassChar[nDevice] = '?'; break;
                }
            }
            m_strPoseClasses += m_rDevClassChar[nDevice];*/
        }
    }

    if (trackedDevicePose[vr::k_unTrackedDeviceIndex_Hmd].bPoseIsValid) {
        hmdPose = devicePose[vr::k_unTrackedDeviceIndex_Hmd];
        hmdPose = glm::inverse(hmdPose);
    }
#endif
}
