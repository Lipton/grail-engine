#ifndef GRAIL_SCREEN_H
#define GRAIL_SCREEN_H

#include "InputProcessor.h"
#include "ScreenManager.h"

namespace grail {
	class Screen {
		friend class ScreenManager;
	public:
		virtual void init() {};
		virtual void show() {};
		virtual void hide() {};
		virtual void render(float delta) {  }
		virtual void resize(int width, int height) {  }
		virtual void dispose() {};

		ScreenManager& getScreenManager();
		InputProcessor& getInputProcessor();
	private:
		ScreenManager* screenManager = nullptr;
		InputProcessor* inputProcessor = nullptr;

		void innerInit();
	};
}

#endif