#ifndef GRAIL_CONSTANTS_H
#define GRAIL_CONSTANTS_H

#include "Common.h"

namespace grail {
	namespace engineConstants {
		static const uint32_t ENGINE_VERSION = 1;
		static const uint32_t VULKAN_VERSION_MAJOR = 1;
		static const uint32_t VULKAN_VERSION_PATCH = 0;
		static const uint32_t VULKAN_VERSION_MINOR = 0;
	}
}

#endif