#ifndef GRAIL_RENDER_CHAIN
#define GRAIL_RENDER_CHAIN

#include "Framebuffer.h"
#include "VkStructs.h"
#include <map>

namespace grail {
	struct RenderStep {
		VkPipelineStageFlags submitPipelineStages = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

		uint32_t width;
		uint32_t height;

		RenderStep* prevStep = nullptr;
		RenderStep* nextStep = nullptr;

		std::vector<VkCommandBuffer> cmdBuffers;
		std::vector<VkSemaphore> semaphores;

		std::map<std::string, FramebufferAttachment> attachments;

		VkSemaphore completeSemaphore;

		VkFramebuffer framebuffer;
		VkRenderPass renderPass;

		std::string identifier;
		std::string dependancy;

		uint32_t frameIndex;

		VkCommandBufferBeginInfo defaultBeginInfo;
		VkRenderPassBeginInfo defaultRenderPassBeginInfo;
		std::vector<VkClearValue> defaultClearValues;
		VkViewport defaultViewport;
		VkRect2D defaultScissor;
		VkSubmitInfo defaultSubmitInfo;

		std::vector<VkQueryPool> queryPools;
		uint32_t queryPoolSize;

		uint32_t chainIndex;
		double executionTime;
		double minTime = std::numeric_limits<double>().max();
		double maxTime = std::numeric_limits<double>().min();
		bool setup = false;

		bool resetPool = false;

		void beginStep(VkCommandBuffer cmdBuff) {
			VK_VALIDATE_RESULT(vkBeginCommandBuffer(cmdBuff, &defaultBeginInfo));


	//		GRAIL_LOG(INFO, "hmmmmm") << queryPools.size() << " " << chainIndex << " " << frameIndex << " " << queryPoolSize;

			if (resetPool) {
				//vkCmdResetQueryPool(cmdBuff, queryPools[frameIndex], 0, queryPoolSize);
			}


			//vkCmdWriteTimestamp(cmdBuff, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, queryPools[frameIndex], chainIndex * 2);
		}

		void setupStep(VkCommandBuffer cmdBuff) {
			setup = true;

			beginStep(cmdBuff);

			vkCmdSetViewport(cmdBuff, 0, 1, &defaultViewport);
			vkCmdSetScissor(cmdBuff, 0, 1, &defaultScissor);
			vkCmdBeginRenderPass(cmdBuff, &defaultRenderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);
		}

		void endStep(VkQueue queue, VkCommandBuffer cmdBuff) {
			if (setup) {
				vkCmdEndRenderPass(cmdBuff);
				setup = false;
			}

			//vkCmdWriteTimestamp(cmdBuff, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, queryPools[frameIndex], (chainIndex * 2) + 1);

			VK_VALIDATE_RESULT(vkEndCommandBuffer(cmdBuff));

			defaultSubmitInfo.commandBufferCount = 1;
			defaultSubmitInfo.pCommandBuffers = &cmdBuff;

			submitStep(queue);
		}

		void endStep(VkCommandBuffer cmdBuff) {
			if (setup) {
				vkCmdEndRenderPass(cmdBuff);
				setup = false;
			}

			//vkCmdWriteTimestamp(cmdBuff, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, queryPools[frameIndex], (chainIndex * 2) + 1);

			VK_VALIDATE_RESULT(vkEndCommandBuffer(cmdBuff));

			defaultSubmitInfo.commandBufferCount = 1;
			defaultSubmitInfo.pCommandBuffers = &cmdBuff;
		}

		void submitStep(VkQueue queue) {
			VK_VALIDATE_RESULT(vkQueueSubmit(queue, 1, &defaultSubmitInfo, VK_NULL_HANDLE));
		}
	};

	struct RenderStepAttachmentCreateInfo {
		TextureCreateInfo textureInfo;

		uint32_t width;
		uint32_t height;

		ImageLayout initialLayout = ImageLayout::UNDEFINED;
		ImageLayout finalLayout = ImageLayout::SHADER_READ_ONLY_OPTIMAL;

		AttachmentLoadOp loadOp = AttachmentLoadOp::CLEAR;
		AttachmentStoreOp storeOp = AttachmentStoreOp::STORE;

		AttachmentLoadOp stencilLoadOp = AttachmentLoadOp::DONT_CARE;
		AttachmentStoreOp stencilStoreOp = AttachmentStoreOp::DONT_CARE;
	};

	struct RenderStepDescription {
		uint32_t width;
		uint32_t height;

		uint32_t commandBufferCount;
		uint32_t semaphoreCount;

		std::vector<std::string> attachmentDependancies;

		std::string dependancy;

		bool compute = false;
	};

	class RenderChain {
	public:
		uint32_t queryPoolSize;
		uint32_t frameIndex = 0;

		RenderChain() {
			VkCommandPoolCreateInfo cmdPoolInfo = {};
			cmdPoolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
			cmdPoolInfo.pNext = nullptr;
			cmdPoolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
			cmdPoolInfo.queueFamilyIndex = VulkanContext::mainGPU.graphicsQueue.index;
			VK_VALIDATE_RESULT(vkCreateCommandPool(VulkanContext::mainGPU.device, &cmdPoolInfo, nullptr, &graphicsCommandBufferPool));

			cmdPoolInfo.queueFamilyIndex = VulkanContext::mainGPU.computeQueue.index;
			VK_VALIDATE_RESULT(vkCreateCommandPool(VulkanContext::mainGPU.device, &cmdPoolInfo, nullptr, &computeCommandBufferPool));
		}

		void createAttachment(std::string identifier, RenderStepAttachmentCreateInfo& createInfo) {
			if (identifier.length() <= 0)
				throw new exceptions::RuntimeException("Cant have an empty attachment identifier!");

			VkTexture* texture = nullptr;

			if (!(createInfo.width == 0 || createInfo.height == 0))
				texture = Resources::createTexture(identifier, createInfo.width, createInfo.height, createInfo.textureInfo);

			VkAttachmentDescription desc = {};
			desc.flags = 0;
			desc.samples = static_cast<VkSampleCountFlagBits>(createInfo.textureInfo.sampleCount);
			desc.loadOp = static_cast<VkAttachmentLoadOp>(createInfo.loadOp);
			desc.storeOp = static_cast<VkAttachmentStoreOp>(createInfo.storeOp);
			desc.stencilLoadOp = static_cast<VkAttachmentLoadOp>(createInfo.stencilLoadOp);
			desc.stencilStoreOp = static_cast<VkAttachmentStoreOp>(createInfo.stencilStoreOp);
			desc.format = createInfo.textureInfo.format;
			desc.initialLayout = static_cast<VkImageLayout>(createInfo.initialLayout);
			desc.finalLayout = static_cast<VkImageLayout>(createInfo.finalLayout);

			attachments.insert({ identifier, {createInfo.width, createInfo.height, texture, desc} });
		}

		void describeStep(std::string identifier, RenderStepDescription& stepDescription) {
			if (identifier.length() <= 0)
				throw new exceptions::RuntimeException("Cant have an empty render step identifier!");

			RenderStep step = {};
			step.width = stepDescription.width;
			step.height = stepDescription.height;
			step.dependancy = stepDescription.dependancy;
			step.identifier = identifier;
			step.cmdBuffers.resize(stepDescription.commandBufferCount);
			step.semaphores.resize(stepDescription.semaphoreCount);

			VkSemaphoreCreateInfo semaphoreInfo = {};
			semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
			semaphoreInfo.pNext = nullptr;
			semaphoreInfo.flags = 0;
			VK_VALIDATE_RESULT(vkCreateSemaphore(VulkanContext::mainGPU.device, &semaphoreInfo, nullptr, &step.completeSemaphore));

			if (stepDescription.semaphoreCount > 0) {
				for (uint32_t i = 0; i < stepDescription.semaphoreCount; i++) {
					VK_VALIDATE_RESULT(vkCreateSemaphore(VulkanContext::mainGPU.device, &semaphoreInfo, nullptr, &step.semaphores[i]));
				}
			}

			if (stepDescription.commandBufferCount > 0) {
				VkCommandBufferAllocateInfo cmdBufferInfo = {};
				cmdBufferInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
				cmdBufferInfo.pNext = nullptr;
				cmdBufferInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
				cmdBufferInfo.commandBufferCount = stepDescription.commandBufferCount;
				cmdBufferInfo.commandPool = stepDescription.compute ? computeCommandBufferPool : graphicsCommandBufferPool;
				VK_VALIDATE_RESULT(vkAllocateCommandBuffers(VulkanContext::mainGPU.device, &cmdBufferInfo, &step.cmdBuffers[0]));
			}

			for (uint32_t i = 0; i < stepDescription.attachmentDependancies.size(); i++)
				step.attachments.insert({ stepDescription.attachmentDependancies[i], attachments[stepDescription.attachmentDependancies[i]] });

			steps.insert({ identifier, step });
		}

		void finalizeChain() {
			// ORDER STEPS
			RenderStep* last = nullptr;

			int start = 0;
			for (auto& step : steps) {
				if (step.second.dependancy.length() <= 0) {
					last = &step.second;
					start++;
					break;
				}
			}

			if (last == nullptr)
				throw new exceptions::RuntimeException("Could not find chain beginning!");

			chainOrder.push_back(last->identifier);

			if (steps.size() > 1) {
				int cIndex = 0;
				for (auto& step : steps) {
					if (cIndex != start) {
						RenderStep* next = findDependant(last);

						if (next == nullptr) {
							GRAIL_LOG(WARNING, "RENDER CHAIN") << "Could not find dependancy for render step: " << last->identifier;
							continue;
						}

						last->nextStep = next;
						next->prevStep = last;

						chainOrder.push_back(next->identifier);

						last = next;
					}
					cIndex++;
				}

				if (chainOrder.size() != steps.size()) {
					GRAIL_LOG(WARNING, "RENDER CHAIN") << (steps.size() - chainOrder.size()) << " steps are not used in chain!";
				}
			}


			// ORDER ATTACHMENT TRANSITIONS
			std::vector<std::string> usedAttachments;

			for (std::string& id : chainOrder) {
				RenderStep& step = steps[id];

				for (auto& attachment : step.attachments) {
					if (std::find(usedAttachments.begin(), usedAttachments.end(), attachment.first) != usedAttachments.end()) {
						attachment.second.description.initialLayout = attachments[attachment.first].description.finalLayout;
						attachment.second.description.loadOp = static_cast<VkAttachmentLoadOp>(AttachmentLoadOp::LOAD);
					}
					else {
						usedAttachments.push_back(attachment.first);
					}
				}
			}

			if (usedAttachments.size() != attachments.size()) {
				GRAIL_LOG(WARNING, "RENDER CHAIN") << (attachments.size() - usedAttachments.size()) << " attachments are not used in chain!";
			}

			// CREATE RENDER PASSES AND FRAMEBUFFERS
			for (auto& step : steps) {
				// COMMAND BUFFER BEGIN INFO
				{
					VkCommandBufferBeginInfo cmdBufferBeginInfo = {};
					cmdBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
					cmdBufferBeginInfo.pNext = nullptr;
					cmdBufferBeginInfo.flags = 0;
					cmdBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

					step.second.defaultBeginInfo = cmdBufferBeginInfo;
				}

				// SUBMIT INFO
				VkSubmitInfo submitInfo = {};
				submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
				submitInfo.pNext = nullptr;
				submitInfo.pWaitDstStageMask = &step.second.submitPipelineStages;
				submitInfo.signalSemaphoreCount = 1;
				submitInfo.pSignalSemaphores = &step.second.completeSemaphore;
				if (step.second.prevStep != nullptr) {
					submitInfo.waitSemaphoreCount = 1;
					submitInfo.pWaitSemaphores = &step.second.prevStep->completeSemaphore;
				}

				step.second.defaultSubmitInfo = submitInfo;

				if (step.second.attachments.size() <= 0) continue;

				std::vector<VkAttachmentReference> colorAttachments;
				VkAttachmentReference depthAttachment = { UINT32_MAX, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL };

				uint32_t index = 0;
				for (auto& attachment : step.second.attachments) {
					if (VulkanContext::isDepthStencil(attachment.second.description.format)) {
						depthAttachment.attachment = index;
					}
					else {
						colorAttachments.push_back({ index, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL });
					}

					index++;
				}

				VkSubpassDescription subpass = {};
				subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
				subpass.pColorAttachments = colorAttachments.data();
				subpass.colorAttachmentCount = static_cast<uint32_t>(colorAttachments.size());
				if (depthAttachment.attachment != UINT32_MAX)
					subpass.pDepthStencilAttachment = &depthAttachment;

				std::array<VkSubpassDependency, 2> dependencies;

				dependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
				dependencies[0].dstSubpass = 0;
				dependencies[0].srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
				dependencies[0].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
				dependencies[0].srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
				dependencies[0].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
				dependencies[0].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

				dependencies[1].srcSubpass = 0;
				dependencies[1].dstSubpass = VK_SUBPASS_EXTERNAL;
				dependencies[1].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
				dependencies[1].dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
				dependencies[1].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
				dependencies[1].dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
				dependencies[1].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

				std::vector<VkAttachmentDescription> descriptions;
				for (auto& attachment : step.second.attachments) {
					descriptions.push_back(attachment.second.description);
				}

				VkRenderPassCreateInfo renderPassInfo = {};
				renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
				renderPassInfo.pAttachments = descriptions.data();
				renderPassInfo.attachmentCount = static_cast<uint32_t>(descriptions.size());
				renderPassInfo.subpassCount = 1;
				renderPassInfo.pSubpasses = &subpass;
				renderPassInfo.dependencyCount = 2;
				renderPassInfo.pDependencies = dependencies.data();
				VK_VALIDATE_RESULT(vkCreateRenderPass(VulkanContext::mainGPU.device, &renderPassInfo, nullptr, &step.second.renderPass));


				bool framebufferless = false;
				for (auto& attachment : step.second.attachments) {
					if (attachment.second.texture == nullptr) {
						framebufferless = true;
						break;
					}
				}

				if (!framebufferless) {
					std::vector<VkImageView> attachmentViews;
					for (auto& attachment : step.second.attachments) {
						//attachmentViews.push_back(attachment.second.texture->mipViews[0]);
					}

					uint32_t maxLayers = 0;
					for (auto& attachment : step.second.attachments) {
						if (attachment.second.texture->getLayerCount() > maxLayers) {
							maxLayers = attachment.second.texture->getLayerCount();
						}
					}

					VkFramebufferCreateInfo framebufferInfo = {};
					framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
					framebufferInfo.renderPass = step.second.renderPass;
					framebufferInfo.pAttachments = attachmentViews.data();
					framebufferInfo.attachmentCount = static_cast<uint32_t>(attachmentViews.size());
					framebufferInfo.width = step.second.width;
					framebufferInfo.height = step.second.height;
					framebufferInfo.layers = maxLayers;
					VK_VALIDATE_RESULT(vkCreateFramebuffer(VulkanContext::mainGPU.device, &framebufferInfo, nullptr, &step.second.framebuffer));
				}


				// SETUP DEFAULT INFO

				// CLEAR VALUES
				std::vector<VkClearValue> clearValues;
				clearValues.resize(step.second.attachments.size());
				uint32_t i = 0;
				for (auto& attachment : step.second.attachments) {
					if (VulkanContext::isDepthStencil(attachment.second.description.format)) {
						clearValues[i].depthStencil = { 1.0f, 0 };
					}
					else {
						clearValues[i].color = { 0.0f, 0.0f, 0.0f, 1.0f };
					}

					i++;
				}
				step.second.defaultClearValues = clearValues;

				// RENDER PASS BEGIN INFO
				VkRenderPassBeginInfo renderPassBeginInfo = {};
				renderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
				renderPassBeginInfo.pNext = nullptr;
				renderPassBeginInfo.renderPass = step.second.renderPass;
				renderPassBeginInfo.renderArea.offset.x = 0;
				renderPassBeginInfo.renderArea.offset.y = 0;
				renderPassBeginInfo.renderArea.extent.width = step.second.width;
				renderPassBeginInfo.renderArea.extent.height = step.second.height;
				renderPassBeginInfo.clearValueCount = step.second.defaultClearValues.size();
				renderPassBeginInfo.pClearValues = &step.second.defaultClearValues[0];

				if (!framebufferless)
					renderPassBeginInfo.framebuffer = step.second.framebuffer;

				step.second.defaultRenderPassBeginInfo = renderPassBeginInfo;

				// VIEWPORT INFO
				VkViewport viewport = {};
				viewport.width = (float)step.second.width;
				viewport.height = (float)step.second.height;
				viewport.minDepth = (float) 0.0f;
				viewport.maxDepth = (float) 1.0f;

				step.second.defaultViewport = viewport;

				// SCISSOR INFO
				VkRect2D scissor = {};
				scissor.extent.width = step.second.width;
				scissor.extent.height = step.second.height;
				scissor.offset.x = 0;
				scissor.offset.y = 0;

				step.second.defaultScissor = scissor;
			}

			queryPoolSize = chainOrder.size() * 2;
			timestamps.resize(queryPoolSize);
			queryPools.resize(2);

			for (int i = 0; i < timestamps.size(); i++) {
				timestamps[i] = 0;
			}

			VkQueryPoolCreateInfo createInfo = {};
			createInfo.sType = VK_STRUCTURE_TYPE_QUERY_POOL_CREATE_INFO;
			createInfo.pNext = nullptr;
			createInfo.queryType = VK_QUERY_TYPE_TIMESTAMP;
			createInfo.queryCount = queryPoolSize;

			VK_VALIDATE_RESULT(vkCreateQueryPool(VulkanContext::mainGPU.device, &createInfo, nullptr, &queryPools[0]));
			VK_VALIDATE_RESULT(vkCreateQueryPool(VulkanContext::mainGPU.device, &createInfo, nullptr, &queryPools[1]));

			steps[chainOrder[0]].resetPool = true;
			for (auto& step : steps) {
				step.second.queryPools = queryPools;
				step.second.queryPoolSize = queryPoolSize;
			}

			for (uint32_t i = 0; i < chainOrder.size(); i++) {
				getStep(chainOrder[i]).chainIndex = i;
			}
		}

		std::vector<std::string>& getChainOrder() {
			return chainOrder;
		}

		RenderStep& getStep(std::string id) {
			return steps[id];
		}

		RenderStep* beginChain() {

			for (uint32_t i = 0; i < chainOrder.size(); i++) {
				RenderStep* step = &steps[chainOrder[i]];

				step->frameIndex = frameIndex;
			}

			if (!initialStart) {
				//vkGetQueryPoolResults(VulkanContext::mainGPU.device, queryPools[frameIndex], 0, queryPoolSize, queryPoolSize * sizeof(uint64_t), &timestamps[0], sizeof(uint64_t), VK_QUERY_RESULT_64_BIT);

				for (uint32_t i = 0; i < chainOrder.size(); i++) {
					/*RenderStep* step = &steps[chainOrder[i]];

					double time = static_cast<double>(timestamps[i * 2 + 1] - timestamps[i * 2]) * 1e-6;

					if (time != 0) {
						if (time < step->minTime) {
							step->minTime = time;
						}
					}

					if (time > step->maxTime) {
						step->maxTime = time;
					}

					step->executionTime = time;*/
				}
			}
			else {
				initialStart = false;
			}

			chainIndex = 0;

			return &steps[chainOrder[chainIndex]];
		}

		RenderStep* getNextStep() {
			if (chainIndex + 1 == chainOrder.size()) return nullptr;

			return &steps[chainOrder[++chainIndex]];
		}

		FramebufferAttachment& getAttachment(std::string id) {
			return attachments[id];
		}

		std::string getStepExecutionTimes() {
			std::stringstream ss;
			double totalTime = 0;
			for (uint32_t i = 0; i < chainOrder.size(); i++) {
				std::string executionTime = (std::to_string(steps[chainOrder[i]].executionTime).substr(0, 5) + "ms");
				std::string name = chainOrder[i];

				ss << align(name, 20) << ": " << align(executionTime, 13) << "\n";
					/*<< " [min: " << align(std::to_string(steps[chainOrder[i]].minTime) + "ms", 13) 
					<< " max: " << align(std::to_string(steps[chainOrder[i]].maxTime) + "ms", 13) 
					<< " avg: " << align(std::to_string((steps[chainOrder[i]].minTime + steps[chainOrder[i]].maxTime) * 0.5) + "ms", 10) << "]\n";*/

				totalTime += steps[chainOrder[i]].executionTime;
			}

			if (totalTime != 0) {
				if (totalTime < minTime) {
					minTime = totalTime;
				}
			}

			if (totalTime > maxTime) {
				maxTime = totalTime;
			}

			std::string time = std::to_string(totalTime).substr(0, 5);
			time.append("ms");

			ss << "\n\nTotal: " << align(time, 15) << Graphics::getSmoothedFPS() << " FPS";
				/*<< " [min: " << align(std::to_string(minTime) + "ms", 13)
				<< " max: " << align(std::to_string(maxTime) + "ms", 13)
				<< " avg: " << align(std::to_string((minTime + maxTime) * 0.5) + "ms", 10) << "]\n";;/*/

			return totalTime == 0.0 ? (std::to_string(Graphics::getSmoothedFPS()) + " FPS") : ss.str();
		}
	private:
		double minTime = std::numeric_limits<double>().max();
		double maxTime = std::numeric_limits<double>().min();

		bool initialStart = true;

		std::vector<VkQueryPool> queryPools;
		std::vector<uint64_t> timestamps;

		uint32_t chainIndex = 0;

		std::unordered_map<std::string, RenderStep> steps;
		std::unordered_map<std::string, FramebufferAttachment> attachments;

		std::vector<std::string> chainOrder;

		VkCommandPool graphicsCommandBufferPool;
		VkCommandPool computeCommandBufferPool;

		std::string align(std::string& input, uint32_t targetWidth) {
			std::string result = input;

			int pad = targetWidth - input.length();
			
			if (pad >= 1) {
				for (uint32_t i = 0; i < pad; i++) {
					result.append(" ");
				}
 			}

			return result;
		}

		RenderStep* findDependant(RenderStep* on) {
			for (auto& step : steps) {
				if (&step.second != on) {
					if (step.second.dependancy.compare(on->identifier) == 0) {
						return &step.second;
					}
				}
			}

			return nullptr;
		}
	};
}

#endif