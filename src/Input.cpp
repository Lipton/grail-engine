#include "Input.h"

#include "ApplicationWindow.h"

float grail::Input::mouseX = 0;
float grail::Input::mouseY = 0;

float grail::Input::mouseTouchX = 0;
float grail::Input::mouseTouchY = 0;

float grail::Input::mouseScrollX = 0;
float grail::Input::mouseScrollY = 0;

bool grail::Input::mouseCaptured = false;

bool grail::Input::keys[grail::Keys::LAST];
bool grail::Input::buttons[grail::Buttons::LAST];

bool grail::Input::justPressedKeys[grail::Keys::LAST];
bool grail::Input::justPressedButtons[grail::Buttons::LAST];

grail::ApplicationWindow* grail::Input::window = nullptr;

float grail::Input::getMouseX() {
	return mouseX;
}

float grail::Input::getMouseY() {
	return mouseY;
}

float grail::Input::getMouseTouchX() {
	return mouseTouchX;
}

float grail::Input::getMouseTouchY() {
	return mouseTouchY;
}

float grail::Input::getMouseScrollX() {
	return mouseScrollX;
}

float grail::Input::getMouseScrollY() {
	return mouseScrollY;
}

bool grail::Input::isKeyPressed(unsigned short int key) {
	return keys[key];
}

bool grail::Input::isButtonPressed(unsigned short int button) {
	return buttons[button];
}

bool grail::Input::isKeyJustPressed(unsigned short int key) {
	if (!justPressedKeys[key]) {
		justPressedKeys[key] = true;
		return true;
	}

	return false;
}

bool grail::Input::isButtonJustPressed(unsigned short int button) {
	if (!justPressedButtons[button]) {
		justPressedButtons[button] = true;
		return true;
	}

	return false;
}

void grail::Input::captureMouse(bool capture) {
	window->captureCursor(capture);
}

bool grail::Input::isMouseCaptured() {
#if defined(OS_WINDOWS) || defined(OS_LINUX)
	return mouseCaptured;
#else
	return true;
#endif
}
