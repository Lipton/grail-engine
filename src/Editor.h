#ifndef EDITOR_H
#define EDITOR_H

#include "Screen.h"

#include "ImGuiHelper.h"
#include "ImGuizmo.h"

#ifdef OS_ANDROID	
#define GRAIL_DEPLOY_BUILD
#endif

//#define GRAIL_DEPLOY_BUILD
//#define GRAIL_ENABLE_VR


//#define ENABLE_RTX

//#pragma comment(lib,"WS2_32")

std::ofstream logFile;
class FileLogger : public grail::logger::Implementation {
	void print(std::stringstream& ss, time_t timestamp, grail::LogType type, const char* log, const char* tag);
};

class TextEditor;

namespace grail {
	class PerspectiveCamera;
	class FirstPersonCameraController;
	class ImGuiHelper;
	class ImGuiConsole;
	class SceneNode;
	class Script;
	class DeferredRenderer;
	class FrameGraph;
	class Mesh;

	class ProjectStructureViewer;

	class DeployScreen : public grail::Screen {
	public:
		PerspectiveCamera* camera;
		DeferredRenderer* renderer;
		FirstPersonCameraController* controller;

		void init();
		void render(float delta);
	};

	class Editor : public grail::Screen {
	public:
		ProjectStructureViewer* structureViewer;

		enum class EditorWindowType : uint32_t {
			PERFORMACE_METRICS = 0,
			SCENE_HIERARCHY = 1,
			NODE_PROPERTIES = 2,
			RESOURCES = 3,
			CONSOLE = 4,
			RESOURCE_INSPECTOR = 5,
			SCRIPT_EDITOR = 6,
			MEMORY_INSPECTOR = 7,
			VR_INFO = 8,

			IMGUI_DEMO = 9,
			SIZE = IMGUI_DEMO + 1
		};

		std::map<EditorWindowType, std::string> editorWindowNames = {
			{EditorWindowType::PERFORMACE_METRICS, "Performance Metrics"},
			{EditorWindowType::SCENE_HIERARCHY, "Scene Hierarchy"},
			{EditorWindowType::NODE_PROPERTIES, "Node Properties"},
			{EditorWindowType::RESOURCES, "Resources"},
			{EditorWindowType::CONSOLE, "Console"},
			{EditorWindowType::RESOURCE_INSPECTOR, "Resource Inspector"},
			{EditorWindowType::SCRIPT_EDITOR, "Script Editor"},
			{EditorWindowType::MEMORY_INSPECTOR, "Memory Inspector" },
			{EditorWindowType::VR_INFO, "VR Information" },

			{EditorWindowType::IMGUI_DEMO, "ImGui Demo"}
		};

		enum class ResourceInspectorType {
			TEXTURE,
			MESH
		};

		enum class EditorState {
			PLAYING,
			PAUSED,
			STOPPED
		};

		grail::DeferredRenderer* renderer;		
		grail::FrameGraph* meshDisplayGraph;

		void setState(EditorState newState);

		EditorState state = EditorState::STOPPED;
		bool mouseState = false;
		std::string lastSave = "";

		struct UISettings {
			float cameraSpeed = 80.0f;
			float cameraSpeedMod = 2.25f;

			struct {
				std::string currentDirectory = "";

			} resources;

			bool highlightSelected = false;
			bool debugPhysics = false;

			bool enabled = true;
			float mainPadding = 0.155f;
			float edgePad = 0.01f;
			bool shrinkMainView = true;

			const int fpsGraphSamples = 200;
			int fpsGraphCurrentSample = 0;
			float fpsGraphSampleRate = 20.0f;
			std::chrono::high_resolution_clock::time_point fpsGraphLastSampleTime = std::chrono::high_resolution_clock::now();
			std::vector<float> fpsGraph;

			std::chrono::high_resolution_clock::time_point lastCPUSampleTime = std::chrono::high_resolution_clock::now();
			double CPUOverhead = 0.0f;

			grail::SceneNode* selectedNode;

			ImGuizmo::OPERATION transformType = ImGuizmo::OPERATION::DISABLED;
			bool useSnapping = false;
			bool transforming = false;
			bool transformWorld = true;

			glm::vec3 snapSizes = glm::vec3(1.0);
			glm::mat4 localCopy;
			glm::mat4 worldCopy;
			glm::vec3 scaleCopy;
			glm::mat4 delta = glm::mat4(1.0);
			glm::vec3 temp;

			int node_clicked = -1;
			bool renaming = false;

			std::string tempString;
			std::string nodeFilter = "";
			std::string meshFilter = "";
			std::string textureFilter = "";

			bool moving = false;

			ResourceInspectorType resourceInspectorType = ResourceInspectorType::TEXTURE;
			VkTexture* inspectedTexture = nullptr;
			VkImageView inspectedTextureView = VK_NULL_HANDLE;
			VkImageSubresourceRange inspectedTextureSubresourceRange = { 0, 0, 1, 0, 1 };

			grail::PerspectiveCamera* inspectedMeshCamera;
			Mesh* inspectedMesh = nullptr;
			float inspectedMeshTime = 0.0;

			struct {
				ImGuiID leftBarDockSpace = 0;
				ImGuiID rightBarDockSpace = 0;
				ImGuiID bottomBarDockSpace = 0;

				std::array<bool, static_cast<uint32_t>(EditorWindowType::SIZE)> windows;
			} handles;

			struct {
				Script* handle;
				SceneNode* node;
			} editedScript;

			uint32_t openPopup = 0;
			uint32_t POPUP_SAVE_SCENE = 1;
			uint32_t POPUP_LOAD_SCENE = 2;
			uint32_t POPUP_NAME_SCRIPT = 3;

			std::string saveLoadFilename;

			TextEditor* textEditor;

			glm::vec4 mainViewBounds = glm::vec4(1.0);
		} ui;
		grail::ImGuiHelper* imguiHelper;

		double virtualY = 0;
		double virtualX = 0;

		double offsetX = 0;
		double offsetY = 0;

		double lastClickTime = 0;

		float time = 0.0f;
		float lastFrameTime = 0.0f;
		uint32_t secondsPassed = 0;
		uint64_t frameCount = 0;

		grail::PerspectiveCamera* camera = nullptr;
		grail::FirstPersonCameraController* cameraController = nullptr;
		
		grail::MaterialDescription* highlightMaterial;
		grail::MaterialInstance* highlighMaterialInstance;

		void init();

		void renderUI();
		void UI_Memory_Inspector(const std::string& windowName, bool* open);
		void VR_Info_Window(const std::string& windowName, bool* open);

		void render(float delta);

		int parseNode(grail::SceneNode* node, int index, int* node_clicked, int selection_mask, grail::SceneNode** newRoot);
		void nodeContextMenu(grail::SceneNode* node);

		grail::SceneNode* castPickRay();

		public:
			static ImGuiConsole* console;
	};
}

#endif
