#ifndef GRAIL_COMMON_H
#define GRAIL_COMMON_H

#include <cstring>
#include <fstream>
#include <array>
#include <cassert>
#include <string>
#include <vector>
#include <unordered_map>
#include <map>
#include <functional>

#include "Platform.h"
#include "Logger.h"
#include "Graphics.h"
#include "Input.h"
#include "Exceptions.h"

#include "VulkanLoader.h"
#include "VulkanContext.h"
#include "VkStructs.h"

//#include "FileIO.h"

#include "GLMIncludes.h"

#if defined(OS_ANDROID)
#include <android_native_app_glue.h>
#include <android/input.h>
#endif

#undef ERROR

#endif