#ifndef GRAIL_APPLICATION_WINDOW_H
#define GRAIL_APPLICATION_WINDOW_H

#include "Grail.h"

#if defined (OS_ANDROID)

#else
#if defined(OS_WINDOWS)
#define GLFW_EXPOSE_NATIVE_WIN32
#elif defined(OS_LINUX)
#define GLFW_EXPOSE_NATIVE_X11
#endif
#include <GLFW/glfw3.h>
#include <GLFW/glfw3native.h>
#endif

namespace grail {
	class ScreenManager;

	class ApplicationWindow {
		friend class Grail;
		friend class Swapchain;
	public:
		// only on desktop platforms
		void captureCursor(bool capture);
		void setTitle(const char* title);
	private:
		ApplicationWindow();
		~ApplicationWindow();

		bool initialize(EngineInitializationInfo& initInfo, ScreenManager* screenManager);

		bool load();

		void processFrame();

		bool shouldClose();

		VkSurfaceKHR createSurface();

		ScreenManager* screenManager;

#if defined(OS_ANDROID)
		struct android_app* android_app;

		int ident;
		int events;
		bool initialized = false;
		struct android_poll_source* source;

		static int32_t handleInput(struct android_app* app, AInputEvent* event);
		static void cmdCallback(struct android_app *android_app, int32_t cmd);
#else 
		GLFWwindow* window;

		static void mouseMovementCallback(GLFWwindow* window, double posX, double posY);
		static void mouseClickCallback(GLFWwindow* window, int button, int action, int mods);
		static void keyPressCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
		static void mouseScrollCallback(GLFWwindow* window, double xoffset, double yoffset);
		static void dropCallback(GLFWwindow* window, int count, const char** paths);
		static void charCallback(GLFWwindow* window, unsigned int c, int mods);
#endif
		static void mouseMovementCallback(ApplicationWindow* self, float posX, float posY);
		static void mouseClickCallback(ApplicationWindow* self, int button, int action, int mods);
		static void keyPressCallback(ApplicationWindow* self, int key, int scancode, int action, int mods);
		static void mouseScrollCallback(ApplicationWindow* self, float xoffset, float yoffset);
		static void dropCallback(ApplicationWindow* self, int count, const char** paths);
		static void charCallback(ApplicationWindow* window, unsigned int c, int mods);
	};
}

#endif