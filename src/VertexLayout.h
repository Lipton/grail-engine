#ifndef GRAIL_VERTEX_LAYOUT_H
#define GRAIL_VERTEX_LAYOUT_H

#include "Common.h"

namespace grail {
	enum VertexAttributeType {
		GENERIC,
		POSITION,
		COLOR,
		NORMAL,
		UV,
		TANGENT,
		BITANGENT,
		DUMMY
	};

	class VertexAttribute {
	public:
		VertexAttribute(VertexAttributeType type, VkFormat format, uint32_t elementCount, uint32_t elementSize, bool packed);
		VertexAttribute(VertexAttributeType type, VkFormat format, uint32_t elementCount, uint32_t elementSize);
		VertexAttribute(VkFormat format, uint32_t elementCount, uint32_t elementSize, bool packed);
		VertexAttribute(VkFormat format, uint32_t elementCount, uint32_t elementSize);

		VertexAttributeType getType();
		VkFormat getFormat();

		bool isPacked();

		uint32_t getElementCount();
		uint32_t getElementSize();

		uint32_t getSize();

		uint32_t getOffset();
		void setOffset(uint32_t offset);

		uint32_t getBinding();
		void setBinding(uint32_t binding);
	private:
		VertexAttributeType type;
		VkFormat format;

		uint32_t elementCount;
		uint32_t elementSize;

		uint32_t offset = 0;
		uint32_t binding = 0;

		bool packed;
	};

	class VertexLayout {
	public:
		VertexLayout(std::initializer_list<VertexAttribute> attributes);
		VertexLayout(std::vector<VertexAttribute> attributes);
		VertexLayout();

		VertexAttribute& getVertexAttribute(uint32_t index);

		uint32_t getVertexSize();
		uint32_t getVertexElementCount();
		uint32_t getVertexAttributeCount();

		bool hasAttributeType(VertexAttributeType type);

		VkPipelineVertexInputStateCreateInfo getVertexInputState();
	private:
		std::vector<VertexAttribute> layout;

		VkPipelineVertexInputStateCreateInfo vertexInputState;
		VkVertexInputBindingDescription bindingDescription;
		std::vector<VkVertexInputAttributeDescription> attributeDescriptions;

		uint32_t totalSize;
		uint32_t elementCount;
	};
}

#endif