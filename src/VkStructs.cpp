#include "VkStructs.h"

std::vector<std::string> grail::vkUtils::samplerFilterNames = { 
	"LINEAR", "NEAREST" 
};

std::vector<std::string> grail::vkUtils::samplerMipmapModeNames = { 
	"LINEAR", "NEAREST"
};

std::vector<std::string> grail::vkUtils::samplerAddressModeNames = { 
	"MIRRORED_REPEAT",
	"CLAMP_TO_EDGE",
	"CLAMP_TO_BORDER",
	"MIRROR_CLAMP_TO_EDGE",
	"REPEAT"
};

grail::SamplerFilter grail::vkUtils::samplerFilterFromString(const ::std::string& string) {
	if (string.compare("LINEAR") == 0) {
		return SamplerFilter::LINEAR;
	}
	else {
		return SamplerFilter::NEAREST;
	}
}

::std::string grail::vkUtils::samplerFilterToString(const SamplerFilter& filter) {
	if (filter == SamplerFilter::LINEAR) {
		return "LINEAR";
	}
	else {
		return "NEAREST";
	}
}

grail::SamplerMipmapMode grail::vkUtils::samplerMipmapModeFromString(const ::std::string& string) {
	if (string.compare("LINEAR") == 0) {
		return SamplerMipmapMode::LINEAR;
	}
	else {
		return SamplerMipmapMode::NEAREST;
	}
}

::std::string grail::vkUtils::samplerMipmapModeToString(const SamplerMipmapMode& filter) {
	if (filter == SamplerMipmapMode::LINEAR) {
		return "LINEAR";
	}
	else {
		return "NEAREST";
	}
}

grail::SamplerAddressMode grail::vkUtils::samplerAddressModeFromString(const ::std::string& string) {
	if (string.compare("MIRRORED_REPEAT") == 0) {
		return SamplerAddressMode::MIRRORED_REPEAT;
	}
	else if (string.compare("CLAMP_TO_EDGE") == 0) {
		return SamplerAddressMode::CLAMP_TO_EDGE;
	}
	else if (string.compare("CLAMP_TO_BORDER") == 0) {
		return SamplerAddressMode::CLAMP_TO_BORDER;
	}
	else if (string.compare("MIRROR_CLAMP_TO_EDGE") == 0) {
		return SamplerAddressMode::MIRROR_CLAMP_TO_EDGE;
	}
	else {
		return SamplerAddressMode::REPEAT;
	}
}

::std::string grail::vkUtils::samplerAddressModeToString(const SamplerAddressMode& mode) {
	if (mode == SamplerAddressMode::MIRRORED_REPEAT) {
		return "MIRRORED_REPEAT";
	}
	if (mode == SamplerAddressMode::CLAMP_TO_EDGE) {
		return "CLAMP_TO_EDGE";
	}
	if (mode == SamplerAddressMode::CLAMP_TO_BORDER) {
		return "CLAMP_TO_BORDER";
	}
	if (mode == SamplerAddressMode::MIRROR_CLAMP_TO_EDGE) {
		return "MIRROR_CLAMP_TO_EDGE";
	}
	else {
		return "REPEAT";
	}
}

