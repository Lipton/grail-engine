#ifndef GRAIL_SCENE_H
#define GRAIL_SCENE_H

#include <entt.hpp>

#include "Common.h"
#include "Camera.h"
#include "SceneNode.h"

//#include <btBulletDynamicsCommon.h>

namespace grail {
	namespace nodeComponents {
		class NodeComponent;
	}

	class Scene {
		//friend class Editor;
		friend class Grail;
	public:
		// Update the scene, used to update scripts
		static void update(float delta);

		// Process nodes that need to be deleted, gets called with update()
		static void processNodes();

		// Notify components that the scene has started after loading
		static void start();

		static SceneNode* getRootNode();

		// Find a node by name, returns a nullptr if not found
		static SceneNode* getNode(const std::string& name);

		// Create a new node with the given name
		// If left empty, will be given a name "New Node"
		// If given name clashes, will be named with a number
		// There might be a more graciuos way to solve name clashes
		static SceneNode* createNode(const std::string& name = "");

		// Clones the specified node, does a full copy including the children
		static SceneNode* cloneNode(SceneNode* node);

		// Remove node by value, does nothing with the root node
		static void removeNode(SceneNode* node);

		// Renames the specified node, if newName already exists, a duplicate marked name is assigned
		static void renameNode(SceneNode* node, const std::string& newName);

		// Remove node by name, does nothing with the root node, or node that is not found
		static void removeNode(const std::string& name);

		template<typename... Component>
		static entt::basic_view<entt::entity, Component...> getNodesWithComponents() {
			return entityRegistry.view<Component...>();
		}

		template<typename Component, typename ...Args>
		static inline void assignComponent(SceneNode* node, Args&& ...args) {
			if (getComponent<Component>(node) == nullptr) {
				entityRegistry.assign<Component>(node->entity, args...).node = node;
			}
		}

		template<typename Component>
		static inline auto* getComponent(SceneNode* node) {
			return entityRegistry.try_get<Component>(node->entity);
		}

		static void assignComponentByName(SceneNode* node, const std::string& componentName);
		static nodeComponents::NodeComponent* getComponentByName(SceneNode* node, const std::string& componentName);
	public:

		// Cast a ray into the scene and return the closest node hit
		// EXTREMELY SLOW AND UNOPTIMIZED AT THE MOMEMENT
		static SceneNode* castRay(glm::vec3 rayOrigin, glm::vec3 rayDirection, glm::vec3& intersectionPoint);
		
		// Cast a ray into the scene and return the closest node hit
		// A sligtly more optimized version of the castRay function
		static SceneNode* castRayOptimized(glm::vec3 rayOrigin, glm::vec3 rayDirection, glm::vec3& intersectionPoint);

		// Clears the whole scene, removing and disposing of all nodes
		static void clear();

		static const std::vector<SceneNode*> getAllNodes();
	private:
		static SceneNode* rootNode;

		// Flat list of nodes, no respect for hierarchy
		static std::vector<SceneNode*> nodes;

		// Map of nodes for fast name-based search, as a consequence identically named nodes are not allowed
		static std::unordered_map<std::string, SceneNode*> nodeMap;
		
		// Registry to allocate and manage entities tied to nodes
		static entt::registry entityRegistry;
	private:
	
		// Helper functions to ease the creation and removal of nodes
		static void addNode(SceneNode* node);
		static void deleteNode(SceneNode* node);

		static SceneNode* cloneInternal(SceneNode* parent, SceneNode* node);
		static SceneNode* createCopy(SceneNode* src);

		static std::string assignNewName(const std::string& input);

		static std::vector<SceneNode*> removeQueue;
	private:
		Scene& operator = (const Scene&) = delete;
		Scene(const Scene&) = delete;
		Scene() = default;

		// Should only be called once in the beginning of the application lifetime
		static void Initialize();
	};
}

#endif