#include "Texture.h"

/*grail::Texture::Texture(TextureCreateInfo& createInfo, uint32_t width, uint32_t height, void * buffer, MemoryRegion & stagingMemory, MemoryRegion & memory) {
	this->gpu = createInfo.gpu;
	this->width = width;
	this->height = height;
	
	VkFormatProperties formatProperties;
	vkGetPhysicalDeviceFormatProperties(createInfo.gpu.physicalDevice, createInfo.format, &formatProperties);

	VkMemoryAllocateInfo memAllocateInfo = {};
	memAllocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	memAllocateInfo.pNext = nullptr;
	VkMemoryRequirements memoryReqs;

	VkBuffer stageBuffer;

	VkBufferCreateInfo bufferInfo = {};
	bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferInfo.size = width * height * createInfo.bpp;
	bufferInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
	bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	VK_VALIDATE_RESULT(vkCreateBuffer(createInfo.gpu.device, &bufferInfo, nullptr, &stageBuffer));

	vkGetBufferMemoryRequirements(gpu.device, stageBuffer, &memoryReqs);
	MemoryRegionDescriptor stageRegion = stagingMemory.allocateRegion(memoryReqs.size, memoryReqs.alignment, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, memoryReqs.memoryTypeBits);
	VK_VALIDATE_RESULT(vkBindBufferMemory(gpu.device, stageBuffer, stageRegion.block->memoryHandle, stageRegion.offset));

	void* ptr;
	ptr = stageRegion.block->mapMemory(stageRegion);
	{
		memcpy(ptr, buffer, width * height * createInfo.bpp);
	}
	stageRegion.block->unmapMemory();

	mipLevels = createInfo.generateMipmaps ? static_cast<uint32_t>(std::floor(std::log2(std::max(width, height))) + 1) : 1;

	VkImageCreateInfo imageCreateInfo = {};
	imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	imageCreateInfo.pNext = nullptr;
	imageCreateInfo.flags = 0;
	imageCreateInfo.imageType = static_cast<VkImageType>(createInfo.type);
	imageCreateInfo.format = createInfo.format;
	imageCreateInfo.mipLevels = mipLevels;
	imageCreateInfo.arrayLayers = createInfo.arrayLevels;
	imageCreateInfo.samples = static_cast<VkSampleCountFlagBits>(createInfo.sampleCount);
	imageCreateInfo.tiling = static_cast<VkImageTiling>(createInfo.tiling);
	imageCreateInfo.usage = static_cast<VkImageUsageFlagBits>(createInfo.usageBits);
	imageCreateInfo.sharingMode = static_cast<VkSharingMode>(createInfo.sharingMode);
	imageCreateInfo.initialLayout = static_cast<VkImageLayout>(createInfo.initialLayout);
	imageCreateInfo.extent = { static_cast<uint32_t>(width), static_cast<uint32_t>(height), 1 };
	VK_VALIDATE_RESULT(vkCreateImage(gpu.device, &imageCreateInfo, nullptr, &image));

	vkGetImageMemoryRequirements(createInfo.gpu.device, image, &memoryReqs);

	MemoryRegionDescriptor region = memory.allocateRegion(memoryReqs.size, memoryReqs.alignment, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, memoryReqs.memoryTypeBits);

	VK_VALIDATE_RESULT(vkBindImageMemory(gpu.device, image, region.block->memoryHandle, region.offset));

	TemporaryCommandBuffer transferBuffer = gpu.createTemporaryBuffer(gpu.transferQueue, VK_COMMAND_BUFFER_LEVEL_PRIMARY);
	transferBuffer.begin();
	
		VkImageSubresourceRange subresourceRange = {};
		subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		subresourceRange.baseMipLevel = 0;
		subresourceRange.levelCount = 1;
		subresourceRange.layerCount = 1;

		VkImageLayout imageLayout;

		//GRAIL_LOG(DEBUG, "TEXTURE2D") << "Setting undefined -> transfer dst optimal...";
		setImageLayout(
			transferBuffer.getHandle(),
			VK_IMAGE_ASPECT_COLOR_BIT,
			VK_IMAGE_LAYOUT_UNDEFINED,
			VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
			subresourceRange
		);

		VkBufferImageCopy bufferCopyRegion = {};
		bufferCopyRegion.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		bufferCopyRegion.imageSubresource.mipLevel = 0;
		bufferCopyRegion.imageSubresource.baseArrayLayer = 0;
		bufferCopyRegion.imageSubresource.layerCount = createInfo.arrayLevels;
		bufferCopyRegion.imageExtent.width = width;
		bufferCopyRegion.imageExtent.height = height;
		bufferCopyRegion.imageExtent.depth = 1;

		vkCmdCopyBufferToImage(
			transferBuffer.getHandle(),
			stageBuffer,
			image,
			VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
			1,
			&bufferCopyRegion
		);

		//GRAIL_LOG(DEBUG, "TEXTURE2D") << "Setting transfer dst optimal -> transfer src optimal...";
		imageLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
		setImageLayout(
			transferBuffer.getHandle(),
			VK_IMAGE_ASPECT_COLOR_BIT,
			VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
			imageLayout,
			subresourceRange
		);
	
	transferBuffer.end();
	transferBuffer.submit();
	transferBuffer.dispose();

	TemporaryCommandBuffer blitBuffer = gpu.createTemporaryBuffer(gpu.graphicsQueue, VK_COMMAND_BUFFER_LEVEL_PRIMARY);
	blitBuffer.begin();

		for (int i = 1; i < mipLevels; i++) {
			VkImageBlit imageBlit = {};

			imageBlit.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			imageBlit.srcSubresource.layerCount = 1;
			imageBlit.srcSubresource.mipLevel = i - 1;
			imageBlit.srcOffsets[1].x = int32_t(width >> (i - 1));
			imageBlit.srcOffsets[1].y = int32_t(height >> (i - 1));
			imageBlit.srcOffsets[1].z = 1;


			imageBlit.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			imageBlit.dstSubresource.layerCount = 1;
			imageBlit.dstSubresource.mipLevel = i;
			imageBlit.dstOffsets[1].x = int32_t(width >> i);
			imageBlit.dstOffsets[1].y = int32_t(height >> i);
			imageBlit.dstOffsets[1].z = 1;

			VkImageSubresourceRange mipSubRange = {};
			mipSubRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			mipSubRange.baseMipLevel = i;
			mipSubRange.levelCount = 1;
			mipSubRange.layerCount = 1;

			//GRAIL_LOG(DEBUG, "TEXTURE2D") << "Setting undefined MIP -> transfer dst optimal MIP...";

			setImageLayout(
				blitBuffer.getHandle(),
				VK_IMAGE_ASPECT_COLOR_BIT,
				VK_IMAGE_LAYOUT_UNDEFINED,
				VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
				mipSubRange,
				VK_PIPELINE_STAGE_TRANSFER_BIT,
				VK_PIPELINE_STAGE_TRANSFER_BIT);

			vkCmdBlitImage(
				blitBuffer.getHandle(),
				image,
				VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
				image,
				VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
				1,
				&imageBlit,
				VK_FILTER_LINEAR);

			//GRAIL_LOG(DEBUG, "TEXTURE2D") << "Setting transfer dst optimal MIP -> trasfer src MIP...";

			setImageLayout(
				blitBuffer.getHandle(),
				VK_IMAGE_ASPECT_COLOR_BIT,
				VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
				VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
				mipSubRange,
				VK_PIPELINE_STAGE_TRANSFER_BIT,
				VK_PIPELINE_STAGE_TRANSFER_BIT);
		}

		subresourceRange.levelCount = mipLevels;

		//GRAIL_LOG(DEBUG, "TEXTURE2D") << "Setting transfer src optimal -> shader read only optimal...";
		setImageLayout(
			blitBuffer.getHandle(),
			VK_IMAGE_ASPECT_COLOR_BIT,
			VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
			VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
			subresourceRange);
	
	blitBuffer.end();
	blitBuffer.submit();
	blitBuffer.dispose();

	stagingMemory.clearBlocks();

	createSampler(createInfo.samplerInfo);

	VkImageViewCreateInfo view = {};
	view.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	view.pNext = nullptr;
	view.viewType = VK_IMAGE_VIEW_TYPE_2D;
	view.format = createInfo.format;
	view.components = { VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A };
	view.subresourceRange = { VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 };
	view.subresourceRange.levelCount = mipLevels;
	view.image = image;
	VK_VALIDATE_RESULT(vkCreateImageView(gpu.device, &view, nullptr, &imageView));

	imageLayout = imageLayout;

	/*getDescriptor() = {};
	getDescriptor().imageLayout = imageLayout;
	getDescriptor().imageView = imageView;
	getDescriptor().sampler = sampler;*/
/*
}*/