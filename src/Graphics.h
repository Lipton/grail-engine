#ifndef GRAIL_GRAPHICS_H
#define GRAIL_GRAPHICS_H

#include <stdint.h>

namespace grail {
	class Graphics {
		friend class Grail;
		friend class Swapchain;
		friend class VulkanContext;
	public:
		inline static uint32_t getWidth() { return width; }
		inline static uint32_t getHeight() { return height; }
		inline static int getFramesPerSecond() { return framesPerSecond; }
		inline static int getSmoothedFPS() { return smoothedFPS; }
		inline static double getDeltaTime() { return deltaTime; }
		inline static int getPrerenderedFrameCount() { return framesInFlight; }
		inline static int getFrameIndex() { return frameIndex; }
	private:
		Graphics& operator = (const Graphics&) = delete;
		Graphics(const Graphics&) = delete;
		Graphics() = default;

		static uint32_t width;
		static uint32_t height;
		static int framesPerSecond;
		static int smoothedFPS;
		static float deltaTime;
		static bool vSync;
		static int framesInFlight;
		static int frameIndex;
	};
}

#endif