#include "Editor.h"

#include "Grail.h"

#include "Common.h"
#include "DeferredRenderer.h"
#include "Scene.h"
#include "Resources.h"
#include "Material.h"
#include "PerspectiveCamera.h"
#include "FirstPersonCameraController.h"
#include "Script.h"
#include "FileIO.h"
#include "Utils.h"
#include "SceneLoader.h"
#include "TextEditor.h"
#include "FrameGraph.h"
#include "ProjectStructureViewer.h"

#include <iomanip>
#include <scripthandle/scripthandle.h>
#include "VR.h"
//#include <openvr.h>

grail::ImGuiConsole* grail::Editor::console = new grail::ImGuiConsole();

int grail::Editor::parseNode(grail::SceneNode* node, int index, int* node_clicked, int selection_mask, grail::SceneNode** newRoot) {
	ImGuiTreeNodeFlags node_flags = (index == 0 ? ImGuiTreeNodeFlags_DefaultOpen : 0) | ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick | (index == *node_clicked ? ImGuiTreeNodeFlags_Selected : 0);

	if (node->getChildCount() == 0) {
		node_flags |= ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen; // ImGuiTreeNodeFlags_Bullet
	}

	std::string name = "";
	ImVec4 color = ImVec4(1.0, 1.0, 0.0, 1.0);

	nodeComponents::Render* renderComponent = Scene::getComponent<nodeComponents::Render>(node);
	nodeComponents::ScriptComponent* scriptComponent = Scene::getComponent<nodeComponents::ScriptComponent>(node);

	if(node->isPrefab()) {
		color = ImVec4(0.2, 0.2, 1.0, 1.0);
	}
	else if (renderComponent) {
		if(renderComponent->render)
			color = ImVec4(1.0, 1.0, 1.0, 1.0);
		else
			color = ImVec4(0.3, 0.3, 0.3, 1.0);
	}

	if (scriptComponent) {
		name.append("[s] ");
	}

	static grail::SceneNode* from;
	static grail::SceneNode* to;

	name.append(node->name.c_str());

	// Node
	ImGui::PushStyleColor(ImGuiCol_Text, color);
	bool node_open = ImGui::TreeNodeEx((void*)(intptr_t)index, node_flags, name.c_str(), "");
	ImGui::PopStyleColor();
	if (ImGui::BeginDragDropSource(ImGuiDragDropFlags_SourceNoHoldToOpenOthers | ImGuiDragDropFlags_SourceNoDisableHover)) {
		ImGui::Text("Moving \"%s\"", name.c_str());
		ImGui::SetDragDropPayload("SCENE_REORDER", &node, sizeof(grail::SceneNode * *));
		ImGui::EndDragDropSource();
	}
	if (ImGui::BeginDragDropTarget()) {
		if (const ImGuiPayload * payload = ImGui::AcceptDragDropPayload("SCENE_REORDER", 0))
		{
			from = *(grail::SceneNode * *)payload->Data;
			to = node;
		}
		ImGui::EndDragDropTarget();
	}
	if (UI::IsMouseReleased(0) && UI::IsItemHovered(ImGuiHoveredFlags_None)) {
		*node_clicked = index;
		*newRoot = node;
	}
	nodeContextMenu(node);
	if (node_open) {
		if (node->getChildren().size() > 0) {
			for (grail::SceneNode* n : node->getChildren()) {
				int res = parseNode(n, ++index, node_clicked, selection_mask, newRoot);
				index = res;
			}
			ImGui::TreePop();
		}
	}

	if (from && to) {
		if (from->getParent()) {
			from->getParent()->removeChild(from);
		}

		glm::vec3 position = from->transform.getWorldPosition();
		glm::quat rotation = from->transform.getWorldRotation();
		glm::vec3 scale = from->transform.getWorldScale();
		
		to->addChild(from);

		from->updateTransform(true);
		from->transform.setWorldPosition(position);
		from->transform.setWorldRotation(rotation);
		from->transform.setWorldScale(scale);

		ImGui::SetDragDropPayload("SCENE_REORDER", &to, sizeof(grail::SceneNode * *));

		from = nullptr;
		to = nullptr;
	}
	//ImGui::TreePop();

	return index;

}

void grail::Editor::nodeContextMenu(grail::SceneNode* node) {
	if (node == Scene::getRootNode()) return;

	if (ImGui::BeginPopupContextItem()) {

		if (ImGui::Selectable("Add New Child")) {
			node->addChild(Scene::createNode());
			ImGui::EndPopup();

			return;
		}
		if (ImGui::Selectable("Delete Node")) {
			if (ui.selectedNode == node) {
				if (node->getParent()) {
					ui.selectedNode = node->getParent();
				}
				else {
					ui.selectedNode = Scene::getRootNode();
				}
			}

			Scene::removeNode(node);
			ImGui::EndPopup();

			return;
		}
		if (ImGui::Selectable("Clone Node")) {
			SceneNode* copy = Scene::cloneNode(ui.selectedNode);
			ui.selectedNode = copy;
			ImGui::EndPopup();
			ImGui::TreePop();
			return;
		}
		if (ImGui::BeginMenu("Add Component")) {
			for (auto& item : nodeComponents::detail::componentAssignMap) {
				bool present = ui.selectedNode->getComponentFromMap(item.first);
				if (ImGui::Selectable((item.first + " Component").c_str(), &present, present ? ImGuiSelectableFlags_Disabled : 0)) {
					ui.selectedNode->createComponentFromMap(item.first);

					if (!item.first.compare("Render")) {
						nodeComponents::Render* renderComponent = Scene::getComponent<nodeComponents::Render>(ui.selectedNode);
						renderComponent->node = ui.selectedNode;
						renderComponent->mesh = Resources::getMesh("sphere");
						renderComponent->material = Resources::getMaterial("default")->getBaseMaterial()->createInstance();
					}

					if (!item.first.compare("Script")) {
						nodeComponents::ScriptComponent* scriptComponent = Scene::getComponent<nodeComponents::ScriptComponent>(ui.selectedNode);
						scriptComponent->node = ui.selectedNode;
					}

					if (!item.first.compare("Physics")) {
						nodeComponents::PhysicsComponent* physicsComponent = Scene::getComponent<nodeComponents::PhysicsComponent>(ui.selectedNode);
						nodeComponents::Render* renderComponent = Scene::getComponent<nodeComponents::Render>(ui.selectedNode);
						//Physics::createShapeForMesh(ui.selectedNode, renderComponent->mesh, &physicsComponent->shape, &physicsComponent->body);
						physicsComponent->node = ui.selectedNode;
					}
				}
			}
			ImGui::EndMenu();
		}
		ImGui::EndPopup();
	}
}

grail::SceneNode* grail::Editor::castPickRay() {
		glm::vec2 mCoords = glm::vec2(grail::Input::getMouseX(), grail::Input::getMouseY());

		mCoords.x -= ui.mainViewBounds.x;
		mCoords.y -= ui.mainViewBounds.y;

		if (mCoords.x < 0 || mCoords.y < 0 || mCoords.x > ui.mainViewBounds.z || mCoords.y > ui.mainViewBounds.w) {
			return nullptr;
		}

		float ratioX = mCoords.x / ui.mainViewBounds.z;
		float ratioY = mCoords.y / ui.mainViewBounds.w;

		mCoords.x = ratioX * Graphics::getWidth();
		mCoords.y = ratioY * Graphics::getHeight();

		glm::vec3 v0 = camera->unproject(glm::vec3(float(mCoords.x), float(mCoords.y), 0.0f));
		glm::vec3 v1 = camera->unproject(glm::vec3(float(mCoords.x), float(mCoords.y), 1.0f));

		glm::vec3 dir = (v1 - v0);

		glm::vec3 intersection;

		return Scene::castRay(camera->position, dir, intersection);
}

using namespace grail;

void FileLogger::print(std::stringstream & ss, time_t timestamp, grail::LogType type, const char * log, const char * tag) {
	long minutes = timestamp / (1000 * 60);
	long seconds = timestamp / (1000) % 60;

	ss << "[" << (minutes <= 9 ? "0" : "") << minutes << ":" << (seconds <= 9 ? "0" : "") << seconds << "] [" << grail::logger::logTypeToString(type) << "] [" << tag << "]: " << log << "\n";

	outputMessage(ss.str().c_str());

#ifndef PLATFORM_MOBILE
	logFile << ss.str().c_str();
#endif
	Editor::console->log(type, tag, ss.str());
}

void grail::Editor::setState(EditorState newState) {
	if (newState == EditorState::STOPPED) {
		ui.enabled = true;
		Input::captureMouse(false);

		Scene::clear();
		SceneLoader::loadSceneFromMemory(lastSave);
		/*Scene::getNodesWithComponents<nodeComponents::ScriptComponent>().each([](auto& c) {
			//c.instanciated = false;
			c.instanciate();
		});*/

		std::string scriptHandle;
		if (ui.editedScript.handle) {
			scriptHandle = ui.editedScript.handle->getName();
		}

		ui.selectedNode = nullptr;
		ui.inspectedMesh = nullptr;
		ui.inspectedTexture = nullptr;
		ui.editedScript.handle = Resources::getScript(scriptHandle);
		ui.editedScript.node = nullptr;
	}

	if (newState == EditorState::PAUSED) {
		ui.enabled = true;
		mouseState = Input::isMouseCaptured();
		Input::captureMouse(false);
	}

	if (newState == EditorState::PLAYING) {
		if (state == EditorState::STOPPED) {
			Input::captureMouse(true);
			/*ScriptProvider::recompile();
			if (!ScriptProvider::isCompiled()) return;/

			/*Scene::getNodesWithComponents<nodeComponents::ScriptComponent>().each([](auto& c) {
				c.setup();
			});*/

			SceneLoader::saveSceneToString(lastSave);
			Scene::start();
		}

		if (state == EditorState::PAUSED) {
			Input::captureMouse(mouseState);
		}

		cameraController->resetMouse();
		ui.enabled = false;
	}

	state = newState;
}

//vr::IVRSystem* vrSystem;

/*glm::mat4 getHMDMatrixProjectionEye(vr::Hmd_Eye eye) {
	vr::HmdMatrix44_t mat = vrSystem->GetProjectionMatrix(eye, 0.2f, 256.0f);

	return glm::mat4(
		mat.m[0][0], mat.m[1][0], mat.m[2][0], mat.m[3][0],
		mat.m[0][1], mat.m[1][1], mat.m[2][1], mat.m[3][1],
		mat.m[0][2], mat.m[1][2], mat.m[2][2], mat.m[3][2],
		mat.m[0][3], mat.m[1][3], mat.m[2][3], mat.m[3][3]
	);
}

glm::mat4 getHMDMatrixPoseEye(vr::Hmd_Eye eye) {
	vr::HmdMatrix34_t matEyeRight = vrSystem->GetEyeToHeadTransform(eye);
	
	glm::mat4 matrixObj(
		matEyeRight.m[0][0], matEyeRight.m[1][0], matEyeRight.m[2][0], 0.0,
		matEyeRight.m[0][1], matEyeRight.m[1][1], matEyeRight.m[2][1], 0.0,
		matEyeRight.m[0][2], matEyeRight.m[1][2], matEyeRight.m[2][2], 0.0,
		matEyeRight.m[0][3], matEyeRight.m[1][3], matEyeRight.m[2][3], 1.0f
	);

	return glm::inverse(matrixObj);
}

glm::mat4 convertSteamVRMatrixToMatrix4(const vr::HmdMatrix34_t& matPose) {
	glm::mat4 matrixObj(
		matPose.m[0][0], matPose.m[1][0], matPose.m[2][0], 0.0,
		matPose.m[0][1], matPose.m[1][1], matPose.m[2][1], 0.0,
		matPose.m[0][2], matPose.m[1][2], matPose.m[2][2], 0.0,
		matPose.m[0][3], matPose.m[1][3], matPose.m[2][3], 1.0f
	);
	return matrixObj;
}

vr::TrackedDevicePose_t m_rTrackedDevicePose[vr::k_unMaxTrackedDeviceCount];
int m_iTrackedControllerCount;
int m_iTrackedControllerCount_Last;
int m_iValidPoseCount;
int m_iValidPoseCount_Last;
bool m_bShowCubes;

std::string m_strPoseClasses;                            // what classes we saw poses for this frame
char m_rDevClassChar[vr::k_unMaxTrackedDeviceCount];   // for each device, a character representing its class
glm::mat4 m_rmat4DevicePose[vr::k_unMaxTrackedDeviceCount];

int m_iSceneVolumeWidth;
int m_iSceneVolumeHeight;
int m_iSceneVolumeDepth;
float m_fScaleSpacing;
float m_fScale;

glm::mat4 m_mat4HMDPose;

void updateHMDMatrixPose() {
	vr::VRCompositor()->WaitGetPoses(m_rTrackedDevicePose, vr::k_unMaxTrackedDeviceCount, NULL, 0);

	m_iValidPoseCount = 0;
	m_strPoseClasses = "";
	for (int nDevice = 0; nDevice < vr::k_unMaxTrackedDeviceCount; ++nDevice)
	{
		if (m_rTrackedDevicePose[nDevice].bPoseIsValid)
		{
			m_iValidPoseCount++;
			m_rmat4DevicePose[nDevice] = convertSteamVRMatrixToMatrix4(m_rTrackedDevicePose[nDevice].mDeviceToAbsoluteTracking);
			if (m_rDevClassChar[nDevice] == 0)
			{
				switch (vrSystem->GetTrackedDeviceClass(nDevice))
				{
				case vr::TrackedDeviceClass_Controller:        m_rDevClassChar[nDevice] = 'C'; break;
				case vr::TrackedDeviceClass_HMD:               m_rDevClassChar[nDevice] = 'H'; break;
				case vr::TrackedDeviceClass_Invalid:           m_rDevClassChar[nDevice] = 'I'; break;
				case vr::TrackedDeviceClass_GenericTracker:    m_rDevClassChar[nDevice] = 'G'; break;
				case vr::TrackedDeviceClass_TrackingReference: m_rDevClassChar[nDevice] = 'T'; break;
				default:                                       m_rDevClassChar[nDevice] = '?'; break;
				}
			}
			m_strPoseClasses += m_rDevClassChar[nDevice];
		}
	}

	if (m_rTrackedDevicePose[vr::k_unTrackedDeviceIndex_Hmd].bPoseIsValid)
	{
		m_mat4HMDPose = m_rmat4DevicePose[vr::k_unTrackedDeviceIndex_Hmd];
		m_mat4HMDPose = glm::inverse(m_mat4HMDPose);
	}
}*/

void Editor::init() {
	camera = new grail::PerspectiveCamera(90, grail::Graphics::getWidth(), grail::Graphics::getHeight());

	camera->direction.x = -0.754;
	camera->direction.y = -0.551;
	camera->direction.z = -0.359;

	camera->nearPlane = 0.2f;
	camera->farPlane = 256.0f;
	camera->update();

#ifdef OS_ANDROID
	camera->position = glm::vec3(432, 58, 332);
	camera->direction = glm::vec3(0.98, 0.04, -0.22);
#endif

	cameraController = new grail::FirstPersonCameraController(camera);
	imguiHelper = new grail::ImGuiHelper();
	ui.textEditor = new TextEditor();
	ui.textEditor->SetLanguageDefinition(TextEditor::LanguageDefinition::AngelScript());
	ui.textEditor->SetPalette(ui.textEditor->GetDarkPalette());
	ui.textEditor->SetShowWhitespaces(false);
	ui.textEditor->SetTabSize(8);

	ui.selectedNode = Scene::getRootNode();

	structureViewer = new ProjectStructureViewer();

	{
		uint32_t size = 0;
		char* contents = nullptr;

		FileIO::readBinaryFile("layout.ini", &contents, &size);

		for (uint32_t i = 0; i < ui.handles.windows.size(); i++) {
			ui.handles.windows[i] = contents[i] == '1' ? true : false;
		}

		delete[] contents;
	}

	ui.fpsGraph.resize(ui.fpsGraphSamples);

	getInputProcessor().setOnDroppedFiles([&](int count, const char** paths) {
		for (int i = 0; i < count; i++) {
			std::string file(paths[i]);
			std::string extension = grail::FileIO::getExtension(file);

			std::string filename;
			std::string::size_type idx;
			idx = file.rfind("assets\\");
			if (idx != std::string::npos) {
				filename = file.substr(idx + 7);
			}

			if (grail::utils::string::equalsCaseInsensitive(extension, "obj") ||
				grail::utils::string::equalsCaseInsensitive(extension, "fbx") ||
				grail::utils::string::equalsCaseInsensitive(extension, "gltf") ||
				grail::utils::string::equalsCaseInsensitive(extension, "ply")) {

				Resources::loadModel(filename, nullptr, true);


				utils::string::replaceAll(filename, "\\", "/");

				ui.meshFilter = filename;
			}

			if (grail::utils::string::equalsCaseInsensitive(extension, "png") ||
				grail::utils::string::equalsCaseInsensitive(extension, "jpg") ||
				grail::utils::string::equalsCaseInsensitive(extension, "jpeg") ||
				grail::utils::string::equalsCaseInsensitive(extension, "dds") ||
				grail::utils::string::equalsCaseInsensitive(extension, "tga")) {

				TextureCreateInfo info = {};
				info.samplerInfo.maxAnisotropy = 16.0f;
				info.samplerInfo.anisotropyEnable = true;

				grail::Resources::loadTexture(filename, info);
			}

			if (grail::utils::string::equalsCaseInsensitive(extension, "mat")) {
			}

			if (grail::utils::string::equalsCaseInsensitive(extension, "scn")) {

			}
		}
		});

	getInputProcessor().setOnMouseMoved([&](float x, float y) {
		if (grail::Input::isMouseCaptured()) {
			if (ui.enabled && !ui.moving) return;


			float dx = x - grail::Input::getMouseTouchX();
			float dy = y - grail::Input::getMouseTouchY();

			offsetX = dx;
			offsetY = dy;

			if (!ImGui::GetIO().WantCaptureMouse || !ui.enabled)
				cameraController->updateMouseMoved(virtualX + offsetX, virtualY + offsetY);

		}
		});

	getInputProcessor().setOnKeyPress([&](int keycode, int action) {
		imguiHelper->handleKeyPress(keycode, action);

		if (keycode == grail::Keys::GRAVE_ACCENT && action == GRAIL_KEY_PRESS) {
			if (!ImGui::GetIO().WantTextInput) {
				ui.enabled = !ui.enabled;
			}
		}

		if (keycode == grail::Keys::ESCAPE && action == GRAIL_KEY_PRESS) {
			if (state == EditorState::PLAYING)
				setState(EditorState::PAUSED);
		}
	});

	getInputProcessor().setOnTypedChar([&](unsigned int c, int mods) {
		imguiHelper->handleKeyTyped(c, mods);
	});

	getInputProcessor().setOnMousePress([&](int keycode, int action) {
		if (keycode == grail::Buttons::LEFT && action == GRAIL_MOUSE_PRESS) {
			double timePassed = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count() - lastClickTime;

			lastClickTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();

			if (timePassed <= 220) { /* DOUBLE CLICK */ }
		}

		if (keycode == grail::Buttons::LEFT && action == GRAIL_MOUSE_RELEASE) {
			virtualX += offsetX;
			virtualY += offsetY;

			offsetX = 0;
			offsetY = 0;
		}

		if (keycode == grail::Buttons::RIGHT && action == GRAIL_MOUSE_PRESS) {

		}
		});

	using namespace grail;
	renderer = new grail::DeferredRenderer(camera);
	renderer->setPostRenderFunction(
		[&](RenderPass* pass, VkCommandBuffer& cmd) {
			if (ui.enabled) {
				SceneNode* selectedNode = ui.selectedNode;

				if (selectedNode && ui.highlightSelected) {

					nodeComponents::Render* comp = Scene::getComponent<nodeComponents::Render>(selectedNode);
					if (comp) {
						struct Consts {
							glm::mat4 combined;
							glm::mat4 model;
						} consts;

						consts.combined = camera->combined;
						consts.model = selectedNode->transform.getWorldMatrix();

						pass->bindMaterial(cmd, highlighMaterialInstance);
						pass->setPushConstants(cmd, highlighMaterialInstance, &consts);
						pass->drawMesh(cmd, comp->mesh);


						Physics::debugRenderer->begin(camera, cmd);
						//Physics::debugRenderer->drawLine(glm::mat4(1.0), glm::vec3(0.0), camera->position + glm::vec3(0, 10, 0), glm::vec3(1.0, 0.0, 0.0));
						if (comp->mesh->boundsMin != glm::vec3(FLT_MAX) || comp->mesh->boundsMax != glm::vec3(-FLT_MAX)) {
							Physics::debugRenderer->drawBox(glm::mat4(1.0), comp->node->transform.getWorldBoundsMin(), comp->node->transform.getWorldBoundsMax(), glm::vec3(1.0, 1.0, 0.0));
						}

						Physics::debugRenderer->end(camera, cmd);

					}
				}

				if (ui.debugPhysics)
					Physics::drawDebug(camera, cmd);

				imguiHelper->render(cmd, [&] {
					renderUI();
				});
			}

		}
	);

	const VkRenderPass& present = renderer->getFrameGraph()->getPass("Present")->getRenderPass();

	imguiHelper->initialize(present);
	Physics::initializeDebug(present);

	MaterialDescription* description = Resources::loadMaterial("default", "materials/default.mat", renderer->getFrameGraph());
	
	{
		GraphicsPipelineCreateInfo pipelineInfo = {};
		pipelineInfo.colorBlendState.attachmentCount = 1;
		pipelineInfo.renderPass = present;

		Shader* vS = Resources::loadSPIRVShader("shaders/graph/highlight.vert");
		Shader* fS = Resources::loadSPIRVShader("shaders/graph/highlight.frag");

		pipelineInfo.rasterizationState.cullMode = CullMode::NONE;
		pipelineInfo.rasterizationState.polygonMode = PolygonMode::LINE;

		pipelineInfo.colorBlendState.blendEnable = VK_TRUE;
		pipelineInfo.colorBlendState.srcColorBlendFactor = BlendFactor::SRC_ALPHA;
		pipelineInfo.colorBlendState.dstColorBlendFactor = BlendFactor::ONE_MINUS_SRC_ALPHA;
		pipelineInfo.colorBlendState.srcAlphaBlendFactor = BlendFactor::ONE_MINUS_SRC_ALPHA;
		pipelineInfo.colorBlendState.dstAlphaBlendFactor = BlendFactor::ZERO;

		DescriptorLayout layout;
		layout.addPushConstant({ ShaderStageBits::VERTEX, 0, sizeof(glm::mat4) * 2 });
		layout.finalize();

		VertexLayout vertexLayout =  VertexLayout({
			VertexAttribute(VertexAttributeType::POSITION, VK_FORMAT_R32G32B32_SFLOAT, 3, sizeof(float)),
			VertexAttribute(VertexAttributeType::NORMAL, VK_FORMAT_R32G32B32_SFLOAT, 3, sizeof(float)),
			VertexAttribute(VertexAttributeType::TANGENT, VK_FORMAT_R32G32B32_SFLOAT, 3, sizeof(float)),
			VertexAttribute(VertexAttributeType::UV, VK_FORMAT_R32G32B32_SFLOAT, 3, sizeof(float))
		});

		highlightMaterial = new MaterialDescription("EditorHighlighMaterial");
		Material* base = highlightMaterial->createMaterial(vertexLayout, layout, { vS->getBasePermutation(), fS->getBasePermutation() }, pipelineInfo);
		highlighMaterialInstance = base->createInstance();

		ui.inspectedMeshCamera = new PerspectiveCamera(67, 512, 512);

		meshDisplayGraph = new FrameGraph(VulkanContext::mainGPU);

		AttachmentInfo info = {};
		info.width = 512;
		info.height = 512;
		info.format = VK_FORMAT_R8G8B8A8_UNORM;
		info.initialLayout = ImageLayout::SHADER_READ_ONLY_OPTIMAL;

		RenderAttachment& display = meshDisplayGraph->createAttachment("mesh_display", info);

		info.format = VK_FORMAT_D32_SFLOAT;
		RenderAttachment& depth = meshDisplayGraph->createAttachment("mesh_display_depth", info);

		RenderPass& pass = meshDisplayGraph->createPass("mesh_display_pass", FrameGraphQueueBits::GRAPHICS);
		pass.createMaterialFromShaders("shaders/graph/mesh_display.vert", "shaders/graph/mesh_display.frag");
		pass.addOutputAttachment(display);
		pass.setDepthStencilOutputAttachment(depth);
		pass.addInputTexture(Resources::getTexture("IrradianceCube"));

		UISettings& settings = ui;

		pass.setOnRender([&pass, &settings, &display, &depth](VkCommandBuffer& cmd) {
			MaterialInstance* material = pass.getDefaultMaterial();
			Mesh* mesh = settings.inspectedMesh;

			settings.inspectedMeshTime += Graphics::getDeltaTime();

			struct {
				glm::mat4 model = glm::mat4(1.0f);
				glm::mat4 combined = glm::mat4(1.0f);
			} push;

			push.combined = settings.inspectedMeshCamera->combined;

			glm::vec3 pivot = mesh->boundsMin + mesh->boundsMax;


			push.model = glm::translate(push.model, glm::vec3(pivot.x * 0.5f, pivot.y * 0.5f, pivot.z * 0.5f));
			push.model = glm::rotate(push.model, settings.inspectedMeshTime, glm::vec3(0, 1, 0));
			push.model = glm::translate(push.model, -glm::vec3(pivot.x * 0.5f, pivot.y * 1.0f, pivot.z * 0.5f));


			display.getTexture()->transitionImage(cmd, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);
			depth.getTexture()->transitionImage(cmd, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);

			pass.beginRenderPass(cmd);
			pass.bindMaterial(cmd, material);
			pass.setPushConstants(cmd, material, &push);
			pass.drawMesh(cmd, mesh);
			pass.endRenderPass(cmd);

			display.getTexture()->transitionImage(cmd, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
			});

		meshDisplayGraph->setFinalPass(pass);
		meshDisplayGraph->buildGraph();

	}

	/*GRAIL_LOG(INFO, "VR") << "intializing VR";
	vr::EVRInitError initError = vr::VRInitError_None;
	vrSystem = vr::VR_Init(&initError, vr::VRApplication_Scene);

	
	if (initError != vr::VRInitError_None) {
		GRAIL_LOG(ERROR, "VR ERROR") << vr::VR_GetVRInitErrorAsEnglishDescription(initError);
	}
	else {
		GRAIL_LOG(INFO, "VR INIT") << "Vr Initialized";
	}

	vr::EVRInitError peError = vr::VRInitError_None;

	if (!vr::VRCompositor())	{
		GRAIL_LOG(ERROR, "VR COMPOSITOR") << ("Compositor initialization failed. See log file for details\n");
	}
	memset(m_rDevClassChar, 0, sizeof(m_rDevClassChar));*/
}

void Editor::renderUI() {
	// Left Bar
	ImGui::SetNextWindowSize(ImVec2(Graphics::getWidth() * ui.mainPadding, Graphics::getHeight() - 35 * UI::scale), ImGuiCond_Always);
	ImGui::SetNextWindowPos(ImVec2(0, 35 * UI::scale), ImGuiCond_Always);
	ImGui::Begin("Left_Dock_Bar", nullptr, ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoTitleBar);

	ui.handles.leftBarDockSpace = ImGui::GetID("Left_Dockspace");
	ImGui::DockSpace(ui.handles.leftBarDockSpace, ImVec2(0.0f, 0.0f), ImGuiDockNodeFlags_None | ImGuiDockNodeFlags_PassthruCentralNode);

	ImGui::End();

	// Right Bar
	ImGui::SetNextWindowSize(ImVec2(Graphics::getWidth() * ui.mainPadding, Graphics::getHeight() - 35 * UI::scale), ImGuiCond_Always);
	ImGui::SetNextWindowPos(ImVec2(Graphics::getWidth() - Graphics::getWidth() * ui.mainPadding, 35 * UI::scale), ImGuiCond_Always);
	ImGui::Begin("Right_Dock_Bar", nullptr, ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoTitleBar);

	ui.handles.leftBarDockSpace = ImGui::GetID("Right_Dockspace");
	ImGui::DockSpace(ui.handles.leftBarDockSpace, ImVec2(0.0f, 0.0f), ImGuiDockNodeFlags_None | ImGuiDockNodeFlags_PassthruCentralNode);

	ImGui::End();

	// Bottom Bar
	ImGui::SetNextWindowSize(ImVec2(Graphics::getWidth() - (Graphics::getWidth() * (ui.mainPadding * 2)), Graphics::getHeight() * (ui.mainPadding * 2)), ImGuiCond_Always);
	ImGui::SetNextWindowPos(ImVec2(Graphics::getWidth() * ui.mainPadding, Graphics::getHeight() - (Graphics::getHeight() * (ui.mainPadding * 2))), ImGuiCond_Always);
	ImGui::Begin("Bottom_Dock_Bar", nullptr, ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoTitleBar);

	ui.handles.bottomBarDockSpace = ImGui::GetID("Bottom_Dockspace");
	ImGui::DockSpace(ui.handles.bottomBarDockSpace, ImVec2(0.0f, 0.0f), ImGuiDockNodeFlags_None | ImGuiDockNodeFlags_PassthruCentralNode);

	ImGui::End();

	// Performance Metrics Window
	EditorWindowType currentType = EditorWindowType::PERFORMACE_METRICS;

	if (ui.handles.windows[static_cast<uint32_t>(currentType)]) {
		ImGui::Begin(editorWindowNames[currentType].c_str(), &ui.handles.windows[static_cast<uint32_t>(currentType)]);
		if (ImGui::CollapsingHeader("Timings", ImGuiTreeNodeFlags_DefaultOpen)) {
			std::string currentFPS = "FPS: ";
			currentFPS.append(std::to_string(Graphics::getSmoothedFPS()));
			ImGui::PlotLines(currentFPS.c_str(), &ui.fpsGraph[0], ui.fpsGraphSamples, 0, "", 0.0f, FLT_MAX, ImVec2(0, Graphics::getHeight() * 0.05f));

			std::vector<const RenderPass*> passes;
			renderer->getFrameGraph()->getActivePasses(passes);


			if (ImGui::TreeNodeEx("GPU Timings", ImGuiTreeNodeFlags_DefaultOpen)) {
				double totalTime = 0;

				for (const RenderPass* pass : passes) {
					double time = pass->getAvgGPUTime();

					std::stringstream ss;
					ss << pass->getName() << ": " << std::setprecision(2) << time << "ms" << "###GPUtiming" << pass->getName();

					if (ImGui::TreeNodeEx(ss.str().c_str())) {
						ImGui::Text("Min: %.2fms\nMax: %.2fms", pass->getMinGPUTime(), pass->getMaxGPUTime());
					}

					totalTime += time;
					//ImGui::Text("%s: %.2fms [%.2fms / %.2fms]", pass->getName().c_str(), time, pass->getMinGPUTime(), pass->getMaxGPUTime());
				}

				ImGui::NewLine();
				ImGui::Text("%s: %.2fms", "Total", totalTime);

				ImGui::TreePop();
			}

			ImGui::Separator();

			if (ImGui::TreeNodeEx("CPU Timings")) {
				double totalTime = 0;

				for (const RenderPass* pass : passes) {
					double time = pass->getAvgCPUTime();

					std::stringstream ss;
					ss << pass->getName() << ": " << std::setprecision(2) << time << "ms" << "###CPUtiming" << pass->getName();

					if (ImGui::TreeNodeEx(ss.str().c_str())) {
						ImGui::Text("Min: %.2fms\nMax: %.2fms", pass->getMinCPUTime(), pass->getMaxCPUTime());
					}

					totalTime += time;
				}

				ImGui::NewLine();
				ImGui::Text("%s: %.2fms", "Total", totalTime);
				ImGui::Text("%s: %.2fms", "Scene Update", ui.CPUOverhead);

				ImGui::TreePop();
			}

			ImGui::Separator();
		}

		if (ImGui::CollapsingHeader("Camera", ImGuiTreeNodeFlags_DefaultOpen)) {
			ImGui::InputFloat("Speed ", &ui.cameraSpeed, 2);
			ImGui::InputFloat("Speed Mod ", &ui.cameraSpeedMod, 2);

			glm::vec4 viewport = glm::vec4(camera->viewportWidth, camera->viewportHeight, camera->nearPlane, camera->farPlane);
			ImGui::InputFloat4("Viewport ", &viewport[0], 2);
			camera->viewportWidth = viewport.x;
			camera->viewportHeight = viewport.y;
			camera->nearPlane = viewport.z;
			camera->farPlane = viewport.w;

			ImGui::InputFloat3("Position", &camera->position[0], 2);
			ImGui::InputFloat3("Direction", &camera->direction[0], 2);
		}

		if (ImGui::CollapsingHeader("Renderer", ImGuiTreeNodeFlags_DefaultOpen)) {
			ImGui::InputFloat("Aperture", &renderer->settings.cameraAperture);
			ImGui::InputFloat("Shutter Speed", &renderer->settings.cameraShutterSpeed);
			ImGui::InputFloat("Sensitivity", &renderer->settings.cameraSensitivity);
			ImGui::InputFloat("Exposure", &renderer->settings.computedExposure);
			ImGui::Separator();

			ImGui::InputInt("Motion Blur Samples", &renderer->settings.motionBlurSamples);
			//ImGui::Checkbox("Use New Motion Blur", &settings.useNewMotionBlur);
			ImGui::Separator();

			ImGui::Checkbox("Freeze View Frustrum", &renderer->settings.freezeViewFrustrum);
			ImGui::Separator();

			ImGui::SliderFloat3("Sunlight Direction", &renderer->settings.directionalLightDirection[0], -1.0f, 1.0f);
			ImGui::ColorEdit3("Sunlight Color", &renderer->settings.directionalLightColor[0], ImGuiColorEditFlags_HDR | ImGuiColorEditFlags_Float);
			ImGui::SliderFloat("Sunlight Intensity", &renderer->settings.directionalLightIntensity, 0.0f, 100.0f);
			ImGui::SliderFloat("Cascade Lambda", &renderer->settings.cascadeSplitLambda, 0.0f, 1.0f);
			ImGui::Separator();

			ImGui::Checkbox("Enable TXAA", &renderer->settings.TAAEnabled);
			ImGui::Checkbox("Advanced TXAA", &renderer->settings.advancedTAA);
			ImGui::SliderFloat("Jitter Intensity", &renderer->settings.jitterIntensity, 0.0f, 3.0f);
		}
		ImGui::End();
	}

	currentType = EditorWindowType::SCENE_HIERARCHY;
	if (ui.handles.windows[static_cast<uint32_t>(currentType)]) {
		ImGui::Begin(editorWindowNames[currentType].c_str(), &ui.handles.windows[static_cast<uint32_t>(currentType)]);

		ImGui::BeginChild("hierarchy_container", ImVec2(0, -70 * UI::scale));
		
		if (ui.nodeFilter.length() == 0) {
			int index = 0;
			static int selection_mask = 0;

			parseNode(Scene::getRootNode(), index, &ui.node_clicked, selection_mask, &ui.selectedNode);

			if (ui.node_clicked != -1) {
				selection_mask = ui.node_clicked;
			}

			if (!ui.selectedNode) {
				ui.selectedNode = Scene::getRootNode();
			}
		}
		else {
			std::vector<SceneNode*> nodes;
			Scene::getRootNode()->gatherChildren(nodes);

			std::vector<SceneNode*> filteredNodes;

			for (SceneNode* node : nodes) {
				if (utils::string::containsCaseInsensitive(node->name, ui.nodeFilter)) {
					filteredNodes.push_back(node);
				}
			}

			for (SceneNode* node : filteredNodes) {
				ImVec4 color = ImVec4(1.0, 1.0, 1.0, 1.0);

				if (!node->getComponentFromMap("Render")) {
					color = ImVec4(1.0, 1.0, 0.0, 1.0);
				}

				ImGui::PushStyleColor(ImGuiCol_Text, color);
				ImGui::TreeNodeEx(node->name.c_str(), ImGuiTreeNodeFlags_Leaf);
				if (ImGui::IsItemClicked()) {
					ui.selectedNode = node;
				}
				ImGui::TreePop();
				ImGui::PopStyleColor();
			}
		}
		ImGui::EndChild();
		
		ImGui::Separator();
		
		ImGui::Text("Seach ");
		ImGui::SameLine();
		ImGui::InputText("##node_search", &ui.nodeFilter);
		ImGui::SameLine();
		if (ImGui::Button("Clear##node")) ui.nodeFilter.clear();
		if (ImGui::Button("Add New Node")) Scene::createNode();

		ImGui::End();
	}

	currentType = EditorWindowType::NODE_PROPERTIES;
	if (ui.handles.windows[static_cast<uint32_t>(currentType)]) {
		ImGui::Begin(editorWindowNames[currentType].c_str(), &ui.handles.windows[static_cast<uint32_t>(currentType)]);
		if (ui.selectedNode) {
			SceneNode* node = ui.selectedNode;

			if (!ui.renaming)
				ImGui::Text("%s%s", "Node Name: ", node->name.c_str());
			else {
				ImGui::Text("%s", "Node Name: ");
				ImGui::SameLine();
				ImGui::InputText("##node_rename_field", &ui.tempString);
			}
			if (ui.selectedNode->getDerivedPrefab() != nullptr) {
				UI::Text("Derived From: %s", ui.selectedNode->getDerivedPrefab()->name.c_str());
			}

			if (ImGui::Button("Rename##node_rename")) {
				if (!ui.renaming) {
					ui.renaming = true;
					ui.tempString = node->name;
				}
				else {
					if (!Scene::getNode(ui.tempString)) {
						Scene::renameNode(ui.selectedNode, ui.tempString);
						ui.renaming = false;
					}
				}
			}

			if (ui.selectedNode->isPrefab()) {
				if (UI::Button("Sync clones")) {
					ui.selectedNode->syncPrefabClones();
				}
			}
			else {
				if (UI::Button("Set As Prefab##node_set_prefab")) {
					ui.selectedNode->setAsPrefab(true);
				}
			}
			
			if (ui.renaming) {
				if (Scene::getNode(ui.tempString)) {
					ImGui::TextColored(ImVec4(1, 0, 0, 1), "%s", "Node with this name already exists!");
				}
			}

			if (ImGui::Button("Clone##node_clone")) {
				SceneNode* copy = Scene::cloneNode(ui.selectedNode);
				ui.selectedNode = copy;
			}

			if (ImGui::Button("Delete##node_delete")) {
				if (ui.selectedNode == node) {
					if (node->getParent()) {
						ui.selectedNode = node->getParent();
					}
					else {
						ui.selectedNode = Scene::getRootNode();
					}
				}

				Scene::removeNode(node);
			}

			if (ImGui::CollapsingHeader("Transform", ImGuiTreeNodeFlags_DefaultOpen)) {
				glm::vec3 localPosition = node->transform.getLocalPosition();
				if (ImGui::InputFloat3("Local Position", &localPosition[0])) {
					node->transform.setLocalPosition(localPosition);
				}

				glm::vec3 worldPosition = node->transform.getWorldPosition();
				if (ImGui::InputFloat3("World Position", &worldPosition[0])) {
					node->transform.setWorldPosition(worldPosition);
				}

				glm::vec3 localEuler = node->transform.getLocalEuler();
				if (ImGui::InputFloat3("Local Euler", &localEuler[0])) {
					node->transform.setLocalEuler(localEuler);
				};

				glm::vec3 worldEuler = node->transform.getWorldEuler();
				if (ImGui::InputFloat3("World Euler", &worldEuler[0])) {
					node->transform.setWorldEuler(worldEuler);
				};

				glm::vec3 localScale = node->transform.getLocalScale();
				if (ImGui::InputFloat3("Local Scale", &localScale[0])) {
					node->transform.setLocalScale(localScale);
				};

				glm::vec3 worldScale = node->transform.getWorldScale();
				if (ImGui::InputFloat3("World Scale", &worldScale[0]));

				glm::vec3 right = node->transform.getRight();
				UI::Text("Right: [%.1f, %.1f, %.1f]", right.x, right.y, right.z);

				glm::vec3 up = node->transform.getUp();
				UI::Text("Up: [%.1f, %.1f, %.1f]", up.x, up.y, up.z);

				glm::vec3 foward = node->transform.getForward();
				UI::Text("foward: [%.1f, %.1f, %.1f]", foward.x, foward.y, foward.z);

				ImGui::NewLine();
				ImGui::Text("%s", "Gizmo Mode");
				if (ImGui::RadioButton("Disabled", ui.transformType == ImGuizmo::DISABLED))
					ui.transformType = ImGuizmo::DISABLED;
				if (ImGui::RadioButton("Translate", ui.transformType == ImGuizmo::TRANSLATE))
					ui.transformType = ImGuizmo::TRANSLATE;
				if (ImGui::RadioButton("Rotate", ui.transformType == ImGuizmo::ROTATE))
					ui.transformType = ImGuizmo::ROTATE;
				ImGui::SameLine();
				if (ImGui::RadioButton("Scale", ui.transformType == ImGuizmo::SCALE))
					ui.transformType = ImGuizmo::SCALE;

				ImGui::Checkbox("Snap##use_snapping", &ui.useSnapping);
				ImGui::Checkbox("Transform In World Space##transform_world", &ui.transformWorld);
				switch (ui.transformType)
				{
				case ImGuizmo::ROTATE:
					ImGui::InputFloat(" Angle Snap", &ui.snapSizes[0]);
					break;
				case ImGuizmo::SCALE:
					ImGui::InputFloat(" Scale Snap", &ui.snapSizes[0]);
					break;
				default:
					ImGui::InputFloat3(" Snap", &ui.snapSizes[0]);
					break;
				}
			}

			ImGui::Separator();
			ImGui::Text("%s", "Components");
			if (ImGui::BeginChild("Component_child_container")) {
				if (ui.selectedNode != Scene::getRootNode()) {
					if (ImGui::BeginPopupContextWindow("component_view_context_window")) {
						if (ImGui::BeginMenu("Add Component")) {
							for (auto& item : nodeComponents::detail::componentAssignMap) {
								bool present = ui.selectedNode->getComponentFromMap(item.first);
								if (ImGui::Selectable((item.first + " Component").c_str(), &present, present ? ImGuiSelectableFlags_Disabled : 0)) {
									ui.selectedNode->createComponentFromMap(item.first);

									if (!item.first.compare("Render")) {
										nodeComponents::Render* renderComponent = Scene::getComponent<nodeComponents::Render>(ui.selectedNode);
										renderComponent->node = ui.selectedNode;
										renderComponent->mesh = Resources::getMesh("sphere");
										renderComponent->material = Resources::getMaterial("default")->getBaseMaterial()->createInstance();
									}

									if (!item.first.compare("Script")) {
										nodeComponents::ScriptComponent* scriptComponent = Scene::getComponent<nodeComponents::ScriptComponent>(ui.selectedNode);
										scriptComponent->node = ui.selectedNode;
									}

									if (!item.first.compare("Physics")) {
										nodeComponents::PhysicsComponent* physicsComponent = Scene::getComponent<nodeComponents::PhysicsComponent>(ui.selectedNode);
										nodeComponents::Render* renderComponent = Scene::getComponent<nodeComponents::Render>(ui.selectedNode);
										//Physics::createShapeForMesh(ui.selectedNode, renderComponent->mesh, &physicsComponent->shape, &physicsComponent->body);
										physicsComponent->node = ui.selectedNode;
									}
								}
							}
							ImGui::EndMenu();
						}
						ImGui::EndPopup();
					}
				}
				ImGui::NewLine();


				nodeComponents::Render* renderComponent = Scene::getComponent<nodeComponents::Render>(ui.selectedNode);
				nodeComponents::ScriptComponent* scriptComponent = Scene::getComponent<nodeComponents::ScriptComponent>(ui.selectedNode);
				nodeComponents::PhysicsComponent* physicsComponent = Scene::getComponent<nodeComponents::PhysicsComponent>(ui.selectedNode);

				if (renderComponent) {
					if (ImGui::CollapsingHeader("Render##component_prop")) {
						if (ImGui::CollapsingHeader("Geometry##component_prop", ImGuiTreeNodeFlags_DefaultOpen)) {
							ImGui::Checkbox("Should Render", &renderComponent->render);

							ImGui::LabelText(renderComponent->mesh->identifier.c_str(), "Mesh");
							if (ImGui::BeginDragDropTarget()) {
								if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("mesh_resource_dd")) {
									if (payload->Data) {
										renderComponent->mesh = Resources::getMesh(*(std::string*)payload->Data);
									}
								}
								ImGui::EndDragDropTarget();
							}
						}

						if (ImGui::CollapsingHeader("Material##component_prop", ImGuiTreeNodeFlags_DefaultOpen)) {
							ImGui::LabelText(renderComponent->material->getMaterialDescription()->getIdentifier().c_str(), "Material");
							if (ImGui::BeginDragDropTarget()) {
								if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("material_resource_dd")) {
									if (payload->Data)
										renderComponent->material = Resources::getMaterial(*(std::string*)payload->Data)->getBaseMaterial()->createInstance();
								}
								ImGui::EndDragDropTarget();
							}

							ImGui::NewLine();

							if (ImGui::CollapsingHeader("Options##component_prop", ImGuiTreeNodeFlags_DefaultOpen)) {

								uint64_t key = renderComponent->material->getBaseMaterial()->getKey();

								std::array<bool, 32> optionState;

								uint32_t optionIndex = 0;
								for (std::string& option : renderComponent->material->getMaterialDescription()->materialOptions) {
									uint64_t flag = renderComponent->material->getMaterialDescription()->optionIndices[option];

									optionState[optionIndex++] = key & flag;
								}

								optionIndex = 0;
								for (std::string& option : renderComponent->material->getMaterialDescription()->materialOptions) {
									ImGui::Checkbox(option.c_str(), &optionState[optionIndex++]);
								}

								uint64_t newKey = 0;
								optionIndex = 0;
								for (std::string& option : renderComponent->material->getMaterialDescription()->materialOptions) {
									if (optionState[optionIndex++]) {
										newKey |= renderComponent->material->getMaterialDescription()->optionIndices[option];
									}
								}

								if (key != newKey) {
									MaterialInstance* newInstance = renderComponent->material->
										getMaterialDescription()->getMaterial(newKey)->createInstance();
									newInstance->copyFrom(renderComponent->material);

									renderComponent->material = newInstance;
								}
							}

							ImGui::Separator();

							DescriptorGroup& group = renderComponent->material->getCurrentFrameDescriptor();
							for (uint32_t i = 0; i < group.getDescriptorCount(); i++) {
								for (uint32_t j = 0; j < group.getSet(i).getBindingCount(); j++) {
									DescriptorBinding& binding = group.getSet(i).getBinding(j);
									if (binding.getResourceType() != DescriptorResourceType::IMAGE) continue;

									VkTexture* texture = reinterpret_cast<VkTexture*>(binding.getResource());

									float size = 60 * UI::scale;

									ImGui::Image((void*)imguiHelper->imageID(texture), ImVec2(size, size));
									if (ImGui::BeginDragDropTarget()) {
										if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("texture_resource_dd", ImGuiDragDropFlags_SourceAllowNullID)) {
											if (payload->Data) {
												VkTexture* newTex = Resources::getTexture(*(std::string*)payload->Data);

												renderComponent->material->setDescriptorBinding(i, j, newTex, &newTex->getDescriptor());
											}
										}
										ImGui::EndDragDropTarget();
									}

									ImGui::SameLine(); ImGui::TextWrapped("[%i, %i] %s:\n%s", binding.setSetIndex(), binding.getBindingIndex(), binding.getIdentifier().c_str(), texture->getIdentifier().c_str()); ImGui::NextColumn();
								}
							}

							ImGui::Separator();

							BufferBlock& matMem = renderComponent->material->getBaseMaterial()->materialBlock;
							if (matMem.size > 0) {
								float* basePtr = static_cast<float*>(renderComponent->material->getMemoryPtr());

								if (ImGui::CollapsingHeader("Material Parameters##component_prop", ImGuiTreeNodeFlags_DefaultOpen)) {
									for (BufferBlockShaderVar& var : matMem.variables) {
										if (renderComponent->material->getMaterialDescription()->memoryDescriptions.find(var.name) != renderComponent->material->getMaterialDescription()->memoryDescriptions.end()) {
											MaterialMemoryResource& res = renderComponent->material->getMaterialDescription()->memoryDescriptions[var.name];

											ImGui::TextColored(ImVec4(1.0, 0.7, 0.0, 1.0), "%s", res.editorName.c_str());
											if (res.description.length() > 0) {
												ImGui::SameLine();
												ImGui::HelpMarker(res.description.c_str());
											}

											if (res.type == MaterialMemoryResourceType::COLOR) {
												if (var.rows == 3) {
													ImGui::ColorEdit3(("##c3" + var.name).c_str(), basePtr);
												}

												if (var.rows == 4) {
													ImGui::ColorEdit4(("##c4" + var.name).c_str(), basePtr);
												}
											}

											if (res.type == MaterialMemoryResourceType::SCALAR) {
												switch (var.rows)
												{
												case 1:
													ImGui::InputFloat(("##f1" + var.name).c_str(), basePtr);
													break;
												case 2:
													ImGui::InputFloat2(("##f2" + var.name).c_str(), basePtr);
													break;
												case 3:
													ImGui::InputFloat3(("##f3" + var.name).c_str(), basePtr);
													break;
												case 4:
													ImGui::InputFloat4(("##f4" + var.name).c_str(), basePtr);
													break;
												}
											}

											if (res.type == MaterialMemoryResourceType::SLIDER) {
												switch (var.rows)
												{
												case 1:
													ImGui::SliderFloat(("##sf1" + var.name).c_str(), basePtr, res.min, res.max, "%.3f", res.step);
													break;
												case 2:
													ImGui::SliderFloat2(("##sf2" + var.name).c_str(), basePtr, res.min, res.max, "%.3f", res.step);
													break;
												case 3:
													ImGui::SliderFloat3(("##sf3" + var.name).c_str(), basePtr, res.min, res.max, "%.3f", res.step);
													break;
												case 4:
													ImGui::SliderFloat4(("##sf4" + var.name).c_str(), basePtr, res.min, res.max, "%.3f", res.step);
													break;
												}
											}

											if (res.type == MaterialMemoryResourceType::SCALAR_SLIDER) {
												switch (var.rows)
												{
												case 1:
													ImGui::InputFloat(("##ssf1" + var.name).c_str(), basePtr);
													break;
												case 2:
													ImGui::InputFloat2(("##ssf2" + var.name).c_str(), basePtr);
													break;
												case 3:
													ImGui::InputFloat3(("##ssf3" + var.name).c_str(), basePtr);
													break;
												case 4:
													ImGui::InputFloat4(("##ssf4" + var.name).c_str(), basePtr);
													break;
												}

												ImGui::SliderFloat(("##sssf1" + var.name).c_str(), basePtr, res.min, res.max, "%.3f", res.step);

												float* temp = basePtr;
												float val = *basePtr;

												for (uint32_t i = 0; i < var.rows; i++) {
													*temp = val;
													temp++;
												}
											}
										}
										else {
											ImGui::TextColored(ImVec4(1.0, 0.7, 0.0, 1.0), "%s", var.name.c_str());

											switch (var.rows)
											{
											case 1:
												ImGui::InputFloat(("##f1" + var.name).c_str(), basePtr);
												break;
											case 2:
												ImGui::InputFloat2(("##f2" + var.name).c_str(), basePtr);
												break;
											case 3:
												ImGui::InputFloat3(("##f3" + var.name).c_str(), basePtr);
												break;
											case 4:
												ImGui::InputFloat4(("##f4" + var.name).c_str(), basePtr);
												break;
											}
										}

										basePtr += var.size / sizeof(float);
										ImGui::Separator();
									}
								}
							}
						}
					}
				}

				if (scriptComponent) {
					if (ImGui::CollapsingHeader("Script")) {

						ImGui::LabelText(
							scriptComponent->getScript() ?
							scriptComponent->getScript()->getName().c_str() :
							"None", "Script");

						if (ImGui::BeginDragDropTarget()) {
							if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("script_resource_dd")) {
								if (payload->Data) {
									Script* script = Resources::getScript(*(std::string*)payload->Data);

									if (script) {
										scriptComponent->setScript(script);
									}
								}
							}
							ImGui::EndDragDropTarget();
						}

						if (scriptComponent->getScript()) {
							if (UI::Button("Edit Script")) {
								ui.editedScript.handle = scriptComponent->getScript();
								ui.editedScript.node = scriptComponent->node;
								ui.textEditor->SetText(scriptComponent->getScript()->getScriptSource());
							}

							if (UI::Button("Remove Script")) {
								scriptComponent->setScript(nullptr);
							}

							if (scriptComponent->serializedVars.size() > 0) {
								ImGui::NewLine();
								ImGui::Text("Script variables");
								ImGui::Separator();
								for (SerializedScriptVar& var : scriptComponent->serializedVars) {
									std::string formattedName = var.name;

									if (formattedName.length() > 1) {
										formattedName[0] = std::toupper(var.name[0]);

										for (auto it = formattedName.begin() + 1; it != formattedName.end(); it += 1) {
											if (it != formattedName.end() && it != formattedName.begin()) {
												if (std::isupper(*it) && !std::isupper(*(it + 1)) || std::isupper(*it) && !std::isupper(*(it - 1))) {
													it = formattedName.insert(it, ' ') + 1;
												}
											}
										}
									}
									
									ImGui::TextColored(ImVec4(1.0, 0.7, 0.0, 1.0), "%s", formattedName.c_str());
									UI::SameLine();
									

									if (var.type == ScriptVarType::INT) {
										UI::Text(" - %s", "Integer");
										UI::InputInt(("##sivi" + var.name).c_str(), (int*)var.address);
									}

									if (var.type == ScriptVarType::FLOAT) {
										UI::Text(" - %s", "Float");
										UI::InputFloat(("##sivf" + var.name).c_str(), (float*)var.address);
									}

									if (var.type == ScriptVarType::DOUBLE) {
										UI::Text(" - %s", "Double");
										UI::InputDouble(("##sivf" + var.name).c_str(), (double*)var.address);
									}

									if (var.type == ScriptVarType::BOOL) {
										UI::Text(" - %s", "Boolean");
										const char* combo[] = { "True", "False" };
										bool* valuePtr = (bool*)var.address;

										const char* currentVal = *valuePtr ? "True" : "False";

										if (UI::BeginCombo(("##sivb" + var.name).c_str(), currentVal)) {
											for (int i = 0; i < 2; i++) {
												bool isSelected = (strcmp(currentVal, combo[i]) == 0);
												if (UI::Selectable(combo[i], isSelected)) {
													if (strcmp(combo[i], "True") == 0) {
														*valuePtr = true;
													}
													else {
														*valuePtr = false;
													}
												}

												if (isSelected)
													UI::SetItemDefaultFocus();
											}
											UI::EndCombo();
										}
									}

									if (var.type == ScriptVarType::STRING) {
										UI::Text(" - %s", "String");
										UI::InputText(("##sivstr" + var.name).c_str(), (std::string*)var.address);
									}

									if (var.type == ScriptVarType::VEC2) {
										UI::Text(" - %s", "Vec2");
										UI::InputFloat2(("##sivv2" + var.name).c_str(), (float*)var.address);
									}

									if (var.type == ScriptVarType::VEC3) {
										UI::Text(" - %s", "Vec3");
										UI::InputFloat3(("##sivv3" + var.name).c_str(), (float*)var.address);
									}

									if (var.type == ScriptVarType::VEC4) {
										UI::Text(" - %s", "Vec4");
										UI::InputFloat4(("##sivv4" + var.name).c_str(), (float*)var.address);
									}

									if (var.type == ScriptVarType::NODE) {
										UI::Text(" - %s", "Node");
										SceneNode** address = reinterpret_cast<SceneNode**>(var.address);
										SceneNode* node = *address;

										std::string name = (node ? node->name : "None");

										UI::InputText(("##sivnstr" + var.name).c_str(), &name, ImGuiInputTextFlags_ReadOnly);
										if (ImGui::BeginDragDropTarget()) {
											if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("SCENE_REORDER", 0))
											{
												*address = *(grail::SceneNode**)payload->Data;
												//to = node;
											}
											ImGui::EndDragDropTarget();
										}
										UI::SameLine();
										if (UI::Button("X")) {
											*address = nullptr;
										}

										/*asIScriptObject* handle = reinterpret_cast<CScriptHandle*>(var.address);
										//ref = *(void**)ref;
										SceneNode* node = reinterpret_cast<SceneNode*>(handle->GetRef());

										//GRAIL_LOG(INFO, "AEWFAEW") << handle->GetType()->GetName();

										UI::Text("%s", (node ? node->name : "None"));
										if (ImGui::BeginDragDropTarget()) {
											if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("SCENE_REORDER", 0))
											{
												handle->Set(*(grail::SceneNode**)payload->Data, 0);
												//to = node;
											}
											ImGui::EndDragDropTarget();
										}*/
									}
								}
							}
						}
						else {
							/*if (UI::Button("Create New")) {
								scriptComponent->script = ScriptProvider::createScript(ui.selectedNode->name);
								scriptComponent->script->compile();
								scriptComponent->script->setup(ui.selectedNode);
							}*/
						}

					}
				}

				if (physicsComponent) {
					/*if (ImGui::CollapsingHeader("Physics##component_prop")) {
						ImGui::Text("%s", "Body");
						ImGui::Separator();

						float mass = physicsComponent->getMass();
						if (UI::InputFloat("Mass ", &mass)) { physicsComponent->setMass(mass); };

						float restitution = physicsComponent->getRestitution();
						if (UI::InputFloat("Restitution ", &restitution)) { physicsComponent->setRestitution(restitution); };

						float friction = physicsComponent->getFriction();
						if (UI::InputFloat("Friction ", &friction)) { physicsComponent->setFriction(friction); };

						float rollingFriction = physicsComponent->getRollingFriction();
						if (UI::InputFloat("Rolling Friction ", &rollingFriction)) { physicsComponent->setRollingFriction(rollingFriction); };

						glm::vec3 angularFactor = physicsComponent->getAngularFactor();
						if (UI::InputFloat3("Angular Factor ", &angularFactor[0])) { physicsComponent->setAngularFactor(angularFactor); };

						glm::vec3 linearFactor = physicsComponent->getLinearFactor();
						if (UI::InputFloat3("Linear Factor ", &linearFactor[0])) { physicsComponent->setLinearFactor(linearFactor); };

						ImGui::NewLine();
						ImGui::Text("%s", "Shape");
						ImGui::Separator();

						if (UI::Button("Set Capsule Shape")) {
							physicsComponent->createCapsuleShape();
						}

						if (UI::Button("Set Convex Hull Shape")) {
							physicsComponent->createConvexHullShape();
						}

						if (UI::Button("Bake Static Mesh Shape")) {
							btTriangleMesh* triangleMesh = new btTriangleMesh();

							for (int i = 0; i < renderComponent->mesh->indicesCount; i += 3) {
								uint32_t i0 = renderComponent->mesh->indices[i];
								uint32_t i1 = renderComponent->mesh->indices[i + 1];
								uint32_t i2 = renderComponent->mesh->indices[i + 2];

								glm::vec3 vertex0 = glm::vec4(renderComponent->mesh->vertices[((i0) * 12) + 0], renderComponent->mesh->vertices[((i0) * 12) + 1], renderComponent->mesh->vertices[((i0) * 12) + 2], 1.0);
								glm::vec3 vertex1 = glm::vec4(renderComponent->mesh->vertices[((i1) * 12) + 0], renderComponent->mesh->vertices[((i1) * 12) + 1], renderComponent->mesh->vertices[((i1) * 12) + 2], 1.0);
								glm::vec3 vertex2 = glm::vec4(renderComponent->mesh->vertices[((i2) * 12) + 0], renderComponent->mesh->vertices[((i2) * 12) + 1], renderComponent->mesh->vertices[((i2) * 12) + 2], 1.0);

								triangleMesh->addTriangle(
									btVector3(vertex0.x, vertex0.y, vertex0.z),
									btVector3(vertex1.x, vertex1.y, vertex1.z),
									btVector3(vertex2.x, vertex2.y, vertex2.z)
								);
							}

							glm::vec3 scale = node->getParent()->transform.getLocalToWorldMatrix() * glm::vec4(node->transform.getLocalScale(), 0.0f);
							btCollisionShape* shape = new btBvhTriangleMeshShape(triangleMesh, true);
							*/
							/*physicsComponent->setShape(shape);
							physicsComponent->setMass(0.0f);
							physicsComponent->setRestitution(0.1f);
							physicsComponent->syncTransform();*/
						//}
					//}
				}
				ImGui::EndChild();
			}
		}

		{
			SceneNode* node = ui.selectedNode;

			if (node) {
				if (!ImGui::GetIO().WantCaptureKeyboard) {
					if (!ImGuizmo::IsUsing()) {
						if (Input::isKeyPressed(Keys::R)) {
							ui.transformType = ImGuizmo::OPERATION::ROTATE;
						}

						if (Input::isKeyPressed(Keys::T)) {
							ui.transformType = ImGuizmo::OPERATION::TRANSLATE;
						}

						if (Input::isKeyPressed(Keys::V)) {
							ui.transformType = ImGuizmo::OPERATION::SCALE;
						}

						if (Input::isKeyPressed(Keys::B)) {
							ui.transformType = ImGuizmo::OPERATION::BOUNDS;
						}

						if (Input::isKeyPressed(Keys::C)) {
							ui.transformType = ImGuizmo::OPERATION::DISABLED;
						}
					}
				}
				
				glm::mat4 projection = camera->projection;
				glm::mat4 view = camera->view;
				projection = glm::scale(projection, glm::vec3(1, -1, 1));

				if (ui.transformType != ImGuizmo::DISABLED && node != Scene::getRootNode()) {
					if (ImGuizmo::IsUsing()) {
						if (!ui.transforming) {
							ui.worldCopy = node->transform.getWorldMatrix();
							ui.scaleCopy = node->transform.getWorldScale();
							ui.delta = glm::mat4();
							ui.transforming = true;
						}
					}
					else {
						ui.transforming = false;
						ui.worldCopy = node->transform.getWorldMatrix();
						ui.scaleCopy = node->transform.getWorldScale();
					}

					glm::vec3 min = node->transform.getLocalBoundsMin();
					glm::vec3 max = node->transform.getLocalBoundsMax();

					float bounds[] = {
						min.x, min.y, min.z, max.x, max.y, max.z
					};

					ImGuizmo::SetRect(ui.mainViewBounds.x, ui.mainViewBounds.y, ui.mainViewBounds.z, ui.mainViewBounds.w);

					ImGuizmo::Manipulate(
						glm::value_ptr(view),
						glm::value_ptr(projection),
						ui.transformType,
						ui.transformWorld ? ImGuizmo::MODE::WORLD : ImGuizmo::MODE::LOCAL,
						glm::value_ptr(ui.worldCopy),
						glm::value_ptr(ui.delta),
						ui.useSnapping ? &ui.snapSizes[0] : nullptr,
						nullptr);

					if (ImGuizmo::IsUsing() && ui.transforming) {
						glm::vec3 translation;
						glm::vec3 rotation;
						glm::vec3 scale;

						ImGuizmo::DecomposeMatrixToComponents(
							glm::value_ptr(ui.delta),
							&translation[0],
							&rotation[0],
							&scale[0]);

						if (ui.transformType == ImGuizmo::TRANSLATE) {	
							node->transform.translateWorld(translation);
						}

						if (ui.transformType == ImGuizmo::ROTATE) {
							node->transform.rotateWorldEuler(rotation);
						}

						if (ui.transformType == ImGuizmo::SCALE) {
							glm::vec3 newScale = glm::vec3(
								ui.scaleCopy.x * scale[0],
								ui.scaleCopy.y * scale[1],
								ui.scaleCopy.z * scale[2]
							);

							node->transform.setWorldScale(newScale);
						}

						nodeComponents::PhysicsComponent* physics = Scene::getComponent<nodeComponents::PhysicsComponent>(node);
						if (physics) {
							//physics->syncTransform();
						}
					}
				}
			}
		}

		ImGui::End();
	}

	currentType = EditorWindowType::RESOURCES;
	if (ui.handles.windows[static_cast<uint32_t>(currentType)]) {
		ImGui::Begin(editorWindowNames[currentType].c_str(), &ui.handles.windows[static_cast<uint32_t>(currentType)]);

		if (ImGui::BeginTabBar("Resource_Tab_Bar")) {
			if (UI::BeginTabItem("Scripts")) {
				if (UI::BeginChild("Script_Container")) {
					for (const auto& val : Resources::scripts) {
						if (UI::Button(val.second->getName().c_str())) {
							ui.editedScript.handle = val.second;
							ui.textEditor->SetText(val.second->getScriptSource());
							ui.editedScript.node = nullptr;
						}

						if (ImGui::BeginDragDropSource(ImGuiDragDropFlags_SourceNoHoldToOpenOthers | ImGuiDragDropFlags_SourceNoDisableHover)) {
							ImGui::Text("Script \"%s\"", val.second->getName().c_str());
							ImGui::SetDragDropPayload("script_resource_dd", &val.second->name, sizeof(std::string));
							ImGui::EndDragDropSource();
						}
					}

					if (UI::BeginPopupContextWindow()) {
						if (UI::Selectable("New Script")) {
							ui.openPopup = ui.POPUP_NAME_SCRIPT;
						}

						UI::EndPopup();
					}
					UI::EndChild();
				}
				UI::EndTabItem();
			}

			if (ImGui::BeginTabItem("Meshes")) {
				float size = ImGui::GetWindowWidth();

				uint32_t index = 0;
				float combinedWidth = 0;
				float parentWidth = ImGui::GetWindowWidth();
				ImGui::NewLine();
				ImGui::BeginChild("mesh_resource_container", ImVec2(0, -50));
				for (const auto& val : Resources::meshes) {
					std::string finalName = (val.second->baseAsset.length() == 0 ?
						val.second->identifier.c_str() :
						(val.second->baseAsset + "\n" + val.second->identifier));

					if (ui.meshFilter.length() > 0) {
						if (!utils::string::containsCaseInsensitive(finalName, ui.meshFilter)) {
							continue;
						}
					}

					if (combinedWidth + ImGui::GetItemRectSize().x < parentWidth * 0.95f) {
						ImGui::SameLine();
					}
					else {
						combinedWidth = 0;
						//ImGui::NewLine();
					}

					if (ImGui::Button(
						(val.second->baseAsset.length() == 0 ?
							val.second->identifier.c_str() :
							(val.second->baseAsset + "\n" + val.second->identifier).c_str()))) {

						ui.resourceInspectorType = ResourceInspectorType::MESH;
						ui.inspectedMesh = Resources::getMesh(val.second->identifier);

						glm::vec3 min = glm::vec3(0.0);
						glm::vec3 max = glm::vec3(1.0);

						if (glm::length2(ui.inspectedMesh->boundsMin + ui.inspectedMesh->boundsMax) > 0) {
							min = ui.inspectedMesh->boundsMin;
							max = ui.inspectedMesh->boundsMax;
						}

						glm::vec3 size = max - min;
						float radius = glm::max(glm::max(size.x, size.y), size.z);

						glm::vec3 pivot = min + max;

						ui.inspectedMeshTime = 0.0f;

						ui.inspectedMeshCamera->nearPlane = 0.01f;
						ui.inspectedMeshCamera->farPlane = radius * glm::length(pivot);

						ui.inspectedMeshCamera->position = glm::vec3(0, radius * 0.2f, radius * 1.3f);
						ui.inspectedMeshCamera->translate(pivot * 0.5f);
						ui.inspectedMeshCamera->direction = glm::vec3(0, 0, 1);
						ui.inspectedMeshCamera->lookAt(glm::vec3(0, 0, 0));

						ui.inspectedMeshCamera->update();
					}

					combinedWidth += ImGui::GetItemRectSize().x;

					if (ImGui::BeginDragDropSource(ImGuiDragDropFlags_SourceNoHoldToOpenOthers | ImGuiDragDropFlags_SourceNoDisableHover)) {
						ImGui::Text("Mesh \"%s\"", val.second->identifier.c_str());
						ImGui::SetDragDropPayload("mesh_resource_dd", &val.second->identifier, sizeof(std::string));
						ImGui::EndDragDropSource();
					}
					index++;
				}
				ImGui::EndChild();

				ImGui::Text("Seach ");
				ImGui::SameLine();
				ImGui::InputText("##mesh_seach", &ui.meshFilter);
				ImGui::SameLine();
				if (ImGui::Button("Clear##mesh_s")) ui.meshFilter.clear();

				ImGui::EndTabItem();
			}

			if (ImGui::BeginTabItem("Textures")) {
				uint32_t rowIndex = 0;
				float windowSize = ImGui::GetWindowWidth();
				float imageSize = 80 * UI::scale;
				float currentSize = 0;

				std::vector<VkTexture*> textures;
				for (const auto& val : Resources::textures) {
					VkTexture* texture = Resources::getTexture(val.second->getIdentifier());

					if (!texture) continue;
					textures.push_back(texture);
				}

				struct sort_key
				{
					inline bool operator() (const VkTexture* o, const VkTexture* t)
					{
						return o->identifier > t->identifier;
					}
				};

				std::sort(textures.begin(), textures.end(), sort_key());


				float itemSize = 128 * UI::scale;
				int columns = (int)(windowSize / (itemSize));
				columns = columns < 1 ? 1 : columns;
				ImGui::BeginChild("texture_resource_container", ImVec2(0, -50));
				ImGui::Columns(columns, "texture_resources_column", false);

				for (VkTexture* t : textures) {
					if (ui.textureFilter.length() > 0) {
						if (!utils::string::containsCaseInsensitive(t->getIdentifier(), ui.textureFilter)) {
							continue;
						}
					}

					VkImageSubresourceRange subresource;
					subresource.baseArrayLayer = 0;
					subresource.baseMipLevel = 0;
					subresource.layerCount = 1;
					subresource.levelCount = 1;

					ImGui::BeginGroup();
					ImGui::Image((void*)imguiHelper->imageID(t, t->getDescriptor(t->getImageView(subresource, VK_IMAGE_VIEW_TYPE_2D))), ImVec2(itemSize, itemSize));
					ImGui::TextWrapped("%s", t->getIdentifier().c_str());
					ImGui::EndGroup();
					if (ImGui::BeginPopupContextItem(t->getIdentifier().c_str())) {
						if (ImGui::Selectable("Inspect")) {
							ui.resourceInspectorType = ResourceInspectorType::TEXTURE;
							ui.inspectedTexture = t;
							ui.inspectedTextureSubresourceRange = { 0, 0, 1, 0, 1 };
						}
						ImGui::EndPopup();
					}
					if (ImGui::BeginDragDropSource(ImGuiDragDropFlags_SourceNoHoldToOpenOthers | ImGuiDragDropFlags_SourceNoDisableHover | ImGuiDragDropFlags_SourceAllowNullID)) {
						ImGui::Image((void*)imguiHelper->imageID(t, t->getDescriptor(t->getImageView(subresource, VK_IMAGE_VIEW_TYPE_2D))), ImVec2(imageSize, imageSize));
						ImGui::TextWrapped("%s", t->getIdentifier().c_str());
						ImGui::SetDragDropPayload("texture_resource_dd", &t->identifier, sizeof(std::string));
						ImGui::EndDragDropSource();
					}

					ImGui::NextColumn();
				}
				//ImGui::endColumns();
				ImGui::EndChild();

				ImGui::Text("Search ");
				ImGui::SameLine();
				ImGui::InputText("##texture_seach", &ui.textureFilter);
				ImGui::SameLine();
				if (ImGui::Button("Clear##texture_s")) ui.textureFilter.clear();

				ImGui::EndTabItem();
			}

			if (ImGui::BeginTabItem("Materials")) {
				uint32_t index = 0;
				float combinedWidth = 0;
				float parentWidth = ImGui::GetWindowWidth();
				ImGui::NewLine();
				ImGui::BeginChild("material_resource_container", ImVec2(0, 0));
				for (const auto& val : Resources::materials) {
					if (combinedWidth + ImGui::GetItemRectSize().x < parentWidth * 0.95f) {
						ImGui::SameLine();
					}
					else {
						combinedWidth = 0;
						//ImGui::NewLine();
					}

					ImGui::Button(val.second->getIdentifier().c_str());
					if (ImGui::BeginPopupContextItem(val.second->getIdentifier().c_str())) {
						if (ImGui::Selectable("Inspect")) {
							
						}
						ImGui::EndPopup();
					}

					combinedWidth += ImGui::GetItemRectSize().x;

					if (ImGui::BeginDragDropSource(ImGuiDragDropFlags_SourceNoHoldToOpenOthers | ImGuiDragDropFlags_SourceNoDisableHover)) {
						ImGui::Text("Material \"%s\"", val.second->getIdentifier().c_str());

						std::string identifier = val.second->getIdentifier();

						ImGui::SetDragDropPayload("material_resource_dd", &identifier, sizeof(std::string));
						ImGui::EndDragDropSource();
					}
					index++;
				}
				ImGui::EndChild();
				ImGui::EndTabItem();
			}

			if (ImGui::BeginTabItem("Shaders")) {
				ImGui::EndTabItem();
			}
			ImGui::EndTabBar();
		}
		ImGui::End();
	}

	currentType = EditorWindowType::CONSOLE;
	if (ui.handles.windows[static_cast<uint32_t>(currentType)]) {
		console->draw(&ui.handles.windows[static_cast<uint32_t>(currentType)]);
	}

	currentType = EditorWindowType::RESOURCE_INSPECTOR;
	if (ui.handles.windows[static_cast<uint32_t>(currentType)]) {
		ImGui::Begin(editorWindowNames[currentType].c_str(), &ui.handles.windows[static_cast<uint32_t>(currentType)]);
		
		float displaySize = ImGui::GetWindowWidth() - ImGui::GetStyle().WindowPadding.x * 1.5f;

		if (ui.resourceInspectorType == ResourceInspectorType::TEXTURE) {
			if (ui.inspectedTexture) {
				VkTexture* texture = ui.inspectedTexture;

				ImGui::Image((void*)imguiHelper->imageID(texture, texture->getDescriptor(texture->getImageView(ui.inspectedTextureSubresourceRange, VK_IMAGE_VIEW_TYPE_2D))), ImVec2(displaySize, displaySize));

				if (ImGui::CollapsingHeader("Texture Properties", ImGuiTreeNodeFlags_DefaultOpen)) {
					ImGui::Text("Name: %s", texture->getIdentifier().c_str());
					ImGui::Text("Dimensions: %i x %i x %i", texture->getWidth(), texture->getHeight(), texture->getDepth());
					ImGui::Text("Mip Levels: %i", texture->getMipCount());
					ImGui::Text("Array Levels: %i", texture->getLayerCount());
					ImGui::Text("Format: %s", utils::vulkan::formatToString(texture->getFormat()).c_str());
					ImGui::Text("Layout: %s", utils::vulkan::imageLayoutToString(texture->getLayout()).c_str());
					ImGui::Text("Type: %s", utils::vulkan::imageViewTypeToString(texture->getViewType()).c_str());
					ImGui::Text("Size %s Bytes", utils::string::bytesToString(texture->getMemoryRegion().size).c_str());
				}

				if (ImGui::CollapsingHeader("Texture View Properties", ImGuiTreeNodeFlags_DefaultOpen)) {
					ImGui::SliderInt("Mip Level", reinterpret_cast<int*>(&ui.inspectedTextureSubresourceRange.baseMipLevel), 0, texture->getMipCount() - 1);
					ImGui::SliderInt("Layer Level", reinterpret_cast<int*>(&ui.inspectedTextureSubresourceRange.baseArrayLayer), 0, texture->getLayerCount() - 1);
				}
			}
		}

		if (ui.resourceInspectorType == ResourceInspectorType::MESH) {

			if (ui.inspectedMesh) {
				meshDisplayGraph->buildFrame();
				meshDisplayGraph->submitFrame();

				VkTexture* texture = Resources::getTexture("mesh_display");

				ImGui::Image((void*)imguiHelper->imageID(texture, texture->getDescriptor(texture->getImageView(ui.inspectedTextureSubresourceRange, VK_IMAGE_VIEW_TYPE_2D))), ImVec2(displaySize, displaySize));

				if (ImGui::CollapsingHeader("Mesh Properties", ImGuiTreeNodeFlags_DefaultOpen)) {
					glm::vec3 min = ui.inspectedMesh->boundsMin;
					glm::vec3 max = ui.inspectedMesh->boundsMax;

					glm::vec3 size = ui.inspectedMesh->boundsMax - ui.inspectedMesh->boundsMin;
					float radius = glm::max(glm::max(size.x, size.y), size.z);

					glm::vec3 pivot = ui.inspectedMesh->boundsMin + ui.inspectedMesh->boundsMax;

					ImGui::Text("Name: %s", ui.inspectedMesh->identifier.c_str());
					ImGui::Text("Base Asset: %s", ui.inspectedMesh->baseAsset.c_str());
					ImGui::Text("Bounds Min: [%f, %f, %f]", min.x, min.y, min.z);
					ImGui::Text("Bounds Max: [%f, %f, %f]", max.x, max.y, max.z);
					ImGui::Text("Bounds Size: [%f, %f, %f]", size.x, size.y, size.z);
					ImGui::Text("Pivot: [%f, %f, %f]", pivot.x, pivot.y, pivot.z);
					ImGui::Text("Radius: %f", radius);
					ImGui::Text("Number Of Vertices: %u", ui.inspectedMesh->verticesCount);
					ImGui::Text("Number Of Indices: %u", ui.inspectedMesh->indicesCount);
					ImGui::Text("Vertex Buffer Size: %s Bytes", utils::string::bytesToString(ui.inspectedMesh->vertexBufferSize).c_str());
					ImGui::Text("Index Buffer Size: %s Bytes", utils::string::bytesToString(ui.inspectedMesh->indexBufferSize).c_str());
					ImGui::Text("Total Buffer Size: %s Bytes", utils::string::bytesToString(ui.inspectedMesh->memoryRegion.size).c_str());
				}
			}
		}

		ImGui::End();
	}

	currentType = EditorWindowType::SCRIPT_EDITOR;
	if (ui.handles.windows[static_cast<uint32_t>(currentType)]) {
		ImGui::Begin(editorWindowNames[currentType].c_str(), &ui.handles.windows[static_cast<uint32_t>(currentType)]);

		if (ui.editedScript.handle) {
			ImGui::BeginChild("script_editor_frame", ImVec2(0, -30 * UI::scale));
			/*if (!ui.editedScript.handle->errors.empty() && !ui.textEditor->CanUndo()) {
				TextEditor::ErrorMarkers markers;

				for (ScriptError& error : ui.editedScript.handle->errors) {
					markers.insert({ error.row, error.message });
					
				}
				ui.textEditor->SetErrorMarkers(markers);
			}
			else {
				ui.textEditor->SetErrorMarkers(TextEditor::ErrorMarkers());
			}*/

			auto cpos = ui.textEditor->GetCursorPosition();
			ImGui::Text("%6d/%-6d %6d lines  | %s | %s | %s%s", cpos.mLine + 1, cpos.mColumn + 1, ui.textEditor->GetTotalLines(),
				ui.textEditor->IsOverwrite() ? "Ovr" : "Ins",
				ui.textEditor->GetLanguageDefinition().mName.c_str(),
				(ui.editedScript.handle->getName()).c_str(),
				ui.textEditor->CanUndo() ? "*" : " ");
			ImGui::SameLine();
			if (ui.textEditor->CanUndo()) {
				ImGui::TextColored(ImVec4(1, 1, 0.5, 1), " Unsaved");
			}
			else {
				if (ui.editedScript.handle->isCompiled()) {
					ImGui::TextColored(ImVec4(0, 1, 0, 1), " Compiled");
				}
				else {
					ImGui::TextColored(ImVec4(1, 0, 0, 1), " Compilation Failed");
				}
			}
			ui.textEditor->Render(editorWindowNames[currentType].c_str());

			ImGui::EndChild();

			if (ImGui::Button("Save##saveScript")) {
				if (ui.textEditor->CanUndo()) {
					std::string current = ui.textEditor->GetText();

					if (!current.empty() && current[current.length() - 1] == '\n') {
						current.erase(current.length() - 1);
					}

					ui.editedScript.handle->setScriptSource(current);
					ScriptProvider::recompile();
					//ui.editedScript.handle->compile();
					ui.textEditor->SetText(current);

					if (ui.editedScript.node) {
						//ui.editedScript.handle->setup(ui.editedScript.node);
					}
				}
			}
		}

		ImGui::End();
	}

	currentType = EditorWindowType::IMGUI_DEMO;
	if (ui.handles.windows[static_cast<uint32_t>(currentType)]) {
		ImGui::ShowDemoWindow(&ui.handles.windows[static_cast<uint32_t>(currentType)]);
	}

	currentType = EditorWindowType::MEMORY_INSPECTOR;
	if (ui.handles.windows[static_cast<uint32_t>(currentType)]) {
		UI_Memory_Inspector(editorWindowNames[currentType], &ui.handles.windows[static_cast<uint32_t>(currentType)]);
	}

	currentType = EditorWindowType::VR_INFO;
	if (ui.handles.windows[static_cast<uint32_t>(currentType)]) {
		VR_Info_Window(editorWindowNames[currentType], &ui.handles.windows[static_cast<uint32_t>(currentType)]);
	}

	//structureViewer->render();

	// Top Menu Bar
	ImGui::BeginMainMenuBar();
	{
		std::stringstream ss;
		ss << "Current Device: " << VulkanContext::mainGPU.properties.deviceName << "  ";
		ImGui::TextColored(ImVec4(0.5, 0.5, 0.5, 1.0), ss.str().c_str(), "");

		if (ImGui::BeginMenu("Settings")) {
			//ImGui::MenuItem("Shrink Main View", nullptr, &settings.shrinkMainView);
			if (ImGui::MenuItem("Hide Editor")) {
				ui.enabled = false;
			}

			ImGui::MenuItem("Highlight Selected Nodes", nullptr, &ui.highlightSelected);
			ImGui::MenuItem("Debug Physics", nullptr, &ui.debugPhysics);
			ImGui::EndMenu();
		}

		if (ImGui::BeginMenu("Scene")) {
			bool selected = false;
			if (ImGui::Selectable("Save Scene", &selected, (state == EditorState::STOPPED) ? 0 : ImGuiSelectableFlags_Disabled)) {
				ui.openPopup = ui.POPUP_SAVE_SCENE;
			}

			if (ImGui::Selectable("Load Scene", &selected, (state == EditorState::STOPPED) ? 0 : ImGuiSelectableFlags_Disabled)) {
				ui.openPopup = ui.POPUP_LOAD_SCENE;
			}

			ImGui::EndMenu();
		}

		if (ImGui::BeginMenu("Editor")) {
			if (ImGui::MenuItem("Save Editor Layout")) {
				size_t size = 0;
				const char* settings = ImGui::SaveIniSettingsToMemory(&size);

				std::string props = "";
				for (uint32_t i = 0; i < ui.handles.windows.size(); i++) {
					props.append(std::to_string(static_cast<int>(ui.handles.windows[i])));
				}

				std::ofstream file;
				file.open(FileIO::getAssetPath("layout.ini"));
				file << props << "\n" << settings;
				file.close();
			}
			ImGui::EndMenu();
		}

		if (ImGui::BeginMenu("Windows")) {
			for (uint32_t i = 0; i < ui.handles.windows.size(); i++) {
				if (ImGui::MenuItem(editorWindowNames[static_cast<EditorWindowType>(i)].c_str(), nullptr, ui.handles.windows[i], !ui.handles.windows[i])) {
					ui.handles.windows[i] = true;
				}
			}

			ImGui::EndMenu();
		}

		if (ImGui::BeginMenu("Tests")) {
			if (ImGui::MenuItem("Bake Basics Shapes For Scene")) {
				Scene::getNodesWithComponents<nodeComponents::Render>().each([](auto& c) {
					nodeComponents::PhysicsComponent* pc = Scene::getComponent<nodeComponents::PhysicsComponent>(c.node);

					if (!pc) {
						Scene::assignComponent<nodeComponents::PhysicsComponent>(c.node);
						pc = Scene::getComponent<nodeComponents::PhysicsComponent>(c.node);
						pc->node = c.node;
						//Physics::createShapeForMesh(c.node, c.mesh, &pc->shape, &pc->body);
					}
				});
			}

			if (ImGui::MenuItem("Bake Static Meshes For Scene")) {
				Scene::getNodesWithComponents<nodeComponents::Render>().each([](auto& c) {
					nodeComponents::PhysicsComponent* pc = Scene::getComponent<nodeComponents::PhysicsComponent>(c.node);



					if (!pc) {
						/*Scene::assignComponent<nodeComponents::PhysicsComponent>(c.node);
						pc = Scene::getComponent<nodeComponents::PhysicsComponent>(c.node);
						Physics::createShapeForMesh(c.node, c.mesh, &pc->shape, &pc->body);
						pc->node = c.node;

						btTriangleMesh* triangleMesh = new btTriangleMesh();

						for (int i = 0; i < c.mesh->indicesCount; i += 3) {
							uint32_t i0 = c.mesh->indices[i];
							uint32_t i1 = c.mesh->indices[i + 1];
							uint32_t i2 = c.mesh->indices[i + 2];

							glm::vec3 vertex0 = glm::vec4(c.mesh->vertices[((i0) * 12) + 0], c.mesh->vertices[((i0) * 12) + 1], c.mesh->vertices[((i0) * 12) + 2], 1.0);
							glm::vec3 vertex1 = glm::vec4(c.mesh->vertices[((i1) * 12) + 0], c.mesh->vertices[((i1) * 12) + 1], c.mesh->vertices[((i1) * 12) + 2], 1.0);
							glm::vec3 vertex2 = glm::vec4(c.mesh->vertices[((i2) * 12) + 0], c.mesh->vertices[((i2) * 12) + 1], c.mesh->vertices[((i2) * 12) + 2], 1.0);

							triangleMesh->addTriangle(
								btVector3(vertex0.x, vertex0.y, vertex0.z),
								btVector3(vertex1.x, vertex1.y, vertex1.z),
								btVector3(vertex2.x, vertex2.y, vertex2.z)
							);
						}

						glm::vec3 scale = glm::abs(c.node->transform.getWorldScale());
						btCollisionShape* shape = new btBvhTriangleMeshShape(triangleMesh, true);
						Physics::dynamicsWorld->removeRigidBody(pc->body);
						shape->setLocalScaling(btVector3(scale.x, scale.y, scale.z));
						btVector3 localInertia;
						shape->calculateLocalInertia(0.0, localInertia);
						pc->body->setCollisionShape(shape);
						pc->setMass(0.0f);
						pc->setRestitution(0.1f);
						Physics::dynamicsWorld->addRigidBody(pc->body);*/
					}
				});
			}
			ImGui::EndMenu();
		}
		ImGui::Separator();
		ImGui::SetCursorPosX(Graphics::getWidth() * 0.5f);
		if (state == EditorState::STOPPED) {
			if (ImGui::MenuItem("Play")) { setState(EditorState::PLAYING); }
		}

		else {
			if (ImGui::MenuItem("Play")) { setState(EditorState::PLAYING); }
			if (ImGui::MenuItem("Stop")) { setState(EditorState::STOPPED); }
		}
	}
	ImGui::EndMainMenuBar();

	{
		if (ui.openPopup == ui.POPUP_SAVE_SCENE) ImGui::OpenPopup("Save Scene Dialog");
		if (ui.openPopup == ui.POPUP_LOAD_SCENE) ImGui::OpenPopup("Load Scene Dialog");
		if (ui.openPopup == ui.POPUP_NAME_SCRIPT) UI::OpenPopup("New Script Dialog");

		if (ImGui::BeginPopupModal("Save Scene Dialog")) {
			ImGui::Text("Choose a filename, the .scn extension gets added automatically");

			ImGui::InputText("Filename##save_dialog", &ui.saveLoadFilename);

			bool exists = FileIO::fileExists(FileIO::getAssetPath("scenes/" + ui.saveLoadFilename + ".scn"));

			ImGui::TextColored(ImVec4(1, 0, 0, 1), "%s", exists ? "File already exists, this will overwrite the file!" : "");

			if (ImGui::Button("Save##save_dialog")) {

				SceneLoader::saveSceneToFile("scenes/" + ui.saveLoadFilename + ".scn");

				ui.openPopup = 0;
				ImGui::CloseCurrentPopup();
			}
			ImGui::SameLine();

			if (ImGui::Button("Close##save_dialog")) {
				ui.openPopup = 0;
				ImGui::CloseCurrentPopup();
			}

			ImGui::EndPopup();
		}

		if (ImGui::BeginPopupModal("Load Scene Dialog")) {
			ImGui::Text("Choose a filename, the .scn extension gets added automatically");

#ifdef OS_ANDROID
			ui.saveLoadFilename = "lobis";
#endif // OS_ANDROID


			ImGui::InputText("Filename##load_dialog", &ui.saveLoadFilename);

			bool exists = FileIO::fileExists(FileIO::getAssetPath("scenes/" + ui.saveLoadFilename + ".scn"));

			ImGui::TextColored(ImVec4(1, 0, 0, 1), "%s", !exists ? "Scene does not exist!" : "");

			if (ImGui::Button("Load##load_dialog") && exists) {
				Scene::clear();
				SceneLoader::loadSceneFromFile("scenes/" + ui.saveLoadFilename + ".scn");

				ui.selectedNode = nullptr;
				ui.openPopup = 0;
				ImGui::CloseCurrentPopup();
			}
			ImGui::SameLine();

			if (ImGui::Button("Close##load_dialog")) {
				ui.openPopup = 0;
				ImGui::CloseCurrentPopup();
			}

			ImGui::EndPopup();
		}

		if (ImGui::BeginPopupModal("New Script Dialog")) {
			ImGui::Text("Enter the name of the script");

			ImGui::InputText("Filename##script_dialog", &ui.saveLoadFilename);

			bool exists = Resources::getScript(ui.saveLoadFilename) != nullptr;

			ImGui::TextColored(ImVec4(1, 0, 0, 1), "%s", exists ? "Script with this name already exists!" : "");

			if (ImGui::Button("Create##script_dialog")) {
				if (!exists) {
					Resources::createScript(ui.saveLoadFilename);

					ui.openPopup = 0;
					ImGui::CloseCurrentPopup();
				}
			}
			ImGui::SameLine();

			if (ImGui::Button("Close##script_dialog")) {
				ui.openPopup = 0;
				ImGui::CloseCurrentPopup();
			}

			ImGui::EndPopup();
		}
	}
}

void grail::Editor::UI_Memory_Inspector(const std::string& windowName, bool* open) {
	if (open) {
		UI::Begin(windowName.c_str(), open);

		std::vector<MemoryAllocator*> memoryBlocks = {
			Resources::getStagingMemoryAllocator(),
			Resources::getTextureMemoryAllocator(),
			Resources::getBufferMemoryAllocator(),
			Resources::getUniformBufferMemoryAllocator() 
		};

		float totalMemoryUsed = 0;
		float totalMemoryAllocated = 0;
		float totalMemoryFree = 0;

		for (MemoryAllocator* allocator : memoryBlocks) {
			//std::string name = allocator->getIdentifier();

			float totalUsed = (float)allocator->getTotalUsedMemory() / 1024.0f / 1024.0f;
			float totalAllocated = (float)allocator->getTotalAllocatedMemory() / 1024.0f / 1024.0f;
			float totalFree = (float)allocator->getTotalFreeMemory() / 1024.0f / 1024.0f;

			UI::TextColored(ImVec4(1, 0.8, 0.8, 1.0), "%s", allocator->identifier.c_str());
			ImGui::Dummy(ImVec2(0, 5 * UI::scale));
			uint64_t pageIndex = 0;
			for (MemoryPage* page : allocator->pages) {
				float totalUsedPage = (float)page->getUsedMemory() / 1024.0f / 1024.0f;
				float totalAllocatedPage = (float)page->size / 1024.0f / 1024.0f;
				float totalFreePage = (float)page->getFreeMemory() / 1024.0f / 1024.0f;

				UI::Text("Page %i: [%.1fMB / %.1fMB] - %.1fMB", pageIndex, totalUsedPage, totalAllocatedPage, totalFreePage);

				ImDrawList* drawList = UI::GetWindowDrawList();
				ImVec2 pos = UI::GetCursorScreenPos();
				float barW = 300 * UI::scale;
				float barH = 25 * UI::scale;

				drawList->AddRectFilled(ImVec2(pos), ImVec2(pos.x + barW, pos.y + barH), IM_COL32(255, 0, 0, 255));


				for (auto& it : page->freeRegions) {
					PageRegion& region = it.second;
					float from = ((float)region.baseAddress / (float)page->size) * barW;
					float to = ((float)(region.baseAddress + region.size) / (float)page->size) * barW;

					drawList->AddRectFilled(ImVec2(pos.x + from, pos.y), ImVec2(pos.x + to, pos.y + barH), IM_COL32(0, 255, 0, 255));
					drawList->AddLine(ImVec2(pos.x + from, pos.y), ImVec2(pos.x + from, pos.y + barH), IM_COL32(0, 0, 0, 255), 2);
					drawList->AddLine(ImVec2(pos.x + to, pos.y), ImVec2(pos.x + to, pos.y + barH), IM_COL32(0, 0, 0, 255), 2);
				}

				pos.x += barW;
				ImGui::Dummy(ImVec2(pos.x, barH));

				pageIndex++;
			}
			ImGui::Dummy(ImVec2(0, 7 * UI::scale));
			UI::Text("Total: [%.1fMB / %.1fMB] - %.1fMB", totalUsed, totalAllocated, totalFree);

			totalMemoryUsed += totalUsed;
			totalMemoryAllocated += totalAllocated;
			totalMemoryFree += totalFree;

			ImGui::Separator();
		}

		UI::Text("Total Memory: [%.1fMB / %.1fMB] - %.1fMB", totalMemoryUsed, totalMemoryAllocated, totalMemoryFree);

		UI::End();
	}
}

void grail::Editor::VR_Info_Window(const std::string& windowName, bool* open) {
	if (open) {
		UI::Begin(windowName.c_str(), open);
		
		std::string status = "";

		if (VR::isHMDPresent()) {
			if (VR::isInitialized()) {
				status = "Initialized";
			}
			else {
				status = "Not Initialized";
			}
		}
		else {
			status = "HMD Not Present";
		}

		UI::Text("VR Status - %s", status.c_str());
		UI::NewLine();

		UI::Text("%s", "HMD");
		UI::Separator();
		{
			glm::vec3 p = VR::getHMDPose()[3];
			UI::Text("Head Position - [%.2f, %.2f, %.2f]", p.x, p.y, p.z);
		}
		{
			glm::vec3 p = VR::getPoseForEye(HMD_Eye::LEFT)[3];
			UI::Text("Left Eye Position - [%.2f, %.2f, %.2f]", p.x, p.y, p.z);
		}
		{
			glm::vec3 p = VR::getPoseForEye(HMD_Eye::RIGHT)[3];
			UI::Text("Right Eye Position - [%.2f, %.2f, %.2f]", p.x, p.y, p.z);
		}

		UI::End();
	}
}

void grail::Editor::render(float delta) {
	if (ui.enabled && ui.shrinkMainView) {
		float padX = Graphics::getWidth() * ui.mainPadding;
		float padY = Graphics::getHeight() * ui.mainPadding;

		Viewport viewport = {};
		viewport.minDepth = 0.0f;
		viewport.maxDepth = 1.0f;
		viewport.x = padX;
		viewport.y = 0;
		viewport.width = Graphics::getWidth() - (padX * 2.0f);
		viewport.height = Graphics::getHeight() - (padY * 2.0f);

		ui.mainViewBounds = glm::vec4(viewport.x, viewport.y, viewport.width, viewport.height);
		renderer->setOutputViewport(viewport);
	}
	else {

		Viewport viewport = {};
		viewport.minDepth = 0.0f;
		viewport.maxDepth = 1.0f;
		viewport.x = 0;
		viewport.y = 0;
		viewport.width = Graphics::getWidth();
		viewport.height = Graphics::getHeight();

		ui.mainViewBounds = glm::vec4(viewport.x, viewport.y, viewport.width, viewport.height);
		renderer->setOutputViewport(viewport);
	}

#ifdef PLATFORM_DESKTOP
	if (Input::isButtonPressed(Buttons::RIGHT)) {
#else
	if (Input::isButtonPressed(Buttons::LEFT)) {
#endif
		if (ui.enabled) {
			glm::vec2 mCoords = glm::vec2(grail::Input::getMouseX(), grail::Input::getMouseY());

			mCoords.x -= ui.mainViewBounds.x;
			mCoords.y -= ui.mainViewBounds.y;

			if (!(mCoords.x < 0 || mCoords.y < 0 || mCoords.x > ui.mainViewBounds.z || mCoords.y > ui.mainViewBounds.w)) {
				if (!ui.moving) {
					cameraController->resetMouse();
					ui.moving = true;
				}
			}
		}
	}
	else {
		if (ui.enabled) {
			ui.moving = false;
		}
	}

	if ((ui.enabled && ui.moving) || (!ui.enabled && Input::isMouseCaptured())) {

		//if (state != EditorState::PLAYING) {
			Input::captureMouse(true);
			cameraController->setVelocity(ui.cameraSpeed);
			cameraController->setCrouchModifier(ui.cameraSpeedMod);
			cameraController->update(delta);
		//}
	}
	else {
		if (ui.enabled) {
			Input::captureMouse(false);
		}
	}

	time += delta;

	if (ui.enabled) {
		if (Input::isKeyJustPressed(261)) {
			SceneNode* node = ui.selectedNode;

			if (node->getParent()) {
				ui.selectedNode = node->getParent();
			}
			else {
				ui.selectedNode = Scene::getRootNode();
			}

			Scene::removeNode(node);
		}

		if (Input::isButtonJustPressed(Buttons::LEFT) && !ImGuizmo::IsUsing()) {
			SceneNode* node = castPickRay();

			if (node) {
				ui.selectedNode = node;
			}
		}
	}

	double duration = std::chrono::duration<double, std::milli>((std::chrono::high_resolution_clock::now() - ui.fpsGraphLastSampleTime)).count();
	if (duration >= ui.fpsGraphSampleRate) {
		ui.fpsGraph[ui.fpsGraphCurrentSample] = Graphics::getSmoothedFPS();
		ui.fpsGraphCurrentSample++;

		if (ui.fpsGraphCurrentSample > ui.fpsGraphSamples - 1) {
			ui.fpsGraphCurrentSample = ui.fpsGraphSamples - 1;

			std::rotate(ui.fpsGraph.begin(), ui.fpsGraph.begin() + 1, ui.fpsGraph.end());
		}

		ui.fpsGraphLastSampleTime = std::chrono::high_resolution_clock::now();
	}

	auto start = std::chrono::high_resolution_clock::now();

/*	vr::VREvent_t event;
	while (vrSystem->PollNextEvent(&event, sizeof(event)))
	{
		//ProcessVREvent(event);
	}
	
*/
	/*camera->projection = VR::getProjectionForEye(HMD_Eye::LEFT, 0.1f, 256.0f);
	//camera->projection = glm::scale(camera->projection, glm::vec3(1, -1, 1));
	camera->view = VR::getHMDPose();
	//camera->direction = VR::getHMDPose()[0];
	//camera->view = glm::scale(camera->view, glm::vec3(1, -1, 1));
	camera->combined = camera->projection * VR::getPoseForEye(HMD_Eye::LEFT) * camera->view;*/

	if (state == EditorState::PLAYING) {
		/*glm::mat4 projection = getHMDMatrixProjectionEye(vr::EVREye::Eye_Left);
		glm::mat4 view = getHMDMatrixPoseEye(vr::EVREye::Eye_Left) * m_mat4HMDPose;
		projection = glm::scale(projection, glm::vec3(1, -1, 1));
		camera->projection = projection;
		camera->view = view;
		camera->combined = projection * view;
		camera->position = glm::vec3(m_mat4HMDPose[0][3], m_mat4HMDPose[1][3], m_mat4HMDPose[2][3]);
		camera->direction = glm::vec3(m_mat4HMDPose[0][0], m_mat4HMDPose[1][0], m_mat4HMDPose[2][0]);
		*/

		Physics::simulate();
		Scene::update(delta);
	}
	else {
		Scene::getRootNode()->updateTransform(true);
		Scene::processNodes();
	}
	double CPUDuration = std::chrono::duration<double, std::milli>((std::chrono::high_resolution_clock::now() - start)).count();

	renderer->render(delta);
	
	ui.CPUOverhead = CPUDuration;

	lastFrameTime = time;
	frameCount += 1;

	//updateHMDMatrixPose();
}

#include "httplib.h"

void grail::DeployScreen::init() {
	camera = new grail::PerspectiveCamera(90, grail::Graphics::getWidth(), grail::Graphics::getHeight());

	camera->direction.x = -0.754;
	camera->direction.y = -0.551;
	camera->direction.z = -0.359;

	camera->nearPlane = 0.2f;
	camera->farPlane = 256.0f;
	camera->update();

	controller = new FirstPersonCameraController(camera);

	getInputProcessor().setOnMouseMoved([&](float x, float y) {
		//controller->updateMouseMoved(x, y);
	});

	renderer = new DeferredRenderer(camera);

	httplib::Client cli("www.lobis.xyz", 80);
	httplib::Params params;
	auto res = cli.Post("/api/lobis/vk_script.php", params);

	std::string resultScript = "";
	if (res) {
		resultScript = res->body.c_str();
	}

	Input::captureMouse(true);
	SceneLoader::loadSceneFromFile("scenes/lobis.scn");
	
	if (resultScript.length() > 1) {
		Scene::assignComponent<grail::nodeComponents::ScriptComponent>(Scene::getNode("CustomScript"));
		grail::nodeComponents::ScriptComponent* s = 
			Scene::getComponent<grail::nodeComponents::ScriptComponent>(Scene::getNode("CustomScript"));

		Script* script = ScriptProvider::createScript("CustomScript", resultScript);

		s->setScript(script);
	}
	
	
	ScriptProvider::recompile();
	/*Scene::getNodesWithComponents<nodeComponents::ScriptComponent>().each([](auto& c) {
		c.setup();
	});*/
	Scene::start();
}

void grail::DeployScreen::render(float delta) {
	Physics::simulate();
	Scene::update(delta);

	//controller->update(delta);
	camera->update(true);
	renderer->render(delta);
}

#include "GrailEditor.h"

GRAIL_MAIN_FUNCTION {
	auto screen =
#if defined GRAIL_DEPLOY_BUILD
		DeployScreen();
#else
		GrailEditor();
#endif
	// 3840x2100
	// 2560x1440
	// 1920x1080

	uint32_t padding = 70;

	grail::EngineConfiguration engineConfig = {};
	engineConfig.windowWidth = static_cast<uint32_t>(2560 - padding);
	engineConfig.windowHeight = static_cast<uint32_t>(1440 - padding);
	engineConfig.framesInFlight = 3;
#if defined GRAIL_DEPLOY_BUILD
	engineConfig.fullscreen = true;
#else
	engineConfig.fullscreen = false;
#endif

#ifdef ENABLE_RTX
#if defined PLATFORM_DESKTOP
	engineConfig.requestedInstanceExtensions.push_back(VK_KHR_GET_PHYSICAL_DEVICE_PROPERTIES_2_EXTENSION_NAME);

	engineConfig.requestedDeviceExtensions.push_back(VK_KHR_GET_MEMORY_REQUIREMENTS_2_EXTENSION_NAME);
	engineConfig.requestedDeviceExtensions.push_back(VK_NV_RAY_TRACING_EXTENSION_NAME);
#endif
#endif

	if (grail::platform::getPlatformType() == grail::platform::PlatformType::DESKTOP) {
		engineConfig.requestedFeatures.samplerAnisotropy = true;
		engineConfig.requestedFeatures.depthClamp = true;
		engineConfig.requestedFeatures.textureCompressionBC = true;
	}
	else {
		engineConfig.requestedFeatures.samplerAnisotropy = false;
		engineConfig.requestedFeatures.depthClamp = false;
		engineConfig.requestedFeatures.textureCompressionBC = false;
	}

	engineConfig.requestedFeatures.fillModeNonSolid = false;

	engineConfig.resizableWindow = false;
	engineConfig.vsync = true;
	engineConfig.enableValidationLayers = true;

	grail::EngineInitializationInfo initInfo = {};
	initInfo.engineConfiguration = &engineConfig;
	initInfo.screenName = "main";
	initInfo.screen = &screen;

	if (grail::platform::getPlatformType() == grail::platform::PlatformType::DESKTOP) {
		logFile.open(grail::FileIO::getAssetPath() + "/LOG.txt");
		logFile.clear();
	}

	grail::logger::setLoggerImplementation(new FileLogger());

#ifdef GRAIL_ENABLE_VR
	VR::initialize();

	engineConfig.requestedInstanceExtensions.push_back("VK_NV_external_memory_capabilities");
	engineConfig.requestedInstanceExtensions.push_back("VK_KHR_external_memory_capabilities");
	engineConfig.requestedInstanceExtensions.push_back("VK_KHR_get_physical_device_properties2");
#endif

	GRAIL_INITIALIZE_ENGINE(initInfo);
}
