#include "ModelLoader.h"
#include "FileIO.h"

#include <limits>


void ReplaceStringInPlace(std::string& subject, const std::string& search,
	const std::string& replace) {
	size_t pos = 0;
	while ((pos = subject.find(search, pos)) != std::string::npos) {
		subject.replace(pos, search.length(), replace);
		pos += replace.length();
	}
}


std::vector<std::string> list_dir(const char *path) {
	std::vector<std::string> files;


	return files;
}

struct GroupedMesh {
	std::vector<float> vertices;
	std::vector<uint32_t> indices;
};

std::vector<grail::MeshDataR> grail::ModelLoader::loadFile(VertexLayout & layout, const std::string & path, const std::string & texturePathPrefix) {
	std::vector<grail::MeshDataR> result;


#ifdef TINY_OBJ
	uint32_t size;
	char* contents;

	grail::FileIO::readBinaryFile(path, &contents, &size);

	tinyobj::attrib_t attribs;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;

	std::string error;

	std::istringstream is = std::istringstream(std::string(contents, size));

	delete[] contents;
	contents = nullptr;

	bool success;
	bool useMaterial = materialFile.compare("none") != 0;

	if(!useMaterial)
		success = tinyobj::LoadObj(&attribs, &shapes, &materials, &error, &is);
	else {
		grail::FileIO::readBinaryFile(materialFile, &contents, &size);

		std::istringstream mIs = std::istringstream(std::string(contents, size));

		success = tinyobj::LoadObj(&attribs, &shapes, &materials, &error, &is, new tinyobj::MaterialStreamReader(mIs));

		delete[] contents;
		contents = nullptr;
	}

	if (!error.empty())
		GRAIL_LOG(ERROR, "MODEL LOADER") << error;

	if (!success)
		throw new exceptions::RuntimeException("Could not load model: " + path);

	/*bool hasPositions = layout.hasAttributeType(VertexAttributeType::POSITION);
	bool hasNormals = layout.hasAttributeType(VertexAttributeType::NORMAL);
	bool hasTexCoords = layout.hasAttributeType(VertexAttributeType::UV);
	bool hasDummyVerts = layout.hasAttributeType(VertexAttributeType::DUMMY);*/

	uint32_t attribSize = layout.getVertexElementCount();

	if (useMaterial) {
		std::unordered_map<int, MeshData> materialMap;
		for (size_t s = 0; s < shapes.size(); s++) {
			size_t index_offset = 0;

			for (size_t f = 0; f < shapes[s].mesh.num_face_vertices.size(); f++) {
				int materialID = shapes[s].mesh.material_ids[f];

				if (materialMap.find(materialID) == materialMap.end()) {
					materialMap.insert({ materialID, MeshData() });

					MeshData& mesh = materialMap.at(materialID);

					if (useMaterial) {
						if (materialID > -1) {
							std::string path = materials[materialID].diffuse_texname;

							ReplaceStringInPlace(path, "\\", "/");

							mesh.diffusePath = path;
							mesh.color = glm::vec3(materials[materialID].diffuse[0], materials[materialID].diffuse[1], materials[materialID].diffuse[2]);
						}
						else {
							mesh.diffusePath = "none";
							mesh.color = glm::vec3(1, 1, 1);
						}
					}
				}
			}
		}

		for (size_t s = 0; s < shapes.size(); s++) {
			size_t index_offset = 0;

			for (size_t f = 0; f < shapes[s].mesh.num_face_vertices.size(); f++) {
				int materialID = shapes[s].mesh.material_ids[f];

				MeshData& mesh = materialMap.at(materialID);

				uint32_t fv = shapes[s].mesh.num_face_vertices[f];

				for (size_t v = 0; v < fv; v++) {
					tinyobj::index_t idx = shapes[s].mesh.indices[index_offset + v];

					for (uint32_t i = 0; i < layout.getVertexAttributeCount(); i++) {
						VertexAttributeType type = layout.getVertexAttribute(i).getType();

						switch (type)
						{
						case VertexAttributeType::POSITION:
							mesh.vertices.push_back(attribs.vertices[3 * idx.vertex_index + 0]);
							mesh.vertices.push_back(attribs.vertices[3 * idx.vertex_index + 1]);
							mesh.vertices.push_back(attribs.vertices[3 * idx.vertex_index + 2]);
							break;
						case VertexAttributeType::NORMAL:
							mesh.vertices.push_back(attribs.normals[3 * idx.normal_index + 0]);
							mesh.vertices.push_back(attribs.normals[3 * idx.normal_index + 1]);
							mesh.vertices.push_back(attribs.normals[3 * idx.normal_index + 2]);
							break;
						case VertexAttributeType::UV:
							mesh.vertices.push_back(attribs.texcoords[2 * idx.texcoord_index + 0]);
							mesh.vertices.push_back(attribs.texcoords[2 * idx.texcoord_index + 1]);
							break;
						case VertexAttributeType::DUMMY:
							for (uint32_t c = 0; c < layout.getVertexAttribute(i).getElementCount(); c++)
								mesh.vertices.push_back(0.0f);
							break;
						}
					}

					mesh.indices.push_back(mesh.indexOffset + v);
				}
				mesh.indexOffset += fv;
				index_offset += fv;
			}
		}

		std::unordered_map<int, MeshData>::iterator it;
		for (it = materialMap.begin(); it != materialMap.end(); it++) {
			result.push_back(materialMap.at(it->first));
		}
	}
	else {
		for (size_t s = 0; s < shapes.size(); s++) {
			size_t index_offset = 0;

			result.push_back(MeshData());

			for (size_t f = 0; f < shapes[s].mesh.num_face_vertices.size(); f++) {
				int materialID = shapes[s].mesh.material_ids[f];

				MeshData& mesh = result.at(s);

				uint32_t fv = shapes[s].mesh.num_face_vertices[f];

				for (size_t v = 0; v < fv; v++) {
					tinyobj::index_t idx = shapes[s].mesh.indices[index_offset + v];

					for (uint32_t i = 0; i < layout.getVertexAttributeCount(); i++) {
						VertexAttributeType type = layout.getVertexAttribute(i).getType();

						switch (type)
						{
						case VertexAttributeType::POSITION:
							mesh.vertices.push_back(attribs.vertices[3 * idx.vertex_index + 0]);
							mesh.vertices.push_back(attribs.vertices[3 * idx.vertex_index + 1]);
							mesh.vertices.push_back(attribs.vertices[3 * idx.vertex_index + 2]);
							break;
						case VertexAttributeType::NORMAL:
							mesh.vertices.push_back(attribs.normals[3 * idx.normal_index + 0]);
							mesh.vertices.push_back(attribs.normals[3 * idx.normal_index + 1]);
							mesh.vertices.push_back(attribs.normals[3 * idx.normal_index + 2]);
							break;
						case VertexAttributeType::UV:
							mesh.vertices.push_back(attribs.texcoords[2 * idx.texcoord_index + 0]);
							mesh.vertices.push_back(attribs.texcoords[2 * idx.texcoord_index + 1]);
							break;
						case VertexAttributeType::DUMMY:
							for (uint32_t c = 0; c < layout.getVertexAttribute(i).getElementCount(); c++)
								mesh.vertices.push_back(0.0f);
							break;
						}
					}

					mesh.indices.push_back(mesh.indexOffset + v);
				}
				mesh.indexOffset += fv;
				index_offset += fv;
			}
		}
	}

	GRAIL_LOG(INFO, "SIZE") << result.size();

	//TODO: Group meshes by material

	/*result.resize(shapes.size());
	GRAIL_LOG(INFO, "ORIGINAL SIZE") << shapes.size();

	for (size_t s = 0; s < shapes.size(); s++) {
		size_t index_offset = 0;

		result[s].vertices.resize(shapes[s].mesh.num_face_vertices.size() * 3 * attribSize);
		result[s].indices.resize(shapes[s].mesh.num_face_vertices.size() * 3);

		uint32_t vertexArrayOffset = 0;
		uint32_t indexArrayOffset = 0;

		float minX = std::numeric_limits<float>().max();
		float minY = std::numeric_limits<float>().max();
		float minZ = std::numeric_limits<float>().max();

		float maxX = std::numeric_limits<float>().min();
		float maxY = std::numeric_limits<float>().min();
		float maxZ = std::numeric_limits<float>().min();

		for (size_t f = 0; f < shapes[s].mesh.num_face_vertices.size(); f++) {
			uint32_t fv = shapes[s].mesh.num_face_vertices[f];

			for (size_t v = 0; v < fv; v++) {
				tinyobj::index_t idx = shapes[s].mesh.indices[index_offset + v];

				MeshData& mesh = result[s];

				for (uint32_t i = 0; i < layout.getVertexAttributeCount(); i++) {
					VertexAttributeType type = layout.getVertexAttribute(i).getType();

					switch (type)
					{
					case VertexAttributeType::POSITION:
						mesh.vertices[vertexArrayOffset++] = (attribs.vertices[3 * idx.vertex_index + 0]);
						mesh.vertices[vertexArrayOffset++] = (attribs.vertices[3 * idx.vertex_index + 1]);
						mesh.vertices[vertexArrayOffset++] = (attribs.vertices[3 * idx.vertex_index + 2]);
						break;
					case VertexAttributeType::NORMAL:
						mesh.vertices[vertexArrayOffset++] = (attribs.normals[3 * idx.normal_index + 0]);
						mesh.vertices[vertexArrayOffset++] = (attribs.normals[3 * idx.normal_index + 1]);
						mesh.vertices[vertexArrayOffset++] = (attribs.normals[3 * idx.normal_index + 2]);
						break;
					case VertexAttributeType::UV:
						mesh.vertices[vertexArrayOffset++] = (attribs.texcoords[2 * idx.texcoord_index + 0]);
						mesh.vertices[vertexArrayOffset++] = (attribs.texcoords[2 * idx.texcoord_index + 1]);
						break;
					case VertexAttributeType::DUMMY:
						for (uint32_t c = 0; c < layout.getVertexAttribute(i).getElementCount(); c++)
							mesh.vertices[vertexArrayOffset++] = (0.0f);
						break;
					}
				}

				result[s].indices[indexArrayOffset++] = (index_offset + v);
			}
			index_offset += fv;

			if (useMaterial) {
				int matID = shapes[s].mesh.material_ids[f];

				if (matID > -1) {
					std::string path = materials[matID].diffuse_texname;

					ReplaceStringInPlace(path, "\\", "/");

					result[s].diffusePath = path;
					result[s].color = glm::vec3(materials[matID].diffuse[0], materials[matID].diffuse[1], materials[matID].diffuse[2]);
				}
				else {
					result[s].diffusePath = "none";
					result[s].color = glm::vec3(1, 1, 1);
				}
			}
		}

		float radius = std::max(std::max(maxX, maxY), maxZ) - std::min(std::min(minX, minY), minZ);

		result[s].radius = radius;
	}*/
#else
	Assimp::Importer importer = Assimp::Importer();
	const aiScene* scene = importer.ReadFile(path, 
		aiProcess_JoinIdenticalVertices |
		aiProcess_ValidateDataStructure |
		aiProcess_ImproveCacheLocality |
		aiProcess_RemoveRedundantMaterials |
		aiProcess_FindDegenerates |
		aiProcess_FindInvalidData |
		aiProcess_GenUVCoords |
		aiProcess_FlipUVs |
		aiProcess_FindInstances |
		aiProcess_OptimizeMeshes |
		aiProcess_OptimizeGraph |
		aiProcess_GenSmoothNormals |
		aiProcess_Triangulate |
		aiProcess_SortByPType |
		aiProcess_CalcTangentSpace
	);

	if ((!scene) || (scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE) || (!scene->mRootNode))
		throw new exceptions::RuntimeException("Could not load Model \"" + path + "\": " + importer.GetErrorString());

	result.resize(scene->mNumMeshes);

	for (unsigned int i = 0; i < scene->mNumMeshes; ++i) {
		aiMesh* mesh = scene->mMeshes[i];
		aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];

		if (material) {
			aiString tPath;

			material->GetTexture(aiTextureType_DIFFUSE, 0, &tPath);

			std::string tString = tPath.C_Str();
			ReplaceStringInPlace(tString, "\\", "/");

			//if(tString.length() > 0)
				result[i].diffusePath = (texturePathPrefix + tString);
		}

		for (unsigned int j = 0; j < mesh->mNumVertices; ++j) {
			result[i].vertices.push_back(mesh->mVertices[j].x);
			result[i].vertices.push_back(mesh->mVertices[j].y);
			result[i].vertices.push_back(mesh->mVertices[j].z);

			result[i].vertices.push_back(mesh->mNormals[j].x);
			result[i].vertices.push_back(mesh->mNormals[j].y);
			result[i].vertices.push_back(mesh->mNormals[j].z);

			result[i].vertices.push_back(mesh->mTextureCoords[0][j].x);
			result[i].vertices.push_back(mesh->mTextureCoords[0][j].y);
		}

		for (unsigned int j = 0; j < mesh->mNumFaces; ++j) {
			aiFace face = mesh->mFaces[j];
			for (unsigned int k = 0; k < face.mNumIndices; ++k) { result[i].indices.push_back(face.mIndices[k]); }
		}
	}

	//GRAIL_LOG(INFO, "COUNT") << result.size();

#endif
	return result;
}

std::vector<grail::MeshDataR> grail::ModelLoader::loadFiles(VertexLayout & layout, const std::string & path, const std::string & texturePathPrefix) {
	std::vector<MeshDataR> data;

	std::vector<std::string> files = list_dir(path.c_str());
	std::vector<std::string> models;

	for (std::string& filename : files) {
		if (filename.find(".obj") != std::string::npos) {
			models.push_back(filename);
		}
	}

	for (std::string filename : models) {

		std::string fn = filename;
		ReplaceStringInPlace(fn, ".obj", "");

		//GRAIL_LOG(INFO, "PPP") << ("pumping_station/") + fn + "_albedo.tga";

		std::vector<MeshDataR> d = loadFile(layout, path + filename, ("pumping_station/") + fn + "_albedo.tga");

		data.insert(data.end(), d.begin(), d.end());
	}

	return data;
}

grail::MeshDataR grail::ModelLoader::processNode(aiNode * node, const aiScene * scene) {
	return MeshDataR();
}


