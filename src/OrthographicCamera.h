#ifndef GRAIL_ORTHOGRAPHIC_CAMERA_H
#define GRAIL_ORTHOGRAPHIC_CAMERA_H

#include "Camera.h"

namespace grail {
	class OrthographicCamera : public Camera {
	public:
		float zoom = 1;

		OrthographicCamera();
		OrthographicCamera(float viewportWidth, float viewportHeight);

		void update(bool updateFrustum = true);

		void setToOrtho(bool yDown);
		void setToOrtho(bool yDown, float viewportWidth, float viewportHeight);

		void translate(float x, float y);
		void translate(glm::vec2 translation);
	};
}

#endif