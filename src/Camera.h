#ifndef GRAIL_CAMERA_H
#define GRAIL_CAMERA_H

#include "Common.h"
#include "Frustum.h"

namespace grail {
	class Camera {
	public:
		glm::vec3 position;
		glm::vec3 direction;
		glm::vec3 up;

		glm::mat4 projection;
		glm::mat4 view;
		glm::mat4 combined;
		glm::mat4 invProjectionView;

		float nearPlane = 1.0f;
		float farPlane = 100.0f;

		float viewportWidth;
		float viewportHeight;

		Frustum frustum;

		Camera();

		virtual void update(bool updateFrustum = true) = 0;

		void normalizeUp();

		void lookAt(float x, float y, float z);
		void lookAt(const glm::vec3& target);

		void translate(float x, float y, float z);
		void translate(const glm::vec3& translation);

		glm::vec3 unprojectViewport(glm::vec3 screenCoords, float viewportX, float viewportY, float viewportWidth, float viewportHeight);
		glm::vec3 unproject(glm::vec3 screenCoords);

		glm::vec3 projectViewport(glm::vec3 worldCoords, float viewportX, float viewportY, float viewportWidth, float viewportHeight);
		glm::vec3 project(glm::vec3 worldCoords);
	};
}

#endif