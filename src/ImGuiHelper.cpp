#include "ImGuiHelper.h"

#include "Resources.h"
#include "VkTexture.h"
#include "VkBuffer.h"
#include "Material.h"
#include "VertexLayout.h"
#include "MaterialInstance.h"

#include "FileIO.h"
#include "ImGuizmo.h"

float ImGui::scale = 0.0f;

void grail::ImGuiHelper::initialize(VkRenderPass renderPass, const std::string& settingsFile) {
	context = ImGui::CreateContext();

	if (!settingsFile.empty()) {
		if (FileIO::fileExists(settingsFile)) {
			uint32_t size = 0;
			char* contents = nullptr;

			FileIO::readBinaryFileExternal(settingsFile, &contents, &size);

			ImGui::LoadIniSettingsFromMemory(contents, size);
		
			delete[] contents;
		}
	}

	ImGuiStyle & style = ImGui::GetStyle();

	style.AntiAliasedFill = true;
	style.AntiAliasedLines = true;
	style.FrameBorderSize = .0f;

	ImGui::GetStyle().WindowBorderSize = 0.0f;
	ImGui::GetStyle().ChildBorderSize = 0.0f;
	ImGui::GetStyle().FrameBorderSize = 1.0f;
	
	ImGui::GetStyle().FrameRounding = 4.0f;
	ImGui::GetStyle().GrabRounding = 4.0f;
	ImGui::GetStyle().WindowRounding = 4.0f;
	

	ImVec4* colors = ImGui::GetStyle().Colors;
	colors[ImGuiCol_Text] = ImVec4(0.95f, 0.96f, 0.98f, 1.00f);
	colors[ImGuiCol_TextDisabled] = ImVec4(0.36f, 0.42f, 0.47f, 1.00f);
	colors[ImGuiCol_WindowBg] = ImVec4(0.11f, 0.15f, 0.17f, 1.00f);
	colors[ImGuiCol_ChildBg] = ImVec4(0.15f, 0.18f, 0.22f, 1.00f);
	colors[ImGuiCol_PopupBg] = ImVec4(0.08f, 0.08f, 0.08f, 0.94f);
	colors[ImGuiCol_Border] = ImVec4(0.08f, 0.10f, 0.12f, 1.00f);
	colors[ImGuiCol_BorderShadow] = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
	colors[ImGuiCol_FrameBg] = ImVec4(0.20f, 0.25f, 0.29f, 1.00f);
	colors[ImGuiCol_FrameBgHovered] = ImVec4(0.12f, 0.20f, 0.28f, 1.00f);
	colors[ImGuiCol_FrameBgActive] = ImVec4(0.09f, 0.12f, 0.14f, 1.00f);
	colors[ImGuiCol_TitleBg] = ImVec4(0.09f, 0.12f, 0.14f, 0.65f);
	colors[ImGuiCol_TitleBgActive] = ImVec4(0.08f, 0.10f, 0.12f, 1.00f);
	colors[ImGuiCol_TitleBgCollapsed] = ImVec4(0.00f, 0.00f, 0.00f, 0.51f);
	colors[ImGuiCol_MenuBarBg] = ImVec4(0.15f, 0.18f, 0.22f, 1.00f);
	colors[ImGuiCol_ScrollbarBg] = ImVec4(0.02f, 0.02f, 0.02f, 0.39f);
	colors[ImGuiCol_ScrollbarGrab] = ImVec4(0.20f, 0.25f, 0.29f, 1.00f);
	colors[ImGuiCol_ScrollbarGrabHovered] = ImVec4(0.18f, 0.22f, 0.25f, 1.00f);
	colors[ImGuiCol_ScrollbarGrabActive] = ImVec4(0.09f, 0.21f, 0.31f, 1.00f);
	colors[ImGuiCol_CheckMark] = ImVec4(0.28f, 0.56f, 1.00f, 1.00f);
	colors[ImGuiCol_SliderGrab] = ImVec4(0.28f, 0.56f, 1.00f, 1.00f);
	colors[ImGuiCol_SliderGrabActive] = ImVec4(0.37f, 0.61f, 1.00f, 1.00f);
	colors[ImGuiCol_Button] = ImVec4(0.20f, 0.25f, 0.29f, 1.00f);
	colors[ImGuiCol_ButtonHovered] = ImVec4(0.28f, 0.56f, 1.00f, 1.00f);
	colors[ImGuiCol_ButtonActive] = ImVec4(0.06f, 0.53f, 0.98f, 1.00f);
	colors[ImGuiCol_Header] = ImVec4(0.20f, 0.25f, 0.29f, 0.55f);
	colors[ImGuiCol_HeaderHovered] = ImVec4(0.26f, 0.59f, 0.98f, 0.80f);
	colors[ImGuiCol_HeaderActive] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
	colors[ImGuiCol_Separator] = ImVec4(0.20f, 0.25f, 0.29f, 1.00f);
	colors[ImGuiCol_SeparatorHovered] = ImVec4(0.10f, 0.40f, 0.75f, 0.78f);
	colors[ImGuiCol_SeparatorActive] = ImVec4(0.10f, 0.40f, 0.75f, 1.00f);
	colors[ImGuiCol_ResizeGrip] = ImVec4(0.26f, 0.59f, 0.98f, 0.25f);
	colors[ImGuiCol_ResizeGripHovered] = ImVec4(0.26f, 0.59f, 0.98f, 0.67f);
	colors[ImGuiCol_ResizeGripActive] = ImVec4(0.26f, 0.59f, 0.98f, 0.95f);
	colors[ImGuiCol_Tab] = ImVec4(0.11f, 0.15f, 0.17f, 1.00f);
	colors[ImGuiCol_TabHovered] = ImVec4(0.26f, 0.59f, 0.98f, 0.80f);
	colors[ImGuiCol_TabActive] = ImVec4(0.20f, 0.25f, 0.29f, 1.00f);
	colors[ImGuiCol_TabUnfocused] = ImVec4(0.11f, 0.15f, 0.17f, 1.00f);
	colors[ImGuiCol_TabUnfocusedActive] = ImVec4(0.11f, 0.15f, 0.17f, 1.00f);
	colors[ImGuiCol_PlotLines] = ImVec4(0.61f, 0.61f, 0.61f, 1.00f);
	colors[ImGuiCol_PlotLinesHovered] = ImVec4(1.00f, 0.43f, 0.35f, 1.00f);
	colors[ImGuiCol_PlotHistogram] = ImVec4(0.90f, 0.70f, 0.00f, 1.00f);
	colors[ImGuiCol_PlotHistogramHovered] = ImVec4(1.00f, 0.60f, 0.00f, 1.00f);
	colors[ImGuiCol_TextSelectedBg] = ImVec4(0.26f, 0.59f, 0.98f, 0.35f);
	colors[ImGuiCol_DragDropTarget] = ImVec4(1.00f, 1.00f, 0.00f, 0.90f);
	colors[ImGuiCol_NavHighlight] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
	colors[ImGuiCol_NavWindowingHighlight] = ImVec4(1.00f, 1.00f, 1.00f, 0.70f);
	colors[ImGuiCol_NavWindowingDimBg] = ImVec4(0.80f, 0.80f, 0.80f, 0.20f);
	colors[ImGuiCol_ModalWindowDimBg] = ImVec4(0.80f, 0.80f, 0.80f, 0.35f);
    

	// Dimensions
	ImGuiIO& io = ImGui::GetIO();

	io.IniFilename = nullptr;
	if (FileIO::fileExists(FileIO::getAssetPath("layout.ini"))) {
		char* contents;
		uint32_t size = 0;
		FileIO::readBinaryFile(("layout.ini"), &contents, &size);
		ImGui::LoadIniSettingsFromMemory(contents, size);
	}

	io.ConfigInputTextCursorBlink = true;
	io.DisplaySize = ImVec2(Graphics::getWidth() / 2, Graphics::getHeight() / 2);
	io.DisplayFramebufferScale = ImVec2(1.0f, .0f);
	io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard | ImGuiConfigFlags_DockingEnable;
	io.ConfigWindowsMoveFromTitleBarOnly = true;

	if (platform::getPlatformType() == platform::PlatformType::MOBILE) {
		io.ConfigFlags |= ImGuiConfigFlags_IsTouchScreen;
	}

	//}

	io.KeyMap[ImGuiKey_Tab] = Keys::TAB;                     // Keyboard mapping. ImGui will use those indices to peek into the io.KeyDown[] array.
	io.KeyMap[ImGuiKey_LeftArrow] = Keys::LEFT;
	io.KeyMap[ImGuiKey_RightArrow] = Keys::RIGHT;
	io.KeyMap[ImGuiKey_UpArrow] = Keys::UP;
	io.KeyMap[ImGuiKey_DownArrow] = Keys::DOWN;
	io.KeyMap[ImGuiKey_PageUp] = Keys::PAGE_UP;
	io.KeyMap[ImGuiKey_PageDown] = Keys::PAGE_DOWN;
	io.KeyMap[ImGuiKey_Home] = Keys::HOME;
	io.KeyMap[ImGuiKey_End] = Keys::END;
	io.KeyMap[ImGuiKey_Delete] = 261;
	io.KeyMap[ImGuiKey_Backspace] = Keys::BACKSPACE;
	io.KeyMap[ImGuiKey_Enter] = Keys::ENTER;
	io.KeyMap[ImGuiKey_Space] = Keys::SPACE;
	io.KeyMap[ImGuiKey_Escape] = Keys::ESCAPE;
	io.KeyMap[ImGuiKey_A] = Keys::A;
	io.KeyMap[ImGuiKey_C] = Keys::C;
	io.KeyMap[ImGuiKey_V] = Keys::V;
	io.KeyMap[ImGuiKey_X] = Keys::X;
	io.KeyMap[ImGuiKey_Y] = Keys::Y;
	io.KeyMap[ImGuiKey_Z] = Keys::Z;

	// Create font texture
	unsigned char* fontData;
	int texWidth, texHeight;
	
	uint32_t size;
	char* data;

	float baseScale = 0.6f;

	float hRatio = 1080.0f / Graphics::getHeight();
	float wRatio = 1920.0f / Graphics::getWidth();

	float maxRatio = std::max(hRatio, wRatio);

	baseScale /= maxRatio;
	ImGui::scale = baseScale;

	FileIO::readBinaryFile("fonts/Muli-Bold.ttf", &data, &size);
	ImFontConfig config;
	config.OversampleH = 8;
	config.OversampleV = 8;

	ImFont* font = io.Fonts->AddFontFromMemoryTTF(data, size, 25.0f * baseScale, &config);
	font->DisplayOffset.y = -baseScale;
	style.ScaleAllSizes(baseScale * 1.2f);

	io.Fonts->GetTexDataAsRGBA32(&fontData, &texWidth, &texHeight);
	VkDeviceSize uploadSize = texWidth * texHeight * 4 * sizeof(char);

	TextureCreateInfo attachmentInfo = {};
	attachmentInfo.generateMipmaps = false;
	attachmentInfo.samplerInfo.addressModeU = SamplerAddressMode::CLAMP_TO_EDGE;
	attachmentInfo.samplerInfo.addressModeV = SamplerAddressMode::CLAMP_TO_EDGE;
	attachmentInfo.samplerInfo.magFilter = SamplerFilter::NEAREST;
	attachmentInfo.samplerInfo.minFilter = SamplerFilter::NEAREST;
	attachmentInfo.format = VK_FORMAT_R8G8B8A8_UNORM;
	fontTexture = Resources::createTexture("imguiFontTexture", texWidth, texHeight, fontData, uploadSize, attachmentInfo);

	io.FontDefault = font;

	DescriptorLayout descriptorLayout;
	descriptorLayout.addDescriptor({ "texture", ShaderStageBits::FRAGMENT, DescriptorBindingType::COMBINED_IMAGE_SAMPLER, 0, 0 });
	descriptorLayout.addPushConstant({ ShaderStageBits::VERTEX, 0, sizeof(glm::vec4) });
	descriptorLayout.finalize();

	for (int i = 0; i < imageDescriptors.size(); i++) {
		imageDescriptors[i] = new DescriptorGroup(descriptorLayout);
		imageDescriptors[i]->getBinding("texture").setBinding(fontTexture, &fontTexture->getDescriptor());
		imageDescriptors[i]->update();
	}

	int quadCount = 100000;

	BufferCreateInfo bufferInfo = {};
	bufferInfo.memoryType = MemoryType::DYNAMIC;
	bufferInfo.memoryProperties |= MemoryPropertyFlags::HOST_COHERENT;
	bufferInfo.usageBits = BufferUsageBits::VERTEX_BUFFER_BIT;
	bufferInfo.size = sizeof(ImDrawVert) * quadCount;
	vertexBuffer = new Buffer(bufferInfo, Resources::getBufferMemoryAllocator());

	bufferInfo.usageBits = BufferUsageBits::INDEX_BUFFER_BIT;
	bufferInfo.size = sizeof(ImDrawIdx) * quadCount * 6;
	indexBuffer = new Buffer(bufferInfo, Resources::getBufferMemoryAllocator());

	vertexLayout = new VertexLayout({
		VertexAttribute(VertexAttributeType::POSITION, VK_FORMAT_R32G32_SFLOAT, 2, sizeof(float)),
		VertexAttribute(VertexAttributeType::UV, VK_FORMAT_R32G32_SFLOAT, 2, sizeof(float)),
		VertexAttribute(VertexAttributeType::COLOR, VK_FORMAT_R8G8B8A8_UNORM, 3, sizeof(uint32_t), true)
	});

	PushConstantRange pushConstantRange = {};
	pushConstantRange.stageFlags = ShaderStageBits::VERTEX;
	pushConstantRange.size = sizeof(glm::vec4);
	pushConstantRange.offset = 0;

	GraphicsPipelineCreateInfo pipelineInfo = {};
	pipelineInfo.colorBlendState.attachmentCount = 1;
	pipelineInfo.renderPass = renderPass;

	Shader* vS = Resources::loadSPIRVShader("shaders/glsl/ui.vert");
	Shader* fS = Resources::loadSPIRVShader("shaders/glsl/ui.frag");

	pipelineInfo.rasterizationState.cullMode = CullMode::NONE;

	pipelineInfo.colorBlendState.blendEnable = VK_TRUE;
	pipelineInfo.colorBlendState.srcColorBlendFactor = BlendFactor::SRC_ALPHA;
	pipelineInfo.colorBlendState.dstColorBlendFactor = BlendFactor::ONE_MINUS_SRC_ALPHA;
	pipelineInfo.colorBlendState.srcAlphaBlendFactor = BlendFactor::ONE_MINUS_SRC_ALPHA;
	pipelineInfo.colorBlendState.dstAlphaBlendFactor = BlendFactor::ZERO;

	material = new MaterialDescription("ImGuiMaterial");
	Material* base = material->createMaterial(*vertexLayout, descriptorLayout, { vS->getBasePermutation(), fS->getBasePermutation() }, pipelineInfo);
	matInstance = base->createInstance();
	matInstance->setDescriptorBinding("texture", fontTexture, &fontTexture->getDescriptor());
	matInstance->updateAllDescriptors();

	io.MousePos = ImVec2(Graphics::getWidth() * 0.5f, Graphics::getHeight() * 0.5f);
	io.MouseDown[0] = 1;
}

void grail::ImGuiHelper::render(VkCommandBuffer cmd, std::function<void()> renderFunc) {
	ImGuiIO& io = ImGui::GetIO();
	io.DeltaTime = Graphics::getDeltaTime();

	ImGui::NewFrame();
	ImGuizmo::BeginFrame();

	ImGuizmo::SetRect(0, 0, io.DisplaySize.x, io.DisplaySize.y);

	renderFunc();

	ImGui::EndFrame();
	ImGui::Render();

	io.DisplaySize = ImVec2((float)Graphics::getWidth(), (float)Graphics::getHeight());
	io.DeltaTime = Graphics::getDeltaTime();

	if (platform::getPlatformType() == platform::PlatformType::MOBILE) {
		io.MousePos = ImVec2(Input::getMouseX(), Input::getMouseY());
	}
	else {
		if (Input::isMouseCaptured()) {
			io.MousePos = ImVec2(-1, -1);
		} else {
			io.MousePos = ImVec2(Input::getMouseX(), Input::getMouseY());
		}
	}
	io.MouseDown[0] = Input::isButtonPressed(Buttons::LEFT);
	io.MouseDown[1] = Input::isButtonPressed(Buttons::RIGHT);
	io.MouseWheel = Input::getMouseScrollY();

	ImDrawData* imDrawData = ImGui::GetDrawData();

	// Upload data
	vertexBuffer->map();
	ImDrawVert* vtxDst = (ImDrawVert*)(static_cast<char*>(vertexBuffer->getMappedPtr()) + vertexBuffer->getOffset());
	for (int n = 0; n < imDrawData->CmdListsCount; n++) {
		const ImDrawList* cmd_list = imDrawData->CmdLists[n];
		memcpy(vtxDst, cmd_list->VtxBuffer.Data, cmd_list->VtxBuffer.Size * sizeof(ImDrawVert));
		vtxDst += cmd_list->VtxBuffer.Size;
	}
	vertexBuffer->unmap();
	//vertexBuffer->flush();

	indexBuffer->map();
	ImDrawIdx* idxDst = (ImDrawIdx*)(static_cast<char*>(indexBuffer->getMappedPtr()) + indexBuffer->getOffset());
	for (int n = 0; n < imDrawData->CmdListsCount; n++) {
		const ImDrawList* cmd_list = imDrawData->CmdLists[n];
		memcpy(idxDst, cmd_list->IdxBuffer.Data, cmd_list->IdxBuffer.Size * sizeof(ImDrawIdx));
		idxDst += cmd_list->IdxBuffer.Size;
	}
	indexBuffer->unmap();

	VkViewport viewport = {};
	viewport.width = ImGui::GetIO().DisplaySize.x;
	viewport.height = ImGui::GetIO().DisplaySize.y;
	viewport.minDepth = 0.0f;
	viewport.maxDepth = 1.0f;
	vkCmdSetViewport(cmd, 0, 1, &viewport);

	glm::vec4 push = glm::vec4(2.0f / io.DisplaySize.x, 2.0f / io.DisplaySize.y, -1.0f, -1.0f);
	vkCmdPushConstants(cmd, 
		matInstance->getCurrentFrameDescriptor().getPipelineLayout(),
		static_cast<VkShaderStageFlagBits>(matInstance->getCurrentFrameDescriptor().getPushConstant(0).stageFlags),
		matInstance->getCurrentFrameDescriptor().getPushConstant(0).offset,
		matInstance->getCurrentFrameDescriptor().getPushConstant(0).size, &push);

	int32_t vertexOffset = 0;
	int32_t indexOffset = 0;

	if (imDrawData->CmdListsCount > 0) {
		vkCmdBindPipeline(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, matInstance->getPipeline());

		VkDeviceSize offsets[1] = { vertexBuffer->getOffset() };
		vkCmdBindVertexBuffers(cmd, 0, 1, &vertexBuffer->handle, offsets);
		vkCmdBindIndexBuffer(cmd, indexBuffer->handle, indexBuffer->getOffset(), VK_INDEX_TYPE_UINT16);

		for (int32_t i = 0; i < imDrawData->CmdListsCount; i++) {
			const ImDrawList* cmd_list = imDrawData->CmdLists[i];
			for (int32_t j = 0; j < cmd_list->CmdBuffer.Size; j++) {
				const ImDrawCmd* pcmd = &cmd_list->CmdBuffer[j];

				if (pcmd->TextureId) {
					DescriptorGroup* group = imageDescriptors[(size_t)textureIndices[(VkTexture*)(pcmd->TextureId)]];
					group->update();


					vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, group->getPipelineLayout(), 0, group->getDescriptorCount(), group->getHandlePtr(), 0, nullptr);
				}
				else {
					DescriptorGroup* group = &matInstance->getCurrentFrameDescriptor();
					vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, group->getPipelineLayout(), 0, group->getDescriptorCount(), group->getHandlePtr(), 0, nullptr);
				}

				/*DescriptorSetGroup* group = &material->getDescriptors();
				vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, group->getPipelineLayout(), 0, group->getSetSize(), group->getSetPtr(), 0, nullptr);*/

				VkRect2D scissorRect;
				scissorRect.offset.x = std::max((int32_t)(pcmd->ClipRect.x), 0);
				scissorRect.offset.y = std::max((int32_t)(pcmd->ClipRect.y), 0);
				scissorRect.extent.width = (uint32_t)(pcmd->ClipRect.z - pcmd->ClipRect.x);
				scissorRect.extent.height = (uint32_t)(pcmd->ClipRect.w - pcmd->ClipRect.y);
				vkCmdSetScissor(cmd, 0, 1, &scissorRect);

				vkCmdDrawIndexed(cmd, pcmd->ElemCount, 1, indexOffset, vertexOffset, 0);

				indexOffset += pcmd->ElemCount;
			}
			vertexOffset += cmd_list->VtxBuffer.Size;
		}
	}

	imageDescriptorIndex = (((frameIndex + 1) % Graphics::getPrerenderedFrameCount()) * imageDescriptors.size() / Graphics::getPrerenderedFrameCount());
	frameIndex = (frameIndex + 1) % Graphics::getPrerenderedFrameCount();

	textureIndices.clear();
}

void grail::ImGuiHelper::build(std::function<void()> buildFunc) {
	ImGuiIO& io = ImGui::GetIO();

	ImGui::NewFrame();
	ImGuizmo::BeginFrame();

	ImGuizmo::SetRect(0, 0, io.DisplaySize.x, io.DisplaySize.y);

	buildFunc();

	ImGui::EndFrame();
	ImGui::Render();
}

void grail::ImGuiHelper::updateBuffers() {
	/*for (uint32_t index = 0; index < imageDescriptorIndex; index++) {
		imageDescriptors[index]->update();
		
	}*/

	ImDrawData* imDrawData = ImGui::GetDrawData();

	if (!imDrawData) return;

	// Upload data
	vertexBuffer->map();
	ImDrawVert* vtxDst = (ImDrawVert*)(static_cast<char*>(vertexBuffer->getMappedPtr()) + vertexBuffer->getOffset());
	for (int n = 0; n < imDrawData->CmdListsCount; n++) {
		const ImDrawList* cmd_list = imDrawData->CmdLists[n];
		memcpy(vtxDst, cmd_list->VtxBuffer.Data, cmd_list->VtxBuffer.Size * sizeof(ImDrawVert));
		vtxDst += cmd_list->VtxBuffer.Size;
	}
	vertexBuffer->unmap();
	//vertexBuffer->flush();

	indexBuffer->map();
	ImDrawIdx* idxDst = (ImDrawIdx*)(static_cast<char*>(indexBuffer->getMappedPtr()) + indexBuffer->getOffset());
	for (int n = 0; n < imDrawData->CmdListsCount; n++) {
		const ImDrawList* cmd_list = imDrawData->CmdLists[n];
		memcpy(idxDst, cmd_list->IdxBuffer.Data, cmd_list->IdxBuffer.Size * sizeof(ImDrawIdx));
		idxDst += cmd_list->IdxBuffer.Size;
	}
	indexBuffer->unmap();
	//vertexBuffer->flush();
}

void grail::ImGuiHelper::handleKeyPress(int keycode, int action) {
	ImGuiIO& io = ImGui::GetIO();

	if (action == GRAIL_KEY_PRESS) {
		io.KeysDown[keycode] = true;
	}

	if (action == GRAIL_KEY_RELEASE) {
		io.KeysDown[keycode] = false;
	}

	io.KeyCtrl = io.KeysDown[grail::Keys::LEFT_CONTROL] || io.KeysDown[grail::Keys::RIGHT_CONTROL];
	io.KeyShift = io.KeysDown[grail::Keys::LEFT_SHIFT] || io.KeysDown[grail::Keys::RIGHT_SHIFT];
	io.KeyAlt = io.KeysDown[grail::Keys::LEFT_ALT] || io.KeysDown[grail::Keys::RIGHT_ALT];
}

void grail::ImGuiHelper::handleKeyTyped(unsigned int c, int mods) {
	ImGuiIO& io = ImGui::GetIO();
	if (c > 0 && c < 0x10000)
		io.AddInputCharacter((unsigned short)c);
}

grail::VkTexture* grail::ImGuiHelper::imageID(VkTexture* texture) {
	return imageID(texture, texture->getDescriptor());
}

grail::VkTexture* grail::ImGuiHelper::imageID(VkTexture* texture, VkDescriptorImageInfo& descriptor) {
	if (textureIndices.find(texture) == textureIndices.end()) {
		imageDescriptors[++imageDescriptorIndex]->getSet(0).getBinding(0).setBinding(texture, &descriptor);
		textureIndices.insert({ texture, imageDescriptorIndex });
	}

	return texture;
}

struct InputTextCallback_UserData {
	std::string* Str;
	ImGuiInputTextCallback  ChainCallback;
	void* ChainCallbackUserData;
};

static int InputTextCallback(ImGuiInputTextCallbackData* data) {
	InputTextCallback_UserData* user_data = (InputTextCallback_UserData*)data->UserData;
	if (data->EventFlag == ImGuiInputTextFlags_CallbackResize) {
		// Resize string callback
		// If for some reason we refuse the new length (BufTextLen) and/or capacity (BufSize) we need to set them back to what we want.
		std::string* str = user_data->Str;
		IM_ASSERT(data->Buf == str->c_str());
		str->resize(data->BufTextLen);
		data->Buf = (char*)str->c_str();
	}
	else if (user_data->ChainCallback) {
		// Forward to user callback, if any
		data->UserData = user_data->ChainCallbackUserData;
		return user_data->ChainCallback(data);
	}
	return 0;
}

bool ImGui::InputText(const char* label, std::string* str, ImGuiInputTextFlags flags, ImGuiInputTextCallback callback, void* user_data) {
	IM_ASSERT((flags & ImGuiInputTextFlags_CallbackResize) == 0);
	flags |= ImGuiInputTextFlags_CallbackResize;

	InputTextCallback_UserData cb_user_data;
	cb_user_data.Str = str;
	cb_user_data.ChainCallback = callback;
	cb_user_data.ChainCallbackUserData = user_data;
	return InputText(label, (char*)str->c_str(), str->capacity() + 1, flags, InputTextCallback, &cb_user_data);
}

bool ImGui::InputTextMultiline(const char* label, std::string* str, const ImVec2& size, ImGuiInputTextFlags flags, ImGuiInputTextCallback callback, void* user_data) {
	IM_ASSERT((flags & ImGuiInputTextFlags_CallbackResize) == 0);
	flags |= ImGuiInputTextFlags_CallbackResize;

	InputTextCallback_UserData cb_user_data;
	cb_user_data.Str = str;
	cb_user_data.ChainCallback = callback;
	cb_user_data.ChainCallbackUserData = user_data;
	return InputTextMultiline(label, (char*)str->c_str(), str->capacity() + 1, size, flags, InputTextCallback, &cb_user_data);
}

bool ImGui::InputTextWithHint(const char* label, const char* hint, std::string* str, ImGuiInputTextFlags flags, ImGuiInputTextCallback callback, void* user_data) {
	IM_ASSERT((flags & ImGuiInputTextFlags_CallbackResize) == 0);
	flags |= ImGuiInputTextFlags_CallbackResize;

	InputTextCallback_UserData cb_user_data;
	cb_user_data.Str = str;
	cb_user_data.ChainCallback = callback;
	cb_user_data.ChainCallbackUserData = user_data;
	return InputTextWithHint(label, hint, (char*)str->c_str(), str->capacity() + 1, flags, InputTextCallback, &cb_user_data);
}

IMGUI_API void ImGui::HelpMarker(const char * desc) {
	ImGui::TextDisabled("(?)");

	if (ImGui::IsItemHovered()) {
		ImGui::BeginTooltip();
		ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
		ImGui::TextUnformatted(desc);
		ImGui::PopTextWrapPos();
		ImGui::EndTooltip();
	}
}

#include "imgui_internal.h"
IMGUI_API bool ImGui::SceneTreeNode(const char* label) {
	ImGuiContext& g = *GImGui;
	ImGuiWindow* window = g.CurrentWindow;

	ImGuiID id = window->GetID(label);
	ImVec2 pos = window->DC.CursorPos;
	ImRect bb(pos, ImVec2(pos.x + ImGui::GetContentRegionAvail().x, pos.y + g.FontSize + g.Style.FramePadding.y * 2));
	bool opened = ImGui::TreeNodeBehaviorIsOpen(id);
	bool hovered, held;
	if (ImGui::ButtonBehavior(bb, id, &hovered, &held, true))
		window->DC.StateStorage->SetInt(id, opened ? 0 : 1);
	if (hovered || held)
		window->DrawList->AddRectFilled(bb.Min, bb.Max, GetColorU32(g.Style.Colors[(held ? ImGuiCol_HeaderActive : ImGuiCol_HeaderHovered)]));

	// Icon, text
	float button_sz = g.FontSize + g.Style.FramePadding.y * 2;
	window->DrawList->AddRectFilled(pos, ImVec2(pos.x + button_sz, pos.y + button_sz), opened ? ImColor(255, 0, 0) : ImColor(0, 255, 0));
	ImGui::RenderText(ImVec2(pos.x + button_sz + g.Style.ItemInnerSpacing.x, pos.y + g.Style.FramePadding.y), label);

	ImGui::ItemSize(bb, g.Style.FramePadding.y);
	ImGui::ItemAdd(bb, id);

	if (opened)
		ImGui::TreePush(label);
	return opened;
}

grail::ImGuiConsole::ImGuiConsole() {

}

void grail::ImGuiConsole::clear() {
	items.clear();
}

void grail::ImGuiConsole::log(LogType type, const std::string label, const std::string & message) {
	ConsoleLogItem item;
	item.type = type;
	item.label = label;
	item.message = message;

	if (type == LogType::INFO) {
		item.color = ImVec4(1.0f, 1.0f, 1.0f, 1.0f);
	}
	else if (type == LogType::DEBUG) {
		item.color = ImVec4(0.5f, 0.0f, 0.86f, 1.0f);
	}
	else if (type == LogType::WARNING) {
		item.color = ImVec4(1.0f, 0.45f, 0.0f, 1.0f);
	}
	else {
		item.color = ImVec4(1.0f, 0.0f, 0.0f, 1.0f);
	}

	items.push_back(item);
}

void grail::ImGuiConsole::draw(bool* open) {
	ImGui::SetNextWindowSize(ImVec2(520, 600), ImGuiCond_FirstUseEver);
	if (!ImGui::Begin("Console", open)) {
		ImGui::End();
		return;
	}

	if (ImGui::BeginPopupContextItem()) {
		if (ImGui::MenuItem("Close Console"))
			*open = false;
		ImGui::EndPopup();
	}

	/*if (ImGui::SmallButton("Add Dummy Text")) { log(LogType::INFO, "TEST", "Hello"); }
	if (ImGui::SmallButton("Add Dummy debug")) { log(LogType::DEBUG, "TEST", "Hello"); }
	if (ImGui::SmallButton("Add Dummy warning")) { log(LogType::WARNING, "TEST", "Hello"); }
	if (ImGui::SmallButton("Add Dummy error")) { log(LogType::ERROR, "TEST", "Hello"); }
	if (ImGui::SmallButton("Clear")) { clear(); } ImGui::SameLine();

	ImGui::Separator();*/

	if (ImGui::BeginPopup("Options")) {
		ImGui::Checkbox("Auto-scroll", &autoScroll);
		ImGui::EndPopup();
	}

	const float footer_height_to_reserve = ImGui::GetStyle().ItemSpacing.y + ImGui::GetFrameHeightWithSpacing(); // 1 separator, 1 input text
	ImGui::BeginChild("ScrollingRegion", ImVec2(0, -footer_height_to_reserve), false, ImGuiWindowFlags_HorizontalScrollbar); // Leave room for 1 separator + 1 InputText
	if (ImGui::BeginPopupContextWindow()) {
		if (ImGui::Selectable("Clear")) clear();
		ImGui::EndPopup();
	}

	ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(4, 1)); // Tighten spacing
	for (int i = 0; i < items.size(); i++) {
		ConsoleLogItem& item = items[i];

		ImGui::PushTextWrapPos(ImGui::GetWindowWidth() - 20);
		ImGui::PushStyleColor(ImGuiCol_Text, item.color);
		ImGui::TextWrapped("%s", item.message.c_str());
		ImGui::PopStyleColor();
		ImGui::PopTextWrapPos();
	}

	if (scrollToBottom || (autoScroll && ImGui::GetScrollY() >= ImGui::GetScrollMaxY()))
		ImGui::SetScrollHereY(1.0f);
	scrollToBottom = false;

	ImGui::PopStyleVar();
	ImGui::EndChild();
	ImGui::Separator();

	// Command-line
	bool reclaim_focus = false;

	if (ImGui::InputText("Input", &inputBuffer, ImGuiInputTextFlags_EnterReturnsTrue | ImGuiInputTextFlags_CallbackCompletion | ImGuiInputTextFlags_CallbackHistory)) {
		reclaim_focus = true;
	}

	// Auto-focus on window apparition
	ImGui::SetItemDefaultFocus();
	if (reclaim_focus)
		ImGui::SetKeyboardFocusHere(-1); // Auto focus previous widget

	ImGui::End();
}
