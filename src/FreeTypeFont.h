#ifndef GRAIL_FREE_TYPE_FONT_H
#define GRAIL_FREE_TYPE_FONT_H

#include "Common.h"

#include <map>
#include <ft2build.h>
#include FT_FREETYPE_H

#include "VkTexture.h"

namespace grail {
	class FreeTypeFont {
		friend class FreeTypeFontGenerator;
	public:
		struct Character {
			uint32_t width;
			uint32_t height;

			int32_t bearingX;
			int32_t bearingY;

			long advance;

			float s0;
			float s1;

			float t0;
			float t1;
		};

		VkTexture* texture;

		Character& getCharacter(char c);
	private:
		std::map<char, Character> charMap;

		FreeTypeFont(std::map<char, Character> charMap, VkTexture* texture);
		~FreeTypeFont();
	};

	class FreeTypeFontGenerator {
	public:
		FreeTypeFontGenerator(const char* fontPath);
		~FreeTypeFontGenerator();

		FreeTypeFont* generateFont(uint32_t fontSize);
	private:
		std::string fontName;

		const uint32_t padding = 2;
		const float surfacePadding = 1.0f;

		struct Glyph {
			unsigned long c;

			uint32_t x;
			uint32_t y;

			uint32_t width;
			uint32_t height;

			int32_t bearingX;
			int32_t bearingY;

			long advance;

			bool operator < (const Glyph& rhs) const { return height > rhs.height; }
		};

		FT_Face face;
		FT_Library lib;
	};
}

#endif