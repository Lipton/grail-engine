#ifndef GRAIL_UTILS_H
#define GRAIL_UTILS_H

#include "Common.h"

#include <string>

#include <regex>
#include <algorithm>
#include <cctype>
#include <locale>
#include <functional>

#define GRAIL_SID(str) grail::utils::string::hashFromString(str)
#define STRINGIFY(str) #str

namespace grail {
	//using uuid = sole::uuid;

	namespace utils {
		/*namespace guid {
			uuid getUUID4();
		}*/

		namespace hash {
			template <class T> inline void combineHash(std::size_t& s, const T& v) {
				std::hash<T> h;
				s ^= h(v) + 0x9e3779b9 + (s << 6) + (s >> 2);
			}
		}

		namespace vulkan {
			std::string formatToString(VkFormat format);
			std::string imageLayoutToString(VkImageLayout layout);
			std::string imageViewTypeToString(VkImageViewType type);
		}

		namespace string {
			namespace detail {
				struct separator : std::numpunct<char> {
					char do_thousands_sep() const { return ','; }
					std::string do_grouping() const { return "\3"; }

					static void imbue(std::ostream& os) {
						os.imbue(std::locale(os.getloc(), new separator));
					}
				};
			}

			uint32_t hashFromString(const std::string& string);
			uint32_t hashFromString(const char* string);

			std::string bytesToString(size_t bytes);

			std::string align(const std::string& input, uint32_t targetLength);

			bool equalsCaseInsensitive(const std::string& o, const std::string& t);

			bool contains(const std::string& o, const std::string& t);
			bool containsCaseInsensitive(const std::string& o, const std::string& t);

			std::vector<std::string> splitByRegex(const std::string& input, const std::string& regex);

			bool isStringNumeric(const std::string& s);

			std::string ltrim(std::string s);
			std::string rtrim(std::string s);
			std::string trim(std::string s);

			bool replace(std::string& str, const std::string& from, const std::string& to);

			void toLower(std::string& str);
			void toUpper(std::string& str);

			void replaceAll(std::string& s, const std::string& search, const std::string& replace);
		}
	}
}

#endif