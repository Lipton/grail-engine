#include "FileIO.h"

#include <fstream>
#include <assert.h>
#include <algorithm>

#if defined(OS_ANDROID)
#include <android/asset_manager.h>
#include <android_native_app_glue.h>
#include <android/input.h>

android_app* grail::FileIO::android_app = nullptr;
#else
#endif

#include <sys/stat.h>

#if defined(OS_WINDOWS)
#include "dirent.h"
const char grail::FileIO::DIRECTORY_SEPARATOR = '\\';
#else
#include <dirent.h>
const char grail::FileIO::DIRECTORY_SEPARATOR = '/';
#endif


void grail::FileIO::readBinaryFile(const std::string & path, char ** contents, uint32_t * size) {
	std::string fullPath = getAssetPath() + path;

	if (!fileExists(fullPath))
		throw exceptions::FileNotFoundException("File: " + fullPath + " does not exist");

#if defined(OS_ANDROID)
	AAsset* asset = AAssetManager_open(android_app->activity->assetManager, fullPath.c_str(), AASSET_MODE_STREAMING);
	*size = AAsset_getLength(asset);

	

	*contents = new char[*size];
	AAsset_read(asset, *contents, *size);
	AAsset_close(asset);
#else
	std::ifstream is(fullPath, std::ios::binary | std::ios::in | std::ios::ate);

	if (is.is_open()) {
		*size = is.tellg();
		is.seekg(0, std::ios::beg);

		*contents = new char[*size];
		is.read(*contents, *size);
		is.close();
	}
#endif
}

void grail::FileIO::readBinaryFile(const std::string & path, unsigned char ** contents, uint32_t * size) {
	std::string fullPath = getAssetPath() + path;

	if (!fileExists(fullPath))
		throw exceptions::FileNotFoundException("File: " + fullPath + " does not exist");

#if defined(OS_ANDROID)
	AAsset* asset = AAssetManager_open(android_app->activity->assetManager, fullPath.c_str(), AASSET_MODE_STREAMING);
	*size = AAsset_getLength(asset);

	*contents = new unsigned char[*size];
	AAsset_read(asset, (char*)*contents, *size);
	AAsset_close(asset);
#else
	std::ifstream is(fullPath, std::ios::binary | std::ios::in | std::ios::ate);

	if (is.is_open()) {
		*size = is.tellg();
		is.seekg(0, std::ios::beg);

		*contents = new unsigned char[*size];
		is.read((char*)*contents, *size);
		is.close();
	}
#endif
}

void grail::FileIO::readBinaryFileExternal(const std::string& path, unsigned char** contents, uint32_t* size) {
	if (!fileExists(path))
		throw exceptions::FileNotFoundException("File: " + path + " does not exist");

#if defined(OS_ANDROID)
	AAsset* asset = AAssetManager_open(android_app->activity->assetManager, path.c_str(), AASSET_MODE_STREAMING);
	*size = AAsset_getLength(asset);

	*contents = new unsigned char[*size];
	AAsset_read(asset, (char*)*contents, *size);
	AAsset_close(asset);
#else
	std::ifstream is(path, std::ios::binary | std::ios::in | std::ios::ate);

	if (is.is_open()) {
		*size = is.tellg();
		is.seekg(0, std::ios::beg);

		*contents = new unsigned char[*size];
		is.read((char*)*contents, *size);
		is.close();
	}
#endif
}

void grail::FileIO::readBinaryFileExternal(const std::string& path, char** contents, uint32_t* size) {
	if (!fileExists(path))
		throw exceptions::FileNotFoundException("File: " + path + " does not exist");

#if defined(OS_ANDROID)
	AAsset* asset = AAssetManager_open(android_app->activity->assetManager, path.c_str(), AASSET_MODE_STREAMING);
	*size = AAsset_getLength(asset);

	*contents = new char[*size];
	AAsset_read(asset, (char*)*contents, *size);
	AAsset_close(asset);
#else
	std::ifstream is(path, std::ios::binary | std::ios::in | std::ios::ate);

	if (is.is_open()) {
		*size = is.tellg();
		is.seekg(0, std::ios::beg);

		*contents = new char[*size];
		is.read((char*)*contents, *size);
		is.close();
	}
#endif
}

bool grail::FileIO::fileExists(const std::string & filename) {
#if defined(OS_ANDROID)
	AAsset* asset = AAssetManager_open(android_app->activity->assetManager, filename.c_str(), AASSET_MODE_UNKNOWN);

	if (asset != nullptr) {
		AAsset_close(asset);
		return true;
	}

	return false;
#else
	struct stat buffer;

	return (stat(filename.c_str(), &buffer) == 0);
#endif
}

std::string grail::FileIO::getExtension(const std::string & path) {
	std::string::size_type idx;
	std::string extension = " ";

	idx = path.rfind('.');

	if (idx != std::string::npos) {
		extension = path.substr(idx + 1);
	}

	std::transform(extension.begin(), extension.end(), extension.begin(), tolower);

	return extension;
}

std::string grail::FileIO::getFilename(const std::string& path) {
	std::string::size_type idx;
	std::string filename = path;

	idx = path.rfind('/');

	if (idx != std::string::npos) {
		filename = path.substr(idx + 1);
	}

	idx = filename.rfind('.');

	if (idx != std::string::npos) {
		filename = filename.substr(0, idx);
	}

	std::transform(filename.begin(), filename.end(), filename.begin(), tolower);

	return filename;
}

std::string grail::FileIO::getBasePath(const std::string& path) {
	std::string::size_type idx;
	std::string basePath = path;

	idx = path.rfind('/');
	if (idx != std::string::npos) {
		basePath = path.substr(0, idx + 1);
	}

	return basePath;
}

grail::PathType grail::FileIO::getPathType(const std::string& path) {
	struct stat buffer;
	int result = stat(path.c_str(), &buffer);
	
	if (result != 0) return PathType::NON_EXISTANT;
	if (buffer.st_mode & S_IFREG) return PathType::FILE;
	if (buffer.st_mode & S_IFDIR) return PathType::DIRECTORY;

	return PathType::UNKNOWN;
}

bool grail::FileIO::hasExtension(const std::string& path) {
	return path.rfind('.') != std::string::npos;
}

void grail::FileIO::getDirectoryContents(const std::string& path, std::vector<PathInfo>& contents) {
	struct dirent* entry;
	DIR* dp;

	dp = opendir(path.c_str());
	if (dp == NULL) {
		return;
	}

	std::vector<std::string> paths;

	while ((entry = readdir(dp))) {
		if (strcmp(entry->d_name, ".") != 0) {
			PathType pathType = PathType::UNKNOWN;

			if (entry->d_type & S_IFREG) pathType = PathType::FILE;
			if (entry->d_type & S_IFDIR) pathType = PathType::DIRECTORY;

			contents.push_back({ entry->d_name, pathType });
		}
	}

	closedir(dp);
}

std::string grail::FileIO::getAssetPath() {
#if defined(OS_ANDROID)
	return "";
#else
	return "assets/";
#endif
}

std::string grail::FileIO::getAssetPath(std::string path) {
	return std::string(getAssetPath() + path);
}

bool grail::FileIO::copyFile(const std::string& src, const std::string& dst) {
	if (!fileExists(src)) {
		GRAIL_LOG(ERROR, "FILE COPY") << "Source file: " << src << " was not found";
		return false;
	}

	if (fileExists(dst)) {
		return false;
	}

	std::ifstream  srcFile(src, std::ios::binary);
	std::ofstream  dstFile(dst, std::ios::binary);

	dstFile << srcFile.rdbuf();

	return true;
}
