#include "SceneNode.h"
#include "Logger.h"

#include <stack>

#include <glm/gtx/quaternion.hpp>
#include "Scene.h"

#include "SceneNodeComponents.h"
#include "Mesh.h"

unsigned int grail::SceneNode::IDCounter = 0;

grail::SceneNode::SceneNode() {
	this->ID = SceneNode::IDCounter++;
	this->name = "Node" + std::to_string(ID);
}

grail::SceneNode::SceneNode(const std::string & name) {
	this->ID = SceneNode::IDCounter++;
	this->name = name;
}

grail::SceneNode::SceneNode(SceneNode * copy) {
	this->ID = SceneNode::IDCounter++;
	this->name = copy->name;

	std::stack<SceneNode*> nodeStack;
	for (unsigned int i = 0; i < copy->getChildCount(); ++i)
		nodeStack.push(copy->getChildByIndex(i));
	while (!nodeStack.empty()) {
		SceneNode* child = nodeStack.top();
		nodeStack.pop();

		SceneNode* newChild = new SceneNode();
		newChild->name = child->name;
		this->addChild(newChild);

		for (unsigned int i = 0; i < child->getChildCount(); ++i)
			nodeStack.push(child->getChildByIndex(i));
	}
}

grail::SceneNode::~SceneNode() {
	for (uint32_t i = 0; i < children.size(); ++i) {
		delete children[i];
	}
}

uint32_t grail::SceneNode::getID() {
	return ID;
}

void grail::SceneNode::addChild(SceneNode * node, bool preservePosition) {

	if (node->parent) {
		node->parent->removeChild(node->ID);
	}

	node->parent = this;
	node->transform.parent = &this->transform;

	if (preservePosition) {
		glm::vec3 position = node->transform.getWorldPosition();
		glm::quat rotation = node->transform.getWorldRotation();
		glm::vec3 scale = node->transform.getWorldScale();

		node->updateTransform(true);

		node->transform.setWorldPosition(position);
		node->transform.setWorldRotation(rotation);
		node->transform.setWorldScale(scale);
	}

	children.push_back(node);
}

void grail::SceneNode::removeChild(uint32_t id) {
	auto it = std::find(children.begin(), children.end(), getChild(id));
	if (it != children.end())
		children.erase(it);
}

void grail::SceneNode::removeChild(SceneNode * child) {
	auto it = std::find(children.begin(), children.end(), child);
	if (it != children.end())
		children.erase(it);
}

std::vector<grail::SceneNode*> grail::SceneNode::getChildren() {
	return children;
}

uint32_t grail::SceneNode::getChildCount() {
	return children.size();
}

grail::SceneNode * grail::SceneNode::getChild(uint32_t id) {
	for (uint32_t i = 0; i < children.size(); i++) {
		if (children[i]->getID() == id)
			return children[i];
	}
	return nullptr;
}

grail::SceneNode * grail::SceneNode::getChildByIndex(uint32_t index) {
	assert(index < getChildCount());
	return children[index];
}

grail::SceneNode * grail::SceneNode::getChildByName(const std::string & name) {
	for (uint32_t i = 0; i < children.size(); i++) {
		if (strcmp(name.c_str(), children[i]->name.c_str()) == 0)
			return children[i];
	}
	return nullptr;
}

grail::SceneNode * grail::SceneNode::getParent() {
	return parent;
}

void grail::SceneNode::clearChildren() {
	this->children.clear();
}

void grail::SceneNode::gatherChildren(std::vector<SceneNode*>& list) {
	list.push_back(this);

	for (grail::SceneNode* node : this->getChildren()) {
		gatherChildren(node, list);
	}
}

void grail::SceneNode::updateTransform(bool forceUpdate) {
	if (forceUpdate)
		this->transform.dirty = true;

	transform.lastFrameLocalMatrix = transform.localMatrix;
	transform.lastFrameWorldMatrix = transform.worldMatrix;

	if (transform.dirty) {
		glm::mat4 t = glm::mat4(1.0f);
		glm::mat4 r = glm::mat4(1.0f);
		glm::mat4 s = glm::mat4(1.0f);

		t = glm::translate(t, transform.localPosition);
		r = glm::toMat4(transform.localRotation);
		s = glm::scale(s, transform.localScale);

		transform.localMatrix = t * r * s;

		if (parent) {
			transform.parent = &parent->transform;
			transform.worldMatrix = parent->transform.worldMatrix * transform.localMatrix;
			transform.inverseParentWorld = glm::inverse(parent->transform.worldMatrix);
			transform.inverseParentRotation = glm::inverse(parent->transform.worldRotation);

			transform.worldPosition = parent->transform.worldMatrix * glm::vec4(transform.localPosition, 1.0);
			transform.worldRotation = parent->transform.worldRotation * transform.localRotation;
			transform.worldScale = parent->transform.worldScale * transform.localScale;
		}
		else {
			transform.worldMatrix = transform.localMatrix;
			transform.parent = nullptr;

			transform.worldPosition = transform.localPosition;
			transform.worldRotation = transform.localRotation;
			transform.worldScale = transform.localScale;
		}
		transform.localEuler = glm::degrees(glm::eulerAngles(transform.localRotation));
		transform.worldEuler = glm::degrees(glm::eulerAngles(transform.worldRotation));
		transform.inverseWorldMatrix = glm::inverse(transform.worldMatrix);

		transform.right = glm::normalize(transform.worldRotation * glm::vec4(1, 0, 0, 0));
		transform.up = glm::normalize(transform.worldRotation * glm::vec4(0, 1, 0, 0));
		transform.forward = glm::normalize(transform.worldRotation * glm::vec4(0, 0, 1, 0));

		nodeComponents::Render* c = Scene::getComponent<nodeComponents::Render>(this);
		if (c) {
			if (c->mesh) {
				transform.boundsMinLocal = c->mesh->boundsMin;
				transform.boundsMaxLocal = c->mesh->boundsMax;

				transform.boundsMin = transform.boundsMinLocal;
				transform.boundsMax = transform.boundsMaxLocal;

				glm::vec3 min = transform.boundsMinLocal;
				glm::vec3 max = transform.boundsMaxLocal;

				std::vector<glm::vec4> points = {
					glm::vec4(min.x, min.y, min.z, 1.0f),
					glm::vec4(max.x, min.y, min.z, 1.0f),
					glm::vec4(min.x, max.y, min.z, 1.0f),
					glm::vec4(max.x, max.y, min.z, 1.0f),
					glm::vec4(min.x, min.y, max.z, 1.0f),
					glm::vec4(max.x, min.y, max.z, 1.0f),
					glm::vec4(min.x, max.y, max.z, 1.0f),
					glm::vec4(max.x, max.y, max.z, 1.0f)
				};
				min = glm::vec3(FLT_MAX);
				max = glm::vec3(-FLT_MAX);

				for (glm::vec4& point : points) {
					glm::vec4 t = transform.getLocalToWorldMatrix() * point;

					if (min.x > t.x) min.x = t.x;
					if (min.y > t.y) min.y = t.y;
					if (min.z > t.z) min.z = t.z;

					if (max.x < t.x) max.x = t.x;
					if (max.y < t.y) max.y = t.y;
					if (max.z < t.z) max.z = t.z;
				}

				transform.boundsMin = min;
				transform.boundsMax = max;
			}
		}
	}

	for (uint32_t i = 0; i < children.size(); i++) {
		if(transform.dirty) 
			children[i]->transform.dirty = true;
		children[i]->updateTransform(forceUpdate);
	}
	
	transform.dirty = false;
}

bool grail::SceneNode::descendantOf(SceneNode* node) {
	if (node == this) {
		return false;
	}

	SceneNode* current = getParent();
	while (current != nullptr) {
		if (current == node) {
			return true;
		}

		current = current->getParent();
	}

	return false;
}

void grail::SceneNode::createComponentFromMap(const std::string & componentName) {
	Scene::assignComponentByName(this, componentName);
}

grail::nodeComponents::NodeComponent * grail::SceneNode::getComponentFromMap(const std::string & componentName) {
	return Scene::getComponentByName(this, componentName);
}

void grail::SceneNode::setAsPrefab(bool prefab) {
	this->prefab = prefab;
}

bool grail::SceneNode::isPrefab() {
	return prefab;
}

grail::SceneNode* grail::SceneNode::getDerivedPrefab() {
	return derivedPrefab;
}

std::vector<std::string> grail::SceneNode::getPrefabClones() {
	return prefabClones;
}

void grail::SceneNode::syncPrefabClones() {
	if (!isPrefab()) return;
	
	for (std::string name : prefabClones) {
		SceneNode* node = Scene::getNode(name);

		if (!node) continue;
		
		for (auto& item : nodeComponents::detail::componentAssignMap) {
			nodeComponents::NodeComponent* srcComp = getComponentFromMap(item.first);
			nodeComponents::NodeComponent* dstComp = Scene::getComponentByName(node, item.first);

			if (srcComp && dstComp) {
				dstComp->copy(srcComp);
			}
		}
	}
}

grail::SceneNode* grail::SceneNode::clone() {
	return Scene::cloneNode(this);
}

void grail::SceneNode::gatherChildren(SceneNode * base, std::vector<SceneNode*> list) {
	list.push_back(base);

	for (grail::SceneNode* node : base->getChildren()) {
		gatherChildren(node, list);
	}
}

glm::vec3 grail::Transform::getLocalBoundsMin() {
	return boundsMinLocal;
}

glm::vec3 grail::Transform::getLocalBoundsMax() {
	return boundsMaxLocal;
}

void grail::Transform::setLocalPosition(glm::vec3 position) {
	this->localPosition = position;
	this->dirty = true;
}

void grail::Transform::setLocalPosition(float x, float y, float z) {
	setLocalPosition(glm::vec3(x, y, z));
}

void grail::Transform::setLocalPositionX(float x) {
	setLocalPosition(glm::vec3(x, localPosition.y, localPosition.z));
}

void grail::Transform::setLocalPositionY(float y) {
	setLocalPosition(glm::vec3(localPosition.x, y, localPosition.z));
}

void grail::Transform::setLocalPositionZ(float z) {
	setLocalPosition(glm::vec3(localPosition.x, localPosition.y, z));
}

void grail::Transform::translateLocal(glm::vec3 translation) {
	setLocalPosition(getLocalPosition() + translation);
}

void grail::Transform::translateLocal(float x, float y, float z) {
	translateLocal(glm::vec3(x, y, z));
}

void grail::Transform::translateLocalX(float x) {
	translateLocal(glm::vec3(x, 0, 0));
}

void grail::Transform::translateLocalY(float y) {
	translateLocal(glm::vec3(0, y, 0));
}

void grail::Transform::translateLocalZ(float z) {
	translateLocal(glm::vec3(0, 0, z));
}

glm::vec3 grail::Transform::getLocalPosition() {
	return localPosition;
}

void grail::Transform::setWorldPosition(glm::vec3 position) {
	this->localPosition = inverseParentWorld * glm::vec4(position, 1.0f);
	this->worldPosition = glm::inverse(inverseParentWorld) * glm::vec4(localPosition, 1.0);
	this->dirty = true;
}

void grail::Transform::setWorldPosition(float x, float y, float z) {
	setWorldPosition(glm::vec3(x, y, z));
}

void grail::Transform::setWorldPositionX(float x) {
	setWorldPosition(glm::vec3(x, worldPosition.y, worldPosition.z));
}

void grail::Transform::setWorldPositionY(float y) {
	setWorldPosition(glm::vec3(worldPosition.x, y, worldPosition.z));
}

void grail::Transform::setWorldPositionZ(float z) {
	setWorldPosition(glm::vec3(worldPosition.x, worldPosition.y, z));
}

void grail::Transform::translateWorld(glm::vec3 translation) {
	setWorldPosition(worldPosition + translation);
}

void grail::Transform::translateWorld(float x, float y, float z) {
	translateWorld(glm::vec3(x, y, z));
}

void grail::Transform::translateWorldX(float x) {
	translateWorld(glm::vec3(x, 0, 0));
}

void grail::Transform::translateWorldY(float y) {
	translateWorld(glm::vec3(0, y, 0));
}

void grail::Transform::translateWorldZ(float z) {
	translateWorld(glm::vec3(0, 0, z));
}

glm::vec3 grail::Transform::getWorldPosition() {
	return worldPosition;
}

void grail::Transform::setLocalEuler(glm::vec3 angles) {
	this->localRotation = glm::tquat(glm::radians(angles));
	this->dirty = true;
}

void grail::Transform::setLocalEuler(float x, float y, float z) {
	setLocalEuler(glm::vec3(x, y, z));
}

void grail::Transform::setLocalEulerX(float x) {
	setLocalEuler(glm::vec3(x, localEuler.y, localEuler.z));
}

void grail::Transform::setLocalEulerY(float y) {
	setLocalEuler(glm::vec3(localEuler.x, y, localEuler.z));
}

void grail::Transform::setLocalEulerZ(float z) {
	setLocalEuler(glm::vec3(localEuler.x, localEuler.y, z));
}

void grail::Transform::rotateLocalEuler(glm::vec3 rotation) {
	this->localRotation = glm::tquat(glm::radians(rotation)) * this->localRotation;
	this->dirty = true;
}

void grail::Transform::rotateLocalEuler(float x, float y, float z) {
	rotateLocalEuler(glm::vec3(x, y, z));
}

void grail::Transform::rotateLocalEulerX(float x) {
	rotateLocalEuler(glm::vec3(x, 0, 0));
}

void grail::Transform::rotateLocalEulerY(float y) {
	rotateLocalEuler(glm::vec3(0, y, 0));
}

void grail::Transform::rotateLocalEulerZ(float z) {
	rotateLocalEuler(glm::vec3(0, 0, z));
}

glm::vec3 grail::Transform::getLocalEuler() {
	return localEuler;
}

void grail::Transform::setLocalRotation(glm::quat rotation) {
	this->localRotation = rotation;
	this->dirty = true;
}

void grail::Transform::rotateLocal(glm::quat rotation) {
	this->localRotation = rotation * this->localRotation;
	this->dirty = true;
}

glm::quat grail::Transform::getLocalRotation() {
	return localRotation;
}

void grail::Transform::setWorldEuler(glm::vec3 angles) {
	this->localRotation = glm::tquat((glm::vec3(inverseParentRotation * glm::vec4(glm::radians(angles), 0.0))));
	this->worldRotation = glm::inverse(inverseParentRotation) * localRotation;
	this->dirty = true;
}

void grail::Transform::setWorldEuler(float x, float y, float z) {
	setWorldEuler(glm::vec3(x, y, z));
}

void grail::Transform::setWorldEulerX(float x) {
	setWorldEuler(glm::vec3(x, worldEuler.y, worldEuler.z));
}

void grail::Transform::setWorldEulerY(float y) {
	setWorldEuler(glm::vec3(worldEuler.x, y, worldEuler.z));
}

void grail::Transform::setWorldEulerZ(float z) {
	setWorldEuler(glm::vec3(worldEuler.x, worldEuler.y, z));
}

void grail::Transform::rotateWorldEuler(glm::vec3 rotation) {
	this->localRotation = glm::tquat((glm::vec3(inverseParentRotation * glm::vec4(glm::radians(rotation), 1.0)))) * this->localRotation;
	this->dirty = true;
}

void grail::Transform::rotateWorldEuler(float x, float y, float z) {
	rotateWorldEuler(glm::vec3(x, y, z));
}

void grail::Transform::rotateWorldEulerX(float x) {
	rotateWorldEuler(glm::vec3(x, 0, 0));
}

void grail::Transform::rotateWorldEulerY(float y) {
	rotateWorldEuler(glm::vec3(0, y, 0));
}

void grail::Transform::rotateWorldEulerZ(float z) {
	rotateWorldEuler(glm::vec3(0, 0, z));
}

glm::vec3 grail::Transform::getWorldEuler() {
	return worldEuler;
}

void grail::Transform::setWorldRotation(glm::quat rotation) {
	this->localRotation = inverseParentRotation * rotation;
	this->worldRotation = glm::inverse(inverseParentRotation) * localRotation;
	this->dirty = true;
}

void grail::Transform::rotateWorld(glm::quat& rotation) {
	this->localRotation = (inverseParentRotation * rotation) * this->localRotation;
	this->worldRotation = glm::inverse(inverseParentRotation) * localRotation;
	this->dirty = true;
}

glm::quat grail::Transform::getWorldRotation() {
	return worldRotation;
}

void grail::Transform::setLocalScale(glm::vec3 scale) {
	this->localScale = scale;
	this->dirty = true;
}

void grail::Transform::setLocalScale(float x, float y, float z) {
	setLocalScale(glm::vec3(x, y, z));
}

void grail::Transform::setLocalScaleX(float x) {
	setLocalScale(glm::vec3(x, localScale.y, localScale.z));
}

void grail::Transform::setLocalScaleY(float y) {
	setLocalScale(glm::vec3(localScale.x, y, localScale.z));
}

void grail::Transform::setLocalScaleZ(float z) {
	setLocalScale(glm::vec3(localScale.x, localScale.y, z));
}

void grail::Transform::scaleLocal(glm::vec3 scale) {
	setLocalScale(getLocalScale() * scale);
}

void grail::Transform::scaleLocal(float x, float y, float z) {
	scaleLocal(glm::vec3(x, y, z));
}

void grail::Transform::scaleLocalX(float x) {
	scaleLocal(glm::vec3(x, 0, 0));
}

void grail::Transform::scaleLocalY(float y) {
	scaleLocal(glm::vec3(0, y, 0));
}

void grail::Transform::scaleLocalZ(float z) {
	scaleLocal(glm::vec3(0, 0, z));
}

glm::vec3 grail::Transform::getLocalScale() {
	return localScale;
}

void grail::Transform::setWorldScale(glm::vec3 scale) {
	this->localScale = (scale / parent->worldScale);
	this->worldScale = scale;
	this->dirty = true;
}

void grail::Transform::setWorldScale(float x, float y, float z) {
	setWorldScale(glm::vec3(x, y, z));
}

void grail::Transform::setWorldScaleX(float x) {
	setWorldScale(glm::vec3(x, localScale.y, localScale.z));
}

void grail::Transform::setWorldScaleY(float y) {
	setWorldScale(glm::vec3(localScale.x, y, localScale.z));
}

void grail::Transform::setWorldScaleZ(float z) {
	setWorldScale(glm::vec3(localScale.x, localScale.y, z));
}

void grail::Transform::scaleWorld(glm::vec3 scale) {
	setWorldScale(getWorldScale() * scale);
}

void grail::Transform::scaleWorld(float x, float y, float z) {
	scaleWorld(glm::vec3(x, y, z));
}

void grail::Transform::scaleWorldX(float x) {
	scaleWorld(glm::vec3(x, 0, 0));
}

void grail::Transform::scaleWorldY(float y) {
	scaleWorld(glm::vec3(0, y, 0));
}

void grail::Transform::scaleWorldZ(float z) {
	scaleWorld(glm::vec3(0, 0, z));
}

glm::vec3 grail::Transform::getWorldScale() {
	return worldScale;
}

glm::mat4 grail::Transform::getLocalToWorldMatrix() {
	return worldMatrix;
}

glm::mat4 grail::Transform::getWorldToLocalMatrix() {
	return inverseWorldMatrix;
}

glm::vec3 grail::Transform::getRight() {
	return right;
}

glm::vec3 grail::Transform::getUp() {
	return up;
}

glm::vec3 grail::Transform::getForward() {
	return forward;
}

void grail::Transform::copyTo(Transform& dst) {
	dst.setWorldPosition(getWorldPosition());
	dst.setWorldRotation(getWorldRotation());
	dst.setWorldScale(getWorldScale());
}

glm::vec3 grail::Transform::getWorldBoundsMin() {
	return boundsMin;
}

glm::vec3 grail::Transform::getWorldBoundsMax() {
	return boundsMax;
}
