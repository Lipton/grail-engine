#include "SceneLoader.h"

#include <json.hpp>

#include "FileIO.h"
#include "Utils.h"
#include "Mesh.h"
#include "Resources.h"
#include "Renderer.h"
#include "Scene.h"
#include "SceneNodeComponents.h"
#include "SceneNode.h"
#include "Material.h"
#include "Script.h"

#include <glm/gtx/quaternion.hpp>

#include "ThreadPool.h"

void grail::SceneLoader::saveSceneToFile(const std::string& file) {
	nlohmann::json node;
	saveInternal(node);

	std::ofstream myfile;
	myfile.open(FileIO::getAssetPath(file));
	myfile << node.dump(4);
	myfile.close();
}

void grail::SceneLoader::saveSceneToString(std::string& string) {
	nlohmann::json node;
	saveInternal(node);

	string = node.dump();
}

void grail::SceneLoader::loadSceneFromFile(const std::string& file) { 
	char* contents;
	uint32_t size;

	grail::FileIO::readBinaryFile(file, &contents, &size);
	loadInternal(std::string(contents, size));
	delete[] contents;
}

void grail::SceneLoader::loadSceneFromMemory(const std::string& scene) {
	loadInternal(scene);
}

void grail::SceneLoader::saveInternal(nlohmann::json& node) {
	node["scene"] = nlohmann::json::object();
	node["scene"]["nodes"] = nlohmann::json::object();

	node["resources"] = nlohmann::json::object();

	//NOTE: implement proper resource reference tracking and not use components
	std::list<std::string> textures;
	std::list<std::string> meshes;
	Scene::getNodesWithComponents<nodeComponents::Render>().each([&](auto& c) {
		MaterialInstance* material = c.material;

		DescriptorGroup& group = material->getCurrentFrameDescriptor();
		for (uint32_t i = 0; i < group.getDescriptorCount(); i++) {
			for (uint32_t j = 0; j < group.getSet(i).getBindingCount(); j++) {
				DescriptorBinding& binding = group.getSet(i).getBinding(j);

				if (binding.getResourceType() == DescriptorResourceType::IMAGE) {
					VkTexture* texture = reinterpret_cast<VkTexture*>(binding.getResource());
					if (FileIO::hasExtension(texture->getIdentifier())) {
						textures.push_back(texture->getIdentifier());
					}
				}
			}
		}

		Mesh* mesh = c.mesh;
		if (mesh) {
			if (mesh->baseAsset.length() > 0) {
				meshes.push_back(mesh->baseAsset);
			}
		}
		});

	textures.unique();
	meshes.unique();

	for (std::string& tex : textures) {
		utils::string::replaceAll(tex, "\\", "/");
	}

	for (std::string& mesh : meshes) {
		utils::string::replaceAll(mesh, "\\", "/");
	}

	node["resources"]["textures"] = textures;
	node["resources"]["meshes"] = meshes;


	std::map<std::string, std::string> scripts;
	for (auto& it : Resources::scripts) {
		scripts.insert({ it.second->getName(), it.second->getScriptSource() });
	}
	node["resources"]["scripts"] = scripts;

	// END OF NOTE

	saveNode(node["scene"]["nodes"], Scene::getRootNode());
}

void grail::SceneLoader::loadInternal(const std::string& contents) {
	nlohmann::json sceneNode = nlohmann::json::parse(contents);

	std::vector<std::string> meshes = sceneNode["resources"]["meshes"].get<std::vector<std::string>>();
	for (std::string& path : meshes) {

		try {
			Resources::loadModel(path);
		}
		catch (exceptions::FileNotFoundException) {

		}
	}

	ThreadPool threadPool;
	threadPool.allocateThreads(std::thread::hardware_concurrency());
	uint32_t threadIndex = 0;
	uint32_t threads = std::thread::hardware_concurrency();

	std::vector<std::string> textures = sceneNode["resources"]["textures"].get<std::vector<std::string>>();
	for (std::string& path : textures) {

		threadPool.threads[threadIndex]->addJob([&path] {
			TextureCreateInfo info = {};
			info.samplerInfo.maxAnisotropy = 16.0f;
			info.samplerInfo.anisotropyEnable = true;

			try {
				Resources::loadTexture(path, info);
			}
			catch (exceptions::FileNotFoundException) {

			}
			});

		//threadIndex = (threadIndex + 1) % threads;
	}

	threadPool.wait();

	std::map<std::string, std::string> scripts;

	if (sceneNode["resources"].find("scripts") != sceneNode["resources"].end()) {
		scripts = sceneNode["resources"]["scripts"].get<std::map<std::string, std::string>>();
	}

	for (auto& it : scripts) {
		Resources::createScript(it.first, it.second);
	}

	loadNode(sceneNode["scene"]["nodes"], nullptr);

	// After the scene is loaded, dispose of the staging memory
	Resources::getStagingMemoryAllocator()->clearAllPages();
	Resources::getStagingMemoryAllocator()->removeEmptyPages();

	for (SceneNode* node : Scene::getAllNodes()) {
		if (node->isPrefab()) {
			for (std::string clone : node->prefabClones) {
				SceneNode* cloneNode = Scene::getNode(clone);

				if (cloneNode) {
					cloneNode->derivedPrefab = node;
				}
			}
		}
	}

	ScriptProvider::recompile();
}

void grail::SceneLoader::saveNode(nlohmann::json & jsonNode, SceneNode * node) {
	jsonNode[node->name] = nlohmann::json::object();
	nlohmann::json& jNode = jsonNode[node->name];

	jNode["name"] = node->name;
	jNode["isPrefab"] = node->isPrefab();

	if (node->isPrefab()) {
		if (node->prefabClones.size() > 0)
			jNode["prefabClones"] = node->prefabClones;
	}

	std::vector<float> transform;
	for (int i = 0; i < 3; i++) { transform.push_back(glm::value_ptr(node->transform.getLocalPosition())[i]); }
	jNode["transform"]["position"] = transform;

	transform.clear();
	for (int i = 0; i < 3; i++) { transform.push_back(glm::value_ptr(node->transform.getLocalScale())[i]); }
	jNode["transform"]["scale"] = transform;

	transform.clear();
	for (int i = 0; i < 16; i++) { transform.push_back(glm::value_ptr(glm::toMat4(node->transform.getLocalRotation()))[i]); }
	jNode["transform"]["rotation"] = transform;

	jNode["components"] = nlohmann::json::object();

	for (auto& val : nodeComponents::detail::componentRetrieveMap) {
		nodeComponents::NodeComponent* component = node->getComponentFromMap(val.first);

		if (component) {
			component->save(jNode["components"]);
		}
	}

	if (node->getChildren().size() > 0) {
		jNode["children"] = nlohmann::json::object();

		for (SceneNode* n : node->getChildren()) {
			saveNode(jNode["children"], n);
		}
	}
}

void grail::SceneLoader::loadNode(nlohmann::json& jsonNode, SceneNode* rootNode) {
	for (auto& val : nlohmann::json::iterator_wrapper(jsonNode)) {
		nlohmann::json& jNode = val.value();

		std::string name = jNode["name"];

		SceneNode* node = (rootNode == nullptr ? Scene::getRootNode() : nullptr);
		if (node == Scene::getRootNode()) Scene::renameNode(node, name);
		if (!node) {
			if (!Scene::getNode(name))
				node = Scene::createNode(name);
		}
 
		if (!node) return;

		if (rootNode && rootNode != Scene::getRootNode()) {
			rootNode->addChild(node);
		}
		if (jNode.find("isPrefab") != jNode.end()) {
			node->prefab = jNode["isPrefab"];
			if (node->isPrefab() && jNode.find("prefabClones") != jNode.end()) {
				node->prefabClones = jNode["prefabClones"].get<std::vector<std::string>>();
			}
		}

		std::vector<float> transform;

		transform = jNode["transform"]["position"].get<std::vector<float>>();
		node->transform.setLocalPosition(transform[0], transform[1], transform[2]);
		//node->transform.position = glm::vec3(transform[0], transform[1], transform[2]);

		transform = jNode["transform"]["rotation"].get<std::vector<float>>();
		node->transform.setLocalRotation(glm::quat(glm::make_mat4(transform.data())));
		//node->transform.rotation = glm::quat(glm::make_mat4(transform.data()));

		transform = jNode["transform"]["scale"].get<std::vector<float>>();
		node->transform.setLocalScale(transform[0], transform[1], transform[2]);
		//node->transform.scale = glm::vec3(transform[0], transform[1], transform[2]);

		if (jNode.find("components") != jNode.end()) {
			for (nlohmann::json& componentNode : jNode["components"]) {
				std::string componentName = componentNode["name"];

				node->createComponentFromMap(componentName);

				nodeComponents::NodeComponent* component = node->getComponentFromMap(componentName);
				component->node = node;
				component->load(componentNode);
			}
		}

		node->updateTransform();

		if (jNode.find("children") != jNode.end()) {
			loadNode(jNode["children"], node);
		}
	}
}
