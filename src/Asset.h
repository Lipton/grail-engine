#ifndef GRAIL_ASSET_H
#define GRAIL_ASSET_H

#include "Common.h"
#include "UUID.h"
#include "FileIO.h"

#include <json.hpp>

#include "VkTexture.h"

namespace grail {
	class VkTexture;
	class Mesh;
	class Buffer;

	namespace assets {
		extern std::vector<std::string> textureFormats;
		extern std::vector<std::string> meshFormats;
		extern std::vector<std::string> scriptFormats;
	}

	class AssetDescriptor {
	public:
		// For versioning
		uint32_t version = 1;

		// full path to the asset
		std::string assetPath;

		// extension of the asset, keep in mind if i ever move to custom formats
		std::string assetExtension;

		// directory where the asset is located
		std::string assetDirectory;

		// the uuid of the asset the descriptor belongs to
		uuid uuid;
	protected:
		virtual void write(nlohmann::json& node) = 0;
		virtual void read(nlohmann::json& node) = 0;
	};

	class Asset {
		friend class AssetImporter;
		friend class Resources;

	public:
		const std::string& getAssetName() const;
		const std::string& getAssetUUIDString() const;
		AssetDescriptor* getDescriptor() const;

		uuid getAssetUUID() const;
	protected:
		std::string name;
		std::string path;
		std::string directory;
		std::string extension;

		uuid id;
		std::string idString;

		AssetDescriptor* descriptor = nullptr;

		virtual void dispose() = 0;
	};

	class TextureAsset : public Asset {
		friend class Resources;
	public:
		VkTexture& getHandle() { return *handle; }
	protected:
		void dispose() override { 
			if (handle) {
				handle->dispose();
			}
		}
	private:
		VkTexture* handle = nullptr;
	};

	class TextureAssetDescriptor : public AssetDescriptor {
		friend class TextureAssetImporter;
	public:
		SamplerFilter minFilter = SamplerFilter::LINEAR;
		SamplerFilter magFilter = SamplerFilter::LINEAR;

		SamplerMipmapMode mipmapMode = SamplerMipmapMode::LINEAR;

		SamplerAddressMode addressModeU = SamplerAddressMode::REPEAT;
		SamplerAddressMode addressModeV = SamplerAddressMode::REPEAT;
		SamplerAddressMode addressModeW = SamplerAddressMode::REPEAT;

		bool generateMipmaps = false;

		float maxAnisotropy = 0.0f;
	protected:
		virtual void write(nlohmann::json& node) override {
			node["version"] = version;
			node["path"] = assetPath;
			node["uuid"] = uuid.str();

			node["minFilter"] = vkUtils::samplerFilterToString(minFilter);
			node["magFilter"] = vkUtils::samplerFilterToString(magFilter);

			node["mipmapMode"] = vkUtils::samplerMipmapModeToString(mipmapMode);

			node["addressModeU"] = vkUtils::samplerAddressModeToString(addressModeU);
			node["addressModeV"] = vkUtils::samplerAddressModeToString(addressModeV);
			node["addressModeW"] = vkUtils::samplerAddressModeToString(addressModeW);

			node["generateMipmaps"] = generateMipmaps;
			node["anisotropy"] = maxAnisotropy;
		};

		virtual void read(nlohmann::json& node) override {	
			version = node["version"].get<uint32_t>();
			assetPath = node["path"];

			assetExtension = FileIO::getExtension(assetPath);
			assetDirectory = FileIO::getBasePath(assetPath);
			uuid = sole::rebuild(node["uuid"]);

			minFilter = vkUtils::samplerFilterFromString(node["minFilter"]);
			magFilter = vkUtils::samplerFilterFromString(node["magFilter"]);

			mipmapMode = vkUtils::samplerMipmapModeFromString(node["mipmapMode"]);

			addressModeU = vkUtils::samplerAddressModeFromString(node["addressModeU"]);
			addressModeV = vkUtils::samplerAddressModeFromString(node["addressModeV"]);
			addressModeW = vkUtils::samplerAddressModeFromString(node["addressModeW"]);

			generateMipmaps = (node["generateMipmaps"]);
			maxAnisotropy = (node["anisotropy"]);
		};
	};

	template<typename T>
	using AssetRef = std::shared_ptr<T>;
}

#endif