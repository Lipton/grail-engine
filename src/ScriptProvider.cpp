#include "ScriptProvider.h"

#include "Common.h"

#include "SceneNode.h"
#include "Camera.h"
#include "Scene.h"

#include "Script.h"

#define AS_USE_STLNAMES 1
#define AS_USE_ACCESSORS 1
#ifdef OS_ANDROID
	#define AS_MAX_PORTABILITY
#endif
#include <angelscript.h>
#include <scriptstdstring/scriptstdstring.h>
#include <scriptbuilder/scriptbuilder.h>
#include <scriptany/scriptany.h>
#include <scripthandle/scripthandle.h>
#include <scriptarray/scriptarray.h>
#include <autowrapper/aswrappedcall.h>
#include <scripthelper/scripthelper.h>

#include "SceneNodeComponents.h"
#include "Utils.h"
#include "Material.h"

#include "httplib.h"

#include "Resources.h"

#include "FreeTypeFont.h"
#include "DeferredRenderer.h"
#include "PerspectiveCamera.h"
#include "VR.h"

grail::FreeTypeFont* grail::ScriptProvider::temp_font = nullptr;

asIScriptEngine* grail::ScriptProvider::engine = nullptr;
asIScriptModule* grail::ScriptProvider::mainModule = nullptr;
CScriptBuilder* grail::ScriptProvider::builder = nullptr;
bool grail::ScriptProvider::needsRecompilation = true;
uint64_t grail::ScriptProvider::scriptID = 0;
std::vector<grail::Script*> grail::ScriptProvider::scripts = std::vector<grail::Script*>();

grail::Script* grail::ScriptProvider::createScript(const std::string& name) {
	return createScript(name, "");
}

grail::Script* grail::ScriptProvider::createScript(const std::string& name, const std::string& source) {
	Script* script = new Script();
	script->name = name;
	script->sectionName = "Section_" + std::to_string(scriptID++);
	script->scriptSource = source;
	script->engine = ScriptProvider::engine;

	// Implement this later on
	script->globalPosition = 0;

	scripts.push_back(script);

	needsRecompilation = true;

	return script;
}

void grail::ScriptProvider::removeScript(Script* script) {
	scripts.erase(std::find(scripts.begin(), scripts.end(), script));
	needsRecompilation = true;
}

void grail::ScriptProvider::executeString(const std::string& code) {
	ExecuteString(engine, code.c_str(), engine->GetModule("main_module"));
}


void grail::ScriptProvider::recompile() {
	//GRAIL_LOG(DEBUG, "RECOMPILE");

	if (isCompiled()) return;

	//GRAIL_LOG(WARNING, "TRIGGER") << "Recompile";

	if (mainModule) engine->DiscardModule("main_module");
	builder->StartNewModule(engine, "main_module");
	
	std::string source = STRINGIFY(
		namespace Keys {
			namespace Mods {
				const int SHIFT = 0x0001;
				const int CONTROL = 0x0002;
				const int ALT = 0x0004;
				const int SUPER = 0x0008;
			}

			const int UNKNOWN = -1;
			const int SPACE = 32;
			const int APOSTROPHE = 39;
			const int COMMA = 44;
			const int MINUS = 45;
			const int PERIOD = 46;
			const int SLASH = 47;
			const int NUM_0 = 48;
			const int NUM_1 = 49;
			const int NUM_2 = 50;
			const int NUM_3 = 51;
			const int NUM_4 = 52;
			const int NUM_5 = 53;
			const int NUM_6 = 54;
			const int NUM_7 = 55;
			const int NUM_8 = 56;
			const int NUM_9 = 57;
			const int SEMICOLON = 59;
			const int EQUAL = 61;
			const int A = 65;
			const int B = 66;
			const int C = 67;
			const int D = 68;
			const int E = 69;
			const int F = 70;
			const int G = 71;
			const int H = 72;
			const int I = 73;
			const int J = 74;
			const int K = 75;
			const int L = 76;
			const int M = 77;
			const int N = 78;
			const int O = 79;
			const int P = 80;
			const int Q = 81;
			const int R = 82;
			const int S = 83;
			const int T = 84;
			const int U = 85;
			const int V = 86;
			const int W = 87;
			const int X = 88;
			const int Y = 89;
			const int Z = 90;
			const int LEFT_BRACKET = 91;
			const int BACKSLASH = 92;
			const int RIGHT_BRACKET = 93;
			const int GRAVE_ACCENT = 96;
			const int WORLD_1 = 161;
			const int WORLD_2 = 162;
			const int ESCAPE = 256;
			const int ENTER = 257;
			const int TAB = 258;
			const int BACKSPACE = 259;
			const int INSERT = 260;
			const int DELETE = 261;
			const int RIGHT = 262;
			const int LEFT = 263;
			const int DOWN = 264;
			const int UP = 265;
			const int PAGE_UP = 266;
			const int PAGE_DOWN = 267;
			const int HOME = 268;
			const int END = 269;
			const int CAPS_LOCK = 280;
			const int SCROLL_LOCK = 281;
			const int NUM_LOCK = 282;
			const int PRINT_SCREEN = 283;
			const int PAUSE = 284;
			const int F1 = 290;
			const int F2 = 291;
			const int F3 = 292;
			const int F4 = 293;
			const int F5 = 294;
			const int F6 = 295;
			const int F7 = 296;
			const int F8 = 297;
			const int F9 = 298;
			const int F10 = 299;
			const int F11 = 300;
			const int F12 = 301;
			const int F13 = 302;
			const int F14 = 303;
			const int F15 = 304;
			const int F16 = 305;
			const int F17 = 306;
			const int F18 = 307;
			const int F19 = 308;
			const int F20 = 309;
			const int F21 = 310;
			const int F22 = 311;
			const int F23 = 312;
			const int F24 = 313;
			const int F25 = 314;
			const int KEYPAD_0 = 320;
			const int KEYPAD_1 = 321;
			const int KEYPAD_2 = 322;
			const int KEYPAD_3 = 323;
			const int KEYPAD_4 = 324;
			const int KEYPAD_5 = 325;
			const int KEYPAD_6 = 326;
			const int KEYPAD_7 = 327;
			const int KEYPAD_8 = 328;
			const int KEYPAD_9 = 329;
			const int KEYPAD_DECIMAL = 330;
			const int KEYPAD_DIVIDE = 331;
			const int KEYPAD_MULTIPLY = 322;
			const int KEYPAD_SUBTRACT = 333;
			const int KEYPAD_ADD = 334;
			const int KEYPAD_ENTER = 335;
			const int KEYPAD_EQUAL = 336;
			const int LEFT_SHIFT = 340;
			const int LEFT_CONTROL = 341;
			const int LEFT_ALT = 342;
			const int LEFT_SUPER = 343;
			const int RIGHT_SHIFT = 344;
			const int RIGHT_CONTROL = 345;
			const int RIGHT_ALT = 346;
			const int RIGHT_SUPER = 347;
			const int MENU = 348;

			const int LAST = MENU;
		}
		
		namespace Buttons {
			const int BUTTON_1 = 0;
			const int BUTTON_2 = 1;
			const int BUTTON_3 = 2;
			const int BUTTON_4 = 3;
			const int BUTTON_5 = 4;
			const int BUTTON_6 = 5;
			const int BUTTON_7 = 6;
			const int BUTTON_8 = 7;
			const int LEFT = BUTTON_1;
			const int RIGHT = BUTTON_2;
			const int MIDDLE = BUTTON_3;

			const int LAST = BUTTON_8;
		}

		class NodeBehaviour {
			protected Node@ node;
			
			Component@ getComponent(const string& in name) {
				return Scene::getComponent(node, name);
			}
			Component@ createComponent(const string& in name) {
				return Scene::createComponent(node, name);
			}

			Transform@ getTransform() {
				return @node.transform;
			}
		}
	);
	builder->AddSectionFromMemory("grail_shared_section_internal", source.c_str(), source.size());
	
	for (Script* s : scripts) {
		engine->SetUserData(s);
		builder->AddSectionFromMemory(
			s->sectionName.c_str(),
			s->scriptSource.c_str(),
			s->scriptSource.size()
		);
	}

	engine->SetUserData(nullptr);

	if (builder->BuildModule() >= 0) {
		mainModule = engine->GetModule("main_module");
		for (Script* s : scripts) {
			s->isScriptCompiled = true;
			s->scriptModule = mainModule;
		}

		Scene::getNodesWithComponents<nodeComponents::ScriptComponent>().each([](auto& c) {
			c.instanciated = false;
			c.instanciate();
		});

		needsRecompilation = false;
	}
	else {
		GRAIL_LOG(ERROR, "SCRIPT PROVIDER") << "Error compiling scripts";
	}
}

bool grail::ScriptProvider::isCompiled() {
	for (Script* s : scripts) {
		if (!s->isCompiled()) return false;
	}

	return true;
}

void grail::ScriptProvider::callback(const asSMessageInfo* msg, void* param) {
	LogType type = LogType::ERROR;
	if (msg->type == asMSGTYPE_WARNING)
		type = LogType::WARNING;
	else if (msg->type == asMSGTYPE_INFORMATION)
		type = LogType::INFO;

	void* data = engine->GetUserData();
	Script* script = reinterpret_cast<Script*>(data);

	if (type == LogType::ERROR) { needsRecompilation = true; }
	Log(type, "SCRIPT ENGINE") << (script ? script->getName() : msg->section) << " " << msg->row << " " << msg->col << " " << msg->message;
}

grail::nodeComponents::NodeComponent* getComponentFor(const std::string& name, grail::SceneNode* node) {
	return node->getComponentFromMap(name);
}

template<class C, typename ...Args> void __constructor__(C* gen, Args... args) {
	new (gen) C(Args(args)...);
}

std::string HTTPGet(const std::string& url, const std::string& path) {
	httplib::Client cli(url.c_str(), 80);
	httplib::Params params;
	auto res = cli.Post(path.c_str(), params);

	if (res) {
		return res->body.c_str();
	}
	else {
		return "";
	}
}

#include <random>

int boundedRandom(int low, int high) {
	return rand() % (high - low) + high;
}

int randomNum() {
	return rand();
}

int randomTo(int to) {
	return rand() % to;
}

void grail::ScriptProvider::initialize() {
	srand(time(nullptr));

	asDWORD asObjFirst = asCALL_CDECL_OBJFIRST;
	asDWORD asObjLast = asCALL_CDECL_OBJLAST;
	asDWORD asThiscall = asCALL_THISCALL;
	asDWORD asCdecl = asCALL_CDECL;

#if defined (AS_MAX_PORTABILITY)
	asObjFirst = asCALL_GENERIC;
	asObjLast = asCALL_GENERIC;
	asThiscall = asCALL_GENERIC;
	asCdecl = asCALL_GENERIC;

#define ARGS

#define WRAP_FUNCTION WRAP_FN
#define WRAP_FUNCTION_PR WRAP_FN_PR

#define WRAP_METHOD WRAP_MFN
#define WRAP_METHOD_PR WRAP_MFN_PR

#define WRAP_CONSTRUCTOR WRAP_CON

#define WRAP_METHOD_OBJ_FIRST WRAP_OBJ_FIRST
#define WRAP_METHOD_OBJ_FIRST_PR WRAP_OBJ_FIRST_PR

#define WRAP_METHOD_OBJ_LAST WRAP_OBJ_LAST
#define WRAP_METHOD_OBJ_LAST_PR WRAP_OBJ_LAST_PR
#else
#define ARGS(...) __VA_ARGS__

#define WRAP_FUNCTION asFUNCTION
#define WRAP_FUNCTION_PR asFUNCTIONPR

#define WRAP_METHOD asMETHOD
#define WRAP_METHOD_PR asMETHODPR

#define WRAP_CONSTRUCTOR(Type, Params) asFUNCTIONPR((__constructor__<Type, Params>), (Type*, Params), void)

#define WRAP_METHOD_OBJ_FIRST asFUNCTION
#define WRAP_METHOD_OBJ_FIRST_PR asFUNCTIONPR

#define WRAP_METHOD_OBJ_LAST asFUNCTION
#define WRAP_METHOD_OBJ_LAST_PR asFUNCTIONPR
#endif

	engine = asCreateScriptEngine();

	engine->SetEngineProperty(asEP_DISALLOW_VALUE_ASSIGN_FOR_REF_TYPE, true);
	engine->SetEngineProperty(asEP_REQUIRE_ENUM_SCOPE, true);
	engine->SetEngineProperty(asEP_ALLOW_UNSAFE_REFERENCES, true);
	engine->SetEngineProperty(asEP_COPY_SCRIPT_SECTIONS, false);
	engine->SetEngineProperty(asEP_USE_CHARACTER_LITERALS, true);

	int r = engine->SetMessageCallback(asFUNCTION(callback), 0, asCALL_CDECL);

	const char* defaultNamespace = engine->GetDefaultNamespace();

	RegisterStdString(engine);
	RegisterScriptAny(engine);
	RegisterScriptHandle(engine);
	RegisterScriptArray(engine, true);
	RegisterStdStringUtils(engine);

	// Base Namespace
	builder = new CScriptBuilder();

	// VEC2
	engine->RegisterObjectType("Vec2", sizeof(glm::vec2), asOBJ_VALUE | asOBJ_POD | asGetTypeTraits<glm::vec2>() | asOBJ_APP_CLASS_ALLFLOATS);
	engine->RegisterObjectBehaviour("Vec2", asBEHAVE_CONSTRUCT, "void f(float, float)", WRAP_CONSTRUCTOR(glm::vec2, ARGS(float, float)), asObjFirst);
	engine->RegisterObjectBehaviour("Vec2", asBEHAVE_CONSTRUCT, "void f(float)", WRAP_CONSTRUCTOR(glm::vec2, ARGS(float)), asObjFirst);
	engine->RegisterObjectProperty("Vec2", "float x", asOFFSET(glm::vec2, glm::vec2::x));
	engine->RegisterObjectProperty("Vec2", "float y", asOFFSET(glm::vec2, glm::vec2::y));

	engine->RegisterObjectMethod("Vec2", "Vec2& opAddAssign(const Vec2 &in) const", WRAP_METHOD_PR(glm::vec2, operator+=, (const glm::vec2&), glm::vec2&), asThiscall);
	engine->RegisterObjectMethod("Vec2", "Vec2& opSubAssign(const Vec2 &in) const", WRAP_METHOD_PR(glm::vec2, operator-=, (const glm::vec2&), glm::vec2&), asThiscall);
	engine->RegisterObjectMethod("Vec2", "Vec2& opMulAssign(const Vec2 &in) const", WRAP_METHOD_PR(glm::vec2, operator*=, (const glm::vec2&), glm::vec2&), asThiscall);
	engine->RegisterObjectMethod("Vec2", "Vec2& opDivAssign(const Vec2 &in) const", WRAP_METHOD_PR(glm::vec2, operator/=, (const glm::vec2&), glm::vec2&), asThiscall);
	
	engine->RegisterObjectMethod("Vec2", "Vec2& opAddAssign(float) const", WRAP_METHOD_PR(glm::vec2, operator+=, (float), glm::vec2&), asThiscall);
	engine->RegisterObjectMethod("Vec2", "Vec2& opSubAssign(float) const", WRAP_METHOD_PR(glm::vec2, operator-=, (float), glm::vec2&), asThiscall);
	engine->RegisterObjectMethod("Vec2", "Vec2& opMulAssign(float) const", WRAP_METHOD_PR(glm::vec2, operator*=, (float), glm::vec2&), asThiscall);
	engine->RegisterObjectMethod("Vec2", "Vec2& opDivAssign(float) const", WRAP_METHOD_PR(glm::vec2, operator/=, (float), glm::vec2&), asThiscall);

	engine->RegisterObjectMethod("Vec2", "Vec2 opAdd_r(const Vec2 &in) const", WRAP_METHOD_OBJ_LAST_PR(glm::operator+, (const glm::vec2&, const glm::vec2&), glm::vec2), asObjLast);
	engine->RegisterObjectMethod("Vec2", "Vec2 opSub_r(const Vec2 &in) const", WRAP_METHOD_OBJ_LAST_PR(glm::operator-, (const glm::vec2&, const glm::vec2&), glm::vec2), asObjLast);
	engine->RegisterObjectMethod("Vec2", "Vec2 opMul_r(const Vec2 &in) const", WRAP_METHOD_OBJ_LAST_PR(glm::operator*, (const glm::vec2&, const glm::vec2&), glm::vec2), asObjLast);
	engine->RegisterObjectMethod("Vec2", "Vec2 opDiv_r(const Vec2 &in) const", WRAP_METHOD_OBJ_LAST_PR(glm::operator/, (const glm::vec2&, const glm::vec2&), glm::vec2), asObjLast);
	engine->RegisterObjectMethod("Vec2", "Vec2 opAdd(const Vec2 &in) const", WRAP_METHOD_OBJ_FIRST_PR(glm::operator+, (const glm::vec2&, const glm::vec2&), glm::vec2), asObjFirst);
	engine->RegisterObjectMethod("Vec2", "Vec2 opSub(const Vec2 &in) const", WRAP_METHOD_OBJ_FIRST_PR(glm::operator-, (const glm::vec2&, const glm::vec2&), glm::vec2), asObjFirst);
	engine->RegisterObjectMethod("Vec2", "Vec2 opMul(const Vec2 &in) const", WRAP_METHOD_OBJ_FIRST_PR(glm::operator*, (const glm::vec2&, const glm::vec2&), glm::vec2), asObjFirst);
	engine->RegisterObjectMethod("Vec2", "Vec2 opDiv(const Vec2 &in) const", WRAP_METHOD_OBJ_FIRST_PR(glm::operator/, (const glm::vec2&, const glm::vec2&), glm::vec2), asObjFirst);

	engine->RegisterObjectMethod("Vec2", "Vec2 opAdd_r(float) const", WRAP_METHOD_OBJ_LAST_PR(glm::operator+, (float, const glm::vec2&), glm::vec2), asObjLast);
	engine->RegisterObjectMethod("Vec2", "Vec2 opSub_r(float) const", WRAP_METHOD_OBJ_LAST_PR(glm::operator-, (float, const glm::vec2&), glm::vec2), asObjLast);
	engine->RegisterObjectMethod("Vec2", "Vec2 opMul_r(float) const", WRAP_METHOD_OBJ_LAST_PR(glm::operator*, (float, const glm::vec2&), glm::vec2), asObjLast);
	engine->RegisterObjectMethod("Vec2", "Vec2 opDiv_r(float) const", WRAP_METHOD_OBJ_LAST_PR(glm::operator/, (float, const glm::vec2&), glm::vec2), asObjLast);
	engine->RegisterObjectMethod("Vec2", "Vec2 opAdd(float) const", WRAP_METHOD_OBJ_FIRST_PR(glm::operator+, (const glm::vec2&, float), glm::vec2), asObjFirst);
	engine->RegisterObjectMethod("Vec2", "Vec2 opSub(float) const", WRAP_METHOD_OBJ_FIRST_PR(glm::operator-, (const glm::vec2&, float), glm::vec2), asObjFirst);
	engine->RegisterObjectMethod("Vec2", "Vec2 opMul(float) const", WRAP_METHOD_OBJ_FIRST_PR(glm::operator*, (const glm::vec2&, float), glm::vec2), asObjFirst);
	engine->RegisterObjectMethod("Vec2", "Vec2 opDiv(float) const", WRAP_METHOD_OBJ_FIRST_PR(glm::operator/, (const glm::vec2&, float), glm::vec2), asObjFirst);

	engine->RegisterObjectType("Vec3", sizeof(glm::vec3), asOBJ_VALUE | asOBJ_POD | asGetTypeTraits<glm::vec3>() | asOBJ_APP_CLASS_ALLFLOATS);
	engine->RegisterObjectBehaviour("Vec3", asBEHAVE_CONSTRUCT, "void f(float, float, float)", WRAP_CONSTRUCTOR(glm::vec3, ARGS(float, float, float)), asObjFirst);
	engine->RegisterObjectBehaviour("Vec3", asBEHAVE_CONSTRUCT, "void f(float)", WRAP_CONSTRUCTOR(glm::vec3, ARGS(float)), asObjFirst);
	engine->RegisterObjectProperty("Vec3", "float x", asOFFSET(glm::vec3, glm::vec3::x));
	engine->RegisterObjectProperty("Vec3", "float y", asOFFSET(glm::vec3, glm::vec3::y));
	engine->RegisterObjectProperty("Vec3", "float z", asOFFSET(glm::vec3, glm::vec3::z));
	engine->RegisterObjectMethod("Vec3", "Vec3& opAddAssign(const Vec3 &in) const", WRAP_METHOD_PR(glm::vec3, operator+=, (const glm::vec3&), glm::vec3&), asThiscall);
	engine->RegisterObjectMethod("Vec3", "Vec3& opSubAssign(const Vec3 &in) const", WRAP_METHOD_PR(glm::vec3, operator-=, (const glm::vec3&), glm::vec3&), asThiscall);
	engine->RegisterObjectMethod("Vec3", "Vec3& opMulAssign(const Vec3 &in) const", WRAP_METHOD_PR(glm::vec3, operator*=, (const glm::vec3&), glm::vec3&), asThiscall);
	engine->RegisterObjectMethod("Vec3", "Vec3& opDivAssign(const Vec3 &in) const", WRAP_METHOD_PR(glm::vec3, operator/=, (const glm::vec3&), glm::vec3&), asThiscall);

	engine->RegisterObjectMethod("Vec3", "Vec3& opAddAssign(float) const", WRAP_METHOD_PR(glm::vec3, operator+=, (float), glm::vec3&), asThiscall);
	engine->RegisterObjectMethod("Vec3", "Vec3& opSubAssign(float) const", WRAP_METHOD_PR(glm::vec3, operator-=, (float), glm::vec3&), asThiscall);
	engine->RegisterObjectMethod("Vec3", "Vec3& opMulAssign(float) const", WRAP_METHOD_PR(glm::vec3, operator*=, (float), glm::vec3&), asThiscall);
	engine->RegisterObjectMethod("Vec3", "Vec3& opDivAssign(float) const", WRAP_METHOD_PR(glm::vec3, operator/=, (float), glm::vec3&), asThiscall);

	engine->RegisterObjectMethod("Vec3", "Vec3 opAdd_r(const Vec3 &in) const", WRAP_METHOD_OBJ_LAST_PR(glm::operator+, (const glm::vec3&, const glm::vec3&), glm::vec3), asObjLast);
	engine->RegisterObjectMethod("Vec3", "Vec3 opSub_r(const Vec3 &in) const", WRAP_METHOD_OBJ_LAST_PR(glm::operator-, (const glm::vec3&, const glm::vec3&), glm::vec3), asObjLast);
	engine->RegisterObjectMethod("Vec3", "Vec3 opMul_r(const Vec3 &in) const", WRAP_METHOD_OBJ_LAST_PR(glm::operator*, (const glm::vec3&, const glm::vec3&), glm::vec3), asObjLast);
	engine->RegisterObjectMethod("Vec3", "Vec3 opDiv_r(const Vec3 &in) const", WRAP_METHOD_OBJ_LAST_PR(glm::operator/, (const glm::vec3&, const glm::vec3&), glm::vec3), asObjLast);
	engine->RegisterObjectMethod("Vec3", "Vec3 opAdd(const Vec3 &in) const", WRAP_METHOD_OBJ_FIRST_PR(glm::operator+, (const glm::vec3&, const glm::vec3&), glm::vec3), asObjFirst);
	engine->RegisterObjectMethod("Vec3", "Vec3 opSub(const Vec3 &in) const", WRAP_METHOD_OBJ_FIRST_PR(glm::operator-, (const glm::vec3&, const glm::vec3&), glm::vec3), asObjFirst);
	engine->RegisterObjectMethod("Vec3", "Vec3 opMul(const Vec3 &in) const", WRAP_METHOD_OBJ_FIRST_PR(glm::operator*, (const glm::vec3&, const glm::vec3&), glm::vec3), asObjFirst);
	engine->RegisterObjectMethod("Vec3", "Vec3 opDiv(const Vec3 &in) const", WRAP_METHOD_OBJ_FIRST_PR(glm::operator/, (const glm::vec3&, const glm::vec3&), glm::vec3), asObjFirst);

	engine->RegisterObjectMethod("Vec3", "Vec3 opAdd_r(float) const", WRAP_METHOD_OBJ_LAST_PR(glm::operator+, (float, const glm::vec3&), glm::vec3), asObjLast);
	engine->RegisterObjectMethod("Vec3", "Vec3 opSub_r(float) const", WRAP_METHOD_OBJ_LAST_PR(glm::operator-, (float, const glm::vec3&), glm::vec3), asObjLast);
	engine->RegisterObjectMethod("Vec3", "Vec3 opMul_r(float) const", WRAP_METHOD_OBJ_LAST_PR(glm::operator*, (float, const glm::vec3&), glm::vec3), asObjLast);
	engine->RegisterObjectMethod("Vec3", "Vec3 opDiv_r(float) const", WRAP_METHOD_OBJ_LAST_PR(glm::operator/, (float, const glm::vec3&), glm::vec3), asObjLast);
	engine->RegisterObjectMethod("Vec3", "Vec3 opAdd(float) const", WRAP_METHOD_OBJ_FIRST_PR(glm::operator+, (const glm::vec3&, float), glm::vec3), asObjFirst);
	engine->RegisterObjectMethod("Vec3", "Vec3 opSub(float) const", WRAP_METHOD_OBJ_FIRST_PR(glm::operator-, (const glm::vec3&, float), glm::vec3), asObjFirst);
	engine->RegisterObjectMethod("Vec3", "Vec3 opMul(float) const", WRAP_METHOD_OBJ_FIRST_PR(glm::operator*, (const glm::vec3&, float), glm::vec3), asObjFirst);
	engine->RegisterObjectMethod("Vec3", "Vec3 opDiv(float) const", WRAP_METHOD_OBJ_FIRST_PR(glm::operator/, (const glm::vec3&, float), glm::vec3), asObjFirst);

	engine->RegisterObjectType("Vec4", sizeof(glm::vec4), asOBJ_VALUE | asOBJ_POD | asGetTypeTraits<glm::vec4>() | asOBJ_APP_CLASS_ALLFLOATS);
	engine->RegisterObjectBehaviour("Vec4", asBEHAVE_CONSTRUCT, "void f(float, float, float, float)", WRAP_CONSTRUCTOR(glm::vec4, ARGS(float, float, float, float)), asObjFirst);
	engine->RegisterObjectBehaviour("Vec4", asBEHAVE_CONSTRUCT, "void f(float)", WRAP_CONSTRUCTOR(glm::vec4, ARGS(float)), asObjFirst);
	engine->RegisterObjectProperty("Vec4", "float x", asOFFSET(glm::vec4, glm::vec4::x));
	engine->RegisterObjectProperty("Vec4", "float y", asOFFSET(glm::vec4, glm::vec4::y));
	engine->RegisterObjectProperty("Vec4", "float z", asOFFSET(glm::vec4, glm::vec4::z));
	engine->RegisterObjectProperty("Vec4", "float w", asOFFSET(glm::vec4, glm::vec4::w));
	engine->RegisterObjectMethod("Vec4", "Vec4& opAddAssign(const Vec4 &in) const", WRAP_METHOD_PR(glm::vec4, operator+=, (const glm::vec4&), glm::vec4&), asThiscall);
	engine->RegisterObjectMethod("Vec4", "Vec4& opSubAssign(const Vec4 &in) const", WRAP_METHOD_PR(glm::vec4, operator-=, (const glm::vec4&), glm::vec4&), asThiscall);
	engine->RegisterObjectMethod("Vec4", "Vec4& opMulAssign(const Vec4 &in) const", WRAP_METHOD_PR(glm::vec4, operator*=, (const glm::vec4&), glm::vec4&), asThiscall);
	engine->RegisterObjectMethod("Vec4", "Vec4& opDivAssign(const Vec4 &in) const", WRAP_METHOD_PR(glm::vec4, operator/=, (const glm::vec4&), glm::vec4&), asThiscall);

	engine->RegisterObjectMethod("Vec4", "Vec4& opAddAssign(float) const", WRAP_METHOD_PR(glm::vec4, operator+=, (float), glm::vec4&), asThiscall);
	engine->RegisterObjectMethod("Vec4", "Vec4& opSubAssign(float) const", WRAP_METHOD_PR(glm::vec4, operator-=, (float), glm::vec4&), asThiscall);
	engine->RegisterObjectMethod("Vec4", "Vec4& opMulAssign(float) const", WRAP_METHOD_PR(glm::vec4, operator*=, (float), glm::vec4&), asThiscall);
	engine->RegisterObjectMethod("Vec4", "Vec4& opDivAssign(float) const", WRAP_METHOD_PR(glm::vec4, operator/=, (float), glm::vec4&), asThiscall);

	engine->RegisterObjectMethod("Vec4", "Vec4 opAdd_r(const Vec4 &in) const", WRAP_METHOD_OBJ_LAST_PR(glm::operator+, (const glm::vec4&, const glm::vec4&), glm::vec4), asObjLast);
	engine->RegisterObjectMethod("Vec4", "Vec4 opSub_r(const Vec4 &in) const", WRAP_METHOD_OBJ_LAST_PR(glm::operator-, (const glm::vec4&, const glm::vec4&), glm::vec4), asObjLast);
	engine->RegisterObjectMethod("Vec4", "Vec4 opMul_r(const Vec4 &in) const", WRAP_METHOD_OBJ_LAST_PR(glm::operator*, (const glm::vec4&, const glm::vec4&), glm::vec4), asObjLast);
	engine->RegisterObjectMethod("Vec4", "Vec4 opDiv_r(const Vec4 &in) const", WRAP_METHOD_OBJ_LAST_PR(glm::operator/, (const glm::vec4&, const glm::vec4&), glm::vec4), asObjLast);
	engine->RegisterObjectMethod("Vec4", "Vec4 opAdd(const Vec4 &in) const", WRAP_METHOD_OBJ_FIRST_PR(glm::operator+, (const glm::vec4&, const glm::vec4&), glm::vec4), asObjFirst);
	engine->RegisterObjectMethod("Vec4", "Vec4 opSub(const Vec4 &in) const", WRAP_METHOD_OBJ_FIRST_PR(glm::operator-, (const glm::vec4&, const glm::vec4&), glm::vec4), asObjFirst);
	engine->RegisterObjectMethod("Vec4", "Vec4 opMul(const Vec4 &in) const", WRAP_METHOD_OBJ_FIRST_PR(glm::operator*, (const glm::vec4&, const glm::vec4&), glm::vec4), asObjFirst);
	engine->RegisterObjectMethod("Vec4", "Vec4 opDiv(const Vec4 &in) const", WRAP_METHOD_OBJ_FIRST_PR(glm::operator/, (const glm::vec4&, const glm::vec4&), glm::vec4), asObjFirst);

	engine->RegisterObjectMethod("Vec4", "Vec4 opAdd_r(float) const", WRAP_METHOD_OBJ_LAST_PR(glm::operator+, (float, const glm::vec4&), glm::vec4), asObjLast);
	engine->RegisterObjectMethod("Vec4", "Vec4 opSub_r(float) const", WRAP_METHOD_OBJ_LAST_PR(glm::operator-, (float, const glm::vec4&), glm::vec4), asObjLast);
	engine->RegisterObjectMethod("Vec4", "Vec4 opMul_r(float) const", WRAP_METHOD_OBJ_LAST_PR(glm::operator*, (float, const glm::vec4&), glm::vec4), asObjLast);
	engine->RegisterObjectMethod("Vec4", "Vec4 opDiv_r(float) const", WRAP_METHOD_OBJ_LAST_PR(glm::operator/, (float, const glm::vec4&), glm::vec4), asObjLast);
	engine->RegisterObjectMethod("Vec4", "Vec4 opAdd(float) const", WRAP_METHOD_OBJ_FIRST_PR(glm::operator+, (const glm::vec4&, float), glm::vec4), asObjFirst);
	engine->RegisterObjectMethod("Vec4", "Vec4 opSub(float) const", WRAP_METHOD_OBJ_FIRST_PR(glm::operator-, (const glm::vec4&, float), glm::vec4), asObjFirst);
	engine->RegisterObjectMethod("Vec4", "Vec4 opMul(float) const", WRAP_METHOD_OBJ_FIRST_PR(glm::operator*, (const glm::vec4&, float), glm::vec4), asObjFirst);
	engine->RegisterObjectMethod("Vec4", "Vec4 opDiv(float) const", WRAP_METHOD_OBJ_FIRST_PR(glm::operator/, (const glm::vec4&, float), glm::vec4), asObjFirst);

	engine->RegisterObjectType("Mat4", sizeof(glm::mat4), asOBJ_VALUE | asOBJ_POD | asOBJ_APP_CLASS_CDAK | asOBJ_APP_CLASS_ALLFLOATS);
	engine->RegisterObjectType("Mat3", sizeof(glm::mat3), asOBJ_VALUE | asOBJ_POD | asOBJ_APP_CLASS_CDAK | asOBJ_APP_CLASS_ALLFLOATS);
	engine->RegisterObjectType("Quat", sizeof(glm::quat), asOBJ_VALUE | asOBJ_POD | asOBJ_APP_CLASS_CDAK | asOBJ_APP_CLASS_ALLFLOATS);

	engine->RegisterObjectType("Mesh", 0, asOBJ_REF | asOBJ_NOCOUNT);
	engine->RegisterObjectType("Texture", 0, asOBJ_REF | asOBJ_NOCOUNT);
	engine->RegisterObjectType("Buffer", 0, asOBJ_REF | asOBJ_NOCOUNT);

	engine->RegisterObjectType("Camera", 0, asOBJ_REF | asOBJ_NOCOUNT);
	engine->RegisterObjectProperty("Camera", "Vec3 position", asOFFSET(grail::PerspectiveCamera, grail::PerspectiveCamera::position));
	engine->RegisterObjectProperty("Camera", "Vec3 direction", asOFFSET(grail::PerspectiveCamera, grail::PerspectiveCamera::direction));
	engine->RegisterObjectProperty("Camera", "float near", asOFFSET(grail::PerspectiveCamera, grail::PerspectiveCamera::nearPlane));
	engine->RegisterObjectProperty("Camera", "float far", asOFFSET(grail::PerspectiveCamera, grail::PerspectiveCamera::nearPlane));
	engine->RegisterObjectProperty("Camera", "float fov", asOFFSET(grail::PerspectiveCamera, grail::PerspectiveCamera::fov));
	engine->RegisterObjectMethod("Camera", "Vec3 unproject(Vec3)", WRAP_METHOD(PerspectiveCamera, PerspectiveCamera::unproject), asThiscall);
	engine->RegisterObjectMethod("Camera", "Vec3 project(Vec3)", WRAP_METHOD(PerspectiveCamera, PerspectiveCamera::project), asThiscall);
	engine->RegisterObjectMethod("Camera", "void update(bool = true)", WRAP_METHOD(PerspectiveCamera, PerspectiveCamera::update), asThiscall);

	engine->RegisterObjectType("Transform", 0, asOBJ_REF | asOBJ_NOCOUNT);
	engine->RegisterObjectMethod("Transform", "Mat4 getLocalMatrix()", WRAP_METHOD(Transform, Transform::getLocalMatrix), asThiscall);
	engine->RegisterObjectMethod("Transform", "Mat4 getWorldMatrix()", WRAP_METHOD(Transform, Transform::getWorldMatrix), asThiscall);
	engine->RegisterObjectMethod("Transform", "Mat4 getLastFrameLocalMatrix()", WRAP_METHOD(Transform, Transform::getLastFrameLocalMatrix), asThiscall);
	engine->RegisterObjectMethod("Transform", "Mat4 getLastFrameWorldMatrix()", WRAP_METHOD(Transform, Transform::getLastFrameWorldMatrix), asThiscall);
	
	engine->RegisterObjectMethod("Transform", "Vec3 getUp()", WRAP_METHOD(Transform, Transform::getUp), asThiscall);
	engine->RegisterObjectMethod("Transform", "Vec3 getForward()", WRAP_METHOD(Transform, Transform::getForward), asThiscall);
	engine->RegisterObjectMethod("Transform", "Vec3 getRight()", WRAP_METHOD(Transform, Transform::getRight), asThiscall);

	engine->RegisterObjectMethod("Transform", "Vec3 getWorldBoundsMin()", WRAP_METHOD(Transform, Transform::getWorldBoundsMin), asThiscall);
	engine->RegisterObjectMethod("Transform", "Vec3 getWorldBoundsMax()", WRAP_METHOD(Transform, Transform::getWorldBoundsMax), asThiscall);
	engine->RegisterObjectMethod("Transform", "Vec3 getLocalBoundsMin()", WRAP_METHOD(Transform, Transform::getLocalBoundsMin), asThiscall);
	engine->RegisterObjectMethod("Transform", "Vec3 getLocalBoundsMax()", WRAP_METHOD(Transform, Transform::getLocalBoundsMax), asThiscall);
	
	engine->RegisterObjectMethod("Transform", "Vec3 getLocalPosition()", WRAP_METHOD(Transform, Transform::getLocalPosition), asThiscall);
	engine->RegisterObjectMethod("Transform", "void setLocalPosition(Vec3)", WRAP_METHOD_PR(Transform, Transform::setLocalPosition, (glm::vec3), void), asThiscall);
	engine->RegisterObjectMethod("Transform", "void setLocalPosition(float, float, float)", WRAP_METHOD_PR(Transform, Transform::setLocalPosition, (float, float, float), void), asThiscall);
	engine->RegisterObjectMethod("Transform", "void setLocalPositionX(float)", WRAP_METHOD(Transform, Transform::setLocalPositionX), asThiscall);
	engine->RegisterObjectMethod("Transform", "void setLocalPositionY(float)", WRAP_METHOD(Transform, Transform::setLocalPositionY), asThiscall);
	engine->RegisterObjectMethod("Transform", "void setLocalPositionZ(float)", WRAP_METHOD(Transform, Transform::setLocalPositionZ), asThiscall);
	engine->RegisterObjectMethod("Transform", "void translateLocal(Vec3)", WRAP_METHOD_PR(Transform, Transform::translateLocal, (glm::vec3), void), asThiscall);
	engine->RegisterObjectMethod("Transform", "void translateLocal(float, float, float)", WRAP_METHOD_PR(Transform, Transform::translateLocal, (float, float, float), void), asThiscall);
	engine->RegisterObjectMethod("Transform", "void translateLocalX(float)", WRAP_METHOD(Transform, Transform::translateLocalX), asThiscall);
	engine->RegisterObjectMethod("Transform", "void translateLocalY(float)", WRAP_METHOD(Transform, Transform::translateLocalY), asThiscall);
	engine->RegisterObjectMethod("Transform", "void translateLocalZ(float)", WRAP_METHOD(Transform, Transform::translateLocalZ), asThiscall);

	engine->RegisterObjectMethod("Transform", "Vec3 getWorldPosition()", WRAP_METHOD(Transform, Transform::getWorldPosition), asThiscall);
	engine->RegisterObjectMethod("Transform", "void setWorldPosition(Vec3)", WRAP_METHOD_PR(Transform, Transform::setWorldPosition, (glm::vec3), void), asThiscall);
	engine->RegisterObjectMethod("Transform", "void setWorldPosition(float, float, float)", WRAP_METHOD_PR(Transform, Transform::setWorldPosition, (float, float, float), void), asThiscall);
	engine->RegisterObjectMethod("Transform", "void setWorldPositionX(float)", WRAP_METHOD(Transform, Transform::setWorldPositionX), asThiscall);
	engine->RegisterObjectMethod("Transform", "void setWorldPositionY(float)", WRAP_METHOD(Transform, Transform::setWorldPositionY), asThiscall);
	engine->RegisterObjectMethod("Transform", "void setWorldPositionZ(float)", WRAP_METHOD(Transform, Transform::setWorldPositionZ), asThiscall);
	engine->RegisterObjectMethod("Transform", "void translateWorld(Vec3)", WRAP_METHOD_PR(Transform, Transform::translateWorld, (glm::vec3), void), asThiscall);
	engine->RegisterObjectMethod("Transform", "void translateWorld(float, float, float)", WRAP_METHOD_PR(Transform, Transform::translateWorld, (float, float, float), void), asThiscall);
	engine->RegisterObjectMethod("Transform", "void translateWorldX(float)", WRAP_METHOD(Transform, Transform::translateWorldX), asThiscall);
	engine->RegisterObjectMethod("Transform", "void translateWorldY(float)", WRAP_METHOD(Transform, Transform::translateWorldY), asThiscall);
	engine->RegisterObjectMethod("Transform", "void translateWorldZ(float)", WRAP_METHOD(Transform, Transform::translateWorldZ), asThiscall);

	engine->RegisterObjectMethod("Transform", "Vec3 getLocalEuler()", WRAP_METHOD(Transform, Transform::getLocalEuler), asThiscall);
	engine->RegisterObjectMethod("Transform", "void setLocalEuler(Vec3)", WRAP_METHOD_PR(Transform, Transform::setLocalEuler, (glm::vec3), void), asThiscall);
	engine->RegisterObjectMethod("Transform", "void setLocalEuler(float, float, float)", WRAP_METHOD_PR(Transform, Transform::setLocalEuler, (float, float, float), void), asThiscall);
	engine->RegisterObjectMethod("Transform", "void setLocalEulerX(float)", WRAP_METHOD(Transform, Transform::setLocalEulerX), asThiscall);
	engine->RegisterObjectMethod("Transform", "void setLocalEulerY(float)", WRAP_METHOD(Transform, Transform::setLocalEulerY), asThiscall);
	engine->RegisterObjectMethod("Transform", "void setLocalEulerZ(float)", WRAP_METHOD(Transform, Transform::setLocalEulerZ), asThiscall);
	engine->RegisterObjectMethod("Transform", "void rotateLocalEuler(Vec3)", WRAP_METHOD_PR(Transform, Transform::rotateLocalEuler, (glm::vec3), void), asThiscall);
	engine->RegisterObjectMethod("Transform", "void rotateLocalEuler(float, float, float)", WRAP_METHOD_PR(Transform, Transform::rotateLocalEuler, (float, float, float), void), asThiscall);
	engine->RegisterObjectMethod("Transform", "void rotateLocalEulerX(float)", WRAP_METHOD(Transform, Transform::rotateLocalEulerX), asThiscall);
	engine->RegisterObjectMethod("Transform", "void rotateLocalEulerY(float)", WRAP_METHOD(Transform, Transform::rotateLocalEulerY), asThiscall);
	engine->RegisterObjectMethod("Transform", "void rotateLocalEulerZ(float)", WRAP_METHOD(Transform, Transform::rotateLocalEulerZ), asThiscall);

	engine->RegisterObjectMethod("Transform", "Vec3 getWorldEuler()", WRAP_METHOD(Transform, Transform::getWorldEuler), asThiscall);
	engine->RegisterObjectMethod("Transform", "void setWorldEuler(Vec3)", WRAP_METHOD_PR(Transform, Transform::setWorldEuler, (glm::vec3), void), asThiscall);
	engine->RegisterObjectMethod("Transform", "void setWorldEuler(float, float, float)", WRAP_METHOD_PR(Transform, Transform::setWorldEuler, (float, float, float), void), asThiscall);
	engine->RegisterObjectMethod("Transform", "void setWorldEulerX(float)", WRAP_METHOD(Transform, Transform::setWorldEulerX), asThiscall);
	engine->RegisterObjectMethod("Transform", "void setWorldEulerY(float)", WRAP_METHOD(Transform, Transform::setWorldEulerY), asThiscall);
	engine->RegisterObjectMethod("Transform", "void setWorldEulerZ(float)", WRAP_METHOD(Transform, Transform::setWorldEulerZ), asThiscall);
	engine->RegisterObjectMethod("Transform", "void rotateWorldEuler(Vec3)", WRAP_METHOD_PR(Transform, Transform::rotateWorldEuler, (glm::vec3), void), asThiscall);
	engine->RegisterObjectMethod("Transform", "void rotateWorldEuler(float, float, float)", WRAP_METHOD_PR(Transform, Transform::rotateWorldEuler, (float, float, float), void), asThiscall);
	engine->RegisterObjectMethod("Transform", "void rotateWorldEulerX(float)", WRAP_METHOD(Transform, Transform::rotateWorldEulerX), asThiscall);
	engine->RegisterObjectMethod("Transform", "void rotateWorldEulerY(float)", WRAP_METHOD(Transform, Transform::rotateWorldEulerY), asThiscall);
	engine->RegisterObjectMethod("Transform", "void rotateWorldEulerZ(float)", WRAP_METHOD(Transform, Transform::rotateWorldEulerZ), asThiscall);

	engine->RegisterObjectMethod("Transform", "void setLocalRotation(Quat)", WRAP_METHOD(Transform, Transform::setLocalRotation), asThiscall);
	engine->RegisterObjectMethod("Transform", "void rotateLocal(Quat)", WRAP_METHOD(Transform, Transform::rotateLocal), asThiscall);
	engine->RegisterObjectMethod("Transform", "Quat getLocalRotation()", WRAP_METHOD(Transform, Transform::getLocalRotation), asThiscall);

	engine->RegisterObjectMethod("Transform", "void setWorldRotation(Quat)", WRAP_METHOD(Transform, Transform::setWorldRotation), asThiscall);
	engine->RegisterObjectMethod("Transform", "void rotateWorld(Quat)", WRAP_METHOD(Transform, Transform::rotateWorld), asThiscall);
	engine->RegisterObjectMethod("Transform", "Quat getWorldRotation()", WRAP_METHOD(Transform, Transform::getWorldRotation), asThiscall);

	engine->RegisterObjectMethod("Transform", "Vec3 getLocalScale()", WRAP_METHOD(Transform, Transform::getLocalScale), asThiscall);
	engine->RegisterObjectMethod("Transform", "void setLocalScale(Vec3)", WRAP_METHOD_PR(Transform, Transform::setLocalScale, (glm::vec3), void), asThiscall);
	engine->RegisterObjectMethod("Transform", "void setLocalScale(float, float, float)", WRAP_METHOD_PR(Transform, Transform::setLocalScale, (float, float, float), void), asThiscall);
	engine->RegisterObjectMethod("Transform", "void setLocalScaleX(float)", WRAP_METHOD(Transform, Transform::setLocalScaleX), asThiscall);
	engine->RegisterObjectMethod("Transform", "void setLocalScaleY(float)", WRAP_METHOD(Transform, Transform::setLocalScaleY), asThiscall);
	engine->RegisterObjectMethod("Transform", "void setLocalScaleZ(float)", WRAP_METHOD(Transform, Transform::setLocalScaleZ), asThiscall);
	engine->RegisterObjectMethod("Transform", "void scaleLocal(Vec3)", WRAP_METHOD_PR(Transform, Transform::scaleLocal, (glm::vec3), void), asThiscall);
	engine->RegisterObjectMethod("Transform", "void scaleLocal(float, float, float)", WRAP_METHOD_PR(Transform, Transform::scaleLocal, (float, float, float), void), asThiscall);
	engine->RegisterObjectMethod("Transform", "void scaleLocalX(float)", WRAP_METHOD(Transform, Transform::scaleLocalX), asThiscall);
	engine->RegisterObjectMethod("Transform", "void scaleLocalY(float)", WRAP_METHOD(Transform, Transform::scaleLocalY), asThiscall);
	engine->RegisterObjectMethod("Transform", "void scaleLocalZ(float)", WRAP_METHOD(Transform, Transform::scaleLocalZ), asThiscall);

	engine->RegisterObjectMethod("Transform", "Vec3 getWorldScale()", WRAP_METHOD(Transform, Transform::getWorldScale), asThiscall);
	engine->RegisterObjectMethod("Transform", "void setWorldScale(Vec3)", WRAP_METHOD_PR(Transform, Transform::setWorldScale, (glm::vec3), void), asThiscall);
	engine->RegisterObjectMethod("Transform", "void setWorldScale(float, float, float)", WRAP_METHOD_PR(Transform, Transform::setWorldScale, (float, float, float), void), asThiscall);
	engine->RegisterObjectMethod("Transform", "void setWorldScaleX(float)", WRAP_METHOD(Transform, Transform::setWorldScaleX), asThiscall);
	engine->RegisterObjectMethod("Transform", "void setWorldScaleY(float)", WRAP_METHOD(Transform, Transform::setWorldScaleY), asThiscall);
	engine->RegisterObjectMethod("Transform", "void setWorldScaleZ(float)", WRAP_METHOD(Transform, Transform::setWorldScaleZ), asThiscall);
	engine->RegisterObjectMethod("Transform", "void scaleWorld(Vec3)", WRAP_METHOD_PR(Transform, Transform::scaleWorld, (glm::vec3), void), asThiscall);
	engine->RegisterObjectMethod("Transform", "void scaleWorld(float, float, float)", WRAP_METHOD_PR(Transform, Transform::scaleWorld, (float, float, float), void), asThiscall);
	engine->RegisterObjectMethod("Transform", "void scaleWorldX(float)", WRAP_METHOD(Transform, Transform::scaleWorldX), asThiscall);
	engine->RegisterObjectMethod("Transform", "void scaleWorldY(float)", WRAP_METHOD(Transform, Transform::scaleWorldY), asThiscall);
	engine->RegisterObjectMethod("Transform", "void scaleWorldZ(float)", WRAP_METHOD(Transform, Transform::scaleWorldZ), asThiscall);

	engine->RegisterObjectMethod("Transform", "Mat4 getLocalToWorldMatrix()", WRAP_METHOD(Transform, Transform::getLocalToWorldMatrix), asThiscall);
	engine->RegisterObjectMethod("Transform", "Mat4 getWorldToLocalMatrix()", WRAP_METHOD(Transform, Transform::getWorldToLocalMatrix), asThiscall);

	engine->RegisterObjectType("MaterialInstance", 0, asOBJ_REF | asOBJ_NOCOUNT);
	engine->RegisterObjectMethod("MaterialInstance", "void setFloat(const string &in, float)", WRAP_METHOD(MaterialInstance, MaterialInstance::setFloat), asThiscall);
	engine->RegisterObjectMethod("MaterialInstance", "void setVec2(const string &in, Vec2)", WRAP_METHOD_PR(MaterialInstance, MaterialInstance::setVec2, (const std::string&, glm::vec2), void), asThiscall);
	engine->RegisterObjectMethod("MaterialInstance", "void setVec2(const string &in, float, float)", WRAP_METHOD_PR(MaterialInstance, MaterialInstance::setVec2, (const std::string&, float, float), void), asThiscall);
	engine->RegisterObjectMethod("MaterialInstance", "void setVec3(const string &in, Vec3)", WRAP_METHOD_PR(MaterialInstance, MaterialInstance::setVec3, (const std::string&, glm::vec3), void), asThiscall);
	engine->RegisterObjectMethod("MaterialInstance", "void setVec3(const string &in, float, float, float)", WRAP_METHOD_PR(MaterialInstance, MaterialInstance::setVec3, (const std::string&, float, float, float), void), asThiscall);
	engine->RegisterObjectMethod("MaterialInstance", "void setVec4(const string &in, Vec4)", WRAP_METHOD_PR(MaterialInstance, MaterialInstance::setVec4, (const std::string&, glm::vec4), void), asThiscall);
	//engine->RegisterObjectMethod("MaterialInstance", "void setVec4(const string &in, float, float, float, float)", WRAP_METHOD_PR(MaterialInstance, MaterialInstance::setVec4, (const std::string&, float, float, float, float), void), asThiscall);
	engine->RegisterObjectMethod("MaterialInstance", "void setMat3(const string &in, Mat3)", WRAP_METHOD(MaterialInstance, MaterialInstance::setMat3), asThiscall);
	engine->RegisterObjectMethod("MaterialInstance", "void setMat4(const string &in, Mat4)", WRAP_METHOD(MaterialInstance, MaterialInstance::setMat4), asThiscall);
	engine->RegisterObjectMethod("MaterialInstance", "void setDescriptorTexture(const string &in, Texture@)", WRAP_METHOD(MaterialInstance, MaterialInstance::setDescriptorTexture), asThiscall);

	engine->RegisterObjectType("Component", 0, asOBJ_REF | asOBJ_NOCOUNT);

	engine->RegisterObjectType("Node", 0, asOBJ_REF | asOBJ_NOCOUNT);
	engine->RegisterObjectProperty("Node", "string name", asOFFSET(SceneNode, SceneNode::name));
	engine->RegisterObjectProperty("Node", "Transform transform", asOFFSET(SceneNode, SceneNode::transform));
	engine->RegisterObjectMethod("Node", "Component@ getComponent(const string &in)", WRAP_METHOD(SceneNode, SceneNode::getComponentFromMap), asThiscall);
	engine->RegisterObjectMethod("Node", "void addChild(Node@, bool = false)", WRAP_METHOD(SceneNode, SceneNode::addChild), asThiscall);
	engine->RegisterObjectMethod("Node", "Node@ getParent()", WRAP_METHOD(SceneNode, SceneNode::getParent), asThiscall);
	engine->RegisterObjectMethod("Node", "void setAsPrefab(bool &in)", WRAP_METHOD(SceneNode, SceneNode::setAsPrefab), asThiscall);
	engine->RegisterObjectMethod("Node", "bool isPrefab()", WRAP_METHOD(SceneNode, SceneNode::isPrefab), asThiscall);
	engine->RegisterObjectMethod("Node", "Node@ getDerivedPrefab()", WRAP_METHOD(SceneNode, SceneNode::getDerivedPrefab), asThiscall);
	engine->RegisterObjectMethod("Node", "void syncPrefabClones()", WRAP_METHOD(SceneNode, SceneNode::syncPrefabClones), asThiscall);
	engine->RegisterObjectMethod("Node", "Node@ clone()", WRAP_METHOD(SceneNode, SceneNode::clone), asThiscall);
	engine->RegisterObjectMethod("Node", "void updateTransform(bool = false)", WRAP_METHOD(SceneNode, SceneNode::updateTransform), asThiscall);


	engine->RegisterObjectType("RenderComponent", 0, asOBJ_REF | asOBJ_NOCOUNT);
	engine->RegisterObjectMethod("Component", "RenderComponent@ opImplCast()", WRAP_METHOD_OBJ_LAST((cast<nodeComponents::Render, nodeComponents::NodeComponent>)), asObjLast);
	{
		using namespace nodeComponents;
		engine->RegisterObjectMethod("RenderComponent", "void initDefault()", WRAP_METHOD(Render, Render::initDefault), asThiscall);
	}
	engine->RegisterObjectProperty("RenderComponent", "bool shouldRender", asOFFSET(nodeComponents::Render, nodeComponents::Render::render));
	engine->RegisterObjectProperty("RenderComponent", "Mesh@ mesh", asOFFSET(nodeComponents::Render, nodeComponents::Render::mesh));
	engine->RegisterObjectProperty("RenderComponent", "MaterialInstance@ material", asOFFSET(nodeComponents::Render, nodeComponents::Render::material));

	engine->RegisterObjectType("Script", 0, asOBJ_REF | asOBJ_NOCOUNT);
	engine->RegisterObjectMethod("Script", "void setScriptSource(const string &in)", WRAP_METHOD(Script, Script::setScriptSource), asThiscall);

	engine->RegisterObjectType("ScriptComponent", 0, asOBJ_REF | asOBJ_NOCOUNT);
	engine->RegisterObjectMethod("Component", "ScriptComponent@ opImplCast()", WRAP_METHOD_OBJ_LAST((cast<nodeComponents::ScriptComponent, nodeComponents::NodeComponent>)), asObjLast);
	
	{
		using namespace nodeComponents;
		engine->RegisterObjectMethod("ScriptComponent", "ref@ get()", WRAP_METHOD(ScriptComponent, ScriptComponent::get), asThiscall);
		engine->RegisterObjectMethod("ScriptComponent", "void setScript(Script@)", WRAP_METHOD(ScriptComponent, ScriptComponent::setScript), asThiscall);
		engine->RegisterObjectMethod("ScriptComponent", "void callStartFunc()", WRAP_METHOD(ScriptComponent, ScriptComponent::callStartFunc), asThiscall);
		engine->RegisterObjectMethod("ScriptComponent", "void callUpdateFunc(float)", WRAP_METHOD(ScriptComponent, ScriptComponent::callUpdateFunc), asThiscall);
		//engine->RegisterObjectMethod("ScriptComponent", "void setup()", WRAP_METHOD(ScriptComponent, ScriptComponent::setup), asThiscall);
	}

	engine->RegisterObjectType("RendererSettings", 0, asOBJ_REF | asOBJ_NOCOUNT);
	engine->RegisterObjectProperty("RendererSettings", "float cameraAperature", asOFFSET(grail::DeferredRenderer::RendererSettings, grail::DeferredRenderer::RendererSettings::cameraAperture));
	engine->RegisterObjectProperty("RendererSettings", "float cameraShutterSpeed", asOFFSET(grail::DeferredRenderer::RendererSettings, grail::DeferredRenderer::RendererSettings::cameraShutterSpeed));
	engine->RegisterObjectProperty("RendererSettings", "float cameraSensivity", asOFFSET(grail::DeferredRenderer::RendererSettings, grail::DeferredRenderer::RendererSettings::cameraSensitivity));

	engine->RegisterObjectProperty("RendererSettings", "Vec3 sunlightDirection", asOFFSET(grail::DeferredRenderer::RendererSettings, grail::DeferredRenderer::RendererSettings::directionalLightDirection));
	engine->RegisterObjectProperty("RendererSettings", "Vec3 sunlightColor", asOFFSET(grail::DeferredRenderer::RendererSettings, grail::DeferredRenderer::RendererSettings::directionalLightColor));
	engine->RegisterObjectProperty("RendererSettings", "float sunlightIntensity", asOFFSET(grail::DeferredRenderer::RendererSettings, grail::DeferredRenderer::RendererSettings::directionalLightIntensity));


		// LOG Namespace
	engine->SetDefaultNamespace("Log");
	engine->RegisterGlobalFunction("void info(const string &in, const string &in)", WRAP_FUNCTION(logger::info), asCdecl);
	engine->RegisterGlobalFunction("void debug(const string &in, const string &in)", WRAP_FUNCTION(logger::debug), asCdecl);
	engine->RegisterGlobalFunction("void warning(const string &in, const string &in)", WRAP_FUNCTION(logger::warning), asCdecl);
	engine->RegisterGlobalFunction("void error(const string &in, const string &in)", WRAP_FUNCTION(logger::error), asCdecl);

	// Scene Namespace
	engine->SetDefaultNamespace("Scene");
	engine->RegisterGlobalFunction("Node@ createNode(const string &in)", WRAP_FUNCTION(Scene::createNode), asCdecl);
	engine->RegisterGlobalFunction("Node@ getNode(const string &in)", WRAP_FUNCTION(Scene::getNode), asCdecl);
	engine->RegisterGlobalFunction("Node@ cloneNode(Node@)", WRAP_FUNCTION(Scene::cloneNode), asCdecl);
	engine->RegisterGlobalFunction("void removeNode(Node@)", WRAP_FUNCTION_PR(Scene::removeNode, (SceneNode*), void), asCdecl);
	engine->RegisterGlobalFunction("void removeNode(const string &in)", WRAP_FUNCTION_PR(Scene::removeNode, (const std::string&), void), asCdecl);
	engine->RegisterGlobalFunction("Node@ castRay(Vec3, Vec3, Vec3 &out)", WRAP_FUNCTION(Scene::castRay), asCdecl);
	engine->RegisterGlobalFunction("Node@ castRayOptimized(Vec3, Vec3, Vec3 &out)", WRAP_FUNCTION(Scene::castRayOptimized), asCdecl);
	engine->RegisterGlobalFunction("Component@ getComponent(Node@, const string &in)", WRAP_FUNCTION(Scene::getComponentByName), asCdecl);
	engine->RegisterGlobalFunction("Component@ createComponent(Node@, const string &in)", WRAP_FUNCTION(Scene::assignComponentByName), asCdecl);
	// Math Namespace
	engine->SetDefaultNamespace("Math");
	engine->RegisterGlobalFunction("float min(float, float)", WRAP_FUNCTION_PR(glm::min, (float, float), float), asCdecl);
	engine->RegisterGlobalFunction("float max(float, float)", WRAP_FUNCTION_PR(glm::max, (float, float), float), asCdecl);

	engine->RegisterGlobalFunction("float cos(float)", WRAP_FUNCTION_PR(glm::cos, (float), float), asCdecl);
	engine->RegisterGlobalFunction("float sin(float)", WRAP_FUNCTION_PR(glm::sin, (float), float), asCdecl);
	engine->RegisterGlobalFunction("float tan(float)", WRAP_FUNCTION_PR(glm::tan, (float), float), asCdecl);

	engine->RegisterGlobalFunction("float acos(float)", WRAP_FUNCTION_PR(glm::acos, (float), float), asCdecl);
	engine->RegisterGlobalFunction("float asin(float)", WRAP_FUNCTION_PR(glm::asin, (float), float), asCdecl);
	engine->RegisterGlobalFunction("float atan(float)", WRAP_FUNCTION_PR(glm::atan, (float), float), asCdecl);

	engine->RegisterGlobalFunction("float cosh(float)", WRAP_FUNCTION_PR(glm::cosh, (float), float), asCdecl);
	engine->RegisterGlobalFunction("float sinh(float)", WRAP_FUNCTION_PR(glm::sinh, (float), float), asCdecl);
	engine->RegisterGlobalFunction("float tanh(float)", WRAP_FUNCTION_PR(glm::tanh, (float), float), asCdecl);

	engine->RegisterGlobalFunction("float acosh(float)", WRAP_FUNCTION_PR(glm::acosh, (float), float), asCdecl);
	engine->RegisterGlobalFunction("float asinh(float)", WRAP_FUNCTION_PR(glm::asinh, (float), float), asCdecl);
	engine->RegisterGlobalFunction("float atanh(float)", WRAP_FUNCTION_PR(glm::atanh, (float), float), asCdecl);

	engine->RegisterGlobalFunction("float log(float)", WRAP_FUNCTION_PR(glm::log, (float), float), asCdecl);
	engine->RegisterGlobalFunction("float log2(float)", WRAP_FUNCTION_PR(glm::log2, (float), float), asCdecl);

	engine->RegisterGlobalFunction("float pow(float, float)", WRAP_FUNCTION_PR(glm::pow, (float, float), float), asCdecl);
	
	engine->RegisterGlobalFunction("float sqrt(float)", WRAP_FUNCTION_PR(glm::sqrt, (float), float), asCdecl);
	engine->RegisterGlobalFunction("float invsqrt(float)", WRAP_FUNCTION_PR(glm::inversesqrt, (float), float), asCdecl);

	engine->RegisterGlobalFunction("float abs(float)", WRAP_FUNCTION_PR(glm::abs, (float), float), asCdecl);
	
	engine->RegisterGlobalFunction("float ceil(float)", WRAP_FUNCTION_PR(glm::ceil, (float), float), asCdecl);
	engine->RegisterGlobalFunction("float floor(float)", WRAP_FUNCTION_PR(glm::floor, (float), float), asCdecl);

	engine->RegisterGlobalFunction("Vec3 cross(const Vec3 &in, const Vec3 &in)", WRAP_FUNCTION_PR(glm::cross, (const glm::vec3&, const glm::vec3&), glm::vec3), asCdecl);
	engine->RegisterGlobalFunction("Vec3 normalize(const Vec3 &in)", WRAP_FUNCTION_PR(glm::normalize, (const glm::vec3&), glm::vec3), asCdecl);

	// HTTP Namespace
	engine->SetDefaultNamespace("HTTP");
	engine->RegisterGlobalFunction("string get(const string &in, const string &in)", WRAP_FUNCTION(HTTPGet), asCdecl);

	// Input Namespace
	engine->SetDefaultNamespace("Input");
	engine->RegisterGlobalFunction("float getMouseX()", WRAP_FUNCTION(Input::getMouseX), asCdecl);
	engine->RegisterGlobalFunction("float getMouseY()", WRAP_FUNCTION(Input::getMouseY), asCdecl);
	engine->RegisterGlobalFunction("void captureMouse(bool)", WRAP_FUNCTION(Input::captureMouse), asCdecl);

	engine->RegisterGlobalFunction("bool isKeyPressed(int)", WRAP_FUNCTION(Input::isKeyPressed), asCdecl);
	engine->RegisterGlobalFunction("bool isButtonPressed(int)", WRAP_FUNCTION(Input::isButtonPressed), asCdecl);

	engine->RegisterGlobalFunction("bool isKeyJustPressed(int)", WRAP_FUNCTION(Input::isKeyJustPressed), asCdecl);
	engine->RegisterGlobalFunction("bool isButtonJustPressed(int)", WRAP_FUNCTION(Input::isButtonJustPressed), asCdecl);

	// Resources Namespace
	engine->SetDefaultNamespace("Resources");
	engine->RegisterGlobalFunction("Mesh@ getMesh(const string &in)", WRAP_FUNCTION(Resources::getMesh), asCdecl);
	engine->RegisterGlobalFunction("Texture@ getTexture(const string &in)", WRAP_FUNCTION(Resources::getTexture), asCdecl);
	engine->RegisterGlobalFunction("Texture@ getBuffer(const string &in)", WRAP_FUNCTION(Resources::getBuffer), asCdecl);
	engine->RegisterGlobalFunction("Script@ getScript(const string &in)", WRAP_FUNCTION(Resources::getScript), asCdecl);
	engine->RegisterGlobalFunction("Script@ createScript(const string &in)", WRAP_FUNCTION_PR(Resources::createScript, (const std::string&), Script*), asCdecl);
	engine->RegisterGlobalFunction("Script@ createScript(const string &in, const string& in)", WRAP_FUNCTION_PR(Resources::createScript, (const std::string&, const std::string&), Script*), asCdecl);

	// Temp Namespace
	engine->SetDefaultNamespace("Temp");
	engine->RegisterGlobalFunction("Mesh@ createTextMesh(const string& in, const string &in)", WRAP_FUNCTION(ScriptProvider::temp_create_text_mesh), asCdecl);
	engine->RegisterGlobalFunction("Camera@ getCamera()", WRAP_FUNCTION(DeferredRenderer::getCamera), asCdecl);
	
	// Random Namespace
	engine->SetDefaultNamespace("Random");
	engine->RegisterGlobalFunction("int get(int, int)", WRAP_FUNCTION(boundedRandom), asCdecl);
	engine->RegisterGlobalFunction("int get()", WRAP_FUNCTION(randomNum), asCdecl);
	engine->RegisterGlobalFunction("int get(int)", WRAP_FUNCTION(randomTo), asCdecl);
	
	// App Namespace
	engine->SetDefaultNamespace("App");
	engine->RegisterEnum("Platform");
	engine->RegisterEnumValue("Platform", "WINDOWS", 0);
	engine->RegisterEnumValue("Platform", "LINUX", 1);
	engine->RegisterEnumValue("Platform", "ANDROID", 2);
	engine->RegisterEnumValue("Platform", "UNDEFINED", 3);

	engine->RegisterEnum("PlatformType");
	engine->RegisterEnumValue("PlatformType", "DESKTOP", 0);
	engine->RegisterEnumValue("PlatformType", "MOBILE", 1);
	engine->RegisterEnumValue("PlatformType", "UNDEFINED", 2);

	{
		using namespace grail::platform;
		engine->RegisterGlobalFunction("Platform getPlatform()", WRAP_FUNCTION(getPlatform), asCdecl);
		engine->RegisterGlobalFunction("Platform getPlatformType()", WRAP_FUNCTION(getPlatformType), asCdecl);
		engine->RegisterGlobalFunction("string getPlatformName()", WRAP_FUNCTION(getPlatformName), asCdecl);
	}


	// Script Namespace
	engine->SetDefaultNamespace("Script");
	engine->RegisterGlobalFunction("void executeString(const string &in)", WRAP_FUNCTION(ScriptProvider::executeString), asCdecl);


	// Renderer Namespace
	engine->SetDefaultNamespace("Renderer");
	engine->RegisterGlobalFunction("RendererSettings@ getSettings()", WRAP_FUNCTION(DeferredRenderer::getSettings), asCdecl);

	// VR Namespace
	engine->SetDefaultNamespace("VR");
	engine->RegisterGlobalFunction("bool isInitialized()", WRAP_FUNCTION(VR::isInitialized()), asCdecl);


	// Default Namespace
	engine->SetDefaultNamespace(defaultNamespace);
}

grail::Mesh* grail::ScriptProvider::temp_create_text_mesh(const std::string& assetName, const std::string& text) {
	if (!temp_font) {
		FreeTypeFontGenerator generator = FreeTypeFontGenerator("fonts/Muli-Bold.ttf");
		temp_font = generator.generateFont(256);
	}
	
	std::vector<float> vertices;
	std::vector<uint32_t> indices;
	
	vertices.resize(text.length() * 12 * 4);
	indices.resize(text.length() * 6);

	uint32_t j = 0;
	for (uint32_t i = 0; i < text.length() * 6; i += 6, j += 4) {
		indices[i] = j;
		indices[i + 1] = j + 1;
		indices[i + 2] = j + 2;
		indices[i + 3] = j + 2;
		indices[i + 4] = j + 3;
		indices[i + 5] = j;
	}

	std::string::const_iterator c;
	float textHeight = 0;

	std::vector<float> textWidths;

	float wMax = 0;
	float hMax = 0;

	for (c = text.begin(); c != text.end(); c++) {
		if (*c == '\n') {
			textWidths.push_back(wMax);
			textHeight += hMax;

			wMax = 0;
			hMax = 0;

			continue;
		}

		wMax += ((temp_font->getCharacter(*c).advance >> 6));
		textHeight = glm::max(textHeight, 
			(float)(temp_font->getCharacter(*c).height));
	}

	textWidths.push_back(wMax);
	if (textHeight == 0) textHeight = hMax;

	uint32_t idx = 0;

	uint32_t line = 0;

	float x = 0;
	float y = 0;

	float originalX = x;

	for (c = text.begin(); c != text.end(); c++) {
		grail::FreeTypeFont::Character& cc = temp_font->getCharacter(*c);

		float px = x + cc.bearingX - textWidths[line] * 0.5f;
		float py = y - (cc.height - cc.bearingY) - textHeight * 0.5f;

		float width = cc.width;
		float height = cc.height;

		if (*c == '\n') {
			y -= temp_font->getCharacter('A').height * 1.3;
			x = originalX;
			line++;
			continue;
		}

		{
			vertices[idx++] = px;
			vertices[idx++] = py;
			vertices[idx++] = 0;

			vertices[idx++] = 0;
			vertices[idx++] = 0;
			vertices[idx++] = 1;

			vertices[idx++] = 0;
			vertices[idx++] = 1;
			vertices[idx++] = 0;

			vertices[idx++] = cc.s0;
			vertices[idx++] = cc.t1;
			vertices[idx++] = 0;
		}

		{
			vertices[idx++] = px + width;
			vertices[idx++] = py;
			vertices[idx++] = 0;

			vertices[idx++] = 0;
			vertices[idx++] = 0;
			vertices[idx++] = 1;

			vertices[idx++] = 0;
			vertices[idx++] = 1;
			vertices[idx++] = 0;

			vertices[idx++] = cc.s1;
			vertices[idx++] = cc.t1;
			vertices[idx++] = 0;
		}

		{
			vertices[idx++] = px + width;
			vertices[idx++] = py + height;
			vertices[idx++] = 0;

			vertices[idx++] = 0;
			vertices[idx++] = 0;
			vertices[idx++] = 1;

			vertices[idx++] = 0;
			vertices[idx++] = 1;
			vertices[idx++] = 0;

			vertices[idx++] = cc.s1;
			vertices[idx++] = cc.t0;
			vertices[idx++] = 0;
		}

		{
			vertices[idx++] = px;
			vertices[idx++] = py + height;
			vertices[idx++] = 0;

			vertices[idx++] = 0;
			vertices[idx++] = 0;
			vertices[idx++] = 1;

			vertices[idx++] = 0;
			vertices[idx++] = 1;
			vertices[idx++] = 0;

			vertices[idx++] = cc.s0;
			vertices[idx++] = cc.t0;
			vertices[idx++] = 0;
		}

		x += (cc.advance >> 6);
	}

	idx = 0;
	std::string id = "temp_text_mesh_";

	while (Resources::getMesh((id + std::to_string(idx)))) {
		idx++;
	}

	return Resources::createMesh((id + std::to_string(idx)), nullptr, vertices, indices);
}
