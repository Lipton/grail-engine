#ifndef GRAIL_RAY_TRACING_CONTEXT_H
#define GRAIL_RAY_TRACING_CONTEXT_H

//#define ENABLE_RTX
#ifdef ENABLE_RTX

#include "Common.h"

#include "FrameGraph.h"

#define INDEX_RAYGEN 0
#define INDEX_MISS 1
#define INDEX_CLOSEST_HIT 2


extern PFN_vkCreateAccelerationStructureNV vkCreateAccelerationStructureNVc;
extern PFN_vkDestroyAccelerationStructureNV vkDestroyAccelerationStructureNVc;
extern PFN_vkBindAccelerationStructureMemoryNV vkBindAccelerationStructureMemoryNVc;
extern PFN_vkGetAccelerationStructureHandleNV vkGetAccelerationStructureHandleNVc;
extern PFN_vkGetAccelerationStructureMemoryRequirementsNV vkGetAccelerationStructureMemoryRequirementsNVc;
extern PFN_vkCmdBuildAccelerationStructureNV vkCmdBuildAccelerationStructureNVc;
extern PFN_vkCreateRayTracingPipelinesNV vkCreateRayTracingPipelinesNVc;
extern PFN_vkGetRayTracingShaderGroupHandlesNV vkGetRayTracingShaderGroupHandlesNVc;
extern PFN_vkCmdTraceRaysNV vkCmdTraceRaysNVc;

namespace grail {
	class Scene;
	class VertexLayout;
	class Mesh;
	
	class RayTracingContext {
		struct AccelerationStructure {
			VkDeviceMemory memory = VK_NULL_HANDLE;
			VkAccelerationStructureNV accelerationStructure = VK_NULL_HANDLE;
			uint64_t handle = 0;
		};

		struct GeometryInstance {
			float transform[12];
			uint32_t instanceId : 24;
			uint32_t mask : 8;
			uint32_t instanceOffset : 24;
			uint32_t flags : 8;
			uint64_t accelerationStructureHandle;
		};
	
	public:
		RayTracingContext();

		void initialize(VertexLayout* layout);
		void update();
		void render();

	public:
		VkDeviceSize copyShaderIdentifier(uint8_t* data, const uint8_t* shaderHandleStorage, uint32_t groupIndex);

		void createBottomLevelAccelerationStructure(const VkGeometryNV* geometries, uint32_t geometryCount, uint32_t index);
		void createTopLevelAccelerationStructure(uint32_t instanceCount);

		VkPhysicalDeviceRayTracingPropertiesNV rayTracingProperties;

		std::vector<AccelerationStructure> bottomLevelAS;
		AccelerationStructure topLevelAS;

		VkPipeline pipeline = VK_NULL_HANDLE;
		VkPipelineLayout pipelineLayout = VK_NULL_HANDLE;
		VkDescriptorSet descriptorSet = VK_NULL_HANDLE;
		VkDescriptorSetLayout descriptorSetLayout = VK_NULL_HANDLE;

		VkDescriptorPool descriptorPool = VK_NULL_HANDLE;

		Buffer* shaderBindingTable = nullptr;

		std::vector<Mesh*> uniqueMeshes;
		std::vector<VkDescriptorBufferInfo> vertexBufferInfos;
		std::vector<VkDescriptorBufferInfo> indexBufferInfos;
	};
}

#endif

#endif