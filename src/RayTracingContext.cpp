#include "RayTracingContext.h"

#ifdef ENABLE_RTX
#include "Scene.h"
#include "SceneNodeComponents.h"
#include "Mesh.h"
#include "VertexLayout.h"
#include "VkBuffer.h"
#include "Resources.h"

PFN_vkCreateAccelerationStructureNV vkCreateAccelerationStructureNVc;
PFN_vkDestroyAccelerationStructureNV vkDestroyAccelerationStructureNVc;
PFN_vkBindAccelerationStructureMemoryNV vkBindAccelerationStructureMemoryNVc;
PFN_vkGetAccelerationStructureHandleNV vkGetAccelerationStructureHandleNVc;
PFN_vkGetAccelerationStructureMemoryRequirementsNV vkGetAccelerationStructureMemoryRequirementsNVc;
PFN_vkCmdBuildAccelerationStructureNV vkCmdBuildAccelerationStructureNVc;
PFN_vkCreateRayTracingPipelinesNV vkCreateRayTracingPipelinesNVc;
PFN_vkGetRayTracingShaderGroupHandlesNV vkGetRayTracingShaderGroupHandlesNVc;
PFN_vkCmdTraceRaysNV vkCmdTraceRaysNVc;

grail::RayTracingContext::RayTracingContext() {
//	GRAIL_LOG(DEBUG, "RTX") << "Initializing RTX";
}

void grail::RayTracingContext::initialize(VertexLayout* layout) {
	GRAIL_LOG(INFO, "RT") << "Initializing Raytracing";
	GRAIL_LOG(INFO, "RT") << "Getting Device Properties";
	rayTracingProperties = {};
	rayTracingProperties.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PROPERTIES_NV;

	VkPhysicalDeviceProperties2 deviceProps2 = {};
	deviceProps2.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2;
	deviceProps2.pNext = &rayTracingProperties;
	vkGetPhysicalDeviceProperties2(VulkanContext::mainGPU.physicalDevice, &deviceProps2);

	GRAIL_LOG(INFO, "RT MAX INSTANCES") << rayTracingProperties.maxInstanceCount;
	GRAIL_LOG(INFO, "RT MAX INSTANCES") << rayTracingProperties.maxTriangleCount;
	GRAIL_LOG(INFO, "RT") << "Getting function pointers";
	vkCreateAccelerationStructureNVc = reinterpret_cast<PFN_vkCreateAccelerationStructureNV>(vkGetDeviceProcAddr(VulkanContext::mainGPU.device, "vkCreateAccelerationStructureNV"));
	vkDestroyAccelerationStructureNVc = reinterpret_cast<PFN_vkDestroyAccelerationStructureNV>(vkGetDeviceProcAddr(VulkanContext::mainGPU.device, "vkDestroyAccelerationStructureNV"));
	vkBindAccelerationStructureMemoryNVc = reinterpret_cast<PFN_vkBindAccelerationStructureMemoryNV>(vkGetDeviceProcAddr(VulkanContext::mainGPU.device, "vkBindAccelerationStructureMemoryNV"));
	vkGetAccelerationStructureHandleNVc = reinterpret_cast<PFN_vkGetAccelerationStructureHandleNV>(vkGetDeviceProcAddr(VulkanContext::mainGPU.device, "vkGetAccelerationStructureHandleNV"));
	vkGetAccelerationStructureMemoryRequirementsNVc = reinterpret_cast<PFN_vkGetAccelerationStructureMemoryRequirementsNV>(vkGetDeviceProcAddr(VulkanContext::mainGPU.device, "vkGetAccelerationStructureMemoryRequirementsNV"));
	vkCmdBuildAccelerationStructureNVc = reinterpret_cast<PFN_vkCmdBuildAccelerationStructureNV>(vkGetDeviceProcAddr(VulkanContext::mainGPU.device, "vkCmdBuildAccelerationStructureNV"));
	vkCreateRayTracingPipelinesNVc = reinterpret_cast<PFN_vkCreateRayTracingPipelinesNV>(vkGetDeviceProcAddr(VulkanContext::mainGPU.device, "vkCreateRayTracingPipelinesNV"));
	vkGetRayTracingShaderGroupHandlesNVc = reinterpret_cast<PFN_vkGetRayTracingShaderGroupHandlesNV>(vkGetDeviceProcAddr(VulkanContext::mainGPU.device, "vkGetRayTracingShaderGroupHandlesNV"));
	vkCmdTraceRaysNVc = reinterpret_cast<PFN_vkCmdTraceRaysNV>(vkGetDeviceProcAddr(VulkanContext::mainGPU.device, "vkCmdTraceRaysNV"));

	GRAIL_LOG(INFO, "RT") << "Creating acceleration structures";

	std::vector<VkGeometryNV> geometries;
	std::map<Mesh*, uint32_t> instanceIDs;

	std::vector<SceneNode*> nodes;

	Scene::getNodesWithComponents<nodeComponents::Render>().each([this](auto& c) {
		if (c.mesh) {
			if (std::find(uniqueMeshes.begin(), uniqueMeshes.end(), c.mesh) == uniqueMeshes.end()) {
				uniqueMeshes.push_back(c.mesh);
			}
		}
	});

	bottomLevelAS.resize(uniqueMeshes.size());

	for (Mesh* mesh : uniqueMeshes) {
		VkGeometryNV geometry = {};
		geometry.sType = VK_STRUCTURE_TYPE_GEOMETRY_NV;
		geometry.geometryType = VK_GEOMETRY_TYPE_TRIANGLES_NV;
		geometry.geometry.triangles.sType = VK_STRUCTURE_TYPE_GEOMETRY_TRIANGLES_NV;
		geometry.geometry.triangles.vertexData = mesh->buffer;
		geometry.geometry.triangles.vertexOffset = mesh->vertexOffset;
		geometry.geometry.triangles.vertexCount = mesh->verticesCount;
		geometry.geometry.triangles.vertexStride = sizeof(float) * 12; //layout->getVertexSize();
		geometry.geometry.triangles.vertexFormat = VK_FORMAT_R32G32B32_SFLOAT;
		geometry.geometry.triangles.indexData = mesh->buffer;
		geometry.geometry.triangles.indexOffset = mesh->indexOffset;
		geometry.geometry.triangles.indexCount = mesh->indicesCount;
		geometry.geometry.triangles.indexType = VK_INDEX_TYPE_UINT32;
		geometry.geometry.triangles.transformData = VK_NULL_HANDLE;
		geometry.geometry.triangles.transformOffset = 0;
		geometry.geometry.aabbs = {};
		geometry.geometry.aabbs.sType = { VK_STRUCTURE_TYPE_GEOMETRY_AABB_NV };
		geometry.flags = VK_GEOMETRY_OPAQUE_BIT_NV;

		instanceIDs.insert({ mesh, geometries.size() });
		createBottomLevelAccelerationStructure(&geometry, 1, geometries.size());

		geometries.push_back(geometry);

	}


	Buffer* instanceBuffer = nullptr;
	std::vector<GeometryInstance> geometryInstances;

	int i = 0;
	for (SceneNode* node : nodes) {
		nodeComponents::Render* c = Scene::getComponent<nodeComponents::Render>(node);
		//node->updateTransform(true);

		if (c && node) {
			GeometryInstance instance = {};
			instance.accelerationStructureHandle = bottomLevelAS[instanceIDs[c->mesh]].handle;
			instance.flags = VK_GEOMETRY_INSTANCE_TRIANGLE_CULL_DISABLE_BIT_NV;
			instance.instanceId = instanceIDs[c->mesh];
			instance.instanceOffset = 0;
			instance.mask = 0xff;

			GRAIL_LOG(INFO, "INSTANCE") << instance.accelerationStructureHandle << " " << instance.instanceId << " " << instance.flags << " " << instance.instanceOffset;

			glm::mat4 transform = node->transform.getWorldMatrix();
			//transform = glm::translate(transform, glm::vec3((i + 1) * 20, 0, 0));

			glm::mat4x4 transp = glm::transpose(transform);
			memcpy(&instance.transform[0], glm::value_ptr(transp), 12 * sizeof(float));

			geometryInstances.push_back(instance);

			i++;
		}
	}




		BufferCreateInfo bufferInfo = {};
		bufferInfo.usageBits = BufferUsageBits::RAY_TRACING_BIT_NV;
		bufferInfo.memoryProperties = MemoryPropertyFlags::HOST_VISIBLE | MemoryPropertyFlags::HOST_COHERENT;
		bufferInfo.size = sizeof(GeometryInstance) * geometryInstances.size() * 60000;

		GRAIL_LOG(WARNING, "BUFFER SIZE") << bufferInfo.size;

		instanceBuffer = new Buffer(bufferInfo, Resources::getBufferMemoryAllocator());
		instanceBuffer->map();

		void* mappedPointer;
		mappedPointer = instanceBuffer->getMappedPtr();
		//VK_VALIDATE_RESULT(vkMapMemory(VulkanContext::mainGPU.device, instanceBuffer->getMemoryRegionDescriptor().page., instanceBuffer->getMemoryRegionDescriptor().offset, sizeof(GeometryInstance)* geometryInstances.size(), 0, &mappedPointer));

		GeometryInstance* mp = (GeometryInstance*)mappedPointer;

		for (GeometryInstance& inst : geometryInstances) {
			GRAIL_LOG(INFO, "INSTANCE") << inst.accelerationStructureHandle << " " << inst.instanceId << " " << inst.flags << " " << inst.instanceOffset;

			memcpy(mp, &inst, sizeof(GeometryInstance));

			mp++;
		}

		//memcpy(mappedPointer, &geometryInstances[0], sizeof(GeometryInstance)* geometryInstances.size());


	createTopLevelAccelerationStructure(geometryInstances.size());

	GRAIL_LOG(INFO, "RT") << "Building Acceleration Structures";

	// Acceleration structure build requires some scratch space to store temporary information
	VkAccelerationStructureMemoryRequirementsInfoNV memoryRequirementsInfo{};
	memoryRequirementsInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_MEMORY_REQUIREMENTS_INFO_NV;
	memoryRequirementsInfo.type = VK_ACCELERATION_STRUCTURE_MEMORY_REQUIREMENTS_TYPE_OBJECT_NV;

	VkDeviceSize maxBottom = 0;
	for (AccelerationStructure& structure : bottomLevelAS) {
		VkMemoryRequirements2 memReqBottomLevelAS;
		memoryRequirementsInfo.accelerationStructure = structure.accelerationStructure;
		vkGetAccelerationStructureMemoryRequirementsNVc(VulkanContext::mainGPU.device, &memoryRequirementsInfo, &memReqBottomLevelAS);

		maxBottom = std::max(maxBottom, memReqBottomLevelAS.memoryRequirements.size);
	}

	GRAIL_LOG(DEBUG, "SIZEOF") << sizeof(GeometryInstance);

	VkMemoryRequirements2 memReqTopLevelAS;
	memoryRequirementsInfo.accelerationStructure = topLevelAS.accelerationStructure;
	vkGetAccelerationStructureMemoryRequirementsNVc(VulkanContext::mainGPU.device, &memoryRequirementsInfo, &memReqTopLevelAS);

	const VkDeviceSize scratchBufferSize = std::max(maxBottom, memReqTopLevelAS.memoryRequirements.size);
	Buffer* scratchBuffer = nullptr;
	{
		BufferCreateInfo bufferInfo = {};
		bufferInfo.usageBits = BufferUsageBits::RAY_TRACING_BIT_NV;
		bufferInfo.memoryProperties = MemoryPropertyFlags::DEVICE_LOCAL;
		bufferInfo.size = scratchBufferSize;
		bufferInfo.specialMemoryReqs = memReqTopLevelAS;
		scratchBuffer = new Buffer(bufferInfo, Resources::getBufferMemoryAllocator());
	}

	TemporaryCommandBuffer cmdBuffer = VulkanContext::mainGPU.createTemporaryBuffer(VulkanContext::mainGPU.graphicsQueue, VK_COMMAND_BUFFER_LEVEL_PRIMARY);
	cmdBuffer.begin();
	/*
		Build bottom level acceleration structure
	*/
	VkAccelerationStructureInfoNV buildInfo{};
	VkMemoryBarrier memoryBarrier = {};

	uint32_t acc_index = 0;
	for (VkGeometryNV& inst : geometries) {
		VkAccelerationStructureInfoNV buildInfo{};
		buildInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_INFO_NV;
		buildInfo.type = VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_NV;
		buildInfo.geometryCount = 1;
		buildInfo.pGeometries = &geometries[acc_index];

		vkCmdBuildAccelerationStructureNVc(
			cmdBuffer.getHandle(),
			&buildInfo,
			VK_NULL_HANDLE,
			0,
			VK_FALSE,
			bottomLevelAS[acc_index].accelerationStructure,
			VK_NULL_HANDLE,
			scratchBuffer->handle,
			0);

		VkMemoryBarrier memoryBarrier = {};
		memoryBarrier.sType = VK_STRUCTURE_TYPE_MEMORY_BARRIER;
		memoryBarrier.srcAccessMask = VK_ACCESS_ACCELERATION_STRUCTURE_WRITE_BIT_NV | VK_ACCESS_ACCELERATION_STRUCTURE_READ_BIT_NV;
		memoryBarrier.dstAccessMask = VK_ACCESS_ACCELERATION_STRUCTURE_WRITE_BIT_NV | VK_ACCESS_ACCELERATION_STRUCTURE_READ_BIT_NV;
		vkCmdPipelineBarrier(cmdBuffer.getHandle(), VK_PIPELINE_STAGE_ACCELERATION_STRUCTURE_BUILD_BIT_NV, VK_PIPELINE_STAGE_ACCELERATION_STRUCTURE_BUILD_BIT_NV, 0, 1, &memoryBarrier, 0, 0, 0, 0);

		acc_index++;
	}

	cmdBuffer.end();
	cmdBuffer.submit();

	cmdBuffer.begin();

	/*
		Build top-level acceleration structure
	*/
	buildInfo.type = VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_NV;
	buildInfo.pGeometries = 0;
	buildInfo.geometryCount = 0;
	buildInfo.instanceCount = geometryInstances.size();

	vkCmdBuildAccelerationStructureNVc(
		cmdBuffer.getHandle(),
		&buildInfo,
		instanceBuffer->handle,
		0,
		VK_FALSE,
		topLevelAS.accelerationStructure,
		VK_NULL_HANDLE,
		scratchBuffer->handle,
		0);

	vkCmdPipelineBarrier(cmdBuffer.getHandle(), VK_PIPELINE_STAGE_ACCELERATION_STRUCTURE_BUILD_BIT_NV, VK_PIPELINE_STAGE_ACCELERATION_STRUCTURE_BUILD_BIT_NV, 0, 1, &memoryBarrier, 0, 0, 0, 0);
	cmdBuffer.end();
	cmdBuffer.submit();

	VkDescriptorSetLayoutBinding accelerationStructureLayoutBinding{};
	accelerationStructureLayoutBinding.binding = 0;
	accelerationStructureLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_NV;
	accelerationStructureLayoutBinding.descriptorCount = 1;
	accelerationStructureLayoutBinding.stageFlags = VK_SHADER_STAGE_RAYGEN_BIT_NV | VK_SHADER_STAGE_CLOSEST_HIT_BIT_NV;

	VkDescriptorSetLayoutBinding resultImageLayoutBinding{};
	resultImageLayoutBinding.binding = 1;
	resultImageLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
	resultImageLayoutBinding.descriptorCount = 1;
	resultImageLayoutBinding.stageFlags = VK_SHADER_STAGE_RAYGEN_BIT_NV;

	VkDescriptorSetLayoutBinding uniformBufferBinding{};
	uniformBufferBinding.binding = 2;
	uniformBufferBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	uniformBufferBinding.descriptorCount = 1;
	uniformBufferBinding.stageFlags = VK_SHADER_STAGE_RAYGEN_BIT_NV | VK_SHADER_STAGE_CLOSEST_HIT_BIT_NV | VK_SHADER_STAGE_MISS_BIT_NV;

	VkDescriptorSetLayoutBinding vertexBufferBinding{};
	vertexBufferBinding.binding = 3;
	vertexBufferBinding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
	vertexBufferBinding.descriptorCount = 60;
	vertexBufferBinding.stageFlags = VK_SHADER_STAGE_CLOSEST_HIT_BIT_NV;

	VkDescriptorSetLayoutBinding indexBufferBinding{};
	indexBufferBinding.binding = 4;
	indexBufferBinding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
	indexBufferBinding.descriptorCount = 60;
	indexBufferBinding.stageFlags = VK_SHADER_STAGE_CLOSEST_HIT_BIT_NV;

	VkDescriptorSetLayoutBinding textureBinding{};
	textureBinding.binding = 5;
	textureBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	textureBinding.descriptorCount = 1;
	textureBinding.stageFlags = VK_SHADER_STAGE_CLOSEST_HIT_BIT_NV;

	VkDescriptorSetLayoutBinding envTexBinding{};
	envTexBinding.binding = 6;
	envTexBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	envTexBinding.descriptorCount = 1;
	envTexBinding.stageFlags = VK_SHADER_STAGE_CLOSEST_HIT_BIT_NV | VK_SHADER_STAGE_MISS_BIT_NV;

	VkDescriptorSetLayoutBinding normalTexBinding{};
	normalTexBinding.binding = 7;
	normalTexBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	normalTexBinding.descriptorCount = 1;
	normalTexBinding.stageFlags = VK_SHADER_STAGE_CLOSEST_HIT_BIT_NV | VK_SHADER_STAGE_MISS_BIT_NV | VK_SHADER_STAGE_RAYGEN_BIT_NV;

	VkDescriptorSetLayoutBinding depthTexBinding{};
	depthTexBinding.binding = 8;
	depthTexBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	depthTexBinding.descriptorCount = 1;
	depthTexBinding.stageFlags = VK_SHADER_STAGE_CLOSEST_HIT_BIT_NV | VK_SHADER_STAGE_MISS_BIT_NV | VK_SHADER_STAGE_RAYGEN_BIT_NV;

	VkDescriptorSetLayoutBinding shadowBinding{};
	shadowBinding.binding = 9;
	shadowBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	shadowBinding.descriptorCount = 1;
	shadowBinding.stageFlags = VK_SHADER_STAGE_CLOSEST_HIT_BIT_NV;

	std::vector<VkDescriptorSetLayoutBinding> bindings({
		accelerationStructureLayoutBinding,
		resultImageLayoutBinding,
		uniformBufferBinding,
		vertexBufferBinding,
		indexBufferBinding,
		textureBinding,
		envTexBinding,
		normalTexBinding,
		depthTexBinding,
		shadowBinding
		});

	GRAIL_LOG(INFO, "INFO") << bindings.size();

	VkDescriptorSetLayoutCreateInfo layoutInfo{};
	layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	layoutInfo.bindingCount = static_cast<uint32_t>(bindings.size());
	layoutInfo.pBindings = bindings.data();
	VK_VALIDATE_RESULT(vkCreateDescriptorSetLayout(VulkanContext::mainGPU.device, &layoutInfo, nullptr, &descriptorSetLayout));

	VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo{};
	pipelineLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipelineLayoutCreateInfo.setLayoutCount = 1;
	pipelineLayoutCreateInfo.pSetLayouts = &descriptorSetLayout;

	VK_VALIDATE_RESULT(vkCreatePipelineLayout(VulkanContext::mainGPU.device, &pipelineLayoutCreateInfo, nullptr, &pipelineLayout));

	const uint32_t shaderIndexRaygen = 0;
	const uint32_t shaderIndexMiss = 1;
	const uint32_t shaderIndexClosestHit = 2;

	std::array<VkPipelineShaderStageCreateInfo, 3> shaderStages;
	{
		VkShaderModule module = Resources::loadShader("shaders/rt/raygen.rgen.spv");

		VkPipelineShaderStageCreateInfo info = {};
		info.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		info.stage = VK_SHADER_STAGE_RAYGEN_BIT_NV;
		info.module = module;
		info.pName = "main";

		shaderStages[shaderIndexRaygen] = info;
	}
	{
		VkShaderModule module = Resources::loadShader("shaders/rt/miss.rmiss.spv");

		VkPipelineShaderStageCreateInfo info = {};
		info.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		info.stage = VK_SHADER_STAGE_MISS_BIT_NV;
		info.module = module;
		info.pName = "main";

		shaderStages[shaderIndexMiss] = info;
	}
	{
		VkShaderModule module = Resources::loadShader("shaders/rt/closesthit.rchit.spv");

		VkPipelineShaderStageCreateInfo info = {};
		info.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		info.stage = VK_SHADER_STAGE_CLOSEST_HIT_BIT_NV;
		info.module = module;
		info.pName = "main";

		shaderStages[shaderIndexClosestHit] = info;
	}

	/*
		Setup ray tracing shader groups
	*/
	std::array<VkRayTracingShaderGroupCreateInfoNV, 3> groups{};
	for (auto& group : groups) {
		// Init all groups with some default values
		group.sType = VK_STRUCTURE_TYPE_RAY_TRACING_SHADER_GROUP_CREATE_INFO_NV;
		group.generalShader = VK_SHADER_UNUSED_NV;
		group.closestHitShader = VK_SHADER_UNUSED_NV;
		group.anyHitShader = VK_SHADER_UNUSED_NV;
		group.intersectionShader = VK_SHADER_UNUSED_NV;
	}

	// Links shaders and types to ray tracing shader groups
	// Ray generation shader group
	groups[INDEX_RAYGEN].type = VK_RAY_TRACING_SHADER_GROUP_TYPE_GENERAL_NV;
	groups[INDEX_RAYGEN].generalShader = shaderIndexRaygen;
	// Scene miss shader group
	groups[INDEX_MISS].type = VK_RAY_TRACING_SHADER_GROUP_TYPE_GENERAL_NV;
	groups[INDEX_MISS].generalShader = shaderIndexMiss;
	// Scene closest hit shader group
	groups[INDEX_CLOSEST_HIT].type = VK_RAY_TRACING_SHADER_GROUP_TYPE_TRIANGLES_HIT_GROUP_NV;
	groups[INDEX_CLOSEST_HIT].generalShader = VK_SHADER_UNUSED_NV;
	groups[INDEX_CLOSEST_HIT].closestHitShader = shaderIndexClosestHit;

	VkRayTracingPipelineCreateInfoNV rayPipelineInfo{};
	rayPipelineInfo.sType = VK_STRUCTURE_TYPE_RAY_TRACING_PIPELINE_CREATE_INFO_NV;
	rayPipelineInfo.stageCount = static_cast<uint32_t>(shaderStages.size());
	rayPipelineInfo.pStages = shaderStages.data();
	rayPipelineInfo.groupCount = static_cast<uint32_t>(groups.size());
	rayPipelineInfo.pGroups = groups.data();
	rayPipelineInfo.maxRecursionDepth = 1;
	rayPipelineInfo.layout = pipelineLayout;
	VK_VALIDATE_RESULT(vkCreateRayTracingPipelinesNVc(VulkanContext::mainGPU.device, VK_NULL_HANDLE, 1, &rayPipelineInfo, nullptr, &pipeline));

	GRAIL_LOG(INFO, "RT") << "Creating RT Shader Binding Table";
	// Create buffer for the shader binding table
	const uint32_t sbtSize = rayTracingProperties.shaderGroupHandleSize * 3;
	{
		BufferCreateInfo bufferInfo = {};
		bufferInfo.usageBits = BufferUsageBits::RAY_TRACING_BIT_NV;
		bufferInfo.memoryProperties = MemoryPropertyFlags::HOST_VISIBLE;
		bufferInfo.size = sbtSize;
		shaderBindingTable = new Buffer(bufferInfo, Resources::getBufferMemoryAllocator());
		shaderBindingTable->map();
	}

	auto shaderHandleStorage = new uint8_t[sbtSize];
	// Get shader identifiers
	VK_VALIDATE_RESULT(vkGetRayTracingShaderGroupHandlesNVc(VulkanContext::mainGPU.device, pipeline, 0, 3, sbtSize, shaderHandleStorage));
	auto* data = static_cast<uint8_t*>(shaderBindingTable->getMappedPtr());
	// Copy the shader identifiers to the shader binding table
	VkDeviceSize offset = 0;
	data += copyShaderIdentifier(data, shaderHandleStorage, INDEX_RAYGEN);
	data += copyShaderIdentifier(data, shaderHandleStorage, INDEX_MISS);
	data += copyShaderIdentifier(data, shaderHandleStorage, INDEX_CLOSEST_HIT);
	shaderBindingTable->unmap();

	GRAIL_LOG(INFO, "RT") << "Creating RT Descriptor Sets";
	std::vector<VkDescriptorPoolSize> poolSizes = {
			{ VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_NV, 1 },
			{ VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 1 },
			{ VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1 },
			{ VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, static_cast<uint32_t>(uniqueMeshes.size()) * 2 },
			{VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 5}
	};
	VkDescriptorPoolCreateInfo descriptorPoolCreateInfo = {};
	descriptorPoolCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	descriptorPoolCreateInfo.poolSizeCount = poolSizes.size();
	descriptorPoolCreateInfo.pPoolSizes = &poolSizes[0];
	descriptorPoolCreateInfo.maxSets = 1;
	VK_VALIDATE_RESULT(vkCreateDescriptorPool(VulkanContext::mainGPU.device, &descriptorPoolCreateInfo, nullptr, &descriptorPool));

	VkDescriptorSetAllocateInfo descriptorSetAllocateInfo = {};
	descriptorSetAllocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	descriptorSetAllocateInfo.descriptorPool = descriptorPool;
	descriptorSetAllocateInfo.pSetLayouts = &descriptorSetLayout;
	descriptorSetAllocateInfo.descriptorSetCount = 1;
	VK_VALIDATE_RESULT(vkAllocateDescriptorSets(VulkanContext::mainGPU.device, &descriptorSetAllocateInfo, &descriptorSet));

	VkWriteDescriptorSetAccelerationStructureNV descriptorAccelerationStructureInfo{};
	descriptorAccelerationStructureInfo.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET_ACCELERATION_STRUCTURE_NV;
	descriptorAccelerationStructureInfo.accelerationStructureCount = 1;
	descriptorAccelerationStructureInfo.pAccelerationStructures = &topLevelAS.accelerationStructure;

	VkWriteDescriptorSet accelerationStructureWrite{};
	accelerationStructureWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	// The specialized acceleration structure descriptor has to be chained
	accelerationStructureWrite.pNext = &descriptorAccelerationStructureInfo;
	accelerationStructureWrite.dstSet = descriptorSet;
	accelerationStructureWrite.dstBinding = 0;
	accelerationStructureWrite.descriptorCount = 1;
	accelerationStructureWrite.descriptorType = VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_NV;

	VkDescriptorImageInfo storageImageDescriptor{};
	storageImageDescriptor.imageView = Resources::getTexture("rtx_output")->getImageView();
	storageImageDescriptor.imageLayout = VK_IMAGE_LAYOUT_GENERAL;

	VkWriteDescriptorSet resultImageWrite = {};
	resultImageWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	resultImageWrite.pNext = nullptr;
	resultImageWrite.dstSet = descriptorSet;
	resultImageWrite.dstBinding = 1;
	resultImageWrite.descriptorCount = 1;
	resultImageWrite.pImageInfo = &storageImageDescriptor;
	resultImageWrite.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;

	VkDescriptorBufferInfo ii = Resources::getBuffer("camera")->getDescriptor();

	VkWriteDescriptorSet uniformBufferWrite = {};
	uniformBufferWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	uniformBufferWrite.pNext = nullptr;
	uniformBufferWrite.dstSet = descriptorSet;
	uniformBufferWrite.dstBinding = 2;
	uniformBufferWrite.descriptorCount = 1;
	uniformBufferWrite.pBufferInfo = &ii;
	uniformBufferWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;

	for (uint32_t i = 0; i < uniqueMeshes.size(); i++) {
		VkDescriptorBufferInfo v = {};
		v.buffer = uniqueMeshes[i]->buffer;
		v.offset = uniqueMeshes[i]->vertexOffset;
		v.range = uniqueMeshes[i]->verticesCount * sizeof(float);
		vertexBufferInfos.push_back(v);

		VkDescriptorBufferInfo in = {};
		in.buffer = uniqueMeshes[i]->buffer;
		in.offset = uniqueMeshes[i]->indexOffset;
		in.range = uniqueMeshes[i]->indicesCount * sizeof(uint32_t);
		indexBufferInfos.push_back(in);
	}

	VkWriteDescriptorSet vertexBufferWrite = {};
	vertexBufferWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	vertexBufferWrite.pNext = nullptr;
	vertexBufferWrite.dstSet = descriptorSet;
	vertexBufferWrite.dstBinding = 3;
	vertexBufferWrite.descriptorCount = vertexBufferInfos.size();
	vertexBufferWrite.pBufferInfo = &vertexBufferInfos[0];
	vertexBufferWrite.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;

	VkWriteDescriptorSet indexBufferWrite = {};
	indexBufferWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	indexBufferWrite.pNext = nullptr;
	indexBufferWrite.dstSet = descriptorSet;
	indexBufferWrite.dstBinding = 4;
	indexBufferWrite.descriptorCount = vertexBufferInfos.size();
	indexBufferWrite.pBufferInfo = &indexBufferInfos[0];
	indexBufferWrite.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;

	VkWriteDescriptorSet textureWrite = {};
	textureWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	textureWrite.pNext = nullptr;
	textureWrite.dstSet = descriptorSet;
	textureWrite.dstBinding = 5;
	textureWrite.descriptorCount = 1;
	textureWrite.pImageInfo = &Resources::getTexture("rtx_output")->getDescriptor();
	textureWrite.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;

	VkWriteDescriptorSet envTextureWrite = {};
	envTextureWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	envTextureWrite.pNext = nullptr;
	envTextureWrite.dstSet = descriptorSet;
	envTextureWrite.dstBinding = 6;
	envTextureWrite.descriptorCount = 1;
	envTextureWrite.pImageInfo = &Resources::getTexture("IrradianceCube")->getDescriptor();
	envTextureWrite.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;

	VkWriteDescriptorSet normalTextureWrite = {};
	normalTextureWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	normalTextureWrite.pNext = nullptr;
	normalTextureWrite.dstSet = descriptorSet;
	normalTextureWrite.dstBinding = 7;
	normalTextureWrite.descriptorCount = 1;
	normalTextureWrite.pImageInfo = &Resources::getTexture("graph_G_Normal")->getDescriptor();
	normalTextureWrite.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;

	VkWriteDescriptorSet depthTexWrite = {};
	depthTexWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	depthTexWrite.pNext = nullptr;
	depthTexWrite.dstSet = descriptorSet;
	depthTexWrite.dstBinding = 8;
	depthTexWrite.descriptorCount = 1;
	depthTexWrite.pImageInfo = &Resources::getTexture("graph_G_Depth")->getDescriptor();
	depthTexWrite.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;

	VkWriteDescriptorSet shadowWrite = {};
	shadowWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	shadowWrite.pNext = nullptr;
	shadowWrite.dstSet = descriptorSet;
	shadowWrite.dstBinding = 9;
	shadowWrite.descriptorCount = 1;
	shadowWrite.pImageInfo = &Resources::getTexture("graph_Shadow_Cascades")->getDescriptor();
	shadowWrite.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;

	std::vector<VkWriteDescriptorSet> writeDescriptorSets = {
		accelerationStructureWrite,
		resultImageWrite,
		uniformBufferWrite,
		vertexBufferWrite,
		indexBufferWrite,
		textureWrite,
		envTextureWrite,
		normalTextureWrite,
		depthTexWrite,
		shadowWrite
	};

	vkUpdateDescriptorSets(VulkanContext::mainGPU.device, static_cast<uint32_t>(writeDescriptorSets.size()), writeDescriptorSets.data(), 0, VK_NULL_HANDLE);
}

void grail::RayTracingContext::update() {

}

void grail::RayTracingContext::render() {
}

VkDeviceSize grail::RayTracingContext::copyShaderIdentifier(uint8_t* data, const uint8_t* shaderHandleStorage, uint32_t groupIndex) {
	const uint32_t shaderGroupHandleSize = rayTracingProperties.shaderGroupHandleSize;
	memcpy(data, shaderHandleStorage + groupIndex * shaderGroupHandleSize, shaderGroupHandleSize);
	data += shaderGroupHandleSize;
	return shaderGroupHandleSize;
}

void grail::RayTracingContext::createBottomLevelAccelerationStructure(const VkGeometryNV* geometries, uint32_t geometryCount, uint32_t index) {
	VkAccelerationStructureInfoNV accelerationStructureInfo{};
	accelerationStructureInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_INFO_NV;
	accelerationStructureInfo.type = VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_NV;
	accelerationStructureInfo.instanceCount = 0;
	accelerationStructureInfo.geometryCount = geometryCount;
	accelerationStructureInfo.pGeometries = geometries;

	VkAccelerationStructureCreateInfoNV accelerationStructureCI{};
	accelerationStructureCI.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_NV;
	accelerationStructureCI.info = accelerationStructureInfo;
	accelerationStructureCI.compactedSize = 0;
	VK_VALIDATE_RESULT(vkCreateAccelerationStructureNVc(VulkanContext::mainGPU.device, &accelerationStructureCI, nullptr, &bottomLevelAS[index].accelerationStructure));

	VkAccelerationStructureMemoryRequirementsInfoNV memoryRequirementsInfo{};
	memoryRequirementsInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_MEMORY_REQUIREMENTS_INFO_NV;
	memoryRequirementsInfo.accelerationStructure = bottomLevelAS[index].accelerationStructure;

	VkMemoryRequirements2 memoryRequirements2{};
	vkGetAccelerationStructureMemoryRequirementsNVc(VulkanContext::mainGPU.device, &memoryRequirementsInfo, &memoryRequirements2);

	VkMemoryAllocateInfo memoryAllocateInfo = {};
	memoryAllocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	memoryAllocateInfo.pNext = nullptr;
	memoryAllocateInfo.allocationSize = memoryRequirements2.memoryRequirements.size;
	memoryAllocateInfo.memoryTypeIndex = VulkanContext::mainGPU.getMemoryType(memoryRequirements2.memoryRequirements.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
	VK_VALIDATE_RESULT(vkAllocateMemory(VulkanContext::mainGPU.device, &memoryAllocateInfo, nullptr, &bottomLevelAS[index].memory));

	VkBindAccelerationStructureMemoryInfoNV accelerationStructureMemoryInfo{};
	accelerationStructureMemoryInfo.sType = VK_STRUCTURE_TYPE_BIND_ACCELERATION_STRUCTURE_MEMORY_INFO_NV;
	accelerationStructureMemoryInfo.accelerationStructure = bottomLevelAS[index].accelerationStructure;
	accelerationStructureMemoryInfo.memory = bottomLevelAS[index].memory;
	VK_VALIDATE_RESULT(vkBindAccelerationStructureMemoryNVc(VulkanContext::mainGPU.device, 1, &accelerationStructureMemoryInfo));

	VK_VALIDATE_RESULT(vkGetAccelerationStructureHandleNVc(VulkanContext::mainGPU.device, bottomLevelAS[index].accelerationStructure, sizeof(uint64_t), &bottomLevelAS[index].handle));

}

void grail::RayTracingContext::createTopLevelAccelerationStructure(uint32_t instanceCount) {
	VkAccelerationStructureInfoNV accelerationStructureInfo{};
	accelerationStructureInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_INFO_NV;
	accelerationStructureInfo.type = VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_NV;
	accelerationStructureInfo.instanceCount = instanceCount;
	accelerationStructureInfo.geometryCount = 0;

	VkAccelerationStructureCreateInfoNV accelerationStructureCI{};
	accelerationStructureCI.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_NV;
	accelerationStructureCI.info = accelerationStructureInfo;
	VK_VALIDATE_RESULT(vkCreateAccelerationStructureNVc(VulkanContext::mainGPU.device, &accelerationStructureCI, nullptr, &topLevelAS.accelerationStructure));

	VkAccelerationStructureMemoryRequirementsInfoNV memoryRequirementsInfo{};
	memoryRequirementsInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_MEMORY_REQUIREMENTS_INFO_NV;
	memoryRequirementsInfo.accelerationStructure = topLevelAS.accelerationStructure;

	VkMemoryRequirements2 memoryRequirements2{};
	vkGetAccelerationStructureMemoryRequirementsNVc(VulkanContext::mainGPU.device, &memoryRequirementsInfo, &memoryRequirements2);

	VkMemoryAllocateInfo memoryAllocateInfo = {};
	memoryAllocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	memoryAllocateInfo.pNext = nullptr;
	memoryAllocateInfo.allocationSize = memoryRequirements2.memoryRequirements.size;
	memoryAllocateInfo.memoryTypeIndex = VulkanContext::mainGPU.getMemoryType(memoryRequirements2.memoryRequirements.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
	VK_VALIDATE_RESULT(vkAllocateMemory(VulkanContext::mainGPU.device, &memoryAllocateInfo, nullptr, &topLevelAS.memory));

	VkBindAccelerationStructureMemoryInfoNV accelerationStructureMemoryInfo{};
	accelerationStructureMemoryInfo.sType = VK_STRUCTURE_TYPE_BIND_ACCELERATION_STRUCTURE_MEMORY_INFO_NV;
	accelerationStructureMemoryInfo.accelerationStructure = topLevelAS.accelerationStructure;
	accelerationStructureMemoryInfo.memory = topLevelAS.memory;
	VK_VALIDATE_RESULT(vkBindAccelerationStructureMemoryNVc(VulkanContext::mainGPU.device, 1, &accelerationStructureMemoryInfo));

	VK_VALIDATE_RESULT(vkGetAccelerationStructureHandleNVc(VulkanContext::mainGPU.device, topLevelAS.accelerationStructure, sizeof(uint64_t), &topLevelAS.handle));
}
#endif