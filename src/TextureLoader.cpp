#include "TextureLoader.h"

#include "FileIO.h"
#include "MemoryRegion.h"

/*#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include <gli/gli.hpp>*/

#include "NativeTextureContainer.h"
#include "MemoryAllocator.h"

grail::VkTexture* grail::TextureLoader::loadTexture(std::string path, TextureCreateInfo & createInfo, MemoryAllocator* stagingMemory, MemoryAllocator* dedicatedMemory) {
	// Load image from disk along with mipmaps (if available)
	NativeTextureContainer nativeTex = NativeTextureContainer(path);

	VkTexture* texture = new VkTexture();

	create(
		path,
		texture,
		0, 0, 1,
		createInfo,
		stagingMemory,
		dedicatedMemory,
		nativeTex.getDataPointer(),
		nativeTex.getDataSize(),
		&nativeTex
	);

	return texture;
}

grail::VkTexture* grail::TextureLoader::createTexture(std::string name, uint32_t width, uint32_t height, TextureCreateInfo & createInfo, MemoryAllocator* dedicatedMemory) {
	VkTexture* texture = new VkTexture();

	create(
		name,
		texture,
		width, height, 1,
		createInfo,
		nullptr,
		dedicatedMemory,
		nullptr, 0, nullptr
	);

	return texture;
}

grail::VkTexture* grail::TextureLoader::createTexture(std::string name, uint32_t width, uint32_t height, void * initialData, uint32_t size, TextureCreateInfo & createInfo, MemoryAllocator* dedicatedMemory, MemoryAllocator* stagingMemory) {
	VkTexture* texture = new VkTexture();

	create(
		name,
		texture,
		width, height, 1,
		createInfo, stagingMemory, dedicatedMemory, initialData, size);

	return texture;
}

void grail::TextureLoader::create(std::string& identifier, VkTexture* texture, uint32_t width, uint32_t height, uint32_t depth, TextureCreateInfo& info, MemoryAllocator* stagingMemory, MemoryAllocator* dedicatedMemory, void* data, uint32_t dataSize, NativeTextureContainer* nativeTex) {
	// Setup initial texture parameters
	texture->identifier = identifier;
	texture->gpu = info.gpu;

	if (nativeTex) {
		uint32_t mipLevels = info.generateMipmaps ?
			(info.mipLevels == 0 ?
				nativeTex->getMipLevels() :
				glm::min(info.mipLevels, nativeTex->getMipLevels()))
			: 1;

		texture->mipLevels = mipLevels;
		texture->width = nativeTex->getWidth();
		texture->height = nativeTex->getHeight();
		texture->format = nativeTex->getFormat();
		texture->swizzleMasks[0] = nativeTex->getSwizzleMask(0);
		texture->swizzleMasks[1] = nativeTex->getSwizzleMask(1);
		texture->swizzleMasks[2] = nativeTex->getSwizzleMask(2);
		texture->swizzleMasks[3] = nativeTex->getSwizzleMask(3);
	}
	else {
		uint32_t mipLevels =
			info.generateMipmaps ?
			(info.mipLevels == 0 ?
				static_cast<uint32_t>(std::floor(std::log2(std::max(width, height))) + 1) :
				info.mipLevels) :
			1;

		texture->mipLevels = mipLevels;
		texture->width = width;
		texture->height = height;
		texture->depth = depth;
		texture->format = info.format;
		texture->layerLevels = info.arrayLevels;
	}

	// Get the required format properties
	VkFormatProperties formatProperties;
	vkGetPhysicalDeviceFormatProperties(info.gpu.physicalDevice, texture->format, &formatProperties);

	// Create the image
	VkImageCreateInfo imageCreateInfo = {};
	imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	imageCreateInfo.pNext = nullptr;
	imageCreateInfo.flags = info.createFlags;
	imageCreateInfo.imageType = static_cast<VkImageType>(info.type);
	imageCreateInfo.format = texture->format;
	imageCreateInfo.mipLevels = texture->mipLevels;
	imageCreateInfo.arrayLayers = info.arrayLevels;
	imageCreateInfo.samples = static_cast<VkSampleCountFlagBits>(info.sampleCount);
	imageCreateInfo.tiling = static_cast<VkImageTiling>(info.tiling);
	imageCreateInfo.sharingMode = static_cast<VkSharingMode>(info.sharingMode);
	imageCreateInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	imageCreateInfo.extent = {
		static_cast<uint32_t>(texture->width),
		static_cast<uint32_t>(texture->height),
		static_cast<uint32_t>(texture->depth)
	};
	imageCreateInfo.usage = static_cast<VkImageUsageFlagBits>(info.usageBits);
	VK_VALIDATE_RESULT(vkCreateImage(info.gpu.device, &imageCreateInfo, nullptr, &texture->image));

	// Bind image memory
	VkMemoryRequirements imageMemoryReqs;
	vkGetImageMemoryRequirements(info.gpu.device, texture->image, &imageMemoryReqs);

	/*MemoryRegionDescriptor region = dedicatedMemory->allocateRegion(
		imageMemoryReqs.size,
		imageMemoryReqs.alignment,
		VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
		imageMemoryReqs.memoryTypeBits);
	VK_VALIDATE_RESULT(vkBindImageMemory(info.gpu.device, texture->image, region.block->memoryHandle, region.offset));*/

	MemoryAllocation allocation = dedicatedMemory->allocate(
		imageMemoryReqs.size,
		imageMemoryReqs.alignment,
		VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
		imageMemoryReqs.memoryTypeBits
	);
	VK_VALIDATE_RESULT(vkBindImageMemory(info.gpu.device, texture->image, allocation.page->memoryHandle, allocation.baseAddressAligned));

	// Figure out the aspect mask
	VkImageAspectFlags aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	if (static_cast<uint32_t>(info.usageBits) & static_cast<uint32_t>(ImageUsageBits::COLOR_ATTACHMENT_BIT)) {
		aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	}

	if (static_cast<uint32_t>(info.usageBits) & static_cast<uint32_t>(ImageUsageBits::DEPTH_STENCIL_ATTACHMENT_BIT)) {
		if (VulkanContext::hasDepth(texture->format)) {
			aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
		}

		if (VulkanContext::hasStencil(texture->format)) {
			aspectMask = aspectMask | VK_IMAGE_ASPECT_STENCIL_BIT;
		}
	}
	texture->aspectMask = aspectMask;

	// Create a command buffer on the transfer queue if available otherwise
	// the graphics queue is chosen
	// NOTE: look into creating texture and not waiting for the queue to finish the work
	TemporaryCommandBuffer transferBuffer = info.gpu.createTemporaryBuffer(
		info.gpu.graphicsQueue,
		VK_COMMAND_BUFFER_LEVEL_PRIMARY
	);
	transferBuffer.begin();

	// All of the required copy commands based on mip availability
	// creation flags etc.
	std::vector<VkBufferImageCopy> copyCommands;

	VkBuffer stageBuffer = VK_NULL_HANDLE;
	MemoryAllocation stageRegion;

	// If we have initial data, create a staging buffer and copy it over
	if (data != nullptr && dataSize > 0) {
		VkMemoryAllocateInfo memAllocateInfo = {};
		memAllocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		memAllocateInfo.pNext = nullptr;
		VkMemoryRequirements memoryReqs;

		VkBufferCreateInfo bufferInfo = {};
		bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
		bufferInfo.size = dataSize;
		bufferInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
		bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		VK_VALIDATE_RESULT(vkCreateBuffer(info.gpu.device, &bufferInfo, nullptr, &stageBuffer));

		vkGetBufferMemoryRequirements(info.gpu.device, stageBuffer, &memoryReqs);
		//stageRegion = stagingMemory->allocateRegion(memoryReqs.size, memoryReqs.alignment, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, memoryReqs.memoryTypeBits);
		//VK_VALIDATE_RESULT(vkBindBufferMemory(info.gpu.device, stageBuffer, stageRegion.block->memoryHandle, stageRegion.offset));

		stageRegion = stagingMemory->allocate(
			memoryReqs.size,
			memoryReqs.alignment,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
			memoryReqs.memoryTypeBits
		);
		VK_VALIDATE_RESULT(vkBindBufferMemory(info.gpu.device, stageBuffer, stageRegion.page->memoryHandle, stageRegion.baseAddressAligned));

		void* ptr;
		ptr = stageRegion.mapMemory();
		{
			memcpy(ptr, data, dataSize);
		}
		stageRegion.unmapMemory();


		// If the image source is the native texture, check if it has preloaded mip levels
		if (nativeTex) {
			if (nativeTex->hasMips()) {
				uint32_t offset = 0;

				for (uint32_t i = 0; i < texture->mipLevels; i++) {
					VkBufferImageCopy bufferCopyRegion = {};
					bufferCopyRegion.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
					bufferCopyRegion.imageSubresource.mipLevel = i;
					bufferCopyRegion.imageSubresource.baseArrayLayer = 0;
					bufferCopyRegion.imageSubresource.layerCount = 1;
					bufferCopyRegion.imageExtent.width = static_cast<uint32_t>(nativeTex->getMipExtents(i).x);
					bufferCopyRegion.imageExtent.height = static_cast<uint32_t>(nativeTex->getMipExtents(i).y);
					bufferCopyRegion.imageExtent.depth = 1;
					bufferCopyRegion.bufferOffset = offset;

					copyCommands.push_back(bufferCopyRegion);

					offset += static_cast<uint32_t>(nativeTex->getMipSize(i));
				}
			}
		}

		// If we don't have any preloaded mips, just copy over the base image
		if (copyCommands.size() == 0) {
			VkBufferImageCopy bufferCopyRegion = {};
			bufferCopyRegion.imageSubresource.aspectMask = aspectMask;
			bufferCopyRegion.imageSubresource.mipLevel = 0;
			bufferCopyRegion.imageSubresource.baseArrayLayer = 0;
			bufferCopyRegion.imageSubresource.layerCount = texture->layerLevels;
			bufferCopyRegion.imageExtent.width = texture->width;
			bufferCopyRegion.imageExtent.height = texture->height;
			bufferCopyRegion.imageExtent.depth = texture->depth;

			copyCommands.push_back(bufferCopyRegion);
		}

		// Transition the whole image to transfer dst
		texture->transitionImage(
			transferBuffer.getHandle(),
			VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
		);

		// Copy over the specified regions
		vkCmdCopyBufferToImage(
			transferBuffer.getHandle(),
			stageBuffer,
			texture->image,
			texture->imageLayout,
			copyCommands.size(),
			&copyCommands[0]
		);

		// If mipmap generation was requested and valid and there we no preloaded mips
		if (info.generateMipmaps && texture->mipLevels > 1 && copyCommands.size() == 1) {
			VkImageSubresourceRange mipSubRange = {};
			mipSubRange.aspectMask = aspectMask;
			mipSubRange.baseMipLevel = 0;
			mipSubRange.baseArrayLayer = 0;
			mipSubRange.levelCount = 1;
			mipSubRange.layerCount = 1;

			//Transition the first layer to transfer src
			texture->transitionImage(transferBuffer.getHandle(), VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, mipSubRange);

			for (int i = 1; i < texture->mipLevels; i++) {
				VkImageBlit imageBlit = {};

				imageBlit.srcSubresource.aspectMask = aspectMask;
				imageBlit.srcSubresource.layerCount = info.arrayLevels;
				imageBlit.srcSubresource.mipLevel = i - 1;
				imageBlit.srcOffsets[0] = { 0, 0, 0 };
				imageBlit.srcOffsets[1] = { 
					std::max(int32_t(texture->width >> (i - 1)), 1),
					std::max(int32_t(texture->height >> (i - 1)), 1),
					1 
				};

				imageBlit.dstSubresource.aspectMask = aspectMask;
				imageBlit.dstSubresource.layerCount = info.arrayLevels;
				imageBlit.dstSubresource.mipLevel = i;
				imageBlit.dstOffsets[0] = { 0, 0, 0 };
				imageBlit.dstOffsets[1] = {
					std::max(int32_t(texture->width >> (i)), 1),
					std::max(int32_t(texture->height >> (i)), 1),
					1
				};

				mipSubRange.aspectMask = aspectMask;
				mipSubRange.baseMipLevel = i;
				mipSubRange.levelCount = 1;
				mipSubRange.layerCount = info.arrayLevels;

				// NOTE this still needs to be changed as 
				// each image subresource can have a separate layout
				// in this case, we know the layouts at this point

				// NOTE NOTE is per layer/mip layout tracking needed?
				/*texture.transitionImageInternal(
					transferBuffer.getHandle(),
					VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
					VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
					mipSubRange
				);*/

				// Blit from previous level
				vkCmdBlitImage(
					transferBuffer.getHandle(),
					texture->image,
					VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
					texture->image,
					VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
					1,
					&imageBlit,
					VK_FILTER_LINEAR);

				// Transiton current mip level to transfer source for read in next iteration
				texture->transitionImageInternal(
					transferBuffer.getHandle(),
					VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
					VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
					mipSubRange
				);
			}


			// After mip generation, transition the whole image to transfer dst
			texture->transitionImageInternal(
				transferBuffer.getHandle(),
				VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
				VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
			);
		}
	}

	// Transition the image to the final state 
	texture->transitionImage(
		transferBuffer.getHandle(),
		static_cast<VkImageLayout>(info.initialLayout)
	);


	// Submit command and dispose of the command buffer after everything is done
	transferBuffer.end();
	transferBuffer.submit();
	transferBuffer.dispose();

	// Clear the staging memory, if we used it 
	if (stagingMemory) {
		stagingMemory->clearAllPages();
		//stagingMemory->removeEmptyPages();
	}

	// If we used a staging buffer to transfer data, dispose of it 
	if (stageBuffer != VK_NULL_HANDLE) {
		vkDestroyBuffer(info.gpu.device, stageBuffer, nullptr);
	}

	// Create a sampler for the texture
	VkSamplerCreateInfo samplerInfo = {};
	samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
	samplerInfo.flags = 0;
	samplerInfo.pNext = nullptr;
	samplerInfo.magFilter = static_cast<VkFilter>(info.samplerInfo.magFilter);
	samplerInfo.minFilter = static_cast<VkFilter>(info.samplerInfo.minFilter);
	samplerInfo.mipmapMode = static_cast<VkSamplerMipmapMode>(info.samplerInfo.mipmapMode);
	samplerInfo.addressModeU = static_cast<VkSamplerAddressMode>(info.samplerInfo.addressModeU);
	samplerInfo.addressModeV = static_cast<VkSamplerAddressMode>(info.samplerInfo.addressModeV);
	samplerInfo.addressModeW = static_cast<VkSamplerAddressMode>(info.samplerInfo.addressModeW);
	samplerInfo.mipLodBias = info.samplerInfo.mipLodBias;
	samplerInfo.compareEnable = static_cast<VkBool32>(info.samplerInfo.compareEnable);
	samplerInfo.compareOp = static_cast<VkCompareOp>(info.samplerInfo.compareOp);
	samplerInfo.minLod = info.samplerInfo.minLod;
	samplerInfo.maxLod = (float)texture->mipLevels;
	samplerInfo.maxAnisotropy = info.samplerInfo.maxAnisotropy;
	samplerInfo.anisotropyEnable = info.samplerInfo.anisotropyEnable;
	samplerInfo.borderColor = static_cast<VkBorderColor>(info.samplerInfo.borderColor);
	VK_VALIDATE_RESULT(vkCreateSampler(info.gpu.device, &samplerInfo, nullptr, &texture->sampler));

	// Create a default view
	VkImageViewCreateInfo view = {};
	view.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	view.pNext = nullptr;
	view.viewType = (info.createFlags & VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT) ? VK_IMAGE_VIEW_TYPE_CUBE : (info.arrayLevels == 1) ? VK_IMAGE_VIEW_TYPE_2D : VK_IMAGE_VIEW_TYPE_2D_ARRAY;
	view.format = texture->format;
	view.subresourceRange = { texture->aspectMask, 0, 1, 0, 1 };
	view.subresourceRange.levelCount = texture->mipLevels;
	view.subresourceRange.layerCount = info.arrayLevels;
	view.image = texture->image;
	VK_VALIDATE_RESULT(vkCreateImageView(info.gpu.device, &view, nullptr, &texture->imageView));


	texture->viewType = view.viewType;
	texture->descriptorImageInfo = {};
	texture->descriptorImageInfo.imageLayout = texture->imageLayout;
	texture->descriptorImageInfo.imageView = texture->imageView;
	texture->descriptorImageInfo.sampler = texture->sampler;
	texture->memoryDescriptor = allocation;
}

