#include "Asset.h"

#include "VkTexture.h"
#include "Mesh.h"
#include "VkBuffer.h"
#include "refactor\Asset.h"
#include "FileIO.h"

std::vector<std::string> grail::assets::textureFormats = {
			"png",
			"jpg",
			"jpeg",
			"tga",
			"dds",
			"hdr",
			"ktx",
			"kmg"
};

std::vector<std::string> grail::assets::meshFormats = {
			"obj",
			"fbx",
			"gltf"
};

std::vector<std::string> grail::assets::scriptFormats = {
			"as"
};

const std::string& grail::Asset::getAssetName() const {
	return name;
}

const std::string& grail::Asset::getAssetUUIDString() const {
	return idString;
}

grail::AssetDescriptor* grail::Asset::getDescriptor() const {
	return descriptor;
}

grail::uuid grail::Asset::getAssetUUID() const {
	return id;
}