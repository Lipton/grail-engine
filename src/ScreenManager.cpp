#include "ScreenManager.h"

#include "Screen.h"

grail::ScreenManager::ScreenManager() {
}

grail::ScreenManager::~ScreenManager() {
}

void grail::ScreenManager::render(float delta) {
	this->currentScreen->render(delta);
}

void grail::ScreenManager::resize(int width, int height) {
	this->currentScreen->resize(width, height);
}

void grail::ScreenManager::cursorMoved(float x, float y) {
	this->currentScreen->inputProcessor->mouseMoved(x, y);
}

void grail::ScreenManager::keyPressEvent(int keycode, int action) {
	this->currentScreen->inputProcessor->keyEvent(keycode, action);
}

void grail::ScreenManager::mousePressEvent(int keyCode, int action) {
	this->currentScreen->inputProcessor->mouseEvent(keyCode, action);
}

void grail::ScreenManager::dropCallback(int count, const char ** paths) {
	this->currentScreen->inputProcessor->filesDropped(count, paths);
}

void grail::ScreenManager::charCallback(unsigned int c, int mods) {
	this->currentScreen->inputProcessor->charTyped(c, mods);
}

void grail::ScreenManager::setScreen(const std::string& screen) {
	std::unordered_map<std::string, Screen*>::const_iterator item = screenMap.find(screen);

	if (item == screenMap.end()) {
		GRAIL_LOG(ERROR, "SCREEN MANAGER SET") << "Screen \"" << screen << "\" was not found";
		return;
	}

	if (lastScreen != nullptr)
		lastScreen->hide();

	if (currentScreen != nullptr)
		lastScreen = currentScreen;

	currentScreen = screenMap[screen];
	currentScreen->show();
}

void grail::ScreenManager::registerScreen(const std::string& name, Screen & screen) {
	std::unordered_map<std::string, Screen*>::const_iterator item = screenMap.find(name);

	if (item != screenMap.end()) {
		GRAIL_LOG(ERROR, "SCREEN MANAGER REGISTER") << "Screen \"" << name << "\" already exists";
		return;
	}

	screenMap.insert({ name, &screen });
	screen.screenManager = this;

	GRAIL_LOG(INFO, "SCREEN MANAGER") << "Registered screen \"" << name << "\"";

	screen.innerInit();
}

void grail::ScreenManager::deregisterScreen(const std::string& screen) {
	std::unordered_map<std::string, Screen*>::const_iterator item = screenMap.find(screen);

	if (item == screenMap.end()) {
		GRAIL_LOG(ERROR, "SCREEN MANAGER DEREGISTER") << "Screen \"" << screen << "\" was not found";
		return;
	}

	if (currentScreen == screenMap[screen]) {
		GRAIL_LOG(ERROR, "SCREEN MANAGER DEREGISTER") << "Cannot deregister a currently active screen";
		return;
	}

	//screenMap[screen]->innerDispose();
	screenMap.erase(screen);
	GRAIL_LOG(INFO, "SCREEN MANAGER") << "Deregistered screen \"" << screen << "\"";
}