#ifndef GRAIL_SCRIPT
#define GRAIL_SCRIPT

#include "Common.h"

#include <angelscript.h>
#include <scriptstdstring/scriptstdstring.h>
#include <scriptbuilder/scriptbuilder.h>

namespace grail {
	class SceneNode;

	struct ScriptError {
		std::string message;
		int row;
		int col;
	};

	enum class ScriptVarType : int {
		UNKNOWN = 0,
		INT8,
		INT16,
		INT,
		INT64,
		UINT8,
		UINT16,
		UINT,
		UINT64,
		FLOAT,
		DOUBLE,
		BOOL,
		STRING,
		VEC2,
		VEC3,
		VEC4,
		NODE
	};

	struct SerializedScriptVar {
		ScriptVarType type = ScriptVarType::UNKNOWN;
		std::string name = "Undefined";
		void* address = nullptr;

		// Might not be the best way to handle this, think about that
		void* value = nullptr;
	};

	// Script class that holds all of the relevant information to the script
	class Script {
		friend class ScriptProvider;
	public:
		std::string getScriptSource();
		std::string getSectionName();
		std::string getName();

		uint32_t getGlobalPosition();

		asIScriptEngine* getEngine();
		asIScriptModule* getModule();

		bool isCompiled();

		void setScriptSource(const std::string& source);

		std::string name;
	private:
		std::string scriptSource;
		std::string sectionName;
		// Line at which the script starts in the global script module
		uint32_t globalPosition;

		asIScriptEngine* engine;
		asIScriptModule* scriptModule;

		bool isScriptCompiled = false;

		/*void compile();
		void setup(SceneNode* node);

		void executeFunc();

		void dispose();*/


	};
}

#endif