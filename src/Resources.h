#ifndef GRAIL_RESOURCES_H
#define GRAIL_RESOURCES_H

#include "VkTexture.h"
#include "Mesh.h"
#include "SceneNode.h"
#include "Shader.h"
#include "VkBuffer.h"
#include "Asset.h"
#include "UUID.h"

#include <map>
#include <string>
#include <mutex>

#include "TextureAssetImporter.h"

namespace grail {
	class VertexLayout;
	class Renderer;
	class Material;
	class MaterialInstance;
	class Buffer;
	class FrameGraph;
	class MaterialDescription;
	class Script;
	class MemoryAllocator;

	struct TextureEntry {
		VkTexture texture;

		uint32_t key;

		std::string name;
	};

	class Resources {
		friend class Renderer;
		friend class MeshLoader;
		friend class Editor;
		friend class SceneLoader;
		friend class TextureAssetImporter;
		friend class GrailEditor;

	public:
		static VkTexture* getTexture(const std::string& identifier);
		static Mesh* getMesh(const std::string& identifier);

		static VkTexture* loadTexture(std::string path, TextureCreateInfo& createInfo);
		static VkTexture* loadTexture(std::string path);

		//TEST

		// Same as load texture, but uses a mutex lock before inserting into the map, to be used with a thread pool to concurently load textures
		static VkTexture* loadTextureAsync(std::string path, TextureCreateInfo& createInfo);

		// Same as load texture, but uses a mutex lock before inserting into the map, to be used with a thread pool to concurently load textures
		static VkTexture* loadTextureAsync(std::string path);
		//TEST

		static VkTexture* createTexture(std::string name, uint32_t width, uint32_t height, TextureCreateInfo& createInfo);
		static VkTexture* createTexture(std::string name, uint32_t width, uint32_t height);

		static VkTexture* createTexture(std::string name, uint32_t width, uint32_t height, void* initialData, uint32_t size, TextureCreateInfo& createInfo);
		static VkTexture* createTexture(std::string name, uint32_t width, uint32_t height, void* initialData, uint32_t size);

		// Loads and stores all of the meshes, if the file has not been loaded before
		static void loadModel(const std::string& path, VertexLayout* vertexLayout = nullptr, bool insetInScene = false);

		static SceneNode* loadMesh(std::string path, VertexLayout* vertexLayout, Renderer* renderer, bool loadMaterials = true, std::string customMaterialFile = "");
		static void loadBaseMesh(std::string path, VertexLayout* vertexLayout, Renderer* renderer, bool loadMaterials = true, std::string customMaterialFile = "");

		// Create and store a new base material
		// Returns the base material under given name if already exists
		static MaterialDescription* loadMaterial(const std::string& name, const std::string& descriptorPath, FrameGraph* graph);
		
		// Get a material based on name, returns nullptr if not found
		static MaterialDescription* getMaterial(const std::string& name);

		// Create and store a buffer, memory region defaults to device local buffer memory
		// Returns a buffer under given name if it exists
		static Buffer* createBuffer(const std::string& name, BufferCreateInfo info, MemoryAllocator* memoryRegion = getBufferMemoryAllocator());

		// Retrieves a buffer by given name, returns nullptr if it does not exist
		static Buffer* getBuffer(const std::string& name);

		static Mesh* createMesh(std::string name, VertexLayout* vertexLayout, std::vector<float>& vertices, std::vector<uint32_t>& indices);
		static Mesh* createMesh(std::string name, VertexLayout* vertexLayout, std::vector<float>& vertices);

		// Create an empty script object and initialize it to the default state
		// Returns a scripter under the given name if already exists
		static Script* createScript(const std::string& name);

		// Create an empty script object and fill it with the given source
		// Returns a scripter under the given name if already exists
		static Script* createScript(const std::string& name, const std::string& source);

		static Script* getScript(const std::string& name);

		static VkShaderModule loadShader(std::string path);
		static class Shader* loadSPIRVShader(std::string path, std::vector<std::string>& defines);
		static class Shader* loadSPIRVShader(std::string path);

		static void setStagingMemoryAllocator(MemoryAllocator* allocator);
		static void setTextureMemoryAllocator(MemoryAllocator* allocator);
		static void setBufferMemoryAllocator(MemoryAllocator* allocator);
		static void setUniformBufferMemoryAllocator(MemoryAllocator* allocator);

		static MemoryAllocator* getStagingMemoryAllocator();
		static MemoryAllocator* getTextureMemoryAllocator();
		static MemoryAllocator* getBufferMemoryAllocator();
		static MemoryAllocator* getUniformBufferMemoryAllocator();

		//static AssetRef<TextureAsset> loadTextureAsset(TextureAssetDescriptor& descriptor, const std::string& basePath);
		static AssetRef<Asset> loadAsset(const std::string& path);

		static AssetRef<TextureAsset> loadTextureAsset(TextureAssetDescriptor* descriptor);
		
		static void unloadAsset(const AssetRef<Asset>& asset);
	private:
		static bool parseAsset(AssetRef<Asset>& asset, const std::string& path);

		static MemoryAllocator* stagingMemoryAllocator;
		static MemoryAllocator* textureMemoryAllocator;
		static MemoryAllocator* bufferMemoryAllocator;
		static MemoryAllocator* uniformBufferMemoryAllocator;

		static std::unordered_map<uint32_t, void*> meshAssets;

		static std::unordered_map<uint32_t, VkTexture*> textures;
		static std::unordered_map<uint32_t, SceneNode*> meshNodes;
		static std::unordered_map<uint32_t, VkShaderModule> shaders;
		static std::unordered_map<uint32_t, Mesh*> meshes;
		static std::unordered_map<uint32_t, Shader*> nshaders;
		static std::unordered_map<uint32_t, MaterialDescription*> materials;
		static std::unordered_map<uint32_t, Buffer*> buffers;
		static std::unordered_map<uint32_t, Script*> scripts;

		static std::unordered_map<uuid, AssetRef<Asset>> assets;
		static std::mutex assetsLock;

		/*static std::unordered_map<uuid, std::shared_ptr<AssetBase>> textureAssets;
		static void unloadAsset(AssetRef<TextureAsset> asset);
		*/

		Resources& operator = (const Resources&) = delete;
		Resources(const Resources&) = delete;
		Resources() = default;
	};
}

#endif