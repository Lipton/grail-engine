#ifndef GRAIL_FIRST_PERSON_CAMERA_CONTROLLER_H
#define GRAIL_FIRST_PERSON_CAMERA_CONTROLLER_H

#include "Common.h"

namespace grail {
	class FirstPersonCameraController {
	private:
		class PerspectiveCamera* camera;

		float degreesPerPixel;
		float movementVelocity;
		float lastMouseX;
		float lastMouseY;
		float mouseDeltaX;
		float mouseDeltaY;
		float speedMod;
		float velocity;

		bool firstPass;
	public:
		FirstPersonCameraController(PerspectiveCamera* camera);

		void update(float delta, bool updateFrustrum = false);
		void updateMouseMoved(float x, float y);
		void resetMouse();

		void setVelocity(float velocity);
		float getVeloctiy() const;

		void setMouseSensitivity(float degreesPerPixel);
		float getMouseSensitivity() const;

		void setCrouchModifier(float crouchModifier);
		float getCrouchModifier() const;
	};
}

#endif