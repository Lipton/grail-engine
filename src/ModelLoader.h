#ifndef GRAIL_MODEL_LOADER_H
#define GRAIL_MODEL_LOADER_H

#include "Common.h"
#include "VertexLayout.h"


#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

namespace grail {
	struct MeshDataR {
		std::vector<float> vertices;
		std::vector<uint32_t> indices;

		std::string diffusePath = "none";

		float radius;

		glm::vec3 color = glm::vec3(1, 1, 1);

		uint32_t indexOffset = 0;
	};
	
	class ModelLoader {
	public:
		static std::vector<MeshDataR> loadFile(VertexLayout& layout, const std::string& path, const std::string& texturePathPrefix);
		static std::vector<MeshDataR> loadFiles(VertexLayout& layout, const std::string& path, const std::string& texturePathPrefix);
	private:
		static MeshDataR processNode(aiNode* mesh, const aiScene* scene);
	};
}

#endif