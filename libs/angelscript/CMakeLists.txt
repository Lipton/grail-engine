cmake_minimum_required(VERSION 3.5)

project(angelscript)

option(BUILD_SHARED_LIBS "Build shared library" OFF)
option(AS_NO_EXCEPTIONS "Disable exception handling in script context" OFF)

if(APPLE)
    option(BUILD_FRAMEWORK "Build Framework bundle for OSX" OFF)
endif()

file(READ include/angelscript.h ANGELSCRIPT_H)
string(REGEX MATCH "#define ANGELSCRIPT_VERSION_STRING \"([0-9]*).([0-9]*).([0-9]*)" ANGELSCRIPT_VERSION_REGEX ${ANGELSCRIPT_H})
set(ANGELSCRIPT_VERSION_MAJOR ${CMAKE_MATCH_1})
set(ANGELSCRIPT_VERSION_MINOR ${CMAKE_MATCH_2})
set(ANGELSCRIPT_VERSION_PATCH ${CMAKE_MATCH_3})
set(PROJECT_VERSION ${ANGELSCRIPT_VERSION_MAJOR}.${ANGELSCRIPT_VERSION_MINOR}.${ANGELSCRIPT_VERSION_PATCH})

message(STATUS "Configuring angelscript ${PROJECT_VERSION}")

find_package(Threads)

set(ANGELSCRIPT_HEADERS
    include/angelscript.h
    src/as_array.h
    src/as_builder.h
    src/as_bytecode.h
    src/as_callfunc.h
    src/as_compiler.h
    src/as_config.h
    src/as_configgroup.h
    src/as_context.h
    src/as_criticalsection.h
    src/as_datatype.h
    src/as_debug.h
    src/as_generic.h
    src/as_map.h
    src/as_memory.h
    src/as_module.h
    src/as_objecttype.h
    src/as_outputbuffer.h
    src/as_parser.h
    src/as_property.h
    src/as_restore.h
    src/as_scriptcode.h
    src/as_scriptengine.h
    src/as_scriptfunction.h
    src/as_scriptnode.h
    src/as_scriptobject.h
    src/as_string.h
    src/as_string_util.h
    src/as_texts.h
    src/as_thread.h
    src/as_tokendef.h
    src/as_tokenizer.h
    src/as_typeinfo.h
    src/as_variablescope.h
    include/scriptstdstring/scriptstdstring.h
    include/scriptbuilder/scriptbuilder.h
    include/scriptany/scriptany.h
    include/scripthandle/scripthandle.h
    include/scriptarray/scriptarray.h
    include/autowrapper/aswrappedcall.h
    include/scripthelper/scripthelper.h
)

set(ANGELSCRIPT_SOURCE
    src/as_atomic.cpp
    src/as_builder.cpp
    src/as_bytecode.cpp
    src/as_callfunc.cpp
    src/as_callfunc_mips.cpp
    src/as_callfunc_x86.cpp
    src/as_callfunc_x64_gcc.cpp
    src/as_callfunc_x64_msvc.cpp
    src/as_callfunc_x64_mingw.cpp
    src/as_compiler.cpp
    src/as_configgroup.cpp
    src/as_context.cpp
    src/as_datatype.cpp
    src/as_gc.cpp
    src/as_generic.cpp
    src/as_globalproperty.cpp
    src/as_memory.cpp
    src/as_module.cpp
    src/as_objecttype.cpp
    src/as_outputbuffer.cpp
    src/as_parser.cpp
    src/as_restore.cpp
    src/as_scriptcode.cpp
    src/as_scriptengine.cpp
    src/as_scriptfunction.cpp
    src/as_scriptnode.cpp
    src/as_scriptobject.cpp
    src/as_string.cpp
    src/as_string_util.cpp
    src/as_thread.cpp
    src/as_tokenizer.cpp
    src/as_typeinfo.cpp
    src/as_variablescope.cpp
    include/scriptstdstring/scriptstdstring.cpp
    include/scriptstdstring/scriptstdstring_utils.cpp
    include/scriptbuilder/scriptbuilder.cpp
    include/scriptany/scriptany.cpp
    include/scripthandle/scripthandle.cpp
    include/scriptarray/scriptarray.cpp
    include/scripthelper/scripthelper.cpp
)

if(MSVC AND CMAKE_CL_64)
    enable_language(ASM_MASM)
    if(CMAKE_ASM_MASM_COMPILER_WORKS)
        set(ANGELSCRIPT_SOURCE ${ANGELSCRIPT_SOURCE} src/as_callfunc_x64_msvc_asm.asm)
    else()
        message(FATAL ERROR "MSVC x86_64 target requires a working assembler")
    endif()
endif()

if(${CMAKE_SYSTEM_PROCESSOR} MATCHES "^arm")
    enable_language(ASM)
    if(CMAKE_ASM_COMPILER_WORKS)
        set(ANGELSCRIPT_SOURCE ${ANGELSCRIPT_SOURCE} src/as_callfunc_arm.cpp src/as_callfunc_arm_gcc.S)
        set_property(SOURCE src/as_callfunc_arm_gcc.S APPEND PROPERTY COMPILE_FLAGS " -mimplicit-it=always")
    else()
        message(FATAL ERROR "ARM target requires a working assembler")
    endif()
endif()

if(MSVC)
    set(CMAKE_DEBUG_POSTFIX "d")  
endif()

if(NOT BUILD_FRAMEWORK)
    set(ANGELSCRIPT_LIBRARY_NAME angelscript)
else()
    set(ANGELSCRIPT_LIBRARY_NAME Angelscript) # OS X frameworks should have capitalized name
    set(BUILD_SHARED_LIBS TRUE)
endif()
set(ANGELSCRIPT_LIBRARY_NAME ${ANGELSCRIPT_LIBRARY_NAME} CACHE STRING "" FORCE)

add_library(${ANGELSCRIPT_LIBRARY_NAME} ${ANGELSCRIPT_SOURCE} ${ANGELSCRIPT_HEADERS})

target_include_directories(${ANGELSCRIPT_LIBRARY_NAME} PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/../../include)

if(MSVC)
    target_compile_definitions(${ANGELSCRIPT_LIBRARY_NAME} PRIVATE -D_CRT_SECURE_NO_WARNINGS)    
endif()

target_compile_definitions(${ANGELSCRIPT_LIBRARY_NAME} PRIVATE -DANGELSCRIPT_EXPORT -D_LIB)

if(AS_NO_EXCEPTIONS)
	target_compile_definitions(${ANGELSCRIPT_LIBRARY_NAME} PRIVATE AS_NO_EXCEPTIONS)
endif()

# Fix x64 issues on Linux
if("${CMAKE_SYSTEM_PROCESSOR}" STREQUAL "x86_64" AND UNIX AND NOT APPLE)
    target_compile_options(${ANGELSCRIPT_LIBRARY_NAME} PRIVATE -fPIC)
endif()

# Don't override the default library output path to avoid conflicts when building for multiple target platforms
#set(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/../../lib)
target_link_libraries(${ANGELSCRIPT_LIBRARY_NAME} Threads::Threads)

set_target_properties(${ANGELSCRIPT_LIBRARY_NAME} PROPERTIES VERSION ${PROJECT_VERSION})

if(BUILD_FRAMEWORK)
    set_target_properties(${ANGELSCRIPT_LIBRARY_NAME} PROPERTIES
        FRAMEWORK TRUE
        FRAMEWORK_VERSION ${PROJECT_VERSION}
        MACOSX_FRAMEWORK_IDENTIFIER com.angelcode.Angelscript
        MACOSX_FRAMEWORK_SHORT_VERSION_STRING ${PROJECT_VERSION}
        MACOSX_FRAMEWORK_BUNDLE_VERSION ${PROJECT_VERSION}
        XCODE_ATTRIBUTE_INSTALL_PATH "@rpath"
        PUBLIC_HEADER ../../include/angelscript.h
    )
endif()

if(MSVC)
    set_target_properties(${ANGELSCRIPT_LIBRARY_NAME} PROPERTIES COMPILE_FLAGS "/MP")
endif()

# Don't override the default runtime output path to avoid conflicts when building for multiple target platforms
#set(RUNTIME_OUTPUT_DIRECTORY ${PROJECT_SOURCE_DIR}/../../bin)

#See https://cmake.org/cmake/help/latest/manual/cmake-packages.7.html#creating-packages for a detailed explanation about this part
install(TARGETS ${ANGELSCRIPT_LIBRARY_NAME} EXPORT AngelscriptTargets
	RUNTIME DESTINATION bin
	LIBRARY DESTINATION lib
	ARCHIVE DESTINATION lib
	INCLUDES DESTINATION include
)

install(FILES
	${CMAKE_CURRENT_SOURCE_DIR}/../../include/angelscript.h
	DESTINATION include
	COMPONENT Devel
)

include(CMakePackageConfigHelpers)
write_basic_package_version_file(
	"${CMAKE_CURRENT_BINARY_DIR}/Angelscript/AngelscriptConfigVersion.cmake"
	VERSION ${PROJECT_VERSION}
	COMPATIBILITY AnyNewerVersion
)

export(EXPORT AngelscriptTargets
	FILE "${CMAKE_CURRENT_BINARY_DIR}/Angelscript/AngelscriptTargets.cmake"
	NAMESPACE Angelscript::
)
configure_file(cmake/AngelscriptConfig.cmake
	"${CMAKE_CURRENT_BINARY_DIR}/Angelscript/AngelscriptConfig.cmake"
	COPYONLY
)

set(ConfigPackageLocation lib/cmake/Angelscript)
install(EXPORT AngelscriptTargets
	FILE AngelscriptTargets.cmake
	NAMESPACE Angelscript::
	DESTINATION ${ConfigPackageLocation}
)
install(
	FILES
		cmake/AngelscriptConfig.cmake
		"${CMAKE_CURRENT_BINARY_DIR}/Angelscript/AngelscriptConfigVersion.cmake"
	DESTINATION ${ConfigPackageLocation}
	COMPONENT Devel
)

