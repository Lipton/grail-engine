#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec2 uvs;

layout (binding = 0) uniform sampler2D g_vel;

layout (std140, push_constant) uniform pushConstants {
    vec2 K;
} constants;

layout (location = 0) out vec2 outFragColor;

// CONSTANTS
const vec2 VHALF =  vec2(0.5, 0.5);
const vec2 VONE  =  vec2(1.0, 1.0);
const vec2 VTWO  =  vec2(2.0, 2.0);

const ivec2 IVZERO = ivec2(0, 0);
const ivec2 IVONE  = ivec2(1, 1);

const vec4 GRAY = vec4(0.5, 0.5, 0.5, 1.0);

// Bias/scale helper functions
vec2 readBiasScale(vec2 v)
{
    return v;
}

vec2 writeBiasScale(vec2 v)
{
    return v;
}


void main() {
    int u_iK = 20;

   ivec2 ivTileCorner = ivec2(gl_FragCoord.xy) * u_iK;
    ivec2 ivTexSizeMinusOne = textureSize(g_vel, 0) - IVONE;

    outFragColor = vec2(0.0);

    float fMaxMagnitudeSquared = 0.0;

    for(int s = 0; s < u_iK; ++s)
    {
        for(int t = 0; t < u_iK; ++t)
        {
            vec2 vVelocity = readBiasScale(texelFetch(g_vel,
                clamp(ivTileCorner + ivec2(s, t), IVZERO, ivTexSizeMinusOne),
                0).rg);

            float fMagnitudeSquared = dot(vVelocity, vVelocity);
            if(fMaxMagnitudeSquared < fMagnitudeSquared)
            {
                outFragColor.rg = writeBiasScale(vVelocity);
                fMaxMagnitudeSquared = fMagnitudeSquared;
            }
        }
    }
}