#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable


layout (binding = 0) uniform sampler2D g_depth;
layout (binding = 1) uniform sampler2D g_ssao;

layout (std140, push_constant) uniform pushConstants {
    vec4 size;
} constants;

layout (location = 0) in vec2 uvs;

layout (location = 0) out vec4 outFragColor;

void main() {
    vec2 texelSize = 1.0 / vec2(textureSize(g_ssao, 0));
    float result = 0.0;
    for (int x = -3; x < 3; ++x) {
        for (int y = -3; y < 3; ++y) {
            vec2 offset = vec2(float(x), float(y)) * texelSize;
            result += texture(g_ssao, uvs + offset).r;
        }
    }

    outFragColor.rgb = vec3(result / (6.0 * 6.0));
    outFragColor.a = 1.0;
}