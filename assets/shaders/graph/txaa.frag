#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec2 uvs;


layout (binding = 0) uniform sampler2D a_color;
layout (binding = 1) uniform sampler2D a_history;
layout (binding = 2) uniform sampler2D a_velocity;
layout (binding = 3) uniform sampler2D a_depth;

layout (std140, push_constant) uniform pushConstants {
    vec4 texelSize;
    vec4 subsample;
    mat4 projection;
    vec2 taaEnabled;
} constants;

layout (location = 0) out vec4 outFragColor;

vec2 getVelocity(vec2 pixel) {
    float bestDepth = 90.0;
	vec2 bestPixel = vec2(pixel);


	for (int i = -1; i <= 1; ++i) {
		for (int j = -1; j <= 1; ++j) {
			vec2 curPixel = pixel + vec2(i, j) * constants.texelSize.xy;
			float depth = texture(a_depth, curPixel).r;
		
			if (depth < bestDepth) {
				bestDepth = depth;
				bestPixel = curPixel;
			}
		}
	}

	return texture(a_velocity, bestPixel).xy;
}


#define TAA_COLOR_NEIGHBORHOOD_SAMPLE_SPREAD 1.0
#define TAA_FRAGMENT_MOTION_HISTORY_DECAY 0.95

//6.28318530718
//1.28318530718 for ao stability over shadows
#define TAA_MINIMUM_MOTION_NUDGE 1.28318530718
#define TAA_MINIMUM_MOTION_SENSITIVITY 0.05
#define TAA_MAXIMUM_MOTION_NUDGE 0.5
#define TAA_MAXIMUM_MOTION_SENSITIVITY 0.055

// 0
#define TAA_FINAL_BLEND_FACTOR 0.95

// 2
#define TAA_FINAL_BLEND_STATIC_FACTOR 0.99
#define TAA_FINAL_BLEND_DYNAMIC_FACTOR 0.8
#define TAA_MOTION_AMPLIFICATION 300.0

#define TAA_REMOVE_COLOR_SAMPLE_JITTER 1
#define TAA_USE_EXPERIMENTAL_OPTIMIZATIONS 1
#define TAA_TONEMAP_COLOR_AND_HISTORY_SAMPLES 1
#define TAA_COLOR_NEIGHBORHOOD_SAMPLE_PATTERN 2
#define TAA_DILATE_MOTION_VECTOR_SAMPLE 1
#define TAA_CLIP_HISTORY_SAMPLE 1
#define TAA_STORE_FRAGMENT_MOTION_HISTORY 1
#define TAA_SHARPEN_FACTOR 1
#define TAA_SHARPEN_OUTPUT 2
#define TAA_FINAL_BLEND_METHOD 2
#define TAA_USE_GATHER_FOR_DEPTH_SAMPLE 1

#define TAA_ENABLE 1

float rcp(float value) {
    return 1.0 / value;
}

float getMaximumElement(in vec3 value) {
    return max(max(value.x, value.y), value.z);
}

vec4 map(in vec4 color) {
    return vec4(color.rgb * rcp(getMaximumElement(color.rgb) + 1.0), color.a);
}

vec4 map(in vec4 color, in float weigth) {
    return vec4(color.rgb * rcp(weigth * getMaximumElement(color.rgb) + 1.0), color.a);
}

vec4 unmap(in vec4 color) {
    return vec4(color.rgb * rcp(1.0 - getMaximumElement(color.rgb)), color.a);
}

vec2 getClosestFragment(in vec2 uv) {
    const vec2 k = constants.texelSize.xy;

#ifdef TAA_USE_GATHER_FOR_DEPTH_SAMPLE
    const vec4 neighborhood = textureGather(a_depth, uv);
    vec3 result = vec3(0.0, 0.0, texture(a_depth, uv).r);
#else
    const vec4 neighborhood = vec4(
        texture(a_depth, uv - k).r,
        texture(a_depth, uv + vec2(k.x, -k.y)).r,
        texture(a_depth, uv + vec2(-k.x, k.y)).r,
        texture(a_depth, uv + k).r
    );

    vec3 result = vec3(0.0, 0.0, texture(a_depth, uv).r);
#endif

#ifdef TAA_USE_EXPERIMENTAL_OPTIMIZATIONS
    result = mix(result, vec3(-1.0, -1.0, neighborhood.x), step(neighborhood.x, result.z));
    result = mix(result, vec3(1.0, -1.0, neighborhood.y), step(neighborhood.y, result.z));
    result = mix(result, vec3(-1.0, 1.0, neighborhood.z), step(neighborhood.z, result.z));
    result = mix(result, vec3(1.0, 1.0, neighborhood.w), step(neighborhood.w, result.z));
#else
    if(neighborhood.x < result.z) {
        result = vec3(-1.0, -1.0, neighborhood.x);
    }

    if(neighborhood.y < result.z) {
        result = vec3(1.0, -1.0, neighborhood.y);
    }

    if(neighborhood.z < result.z) {
        result = vec3(-1.0, 1.0, neighborhood.z);
    }

    if(neighborhood.w < result.z) {
        result = vec3(1.0, 1.0, neighborhood.w);
    }
#endif

    return (uv + result.xy * k);
}

float luminance(in vec3 color) {
    return color.r * 0.299 + color.g * 0.587 + color.b * 0.114;
}

vec4 clipToAABB(in vec4 color, in float p, in vec3 minimum, in vec3 maximum) {
    vec3 center = 0.5 * (maximum + minimum);
    vec3 extents = 0.5 * (maximum - minimum);

    vec4 offset = color - vec4(center, p);
    vec3 repeat = abs(offset.xyz / extents);

    repeat.x = max(repeat.x, max(repeat.y, repeat.z));

    if(repeat.x > 1.0) {
        return vec4(center, p) + offset / repeat.x;
    } else {
        return color;
    }
}

void main() {
#ifdef TAA_DILATE_MOTION_VECTOR_SAMPLE
    vec2 motion = texture(a_velocity, getClosestFragment(uvs)).xy;
#else
    vec2 motion = texture(a_velocity, uvs).xy;
#endif

    const vec2 k = TAA_COLOR_NEIGHBORHOOD_SAMPLE_SPREAD * constants.texelSize.xy;
    vec2 uv = uvs;

#ifdef TAA_REMOVE_COLOR_SAMPLE_JITTER
    uv += constants.subsample.xy;
#endif

    vec4 color = texture(a_color, uv);

#if TAA_COLOR_NEIGHBORHOOD_SAMPLE_PATTERN == 0
    // box filter
    mat4 neighborhood = mat4(
        texture(a_color, uv + vec2(0.0, -k.y)),
        texture(a_color, uv + vec2(-k.x, 0.0)),
        texture(a_color, uv + vec2(k.x, 0.0)),
        texture(a_color, uv + vec2(0.0, k.y))
    );

    #ifdef TAA_SHARPEN_OUTPUT
        vec4 edges = (neighborhood[0] + neighborhood[1] + neighborhood[2] + neighborhood[3]) * 0.25;
        color += (color - edges) * TAA_SHARPEN_FACTOR;
        color = max(vec4(0.0), color);
    #endif

    #ifdef TAA_CLIP_HISTORY_SAMPLE
        #ifdef TAA_TONEMAP_COLOR_AND_HISTORY_SAMPLES
            vec4 average = 
            map(neighborhood[0], 0.2) + map(neighborhood[1], 0.2) +
            map(neighborhood[2], 0.2) + map(neighborhood[3], 0.2) +
            map(color, 0.2);
        #else 
            vec4 average = (neighborhood[0] + neighborhood[1] + neighborhood[2] + neighborhood[3] + color) * 0.2;
        #endif
    #endif

    #ifdef TAA_TONEMAP_COLOR_AND_HISTORY_SAMPLES
        neighborhood[0] = map(neighborhood[0]);
        neighborhood[1] = map(neighborhood[1]);
        neighborhood[2] = map(neighborhood[2]);
        neighborhood[3] = map(neighborhood[3]);

        color = map(color);
    #endif

    vec4 minimum = min(min(min(min(neighborhood[0], neighborhood[1]), neighborhood[2]), neighborhood[3]), color);
    vec4 maximum = min(min(min(min(neighborhood[0], neighborhood[1]), neighborhood[2]), neighborhood[3]), color);

#elif TAA_COLOR_NEIGHBORHOOD_SAMPLE_PATTERN == 1
    mat4 top = mat4(
        texture(a_color, uv + vec2(-k.x, -k.y)),
        texture(a_color, uv + vec2(0.0, -k.y)),
        texture(a_color, uv + vec2(k.x, -k.y)),
        texture(a_color, uv + vec2(-k.x, 0.0))
    );

    mat4 bottom = mat4(
        texture(a_color, uv + vec2(k.x, 0.0)),
        texture(a_color, uv + vec2(-k.x, k.y)),
        texture(a_color, uv + vec2(0.0, k.y)),
        texture(a_color, uv + vec2(k.x, k.y))
    );

    #ifdef TAA_SHARPEN_OUTPUT
        vec4 corners = (top[0] + top[2] + bottom[1] + bottom[3]) * 0.25;
        color += (color - corners) * TAA_SHARPEN_FACTOR;
        color = max(vec4(0.0), color);
    #endif
   
    #ifdef TAA_CLIP_HISTORY_SAMPLE
        #ifdef TAA_TONEMAP_COLOR_AND_HISTORY_SAMPLES
            vec4 average = 
            map(top[0], 0.111111) + map(top[1], 0.111111) +
            map(top[2], 0.111111) + map(top[3], 0.111111) +
            map(color, 0.111111) + map(bottom[0], 0.111111) +
            map(bottom[1], 0.111111) + map(bottom[2], 0.111111) + 
            map(bottom[3], 0.111111);
        #else 
            vec4 average = (top[0] + top[1] + top[2] + top[3] + bottom[0] + bottom[1] + bottom[2] + bottom[3] + color) * 0.111111;
        #endif
    #endif

    #ifdef TAA_TONEMAP_COLOR_AND_HISTORY_SAMPLES
        top[0] = map(top[0]);
        top[1] = map(top[1]);
        top[2] = map(top[2]);
        top[3] = map(top[3]);

        color = map(color);

        bottom[0] = map(bottom[0]);
        bottom[1] = map(bottom[1]);
        bottom[2] = map(bottom[2]);
        bottom[3] = map(bottom[3]);
    #endif

    vec4 minimum = min(min(min(min(min(min(min(min(top[0], top[1]), top[2]), top[3]), bottom[0]), bottom[1]), bottom[2]), bottom[3]), color);
    vec4 maximum = max(max(max(max(max(max(max(max(top[0], top[1]), top[2]), top[3]), bottom[0]), bottom[1]), bottom[2]), bottom[3]), color);
#else
    vec4 topLeft = texture(a_color, uv - k * 0.5);
    vec4 bottomRight = texture(a_color, uv + k * 0.5);

    vec4 corners = 4.0 * (topLeft + bottomRight) - 2.0 * color;

    #ifdef TAA_SHARPEN_OUTPUT
        color += (color - corners * 0.166667) * 2.718282 * TAA_SHARPEN_FACTOR;
        color = max(vec4(0.0), color);
    #endif

    vec4 average = (topLeft + bottomRight) * 0.5f;

    #ifdef TAA_TONEMAP_COLOR_AND_HISTORY_SAMPLES
        average = map(average);

        topLeft = map(topLeft);
        bottomRight = map(bottomRight);

        color = map(color);
    #endif 

    vec4 luma = vec4(luminance(topLeft.rgb), luminance(bottomRight.rgb), luminance(average.rgb), luminance(color.rgb));
#endif

    vec4 history = texture(a_history, uvs - motion);

#if TAA_COLOR_NEIGHBORHOOD_SAMPLE_PATTERN == 2
    float sensitivity = clamp(smoothstep(TAA_MINIMUM_MOTION_SENSITIVITY * constants.texelSize.z, TAA_MAXIMUM_MOTION_SENSITIVITY * constants.texelSize.z, history.a), 0.0, 1.0);
    float nudge = mix(TAA_MINIMUM_MOTION_NUDGE, TAA_MAXIMUM_MOTION_NUDGE, sensitivity) * max(abs(luma.z - luma.w), abs(luma.x - luma.y));

    vec4 minimum = mix(bottomRight, topLeft, step(luma.x, luma.y)) - nudge;
    vec4 maximum = mix(topLeft, bottomRight, step(luma.x, luma.y)) + nudge;
#endif

#if TAA_TONEMAP_COLOR_AND_HISTORY_SAMPLES
    history = map(history);
#endif

#if TAA_CLIP_HISTORY_SAMPLE
    history = clipToAABB(history, history.a, minimum.xyz, maximum.xyz);
#else
    history = clamp(history, minimum, maximum);
#endif


#if TAA_STORE_FRAGMENT_MOTION_HISTORY
    color.a = length(motion * TAA_MOTION_AMPLIFICATION);
#endif

#if TAA_FINAL_BLEND_METHOD == 0
    color = mix(color, history, TAA_FINAL_BLEND_FACTOR);
#elif TAA_FINAL_BLEND_METHOD == 1
    #if TAA_COLOR_NEIGHBORHOOD_SAMPLE_PATTERN < 2
        vec2
    #endif

    luma.xy = vec2(luminance(color.rgb), luminance(history.rgb));

    float weigth = 1.0 - abs(luma.x - luma.y) / max(luma.x, max(luma.y, 0.2));
    weigth = mix(TAA_FINAL_BLEND_DYNAMIC_FACTOR, TAA_FINAL_BLEND_STATIC_FACTOR, weigth * weigth);

    color = mix(color, history, weigth);
#elif TAA_FINAL_BLEND_METHOD == 2
    float weigth = clamp(mix(TAA_FINAL_BLEND_STATIC_FACTOR, TAA_FINAL_BLEND_DYNAMIC_FACTOR, history.a), TAA_FINAL_BLEND_DYNAMIC_FACTOR, TAA_FINAL_BLEND_STATIC_FACTOR);

    color = mix(color, history, weigth);
#endif

#if TAA_TONEMAP_COLOR_AND_HISTORY_SAMPLES
    color = unmap(color);
#endif

#if TAA_STORE_FRAGMENT_MOTION_HISTORY
    color.a *= TAA_FRAGMENT_MOTION_HISTORY_DECAY;
#endif

    if(constants.taaEnabled.x > 0) {
        if(constants.taaEnabled.y > 0.0) {
            outFragColor = color;
        }
        else {
            vec2 velocity = getVelocity(uvs);

            vec4 neighborhood[9];
            neighborhood[0] = texture(a_color, uvs + vec2(-1, -1) * constants.texelSize.xy);
            neighborhood[1] = texture(a_color, uvs + vec2(0, -1) * constants.texelSize.xy);
            neighborhood[2] = texture(a_color, uvs + vec2(1, -1) * constants.texelSize.xy);
            neighborhood[3] = texture(a_color, uvs + vec2(-1, 0) * constants.texelSize.xy);
            neighborhood[4] = texture(a_color, uvs + vec2(0, 0) * constants.texelSize.xy);
            neighborhood[5] = texture(a_color, uvs + vec2(1, 0) * constants.texelSize.xy);
            neighborhood[6] = texture(a_color, uvs + vec2(-1, 1) * constants.texelSize.xy);
            neighborhood[7] = texture(a_color, uvs + vec2(0, 1) * constants.texelSize.xy);
            neighborhood[8] = texture(a_color, uvs + vec2(1, 1) * constants.texelSize.xy);
            vec4 neighborhoodMin = neighborhood[0];
            vec4 neighborhoodMax = neighborhood[0];

            for (uint i = 1; i < 9; ++i) {
                neighborhoodMin = min(neighborhoodMin, neighborhood[i]);
                neighborhoodMax = max(neighborhoodMax, neighborhood[i]);
            }

            history = texture(a_history, uvs - velocity);
            history = clamp(history, neighborhoodMin, neighborhoodMax);

            float subpixelCorrection = fract(max(abs(velocity.x) / constants.texelSize.x, abs(velocity.y) / constants.texelSize.x)) * 0.5f;

            vec4 current = neighborhood[4];

            float blendfactor = clamp(mix(0.0225f, 0.8f, subpixelCorrection), 0.0, 1.0);

            vec4 resolved = mix(history, current, blendfactor);

            outFragColor = resolved;
        }
    } else {
          outFragColor = texture(a_color, uvs);
    }
}