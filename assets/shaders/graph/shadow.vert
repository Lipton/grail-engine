#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec3 inPos;
layout (location = 1) in vec3 inNormals;
layout (location = 2) in vec3 inTangent;
layout (location = 3) in vec3 inUvs;

layout (std140, push_constant) uniform pushConstants {
	mat4 model;
    vec4 cascadeIndex;
} material;

layout (binding = 0) uniform UBO {
   	mat4 projection;
	mat4 inverseProjection;

	mat4 view;
	mat4 inverseView;

	mat4 combined;
	mat4 lastCombined;
	mat4 inverseCombined;

	vec4 position;
	vec4 viewDirection;
	vec4 viewport;

    mat4 shadowCascades[4];
	vec4 cascadeSplits[4];
} camera;

out gl_PerVertex {
    vec4 gl_Position;
};

void main() {
	vec4 pos = material.model * vec4(inPos, 1.0);
	vec4 v_pos = camera.shadowCascades[int(material.cascadeIndex.x)] * vec4(pos.xyz, 1.0);
	gl_Position = v_pos;
}
