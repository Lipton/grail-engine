#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec2 position;
layout (location = 1) in vec2 uvs;

layout (location = 0) out vec2 out_uvs;

out gl_PerVertex {
    vec4 gl_Position;
};

void main() {
	vec4 position = vec4(vec3(position, 0.0), 1.0);
	gl_Position = position;

    out_uvs = uvs;
}
