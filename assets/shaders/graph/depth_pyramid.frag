#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec2 uvs;

layout (binding = 0) uniform sampler2D u_lastMip;

layout (location = 0) out float out_depth;

layout (std140, push_constant) uniform pushConstants {
    vec2 texelSize;
    vec2 size;
} constants;

const vec2 g_kernel[4] = vec2[](
    vec2(-0.5f, -0.5f),
    vec2(0.5f, -0.5f),
    vec2(-0.5f, 0.5f),
    vec2(0.5f, 0.5f)
);

void main() {
    vec4 texels;

    texels.x = texture(u_lastMip, uvs).r;
    texels.y = texture(u_lastMip, uvs + vec2(-1, 0) * constants.texelSize).r;
    texels.z = texture(u_lastMip, uvs + vec2(-1, -1) * constants.texelSize).r;
    texels.w = texture(u_lastMip, uvs + vec2(0, -1) * constants.texelSize).r;

    float maxZ = max( max( texels.x, texels.y ), max( texels.z, texels.w ) );

    vec3 extra;

    if(((int(constants.size.x) & 1) != 0) && (int(gl_FragCoord.x) == int(constants.size.x) - 3)) {
        if(((int(constants.size.y) & 1) != 0) && (int(gl_FragCoord.y) == int(constants.size.y) - 3)) {
            extra.z = texture(u_lastMip, uvs + vec2(1, 1) * constants.texelSize).r;
            maxZ = max(maxZ, extra.z);
        }

        extra.x = texture(u_lastMip, uvs + vec2(1, 0) * constants.texelSize).r;
        extra.y = texture(u_lastMip, uvs + vec2(1, -1) * constants.texelSize).r;

        maxZ = max(maxZ, max(extra.x, extra.y));
    } else {
        if(((int(constants.size.y) & 1) != 0) && (int(gl_FragCoord.y) == int(constants.size.y) - 3)) {
            extra.x = texture(u_lastMip, uvs + vec2(0, 1) * constants.texelSize).r;
            extra.y = texture(u_lastMip, uvs + vec2(-1, 1) * constants.texelSize).r;
            maxZ = max(maxZ, max(extra.x, extra.y));
        }
    }

   out_depth = maxZ;
   //out_depth = texture(u_lastMip, uvs).r;
}
