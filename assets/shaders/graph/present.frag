#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec2 uvs;

layout (binding = 0) uniform sampler2D g_color;

layout (std140, push_constant) uniform pushConstants {
    vec4 texelSize;
} constants;

layout (location = 0) out vec4 outFragColor;

vec4 abberation(sampler2D image, in vec2 uv, in vec2 direction) {
    vec4 col = vec4(0.0);
    vec2 offset = vec2(1.333333333333) * direction;

    col.ra = texture(image, uv).ra;
    col.g = texture(image, uv - (offset * constants.texelSize.xy)).g;
    col.b = texture(image, uv - 2.0 * (offset * constants.texelSize.xy)).b;

    return col;
}


void main() {
   vec2 uv = uvs;
    uv *= 1.0 - uv.yx;

    float vig = uv.x * uv.y * 15.0;
    vig = pow(vig, 0.0525);

    outFragColor = abberation(g_color, uvs, (uvs - 0.5) * 20.0 * (1.0 - pow(uv.x * uv.y * 5, 0.0525))) * vig;
    //outFragColor = texture(g_color, uvs);
   // outFragColor = vec4(vec3((1.0 - pow(uv.x * uv.y * 5, 0.0525))), 1.0);
}