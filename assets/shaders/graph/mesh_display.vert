#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec3 inPos;
layout (location = 1) in vec3 inNormals;
layout (location = 2) in vec3 inTangent;
layout (location = 3) in vec3 inUvs;

layout (location = 0) out vec3 pass_position;
layout (location = 1) out vec3 pass_normals;
layout (location = 2) out vec2 pass_uvs;
layout (location = 3) out vec3 pass_tangent;
layout (location = 4) out vec3 pass_bitangent;

layout (std140, push_constant) uniform pushConstants {
	mat4 model;
	mat4 combined;
} props;

out gl_PerVertex {
    vec4 gl_Position;
};

void main() {
	gl_Position = props.combined * props.model * vec4(inPos, 1.0);

	vec3 vertexBinormal = cross(inNormals, inTangent);
	mat3 normalMatrix = transpose(inverse(mat3(props.model)));

    pass_position = (props.model * vec4(inPos, 1.0)).xyz;
	pass_normals = normalize(normalMatrix * inNormals);
    pass_tangent = normalize(normalMatrix * inTangent);
    pass_tangent = normalize(pass_tangent - dot(pass_tangent, pass_normals) * pass_normals);
    pass_bitangent = normalize(normalMatrix * vertexBinormal);
	pass_bitangent = cross(pass_normals, pass_tangent);
	pass_uvs = inUvs.xy;
}
