#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec2 uvs;


layout (binding = 0) uniform sampler2D g_color;
layout (binding = 1) uniform sampler2D g_vel;
layout (binding = 2) uniform sampler2D g_depth;
//layout (binding = 2) uniform sampler2D vel_tile_max;
//layout (binding = 3) uniform sampler2D vel_neighbour_max;
//layout (binding = 4) uniform sampler2D g_depth;

layout (std140, push_constant) uniform pushConstants {
    vec2 texelSize;
    vec2 fps;
    vec4 cameraSettings;
    vec2 vel;
    vec2 motionBlurSamples;
} constants;

const float exposureTime = 0.1;

layout (location = 0) out vec4 outFragColor;

const mat3 ACESInputMat = {
    {0.59719, 0.35458, 0.04823},
    {0.07600, 0.90834, 0.01566},
    {0.02840, 0.13383, 0.83777}
};

// ODT_SAT => XYZ => D60_2_D65 => sRGB
const mat3 ACESOutputMat = {
    { 1.60475, -0.53108, -0.07367},
    {-0.10208,  1.10813, -0.00605},
    {-0.00327, -0.07276,  1.07602}
};

vec3 RRTAndODTFit(vec3 v) {
    vec3 a = v * (v + 0.0245786f) - 0.000090537f;
    vec3 b = v * (0.983729f * v + 0.4329510f) + 0.238081f;
    return a / b;
}

vec3 ACESFitted(vec3 color) {
    color = transpose(ACESInputMat) * color;

    // Apply RRT and ODT
    color = RRTAndODTFit(color);

    color = transpose(ACESOutputMat) * color;

    // Clamp to [0, 1]
    color = clamp(color, 0, 1);

    return color;
}

vec3 ACESFilm( vec3 x )
{
    float a = 2.51f;
    float b = 0.03f;
    float c = 2.43f;
    float d = 0.59f;
    float e = 0.14f;
    return clamp((x*(a*x+b))/(x*(c*x+d)+e), 0, 1);
}

vec3 grade(vec3 color, sampler2D lut) {
    vec2 lutSize = 1.0 / vec2(512, 512);
    vec4 lutUV;

    color = clamp(color, 1.0, 0.0) * (lutSize.y - 1.0);
    lutUV.w = floor(color.b);
    lutUV.xy = (color.rg + 0.5) * lutSize;
    lutUV.x += lutUV.w * lutSize.y;
    lutUV.z = lutUV.x + lutSize.y;

    return mix(texture(lut, lutUV.xy).rgb, texture(lut, lutUV.zy).rgb, color.r - lutUV.w);
}

float softDepthCompare(float za, float zb) {
    return clamp( (1.0 - (za - zb) / .1), 0.0, 1.0 );
}

void main() {
    vec3 color = vec3(0.0);

    vec2 velocity = texture(g_vel, uvs).xy;

    if(length(velocity) == 0) {
        velocity = constants.vel * 0.2;
    }

    velocity *= constants.fps.y;
    float speed = length(velocity / constants.texelSize.xy);
        
    int samples = clamp(int(speed), 1, int(constants.motionBlurSamples.x));

    color = texture(g_color, uvs).rgb;
    float cDepth = -textureLod(g_depth, uvs, 0).r;
    for(int i = 1; i < samples; ++i) {
        vec2 offset = velocity * (float(i) / float(samples - 1) - 0.5);
        
        float depth = -textureLod(g_depth, uvs + offset, 0).r;
        
        color += texture(g_color, uvs + offset).rgb * softDepthCompare(cDepth, depth);
    }
    color /= float(samples);

    vec3 mappedColor = ACESFitted(color.rgb * constants.cameraSettings.w);
    mappedColor = pow(mappedColor, vec3(1.0 / 2.2));

    outFragColor = vec4(mappedColor.rgb, 1.0);
}