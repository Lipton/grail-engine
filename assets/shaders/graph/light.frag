#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

#define PI                 3.14159265359
#define HALF_PI            1.570796327

#define MEDIUMP_FLT_MAX    65504.0
#define MEDIUMP_FLT_MIN    0.00006103515625

#define saturateMediump(x) x

#ifndef PLATFORM_MOBILE
    #define CASCADE_COUNT 3
#else
    #define CASCADE_COUNT 3
#endif

//#define OLD_SHADOWS

  //#pragma optionNV (unroll all)

layout (location = 0) in vec2 uvs;
layout (location = 1) smooth in vec3 pass_eye;

layout (binding = 0) uniform sampler2D g_color;
layout (binding = 1) uniform sampler2D g_normal;
layout (binding = 2) uniform sampler2D g_props;
layout (binding = 3) uniform sampler2D g_depth;
#ifdef OLD_SHADOWS
layout (binding = 4) uniform sampler2DArray g_shadow;
#else
layout (binding = 4) uniform sampler2DArrayShadow g_shadow;
#endif
layout (binding = 5) uniform sampler2D g_ssao;
//layout (binding = 6) uniform sampler2D g_ssaoNormals;
layout (binding = 6) uniform samplerCube i_irradiance;
layout (binding = 7) uniform samplerCube i_specular;
layout (binding = 8) uniform sampler2D i_brdfLUT;

layout (binding = 9) uniform UBO {
   	mat4 projection;
	mat4 inverseProjection;

	mat4 view;
	mat4 inverseView;

	mat4 combined;
    mat4 lastCombined;
	mat4 inverseCombined;

	vec4 position;
	vec4 viewDirection;
    vec4 viewport;

    mat4 shadowCascades[4];
	vec4 cascadeSplits[4];

    vec2 time;
    vec2 subsample;

    //vec4 pad[13];
} camera;

layout (std140, push_constant) uniform pushConstants {
    vec4 lightDirection;
    vec3 lightColor;
    float lightIntensity;
} constants;

layout (location = 0) out vec4 outFragColor;

vec4 getViewPos(in vec2 texCoord, in mat4 inverseProjection, in float depth) {
	float x = texCoord.s * 2.0 - 1.0;
	float y = texCoord.t * 2.0 - 1.0;
	float z = depth ;

	vec4 posProj = vec4(x, y, z, 1.0);
	vec4 posView = inverseProjection * posProj;
	posView /= posView.w;

	return posView;
}

vec4 getWorldPos(in vec2 texCoord, in mat4 inverseCombined, in float depth) {
	float x = texCoord.s * 2.0 - 1.0;
	float y = texCoord.t * 2.0 - 1.0;
	float z = depth;

	vec4 posProj = vec4(x, y, z, 1.0);
	vec4 posView = inverseCombined * posProj;
	posView /= posView.w;

	return posView;
}

vec2 vogelDisk(int sampleIndex, int sampleCount, float phi) {
    float goldenAngle = 2.4f;

    float r = sqrt(sampleIndex + 0.5f) / sqrt(sampleCount);
    float theta = sampleIndex * goldenAngle + phi;

    float sine = sin(theta);
    float cosine = cos(theta);

    return vec2(r * cosine, r * sine);
}

vec2 get_shadow_offsets(vec3 N, vec3 L) {
    float cos_alpha = clamp(dot(N, L), 0, 1);
    float offset_scale_N = sqrt(1 - cos_alpha*cos_alpha); // sin(acos(L·N))
    float offset_scale_L = offset_scale_N / cos_alpha;    // tan(acos(L·N))
    return vec2(offset_scale_N, min(2, offset_scale_L));
}

#ifndef OLD_SHADOWS
vec2 ComputeReceiverPlaneDepthBias(vec3 texCoordDX, vec3 texCoordDY) {
    vec2 biasUV;
    biasUV.x = texCoordDY.y * texCoordDX.z - texCoordDX.y * texCoordDY.z;
    biasUV.y = texCoordDX.x * texCoordDY.z - texCoordDY.x * texCoordDX.z;
    biasUV *= 1.0f / ((texCoordDX.x * texCoordDY.y) - (texCoordDX.y * texCoordDY.x));
    return biasUV;
}

float noise(vec2 position) {
	return fract(52.9829189 * fract(dot(position, vec2( 0.06711056, 0.00583715))));
}

#define UsePlaneDepthBias_ 1
float SampleShadowMap(in vec2 base_uv, in float u, in float v, in vec2 shadowMapSizeInv,
                      in uint cascadeIdx,  in float depth, in vec2 receiverPlaneDepthBias) {

    vec2 uv = base_uv + (vec2(u, v)) * shadowMapSizeInv;

    #if UsePlaneDepthBias_
        float z = depth + dot(vec2(u, v) * shadowMapSizeInv, receiverPlaneDepthBias);
    #else
        float z = depth;
    #endif

    return texture(g_shadow, vec4(uv, cascadeIdx, z));
}


float SampleShadowMapOptimizedPCF(in vec3 shadowPos, in vec3 shadowPosDX,
                         in vec3 shadowPosDY, in uint cascadeIdx, in vec3 N, in vec3 L) {
    vec2 shadowMapSize;
    float numSlices = 3;
    shadowMapSize = textureSize(g_shadow, 0).xy;

    float lightDepth = shadowPos.z;

    const float bias = 0.0;

    #if UsePlaneDepthBias_
        vec2 texelSize = 1.0f / shadowMapSize;

        vec2 receiverPlaneDepthBias = ComputeReceiverPlaneDepthBias(shadowPosDX, shadowPosDY);

        // Static depth biasing to make up for incorrect fractional sampling on the shadow map grid
        float fractionalSamplingError = 2 * dot(get_shadow_offsets(N, L) * texelSize, abs(receiverPlaneDepthBias));
        lightDepth -= min(fractionalSamplingError, 0.01f);
    #else
        vec2 receiverPlaneDepthBias;
        lightDepth -= bias;
    #endif

    vec2 uv = shadowPos.xy * shadowMapSize; // 1 unit - 1 texel

    vec2 shadowMapSizeInv = 1.0 / shadowMapSize;

    vec2 base_uv;
    base_uv.x = floor(uv.x + 0.5);
    base_uv.y = floor(uv.y + 0.5);

    float s = (uv.x + 0.5 - base_uv.x);
    float t = (uv.y + 0.5 - base_uv.y);

    base_uv -= vec2(0.5, 0.5);
    base_uv *= shadowMapSizeInv;

    float sum = 0;

#if defined PLATFORM_MOBILE
    #define FilterSize_ 3
#else
    #define FilterSize_ 7
#endif

    #if FilterSize_ == 2
        return texture(g_shadow, vec4(shadowPos.xy, cascadeIdx, lightDepth));
    #elif FilterSize_ == 3

        float uw0 = (3 - 2 * s);
        float uw1 = (1 + 2 * s);

        float u0 = (2 - s) / uw0 - 1;
        float u1 = s / uw1 + 1;

        float vw0 = (3 - 2 * t);
        float vw1 = (1 + 2 * t);

        float v0 = (2 - t) / vw0 - 1;
        float v1 = t / vw1 + 1;

        sum += uw0 * vw0 * SampleShadowMap(base_uv, u0, v0, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);
        sum += uw1 * vw0 * SampleShadowMap(base_uv, u1, v0, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);
        sum += uw0 * vw1 * SampleShadowMap(base_uv, u0, v1, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);
        sum += uw1 * vw1 * SampleShadowMap(base_uv, u1, v1, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);

        return sum * 1.0f / 16;

    #elif FilterSize_ == 5

        float uw0 = (4 - 3 * s);
        float uw1 = 7;
        float uw2 = (1 + 3 * s);

        float u0 = (3 - 2 * s) / uw0 - 2;
        float u1 = (3 + s) / uw1;
        float u2 = s / uw2 + 2;

        float vw0 = (4 - 3 * t);
        float vw1 = 7;
        float vw2 = (1 + 3 * t);

        float v0 = (3 - 2 * t) / vw0 - 2;
        float v1 = (3 + t) / vw1;
        float v2 = t / vw2 + 2;

        sum += uw0 * vw0 * SampleShadowMap(base_uv, u0, v0, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);
        sum += uw1 * vw0 * SampleShadowMap(base_uv, u1, v0, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);
        sum += uw2 * vw0 * SampleShadowMap(base_uv, u2, v0, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);

        sum += uw0 * vw1 * SampleShadowMap(base_uv, u0, v1, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);
        sum += uw1 * vw1 * SampleShadowMap(base_uv, u1, v1, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);
        sum += uw2 * vw1 * SampleShadowMap(base_uv, u2, v1, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);

        sum += uw0 * vw2 * SampleShadowMap(base_uv, u0, v2, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);
        sum += uw1 * vw2 * SampleShadowMap(base_uv, u1, v2, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);
        sum += uw2 * vw2 * SampleShadowMap(base_uv, u2, v2, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);

        return sum * 1.0f / 144;

    #else // FilterSize_ == 7

        float uw0 = (5 * s - 6);
        float uw1 = (11 * s - 28);
        float uw2 = -(11 * s + 17);
        float uw3 = -(5 * s + 1);

        float u0 = (4 * s - 5) / uw0 - 3;
        float u1 = (4 * s - 16) / uw1 - 1;
        float u2 = -(7 * s + 5) / uw2 + 1;
        float u3 = -s / uw3 + 3;

        float vw0 = (5 * t - 6);
        float vw1 = (11 * t - 28);
        float vw2 = -(11 * t + 17);
        float vw3 = -(5 * t + 1);

        float v0 = (4 * t - 5) / vw0 - 3;
        float v1 = (4 * t - 16) / vw1 - 1;
        float v2 = -(7 * t + 5) / vw2 + 1;
        float v3 = -t / vw3 + 3;

        sum += uw0 * vw0 * SampleShadowMap(base_uv, u0, v0, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);
        sum += uw1 * vw0 * SampleShadowMap(base_uv, u1, v0, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);
        sum += uw2 * vw0 * SampleShadowMap(base_uv, u2, v0, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);
        sum += uw3 * vw0 * SampleShadowMap(base_uv, u3, v0, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);

        sum += uw0 * vw1 * SampleShadowMap(base_uv, u0, v1, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);
        sum += uw1 * vw1 * SampleShadowMap(base_uv, u1, v1, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);
        sum += uw2 * vw1 * SampleShadowMap(base_uv, u2, v1, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);
        sum += uw3 * vw1 * SampleShadowMap(base_uv, u3, v1, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);

        sum += uw0 * vw2 * SampleShadowMap(base_uv, u0, v2, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);
        sum += uw1 * vw2 * SampleShadowMap(base_uv, u1, v2, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);
        sum += uw2 * vw2 * SampleShadowMap(base_uv, u2, v2, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);
        sum += uw3 * vw2 * SampleShadowMap(base_uv, u3, v2, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);

        sum += uw0 * vw3 * SampleShadowMap(base_uv, u0, v3, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);
        sum += uw1 * vw3 * SampleShadowMap(base_uv, u1, v3, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);
        sum += uw2 * vw3 * SampleShadowMap(base_uv, u2, v3, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);
        sum += uw3 * vw3 * SampleShadowMap(base_uv, u3, v3, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);

        return sum * 1.0f / 2704;

    #endif
}
#else

float calculateShadowing(vec3 normal, vec4 lightSpacePos, vec3 lightDir, uint cascadeIndex) {
	vec3 projCoords = lightSpacePos.xyz / lightSpacePos.w;
   // projCoords.xy += camera.subsample * 0.8;

    float closestDepth = texture(g_shadow, vec3(projCoords.xy, cascadeIndex)).r;
	float currDepth = projCoords.z;
	
    vec2 bias = get_shadow_offsets(normal, lightDir);

    float shadow = 0.0;
	vec2 texelSize = 1.0 / textureSize(g_shadow, 0).xy;

#define SAMPLE_PCF(name, uvs, offset) \
    const vec4 name = textureGatherOffset(g_shadow, vec3(uvs, cascadeIndex), offset, 0); \
    if(currDepth - name.x > bias.x * 0.005) { sum += 1.0 ; } count++; \
    if(currDepth - name.y > bias.x * 0.005) { sum += 1.0 ; } count++; \
    if(currDepth - name.z > bias.x * 0.005) { sum += 1.0 ; } count++; \
    if(currDepth - name.w > bias.x * 0.005) { sum += 1.0 ; } count++;

#define LOOPS 2

			float sum = 0, count = 0;
			float x, y;
			/*for (y = -LOOPS; y <= LOOPS; y += 1.0)
			for (x = -LOOPS; x <= LOOPS; x += 1.0) {
				// Can't use texture(Proj)Offset directly since it expects the offset to be a constant value,
				// i.e. no loops, so instead we calculate the offset manually (given the texture size)
				vec2 offset = vec2(x, y);
				float dp = texture(g_shadow, vec3(projCoords.xy + (offset * texelSize), cascadeIndex)).r;
                if(currDepth - dp > bias.x * 0.005) {
                    sum += 1.0;
                }
				count++;
               // SAMPLE_PCF(o, projCoords.xy, ivec2(x, y))
			}*/


        SAMPLE_PCF(o, projCoords.xy, ivec2(1, 0))
        SAMPLE_PCF(t, projCoords.xy, ivec2(1, 1))
        SAMPLE_PCF(p, projCoords.xy, ivec2(-1, -1))
        SAMPLE_PCF(w, projCoords.xy, ivec2(-1, 0))
        //SAMPLE_PCF(s, projCoords.xy, ivec2(-1, 1))

     /*   SAMPLE_PCF(tt, projCoords.xy + vec2(0.5, 0) * texelSize)
        SAMPLE_PCF(pp, projCoords.xy + vec2(-0.5, 0) * texelSize)
        SAMPLE_PCF(ww, projCoords.xy + vec2(0, 1.5) * texelSize)
        SAMPLE_PCF(ss, projCoords.xy + vec2(0, -1.5) * texelSize)*/

			shadow = sum / count;

        return 1.0 - shadow;
}
#endif

const mat4 biasMat = mat4( 
	0.5, 0.0, 0.0, 0.0,
	0.0, 0.5, 0.0, 0.0,
	0.0, 0.0, 1.0, 0.0,
	0.5, 0.5, 0.0, 1.0 
);

const vec4 colors[4] = vec4[4] (
    vec4(1.0, 0.0, 0.0, 1.0),
        vec4(1.0, 1.0, 0.0, 1.0),
            vec4(0.0, 1.0, 0.0, 1.0),
                vec4(0.0, 0.0, 1.0, 1.0)
);

#define POW(input) pow(input, vec3(1.0 / 2.2)

float linearDepth(float depth) {
    float NEAR_PLANE = camera.viewport.z * 0.001;
    float FAR_PLANE = camera.viewport.w;

	float z = depth * 2.0f - 1.0f; 
	return (2.0f * NEAR_PLANE * FAR_PLANE) / (FAR_PLANE + NEAR_PLANE - z * (FAR_PLANE - NEAR_PLANE));	
}

const vec2 invAtan = vec2(0.1591f, 0.3183f);
vec2 sampleSphericalMap(vec3 v) {
    vec2 uv = vec2(atan(v.z, v.x), asin(v.y));
    uv *= invAtan;
    uv += 0.5;
    return uv;
}

vec3 F_SchlickR(float cosTheta, vec3 F0, float roughness) {
	return F0 + (max(vec3(1.0 - roughness), F0) - F0) * pow(1.0 - cosTheta, 5.0);
}

vec3 prefilteredReflection(vec3 R, float roughness) {
	float lod = roughness * textureQueryLevels(i_specular);
	float lodf = floor(lod);
	float lodc = ceil(lod);
	vec3 a = textureLod(i_specular, R, lodf).rgb;
	vec3 b = textureLod(i_specular, R, lodc).rgb;
    
	return mix(a, b, lod - lodf);
}

float sqr(float x) { return x*x; }

float SchlickFresnel(float u) {
    float m = clamp(1-u, 0, 1);
    float m2 = m*m;
    return m2*m2*m; // pow(m,5)
}

float GTR2_aniso(float NdotH, float HdotX, float HdotY, float ax, float ay) {
    return 1 / ( PI * ax*ay * sqr( sqr(HdotX/ax) + sqr(HdotY/ay) + NdotH*NdotH ));
}

float smithG_GGX(float Ndotv, float alphaG) {
    float a = alphaG*alphaG;
    float b = Ndotv*Ndotv;
    return 1/(Ndotv + sqrt(a + b - a*b));
}

float GTR1(float NdotH, float a) {
    if (a >= 1) return 1/PI;
    float a2 = a*a;
    float t = 1 + (a2-1)*NdotH*NdotH;
    return (a2-1) / (PI*log(a2)*t);
}

vec3 mon2lin(vec3 x) {
    return vec3(pow(x[0], 2.2), pow(x[1], 2.2), pow(x[2], 2.2));
}

vec3 FresnelSchlickRoughness(float cosTheta, vec3 F0, float roughness)
{
	return F0 + (max(vec3(1.0 - roughness), F0) - F0) * pow(1.0 - cosTheta, 5.0);
}

float computeSpecularAO(float NoV, float ao, float roughness) {
    return clamp(pow(NoV + ao, exp2(-16.0 * roughness - 1.0)) - 1.0 + ao, 0.0, 1.0);
}

vec3 BRDF(vec3 baseColor, vec3 L, vec3 V, vec3 N, vec3 X, vec3 Y, float metallic, float roughness, float attenuation) {
    float NdotL = clamp(dot(N,L), 0.0, 1.0);
    float NdotV = clamp(dot(N,V), 0.0, 1.0);
    if (NdotL < 0 || NdotV < 0) return vec3(0);

    vec3 H = normalize(L+V);
    float NdotH = dot(N,H);
    float LdotH = dot(L,H);

    vec3 Cdlin = mon2lin(baseColor);
    float Cdlum = .3*Cdlin[0] + .6*Cdlin[1]  + .1*Cdlin[2]; // luminance approx.

    vec3 Ctint = Cdlum > 0 ? Cdlin / Cdlum : vec3(1); // normalize lum. to isolate hue+sat
    vec3 Cspec0 = mix(vec3(0.04), Cdlin, metallic); // mix(specular*.08*mix(vec3(1), Ctint, specularTint), Cdlin, metallic);
    //REMOVED : vec3 Csheen = mix(vec3(1), Ctint, 0.0); //mix(vec3(1), Ctint, sheenTint);

    // Diffuse fresnel - go from 1 at normal incidence to .5 at grazing
    // and mix in diffuse retro-reflection based on roughness
    float FL = SchlickFresnel(NdotL), FV = SchlickFresnel(NdotV);
    float Fd90 = 0.5 + 2 * LdotH*LdotH * roughness;
    float Fd = mix(1, Fd90, FL) * mix(1, Fd90, FV);

    // Based on Hanrahan-Krueger brdf approximation of isotropic bssrdf
    // 1.25 scale is used to (roughly) preserve albedo
    // Fss90 used to "flatten" retroreflection based on roughness
    float Fss90 = LdotH*LdotH*roughness;
    float Fss = mix(1, Fss90, FL) * mix(1, Fss90, FV);
    float ss = 1.25 * (Fss * (1 / (NdotL + NdotV) - .5) + .5);

    // specular
    float aspect = sqrt(0.9); //sqrt(1-anisotropic*.9);
    float ax = max(.001, sqr(roughness)/aspect);
    float ay = max(.001, sqr(roughness)*aspect);
    float Ds = GTR2_aniso(NdotH, dot(H, X), dot(H, Y), ax, ay);
    float FH = SchlickFresnel(LdotH);
    vec3 Fs = mix(Cspec0, vec3(1), FH);
    float roughg = sqr(roughness*.5+.5);
    float Gs = smithG_GGX(NdotL, roughg) * smithG_GGX(NdotV, roughg);

    // sheen
    //REMOVED : vec3 Fsheen = FH * 0.0 * Csheen; //FH * sheen * Csheen;

    // clearcoat (ior = 1.5 -> F0 = 0.04)
    float Dr = GTR1(NdotH, 0.1); //GTR1(NdotH, mix(.1,.001,clearcoatGloss));
    float Fr = mix(.04, 1.0, FH);
    float Gr = smithG_GGX(NdotL, .25) * smithG_GGX(NdotV, .25);

    return (((1/PI) * Fd * Cdlin) //(((1/PI) * mix(Fd, ss, subsurface) * Cdlin + Fsheen)
        * (1-metallic)
        + Gs*Fs*Ds ) * NdotL; //  + Gs*Fs*Ds + .25*clearcoat*Gr*Fr*Dr) * NdotL * attenuation;
}

float V_SmithGGXCorrelated(float roughness, float NoV, float NoL) {
    // Heitz 2014, "Understanding the Masking-Shadowing Function in Microfacet-Based BRDFs"
    float a2 = roughness * roughness;
    // TODO: lambdaV can be pre-computed for all the lights, it should be moved out of this function
    float lambdaV = NoL * sqrt((NoV - a2 * NoV) * NoV + a2);
    float lambdaL = NoV * sqrt((NoL - a2 * NoL) * NoL + a2);
    float v = 0.5 / (lambdaV + lambdaL);
    // a2=0 => v = 1 / 4*NoL*NoV   => min=1/4, max=+inf
    // a2=1 => v = 1 / 2*(NoL+NoV) => min=1/4, max=+inf
    // clamp to the maximum value representable in mediump
    return saturateMediump(v);
}

float visibility(float roughness, float NoV, float NoL) {
    return V_SmithGGXCorrelated(roughness, NoV, NoL);
}

float pow5(float x) {
    float x2 = x * x;
    return x2 * x2 * x;
}

vec3 F_Schlick(const vec3 f0, float f90, float VoH) {
    // Schlick 1994, "An Inexpensive BRDF Model for Physically-Based Rendering"
    return f0 + (f90 - f0) * pow5(1.0 - VoH);
}

vec3 F_Schlick(const vec3 f0, float VoH) {
    float f = pow(1.0 - VoH, 5.0);
    return f + f0 * (1.0 - f);
}

float F_Schlick(float f0, float f90, float VoH) {
    return f0 + (f90 - f0) * pow5(1.0 - VoH);
}


float Fd_Burley(float roughness, float NoV, float NoL, float LoH) {
    // Burley 2012, "Physically-Based Shading at Disney"
    float f90 = 0.5 + 2.0 * roughness * LoH * LoH;
    float lightScatter = F_Schlick(1.0, f90, NoL);
    float viewScatter  = F_Schlick(1.0, f90, NoV);
    return lightScatter * viewScatter * (1.0 / PI);
}

vec2 sign_not_zero(vec2 v) {
    return fma(step(vec2(0.0), v), vec2(2.0), vec2(-1.0));
}

vec3 unpackNormals(vec2 packed_nrm) {
        // Version using newer GLSL capatibilities
        vec3 v = vec3(packed_nrm.xy, 1.0 - abs(packed_nrm.x) - abs(packed_nrm.y));

                if (v.z < 0) v.xy = (1.0 - abs(v.yx)) * sign_not_zero(v.xy);

    return normalize(v);
}


vec3 multibounceAO(float AO, vec3 Albedo) {
	vec3 A = 2 * Albedo - 0.33;
	vec3 B = -4.8 * Albedo + 0.64;
	vec3 C = 2.75 * Albedo + 0.69;
	return max(vec3(AO), ((AO * A + B) * AO + C) * AO);
}

float ApproximateConeConeIntersection(float ArcLength0, float ArcLength1, float AngleBetweenCones) {
	float AngleDifference = abs(ArcLength0 - ArcLength1);

	float Intersection = 
    smoothstep(
        0.0,
        1.0,
        1.0 - clamp((AngleBetweenCones - AngleDifference) / (ArcLength0 + ArcLength1 - AngleDifference), 0.0, 1.0));

	return Intersection;
}

float ReflectionOcclusion(vec3 BentNormal, vec3 ReflectionVector, float Roughness, float OcclusionStrength) {
	float BentNormalLength = length(BentNormal);
	float ReflectionConeAngle = max(Roughness, 0.1) * PI;
	float UnoccludedAngle = BentNormalLength * PI * OcclusionStrength;

	float AngleBetween = acos(dot(BentNormal, ReflectionVector) / max(BentNormalLength, 0.001));
	float ReflectionOcclusion = ApproximateConeConeIntersection(ReflectionConeAngle, UnoccludedAngle, AngleBetween);
	ReflectionOcclusion = mix(0, ReflectionOcclusion, clamp((UnoccludedAngle - 0.1) / 0.2, 0.0, 1.0));
	return ReflectionOcclusion;
}

void main() {
    float depth = texture(g_depth, uvs).r;
    vec4 finalColor = vec4(0.0);

    if(depth >= 1.0) {
        vec3 localPos = normalize(pass_eye);
        localPos.y *= -1.0;

        finalColor = vec4((texture(i_irradiance, (localPos)).rgb), 1.0);
    } else {
        vec4 albedo = texture(g_color, uvs);
        //albedo.rgb = pow(albedo.rgb, vec3(2.2));
        vec4 g_normalBuffer = texture(g_normal, uvs);

        vec3 normal = unpackNormals(g_normalBuffer.xy);

        vec3 tangent = normalize( cross( vec3(0, 1, 0), normal.rgb ) );
        vec3 bitangent = normalize( cross( normal.rgb, tangent ) );

        vec4 worldPos = getWorldPos(uvs, camera.inverseCombined, depth);
        vec3 viewDir = normalize(camera.position.xyz - worldPos.xyz);

        vec4 props = texture(g_props, uvs);

        float roughness = props.r;
        float metallic = props.g;
        float ao = props.b;
        float materialModel = albedo.a * 255;
        float matPropA = props.a;

 		vec3 reflectVector = normalize(reflect(-viewDir, normal.rgb));
        reflectVector.y *= -1.0;

        uint cascadeIndex = 0;

        vec4 viewPos = getViewPos(uvs, camera.inverseProjection, depth);
        float cascadeValue = 0.0;
            for(uint i = 0; i < CASCADE_COUNT; ++i) {
                if(viewPos.z < camera.cascadeSplits[i].x) {	
                    cascadeIndex = i + 1;
                }
            }

            
            vec3 lightDirection = constants.lightDirection.xyz;
#ifdef OLD_SHADOWS
            vec4 shadowCoord = (biasMat * camera.shadowCascades[cascadeIndex]) * vec4(worldPos.xyz, 1.0);
#else
            vec4 shadowCoord = (biasMat * camera.shadowCascades[cascadeIndex]) * vec4(worldPos.xyz, 1.0);
#endif
            //shadowCoord.xyz /= shadowCoord.w;
            vec3 shadowPosDX = dFdxFine(shadowCoord.xyz);
            vec3 shadowPosDY = dFdyFine(shadowCoord.xyz);
            float shadow = 0;

            #ifdef OLD_SHADOWS
            shadow = calculateShadowing(normal.xyz, shadowCoord, -lightDirection, cascadeIndex);
            #else
            shadow = SampleShadowMapOptimizedPCF(shadowCoord.xyz, shadowPosDX, shadowPosDY, cascadeIndex, normal.xyz, -lightDirection);
            #endif

#ifdef PLATFORM_DESKTOP
#define BLEND_CASCADES
#endif
#ifdef BLEND_CASCADES

            const float blendThreshold = 0.2;
            float nextSplit = camera.cascadeSplits[cascadeIndex].x;
            float splitSize = cascadeIndex == 0 ? nextSplit : nextSplit - camera.cascadeSplits[cascadeIndex - 1].x;
            float fadeFactor = (nextSplit - viewPos.z) / splitSize;
            
        if(fadeFactor <= blendThreshold && cascadeIndex != CASCADE_COUNT - 1) {
            vec4 nextCoord = (biasMat * camera.shadowCascades[cascadeIndex + 1]) * vec4(worldPos.xyz, 1.0);

#ifdef OLD_SHADOWS
            float nextShadow =  calculateShadowing(normal.xyz, nextCoord.xyz, -lightDirection, cascadeIndex + 1);
#else
            float nextShadow =  SampleShadowMapOptimizedPCF(nextCoord.xyz, shadowPosDX, shadowPosDY, cascadeIndex + 1, normal.xyz, -lightDirection);
#endif  
            float lerpAmount = smoothstep(0.0, blendThreshold, fadeFactor);

            shadow = mix(nextShadow, shadow, lerpAmount);
        }
#endif

        float ccr = matPropA;
        vec3 ccnormal = vec3(0.0);

        if(materialModel == 1) {
            ccnormal = unpackNormals(g_normalBuffer.zw);
        }

        vec3 N = normal.rgb;
        vec3 L = lightDirection;
        vec3 V = viewDir;
        vec3 X = tangent;
        vec3 Y = bitangent;

        float NdotL = clamp(dot(N,L), 0.0, 1.0);
        float NdotV = clamp(dot(N,V), 0.0, 1.0);

        vec3 H = normalize(L+V);
        float NdotH = dot(N,H);
        float LdotH = dot(L,H);

        vec3 Cdlin = mon2lin(albedo.rgb);
        float Cdlum = .3*Cdlin[0] + .6*Cdlin[1]  + .1*Cdlin[2]; // luminance approx.

        //vec3 Ctint = Cdlum > 0 ? Cdlin / Cdlum : vec3(1); // normalize lum. to isolate hue+sat
        vec3 Cspec0 = mix(vec3(0.04), Cdlin, metallic); // mix(specular*.08*mix(vec3(1), Ctint, specularTint), Cdlin, metallic);
        //REMOVED : vec3 Csheen = mix(vec3(1), Ctint, 0.0); //mix(vec3(1), Ctint, sheenTint);

        // Diffuse fresnel - go from 1 at normal incidence to .5 at grazing
        // and mix in diffuse retro-reflection based on roughness
        float FL = SchlickFresnel(NdotL), FV = SchlickFresnel(NdotV);
        float Fd90 = 0.5 + 2 * LdotH*LdotH * roughness;
        float Fd = mix(1, Fd90, FL) * mix(1, Fd90, FV);

         // Based on Hanrahan-Krueger brdf approximation of isotropic bssrdf
        // 1.25 scale is used to (roughly) preserve albedo
        // Fss90 used to "flatten" retroreflection based on roughness
       /* float Fss90 = LdotH*LdotH*roughness;
        float Fss = mix(1, Fss90, FL) * mix(1, Fss90, FV);
        float ss = 1.25 * (Fss * (1 / (NdotL + NdotV) - .5) + .5);*/

            // specular
        float aspect = sqrt(0.9); //sqrt(1-anisotropic*.9);
        float ax = max(.001, sqr(roughness)/aspect);
        float ay = max(.001, sqr(roughness)*aspect);
        float Ds = GTR2_aniso(NdotH, dot(H, X), dot(H, Y), ax, ay);
        float FH = SchlickFresnel(LdotH);
        vec3 Fs = mix(Cspec0, vec3(1), FH);
        float roughg = sqr(roughness*.5+.5);
        float Gs = smithG_GGX(NdotL, roughg) * smithG_GGX(NdotV, roughg);

            // sheen
            //REMOVED : vec3 Fsheen = FH * 0.0 * Csheen; //FH * sheen * Csheen;

            // clearcoat (ior = 1.5 -> F0 = 0.04)
        float Dr = 0;
        if(materialModel == 0) {
            Dr = GTR1(NdotH, 0.1); //GTR1(NdotH, mix(.1,.001,clearcoatGloss));
        }
        if(materialModel == 1) {
            Dr = GTR1(dot(ccnormal, H), mix(.1,.001, 1.0 - ccr));
        }
        float Fr = mix(.04, 1.0, FH);
        float Gr = smithG_GGX(NdotL, .25) * smithG_GGX(NdotV, .25);
        
        vec2 env = texture(i_brdfLUT, vec2(NdotV, roughness * roughness)).rg;
        float lod = props.r * textureQueryLevels(i_specular);	
        
        vec3 indirectDiffuse = texture(i_irradiance, normal.rgb * vec3(1, -1, 1)).rgb;
        vec3 indirectSpecular = textureLod(i_specular, reflectVector, lod).rgb;

        vec3 directionalColor = vec3(0.0);
        vec3 directionalColorMod = constants.lightIntensity * constants.lightColor;

        if(materialModel == 0) {
            directionalColor = (((1/PI) * Fd * Cdlin) * (1-metallic) + Gs*Fs*Ds ) * NdotL * directionalColorMod;
        }

        if(materialModel == 1) {
          directionalColor = (((1/PI) * Fd * Cdlin) * (1-metallic) + Gs*Fs*Ds + 1.25 * Gr * Fr * Dr) * NdotL * directionalColorMod;
        }

        vec3 F = F_SchlickR(NdotV, Cspec0, roughness * roughness);
        vec3 iblDiffuse = indirectDiffuse * ao;

        vec3 iblSpecular = (indirectSpecular * (F * env.x + env.y)) * computeSpecularAO(NdotV, ao, roughness);
        vec3 diffuse = Cdlin * (1.0 - metallic);  

        if(materialModel == 1) {
            float ccNdotV = clamp(dot(ccnormal, V), 0.0, 1.0);

            vec3 ccreflectVector = normalize(reflect(-viewDir, ccnormal.rgb));
            ccreflectVector.y *= -1.0;

            vec2 ccenv = texture(i_brdfLUT, vec2(ccNdotV, ccr * ccr)).rg;
            float cclod = ccr * textureQueryLevels(i_specular);
            vec3 ccindirectSpecular = textureLod(i_specular, ccreflectVector, cclod).rgb;

            vec3 cF = F_SchlickR(ccNdotV, vec3(0.04), ccr * ccr);
            float fc = F_Schlick(0.04, 1.0, ccNdotV);
            vec3 ccspec = (ccindirectSpecular * (cF * ccenv.x + ccenv.y));

            iblDiffuse *= 1.0 - fc;
            iblSpecular *= sqrt(1.0 - fc);
            iblSpecular += ccspec;
        }

        float baseAO = texture(g_ssao, uvs).r;
        //baseAO = 1.0;

        vec3 bouncedAO = multibounceAO(baseAO, diffuse);
        float specularAO = computeSpecularAO(NdotV, baseAO, roughness);
        finalColor.rgb = diffuse * (iblDiffuse * pow(bouncedAO, vec3(1.5))) + (iblSpecular * specularAO);
        finalColor.rgb += (directionalColor * shadow);
    }

    outFragColor = finalColor;
}