#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec3 pass_position;
layout (location = 1) in vec3 pass_normal;
layout (location = 2) in vec2 pass_uvs;
layout (location = 3) in vec3 pass_tangent;
layout (location = 4) in vec3 pass_bitangent;

layout (location = 5) in vec4 pass_last_final_pos;
layout (location = 6) in vec4 pass_final_pos;

layout (binding = 1) uniform MaterialBuffer {
	vec4 color;
	vec2 uvScale;

    float roughnessMultiplier;
	float metallicMultiplier;

	float userRoughness;
	float userMetallic;

	#ifdef DETAIL_MAP
	vec2 detailUVScale;
	vec2 detailInfluence;
	#endif

	#ifdef CLEARCOAT
	vec2 clearcoatRoughness;
		#ifdef CLEARCOAT_NORMAL
    		vec2 clearcoatUVScale;
		#endif
	#endif

    #ifdef WIND
	float windStrength;
	float windSpeed;
	#endif
} material;

layout (binding = 2) uniform sampler2D albedoTexture;
layout (binding = 3) uniform sampler2D normalTexture;
layout (binding = 4) uniform sampler2D roughnessTexture;
layout (binding = 5) uniform sampler2D metallicTexture;

#ifdef AO_MAP
layout (binding = 100) uniform sampler2D aoTexture;
#endif

#ifdef DETAIL_MAP
layout (binding = 100) uniform sampler2D detailTexture;
#endif

#ifdef CLEARCOAT
	#ifdef CLEARCOAT_NORMAL
    	layout (binding = 100) uniform sampler2D clearcoatNormalTexture;
	#endif
#endif

#ifdef GEOMETRY
    layout (location = 0) out vec4 outFragColor;
    layout (location = 1) out vec4 outFragNormal;
    layout (location = 2) out vec4 outProperties;
    layout (location = 3) out vec2 outFragVelocity;
#endif

layout (std140, push_constant) uniform pushConstants {
	mat4 model;
    mat4 lastModel;
} mats;

mat3 cotangent_frame( vec3 N, vec3 p, vec2 uv ) {
    // get edge vectors of the pixel triangle
    vec3 dp1 = dFdx( p );
    vec3 dp2 = dFdy( p );
    vec2 duv1 = dFdx( uv );
    vec2 duv2 = dFdy( uv );

    // solve the linear system
    vec3 dp2perp = cross( dp2, N );
    vec3 dp1perp = cross( N, dp1 );
    vec3 T = dp2perp * duv1.x + dp1perp * duv2.x;
    vec3 B = dp2perp * duv1.y + dp1perp * duv2.y;

    // construct a scale-invariant frame
    float invmax = inversesqrt( max( dot(T,T), dot(B,B) ) );
    return mat3( T * invmax, B * invmax, N );
}

vec2 sign_not_zero(vec2 v) {
    return fma(step(vec2(0.0), v), vec2(2.0), vec2(-1.0));
}

vec2 packNormals(vec3 v) {
        // Faster version using newer GLSL capatibilities
        v.xy /= dot(abs(v), vec3(1));

        if (v.z <= 0) v.xy = (1.0 - abs(v.yx)) * sign_not_zero(v.xy);
            return v.xy;
}

void main() {
    vec2 scaledUvs = pass_uvs * material.uvScale;

#ifdef ALPHA
    // alpha testing on the last two cascades doesnt make sense
    #if !defined(CASCADE_2) && !defined(CASCADE_3) 
        float alpha = texture(albedoTexture, scaledUvs).w;

        if(alpha < 0.5) discard;
    #endif
#endif

#ifdef GEOMETRY
    vec4 albedo = texture(albedoTexture, scaledUvs) * material.color;
    #ifdef CLEARCOAT
    albedo.a = 1.0 / 255.0;
    #else 
    albedo.a = 0;
    #endif
    outFragColor = albedo;

    mat3 TBN = transpose(mat3(pass_tangent, pass_bitangent, normalize(pass_normal)));
    #ifdef DETAIL_MAP
        vec3 t = texture(normalTexture,   scaledUvs).xyz * 2.0 - 1.0;
        vec3 u = texture(detailTexture, pass_uvs * material.detailUVScale).xyz * 2.0 - 1.0;
        u.xy *= material.detailInfluence.x;
        u = normalize(u);
        vec3 r = vec3(t + u);
        r.z = t.z;
        r = normalize(r * TBN);
        outFragNormal.xy = packNormals(r);
    #else
        vec3 normalMap = texture(normalTexture, scaledUvs).xyz * 2.0 - 1.0;
        normalMap = normalize(normalMap * TBN);
        outFragNormal.xy = packNormals(normalMap);
    #endif

    vec4 properties = vec4(1.0);
    properties.r = texture(roughnessTexture, scaledUvs).r * material.roughnessMultiplier;
    properties.r += material.userRoughness;

    properties.g = texture(metallicTexture, scaledUvs).r * material.metallicMultiplier;
    properties.g += material.userMetallic;
#ifdef AO_MAP
    properties.b = texture(aoTexture, scaledUvs).r;
#else
    properties.b = 1.0;
#endif

#ifdef CLEARCOAT
    #ifdef CLEARCOAT_NORMAL
        vec3 clearNormal = texture(clearcoatNormalTexture, pass_uvs * material.clearcoatUVScale).xyz * 2.0 - 1.0;
        clearNormal = normalize(clearNormal * TBN);
        outFragNormal.zw = packNormals(clearNormal);
    #else
        outFragNormal.zw = packNormals(pass_normal);
    #endif
    
    properties.a = material.clearcoatRoughness.x;
#else 
    outFragNormal.zw = vec2(0.0);
#endif
    outProperties = properties;

    vec2 a = (pass_final_pos.xy / pass_final_pos.w) * 0.5 + 0.5;
    vec2 b = (pass_last_final_pos.xy / pass_last_final_pos.w) * 0.5 + 0.5;
    outFragVelocity = (a - b);
#endif
}
