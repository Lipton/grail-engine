#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec2 uvs;

layout (binding = 0) uniform sampler2D g_gtao;
layout (binding = 1) uniform sampler2D g_depth;

layout (std140, push_constant) uniform pushConstants {
    vec4 texelSize;
    vec2 direction;
	vec2 offset;
} constants;

layout (binding = 2) uniform UBO {
   	mat4 projection;
	mat4 inverseProjection;

	mat4 view;
	mat4 inverseView;

	mat4 combined;
    mat4 lastCombined;
	mat4 inverseCombined;

	vec4 position;
	vec4 viewDirection;
	vec4 viewport;

    mat4 shadowCascades[4];
	vec4 cascadeSplits[4];

	vec2 time;
	vec2 subsample;
	//vec4 pad[13];
} camera;

layout (location = 0) out float outFragColor;

#define KERNEL_RADIUS 4
#define _AO_Sharpeness 0.01


void FetchAoAndDepth(vec2 uv, inout float ao, inout float depth) {
    vec2 vals = texture(g_gtao, uv).rg;
    

	ao = vals.r;
	depth = vals.g;
}

float CrossBilateralWeight(float r, float d, float d0) {
	const float BlurSigma = float(KERNEL_RADIUS) * 0.5;
	const float BlurFalloff = 1 / (2 * BlurSigma * BlurSigma);

    float dz = (d0 - d) * camera.viewport.w * _AO_Sharpeness;
	return exp2(-r * r * BlurFalloff - dz * dz);
}

void ProcessSample(vec2 aoz, float r, float d0, inout float totalAO, inout float totalW) {
	float w = CrossBilateralWeight(r, d0, aoz.y);
	totalW += w;
	totalAO += w * aoz.x;
}

void ProcessRadius(vec2 uv0, vec2 deltaUV, float d0, inout float totalAO, inout float totalW) {
	float ao, z;
	vec2 uv;
	float r = 1;

	for (; r <= KERNEL_RADIUS / 2; r += 1) {
		uv = uv0 + r * deltaUV;
		FetchAoAndDepth(uv, ao, z);
		ProcessSample(vec2(ao, z), r, d0, totalAO, totalW);
	}

	for (; r <= KERNEL_RADIUS; r += 2) {
		uv = uv0 + (r + 0.5) * deltaUV;
		FetchAoAndDepth(uv, ao, z);
		ProcessSample(vec2(ao, z), r, d0, totalAO, totalW);
	}
		
}

vec2 BilateralBlur(vec2 uv0) {
	float totalAO, depth;
	FetchAoAndDepth(uv0, totalAO, depth);
	float totalW = 1;

	vec2 offset = constants.texelSize.xy;
		
	ProcessRadius(uv0, vec2(offset.x, 0), depth, totalAO, totalW);
	ProcessRadius(uv0, vec2(-offset.x, 0), depth, totalAO, totalW);

    ProcessRadius(uv0, vec2(0, offset.y), depth, totalAO, totalW);
	ProcessRadius(uv0, vec2(0, -offset.y), depth, totalAO, totalW);

	totalAO /= totalW;
	return vec2(totalAO, depth);
}

const float kernel_radius = 4;

float BlurFunc(vec2 uv, float r, float center_c, float center_d, inout float w_total) {
	vec2 aoz = texture(g_gtao, uv).xy;
	float c = aoz.x;
	float d = aoz.y;

	const float BlurSigma = float(kernel_radius) * 0.5;
	const float BlurFalloff = 1.0 / (2.0 * BlurSigma * BlurSigma);
	
	float ddiff = (d - center_d) * _AO_Sharpeness;
	float w = exp2(-r * r * BlurFalloff - ddiff * ddiff);

	w_total += w;

	return c * w;
}

void main() {
	/*vec2 aoz = texture(g_gtao, uvs).xy;

	float center_c = aoz.x;
	float center_d = aoz.y;

	float c_total = center_c;
	float w_total = 1.0;

	for(float r = 1; r <= kernel_radius; ++r) {
		vec2 uv = uvs + vec2(constants.direction.x, 0) * r;
		c_total += BlurFunc(uv, r, center_c, center_d, w_total);
	}

	for(float r = 1; r <= kernel_radius; ++r) {
		vec2 uv = uvs - vec2(constants.direction.x, 0) * r;
		c_total += BlurFunc(uv, r, center_c, center_d, w_total);
	}


	for(float r = 1; r <= kernel_radius; ++r) {
		vec2 uv = uvs + vec2(0, constants.direction.y) * r;
		c_total += BlurFunc(uv, r, center_c, center_d, w_total);
	}

	for(float r = 1; r <= kernel_radius; ++r) {
		vec2 uv = uvs - vec2(0, constants.direction.y) * r;
		c_total += BlurFunc(uv, r, center_c, center_d, w_total);
	}

	outFragColor = c_total / w_total;*/

    vec2 result = BilateralBlur(uvs + constants.offset * 0.5);
    //result += BilateralBlur(uvs, vec2(0.0, 1.0) * constants.texelSize.xy);
    outFragColor = result.x;
}