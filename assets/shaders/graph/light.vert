#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec2 inPos;
layout (location = 1) in vec2 inUvs;

layout (binding = 9) uniform UBO {
   	mat4 projection;
	mat4 inverseProjection;

	mat4 view;
	mat4 inverseView;

	mat4 combined;
    mat4 lastCombined;
	mat4 inverseCombined;

	vec4 position;
	vec4 viewDirection;
	vec4 viewport;

    mat4 shadowCascades[4];
	vec4 cascadeSplits[4];

	vec2 time;
	vec2 subsample;
	//vec4 pad[13];
} camera;

out gl_PerVertex {
    vec4 gl_Position;
};

layout (location = 0) out vec2 pass_uvs;
layout (location = 1) smooth out vec3 pass_eye;

void main() {
    mat3 inverseView = transpose(mat3(camera.view));
    vec3 unprojected = (camera.inverseProjection * vec4(inPos, 1.0, 1.0)).xyz;

	gl_Position = vec4(inPos, 0.0, 1.0);

    pass_eye = inverseView * unprojected;
    pass_uvs = inUvs;
}
