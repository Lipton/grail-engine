#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec2 uvs;

layout (binding = 0) uniform sampler2D tileMax;

layout (location = 0) out vec2 outFragColor;

// CONSTANTS
const vec2 VHALF =  vec2(0.5, 0.5);
const vec2 VONE  =  vec2(1.0, 1.0);
const vec2 VTWO  =  vec2(2.0, 2.0);

const ivec2 IVZERO = ivec2(0, 0);
const ivec2 IVONE  = ivec2(1, 1);

const vec4 GRAY = vec4(0.5, 0.5, 0.5, 1.0);

// Bias/scale helper functions
vec2 readBiasScale(vec2 v)
{
    return v;
}

vec2 writeBiasScale(vec2 v)
{
    return v;
}

float compareWithNeighbor(ivec2 tileCorner, int s, int t, float maxMagnitudeSquared)
{
    ivec2 ivTexSizeMinusOne = textureSize(tileMax, 0) - IVONE;
    ivec2 ivOffset = ivec2(s, t);
    vec2 vVelocity = readBiasScale(texelFetch(tileMax,
        clamp(tileCorner + ivOffset, IVZERO, ivTexSizeMinusOne), 0).rg);
        float fMagnitudeSquared = dot(vVelocity, vVelocity);

    if(maxMagnitudeSquared < fMagnitudeSquared)
    {
        float fDisplacement = abs(float(ivOffset.x)) + abs(float(ivOffset.y));
        vec2 vOrientation = sign(vec2(ivOffset) * vVelocity);
        float fDistance = float(vOrientation.x + vOrientation.y);

        if(abs(fDistance) == fDisplacement)
        {
            outFragColor.rg = writeBiasScale(vVelocity);
            maxMagnitudeSquared = fMagnitudeSquared;
        }
    }
    return maxMagnitudeSquared;
}

void main() {
    outFragColor = vec2(0.0);

    ivec2 ivTileCorner = ivec2(gl_FragCoord.xy);
    float fMaxMagnitudeSquared = 0.0;

    fMaxMagnitudeSquared =
        compareWithNeighbor(ivTileCorner, -1, -1, fMaxMagnitudeSquared);
    fMaxMagnitudeSquared =
        compareWithNeighbor(ivTileCorner,  0, -1, fMaxMagnitudeSquared);
    fMaxMagnitudeSquared =
        compareWithNeighbor(ivTileCorner,  1, -1, fMaxMagnitudeSquared);
    fMaxMagnitudeSquared =
        compareWithNeighbor(ivTileCorner, -1,  0, fMaxMagnitudeSquared);
    fMaxMagnitudeSquared =
        compareWithNeighbor(ivTileCorner,  0,  0, fMaxMagnitudeSquared);
    fMaxMagnitudeSquared =
        compareWithNeighbor(ivTileCorner,  1,  0, fMaxMagnitudeSquared);
    fMaxMagnitudeSquared =
        compareWithNeighbor(ivTileCorner, -1,  1, fMaxMagnitudeSquared);
    fMaxMagnitudeSquared =
        compareWithNeighbor(ivTileCorner,  0,  1, fMaxMagnitudeSquared);
    fMaxMagnitudeSquared =
        compareWithNeighbor(ivTileCorner,  1,  1, fMaxMagnitudeSquared);
}