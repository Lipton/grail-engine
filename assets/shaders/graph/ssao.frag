#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

#define PI 3.1415926535897932384626433832795
#define PI_HALF 1.5707963267948966192313216916398
#define HALF_PI 1.5707963267948966192313216916398

const int SSAO_KERNEL_SIZE = 8;
const float SSAO_RADIUS = 10.0;

layout (binding = 0) uniform sampler2D g_depth;
layout (binding = 1) uniform sampler2D g_normal;

layout (binding = 2) uniform UBO {
   	mat4 projection;
	mat4 inverseProjection;

	mat4 view;
	mat4 inverseView;

	mat4 combined;
    mat4 lastCombined;
	mat4 inverseCombined;

	vec4 position;
	vec4 viewDirection;
	vec4 viewport;

    mat4 shadowCascades[4];
	vec4 cascadeSplits[4];

	vec2 time;
	vec2 subsample;
} camera;

layout (std140, push_constant) uniform pushConstants {
    vec4 uvNearFar;
	vec2 params;
	vec4 UVToView;
	vec2 projScale;
} constants;

layout (location = 0) in vec2 uvs;

layout (location = 0) out vec2 outAODepth;
//layout (location = 1) out vec4 outBentNormals;

vec2 sign_not_zero(vec2 v) {
    return fma(step(vec2(0.0), v), vec2(2.0), vec2(-1.0));
}

vec3 unpackNormals(vec2 packed_nrm) {
        // Version using newer GLSL capatibilities
        vec3 v = vec3(packed_nrm.xy, 1.0 - abs(packed_nrm.x) - abs(packed_nrm.y));

                if (v.z < 0) v.xy = (1.0 - abs(v.yx)) * sign_not_zero(v.xy);

    return normalize(v);
}

vec3 GetCameraVec(vec2 uv) {  
    // Returns the vector from camera to the specified position on the camera plane (uv argument), located one unit away from the camera
    // This vector is not normalized.
    // The nice thing about this setup is that the returned vector from this function can be simply multiplied with the linear depth to get pixel's position relative to camera position.
    // This particular function does not account for camera rotation or position or FOV at all (since we don't need it for AO)
    // TODO: AO is dependent on FOV, this function is not!
    // The outcome of using this simplified function is that the effective AO range is larger when using larger FOV
    // Use something more accurate to get proper FOV-independent world-space range, however you will likely also have to adjust the SSAO constants below
	float aspect = constants.uvNearFar.w / constants.uvNearFar.z;
	return vec3(uv.x * -2.0 + 1.0, uv.y * 2.0 * aspect - aspect, 1.0);
}


vec3 getGeometricNormals(float sampleDist, vec2 tc_original, vec3 ray) {
	const vec2 viewsizediv = constants.uvNearFar.xy;

	vec2 uv = tc_original + vec2(viewsizediv.x * sampleDist, 0.0);
	vec3 p1 = ray - GetCameraVec(uv) * textureLod(g_depth, uv, 0.0).x;
	
	uv = tc_original + vec2(0.0, viewsizediv.y * sampleDist);
	vec3 p2 = ray - GetCameraVec(uv) * textureLod(g_depth, uv, 0.0).x;
	
	uv = tc_original + vec2(-viewsizediv.x * sampleDist, 0.0);
	vec3 p3 = ray - GetCameraVec(uv) * textureLod(g_depth, uv, 0.0).x;
	
	uv = tc_original + vec2(0.0, -viewsizediv.y * sampleDist);
	vec3 p4 = ray - GetCameraVec(uv) * textureLod(g_depth, uv, 0.0).x;
	
	vec3 normal1 = normalize(cross(p1, p2));
	vec3 normal2 = normalize(cross(p3, p4));

	vec3 normal = normalize(normal1 + normal2);

	return normal;
}

#define SSAO_LIMIT 200
#define SSAO_SAMPLES 5
#define SSAO_RADIUS 4
#define SSAO_FALLOFF 1.0
#define SSAO_THICKNESSMIX 0.2
#define SSAO_MAX_STRIDE 5

float GTAOFastAcos(float x) {
    float res = -0.156583 * abs(x) + PI_HALF;
    res *= sqrt(1.0 - abs(x));
    return x >= 0 ? res : PI - res;
}
 
float IntegrateArc(float h1, float h2, float n) {
    float cosN = cos(n);
    float sinN = sin(n);
    return 0.25 * (-cos(2.0 * h1 - n) + cosN + 2.0 * h1 * sinN - cos(2.0 * h2 - n) + cosN + 2.0 * h2 * sinN);
}

vec3 Visualize_0_3(float x) {
    const vec3 color0 = vec3(1.0, 0.0, 0.0);
    const vec3 color1 = vec3(1.0, 1.0, 0.0);
    const vec3 color2 = vec3(0.0, 1.0, 0.0);
    const vec3 color3 = vec3(0.0, 1.0, 1.0);
    vec3 color = mix(color0, color1, clamp(x - 0.0, 0.0, 1.0));
    color = mix(color, color2, clamp(x - 1.0, 0.0, 1.0));
    color = mix(color, color3, clamp(x - 2.0, 0.0, 1.0));
    return color;
}

void SliceSample(vec2 tc_base, vec2 aoDir, int i, float targetMip, vec3 ray, vec3 v, inout float closest) {
    vec2 uv = tc_base + aoDir * i;
    float depth = textureLod(g_depth, uv, targetMip).x;
    // Vector from current pixel to current slice sample
    vec3 p = GetCameraVec(uv) * depth - ray;
    // Cosine of the horizon angle of the current sample
    float current = dot(v, normalize(p));
    // Linear falloff for samples that are too far away from current pixel
    float falloff = clamp((SSAO_RADIUS - length(p)) / SSAO_FALLOFF, 0.0, 1.0);
    if(current > closest)
        closest = mix(closest, current, falloff);
    // Helps avoid overdarkening from thin objects
    closest = mix(closest, current, SSAO_THICKNESSMIX * falloff);
}

float GTAO_Noise(vec2 position)
{
	return fract(52.9829189 * fract(dot(position, vec2( 0.06711056, 0.00583715))));
}

void main() {
 	vec2 tc_original = uvs;
   
    // Depth of the current pixel
    float dhere = (textureLod(g_depth, tc_original, 0.0).x);

	float visibility = 1.0;
	vec3 bentNormal = vec3(0.0);

	if(dhere < camera.viewport.w) {
		// Vector from camera to the current pixel's position
		vec3 ray = GetCameraVec(tc_original) * dhere;
	
		const vec2 viewsizediv = constants.uvNearFar.xy;

		vec3 normal = unpackNormals(texture(g_normal, uvs).rg);
		normal = normal * mat3(camera.inverseView);
		normal = normalize(normal);
		normal.xz = -normal.xz;

		//vec3 normal = getGeometricNormals(1.0, tc_original, ray);

		//normal = normalize(normal1 + normal2);

		// Calculate the distance between samples (direction vector scale) so that the world space AO radius remains constant but also clamp to avoid cache trashing
		// viewsizediv = vec2(1.0 / sreenWidth, 1.0 / screenHeight)
		float stride = min((1.0 / length(ray)) * SSAO_LIMIT, SSAO_MAX_STRIDE);
		vec2 dirMult = viewsizediv.xy * stride;
		// Get the view vector (normalized vector from pixel to camera)
		vec3 v = normalize(-ray);

		vec2 fragCoord = (uvs + (camera.subsample * 20)) * constants.uvNearFar.zw;
	
		// Calculate slice direction from pixel's position
		float dirAngle = (PI / 12.0) * (((int(fragCoord.x) + int(fragCoord.y) & 3) << 2) + (int(fragCoord.x) & 3)) + constants.params.x;
		vec2 aoDir = dirMult * vec2(sin(dirAngle), cos(dirAngle));
	
		// Project world space normal to the slice plane
		vec3 toDir = GetCameraVec(tc_original + aoDir);
		vec3 planeNormal = normalize(cross(v, -toDir));
		vec3 projectedNormal = normal - planeNormal * dot(normal, planeNormal);
	
		// Calculate angle n between view vector and projected normal vector
		vec3 projectedDir = normalize(normalize(toDir) + v);
		float n = GTAOFastAcos(dot(-projectedDir, normalize(projectedNormal))) - PI_HALF;
	
		// Init variables
		float c1 = -1.0;
		float c2 = -1.0;
	
		vec2 tc_base = tc_original + aoDir * (0.25 * ((int(fragCoord.y) - int(fragCoord.x)) & 3) - 0.375 + constants.params.y);
	
		const float minMip = 1.0;
		const float maxMip = 5.0;
		const float mipScale = 1.0 / textureQueryLevels(g_depth);
	
		float targetMip = floor(clamp(pow(stride, 1.3) * mipScale, minMip, maxMip));
	
		// Find horizons of the slice
		for(int i = -1; i >= -SSAO_SAMPLES; i--) {
			SliceSample(tc_base, aoDir, i, targetMip, ray, v, c1);
		}

		for(int i = 1; i <= SSAO_SAMPLES; i++) {
			SliceSample(tc_base, aoDir, i, targetMip, ray, v, c2);
		}
	
		// Finalize
		float h1a = -GTAOFastAcos(c1);
		float h2a = GTAOFastAcos(c2);
	
		// Clamp horizons to the normal hemisphere
		float h1 = n + max(h1a - n, -PI_HALF);
		float h2 = n + min(h2a - n, PI_HALF);

		/*float bentAngle = (h1 + h2) * 0.5;
		vec3 tangent = cross(toDir, planeNormal);
		bentNormal = -toDir * cos(bentAngle) - tangent * sin(bentAngle);*/
	
		visibility = mix(1.0, IntegrateArc(h1, h2, n), length(projectedNormal));
		//visibility = pow(visibility, 0.5);	 
	}
   
    outAODepth = vec2(clamp(visibility, 0.0, 1.0), dhere);
//	outBentNormals = vec4(bentNormal, 1.0);
}