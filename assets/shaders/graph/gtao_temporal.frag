#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec2 uvs;

layout (binding = 0) uniform sampler2D g_gtao;
layout (binding = 1) uniform sampler2D g_gtaoHistory;
layout (binding = 2) uniform sampler2D g_vel;
layout (binding = 3) uniform sampler2D g_depth;

layout (std140, push_constant) uniform pushConstants {
    vec4 texelSize;
} constants;

layout (location = 0) out vec2 outFragColor;

float rcp(float value) {
    return 1.0 / value;
}

float Luma4(vec3 Color)
{
    return (Color.g * 2) + (Color.r + Color.b);
}

float HdrWeight4(vec3 Color, float Exposure)
{
    return rcp(Luma4(Color) * Exposure + 4);
}


void ResolverAABB(sampler2D currColor, float Sharpness, float ExposureScale, float AABBScale, vec2 uv, vec2 screenSize, inout vec4 minColor, inout vec4 maxColor, inout vec4 filterColor) {
    vec4 TopLeft = texture(currColor, uv + (vec2(-1, -1) / screenSize));
    vec4 TopCenter = texture(currColor, uv + (vec2(0, -1) / screenSize));
    vec4 TopRight = texture(currColor, uv + (vec2(1, -1) / screenSize));
    vec4 MiddleLeft = texture(currColor, uv + (vec2(-1,  0) / screenSize));
    vec4 MiddleCenter = texture(currColor, uv + (vec2(0,  0) / screenSize));
    vec4 MiddleRight = texture(currColor, uv + (vec2(1,  0) / screenSize));
    vec4 BottomLeft = texture(currColor, uv + (vec2(-1,  1) / screenSize));
    vec4 BottomCenter = texture(currColor, uv + (vec2(0,  1) / screenSize));
    vec4 BottomRight = texture(currColor, uv + (vec2(1,  1) / screenSize));
    
        float SampleWeights[9];
        SampleWeights[0] = HdrWeight4(TopLeft.rgb, ExposureScale);
        SampleWeights[1] = HdrWeight4(TopCenter.rgb, ExposureScale);
        SampleWeights[2] = HdrWeight4(TopRight.rgb, ExposureScale);
        SampleWeights[3] = HdrWeight4(MiddleLeft.rgb, ExposureScale);
        SampleWeights[4] = HdrWeight4(MiddleCenter.rgb, ExposureScale);
        SampleWeights[5] = HdrWeight4(MiddleRight.rgb, ExposureScale);
        SampleWeights[6] = HdrWeight4(BottomLeft.rgb, ExposureScale);
        SampleWeights[7] = HdrWeight4(BottomCenter.rgb, ExposureScale);
        SampleWeights[8] = HdrWeight4(BottomRight.rgb, ExposureScale);

        float TotalWeight = SampleWeights[0] + SampleWeights[1] + SampleWeights[2] + SampleWeights[3] + SampleWeights[4] + SampleWeights[5] + SampleWeights[6] + SampleWeights[7] + SampleWeights[8];  
        vec4 Filtered = (TopLeft * SampleWeights[0] + TopCenter * SampleWeights[1] + TopRight * SampleWeights[2] + MiddleLeft * SampleWeights[3] + MiddleCenter * SampleWeights[4] + MiddleRight * SampleWeights[5] + BottomLeft * SampleWeights[6] + BottomCenter * SampleWeights[7] + BottomRight * SampleWeights[8]) / TotalWeight;

    vec4 m1, m2, mean, stddev;

    //
        m1 = TopLeft + TopCenter + TopRight + MiddleLeft + MiddleCenter + MiddleRight + BottomLeft + BottomCenter + BottomRight;
        m2 = TopLeft * TopLeft + TopCenter * TopCenter + TopRight * TopRight + MiddleLeft * MiddleLeft + MiddleCenter * MiddleCenter + MiddleRight * MiddleRight + BottomLeft * BottomLeft + BottomCenter * BottomCenter + BottomRight * BottomRight;

        mean = m1 / 9;
        stddev = sqrt(m2 / 9 - mean * mean);
        
        minColor = mean - AABBScale * stddev;
        maxColor = mean + AABBScale * stddev;
    //
	/* 
        minColor = min(TopLeft, min(TopCenter, min(TopRight, min(MiddleLeft, min(MiddleCenter, min(MiddleRight, min(BottomLeft, min(BottomCenter, BottomRight))))))));
        maxColor = max(TopLeft, max(TopCenter, max(TopRight, max(MiddleLeft, max(MiddleCenter, max(MiddleRight, max(BottomLeft, max(BottomCenter, BottomRight))))))));
            
        vec4 center = (minColor + maxColor) * 0.5;
        minColor = (minColor - center) * AABBScale + center;
        maxColor = (maxColor - center) * AABBScale + center;
*/
    
        filterColor = Filtered;
        minColor = min(minColor, Filtered);
        maxColor = max(maxColor, Filtered);
}

vec2 getClosestFragment(in vec2 uv) {
    const vec2 k = constants.texelSize.xy;

    const vec4 neighborhood = textureGather(g_depth, uv);
    vec3 result = vec3(0.0, 0.0, texture(g_depth, uv).r);

    result = mix(result, vec3(-1.0, -1.0, neighborhood.x), step(neighborhood.x, result.z));
    result = mix(result, vec3(1.0, -1.0, neighborhood.y), step(neighborhood.y, result.z));
    result = mix(result, vec3(-1.0, 1.0, neighborhood.z), step(neighborhood.z, result.z));
    result = mix(result, vec3(1.0, 1.0, neighborhood.w), step(neighborhood.w, result.z));


    return (uv + result.xy * k);
}



#define _AO_TemporalScale 10.0
#define _AO_TemporalResponse 0.95

#define TAA_MINIMUM_MOTION_NUDGE 1.28318530718
#define TAA_MINIMUM_MOTION_SENSITIVITY 0.05
#define TAA_MAXIMUM_MOTION_NUDGE 0.5
#define TAA_MAXIMUM_MOTION_SENSITIVITY 0.055

void main() {
    vec2 uv = uvs;
    vec2 velocity = texture(g_vel, (uvs)).rg;

    vec4 filterColor = vec4(0.0);
    vec4 minColor, maxColor = vec4(0.0);

    ResolverAABB(g_gtao, 0 , 0, 10, uv, constants.texelSize.zw, minColor, maxColor, filterColor);

    vec4 currColor = texture(g_gtao, uv);
    currColor.y = length(velocity);

    vec4 lastColor = texture(g_gtaoHistory, uv - velocity);
    lastColor.x = clamp(lastColor.x, minColor.x, maxColor.x);
    if (uv.x < 0 || uv.x > 1 || uv.y < 0 || uv.y > 1) {
		lastColor = filterColor;
	}
    
    float weigth = clamp(clamp(_AO_TemporalResponse, 0, 0.999) * (1.0 - length(lastColor.y) ), 0.0, 1.0);

    currColor.xy = mix(currColor.xy, lastColor.xy, vec2(weigth, 0.95));
    //currColor.y *= 0.95;

    outFragColor = vec2(currColor.xy);
    //outFragColor = vec4(1.0);
}