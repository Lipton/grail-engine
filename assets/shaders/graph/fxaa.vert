#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec2 inPos;
layout (location = 1) in vec2 inUvs;

layout (location = 0) out vec2 pass_uvs;
layout (location = 1) out vec2 v_rgbNW;
layout (location = 2) out vec2 v_rgbNE;
layout (location = 3) out vec2 v_rgbSW;
layout (location = 4) out vec2 v_rgbSE;
layout (location = 5) out vec2 v_rgbM;
layout (location = 6) out vec2 v_pos;

layout (std140, push_constant) uniform pushConstants {
    vec4 texelSize;
} constants;

void texcoords(vec2 fragCoord, vec2 resolution,
			out vec2 v_rgbNW, out vec2 v_rgbNE,
			out vec2 v_rgbSW, out vec2 v_rgbSE,
			out vec2 v_rgbM) {
	vec2 inverseVP = 1.0 / resolution.xy;
	v_rgbNW = (fragCoord + vec2(-1.0, -1.0)) * inverseVP;
	v_rgbNE = (fragCoord + vec2(1.0, -1.0)) * inverseVP;
	v_rgbSW = (fragCoord + vec2(-1.0, 1.0)) * inverseVP;
	v_rgbSE = (fragCoord + vec2(1.0, 1.0)) * inverseVP;
	v_rgbM = vec2(fragCoord * inverseVP);
}


out gl_PerVertex {
    vec4 gl_Position;
};

void main() {
	gl_Position = vec4(inPos, 0.0, 1.0);

    pass_uvs = inUvs;
    v_pos = inPos.xy;
    texcoords(inPos.xy, constants.texelSize.xy, v_rgbNW, v_rgbNE, v_rgbSW, v_rgbSE, v_rgbM);
}
