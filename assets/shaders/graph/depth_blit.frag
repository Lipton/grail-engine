#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec2 uvs;

layout (binding = 0) uniform sampler2D g_depth;

layout (std140, push_constant) uniform pushConstants {
    vec2 minMax;
} constants;

layout (location = 0) out float outFragColor;

float linearize_depth(float d,float zNear,float zFar) {
    return zNear * zFar / (zFar + d * (zNear - zFar));
}

void main() {
    outFragColor = linearize_depth(texture(g_depth, uvs).r, constants.minMax.x, constants.minMax.y);
}