#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec3 pass_position;
layout (location = 1) in vec3 pass_normals;
layout (location = 2) in vec2 pass_uvs;
layout (location = 3) in vec3 pass_tangent;
layout (location = 4) in vec3 pass_bitangent;

layout (binding = 0) uniform samplerCube g_irradiance;

layout (location = 0) out vec4 outFragColor;

void main() {
    vec3 lightPos = vec3(0.5, 0.5, 0.5);
    vec3 lightDir = lightPos;
    float d = max(dot(normalize(pass_normals), lightDir), 0.0);
    vec3 diffuse = vec3(1.0) * d;

    vec3 irradiance = texture(g_irradiance, pass_normals.rgb * vec3(1, -1, 1)).rgb;

    outFragColor = vec4(diffuse + (irradiance * 0.2), 1.0);
}