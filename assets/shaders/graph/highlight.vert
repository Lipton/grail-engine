#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec3 inPos;
layout (location = 1) in vec3 inNormals;
layout (location = 2) in vec3 inTangent;
layout (location = 3) in vec3 inUvs;

layout (std140, push_constant) uniform pushConstants {
    mat4 combined;
	mat4 model;
} mats;

out gl_PerVertex {
    vec4 gl_Position;
};

void main() {
	vec4 worldPosition = mats.model * vec4(inPos, 1.0);

	gl_Position = mats.combined * worldPosition;
}
