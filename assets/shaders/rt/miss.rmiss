#version 460
#extension GL_NV_ray_tracing : require

struct RayPayload {
	vec3 color;
	float distance;
	vec3 normal;
	float reflector;

	float ao;
};

layout(location = 0) rayPayloadInNV RayPayload rayPayload;
layout(binding = 6, set = 0) uniform samplerCube envTex;
layout(binding = 7, set = 0) uniform sampler2D g_normal;
layout(binding = 8, set = 0) uniform sampler2D g_depth;

void main() {
	vec3 unitDir = normalize(gl_WorldRayDirectionNV);

	rayPayload.color = pow(texture(envTex, unitDir).rgb, vec3(2.2));

	rayPayload.distance = -1.0f;
	rayPayload.normal = vec3(0.0f);
	rayPayload.reflector = 0.0f;

	rayPayload.ao = 1.0;
}