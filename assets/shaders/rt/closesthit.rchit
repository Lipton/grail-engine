#version 460
#extension GL_NV_ray_tracing : require
#extension GL_EXT_nonuniform_qualifier : enable


#define PI 3.14159265358979323846


struct RayPayload {
	vec3 color;
	float distance;
	vec3 normal;
	float reflector;

	float ao;
};

layout(location = 0) rayPayloadInNV RayPayload rayPayload;

hitAttributeNV vec3 attribs;

layout(binding = 0, set = 0) uniform accelerationStructureNV topLevelAS;
layout(binding = 2, set = 0) uniform CameraProperties {
   	mat4 projection;
	mat4 inverseProjection;

	mat4 view;
	mat4 inverseView;

	mat4 combined;
    mat4 lastCombined;
	mat4 inverseCombined;

	vec4 position;
	vec4 viewDirection;

    mat4 shadowCascades[4];
	vec4 cascadeSplits[4];
} cam;
layout(binding = 3, set = 0) buffer Vertices { vec4 v[]; } vertices[60];
layout(binding = 4, set = 0) buffer Indices { uint i[]; } indices[60];
layout(binding = 5, set = 0) uniform sampler2D diffuseTex;
layout(binding = 6, set = 0) uniform samplerCube envTex;
layout(binding = 7, set = 0) uniform sampler2D g_normal;
layout(binding = 8, set = 0) uniform sampler2D g_depth;
layout(binding = 9, set = 0) uniform sampler2DArray g_shadow;

struct Vertex {
  vec3 pos;
  vec3 normal;
  vec3 tangent;
  vec2 uv;
  float _pad0;
};

Vertex unpack(int iindex, uint index) {
	vec4 d0 = vertices[iindex].v[3 * index + 0];
	vec4 d1 = vertices[iindex].v[3 * index + 1];
	vec4 d2 = vertices[iindex].v[3 * index + 2];

	Vertex v;
	v.pos = d0.xyz;
	v.normal = vec3(d0.w, d1.x, d1.y);
	v.tangent = vec3(d1.z, d1.w, d2.x);
	v.uv = vec2(d2.y, d2.z);
	return v;
}

vec2 get_shadow_offsets(vec3 N, vec3 L) {
    float cos_alpha = clamp(dot(N, L), 0, 1);
    float offset_scale_N = sqrt(1 - cos_alpha*cos_alpha); // sin(acos(L·N))
    float offset_scale_L = offset_scale_N / cos_alpha;    // tan(acos(L·N))
    return vec2(offset_scale_N, min(2, offset_scale_L));
}

float calculateShadowing(vec3 normal, vec4 lightSpacePos, vec3 lightDir, uint cascadeIndex) {
	vec3 projCoords = lightSpacePos.xyz / lightSpacePos.w;

    float closestDepth = texture(g_shadow, vec3(projCoords.xy, cascadeIndex)).r;
	float currDepth = projCoords.z;
	
    vec2 bias = get_shadow_offsets(normal, lightDir);

    float shadow = 0.0;
	vec2 texelSize = 1.0 / textureSize(g_shadow, 0).xy;

#ifndef PLATFORM_MOBILE
	#define LOOPS 3
#else
    #define LOOPS 1
#endif
			float sum = 0, count = 0;
			float x, y;
			for (y = -LOOPS; y <= LOOPS; y += 1.0)
			for (x = -LOOPS; x <= LOOPS; x += 1.0) {
				// Can't use texture(Proj)Offset directly since it expects the offset to be a constant value,
				// i.e. no loops, so instead we calculate the offset manually (given the texture size)
				vec2 offset = vec2(x, y);
				float dp = texture(g_shadow, vec3(projCoords.xy + (offset * texelSize), cascadeIndex)).r;
                if(currDepth - dp > bias.x * 0.005) {
                    sum += 1.0;
                }
				count++;
			}

			shadow = sum / count;

        return 1.0 - shadow;
}

const mat4 biasMat = mat4( 
	0.5, 0.0, 0.0, 0.0,
	0.0, 0.5, 0.0, 0.0,
	0.0, 0.0, 1.0, 0.0,
	0.5, 0.5, 0.0, 1.0 
);

vec3 BRDFA(vec3 baseColor, vec3 l, vec3 v, vec3 n, vec3 X, vec3 Y, float metallic, float roughness, float attenuation, vec3 color) {
    float alphaRoughness = roughness * roughness;
    float r = alphaRoughness;

    vec3 f0 = vec3(0.04);
    vec3 diffuseColor = baseColor.rgb * (vec3(1.0) - f0);
    diffuseColor *= 1.0 - metallic;
    vec3 specularColor = mix(f0, baseColor.rgb, metallic);
   
    float reflectance = max(max(specularColor.r, specularColor.g), specularColor.b);

    float reflectance90 = clamp(reflectance * 25.0, 0.0, 1.0);
    vec3 specularEnvironmentR0 = specularColor.rgb;
    vec3 specularEnvironmentR90 = vec3(1.0, 1.0, 1.0) * reflectance90;

    vec3 h = normalize(l + v);
    
    float NdotL = clamp(dot(n, l), 0.001, 1.0);
    float NdotV = clamp(abs(dot(n, v)), 0.001, 1.0);
    float NdotH = clamp(dot(n, h), 0.0, 1.0);
    float LdotH = clamp(dot(l, h), 0.0, 1.0);
    float VdotH = clamp(dot(v, h), 0.0, 1.0);

    vec3 F = specularEnvironmentR0 + (specularEnvironmentR90 - specularEnvironmentR0) * pow(clamp(1.0 - VdotH, 0.0, 1.0), 5.0);

    float attenuationL = 2.0 * NdotL / (NdotL + sqrt(r * r + (1.0 - r * r) * (NdotL * NdotL)));
    float attenuationV = 2.0 * NdotV / (NdotV + sqrt(r * r + (1.0 - r * r) * (NdotV * NdotV)));
    float G = attenuationL * attenuationV;

    float roughnessSq = alphaRoughness * alphaRoughness;
    float f = (NdotH * roughnessSq - NdotH) * NdotH + 1.0;
    float D = roughnessSq / (PI * f * f);

    vec3 diffuseContrib = (1.0 - F) * (diffuseColor / PI);
    vec3 specContrib = F * G * D / (4.0 * NdotL * NdotV);

    return NdotL * color * (diffuseContrib + specContrib);
}

void main() {
	const vec2 pixelCenter = vec2(gl_LaunchIDNV.xy) + vec2(0.5);
	const vec2 inUV = pixelCenter/vec2(gl_LaunchSizeNV.xy);
	vec2 d = inUV * 2.0 - 1.0;

	int iindex = gl_InstanceCustomIndexNV;

	ivec3 index = ivec3(indices[iindex].i[3 * gl_PrimitiveID], indices[iindex].i[3 * gl_PrimitiveID + 1], indices[iindex].i[3 * gl_PrimitiveID + 2]);

	Vertex v0 = unpack(iindex, index.x);
	Vertex v1 = unpack(iindex, index.y);
	Vertex v2 = unpack(iindex, index.z);

	// Interpolate normal
	const vec3 barycentricCoords = vec3(1.0f - attribs.x - attribs.y, attribs.x, attribs.y);
	vec3 normal = normalize(v0.normal * barycentricCoords.x + v1.normal * barycentricCoords.y + v2.normal * barycentricCoords.z);
	vec2 uvs = (v0.uv * barycentricCoords.x + v1.uv * barycentricCoords.y + v2.uv * barycentricCoords.z);
	vec3 position = (v0.pos * barycentricCoords.x + v1.pos * barycentricCoords.y + v2.pos * barycentricCoords.z);

	vec3 albedo = pow(texture(diffuseTex, uvs).rgb, vec3(2.2));
	

	vec3 lightDirection = vec3(-0.5, 0.724, 0.5);
	mat3 normalMat = transpose(inverse(mat3(gl_ObjectToWorldNV)));

	vec3 worldNormals = normalize(normalMat * normal);
	vec3 worldPosition = gl_WorldRayOriginNV + (gl_WorldRayDirectionNV * gl_RayTmaxNV);
	vec3 viewPosition = vec3(0.0);

	{
		vec4 posProj = vec4(d.x, d.y, gl_RayTmaxNV, 1.0);
		vec4 posView = cam.inverseProjection * posProj;
		posView /= posView.w;

		viewPosition = posView.rgb;
	}
	
	uint cascadeIndex = 0;
            for(uint i = 0; i < 4 - 1; ++i) {
                if(viewPosition.z < cam.cascadeSplits[i].x) {	
                    cascadeIndex = i + 1;
                }
            }

	 vec4 shadowCoord = (biasMat * cam.shadowCascades[cascadeIndex]) * vec4(worldPosition.xyz, 1.0);
     float shadow = 0;	

    shadow = calculateShadowing(worldNormals.xyz, shadowCoord, lightDirection, cascadeIndex);

	vec3 viewDir = normalize(cam.position.rgb - worldPosition.rgb);

	vec3 tangent = normalize( cross( vec3(0, 1, 0), worldNormals.rgb ) );
    vec3 bitangent = normalize( cross( worldNormals.rgb, tangent ) );

  	vec3 directionalColor = BRDFA(albedo.rgb, lightDirection, viewDir, worldNormals.rgb, tangent, bitangent, 0.0, 0.0, 1.0, vec3(10.0));

	vec3 irradiance = texture(envTex, normal.rgb * vec3(1, -1, 1)).rgb;

	rayPayload.color = (albedo * irradiance) + (directionalColor * shadow);
	rayPayload.distance = gl_RayTmaxNV;
	rayPayload.normal = normal;
	rayPayload.reflector = gl_InstanceCustomIndexNV == 60 - 18 ? 1.0 : 0.0;
	rayPayload.ao = 0.0;
}
