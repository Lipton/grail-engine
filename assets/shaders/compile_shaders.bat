glslangvalidator -V glsl/gpass.vert -o gpass.vert.spv
glslangvalidator -V glsl/gpass.frag -o gpass.frag.spv

glslangvalidator -V glsl/gpass_alpha.vert -o gpass_alpha.vert.spv
glslangvalidator -V glsl/gpass_alpha.frag -o gpass_alpha.frag.spv

glslangvalidator -V glsl/gpass_depth.vert -o gpass_depth.vert.spv
glslangvalidator -V glsl/gpass_depth.frag -o gpass_depth.frag.spv

glslangvalidator -V glsl/present.vert -o present.vert.spv
glslangvalidator -V glsl/present_fxaa.vert -o present_fxaa.vert.spv
glslangvalidator -V glsl/present_fxaa.frag -o present_fxaa.frag.spv
glslangvalidator -V glsl/present.frag -o present.frag.spv

glslangvalidator -V glsl/light.vert -o light.vert.spv
glslangvalidator -V glsl/light.frag -o light.frag.spv

glslangvalidator -V glsl/brdfLUT.frag -o brdfLUT.frag.spv

glslangvalidator -V glsl/cube.vert -o cube.vert.spv

glslangvalidator -V glsl/irradiance.frag -o irradiance.frag.spv

glslangvalidator -V glsl/prefilterSpec.frag -o prefilterSpec.frag.spv

glslangvalidator -V glsl/ssao.frag -o ssao.frag.spv

glslangvalidator -V glsl/depth_downsample.frag -o depth_downsample.frag.spv

glslangvalidator -V glsl/ssao_blur.frag -o ssao_blur.frag.spv

glslangvalidator -V glsl/cascade.vert -o cascade.vert.spv
glslangvalidator -V glsl/cascade.frag -o cascade.frag.spv

glslangvalidator -V glsl/TAA_resolve.frag -o TAA_resolve.frag.spv

glslangvalidator -V glsl/TAA.frag -o TAA.frag.spv

glslangvalidator -V glsl/SSR_cast.frag -o SSR_cast.frag.spv

glslangvalidator -V glsl/compute_test.comp -o compute_test.comp.spv

glslangvalidator -V glsl/depth_compute.comp -o depth_compute.comp.spv

glslangvalidator -V glsl/copy.frag -o copy.frag.spv

glslangvalidator -V glsl/ui.frag -o ui.frag.spv
glslangvalidator -V glsl/ui.vert -o ui.vert.spv

glslangvalidator -V glsl/batch.vert -o batch.vert.spv
glslangvalidator -V glsl/batch.frag -o batch.frag.spv

glslangvalidator -V glsl/bloom.frag -o bloom.frag.spv

glslangvalidator -V glsl/blur.frag -o blur.frag.spv

pause
