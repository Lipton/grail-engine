#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec3 inPos;
layout (location = 1) in vec3 inNormals;
layout (location = 2) in vec3 inTangent;
layout (location = 3) in vec3 inUvs;

layout (location = 0) out vec3 pass_position;
layout (location = 1) out vec3 pass_normals;
layout (location = 2) out vec2 pass_uvs;
layout (location = 3) out vec3 pass_tangent;
layout (location = 4) out vec3 pass_bitangent;

layout (location = 5) out vec4 pass_last_final_pos;
layout (location = 6) out vec4 pass_final_pos;

layout (std140, push_constant) uniform pushConstants {
	mat4 model;
	mat4 lastModel;
} mats;

layout (binding = 0) uniform UBO {
   	mat4 projection;
	mat4 inverseProjection;

	mat4 view;
	mat4 inverseView;

	mat4 combined;
	mat4 lastCombined;
	mat4 inverseCombined;

	vec4 position;
	vec4 viewDirection;
	vec4 viewport;

    mat4 shadowCascades[4];
	vec4 cascadeSplits[4];

	vec2 time;
	vec2 subsample;

	//vec4 pad[13];
} camera;

layout (binding = 1) uniform MaterialBuffer {
	vec4 color;
	vec2 uvScale;

	float roughnessMultiplier;
	float metallicMultiplier;

	float userRoughness;
	float userMetallic;

	#ifdef DETAIL_MAP
	vec2 detailUVScale;
	vec2 detailInfluence;
	#endif

	#if defined(AO_MAP) || defined(PACKED_AO_ROUGH_METALLIC)
	vec2 aoScale;
	#endif

	#ifdef ALPHA
	float windStrength;
	float windSpeed;
	float grass;
	#endif
} material;


out gl_PerVertex {
    vec4 gl_Position;
};

void main() {
	vec4 worldPosition = mats.model * vec4(inPos, 1.0);
	vec4 lastWorldPos = mats.lastModel * vec4(inPos, 1.0);

#ifdef ALPHA
	vec3 offset;
	offset.x = (inUvs.y) * 0.075 * sin( mod(inPos.x * 10.0, 3.5) + camera.time.x * 3.0 * material.windSpeed);
	offset.y = 0;
    offset.z = (inUvs.y) * 0.085 * cos( mod(inPos.y * 5.0, 3.5) + camera.time.x * 2.0 * material.windSpeed);
	offset.xyz *= material.windStrength;
	worldPosition.xyz += offset * mix(1, clamp(1.0 - (inUvs.y + 0.2), 0.0, 1.0), material.grass);
	offset.x = (inUvs.y) * 0.075 * sin( mod(inPos.x * 10.0, 3.5) + camera.time.y * 3.0 * material.windSpeed);
	offset.y = 0;
    offset.z = (inUvs.y) * 0.085 * cos( mod(inPos.y * 5.0, 3.5) + camera.time.y * 2.0 * material.windSpeed);
	offset.xyz *= material.windStrength;
	lastWorldPos.xyz +=  offset * mix(1, clamp(1.0 - (inUvs.y + 0.2), 0.0, 1.0), material.grass);
#endif

#ifdef GEOMETRY
	vec4 projectedWorld = camera.combined * worldPosition;
	projectedWorld.xy += camera.subsample.xy * projectedWorld.w;
	vec4 projectedWorldLast = camera.lastCombined * lastWorldPos;
	projectedWorldLast.xy += camera.subsample.xy * projectedWorldLast.w;

	gl_Position = projectedWorld;

	vec3 vertexBinormal = cross(inNormals, inTangent);
	mat3 normalMatrix = transpose(inverse(mat3(mats.model)));

    pass_position = worldPosition.xyz;
	pass_normals = normalize(normalMatrix * inNormals);
    pass_tangent = normalize(normalMatrix * inTangent);
    pass_tangent = normalize(pass_tangent - dot(pass_tangent, pass_normals) * pass_normals);
    pass_bitangent = normalize(normalMatrix * vertexBinormal);
	pass_bitangent = cross(pass_normals, pass_tangent);
	pass_uvs = inUvs.xy;

	pass_final_pos = projectedWorld;
	pass_last_final_pos = projectedWorldLast;
#endif

#ifdef SHADOW
#if defined(CASCADE_0)
	#define INDEX 0
#elif defined(CASCADE_1)
	#define INDEX 1
#elif defined(CASCADE_2)
	#define INDEX 2
#elif defined(CASCADE_3)
	#define INDEX 3
#else
	#define INDEX 0
#endif
	vec4 v_pos = camera.shadowCascades[INDEX] * worldPosition;
	gl_Position = v_pos;

	#if defined(ALPHA)
		pass_uvs = inUvs.xy;
	#endif
#endif
}
