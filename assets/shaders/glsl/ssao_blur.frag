#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (binding = 0) uniform sampler2D u_ssao;
layout (binding = 1) uniform sampler2D u_depth;
layout (binding = 2) uniform sampler2D u_depthFS;

layout (std140, push_constant) uniform pushConstants {
    vec2 texelSize;
} constants;

const float KERNEL_RADIUS = 2;

layout (location = 0) in vec2 inUV;

layout (location = 0) out float outFragColor;

float linearDepth(float depth)
{
    const float NEAR_PLANE = 1.0f;
    const float FAR_PLANE = 90.0f;
	float z = depth * 2.0f - 1.0f; 
	return (2.0f * NEAR_PLANE * FAR_PLANE) / (FAR_PLANE + NEAR_PLANE - z * (FAR_PLANE - NEAR_PLANE));	
}

float getLinearDepth(vec2 uvs)
{
    float NEAR_PLANE = 0.3f;
    float FAR_PLANE = 30.0f;
	float z = texture(u_depthFS, uvs).r * 2.0f - 1.0f; 
	return (2.0f * NEAR_PLANE * FAR_PLANE) / (FAR_PLANE + NEAR_PLANE - z * (FAR_PLANE - NEAR_PLANE));	
}

float BlurFunction(vec2 uv, float r, float center_c, float center_d, inout float w_total)
{
    float  c = texture( u_ssao, uv ).r;
  float d = (texture( u_depth, uv).x);
  
  const float BlurSigma = float(KERNEL_RADIUS) * 0.5;
  const float BlurFalloff = 1.0 / (2.0*BlurSigma*BlurSigma);
  
  float ddiff = (d - center_d) * 0.0025;
  float w = exp2(-r*r*BlurFalloff - ddiff*ddiff);
  w_total += w;

  return c*w;
}

#define DEPTHTEX_ENABLED

const float blurSize = 0.5;
const float depthRange = 1.5;

void main()
{
    float kernel[5];
    kernel[0] = 0.122581;
    kernel[1] = 0.233062;
    kernel[2] = 0.288713;
    kernel[3] = 0.233062;
    kernel[4] = 0.122581;

    vec2 off = vec2(0.0);
   
        off[0] = blurSize / constants.texelSize.x;
        off[1] = 0.0;

    vec2 coord = inUV;

    float sum = 0.0;
    float weightAll = 0.0;

#ifdef NORMALTEX_ENABLED
    vec3 centerNormal = texture2D(normalTex, v_Texcoord).rgb * 2.0 - 1.0;
#endif
#if defined(DEPTHTEX_ENABLED)
    float centerDepth = getLinearDepth(inUV);
#endif

    for (int i = 0; i < 8; i++) {
        vec2 coord = clamp(inUV + vec2(float(i) - 2.0) * off, vec2(0.0), vec2(1.0));

        float w = kernel[i];
#ifdef NORMALTEX_ENABLED
        vec3 normal = texture2D(normalTex, coord).rgb * 2.0 - 1.0;
        w *= clamp(dot(normal, centerNormal), 0.0, 1.0);
#endif
#ifdef DEPTHTEX_ENABLED
        float d = getLinearDepth(coord);
        // PENDING Better equation?
        w *= (1.0 - smoothstep(0.0, 1.0, abs(centerDepth - d) / depthRange));
#endif

        weightAll += w;

        coord = clamp(inUV + vec2(float(i) - 2.0) * (off * 2.0), vec2(0.0), vec2(1.0));
        sum += texture(u_ssao, coord).r * w;
    }

        off[1] = blurSize / constants.texelSize.y;
        off[0] = 0.0;

     for (int i = 0; i < 8; i++) {
        vec2 coord = clamp(inUV + vec2(float(i) - 2.0) * off, vec2(0.0), vec2(1.0));

        float w = kernel[i];
#ifdef NORMALTEX_ENABLED
        vec3 normal = texture2D(normalTex, coord).rgb * 2.0 - 1.0;
        w *= clamp(dot(normal, centerNormal), 0.0, 1.0);
#endif
#ifdef DEPTHTEX_ENABLED
        float d = getLinearDepth(coord);
        // PENDING Better equation?
        w *= (1.0 - smoothstep(0.0, 1.0, abs(centerDepth - d) / depthRange));
#endif

        weightAll += w;
        
        coord = clamp(inUV + vec2(float(i) - 2.0) * (off * 2.0), vec2(0.0), vec2(1.0));
        sum += texture(u_ssao, coord).r * w;
    }

   outFragColor = (sum / weightAll);
}