#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

#define PI 3.14159265358979323846

layout (location = 0) in vec2 pass_uvs;
layout (location = 1) smooth in vec3 pass_eye;

layout (binding = 1) uniform UBO {
    mat4 inverseCombined;
    vec4 cameraPos;
    vec4 cascadeSplits;
    mat4 inverseView;
    vec4 lightDir;
    mat4 cascadeViewProj[4];
    mat4 inverseProjection;
    mat4 combined;
    vec4 lightColorIntensity;
    uint cascadeCount;
    int visualize;
    int brdfFunc;
} ubo;

layout (std140, push_constant) uniform pushConstants {
    vec4 flags;
} consts;

layout (binding = 2) uniform sampler2D u_albedo;
layout (binding = 3) uniform sampler2D u_normals;
layout (binding = 4) uniform sampler2D u_envMap;
layout (binding = 5) uniform sampler2D u_brdfLUT;
layout (binding = 6) uniform samplerCube u_envIrradiance;
layout (binding = 7) uniform samplerCube u_envSpec;
layout (binding = 8) uniform sampler2D u_depth;
layout (binding = 9) uniform sampler2D u_ssao;
layout (binding = 10) uniform sampler2DArray u_cascade;
layout (binding = 11) uniform sampler2D u_lastFrame;
layout (binding = 12) uniform sampler2D u_ssr;

const vec2 poisson[32] = vec2[32](
	vec2( -0.4994766, -0.4100508 ),
	vec2(  0.1725386, -0.50636 ),
	vec2( -0.3050305,  0.7459931 ),
	vec2(  0.3256707,  0.2347208 ),
	vec2( -0.1094937, -0.752005 ),
	vec2(  0.5059697, -0.7294227 ),
	vec2(  0.5059697, -0.7294227 ),
	vec2(  0.3405131,  0.4458854 ),
	vec2( -0.163072,  -0.9741971 ),
	vec2(  0.4260757, -0.02231212 ),
	vec2( -0.8977778,  0.1717084 ),
	vec2(  0.02903906, 0.3999698 ),
	vec2( -0.4680224, -0.4418066 ),
	vec2(  0.09780561, -0.1236207 ),
	vec2( -0.3564819,  0.2770886 ),
	vec2(  0.0663829,  0.9336991 ),
	vec2( -0.8206947, -0.3301564 ),
	vec2(  0.1038207, -0.2167438 ),
	vec2( -0.3123821,  0.2344262 ),
	vec2(  0.1979104,  0.7830779 ),
	vec2( -0.6740047, -0.4649915 ),
	vec2(  0.08938109, -0.005763604 ),
	vec2( -0.6670403,  0.658087 ),
	vec2(  0.8211543,  0.365194 ),
	vec2( -0.8381009, -0.1279669 ),
	vec2(  0.6365152, -0.229197 ),
	vec2( -0.1748933,  0.1948632 ),
	vec2(  0.1710306,  0.5527771 ),
	vec2( -0.5874177, -0.1295959 ),
	vec2(  0.6305282, -0.5586912 ),
	vec2( -0.030519,  0.3487186 ),
	vec2(  0.4240496, -0.1010172 )
);

const mat4 biasMat = mat4( 
	0.5, 0.0, 0.0, 0.0,
	0.0, 0.5, 0.0, 0.0,
	0.0, 0.0, 1.0, 0.0,
	0.5, 0.5, 0.0, 1.0 
);

layout (location = 0) out vec4 out_color;

vec4 getViewPos(vec2 texCoord) {
	// Calculate out of the fragment in screen space the view space position.

	float x = texCoord.s * 2.0 - 1.0;
	float y = texCoord.t * 2.0 - 1.0;

	// Assume we have a normal depth range between 0.0 and 1.0
	float z = texture(u_depth, texCoord).r;

	vec4 posProj = vec4(x, y, z, 1.0);

	vec4 posView = ubo.inverseProjection * posProj;

	posView /= posView.w;

	return posView;
}

vec4 getWorldPos(vec2 texCoord, float depth) {
	float x = texCoord.s * 2.0 - 1.0;
	float y = texCoord.t * 2.0 - 1.0;

	float z = depth;
	vec4 posProj = vec4(x, y, z, 1.0);
	vec4 posView = ubo.inverseCombined * posProj;
	posView /= posView.w;

	return posView;
}

float sqr(float x) { return x*x; }

float SchlickFresnel(float u) {
    float m = clamp(1-u, 0, 1);
    float m2 = m*m;
    return m2*m2*m; // pow(m,5)
}

float GTR2_aniso(float NdotH, float HdotX, float HdotY, float ax, float ay) {
    return 1 / ( PI * ax*ay * sqr( sqr(HdotX/ax) + sqr(HdotY/ay) + NdotH*NdotH ));
}

float smithG_GGX(float Ndotv, float alphaG) {
    float a = alphaG*alphaG;
    float b = Ndotv*Ndotv;
    return 1/(Ndotv + sqrt(a + b - a*b));
}

float GTR1(float NdotH, float a) {
    if (a >= 1) return 1/PI;
    float a2 = a*a;
    float t = 1 + (a2-1)*NdotH*NdotH;
    return (a2-1) / (PI*log(a2)*t);
}

vec3 mon2lin(vec3 x) {
    return vec3(pow(x[0], 1), pow(x[1], 1), pow(x[2], 1));
}

vec3 BRDF(vec3 baseColor, vec3 L, vec3 V, vec3 N, vec3 X, vec3 Y, float metallic, float roughness, float attenuation) {
    float NdotL = clamp(dot(N,L), 0.0, 1.0);
    float NdotV = clamp(dot(N,V), 0.0, 1.0);
    //if (NdotL < 0 || NdotV < 0) return vec3(0);

    vec3 H = normalize(L+V);
    float NdotH = dot(N,H);
    float LdotH = dot(L,H);

    vec3 Cdlin = mon2lin(baseColor);
    float Cdlum = .3*Cdlin[0] + .6*Cdlin[1]  + .1*Cdlin[2]; // luminance approx.

    vec3 Ctint = Cdlum > 0 ? Cdlin / Cdlum : vec3(1); // normalize lum. to isolate hue+sat
    vec3 Cspec0 = mix(vec3(0.04), Cdlin, metallic); // mix(specular*.08*mix(vec3(1), Ctint, specularTint), Cdlin, metallic);
    //REMOVED : vec3 Csheen = mix(vec3(1), Ctint, 0.0); //mix(vec3(1), Ctint, sheenTint);

    // Diffuse fresnel - go from 1 at normal incidence to .5 at grazing
    // and mix in diffuse retro-reflection based on roughness
    float FL = SchlickFresnel(NdotL), FV = SchlickFresnel(NdotV);
    float Fd90 = 0.5 + 2 * LdotH*LdotH * roughness;
    float Fd = mix(1, Fd90, FL) * mix(1, Fd90, FV);

    // Based on Hanrahan-Krueger brdf approximation of isotropic bssrdf
    // 1.25 scale is used to (roughly) preserve albedo
    // Fss90 used to "flatten" retroreflection based on roughness
    float Fss90 = LdotH*LdotH*roughness;
    float Fss = mix(1, Fss90, FL) * mix(1, Fss90, FV);
    float ss = 1.25 * (Fss * (1 / (NdotL + NdotV) - .5) + .5);

    // specular
    float aspect = sqrt(1-0.0*.9); //sqrt(1-anisotropic*.9);
    float ax = max(.001, sqr(roughness)/aspect);
    float ay = max(.001, sqr(roughness)*aspect);
    float Ds = GTR2_aniso(NdotH, dot(H, X), dot(H, Y), ax, ay);
    float FH = SchlickFresnel(LdotH);
    vec3 Fs = mix(Cspec0, vec3(1), FH);
    float roughg = sqr(roughness*.5+.5);
    float Gs = smithG_GGX(NdotL, roughg) * smithG_GGX(NdotV, roughg);

    // sheen
    //REMOVED : vec3 Fsheen = FH * 0.0 * Csheen; //FH * sheen * Csheen;

    // clearcoat (ior = 1.5 -> F0 = 0.04)
    float Dr = GTR1(NdotH, 0.1); //GTR1(NdotH, mix(.1,.001,clearcoatGloss));
    float Fr = mix(.04, 1.0, FH);
    float Gr = smithG_GGX(NdotL, .25) * smithG_GGX(NdotV, .25);

    return (((1/PI) * Fd * Cdlin) //(((1/PI) * mix(Fd, ss, subsurface) * Cdlin + Fsheen)
        * (1-metallic)
        + Gs*(Fs * attenuation)*Ds ) * NdotL * attenuation; //  + Gs*Fs*Ds + .25*clearcoat*Gr*Fr*Dr) * NdotL * attenuation;
}

vec3 Specular_F_Roughness(vec3 specularColor, float a, vec3 h, vec3 v) {
	return (specularColor + (max(vec3(1.0 - a), specularColor) - specularColor) * pow((1 - clamp(dot(v, h), 0.0, 1.0)), 5.0));
}

const vec2 invAtan = vec2(0.1591f, 0.3183f);
vec2 sampleSphericalMap(vec3 v) {
    vec2 uv = vec2(atan(v.z, v.x), asin(v.y));
    uv *= invAtan;
    uv += 0.5;
    return uv;
}

vec3 prefilteredReflection(vec3 R, float roughness) {
	float lod = roughness * textureQueryLevels(u_envSpec);
	float lodf = floor(lod);
	float lodc = ceil(lod);
	vec3 a = textureLod(u_envSpec, R, lodf).rgb;
	vec3 b = textureLod(u_envSpec, R, lodc).rgb;
    
	return mix(a, b, lod - lodf);
}

vec3 F_SchlickR(float cosTheta, vec3 F0, float roughness) {
	return F0 + (max(vec3(1.0 - roughness), F0) - F0) * pow(1.0 - cosTheta, 5.0);
}

float textureProj(vec4 P, vec2 offset, uint cascadeIndex) {
	float shadow = 1.0;
	float bias = 0.005;

	vec4 shadowCoord = P / P.w;
	if ( shadowCoord.z > -1.0 && shadowCoord.z < 1.0 ) {
		float dist = texture(u_cascade, vec3(shadowCoord.st + offset, cascadeIndex)).r;
		if (shadowCoord.w > 0 && dist < shadowCoord.z - bias) {
			shadow = 0.0;
		}
	}
	return shadow;

}

/*float calcPenumbraSize( vec3 shadowCoord, uint cascadeIndex ) {
        float dFragment = shadowCoord.z;
        float dBlocker = 0;
        float penumbra = 0;
        float wLight = 0.5;

        // Sample the shadow map 8 times
        float temp;
        float count = 0;



        for( int i = 0; i < 32; i++ ) {
                temp = texture( u_cascade, vec3(shadowCoord.st + (poisson[ i ] *  0.005), cascadeIndex ) ).r;
                if( temp < dFragment ) {
            dBlocker += temp;
                        count += 1.0;
                }
        }

        if( count > 0.1 ) {
                dBlocker /= count;
                penumbra = wLight * (dFragment - dBlocker) / dFragment;
        }

    return penumbra * 0.025;
}*/

float sampleShadowMap(vec2 base_uv, float u, float v, vec2 shadowSizeInv, uint cascadeIndex, float depth) {
    vec2 uv = base_uv + vec2(u, v) * shadowSizeInv;

    float z = depth;


		float dist = texture(u_cascade, vec3(uv.st, cascadeIndex)).r;
		if (z - dist > 0.05) {
			return 0.0;
		}
	

	return 1.0;
}

vec2 get_shadow_offsets(vec3 N, vec3 L) {
    float cos_alpha = clamp(dot(N, L), 0, 1);
    float offset_scale_N = sqrt(1 - cos_alpha*cos_alpha); // sin(acos(L·N))
    float offset_scale_L = offset_scale_N / cos_alpha;    // tan(acos(L·N))
    return vec2(offset_scale_N, min(2, offset_scale_L));
}

float calculateShadowing(vec3 normal, vec4 lightSpacePos, vec3 lightDir, uint cascadeIndex) {
	vec3 projCoords = lightSpacePos.xyz / lightSpacePos.w;

    float closestDepth = texture(u_cascade, vec3(projCoords.xy, cascadeIndex)).r;
	float currDepth = projCoords.z;
	
    vec2 bias = get_shadow_offsets(normal, lightDir);

    float shadow = 0.0;
	vec2 texelSize = 1.0 / textureSize(u_cascade, 0).xy;

	#define LOOPS 2
			float sum = 0, count = 0;
			float x, y;
			for (y = -LOOPS; y <= LOOPS; y += 1.0)
			for (x = -LOOPS; x <= LOOPS; x += 1.0) {
				// Can't use texture(Proj)Offset directly since it expects the offset to be a constant value,
				// i.e. no loops, so instead we calculate the offset manually (given the texture size)
				vec2 offset = vec2(x, y);
				float dp = texture(u_cascade, vec3(projCoords.xy + (offset * texelSize), cascadeIndex)).r;
                if(currDepth - dp > bias.x * 0.005) {
                    sum += 1.0;
                }
				count++;
			}

			shadow = sum / count;

        return 1.0 - shadow;
}

vec3 BRDFA(vec3 baseColor, vec3 l, vec3 v, vec3 n, vec3 X, vec3 Y, float metallic, float roughness, float attenuation, vec3 color) {
    float alphaRoughness = roughness * roughness;
    float r = alphaRoughness;

    vec3 f0 = vec3(0.04);
    vec3 diffuseColor = baseColor.rgb * (vec3(1.0) - f0);
    diffuseColor *= 1.0 - metallic;
    vec3 specularColor = mix(f0, baseColor.rgb, metallic);
   
    float reflectance = max(max(specularColor.r, specularColor.g), specularColor.b);

    float reflectance90 = clamp(reflectance * 25.0, 0.0, 1.0);
    vec3 specularEnvironmentR0 = specularColor.rgb;
    vec3 specularEnvironmentR90 = vec3(1.0, 1.0, 1.0) * reflectance90;

    vec3 h = normalize(l + v);
    
    float NdotL = clamp(dot(n, l), 0.001, 1.0);
    float NdotV = clamp(abs(dot(n, v)), 0.001, 1.0);
    float NdotH = clamp(dot(n, h), 0.0, 1.0);
    float LdotH = clamp(dot(l, h), 0.0, 1.0);
    float VdotH = clamp(dot(v, h), 0.0, 1.0);

    vec3 F = specularEnvironmentR0 + (specularEnvironmentR90 - specularEnvironmentR0) * pow(clamp(1.0 - VdotH, 0.0, 1.0), 5.0);

    float attenuationL = 2.0 * NdotL / (NdotL + sqrt(r * r + (1.0 - r * r) * (NdotL * NdotL)));
    float attenuationV = 2.0 * NdotV / (NdotV + sqrt(r * r + (1.0 - r * r) * (NdotV * NdotV)));
    float G = attenuationL * attenuationV;

    float roughnessSq = alphaRoughness * alphaRoughness;
    float f = (NdotH * roughnessSq - NdotH) * NdotH + 1.0;
    float D = roughnessSq / (PI * f * f);

    vec3 diffuseContrib = (1.0 - F) * (diffuseColor / PI);
    vec3 specContrib = F * G * D / (4.0 * NdotL * NdotV);

    return NdotL * color * (diffuseContrib + specContrib);
}

//#define UsePlaneDepthBias_ 1

vec2 ComputeReceiverPlaneDepthBias(vec3 texCoordDX, vec3 texCoordDY) {
    vec2 biasUV;
    biasUV.x = texCoordDY.y * texCoordDX.z - texCoordDX.y * texCoordDY.z;
    biasUV.y = texCoordDX.x * texCoordDY.z - texCoordDY.x * texCoordDX.z;
    biasUV *= 1.0f / ((texCoordDX.x * texCoordDY.y) - (texCoordDX.y * texCoordDY.x));
    return biasUV;
}

float SampleShadowMap(in vec2 base_uv, in float u, in float v, in vec2 shadowMapSizeInv,
                      in uint cascadeIdx,  in float depth, in vec2 receiverPlaneDepthBias) {

    vec2 uv = base_uv + vec2(u, v) * shadowMapSizeInv;

    #if UsePlaneDepthBias_
        float z = depth + dot(vec2(u, v) * shadowMapSizeInv, receiverPlaneDepthBias);
    #else
        float z = depth;
    #endif

    float dp = texture(u_cascade, vec3(uv, cascadeIdx)).r;

    return (dp - z) > 0.0 ? 1.0 : 0.0;
}

float sampleCSM(vec3 samplePos, vec3 samplePosDX, vec3 samplePosDY, uint cascadeIndex, vec3 normal, vec3 lightDir) {
    ivec2 shadowMapSize = textureSize(u_cascade, 0).xy;
    float numSlices;

    float lightDepth = samplePos.z;

    vec2 bias = get_shadow_offsets(normal, lightDir);

    #if UsePlaneDepthBias_
        vec2 texelSize = 1.0f / shadowMapSize;

        vec2 receiverPlaneDepthBias = ComputeReceiverPlaneDepthBias(samplePosDX, samplePosDY);

        // Static depth biasing to make up for incorrect fractional sampling on the shadow map grid
        float fractionalSamplingError = 2 * dot(vec2(1.0f, 1.0f) * texelSize, abs(receiverPlaneDepthBias));
        lightDepth -= min(fractionalSamplingError, 0.01f);
    #else
        vec2 receiverPlaneDepthBias;
        lightDepth -= bias.x * 0.005;
    #endif

    vec2 uv = samplePos.xy * shadowMapSize; // 1 unit - 1 texel

    vec2 shadowMapSizeInv = 1.0 / shadowMapSize;

    vec2 base_uv;
    base_uv.x = floor(uv.x + 0.5);
    base_uv.y = floor(uv.y + 0.5);

    float s = (uv.x + 0.5 - base_uv.x);
    float t = (uv.y + 0.5 - base_uv.y);

    base_uv -= vec2(0.5, 0.5);
    base_uv *= shadowMapSizeInv;

    float sum = 0;

    #if FilterSize_ == 2
        return ShadowMap.SampleCmpLevelZero(ShadowSamplerPCF, float3(shadowPos.xy, cascadeIdx), lightDepth);
    #elif FilterSize_ == 3

        float uw0 = (3 - 2 * s);
        float uw1 = (1 + 2 * s);

        float u0 = (2 - s) / uw0 - 1;
        float u1 = s / uw1 + 1;

        float vw0 = (3 - 2 * t);
        float vw1 = (1 + 2 * t);

        float v0 = (2 - t) / vw0 - 1;
        float v1 = t / vw1 + 1;

        sum += uw0 * vw0 * SampleShadowMap(base_uv, u0, v0, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);
        sum += uw1 * vw0 * SampleShadowMap(base_uv, u1, v0, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);
        sum += uw0 * vw1 * SampleShadowMap(base_uv, u0, v1, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);
        sum += uw1 * vw1 * SampleShadowMap(base_uv, u1, v1, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);

        return sum * 1.0f / 16;

    #elif FilterSize_ == 5

        float uw0 = (4 - 3 * s);
        float uw1 = 7;
        float uw2 = (1 + 3 * s);

        float u0 = (3 - 2 * s) / uw0 - 2;
        float u1 = (3 + s) / uw1;
        float u2 = s / uw2 + 2;

        float vw0 = (4 - 3 * t);
        float vw1 = 7;
        float vw2 = (1 + 3 * t);

        float v0 = (3 - 2 * t) / vw0 - 2;
        float v1 = (3 + t) / vw1;
        float v2 = t / vw2 + 2;

        sum += uw0 * vw0 * SampleShadowMap(base_uv, u0, v0, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);
        sum += uw1 * vw0 * SampleShadowMap(base_uv, u1, v0, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);
        sum += uw2 * vw0 * SampleShadowMap(base_uv, u2, v0, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);

        sum += uw0 * vw1 * SampleShadowMap(base_uv, u0, v1, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);
        sum += uw1 * vw1 * SampleShadowMap(base_uv, u1, v1, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);
        sum += uw2 * vw1 * SampleShadowMap(base_uv, u2, v1, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);

        sum += uw0 * vw2 * SampleShadowMap(base_uv, u0, v2, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);
        sum += uw1 * vw2 * SampleShadowMap(base_uv, u1, v2, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);
        sum += uw2 * vw2 * SampleShadowMap(base_uv, u2, v2, shadowMapSizeInv, cascadeIdx, lightDepth, receiverPlaneDepthBias);

        return sum * 1.0f / 144;

    #else // FilterSize_ == 7

        float uw0 = (5 * s - 6);
        float uw1 = (11 * s - 28);
        float uw2 = -(11 * s + 17);
        float uw3 = -(5 * s + 1);

        float u0 = (4 * s - 5) / uw0 - 3;
        float u1 = (4 * s - 16) / uw1 - 1;
        float u2 = -(7 * s + 5) / uw2 + 1;
        float u3 = -s / uw3 + 3;

        float vw0 = (5 * t - 6);
        float vw1 = (11 * t - 28);
        float vw2 = -(11 * t + 17);
        float vw3 = -(5 * t + 1);

        float v0 = (4 * t - 5) / vw0 - 3;
        float v1 = (4 * t - 16) / vw1 - 1;
        float v2 = -(7 * t + 5) / vw2 + 1;
        float v3 = -t / vw3 + 3;

        sum += uw0 * vw0 * SampleShadowMap(base_uv, u0, v0, shadowMapSizeInv, cascadeIndex, lightDepth, receiverPlaneDepthBias);
        sum += uw1 * vw0 * SampleShadowMap(base_uv, u1, v0, shadowMapSizeInv, cascadeIndex, lightDepth, receiverPlaneDepthBias);
        sum += uw2 * vw0 * SampleShadowMap(base_uv, u2, v0, shadowMapSizeInv, cascadeIndex, lightDepth, receiverPlaneDepthBias);
        sum += uw3 * vw0 * SampleShadowMap(base_uv, u3, v0, shadowMapSizeInv, cascadeIndex, lightDepth, receiverPlaneDepthBias);

        sum += uw0 * vw1 * SampleShadowMap(base_uv, u0, v1, shadowMapSizeInv, cascadeIndex, lightDepth, receiverPlaneDepthBias);
        sum += uw1 * vw1 * SampleShadowMap(base_uv, u1, v1, shadowMapSizeInv, cascadeIndex, lightDepth, receiverPlaneDepthBias);
        sum += uw2 * vw1 * SampleShadowMap(base_uv, u2, v1, shadowMapSizeInv, cascadeIndex, lightDepth, receiverPlaneDepthBias);
        sum += uw3 * vw1 * SampleShadowMap(base_uv, u3, v1, shadowMapSizeInv, cascadeIndex, lightDepth, receiverPlaneDepthBias);

        sum += uw0 * vw2 * SampleShadowMap(base_uv, u0, v2, shadowMapSizeInv, cascadeIndex, lightDepth, receiverPlaneDepthBias);
        sum += uw1 * vw2 * SampleShadowMap(base_uv, u1, v2, shadowMapSizeInv, cascadeIndex, lightDepth, receiverPlaneDepthBias);
        sum += uw2 * vw2 * SampleShadowMap(base_uv, u2, v2, shadowMapSizeInv, cascadeIndex, lightDepth, receiverPlaneDepthBias);
        sum += uw3 * vw2 * SampleShadowMap(base_uv, u3, v2, shadowMapSizeInv, cascadeIndex, lightDepth, receiverPlaneDepthBias);

        sum += uw0 * vw3 * SampleShadowMap(base_uv, u0, v3, shadowMapSizeInv, cascadeIndex, lightDepth, receiverPlaneDepthBias);
        sum += uw1 * vw3 * SampleShadowMap(base_uv, u1, v3, shadowMapSizeInv, cascadeIndex, lightDepth, receiverPlaneDepthBias);
        sum += uw2 * vw3 * SampleShadowMap(base_uv, u2, v3, shadowMapSizeInv, cascadeIndex, lightDepth, receiverPlaneDepthBias);
        sum += uw3 * vw3 * SampleShadowMap(base_uv, u3, v3, shadowMapSizeInv, cascadeIndex, lightDepth, receiverPlaneDepthBias);

        return sum * 1.0f / 2704;

    #endif
}

//#define POW(input) pow(input, vec3(1.0 / 2.2))
#define POW(input) input

vec3 sampleEnviroment(vec3 color) {
    if(consts.flags.z == 0.0) {
        return color;
    } else if (consts.flags.z == 1.0) {
        return pow(color, vec3(2.2));
    } else {
        return pow(color, vec3(1.0/2.2));
    }
}

void main() {
    float depth = texture(u_depth, pass_uvs).r;

    vec3 finalColor = vec3(0.0);

    uint cascadeIndex = 0;

    if(depth != 1.0) {
/**

float4 brdfTerm = specularIntegration.SampleLevel(clampSampler, float2(vdotn, 1.0-roughness), 0);
    float3 metalSpecularIBL = specularIBL.rgb; 

    float3 dielectricColor = float3(0.04, 0.04, 0.04);
    float3 diffColor = diffuseColor.rgb * (1.0 - metalness);
    float3 specColor = lerp(dielectricColor.rgb, diffuseColor.rgb, metalness) * specularIntensity;
   
    diffuseIBL.rgb = lerp(diffuseIBL.rgb * 0.3f, diffuseIBL.rgb, bakedAO);
    
    float3 albedoByDiffuse = diffColor.rgb * diffuseIBL.rgb;

    float4 litColor;
    litColor.rgb =  (albedoByDiffuse.rgb + (metalSpecularIBL * (specColor * brdfTerm.x + (brdfTerm.y)))) * bakedAO;
    litColor.a = 1.0;


*/

        vec4 albedo = texture(u_albedo, pass_uvs);
        albedo.rgb = pow(albedo.rgb, vec3(2.2));

        vec4 normals = texture(u_normals, pass_uvs);
        normals.rgb = normalize(normals.rgb);
        //normals.y *= -1.0;
        vec4 position = getWorldPos(pass_uvs, depth);

        vec3 tangent = normalize( cross( vec3(0, 1, 0), normals.rgb ) );
        vec3 bitangent = normalize( cross( normals.rgb, tangent ) );

        vec3 viewDir = normalize(ubo.cameraPos.xyz - position.xyz);

        float roughness = albedo.a;
        float metallic = normals.a;

		vec3 Cspec0 = mix(vec3(0.04), albedo.rgb, metallic);
		vec3 reflectVector = reflect(-viewDir, normals.rgb);
        reflectVector.y *= -1.0;

        vec3 F = F_SchlickR(max(dot(normals.rgb, viewDir), 0.0), Cspec0, roughness);

        vec3 reflection = sampleEnviroment(prefilteredReflection(reflectVector, roughness).rgb) * consts.flags.x;
        //textureLod(u_envSpec, reflectVector, roughness * textureQueryLevels(u_envSpec)).rgb;
		vec3 irradiance = sampleEnviroment(texture(u_envIrradiance, normals.rgb * vec3(1, -1, 1)).rgb) * consts.flags.x;

        vec2 envBRDF = texture(u_brdfLUT, vec2(dot(normals.xyz, viewDir.xyz), 1.0 - roughness)).rg;
        vec3 specular = reflection * (F * envBRDF.x + envBRDF.y);

        vec3 kD = 1.0 - F;
        kD *= 1.0 - metallic;

        vec3 dielectricColor = vec3(0.04, 0.04, 0.04);
        vec3 diffColor = albedo.rgb * (1.0 - metallic);
        vec3 specColor = mix(dielectricColor.rgb, albedo.rgb, metallic);
        vec3 albedoByDiffuse = diffColor.rgb * irradiance.rgb;
        
        vec3 ambient = albedoByDiffuse.rgb + (reflection.rgb * (specColor * envBRDF.x + (envBRDF.y)));

        float ssao = texture(u_ssao, pass_uvs).r;

        vec4 viewPos = getViewPos(pass_uvs);

        float cascadeValue = 0.0;
        for(uint i = 0; i < ubo.cascadeCount - 1; ++i) {
            if(viewPos.z < ubo.cascadeSplits[i]) {	
                cascadeIndex = i + 1;
            }
        }

        vec4 shadowCoord = (biasMat * ubo.cascadeViewProj[cascadeIndex]) * vec4(position.xyz, 1.0);
        vec3 shadowPosDX = dFdxFine(shadowCoord.xyz);
        vec3 shadowPosDY = dFdyFine(shadowCoord.xyz);
        float shadow = 0;	

        shadow = calculateShadowing(normals.xyz, shadowCoord, -ubo.lightDir.xyz, cascadeIndex);

        const float blendThreshold = 0.2;
        float nextSplit = ubo.cascadeSplits[cascadeIndex];
        float splitSize = cascadeIndex == 0 ? nextSplit : nextSplit - ubo.cascadeSplits[cascadeIndex - 1];
        float fadeFactor = (nextSplit - viewPos.z) / splitSize;
        
        if(fadeFactor <= blendThreshold && cascadeIndex != ubo.cascadeCount - 1) {
              vec4 nextCoord = (biasMat * ubo.cascadeViewProj[cascadeIndex + 1]) * vec4(position.xyz, 1.0);

              float nextShadow = calculateShadowing(normals.xyz, nextCoord, -ubo.lightDir.xyz, cascadeIndex + 1);

              float lerpAmount = smoothstep(0.0, blendThreshold, fadeFactor);

              shadow = mix(nextShadow, shadow, lerpAmount);
        }

        //shadow = sampleCSM(shadowCoord.xyz, shadowPosDX, shadowPosDY, cascadeIndex, -ubo.lightDir.xyz, normals.xyz);
        vec3 lightIntensity = (ubo.lightColorIntensity.rgb * ubo.lightColorIntensity.w);
        vec3 directional = vec3(0.0);
        
        vec3 diffuse = ambient * ssao;

        if(ubo.brdfFunc == 0) directional = BRDF(albedo.rgb, -ubo.lightDir.xyz, viewDir, normals.rgb, tangent, bitangent, metallic, roughness, 1.0) * lightIntensity;
        if(ubo.brdfFunc == 1) directional = BRDFA(albedo.rgb, -ubo.lightDir.xyz, viewDir, normals.rgb, tangent, bitangent, metallic, roughness, 1.0, lightIntensity);
        //directional *= lightIntensity;
        directional *= shadow;
        vec3 finalColorLight = (ambient * ssao) + directional;

        finalColor = finalColorLight;

       /* if (ubo.visualize == 1) {
            switch(cascadeIndex) {
                case 0 : 
                    finalColor.rgb *= vec3(1.0f, 0.25f, 0.25f);
                    break;
                case 1 : 
                    finalColor.rgb *= vec3(0.25f, 1.0f, 0.25f);
                    break;
                case 2 : 
                    finalColor.rgb *= vec3(0.25f, 0.25f, 1.0f);
                    break;
                case 3 : 
                    finalColor.rgb *= vec3(1.0f, 1.0f, 0.25f);
                    break;
            }
	    }*/
    } else {
        vec3 localPos = normalize(pass_eye);
        localPos.y *= -1.0;

        if(consts.flags.w == 1.0) {
             finalColor = sampleEnviroment(texture(u_envMap, sampleSphericalMap(localPos)).rgb) * consts.flags.y;
        } else {
               finalColor = sampleEnviroment(texture(u_envIrradiance, (localPos)).rgb) * consts.flags.y;
        }
    }

    out_color = vec4(finalColor.rgb, 1.0);
}

