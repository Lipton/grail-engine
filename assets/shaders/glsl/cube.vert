#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec3 inPos;
layout (location = 1) in vec3 inNormals;
layout (location = 2) in vec2 inUvs;

layout(push_constant) uniform PushConsts {
	mat4 mvp;
    float deltaPhi;
	float deltaTheta;
} pushConsts;

layout (location = 0) out vec3 pass_uvs;

out gl_PerVertex {
    vec4 gl_Position;
};

void main() {
    pass_uvs = inPos;
	gl_Position = pushConsts.mvp * vec4(inPos.xyz, 1.0);
}
