#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec2 pass_uvs;

layout (binding = 0) uniform sampler2D u_texture;

layout (location = 0) out vec4 outFragColor;

const float intensity = 1.1;
const vec3 grayscale = vec3(0.2126, 0.7152, 0.0722);

void main() {
    vec3 color = texture(u_texture, pass_uvs).rgb;

	float brightness = max(dot(color.rgb, grayscale), 0.1);
	vec3 bloomColor = clamp(color.rgb * brightness, 0.0, 100.0); // 20.0

	outFragColor = vec4(bloomColor * intensity, 1.0);
}
