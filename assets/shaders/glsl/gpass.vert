#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec3 inPos;
layout (location = 1) in vec3 inNormals;
layout (location = 2) in vec2 inUvs;

#define SHADOW_MAP_CASCADE_COUNT 4

layout (binding = 0) uniform UBO {
	mat4 combined;
	mat4 lastCombined;

	vec4 jitter;

	mat4[SHADOW_MAP_CASCADE_COUNT] cascadeViewProjMat;
} ubo;

layout (std140, push_constant) uniform pushConstants {
	mat4 model;
	mat4 lastModel;
	
	vec4 color;
	vec2 uvScale;

	uint cascadeIndex;
} material;

layout (location = 0) out vec3 pass_position;
layout (location = 1) out vec3 pass_normals;
layout (location = 2) out vec2 pass_uvs;
layout (location = 3) out vec4 pass_v_pos;
layout (location = 4) out vec4 pass_last_v_pos;

out gl_PerVertex {
    vec4 gl_Position;
};

void main() {
	vec4 position = material.model * vec4(inPos, 1.0);
	pass_position = position.xyz;

	vec4 v_pos = ubo.combined * position;
	v_pos.xy += ubo.jitter.xy * v_pos.w;
	pass_v_pos = v_pos;

	vec4 last_v_pos = ubo.lastCombined * material.lastModel * vec4(inPos, 1.0);
	last_v_pos.xy += ubo.jitter.xy * last_v_pos.w;
	pass_last_v_pos = last_v_pos;

	pass_normals = normalize(mat3(material.model) * inNormals);
	pass_uvs = inUvs * material.uvScale;

	gl_Position = pass_v_pos;
}
