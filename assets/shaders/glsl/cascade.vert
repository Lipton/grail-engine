#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec3 inPos;
layout (location = 1) in vec3 inNormals;
layout (location = 2) in vec2 inUvs;

#define SHADOW_MAP_CASCADE_COUNT 4

layout (binding = 0) uniform UBO {
	mat4 combined;
	mat4 lastCombined;

	vec4 jitter;

	mat4[SHADOW_MAP_CASCADE_COUNT] cascadeViewProjMat;
} ubo;

layout (std140, push_constant) uniform pushConstants {
	mat4 model;
	mat4 lastModel;
	vec2 cascadeIndex;
	
	//param type: color, name: Color
	vec4 color;
	
	//param type: scalar, name: UV Scale, step:0.1, min:0, max:10
	vec2 uvScale;
} material;

layout (location = 0) out vec2 pass_uvs;

out gl_PerVertex {
    vec4 gl_Position;
};

void main() {
	vec4 position = material.model * vec4(inPos, 1.0);
	vec4 v_pos = ubo.cascadeViewProjMat[int(material.cascadeIndex.x)] * vec4(position.xyz, 1.0);

	pass_uvs = inUvs * material.uvScale;

	gl_Position = v_pos;
}
