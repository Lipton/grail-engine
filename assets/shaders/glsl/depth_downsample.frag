#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec2 pass_uvs;

layout (binding = 0) uniform sampler2D u_lastMip;

layout (location = 0) out float out_depth;

layout (std140, push_constant) uniform pushConstants {
    vec2 resolution;
} constants;

const vec2 g_kernel[4] = vec2[](
    vec2(-0.5f, -0.5f),
    vec2(0.5f, -0.5f),
    vec2(-0.5f, 0.5f),
    vec2(0.5f, 0.5f)
);

void main() {
    vec2 texelSize = vec2(1.0 / constants.resolution.x, 1.0 / constants.resolution.y);

    vec4 texels;

    texels.x = texture(u_lastMip, pass_uvs + g_kernel[0]*texelSize).r;
    texels.y = texture(u_lastMip, pass_uvs + g_kernel[1]*texelSize).r;
    texels.z = texture(u_lastMip, pass_uvs + g_kernel[2]*texelSize).r;
    texels.w = texture(u_lastMip, pass_uvs + g_kernel[3]*texelSize).r;

    out_depth = max( max( texels.x, texels.y ), max( texels.z, texels.w ) );
}
