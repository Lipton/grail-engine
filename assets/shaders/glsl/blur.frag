#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec2 pass_uvs;

layout (binding = 0) uniform sampler2D u_texture;

layout (location = 0) out vec4 outFragColor;

layout (std140, push_constant) uniform pushConstants {
	float direction;
} c;

const float intensity = 0.01;
const vec3 grayscale = vec3(0.2126, 0.7152, 0.0722);

void main()  {
	float weight[5];
	weight[0] = 0.227027;
	weight[1] = 0.1945946;
	weight[2] = 0.1216216;
	weight[3] = 0.054054;
	weight[4] = 0.016216;

	vec2 tex_offset = 1.0 / textureSize(u_texture, 0); // gets size of single texel
	vec3 result = texture(u_texture, pass_uvs).rgb * weight[0]; // current fragment's contribution
	for(int i = 1; i < 5; ++i)
	{
		if (c.direction == 1)
		{
			// H
			result += texture(u_texture, pass_uvs + vec2(tex_offset.x * i, 0.0)).rgb * weight[i] * 3;
			result += texture(u_texture, pass_uvs - vec2(tex_offset.x * i, 0.0)).rgb * weight[i] * 3;
		}
		else
		{
			// V
			result += texture(u_texture, pass_uvs + vec2(0.0, tex_offset.y * i)).rgb * weight[i] * 3;
			result += texture(u_texture, pass_uvs - vec2(0.0, tex_offset.y * i)).rgb * weight[i] * 3;
		}
	}
	outFragColor = vec4(result, 1.0);
}
