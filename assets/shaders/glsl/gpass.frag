#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec3 pass_position;
layout (location = 1) in vec3 pass_normal;
layout (location = 2) in vec2 pass_uvs;
layout (location = 3) in vec4 pass_v_pos;
layout (location = 4) in vec4 pass_last_v_pos;

layout (binding = 1) uniform sampler2D u_texture;
layout (binding = 2) uniform sampler2D u_normals;
layout (binding = 3) uniform sampler2D u_rougness;
layout (binding = 4) uniform sampler2D u_metallic;

layout (location = 0) out vec4 outFragColor;
layout (location = 1) out vec4 outFragNormal;
layout (location = 2) out vec2 outFragVelocity;

layout (std140, push_constant) uniform pushConstants {
	mat4 model;
	mat4 lastModel;
	
	vec4 color;
    vec2 uvScale;
} material;

mat3 cotangent_frame( vec3 N, vec3 p, vec2 uv ) {
    // get edge vectors of the pixel triangle
    vec3 dp1 = dFdx( p );
    vec3 dp2 = dFdy( p );
    vec2 duv1 = dFdx( uv );
    vec2 duv2 = dFdy( uv );

    // solve the linear system
    vec3 dp2perp = cross( dp2, N );
    vec3 dp1perp = cross( N, dp1 );
    vec3 T = dp2perp * duv1.x + dp1perp * duv2.x;
    vec3 B = dp2perp * duv1.y + dp1perp * duv2.y;

    // construct a scale-invariant frame
    float invmax = inversesqrt( max( dot(T,T), dot(B,B) ) );
    return mat3( T * invmax, B * invmax, N );
}

void main() {
    vec4 albedo = texture(u_texture, pass_uvs * material.uvScale);

   // if(albedo.a < 0.1) discard;

    albedo.a = texture(u_rougness, pass_uvs * material.uvScale).g;

    albedo.rgb = albedo.rgb;

    outFragColor = albedo;

    vec4 normalMap = texture(u_normals, pass_uvs * material.uvScale);
    normalMap.z = 1.0;
    normalMap = normalize(normalMap * 2.0 - 1.0);
    //normalMap.y *= -1;
    //normalMap.rgb = normalize(normalMap.rgb);
    mat3 TBN = cotangent_frame(pass_normal, pass_position, pass_uvs * material.uvScale);
    outFragNormal.rgb = pass_normal; //normalize(TBN * normalMap.rgb);
    outFragNormal.rgb = normalize(outFragNormal.rgb * 2.0 - 1.0);
    outFragNormal.rgb = vec4(normalize(cotangent_frame(pass_normal, pass_position, pass_uvs) * normalMap.rgb), 1.0).rgb;
    
    outFragNormal.xyz += 0.000003; //0.000003
    
    //vec4(normalize(cotangent_frame(pass_normal, pass_position, pass_uvs) * normalMap.rgb), 1.0);
    outFragNormal.a = texture(u_rougness, pass_uvs * material.uvScale).b;

    //outFragColor.rgb = vec3(outFragNormal.a);

    vec2 a = (pass_v_pos.xy / pass_v_pos.w) * 0.5 + 0.5;
    vec2 b = (pass_last_v_pos.xy / pass_last_v_pos.w) * 0.5 + 0.5;

    outFragVelocity = (a - b);
}
