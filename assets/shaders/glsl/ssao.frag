#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (binding = 2) uniform sampler2D samplerPositionDepth;
layout (binding = 1) uniform sampler2D samplerNormal;
layout (binding = 3) uniform sampler2D ssaoNoise;

const int SSAO_KERNEL_SIZE = 16;
const float SSAO_RADIUS = 2.835;
#define NUM_SAMPLES          11

#define NUM_SPIRAL_TURNS     7

#define VARIATION             2

#define PI 3.14159265358979323846

const float uSampleRadiusWS = 1.5;
const float uBias = 0.02;
const float uIntensity = 10.0;

layout (std140, push_constant) uniform pushConstants {
    vec2 uvScale;
} constants;

layout (binding = 0) uniform UBOSSAOKernel
{
	vec4 samples[SSAO_KERNEL_SIZE];
} uboSSAOKernel;

layout (binding = 4) uniform UBO {
	mat4 projection;
	mat4 inverseView;
	mat4 inverseProjection;
	mat4 inverseCombined;
  mat4 view;
	vec4 cameraPos;
  vec2 jitter;
} ubo;

layout (location = 0) in vec2 inUV;

layout (location = 0) out float outFragColor;

vec4 getViewPos(vec2 texCoord) {
	// Calculate out of the fragment in screen space the view space position.

	float x = texCoord.s * 2.0 - 1.0;
	float y = texCoord.t * 2.0 - 1.0;

	// Assume we have a normal depth range between 0.0 and 1.0
	float z = textureLod(samplerPositionDepth, texCoord, 0).r;

	vec4 posProj = vec4(x, y, z, 1.0);

	vec4 posView = ubo.inverseProjection * posProj;

	posView /= posView.w;

	return posView;
}

float linearDepth(float depth)
{
    const float NEAR_PLANE = 1.0f;
    const float FAR_PLANE = 90.0f;
	float z = depth * 2.0f - 1.0f; 
	return (2.0f * NEAR_PLANE * FAR_PLANE) / (FAR_PLANE + NEAR_PLANE - z * (FAR_PLANE - NEAR_PLANE));	
}

// reconstructs view-space unit normal from view-space position
vec3 reconstructNormalVS(vec3 positionVS) {
  return normalize(cross(dFdx(positionVS), dFdy(positionVS)));
}

vec3 getPositionVS(vec2 uv) {
	// Calculate out of the fragment in screen space the view space position.

	float x = uv.s * 2.0 - 1.0;
	float y = uv.t * 2.0 - 1.0;

	// Assume we have a normal depth range between 0.0 and 1.0
	float z = textureLod(samplerPositionDepth, uv, 1).r;

	vec4 posProj = vec4(x, y, z, 1.0);

	vec4 posView = ubo.inverseProjection * posProj;

	posView /= posView.w;

	return posView.rgb;





	/*	float depth = linearDepth(texture(samplerPositionDepth, uv).r);
  
  vec2 uv2  = uv * 2.0 - vec2(1.0);
  vec4 temp = ubo.inverseCombined * vec4(uv2, -1.0, 1.0);
  vec3 cameraFarPlaneWS = (temp / temp.w).xyz;
  
  vec3 cameraToPositionRay = normalize(cameraFarPlaneWS - ubo.cameraPos);
  vec3 originWS = cameraToPositionRay * depth + ubo.cameraPos;
  vec3 originVS = (ubo.view * vec4(originWS, 1.0)).xyz;
  
  return originVS;*/
}

// returns a unit vector and a screen-space radius for the tap on a unit disk 
// (the caller should scale by the actual disk radius)
vec2 tapLocation(int sampleNumber, float spinAngle, out float radiusSS) {
  // radius relative to radiusSS
  float alpha = (float(sampleNumber) + 0.5) * (1.0 / float(NUM_SAMPLES));
  float angle = alpha * (float(NUM_SPIRAL_TURNS) * 6.28) + spinAngle;
  
  radiusSS = alpha;
  return vec2(cos(angle), sin(angle));
}

vec3 getOffsetPositionVS(vec2 uv, vec2 unitOffset, float radiusSS) {
  uv = uv + radiusSS * unitOffset * (1.0 / (vec2(3840.0, 2160.0) * 0.5));
  
  return getPositionVS(uv);
}

float sampleAO(vec2 uv, vec3 positionVS, vec3 normalVS, float sampleRadiusSS, 
               int tapIndex, float rotationAngle)
{
  const float epsilon = 0.01;
  float radius2 = uSampleRadiusWS * uSampleRadiusWS;
  
  // offset on the unit disk, spun for this pixel
  float radiusSS;
  vec2 unitOffset = tapLocation(tapIndex, rotationAngle, radiusSS);
  radiusSS *= sampleRadiusSS;
  
  vec3 Q = getOffsetPositionVS(uv, unitOffset, radiusSS);
  vec3 v = Q - positionVS;
  
  float vv = dot(v, v);
  float vn = dot(v, normalVS) - uBias;
  
#if VARIATION == 0
  
  // (from the HPG12 paper)
  // Note large epsilon to avoid overdarkening within cracks
  return float(vv < radius2) * max(vn / (epsilon + vv), 0.0);
  
#elif VARIATION == 1 // default / recommended
  
  // Smoother transition to zero (lowers contrast, smoothing out corners). [Recommended]
  float f = max(radius2 - vv, 0.0) / radius2;
  return f * f * f * max(vn / (epsilon + vv), 0.0);
  
#elif VARIATION == 2
  
  // Medium contrast (which looks better at high radii), no division.  Note that the 
  // contribution still falls off with radius^2, but we've adjusted the rate in a way that is
  // more computationally efficient and happens to be aesthetically pleasing.
  float invRadius2 = 1.0 / radius2;
  return 4.0 * max(1.0 - vv * invRadius2, 0.0) * max(vn, 0.0);
  
#else
  
  // Low contrast, no division operation
  return 2.0 * float(vv < radius2) * max(vn, 0.0);
  
#endif
}

/*void main() {
  vec3 originVS = getPositionVS(inUV);
  
#if USE_ACTUAL_NORMALS
  gBufferGeomComponents gBufferValue = decodeGBufferGeom(sGBuffer, vUV, clipFar);
  
  vec3 normalVS = gBufferValue.normal;
#else
  vec3 normalVS = reconstructNormalVS(originVS);
#endif
  
vec3 normalVS = (texture(samplerNormal, inUV).rgb);
    normalVS = normalVS * mat3(ubo.inverseView);

  vec3 sampleNoise = texture(ssaoNoise, inUV * constants.uvScale).xyz;
  
  float randomPatternRotationAngle = 2.0 * PI * sampleNoise.x;
  
  float radiusSS  = 0.0; // radius of influence in screen space
  float radiusWS  = 0.0; // radius of influence in world space
  float occlusion = 0.0;
  
  // TODO (travis): don't hardcode projScale
  float projScale = 90.0;//1.0 / (2.0 * tan(uFOV * 0.5));
  radiusWS = uSampleRadiusWS;
  radiusSS = projScale * radiusWS;
  
  for (int i = 0; i < NUM_SAMPLES; ++i) {
    occlusion += sampleAO(inUV, originVS, normalVS, radiusSS, i, 
                          randomPatternRotationAngle);
  }
  
  occlusion = 1.0 - occlusion / (5.0 * float(NUM_SAMPLES));
  occlusion = clamp(pow(occlusion, 1.0 + uIntensity), 0.0, 1.0);
  
  outFragColor = occlusion;
}
*/
void main() 
{
	// Get G-Buffer values
	vec3 fragPos = getViewPos(inUV).rgb;
	vec3 normal = (texture(samplerNormal, inUV).rgb);
    normal = normal * mat3(ubo.inverseView);

	// Get a random vector using a noise lookup
	ivec2 texDim = textureSize(samplerNormal, 0); 
	ivec2 noiseDim = textureSize(ssaoNoise, 0);
	const vec2 noiseUV = constants.uvScale * inUV;  
	vec3 randomVec = texture(ssaoNoise, noiseUV).xyz;
  //randomVec.xy += (ubo.jitter * fragPos.z) / 10;
	
	// Create TBN matrix
	vec3 tangent = normalize(randomVec - normal * dot(randomVec, normal));
	vec3 bitangent = cross(tangent, normal);
	mat3 TBN = mat3(tangent, bitangent, normal);

	// Calculate occlusion value
	float occlusion = 0.0f;
	for(int i = 0; i < SSAO_KERNEL_SIZE; i++)
	{		
		vec3 samplePos = TBN * uboSSAOKernel.samples[i].xyz; 
		samplePos = fragPos + samplePos * SSAO_RADIUS; 

		// project
		vec4 offset = vec4(samplePos, 1.0f);
		offset = ubo.projection * offset; 
		offset.xyz /= offset.w; 
		offset.xyz = offset.xyz * 0.5f + 0.5f; 
		
		float sampleDepth = -linearDepth(textureLod(samplerPositionDepth, offset.xy, 1).r); 

#define RANGE_CHECK 1
#ifdef RANGE_CHECK
		// Range check
		float rangeCheck = smoothstep(0.0f, 1.0f, SSAO_RADIUS / abs(fragPos.z - sampleDepth));
		occlusion += (sampleDepth >= samplePos.z ? 1.0f : 0.0f) * rangeCheck;           
#else
		occlusion += (sampleDepth >= samplePos.z ? 1.0f : 0.0f);  
#endif
	}
	occlusion = 1.0 - (occlusion / float(SSAO_KERNEL_SIZE));
	
	outFragColor = occlusion;
}