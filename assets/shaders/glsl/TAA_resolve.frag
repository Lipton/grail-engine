#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec2 pass_uvs;

layout (binding = 0) uniform UBO {
    mat4 inverseCombined;
    vec4 cameraPos;
    mat4 inverseView;
    mat4 inverseProjection;
    mat4 combined;
} ubo;

layout (binding = 1) uniform sampler2D u_color;
layout (binding = 2) uniform sampler2D u_normals;
layout (binding = 3) uniform sampler2D u_velocity;
layout (binding = 4) uniform sampler2D u_depth;
layout (binding = 5) uniform sampler2D u_ssr;
layout (binding = 6) uniform sampler2D u_ssr_compute;

layout (location = 0) out vec4 outFragColor;

float linearDepth(float depth) {
    const float NEAR_PLANE = 1.0;
    const float FAR_PLANE = 90.0;
	float z = depth * 2.0 - 1.0; 

	return (2.0 * NEAR_PLANE * FAR_PLANE) / (FAR_PLANE + NEAR_PLANE - z * (FAR_PLANE - NEAR_PLANE));	
}

layout (std140, push_constant) uniform pushConstants {
    float enableSSR;
} constants;

vec4 getViewPos(vec2 texCoord) {
	// Calculate out of the fragment in screen space the view space position.

	float x = texCoord.s * 2.0 - 1.0;
	float y = texCoord.t * 2.0 - 1.0;

	// Assume we have a normal depth range between 0.0 and 1.0
	float z = texture(u_depth, texCoord).r;

	vec4 posProj = vec4(x, y, z, 1.0);

	vec4 posView = ubo.inverseProjection * posProj;

	posView /= posView.w;

	return posView;
}

vec4 getWorldPos(vec2 texCoord, float depth) {
	float x = texCoord.s * 2.0 - 1.0;
	float y = texCoord.t * 2.0 - 1.0;

	float z = depth;
	vec4 posProj = vec4(x, y, z, 1.0);
	vec4 posView = ubo.inverseCombined * posProj;
	posView /= posView.w;

	return posView;
}

// 0.1f
// 16
// 6

#define MAX_REFLECTION_RAY_MARCH_STEP 0.18f
#define NUM_RAY_MARCH_SAMPLES 8
#define NUM_BINARY_SEARCH_SAMPLES 6

#define Scale vec3(.8, .8, .8)
#define K 19.19

vec3 hash(vec3 a)
{
    a = fract(a * Scale);
    a += dot(a, a.yxz + K);
    return fract((a.xxy + a.yxx)*a.zyx);
}



bool getReflection(vec3 screenSpaceReflectionVec, vec3 ScreenSpacePos, out vec2 uvs) {
// Raymarch in the direction of the ScreenSpaceReflectionVec until you get an intersection with your z buffer
	
        vec3 PrevRaySample;
    for (int RayStepIdx = 0; RayStepIdx<NUM_RAY_MARCH_SAMPLES; RayStepIdx++)
	{

		vec3 RaySample = (RayStepIdx * MAX_REFLECTION_RAY_MARCH_STEP) * screenSpaceReflectionVec + ScreenSpacePos;
		float ZBufferVal = texture(u_depth, RaySample.xy, 0).r;

        vec2 UVSamplingAttenuation = smoothstep(0.05, 0.1, RaySample.xy) * (1 - smoothstep(0.95, 1, RaySample.xy));
        UVSamplingAttenuation.x *= UVSamplingAttenuation.y;
				
        //if(UVSamplingAttenuation.x > 0) {
            if (RaySample.z > ZBufferVal )
            {
                vec3 MinRaySample = PrevRaySample;
                vec3 MaxRaySample = RaySample;
                vec3 MidRaySample;
                for (int i = 0; i < NUM_BINARY_SEARCH_SAMPLES; i++)
                {
                    MidRaySample = mix(MinRaySample, MaxRaySample, 0.5);
                    float ZBufferVal = texture(u_depth, MidRaySample.xy, 0).r;

                    if (MidRaySample.z > ZBufferVal)
                        MaxRaySample = MidRaySample;
                    else
                        MinRaySample = MidRaySample;
                }

                uvs = MidRaySample.xy;
                return true;
            }
        //}

        PrevRaySample = RaySample;
	}

	return false;
}

vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}

vec3 Specular_F_Roughness(vec3 specularColor, float a, vec3 h, vec3 v) {
	return (specularColor + (max(vec3(1.0 - a), specularColor) - specularColor) * pow((1 - clamp(dot(v, h), 0.0, 1.0)), 5.0));
}


/*vec3 prefilteredReflection(vec3 R, float roughness) {
	const float MAX_REFLECTION_LOD = 9.0; // todo: param/const
	float lod = roughness * MAX_REFLECTION_LOD;
	float lodf = floor(lod);
	float lodc = ceil(lod);
	vec3 a = textureLod(u_envSpec, R, lodf).rgb;
	vec3 b = textureLod(u_envSpec, R, lodc).rgb;
	return mix(a, b, lod - lodf);
}*/

float uXPixelDistance = 1.0 / (3840.0 / 2.0);
float uYPixelDistance = 1.0 / (2160.0 / 2.0);
 
/*const float jump = 1.0f;
 
const float pointRange = 10.0f;*/

vec3 box(float jump, float pointRange, vec2 uvs) {
    vec4 color = vec4(0, 0, 0, 0);
    vec2 point;
    int count = 0;
  
    // Calculate the total color intensity around the pixel
    // In this case we are calculating pixel intensity around 10 pixels
    for(float u = -pointRange; u < pointRange ; u+=jump) {
        for(float v = -pointRange ; v < pointRange ; v+=jump) {
            point.x = uvs.x  + u * uXPixelDistance;
            point.y = uvs.y  + v * uYPixelDistance;
             
            // If the point is within the range[0, 1]
            if (point.y >= 0.0f && point.x >= 0.0f &&
                point.y <= 1.0f && point.x <= 1.0f ) {
                ++count;
                color += texture(u_ssr, point.xy);
            }
        }
    }
     
    color = color / float(count);

    return color.rgb;
}


void main() {
    vec4 color = texture(u_color, pass_uvs);

    vec4 reflection = vec4(0.0);

    if(constants.enableSSR == 1.0)
        reflection = texture(u_ssr_compute, pass_uvs);

    vec3 finalColor = color.rgb + (reflection.rgb * reflection.w);

    outFragColor = vec4(finalColor, 1.0);
}
