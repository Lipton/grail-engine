#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec3 pass_position;
layout (location = 1) in vec3 pass_normal;
layout (location = 2) in vec2 pass_uvs;

layout (location = 0) out vec4 outFragColor;
layout (location = 1) out vec4 outFragNormal;

layout (binding = 1) uniform sampler2D u_texture;

const vec3 lightPos = vec3(100, 120, 0);
const vec3 viewPos = vec3(0, 0, 0);



void main() {
    vec4 albedo = //vec4(material.color, 1.0);
				texture(u_texture, pass_uvs);

    if(albedo.a < 0.1) discard;

      
    outFragColor = albedo;
    outFragNormal = vec4(1.0, 0.5, 0.2, 1.0);

      /*vec3 ambient = albedo.rgb * 0.2;

      vec3 lightDir = normalize(lightPos - pass_position);
      vec3 normal = normalize(pass_normal);
      float diff = max(dot(lightDir, normal), 0.0);
      vec3 diffuse = albedo.rgb * diff * vec3(1.0, 1.0, 1.0);
sds
      outFragColor = vec4(ambient + diffuse, 1.0);*/
}
