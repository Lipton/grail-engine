#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec2 inPos;
layout (location = 1) in vec2 inUvs;

layout (binding = 0) uniform UBO {
    mat4 inverseProjection;
    mat4 view;
} ubo;

out gl_PerVertex {
    vec4 gl_Position;
};

layout (location = 0) out vec2 pass_uvs;
layout (location = 1) smooth out vec3 pass_eye;

void main() {
    mat3 inverseView = transpose(mat3(ubo.view));
    vec3 unprojected = (ubo.inverseProjection * vec4(inPos, 1.0, 1.0)).xyz;

	gl_Position = vec4(inPos, 0.0, 1.0);

    pass_eye = inverseView * unprojected;
    pass_uvs = inUvs;
}
