#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec3 inPos;
layout (location = 1) in vec3 inNormals;
layout (location = 2) in vec2 inUvs;

layout (binding = 0) uniform UBO {
	mat4 projectionMatrix;
	mat4 viewMatrix;
} ubo;

layout (std140, push_constant) uniform pushConstants {
	mat4 model;
} material;

layout (location = 0) out vec3 pass_position;
layout (location = 1) out vec3 pass_normals;
layout (location = 2) out vec2 pass_uvs;

out gl_PerVertex {
    vec4 gl_Position;
};

void main() {
	vec4 position = material.model * vec4(inPos, 1.0);
	pass_position = position.xyz;
	pass_normals = (mat3(material.model) * inNormals);
	pass_uvs = inUvs;

	gl_Position = ubo.projectionMatrix * ubo.viewMatrix * position;
}
