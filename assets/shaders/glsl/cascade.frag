#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec2 pass_uvs;

layout (binding = 1) uniform sampler2D u_texture;
layout (binding = 2) uniform sampler2D u_normals;
layout (binding = 3) uniform sampler2D u_rougness;
layout (binding = 4) uniform sampler2D u_metallic;

void main() {
    vec4 albedo = texture(u_texture, pass_uvs);

    if(albedo.a < 0.5) discard;
}
