#version 450

layout (location = 0) in vec3 inPos;
layout (location = 1) in vec3 inColor;

layout (push_constant) uniform PushConstants {
    mat4 combined;
} pushConstants;

layout (location = 0) out vec3 outPosition;
layout (location = 1) out vec3 outColor;

out gl_PerVertex 
{
	vec4 gl_Position;   
};

void main() 
{
	outPosition = inPos;
	outColor = inColor;
	gl_Position = pushConstants.combined * vec4(inPos, 1.0);
}