#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec2 pass_uvs;

layout (binding = 0) uniform UBO {
    mat4 inverseCombined;
    vec4 cameraPos;
    mat4 inverseView;
    mat4 inverseProjection;
    mat4 combined;
    vec4 cameraScreen;
} ubo;

layout (binding = 1) uniform sampler2D u_gNormals;
layout (binding = 2) uniform sampler2D u_gAlbedo;
layout (binding = 3) uniform sampler2D u_halfDepth;

layout (location = 0) out vec4 outFragColor;

layout (std140, push_constant) uniform pushConstants {
    vec4 texelCamera;
    int enable;
} constants;

vec4 getWorldPos(vec2 texCoord, float depth) {
	float x = texCoord.s * 2.0 - 1.0;
	float y = texCoord.t * 2.0 - 1.0;

	float z = depth;
	vec4 posProj = vec4(x, y, z, 1.0);
	vec4 posView = ubo.inverseCombined * posProj;
	posView /= posView.w;

	return posView;
}

#define PIXEL_STRIDE 160
#define NUM_BINARY_SEARCH_SAMPLES 3

bool getReflection(int samples, float rayStep, float jitter, vec3 screenSpaceReflectionVec, vec3 ScreenSpacePos, out vec2 uvs, out int iterations) {
    vec3 step = rayStep * screenSpaceReflectionVec;

    for (int RayStepIdx = 0; RayStepIdx < samples; RayStepIdx++) {
        uvs = vec2(0.0);

        vec3 RaySample = (RayStepIdx * rayStep) * screenSpaceReflectionVec + ScreenSpacePos;

        float ZBufferVal = texture(u_halfDepth, RaySample.xy).r;

        if (RaySample.z > ZBufferVal) {
            //if(constants.enable == 1.0) {
                for(int i = 0; i < NUM_BINARY_SEARCH_SAMPLES; i++) {
                    step *= 0.5;

                    RaySample -= step;
                    
                    if(RaySample.z < texture(u_halfDepth, RaySample.xy).r) {
                        RaySample += step;
                    }
                }
            //}
            uvs = RaySample.xy;
            return true;
        }
    }

    iterations = 1;
    uvs = vec2(0.0);
	return false;
}

void main() {
    vec4 g_normals = texture(u_gNormals, pass_uvs);
    vec4 g_albedo = texture(u_gAlbedo, pass_uvs);

    float roughness = g_albedo.a;
    float metallic = g_normals.a;

    float deviceZ = texture(u_halfDepth, pass_uvs).r;
    vec3 worldPos = getWorldPos(pass_uvs, deviceZ).xyz;

    vec3 viewDir = normalize(ubo.cameraPos.xyz - worldPos);

    vec4 screenSpacePos = vec4(pass_uvs, deviceZ, 1.0);

    vec3 reflectionVector = reflect(viewDir.xyz, g_normals.xyz); 
    vec4 pointAlongReflection = vec4(constants.texelCamera.w * reflectionVector.xyz + worldPos.xyz, 1.0);
    vec4 samplePoint = ubo.combined * pointAlongReflection;
    samplePoint /= samplePoint.w;
    samplePoint.xy = samplePoint.xy * 0.5 + 0.5;

    vec3 sampleRay = normalize(samplePoint.xyz - screenSpacePos.xyz);

    vec2 result = vec2(0.0);

    float cameraAttenuation = 1 - smoothstep(0.25, 0.5, dot(-viewDir, reflectionVector));
    
    vec2 rayStep = (1.0 / constants.texelCamera.xy) * PIXEL_STRIDE;
    int samples = int(max(constants.texelCamera.x, constants.texelCamera.y) / PIXEL_STRIDE);

    int iterations = 1;

    vec2 uv2 = pass_uvs * constants.texelCamera.xy;
	float c = (uv2.x + uv2.y) * 0.25;
	float jitter = mod( c, 1.0);

    if(cameraAttenuation > 0)
        getReflection(samples, min(rayStep.x, rayStep.y), jitter, sampleRay, screenSpacePos.xyz, result, iterations);

    vec4 reflectedNormal = texture(u_gNormals, result);
    float directionAttenuation = smoothstep(-0.17, 0.0, dot(reflectedNormal.xyz, reflectionVector));
    
    vec2 uvNDC = result * 2.0 - 1.0;
    float dim = min(1.0, max( abs(uvNDC.x), abs(uvNDC.y)));
    float screenFade = 0.25;
    float uvAttenuation = 1.0 - (max( 0.0, dim - screenFade) / (1.0 - screenFade));

    float iterationAttenuation = 1.0 - (samples / iterations);

    float Eta = 0.0815;
    float fresnel = Eta + (1.0 - Eta) * pow(max(0.0, 1.0 - dot(viewDir, g_normals.rgb)), 1.0);

    outFragColor = vec4(texture(u_gAlbedo, result).rgb, uvAttenuation * cameraAttenuation * fresnel);
}
