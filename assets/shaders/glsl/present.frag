#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec2 pass_uvs;

layout (binding = 0) uniform sampler2D u_texture;
layout (binding = 1) uniform sampler2D u_velocity;
layout (binding = 2) uniform sampler2D u_lut;


layout (location = 0) out vec4 outFragColor;

layout (std140, push_constant) uniform pushConstants {
    vec4 texelSize;
} constants;

const float exposure = 3.5;

const float A = 0.22;
const float B = 0.30;
const float C = 0.10;
const float D = 0.20;
const float E = 0.01;
const float F = 0.30;
const float W = 11.20;

const float gamma = 2.2;

vec3 tonemap(vec3 color) {
	return ((color * (A * color + C * B) + D * E) / (color * (A * color + B) + D * F)) - E / F;
}

const float fringe = 2.0;
const float threshold = 0.0;
vec3 getColor(vec2 coords, vec2 texel, float fringe) {
	vec3 col = vec3(0.0);

	col.r = texture(u_texture, coords + vec2(0.0, 1.0) * texel * fringe).r;
	col.g = texture(u_texture, coords + vec2(-0.866, -0.5) * texel * fringe).g;
	col.b = texture(u_texture, coords + vec2(0.866, -0.5) * texel * fringe).b;

	return col;
}

const mat3 ACESInputMat = {
    {0.59719, 0.35458, 0.04823},
    {0.07600, 0.90834, 0.01566},
    {0.02840, 0.13383, 0.83777}
};

// ODT_SAT => XYZ => D60_2_D65 => sRGB
const mat3 ACESOutputMat = {
    { 1.60475, -0.53108, -0.07367},
    {-0.10208,  1.10813, -0.00605},
    {-0.00327, -0.07276,  1.07602}
};

vec3 RRTAndODTFit(vec3 v) {
    vec3 a = v * (v + 0.0245786f) - 0.000090537f;
    vec3 b = v * (0.983729f * v + 0.4329510f) + 0.238081f;
    return a / b;
}

vec3 ACESFitted(vec3 color) {
    color = transpose(ACESInputMat) * color;

    // Apply RRT and ODT
    color = RRTAndODTFit(color);

    color = transpose(ACESOutputMat) * color;

    // Clamp to [0, 1]
    color = clamp(color, 0, 1);

    return color;
}

vec3 ACESFilm( vec3 x )
{
    float a = 2.51f;
    float b = 0.03f;
    float c = 2.43f;
    float d = 0.59f;
    float e = 0.14f;
    return clamp((x*(a*x+b))/(x*(c*x+d)+e), 0, 1);
}

vec3 grade(vec3 color, sampler2D lut) {
    vec2 lutSize = 1.0 / vec2(512, 512);
    vec4 lutUV;

    color = clamp(color, 1.0, 0.0) * (lutSize.y - 1.0);
    lutUV.w = floor(color.b);
    lutUV.xy = (color.rg + 0.5) * lutSize;
    lutUV.x += lutUV.w * lutSize.y;
    lutUV.z = lutUV.x + lutSize.y;

    return mix(texture(lut, lutUV.xy).rgb, texture(lut, lutUV.zy).rgb, color.r - lutUV.w);
}

vec3 applyLUT(vec3 baseColor) {
	const float Size = 64;
	const float SizeRoot = 8;
	//Our input
	vec3 baseTexture = baseColor;

	//Manual trilinear interpolation

	//We need to clamp since our values go, for example, from 0 to 15. But with a red value of 1.0 we would get 16, which is on the next table already.

	//OBSOLETE: We also need to shift half a pixel to the left, since our sampling locations do not match the storage location (see CreateLUT)
	//float halfOffset = 0.5f;

	float red = baseTexture.r * (Size - 1);

	float redinterpol = fract(red);

	float green = baseTexture.g * (Size - 1);
	float greeninterpol = fract(green);

	float blue = baseTexture.b * (Size - 1);
	float blueinterpol = fract(blue);

	//Blue base value

	float row = trunc(blue / SizeRoot);
	float col = trunc(mod(blue, SizeRoot));

	vec2 blueBaseTable = vec2(trunc(col * Size), trunc(row * Size));

	vec3 b0r1g0;
	vec3 b0r0g1;
	vec3 b0r1g1;
	vec3 b1r0g0;
	vec3 b1r1g0;
	vec3 b1r0g1;
	vec3 b1r1g1;

	/*
	We need to read 8 values (like in a 3d LUT) and interpolate between them.
	This cannot be done with default hardware filtering so I am doing it manually.
	Note that we must not interpolate when on the borders of tables!
	*/

	//Red 0 and 1, Green 0

	vec3 b0r0g0 = texelFetch(u_lut, ivec2(blueBaseTable.x + red, blueBaseTable.y + green), 0).rgb;

	if (red < Size - 1)
		b0r1g0 = texelFetch(u_lut, ivec2(blueBaseTable.x + red + 1, blueBaseTable.y + green), 0).rgb;
	else
		b0r1g0 = b0r0g0;

	// Green 1

	if (green < Size - 1)
	{
		//Red 0 and 1
		b0r0g1 = texelFetch(u_lut, ivec2(blueBaseTable.x + red, blueBaseTable.y + green + 1), 0).rgb;

		if (red < Size - 1)
			b0r1g1 = texelFetch(u_lut, ivec2(blueBaseTable.x + red + 1, blueBaseTable.y + green + 1), 0).rgb;
		else
			b0r1g1 = b0r0g1;
	}
	else
	{
		b0r0g1 = b0r0g0;
		b0r1g1 = b0r0g1;
	}

	if (blue < Size - 1)
	{
		blue += 1;
		row = trunc(blue / SizeRoot);
		col = trunc(mod(blue, SizeRoot));

		blueBaseTable = vec2(trunc(col * Size), trunc(row * Size));

		b1r0g0 = texelFetch(u_lut, ivec2(blueBaseTable.x + red, blueBaseTable.y + green), 0).rgb;

		if (red < Size - 1)
			b1r1g0 = texelFetch(u_lut, ivec2(blueBaseTable.x + red + 1, blueBaseTable.y + green), 0).rgb;
		else
			b1r1g0 = b0r0g0;

		// Green 1
		if (green < Size - 1)
		{
			//Red 0 and 1
			b1r0g1 = texelFetch(u_lut, ivec2(blueBaseTable.x + red, blueBaseTable.y + green + 1), 0).rgb;

			if (red < Size - 1)
				b1r1g1 = texelFetch(u_lut, ivec2(blueBaseTable.x + red + 1, blueBaseTable.y + green + 1), 0).rgb;
			else
				b1r1g1 = b0r0g1;
		}
		else
		{
			b1r0g1 = b0r0g0;
			b1r1g1 = b0r0g1;
		}
	}
	else
	{
		b1r0g0 = b0r0g0;
		b1r1g0 = b0r1g0;
		b1r0g1 = b0r0g0;
		b1r1g1 = b0r1g1;
	}

	vec3 result = mix(mix(b0r0g0, b0r1g0, redinterpol), mix(b0r0g1, b0r1g1, redinterpol), greeninterpol);
	vec3 result2 = mix(mix(b1r0g0, b1r1g0, redinterpol), mix(b1r0g1, b1r1g1, redinterpol), greeninterpol);

	result = mix(result, result2, blueinterpol);

	return result;
}

vec3 lookup(in vec3 textureColor, in sampler2D lookupTable) {
    //#ifndef LUT_NO_CLAMP
        textureColor = clamp(textureColor, 0.0, 1.0);
    //#endif

    float blueColor = textureColor.b * 63.0;

    vec2 quad1;
    quad1.y = floor(floor(blueColor) / 8.0);
    quad1.x = floor(blueColor) - (quad1.y * 8.0);

    vec2 quad2;
    quad2.y = floor(ceil(blueColor) / 8.0);
    quad2.x = ceil(blueColor) - (quad2.y * 8.0);

    vec2 texPos1;
    texPos1.x = (quad1.x * 0.125) + 0.5/512.0 + ((0.125 - 1.0/512.0) * textureColor.r);
    texPos1.y = (quad1.y * 0.125) + 0.5/512.0 + ((0.125 - 1.0/512.0) * textureColor.g);

    #ifdef LUT_FLIP_Y
        texPos1.y = 1.0-texPos1.y;
    #endif

    vec2 texPos2;
    texPos2.x = (quad2.x * 0.125) + 0.5/512.0 + ((0.125 - 1.0/512.0) * textureColor.r);
    texPos2.y = (quad2.y * 0.125) + 0.5/512.0 + ((0.125 - 1.0/512.0) * textureColor.g);

    #ifdef LUT_FLIP_Y
        texPos2.y = 1.0-texPos2.y;
    #endif

    vec4 newColor1 = texture(lookupTable, texPos1);
    vec4 newColor2 = texture(lookupTable, texPos2);

    vec4 newColor = mix(newColor1, newColor2, fract(blueColor));

    return newColor.xyz;
}

void main() {
    vec2 velocity = texture(u_velocity, pass_uvs).xy;
    velocity *= constants.texelSize.z;
    float speed = length(velocity / constants.texelSize.xy);

    vec2 uv = pass_uvs;
    uv *= 1.0 - uv.yx;

    float vig = uv.x * uv.y * 15.0;
    vig = pow(vig, 0.0125);

    int samples = clamp(int(speed), 1, 24);
    vec3 color = texture(u_texture, pass_uvs).rgb;
    for(int i = 1; i < samples; ++i) {
        vec2 offset = velocity * (float(i) / float(samples - 1) - 0.5);
        color += texture(u_texture, pass_uvs + offset).rgb;
    }
    color /= float(samples);

    vec3 mappedColor = vec3(0.0);
    
mappedColor = ACESFitted(color * exposure);
mappedColor = pow(mappedColor, vec3(1.0 / 2.2));
    /*if(constants.texelSize.z == 0) {*/
        //mappedColor = tonemap(color * exposure);
       // vec3 whiteScale = 1.0 / tonemap(vec3(W));
        //mappedColor *= whiteScale;
       	//mappedColor = pow(mappedColor, vec3(1.0 / 2.2)); 
    /*} else {
        mappedColor = ACESFitted(color * exposure);
        // mappedColor = pow(mappedColor, vec3(1.0 / 2.2));
   /* }*/
    if(constants.texelSize.w == 1) {
       vec3 vv = applyLUT(mappedColor);

        outFragColor = vec4(vv * vig, 1.0);
    } else {
        outFragColor = vec4(mappedColor * vig, 1.0);
    }
}
