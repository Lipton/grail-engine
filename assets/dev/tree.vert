#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec3 inPos;
layout (location = 1) in vec3 inNormals;
layout (location = 2) in vec3 inTangent;
layout (location = 3) in vec2 inUvs;

#define SHADOW_MAP_CASCADE_COUNT 4

layout (binding = 0) uniform UBO {
	mat4 combined;
	mat4 lastCombined;

	mat4[SHADOW_MAP_CASCADE_COUNT] cascadeViewProjMat;

	vec4 jitter;
	
	vec4 cameraPos;

	vec2 time;
} ubo;

layout (std140, push_constant) uniform pushConstants {
	mat4 model;
	mat4 lastModel;
	vec2 cascadeIndex;
	
	//param type: color, name: Color
	vec4 color;
	
	//param type: component|scalar, name: UV Scale, step:0.05
	vec2 uvScale;

	//param type:bool, name:Disable Jitter
	float disableJitter;

	//param type:bool, name:Invert Metallic
	float invertMetallic;

	//param type:bool, name:Invert Roughness
	float invertRoughness;

	//param type:list, name:Output To Albedo, list:Albedo;Normals;Roughness;Metallic;Velocity
	float albedoOutput;
} material;

layout (location = 0) out vec3 pass_position;
layout (location = 1) out vec3 pass_normals;
layout (location = 2) out vec2 pass_uvs;
layout (location = 3) out vec4 pass_v_pos;
layout (location = 4) out vec4 pass_last_v_pos;
layout (location = 5) out vec2 pass_velocity;
layout (location = 6) out vec3 pass_tangent;
layout (location = 7) out vec3 pass_bitangent;
out gl_PerVertex {
    vec4 gl_Position;
};

vec4 surface() {
	vec4 surf;

	vec4 position = material.model * vec4(inPos, 1.0);
	pass_position = position.xyz;

	vec4 offset = vec4(0.0);

	/*offset.x = (material.windStrength * 0.015) * sin( mod(inPos.x * 1000.0, 10.5) + ubo.time.x * 1.5);
    offset.z = (material.windStrength * 0.015) * cos( mod(inPos.y * 2000.0, 10.5) + ubo.time.x * 2.0);
	offset.y = (material.windStrength * 0.015) * cos( mod(inPos.y * 2000.0, 10.5) + ubo.time.x * 2.0);*/

	offset.x = (inUvs.y) * 0.075 * sin( mod(inPos.x * 10.0, 3.5) + ubo.time.x * 3.0);
    offset.z = (inUvs.y) * 0.085 * cos( mod(inPos.y * 5.0, 3.5) + ubo.time.x * 2.0);
	offset.xyz *= 4.0;

	position.xyz += offset.xyz;
	pass_velocity = vec2(0);

	//bit = inBitangents;

#if defined(DEFERRED) || defined(DEPTH)
	vec4 v_pos = ubo.combined * position;
	v_pos.xy += (ubo.jitter.xy * material.disableJitter) * v_pos.w;

	surf = v_pos;

#if defined (DEFERRED)
	vec4 lastPos = material.lastModel * vec4(inPos, 1.0);
	lastPos.xyz += offset.xyz;

	vec4 last_v_pos = ubo.lastCombined * lastPos;
	last_v_pos.xy += ubo.jitter.xy * last_v_pos.w;
	pass_last_v_pos = last_v_pos;
#endif

#endif

#if defined(SHADOW)
	vec4 v_pos = ubo.cascadeViewProjMat[int(material.cascadeIndex.x)] * vec4(position.xyz, 1.0);

	surf = v_pos;
#endif
	pass_v_pos = surf;

	return surf;
}

void main() {
	gl_Position = surface();

#define DEFERRED

#if defined (DEFERRED)
	vec3 vertexBinormal = cross(inNormals, inTangent);
	mat3 normalMatrix = transpose(inverse(mat3(material.model)));

	pass_normals = normalize(normalMatrix * inNormals);
    pass_tangent = normalize(normalMatrix * inTangent);
    pass_tangent = normalize(pass_tangent - dot(pass_tangent, pass_normals) * pass_normals);
    pass_bitangent = normalize(normalMatrix * vertexBinormal);
	pass_bitangent = cross(pass_normals, pass_tangent);
#endif
	pass_uvs = inUvs * material.uvScale;
}