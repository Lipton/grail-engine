#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec3 pass_position;
layout (location = 1) in vec3 pass_normal;
layout (location = 2) in vec2 pass_uvs;
layout (location = 3) in vec4 pass_v_pos;
layout (location = 4) in vec4 pass_last_v_pos;
layout (location = 5) in vec2 pass_velocity;
layout (location = 6) in vec3 pass_tangent;
layout (location = 7) in vec3 pass_bitangent;

#define SHADOW_MAP_CASCADE_COUNT 4

layout (binding = 0) uniform UBO {
	mat4 combined;
	mat4 lastCombined;

	vec4 jitter;

	mat4[SHADOW_MAP_CASCADE_COUNT] cascadeViewProjMat;
	
	vec4 cameraPos;

	vec2 time;
} ubo;


layout (binding = 1) uniform sampler2D albedoTexture;
layout (binding = 2) uniform sampler2D normalTexture;
layout (binding = 3) uniform sampler2D RMCTexture;
layout (binding = 4) uniform sampler2D metallicTexture;
layout (binding = 5) uniform sampler2D aoTexture;

layout (location = 0) out vec4 outFragColor;
layout (location = 1) out vec4 outFragNormal;
layout (location = 2) out vec2 outFragVelocity;

layout (std140, push_constant) uniform pushConstants {
	mat4 model;
	mat4 lastModel;
	vec2 cascadeIndex;
	
	//param type: color, name: Color
	vec4 color;
	
	//param type: component|scalar, name: UV Scale, step:0.05
	vec2 uvScale;

	//param type:bool, name:Disable Jitter
	float disableJitter;

	//param type:bool, name:Invert Metallic
	float invertMetallic;

		//param type:bool, name:Invert Roughness
	float invertRoughness;

	//param type:list, name:Output To Albedo, list:Default;Albedo;Normals;Roughness;Metallic;Velocity
	float albedoOutput;
} material;

mat3 cotangent_frame( vec3 N, vec3 p, vec2 uv ) {
    // get edge vectors of the pixel triangle
    vec3 dp1 = dFdx( p );
    vec3 dp2 = dFdy( p );
    vec2 duv1 = dFdx( uv );
    vec2 duv2 = dFdy( uv );

    // solve the linear system
    vec3 dp2perp = cross( dp2, N );
    vec3 dp1perp = cross( N, dp1 );
    vec3 T = dp2perp * duv1.x + dp1perp * duv2.x;
    vec3 B = dp2perp * duv1.y + dp1perp * duv2.y;

    // construct a scale-invariant frame
    float invmax = inversesqrt( max( dot(T,T), dot(B,B) ) );
    return mat3( T * invmax, B * invmax, N );
}

void main() {
    vec4 albedo = texture(albedoTexture, pass_uvs);

    if(albedo.a < 0.1) discard;

#if defined (DEFERRED)
    albedo.a = texture(RMCTexture, pass_uvs).g;

    if(material.invertRoughness == 1.0) {
        albedo.a = 1.0 - albedo.a;
    }

    albedo.rgb = (albedo.rgb * material.color.rgb) * texture(aoTexture, pass_uvs).rgb;

    outFragColor = albedo;

    mat3 TBN = transpose(mat3(pass_tangent, pass_bitangent, normalize(pass_normal)));
    vec3 normalMap = texture(normalTexture, pass_uvs).xyz;
    normalMap.z = 1.0;
    normalMap = normalize(normalMap * 2.0 - 1.0);
    normalMap = normalize(normalMap * TBN);

    outFragNormal.xyz = normalMap.xyz;


    /*vec4 normalMap = texture(normalTexture, pass_uvs);
    normalMap.z = 1.0;
    normalMap = normalize(normalMap * 2.0 - 1.0);
    //normalMap.y *= -1;
    //normalMap.rgb = normalize(normalMap.rgb);
    mat3 TBN = cotangent_frame(pass_normal, pass_position, pass_uvs);
    outFragNormal.rgb = pass_normal; //normalize(TBN * normalMap.rgb);
    outFragNormal.rgb = normalize(outFragNormal.rgb * 2.0 - 1.0);
    outFragNormal.rgb = vec4(normalize(cotangent_frame(pass_normal, pass_position, pass_uvs) * normalMap.rgb), 1.0).rgb;
    
    outFragNormal.xyz += 0.000003; //0.000003*/

    //outFragNormal.xyz = pass_normal;
    
    //vec4(normalize(cotangent_frame(pass_normal, pass_position, pass_uvs) * normalMap.rgb), 1.0);
    outFragNormal.a = texture(RMCTexture, pass_uvs).r;

    if(material.invertMetallic == 1.0) {
        outFragNormal.a = 1.0 - outFragNormal.a;
    }

    //outFragNormal.a = 1.0;
    //outFragColor.a = 0.0;

    //outFragColor.rgb = vec3(outFragNormal.a);

    vec2 a = (pass_v_pos.xy / pass_v_pos.w) * 0.5 + 0.5;
    vec2 b = (pass_last_v_pos.xy / pass_last_v_pos.w) * 0.5 + 0.5;

    outFragVelocity = (a - b) + pass_velocity;

    switch(int(material.albedoOutput)) {
        case 0:
        //outFragColor = albedo;
        break;

        case 1:
        outFragColor = vec4(outFragNormal.rgb, 1.0);
        break;

        case 2:
        outFragColor = vec4(vec3(albedo.a), 1.0);
        break;

        case 3:
        outFragColor = vec4(vec3(outFragNormal.a), 1.0);
        break;

        case 4:
        outFragColor = vec4(outFragVelocity * 100.0, 0.0, 1.0);
        break;
    }
#endif
}
