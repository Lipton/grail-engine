#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

#define SHADOW_MAP_CASCADE_COUNT 4

layout (binding = 0) uniform UBO {
	mat4 combined;
	mat4 lastCombined;

	vec4 jitter;

	mat4[SHADOW_MAP_CASCADE_COUNT] cascadeViewProjMat;
	
	vec4 cameraPos;

	vec2 time;
} ubo;


layout (binding = 1) uniform sampler2D albedoTexture;
layout (binding = 2) uniform sampler2D normalTexture;
layout (binding = 3) uniform sampler2D roughnessTexture;
layout (binding = 4) uniform sampler2D metallicTexture;
layout (binding = 5) uniform sampler2D aoTexture;

layout (location = 0) out vec4 outFragColor;

layout (std140, push_constant) uniform pushConstants {
	mat4 model;
	mat4 lastModel;
} material;


void main() {
    outFragColor = vec4(1.0);
}
