LOCAL_PATH := $(call my-dir)

# FREETYPE
include $(CLEAR_VARS)

LOCAL_MODULE := freetype
LOCAL_CFLAGS := -std=c++11 -fexceptions -frtti

LOCAL_SRC_FILES := $(wildcard $(LOCAL_PATH)/../libs/freetype/src/*.cpp)

LOCAL_C_INCLUDES := $(LOCAL_PATH)/../libs/freetype/include

include $(BUILD_SHARED_LIBRARY)

# GRAIL_ENGINE
include $(CLEAR_VARS)

LOCAL_MODULE := grail-engine
LOCAL_CFLAGS := -std=c++11 -fexceptions -frtti

LOCAL_ASSET_DIR := $(LOCAL_PATH)/../assets

LOCAL_SRC_FILES := $(wildcard $(LOCAL_PATH)/../src/*.cpp)

LOCAL_LDLIBS :=-landroid
LOCAL_STATIC_LIBRARIES := android_native_app_glue freetype

LOCAL_C_INCLUDES := $(LOCAL_PATH)/../libs/vulkan/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../libs/glm/include
LOCAL_C_INCLUDES +:= $(LOCAL_PATH)/../libs/freetype/include

include $(BUILD_SHARED_LIBRARY)

$(call import-module,android/native_app_glue)